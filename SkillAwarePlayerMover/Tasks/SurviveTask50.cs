using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;
using SkillAwarePlayerMover.Class;
using SkillAwarePlayerMover.Helpers;

namespace SkillAwarePlayerMover.Tasks
{
    public class SurviveTask50 : ITask
    {
        private static readonly ILog Log = Logger.GetLoggerInstanceForType();

        private readonly Stopwatch _dodgeCooldownTimer;
        private readonly Stopwatch _kitingCooldownTimer;
        private readonly Stopwatch _fleeStrongboxCooldownTimer;

        private List<Vector2i> _blacklistedPoints;

        private bool _fleeEventBroadcasted;

        private Coroutine _kiteCoroutine;
        private Coroutine _dodgeCoroutine;
        private Coroutine _fleeStrongboxCoroutine;

        private IEnumerable<CachedMonster> _cachedMonsters;

        public SurviveTask50()
        {
            _dodgeCooldownTimer = Stopwatch.StartNew();
            _fleeStrongboxCooldownTimer = Stopwatch.StartNew();
            _kitingCooldownTimer = Stopwatch.StartNew();

            _blacklistedPoints = new List<Vector2i>();

            _kiteCoroutine = new Coroutine(() => KiteMonsters());
        }

        #region IAuthored implementation

        /// <summary>The name of this task.</summary>
        public string Name => "SurviveTask50";

        /// <summary>A description of what this task does.</summary>
        public string Description => "This task handles dodging dangerous attacks and kiting dangerous monsters.";

        /// <summary>The author of this task.</summary>
        public string Author => "Alcor75; modified by stealthy";

        /// <summary>The version of this task.</summary>
        public string Version => "0.0.2.1";

        #endregion 

        /// <summary>
        /// Coroutine logic to execute.
        /// NOTE: I believe most of the Coroutine initialization and resumption should be done in Tick() as is done in QuestBot.
        ///     
        /// </summary>
        /// <param name="type">The requested type of logic to execute.</param>
        /// <param name="param">Data sent to the object from the caller.</param>
        /// <returns>true if logic was executed to handle this type and false otherwise.</returns>
        public async Task<bool> Logic(string type, params object[] param)
        {
            //if (type != "task_execute") return false;

            //// Clean up stuff
            //_cachedMonsters = null;

            //UpdateSettingsGui();

            //if (!LokiPoe.IsInGame || LokiPoe.Me.IsDead || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout)
            //{
            //    _blacklistedPoints.Clear();
            //    return false;
            //}

            //// If I'm kiting and ready to kite, or dodging and ready to dodge, calculate the fear level.
            //var canKite = SkillAwarePlayerMoverSettings.Instance.KitineLogicValue && _kitingCooldownTimer.ElapsedMilliseconds >= SkillAwarePlayerMoverSettings.Instance.FrequencyValue;
            //var canDodge = SkillAwarePlayerMoverSettings.Instance.DodgeLogicValue && _dodgeCooldownTimer.ElapsedMilliseconds >= SkillAwarePlayerMoverSettings.Instance.EscapeCooldownValue;
            //var canFleeStrongbox = !SkillAwarePlayerMoverSettings.Instance.KitineLogicValue && SkillAwarePlayerMoverSettings.Instance.StrongBoxLogicValue;

            //// If we're either kiting or dodging, keep tabs of nearby monsters.
            //double fearLevel = 0;
            //if (canKite || canDodge || canFleeStrongbox || _kiteCoroutine != null || _dodgeCoroutine != null || _fleeStrongboxCoroutine != null)
            //{
            //    using (LokiPoe.AcquireFrame())
            //    {
            //        _cachedMonsters = GetCachedMonsters(100);
            //        fearLevel = DodgeHelper.CalculateMonsterFear(LokiPoe.Me.Position, _cachedMonsters);
            //    }
            //}

            //if (canKite)
            //{
            //    var shouldKite = DodgeHelper.ShouldKite(fearLevel);
            //    if (shouldKite)
            //    {
            //        _kiteCoroutine = new Coroutine(() => KiteMonsters());

            //        _kitingCooldownTimer.Restart();
            //        return true; // Indicate we should restart task iteration
            //    }
            //}
            
            //if (canDodge)
            //{
            //    var shouldDodge = DodgeHelper.ShouldDodge(_cachedMonsters);
            //    if (shouldDodge)
            //    {
            //        _dodgeCoroutine = new Coroutine(() => DodgeMonsters());

            //        _dodgeCooldownTimer.Restart();
            //        return true; // Indicate we should restart task iteration
            //    }
            //}

            //if (canFleeStrongbox)
            //{
            //    // If I'm -not- kiting but using strongbox handling, I've got some more work to do.
            //    var hasFleeStrongboxCooldownTimerExpired = _fleeStrongboxCooldownTimer.ElapsedMilliseconds >= 20000;
            //    if (hasFleeStrongboxCooldownTimerExpired)
            //        _fleeStrongboxCooldownTimer.Restart();

            //    var shouldFleeStrongbox = hasFleeStrongboxCooldownTimerExpired && fearLevel > 5 && 
            //        LokiPoe.ObjectManager.Objects.OfType<Chest>()
            //            .Any(s => s.IsLocked && s.IsStrongBox && s.Position.Distance(LokiPoe.Me.Position) < 20);

            //    if (shouldFleeStrongbox)
            //    {
            //        _dodgeCoroutine = new Coroutine(() => FleeStrongbox());

            //        return true; // Indicate we should restart task iteration
            //    }
            //}

            //// Reset Broadcasted Flee signal if sent
            //if (_fleeEventBroadcasted)
            //{
            //    Loki.Bot.Utility.BroadcastEventExecute("alcor75playermover_player_flee", false);
            //    _fleeEventBroadcasted = false;
            //}
           
            return false;
        }

        /// <summary>
        /// Multi-frame mini-coroutine for fleeing a strongbox (using the given set of monsters).
        /// NOTE: I'm not sure this ever worked properly in Alcor75's player mover - it remains untested in this one.
        /// </summary>
        /// <returns></returns>
        private async Task FleeStrongbox()
        {
            Loki.Bot.Utility.BroadcastEventExecute("alcor75playermover_player_flee", true);
            _fleeEventBroadcasted = true;

            var bestKitingPosition = DodgeHelper.GetBestKitingPosition(LokiPoe.Me.Position, _cachedMonsters,
                SkillAwarePlayerMoverSettings.Instance.KitineRangeValue, Rarity.Normal, 15, _blacklistedPoints);
            if (bestKitingPosition == LokiPoe.MyPosition)
            {
                Log.Debug($"[{Name}:FleeStrongbox] Could not find suitable strongbox fleeing position; not moving.");
                return;
            }

            bool wereStrongboxMonstersKited;
            do
            {
                wereStrongboxMonstersKited = await DodgeHelper.MoveToLocation(bestKitingPosition);
                if (wereStrongboxMonstersKited)
                {
                    Log.Debug($"[{Name}:FleeStrongbox] Movement was successful.");
                }
                else
                {
                    Log.Error($"[{Name}:FleeStrongbox] Movement was unsuccessful. There were {_blacklistedPoints.Count} blacklist entries.");
                    await Coroutine.Yield();
                }
            } while (!wereStrongboxMonstersKited);

            _blacklistedPoints.Clear();
        }

        /// <summary>
        /// Multi-frame mini-coroutine for dodging the given set of monsters.
        /// </summary>
        /// <returns></returns>
        private async Task DodgeMonsters()
        {
            _dodgeCooldownTimer.Restart();
            Loki.Bot.Utility.BroadcastEventExecute("alcor75playermover_player_flee", true);
            _fleeEventBroadcasted = true;

            var bestDodgePosition = DodgeHelper.GetBestDodgePosition(30, _cachedMonsters, _blacklistedPoints);
            if (bestDodgePosition == LokiPoe.MyPosition)
            {
                Log.Debug($"[{Name}:DodgeMonsters] Could not find suitable dodge position; not moving.");
                return;
            }

            // TODO: coroutine-ify this; build in a loop to ensure I've moved
            bool wasMovementSuccessful;
            do
            {
                wasMovementSuccessful = await DodgeHelper.MoveToLocation(bestDodgePosition);
                if (wasMovementSuccessful)
                {
                    Log.Debug($"[{Name}:DodgeMonsters] Movement was successful.");
                }
                else
                {
                    Log.Error($"[{Name}:DodgeMonsters] Movement was unsuccessful. There were {_blacklistedPoints.Count} blacklist entries.");
                    await Coroutine.Yield();
                }
            }
            while (!wasMovementSuccessful);

            _blacklistedPoints.Clear();
        }

        /// <summary>
        /// Multi-frame mini-coroutine for kiting the given set of monsters.
        /// </summary>
        /// <returns></returns>
        private async Task KiteMonsters()
        {
            _kitingCooldownTimer.Restart();
            Loki.Bot.Utility.BroadcastEventExecute("alcor75playermover_player_flee", true);
            _fleeEventBroadcasted = true;

            var bestKitingPosition = DodgeHelper.GetBestKitingPosition(LokiPoe.Me.Position, _cachedMonsters,
                SkillAwarePlayerMoverSettings.Instance.KitineRangeValue, Rarity.Normal, 15, _blacklistedPoints);
            if (bestKitingPosition == LokiPoe.MyPosition)
            {
                Log.Debug($"[{Name}:KiteMonsters] Could not find suitable kiting position; not moving.");
                return;
            }

            bool wasMovementSuccessful;
            do
            {
                // Finish the current action before attempting to use another one...
                if (LokiPoe.Me.HasCurrentAction && LokiPoe.Me.CurrentAction.Skill.Name != "Move")
                {
                    LokiPoe.ProcessHookManager.ClearAllKeyStates();
                    await Coroutines.LatencyWait();
                }

                // TODO: coroutine-ify this; build in a loop to ensure I've moved
                wasMovementSuccessful = await DodgeHelper.MoveToLocation(bestKitingPosition);
                if (wasMovementSuccessful)
                {
                    Log.Debug($"[{Name}:KiteMonsters] Movement was successful.");
                }
                else
                {
                    Log.Error($"[{Name}:KiteMonsters] Movement was unsuccessful. There are {_blacklistedPoints.Count} blacklist entries.");
                    await Coroutine.Yield();
                }

                _blacklistedPoints.Clear();
            } while (!wasMovementSuccessful);
        }

        /// <summary>
        /// Update color-based cooldown indicators in the Settings GUI.
        /// </summary>
        private void UpdateSettingsGui()
        {
            if (_kitingCooldownTimer.IsRunning)
            {
                if (_kitingCooldownTimer.ElapsedMilliseconds >= SkillAwarePlayerMoverSettings.Instance.FrequencyValue)
                {
                    _kitingCooldownTimer.Stop();
                    SkillAwarePlayerMoverSettings.Instance.IsKitineInCoolDownValue = Brushes.Green;
                }
                else
                {
                    SkillAwarePlayerMoverSettings.Instance.IsKitineInCoolDownValue = Brushes.Red;
                }
            }
            else
            {
                SkillAwarePlayerMoverSettings.Instance.IsKitineInCoolDownValue = Brushes.Green;
            }

            if (_dodgeCooldownTimer.IsRunning)
            {
                if (_dodgeCooldownTimer.ElapsedMilliseconds >= SkillAwarePlayerMoverSettings.Instance.EscapeCooldownValue)
                {
                    _dodgeCooldownTimer.Stop();
                    SkillAwarePlayerMoverSettings.Instance.IsDodgeInCoolDownValue = Brushes.Green;
                }
                else
                {
                    SkillAwarePlayerMoverSettings.Instance.IsDodgeInCoolDownValue = Brushes.Red;
                }
            }
            else
            {
                SkillAwarePlayerMoverSettings.Instance.IsDodgeInCoolDownValue = Brushes.Green;
            }
        }

        /// <summary>
        /// Non-coroutine logic to execute.
        /// </summary>
        /// <param name="name">The name of the logic to invoke.</param>
        /// <param name="param">The data passed to the logic.</param>
        /// <returns>Data from the executed logic.</returns>
        public object Execute(string name, params object[] param)
        {
            if (name == "kitineExecuted")
            {
                _kitingCooldownTimer.Restart();
                _dodgeCooldownTimer.Restart();
            }

            if (name == "dodgeExecuted")
            {
                _dodgeCooldownTimer.Restart();
            }

            if (name == "BlacklistPos")
            {
                if (param[0] == null) return null;

                var positionVector = (Vector2i) param[0];

                if (_blacklistedPoints == null)
                    _blacklistedPoints = new List<Vector2i>();

                _blacklistedPoints.Add(positionVector);
            }

            return null;
        }

        /// <summary>The bot Start event.</summary>
        public void Start()
        {
            PlayerMoverPlugin.Log.InfoFormat($"[{Name}] Task Start.");
        }

        /// <summary>The bot Tick event.</summary>
        public void Tick()
        {
            UpdateSettingsGui();

            if (!LokiPoe.IsInGame || LokiPoe.Me.IsDead || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout)
            {
                _blacklistedPoints.Clear();
                return;
            }

            // TODO: This shouldn't be fetched until needed. It *will* have an FPS impact.
            double fearLevel;
            using (LokiPoe.AcquireFrame())
            {
                _cachedMonsters = GetCachedMonsters(100);
                fearLevel = DodgeHelper.CalculateMonsterFear(LokiPoe.Me.Position, _cachedMonsters);
            }

            // Dodge > flee strongbox > kite
            if (_dodgeCoroutine != null && !_dodgeCoroutine.IsFinished)
            {
                // I've copied over existing exception handling for reference, but I'm not sure why it's necessary
                try
                {
                    // TODO: What if the coroutine is already running?
                    _dodgeCoroutine.Resume();
                }
                catch
                {
                    _dodgeCoroutine.Dispose();
                    _dodgeCoroutine = null;
                    throw;
                }

                return; // Don't try to run multiple coroutines in a tick
            }

            if (_fleeStrongboxCoroutine != null && !_fleeStrongboxCoroutine.IsFinished)
            {
                _fleeStrongboxCoroutine.Resume();
                return; // Don't try to run multiple coroutines in a single tick
            }

            if (_kiteCoroutine != null && !_kiteCoroutine.IsFinished)
            {
                _kiteCoroutine.Resume();
                return; // Don't try to run multiple coroutines in a single tick
            }

            // If I need to dodge, kite, or flee a strongbox, build its coroutine and kick it off
            var canDodge = SkillAwarePlayerMoverSettings.Instance.DodgeLogicValue && _dodgeCooldownTimer.ElapsedMilliseconds >= SkillAwarePlayerMoverSettings.Instance.EscapeCooldownValue;
            if (canDodge)
            {
                var shouldDodge = DodgeHelper.ShouldDodge(_cachedMonsters);
                if (shouldDodge)
                {
                    _dodgeCoroutine = new Coroutine(() => DodgeMonsters());
                    _dodgeCoroutine.Resume();

                    _dodgeCooldownTimer.Restart();
                }
            }

            var canKite = SkillAwarePlayerMoverSettings.Instance.KitineLogicValue && _kitingCooldownTimer.ElapsedMilliseconds >= SkillAwarePlayerMoverSettings.Instance.FrequencyValue;
            if (canKite)
            {
                var shouldKite = DodgeHelper.ShouldKite(fearLevel);
                if (shouldKite)
                {
                    _kiteCoroutine = new Coroutine(() => KiteMonsters());
                    _kiteCoroutine.Resume();

                    _kitingCooldownTimer.Restart();
                    return;
                }
            }

            var canFleeStrongbox = !SkillAwarePlayerMoverSettings.Instance.KitineLogicValue && SkillAwarePlayerMoverSettings.Instance.StrongBoxLogicValue;
            if (canFleeStrongbox)
            {
                // If I'm -not- kiting but using strongbox handling, I've got some more work to do.
                var hasFleeStrongboxCooldownTimerExpired = _fleeStrongboxCooldownTimer.ElapsedMilliseconds >= 20000;
                if (hasFleeStrongboxCooldownTimerExpired)
                    _fleeStrongboxCooldownTimer.Restart();

                var shouldFleeStrongbox = hasFleeStrongboxCooldownTimerExpired && fearLevel > 5 &&
                    LokiPoe.ObjectManager.Objects.OfType<Chest>()
                        .Any(s => s.IsLocked && s.IsStrongBox && s.Position.Distance(LokiPoe.Me.Position) < 20);

                if (shouldFleeStrongbox)
                {
                    _fleeStrongboxCoroutine = new Coroutine(() => FleeStrongbox());
                    _fleeStrongboxCoroutine.Resume();
                }
            }
        }

        /// <summary>
        /// Get the cached representation of all living monsters within the given <see cref="searchRadius">distance</see> of the current player.
        /// </summary>
        /// <param name="searchRadius"></param>
        /// <returns></returns>
        private static IEnumerable<CachedMonster> GetCachedMonsters(int searchRadius)
        {
            var monsters = LokiPoe.ObjectManager.GetObjectsByType<Monster>()
                .Where(x => !x.IsDead && x.IsActive && x.Position.Distance(LokiPoe.Me.Position) < searchRadius)
                .Select(x => new CachedMonster(x))
                .ToList();
            return monsters;
        }

        /// <summary>The bot Stop event.</summary>
        public void Stop()
        {
            Log.Info($"[{Name}:Stop]");

            // Clean up any existing coroutines
            _kiteCoroutine = null;
            _dodgeCoroutine = null;
            _fleeStrongboxCoroutine = null;
        }
    }
}
