using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;
using SkillAwarePlayerMover.Class;
using SkillAwarePlayerMover.Helpers;
using Utility = SkillAwarePlayerMover.Helpers.Utility;

namespace SkillAwarePlayerMover.Tasks
{
    // TODO: Examine this; it seems to either not be running sufficiently far from a strongbox, or not reacting fast enough after attempting to open a strongobx.
    public class StrongBoxTask : ITask
    {
        private Stopwatch sw = new Stopwatch();

        private int _tryNr = 0;

        private bool _fleeEventBroadcasted = false;

        /// <summary>The name of this task.</summary>
        public string Name
        {
            get { return "StrongBoxTask"; }
        }

        /// <summary>A description of what this task does.</summary>
        public string Description
        {
            get { return "This task Open Strong Box and run away from the hell."; }
        }

        /// <summary>The author of this task.</summary>
        public string Author
        {
            get { return "DBF mod by Alcor75"; }
        }

        /// <summary>The version of this task.</summary>
        public string Version
        {
            get { return "0.0.1.2"; }
        }

        /// <summary>
        /// Coroutine logic to execute.
        /// </summary>
        /// <param name="type">The requested type of logic to execute.</param>
        /// <param name="param">Data sent to the object from the caller.</param>
        /// <returns>true if logic was executed to handle this type and false otherwise.</returns>
        public async Task<bool> Logic(string type, params object[] param)
        {
            if (type != "task_execute")
                return false;
            if (!LokiPoe.IsInGame || LokiPoe.Me.IsDead || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout)
            {
                RemoveMe();
                return false;
            }                        
            
            // Check for any active shrines.
            var strongboxes =
                LokiPoe.ObjectManager.Objects.OfType<Chest>()
                    .Where(s =>
                    {
                        NetworkObject start = LokiPoe.Me;
                        return !s.IsOpened && 
                                           !s.IsLocked && 
                                           s.IsStrongBox &&
                                           ExilePather.CanObjectSee(LokiPoe.Me, s, true) &&
                                           !Utility.IsAClosedDoorBetween(start.Position, s.Position, 10, 10, true) && 
                                           ExilePather.PathDistance(LokiPoe.Me.Position, s.Position, true, true) < 50;
                    })
                    .OrderBy(s => s.Distance)
                    .ToList();

            if (!strongboxes.Any())
            {
                RemoveMe();
                return false;         
            }

            var strongbox = strongboxes[0];

            // Blacklist the strongbox and remove the task if numbers of try exede the limit.
            if (_tryNr > 5)
            {
                Blacklist.Add(strongbox.Id, new TimeSpan(1, 0, 0), "Failed to navigate to Strongbox for more than 5 times.");
                PlayerMoverPlugin.Log.DebugFormat("[HandleStrongBoxes] Failed to navigate to {0} for more than 5 times, blacklisting and continue.", strongbox.Name);
                RemoveMe();
                return false;
            }

            var moveSkillSlot = PlayerMoverPlugin.GetMovementSkillSlot();
            if (moveSkillSlot != -1)
            {
                
                var firstOrDefault = LokiPoe.Me.AvailableSkills.FirstOrDefault(s => s.Slot == moveSkillSlot);
                if (firstOrDefault != null && firstOrDefault.IsOnCooldown)
                {
                    PlayerMoverPlugin.Log.DebugFormat("[HandleStrongBoxes] Waiting for skill {0} cooldown.", firstOrDefault.Name);
                    RemoveMe();
                    return false;
                }                
            }

            // Try and only move to touch it when we have a somewhat navigable path.
            NetworkObject start1 = LokiPoe.Me;
            if ((Utility.NumberOfMobsBetween(LokiPoe.Me, strongbox, 20, true) < 1 &&
                 Utility.NumberOfMobsNear(LokiPoe.Me, 20) < 1) &&
                !Utility.IsAClosedDoorBetween(start1.Position, strongbox.Position, 10, 10, true) )//|| skellyOverride)
            {
                PlayerMoverPlugin.Log.DebugFormat("[HandleStrongBoxes] Now moving towards the Strongbox {0}.", strongbox.Id);

                var myPos = LokiPoe.Me.Position;

                var pos = ExilePather.FastWalkablePositionFor(strongbox, 20 ,true);
                
                var pathDistance = ExilePather.PathDistance(myPos, pos, true, true);
                
                var inDistance = myPos.Distance(pos) < 22;
                
                PlayerMoverPlugin.Log.DebugFormat("[HandleStrongBoxes] inDistance: {0}", inDistance);

                if (inDistance)
                {
                    if (!sw.IsRunning)
                        sw.Start();

                    if (sw.ElapsedMilliseconds > 15000)
                    {
                        var timeElapsed = Math.Abs(sw.ElapsedMilliseconds/1000);
                        Blacklist.Add(strongbox.Id, new TimeSpan(1, 0, 0), "Failed to open strongbox for more than " + timeElapsed + " seconds.");
                        PlayerMoverPlugin.Log.DebugFormat("[HandleStrongBoxes] Failed to open strongbox for more than {0} seconds, return false.", timeElapsed);
                        RemoveMe();
                        return false; 
                    }

                    PlayerMoverPlugin.Log.DebugFormat("[HandleStrongBoxes] Now attempting to interact with the Strongbox {0}.", strongbox.Id);
                    
                    LokiPoe.ProcessHookManager.ClearAllKeyStates();

                    await Coroutines.InteractWith(strongbox);

                    if (!await Coroutines.InteractWith(strongbox)) return true;

                    var _cachedMonsters = CacheMonsters(LokiPoe.ObjectManager.GetObjectsByType<Monster>().Where(x => !x.IsDead && x.IsActive && x.Position.Distance(LokiPoe.Me.Position) < 100).ToList());

                    var safePosition = DodgeHelper.GetBestKitingPosition(LokiPoe.Me.Position, _cachedMonsters, 70, Rarity.Normal);

                    if (safePosition == LokiPoe.Me.Position) return false;

                    Loki.Bot.Utility.BroadcastEventExecute("alcor75playermover_player_flee", true);
                    _fleeEventBroadcasted = true;

                    if (await DodgeHelper.MoveToLocation(safePosition))
                        //if (!await Coroutines.MoveToLocation(safePosition, 5, 1700))
                    {
                        PlayerMoverPlugin.Log.ErrorFormat("[HandleStrongBoxes] Error Kiting Towards safe Position. [{0} from strongbox]", safePosition.Distance(strongbox.Position));
                    }
                    PlayerMoverPlugin.Log.InfoFormat("[HandleStrongBoxes] Kiting towards safe Position. [{0} from strongbox]", safePosition.Distance(strongbox.Position));
                    return false;
                }
                else
                {
                    Loki.Bot.Utility.BroadcastEventExecute("alcor75playermover_player_flee", true);
                    _fleeEventBroadcasted = true;

                    if (!PlayerMover.MoveTowards(pos))
                    {
                        _tryNr ++;
                        PlayerMoverPlugin.Log.ErrorFormat("[HandleStrongBoxes] MoveTowards failed for {0}.", pos);
                        return false;
                    }
                    PlayerMoverPlugin.Log.DebugFormat("[HandleStrongBoxes] Moving towards {0}. [pathDistance: {1}]", pos, pathDistance);
                }
                return true;
            }

            RemoveMe();
            return false; 
        }

        private List<CachedMonster> CacheMonsters(List<Monster> list)
        {
            return (from monster in list where monster != null select new CachedMonster(monster)).ToList();
        }

        /// <summary>
        /// Non-coroutine logic to execute.
        /// </summary>
        /// <param name="name">The name of the logic to invoke.</param>
        /// <param name="param">The data passed to the logic.</param>
        /// <returns>Data from the executed logic.</returns>
        public object Execute(string name, params object[] param)
        {
            return null;
        }

        /// <summary>The bot Start event.</summary>
        public void Start()
        {
            PlayerMoverPlugin.Log.InfoFormat("[StrongBoxTask] Task Loaded.");
        }

        /// <summary>The bot Tick event.</summary>
        public void Tick()
        {
        }

        /// <summary>The bot Stop event.</summary>
        public void Stop()
        {
        }

        private void RemoveMe()
        {
            if (_fleeEventBroadcasted)
            {
                Loki.Bot.Utility.BroadcastEventExecute("alcor75playermover_player_flee", false);
                _fleeEventBroadcasted = false;
            }

            var task = PlayerMoverPlugin.TaskManager.GetTaskByName("StrongBoxTask");
            if (task != null)
            {
                if (!PlayerMoverPlugin.TaskManager.Remove("StrongBoxTask"))
                {
                    PlayerMoverPlugin.Log.ErrorFormat("[StrongBoxTask] Remove StrongBoxTask failed.");
                }
                else
                {
                    PlayerMoverPlugin.Log.InfoFormat("[StrongBoxTask] Remove StrongBoxTask success.");
                }
            }
        }

    }
}
