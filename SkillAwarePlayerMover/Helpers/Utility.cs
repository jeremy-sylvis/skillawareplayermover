using System;
using System.Linq;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.Objects;

namespace SkillAwarePlayerMover.Helpers
{
    /// <summary>
	/// Shared utility functions.
	/// </summary>
	public static class Utility
	{
	    /// <summary>
		/// Checks for a closed door between start and end.
		/// </summary>
		/// <param name="startVector"></param>
		/// <param name="endVector"></param>
		/// <param name="distanceFromPoint">How far to check around each point for a door object.</param>
		/// <param name="stride">The distance between points to check in the path.</param>
		/// <param name="dontLeaveFrame">Should the current frame not be left?</param>
		/// <returns>true if there's a closed door and false otherwise.</returns>
		public static bool IsAClosedDoorBetween(Vector2i startVector, Vector2i endVector, int distanceFromPoint = 10, int stride = 10, bool dontLeaveFrame = false)
		{
		    var unopenedDoorPositions = LokiPoe.ObjectManager.Doors
		        .Where(d => !d.IsOpened)
		        .Select(d => d.Position);

			if (!unopenedDoorPositions.Any())
				return false;

			var path = ExilePather.GetPointsOnSegment(startVector, endVector, dontLeaveFrame);

			for (var i = 0; i < path.Count; i += stride)
			{
			    var pathVector = path[i];

			    var isAnyDoorPositionWithinDistanceOfPathVector = unopenedDoorPositions.Any(doorPosition => doorPosition.Distance(pathVector) <= distanceFromPoint);
			    if (isAnyDoorPositionWithinDistanceOfPathVector)
			    {
			        return true;
			    }
			}

		    return false;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="distanceFromPoint"></param>
		/// <param name="dontLeaveFrame">Should the current frame not be left?</param>
		/// <returns></returns>
		public static int NumberOfMobsBetween(NetworkObject start, NetworkObject end, int distanceFromPoint = 5,
			bool dontLeaveFrame = false)
		{
			var mobs = LokiPoe.ObjectManager.GetObjectsByType<Monster>().Where(d => d.IsActive).ToList();
			if (!mobs.Any())
				return 0;

			var path = ExilePather.GetPointsOnSegment(start.Position, end.Position, dontLeaveFrame);

			var count = 0;
			for (var i = 0; i < path.Count; i += 10)
			{
				foreach (var mob in mobs)
				{
					if (mob.Position.Distance(path[i]) <= distanceFromPoint)
					{
						++count;
					}
				}
			}

			return count;
		}

		/// <summary>
		/// Returns the number of mobs near a target.
		/// </summary>
		/// <param name="target"></param>
		/// <param name="distance"></param>
		/// <param name="dead"></param>
		/// <returns></returns>
		public static int NumberOfMobsNear(NetworkObject target, float distance, bool dead = false)
		{
			var mpos = target.Position;

			var curCount = 0;

			foreach (var mob in LokiPoe.ObjectManager.Objects.OfType<Monster>())
			{
				if (mob.Id == target.Id)
				{
					continue;
				}

				// If we're only checking for dead mobs... then... yeah...
				if (dead)
				{
					if (!mob.IsDead)
					{
						continue;
					}
				}
				else if (!mob.IsActive)
				{
					continue;
				}

				if (mob.Position.Distance(mpos) < distance)
				{
					curCount++;
				}
			}

			return curCount;
		}

		/// <summary>
		/// Returns a location where a portal should be if we're in a town.
		/// </summary>
		/// <returns>A location where a portal should come into view.</returns>
		public static Vector2i GuessPortalLocation()
		{
			var curArea = LokiPoe.LocalData.WorldArea.Id.ToLowerInvariant();

			if (curArea.EndsWith("1_town"))
			{
				return new Vector2i(151, 246);
			}

			if (curArea.EndsWith("2_town"))
			{
				return new Vector2i(246, 165);
			}

			if (curArea.EndsWith("3_town"))
			{
				return new Vector2i(217, 226);
			}

			if (curArea.EndsWith("4_town"))
			{
				return new Vector2i(286, 491);
			}

			throw new Exception(String.Format((string) "GuessPortalLocation called when curArea = {0}", (object) curArea));
		}

		/// <summary>
		/// Returns a location where the stash should be if we're in a town.
		/// </summary>
		/// <returns>A location where the stash should come into view.</returns>
		public static Vector2i GuessStashLocation()
		{
			var curArea = LokiPoe.LocalData.WorldArea.Id.ToLowerInvariant();

			if (curArea.EndsWith("1_town"))
			{
				return new Vector2i(246, 266);
			}

			if (curArea.EndsWith("2_town"))
			{
				return new Vector2i(178, 195);
			}

			if (curArea.EndsWith("3_town"))
			{
				return new Vector2i(206, 306);
			}

			if (curArea.EndsWith("4_town"))
			{
				return new Vector2i(199, 509);
			}

			throw new Exception(string.Format((string) "GuessStashLocation called when curArea = {0}", (object) curArea));
		}

		/// <summary>
		/// Returns a location where the waypoint should be if we're in a town.
		/// </summary>
		/// <returns>A location where the waypoint should come into view.</returns>
		public static Vector2i GuessWaypointLocation()
		{
			var curArea = LokiPoe.LocalData.WorldArea.Id.ToLowerInvariant();

			if (curArea.EndsWith("1_town"))
			{
				return new Vector2i(196, 172);
			}

			if (curArea.EndsWith("2_town"))
			{
				return new Vector2i(188, 135);
			}

			if (curArea.EndsWith("3_town"))
			{
				return new Vector2i(217, 226);
			}

			if (curArea.EndsWith("4_town"))
			{
				return new Vector2i(286, 491);
			}

			throw new Exception(String.Format((string) "GuessWaypointLocation called when curArea = {0}", (object) curArea));
		}

		/// <summary>
		/// Returns a location where the area transition should be if we're in a town.
		/// </summary>
		/// <returns>A location where the waypoint should come into view.</returns>
		public static Vector2i GuessAreaTransitionLocation(string townExitName)
		{
			var curArea = LokiPoe.LocalData.WorldArea.Id.ToLowerInvariant();

			if (curArea.EndsWith("1_town"))
			{
				if (townExitName == "The Coast")
				{
					return new Vector2i(319, 212);
				}
			}

			if (curArea.EndsWith("2_town"))
			{
				if (townExitName == "The Old Fields")
				{
					return new Vector2i(299, 173);
				}

				if (townExitName == "The Riverways")
				{
					return new Vector2i(78, 172);
				}

				if (townExitName == "The Southern Forest")
				{
					return new Vector2i(186, 89);
				}
			}

			if (curArea.EndsWith("3_town"))
			{
				if (townExitName == "The Slums")
				{
					return new Vector2i(511, 408);
				}

				if (townExitName == "The City of Sarn")
				{
					return new Vector2i(296, 75);
				}
			}

			if (curArea.EndsWith("4_town"))
			{
				if (townExitName == "The Aqueduct")
				{
					return new Vector2i(259, 394);
				}

				if (townExitName == "The Dried Lake")
				{
					return new Vector2i(94, 441);
				}

				if (townExitName == "The Mines Level 1")
				{
					return new Vector2i(328, 624);
				}
			}

			throw new Exception(String.Format(
				"GuessAreaTransitionLocation called when curArea = {0} for exit {1}", curArea, townExitName));
		}

		/// <summary>
		/// Returns hardcoded locations for npcs in a town. We need to make sure these don't change while we aren't looking!
		/// Ideally, we'd explore town to find the location if the npc object was not in view.
		/// </summary>
		public static Tuple<string, Vector2i> GuessWeaponsNpcLocation()
		{
			var curArea = LokiPoe.LocalData.WorldArea.Id.ToLowerInvariant();

			var npcName = "";

			if (curArea.EndsWith("1_town"))
			{
				npcName = "Tarkleigh";
			}

			if (curArea.EndsWith("2_town"))
			{
				npcName = "Greust";
			}

			if (curArea.EndsWith("3_town"))
			{
				npcName = "Hargan";
			}

			if (curArea.EndsWith("4_town"))
			{
				npcName = "Kira";
			}

			if (npcName != "")
			{
				return new Tuple<string, Vector2i>(npcName, GuessNpcLocation(npcName));
			}

			throw new Exception(String.Format((string) "GuessWeaponsNpcLocation called when curArea = {0}.", (object) curArea));
		}

		/// <summary>
		/// Returns hardcoded locations for npcs in a town. We need to make sure these don't change while we aren't looking!
		/// Ideally, we'd explore town to find the location if the npc object was not in view.
		/// </summary>
		public static Vector2i GuessNpcLocation(string npcName)
		{
			var curArea = LokiPoe.LocalData.WorldArea.Id.ToLowerInvariant();

			if (curArea.EndsWith("1_town"))
			{
				if (npcName == "Nessa")
					return new Vector2i(268, 253);

				if (npcName == "Tarkleigh")
					return new Vector2i(312, 189);
			}

			if (curArea.EndsWith("2_town"))
			{
				if (npcName == "Greust")
					return new Vector2i(192, 173);

				if (npcName == "Yeena")
					return new Vector2i(162, 240);
			}

			if (curArea.EndsWith("3_town"))
			{
				if (npcName == "Clarissa")
					return new Vector2i(147, 326);

				if (npcName == "Hargan")
					return new Vector2i(281, 357);
			}

			if (curArea.EndsWith("4_town"))
			{
				if (npcName == "Kira")
					return new Vector2i(169, 500);

				if (npcName == "Petarus and Vanja")
					return new Vector2i(204, 546);
			}

			return Vector2i.Zero;
		}

		/// <summary>
		/// Returns hardcoded locations for npcs in a town. We need to make sure these don't change while we aren't looking!
		/// Ideally, we'd explore town to find the location if the npc object was not in view.
		/// </summary>
		public static Tuple<string, Vector2i> GuessAccessoryNpcLocation()
		{
			var curArea = LokiPoe.LocalData.WorldArea.Id.ToLowerInvariant();

			var npcName = "";

			if (curArea.EndsWith("1_town"))
			{
				npcName = "Nessa";
			}

			if (curArea.EndsWith("2_town"))
			{
				npcName = "Yeena";
			}

			if (curArea.EndsWith("3_town"))
			{
				npcName = "Clarissa";
			}

			if (curArea.EndsWith("4_town"))
			{
				npcName = "Petarus and Vanja";
			}

			if (npcName != "")
			{
				return new Tuple<string, Vector2i>(npcName, GuessNpcLocation(npcName));
			}

			throw new Exception(String.Format((string) "GuessAccessoryNpcLocation called when curArea = {0}.", (object) curArea));
		}
	}
}