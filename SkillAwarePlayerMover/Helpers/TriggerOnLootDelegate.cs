using Loki.Game.Objects;

namespace SkillAwarePlayerMover.Helpers
{
    /// <summary>
    /// A delegate for triggering an item being looted.
    /// </summary>
    /// <param name="item"></param>
    public delegate void TriggerOnLootDelegate(WorldItem item);
}