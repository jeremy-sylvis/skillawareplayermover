using System;
using System.Linq;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace SkillAwarePlayerMover.Helpers
{
    public static class MiscHelpers
    {
        public static T GenericTaskExecute<T>(string taskName, string method, dynamic[] param)
        {
            var task = PlayerMoverPlugin.TaskManager.GetTaskByName(taskName);
            if (task == null) return (T) Convert.ChangeType(default(T), typeof (T));
            var value = task.Execute(method, param);
            return (T)Convert.ChangeType(value, typeof(T));            
        }

        /// <summary>
        /// Curtesy of pushedx.
        /// Checks for a specific closed door between start and end.
        /// </summary>
        /// <param name="door"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="distanceFromPoint">How far to check around each point for a door object.</param>
        /// <param name="stride">The distance between points to check in the path.</param>
        /// <param name="dontLeaveFrame">Should the current frame not be left?</param>
        /// <returns>true if there's a closed door and false otherwise.</returns>
        public static bool ClosedDoorBetween(TriggerableBlockage door, Vector2i start, Vector2i end, int distanceFromPoint = 10, int stride = 10,
            bool dontLeaveFrame = false)
        {

            var path = ExilePather.GetPointsOnSegment(start, end, dontLeaveFrame);

            for (var i = 0; i < path.Count; i += stride)
            {

                    if (door.Position.Distance(path[i]) <= distanceFromPoint)
                    {
                        return true;
                    }
                
            }

            return false;
        }
        public static bool HasMinionFollower
        {
            get
            {
                return LokiPoe.ObjectManager.GetObjectsByType<Monster>().Any(x => x.Type.Contains("Mission") &&
                 x.Distance < 100 &&
                                x.IsFriendly &&
                                x.Health > 0 &&
                                x.Rarity == Rarity.Unique);
            }
        }
    }
}
