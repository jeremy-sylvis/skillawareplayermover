using Loki.Common.MVVM;

namespace SkillAwarePlayerMover.Helpers
{
    /// <summary>
    /// A wrapper class for settings bindings.
    /// </summary>
    public class SettingNameEnabledWrapper : NotificationObject
    {
        private bool _isEnabled;
        private string _name;

        /// <summary>
        /// Is this setting enabled.
        /// </summary>
        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                if (value.Equals(_isEnabled))
                {
                    return;
                }
                _isEnabled = value;
                NotifyPropertyChanged(() => IsEnabled);
            }
        }

        /// <summary>
        /// The name of this setting.
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value == _name)
                {
                    return;
                }
                _name = value;
                NotifyPropertyChanged(() => Name);
            }
        }
    }
}