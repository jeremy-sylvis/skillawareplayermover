using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.Objects;

namespace SkillAwarePlayerMover.Helpers
{
    public static class MoveHelper
    {
        private const string Name = nameof(MoveHelper);
        private static readonly ILog Log = Logger.GetLoggerInstanceForType();

        public static bool IsPathToVectorSafe(Vector2i destinationVector)
        {
            return NumberOfMobsNearLocation(destinationVector, GuessActualCombatRange()) <= 0;
        }

        private static int NumberOfMobsNearLocation(Vector2i location, int distance)
        {
            return LokiPoe.ObjectManager.GetObjectsByType<Monster>()
                .Count(x => x.IsHostile && x.IsActive && !x.IsDead && x.Position.Distance(location) < distance);
        }

        public static int GuessActualCombatRange()
        {
            var currentRoutineHelper = new RoutineHelpers();
            var combatRange = 15;

            var skillSlotNumber = currentRoutineHelper.GetSingleTargetRangedSlot();
            if (skillSlotNumber == -1)
            {
                skillSlotNumber = currentRoutineHelper.GetAoeRangedSlot();
                if (skillSlotNumber == -1)
                {
                    skillSlotNumber = currentRoutineHelper.GetSingleTargetMeleeSlot();
                    if (skillSlotNumber == -1)
                    {
                        combatRange = currentRoutineHelper.GetMaxMeleeRange();
                    }
                }
                else
                {
                    combatRange = currentRoutineHelper.GetMaxRangedRange();
                }
            }
            else
            {
                combatRange = currentRoutineHelper.GetMaxRangedRange();
            }
            return combatRange == 9999 ? 15 : combatRange;
        }

        public static Vector2i CalcSafePosition(Vector2i center)
        {
            var result = LokiPoe.Me.Position;
            for (int i = (30); i <= (330); i = i + 15)
            {
                for (int y = 75; y > 20; y = y - 5)
                {
                    var pos = GetPointOnCircle(center.X, center.Y, i, y);
                    if (!ExilePather.CanObjectSee(LokiPoe.Me, pos, true)) continue;
                    if (!ExilePather.IsWalkable(pos)) continue;
                    if (!ExilePather.PathExistsBetween(LokiPoe.Me.Position, pos, true)) continue;
                    NetworkObject start = LokiPoe.Me;
                    if (Utility.IsAClosedDoorBetween(start.Position, pos, 10, 10, true)) continue;
                    result = pos;
                    break;
                }
                
            }
            return result;
        }

        public static Vector2i GetPointOnCircle(int centerX, int centerY, int degree, int radius)
        {
            int xOncircle = centerX + (int)((double)radius * Math.Cos((double)degree * Math.PI / 180));
            int yOncircle = centerY + (int)((double)radius * Math.Sin((double)degree * Math.PI / 180));
            return new Vector2i(xOncircle, yOncircle);
        }

        public static int GetSkillMinRange(string skillName)
        {
            switch (skillName)
            {
                case "Phase Run":
                    return SkillAwarePlayerMoverSettings.Instance.PrMinDistValue;
                    
                case "Flame Dash":
                    return SkillAwarePlayerMoverSettings.Instance.FdMinDistValue;
             
                case "Lightning Warp":
                    return SkillAwarePlayerMoverSettings.Instance.LwMinDistValue;
                   
                case "Blink Arrow":
                    return SkillAwarePlayerMoverSettings.Instance.BaMinDistValue;
                   
                case "Leap Slam":
                    return SkillAwarePlayerMoverSettings.Instance.LsMinDistValue;
                   
                case "Whirling Blades":
                    return SkillAwarePlayerMoverSettings.Instance.WbMinDistValue;

                case "Shield Charge":
                    return SkillAwarePlayerMoverSettings.Instance.ScMinDistValue;

                default:
                    return 1;
            }
        }
        public static int GetSkillMaxRange(string skillName)
        {
            switch (skillName)
            {
                case "Phase Run":
                    return SkillAwarePlayerMoverSettings.Instance.PrMaxDistValue;

                case "Flame Dash":
                    return SkillAwarePlayerMoverSettings.Instance.FdMaxDistValue;

                case "Lightning Warp":
                    return SkillAwarePlayerMoverSettings.Instance.LwMaxDistValue;

                case "Blink Arrow":
                    return SkillAwarePlayerMoverSettings.Instance.BaMaxDistValue;

                case "Leap Slam":
                    return SkillAwarePlayerMoverSettings.Instance.LsMaxDistValue;

                case "Whirling Blades":
                    return SkillAwarePlayerMoverSettings.Instance.WbMaxDistValue;

                case "Shield Charge":
                    return SkillAwarePlayerMoverSettings.Instance.ScMaxDistValue;

                default:
                    return 70;
            }
        }

        /// <summary>
        /// Multi-frame co-routine behavior for using the movement skill at the given slot in an attempt to move toward the given position.
        /// </summary>
        /// <param name="skillSlot"></param>
        /// <param name="skillTargetPoint"></param>
        /// <returns>"false" if we can immediately allow the next task to execute, otherwise "true" if we should restart iteration.</returns>
        public static bool UseMovementSkill(int skillSlot, Vector2i skillTargetPoint)
        {
            if (skillTargetPoint == Vector2i.Zero) return false;

            const string phaseRunSkillName = "Phase Run";
            const string blinkArrowSkillName = "Blink Arrow";
            const string lightningWarpSkillName = "Lightning Warp";

            var cachedDestination = Vector2i.Zero;

            using (LokiPoe.AcquireFrame())
            {
                var skill = LokiPoe.InGameState.SkillBarHud.Slot(skillSlot);
                if (skill.Name == phaseRunSkillName)
                {
                    if (LokiPoe.Me.Auras.All(x => x.Name != phaseRunSkillName))
                    {
                        Log.Debug($"[{Name}:UseMovementSkill] Using skill {skill.Name}.");
                        LokiPoe.InGameState.SkillBarHud.Use(skillSlot, false);
                    }

                    return false;
                }

                Log.Debug($"[{Name}:UseMovementSkill] Using skill {skill.Name} at position {skillTargetPoint}.");
                var useResult = LokiPoe.InGameState.SkillBarHud.UseAt(skillSlot, true, skillTargetPoint);
                if (useResult != LokiPoe.InGameState.UseResult.None)
                {
                    Log.Error($"[{Name}:UseMovementSkill] {skill.Name} UseAt returned {useResult}.");
                    return false;
                }

                if (LokiPoe.Me.HasCurrentAction && (LokiPoe.Me.CurrentAction.Skill.Name == blinkArrowSkillName || LokiPoe.Me.CurrentAction.Skill.Name == lightningWarpSkillName))
                    cachedDestination = LokiPoe.Me.CurrentAction.Destination;
            }

            // Basically, if we're not using Blink Arrow or Lightning Warp, we're done.
            // Otherwise, we need to keep track of our current destination versus our target destination.
            if (cachedDestination == Vector2i.Zero) return true;

            // TODO: This blends cross-routine coroutine behavior with atomic logic/checks. It should be refactored.
            var averageLatencyMilliseconds = LatencyTracker.Average;
            var maximumLatencyMilliseconds = LatencyTracker.Highest;
            var stopwatch = Stopwatch.StartNew();

            // TODO: I can convert this to a Coroutine, and should
            var sleepDurationMilliseconds = averageLatencyMilliseconds / 2;
            var estimatedReactionDelayMilliseconds = ((averageLatencyMilliseconds + maximumLatencyMilliseconds) / 2 * SkillAwarePlayerMoverSettings.Instance.SensibilityValue) + 500;
            while (stopwatch.ElapsedMilliseconds < estimatedReactionDelayMilliseconds && LokiPoe.Me.Position.Distance(cachedDestination) > 6)
            {
                using (LokiPoe.Memory.ReleaseFrame(LokiPoe.UseHardlock))
                {
                    Thread.Sleep(sleepDurationMilliseconds);
                }
            }
            return true;
        }
    }
}
