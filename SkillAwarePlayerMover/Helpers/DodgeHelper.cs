using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;
using SkillAwarePlayerMover.Class;

namespace SkillAwarePlayerMover.Helpers
{
    internal static class DodgeHelper
    {
        private const string Name = nameof(DodgeHelper);
        private static readonly ILog Log = Logger.GetLoggerInstanceForType();

        /// <summary>
        /// Determine if we should kite monsters based on the given fear level.
        /// NOTES: Currently, only called from SurviveTask50.
        /// </summary>
        /// <param name="fear"></param>
        /// <returns></returns>
        public static bool ShouldKite(double fear)
        {
            if (fear == PlayerMoverPlugin.CachedDangerLevel) return false;

            bool shouldKite;
            if (fear < PlayerMoverPlugin.CachedDangerLevel)
            {
                var modifiedFearThreshold = SkillAwarePlayerMoverSettings.Instance.FearLevelValue * 1.3;
                var isFearAboveModifiedThreshold = fear > modifiedFearThreshold;
                shouldKite = isFearAboveModifiedThreshold;
            }
            else
            {
                var isFearAboveBaseThreshold = fear > SkillAwarePlayerMoverSettings.Instance.FearLevelValue;
                shouldKite = isFearAboveBaseThreshold;
            }

            Log.Debug($"[{Name}:ShouldKite] Current fear level [{fear.ToString("N2")}], last fear level [{PlayerMoverPlugin.CachedDangerLevel.ToString("N2")}], should kite [{shouldKite}].");
            PlayerMoverPlugin.CachedDangerLevel = fear;
            return shouldKite;
        }

        /// <summary>
        /// Calculate the approximate danger of a the given position using the given set of monsters.
        /// NOTES: Currently, only called from SurviveTask50.
        /// </summary>
        /// <param name="centerPoint"></param>
        /// <param name="monsters"></param>
        /// <returns></returns>
        public static double CalculateMonsterFear(Vector2i centerPoint, IEnumerable<CachedMonster> monsters)
        {
            if (!monsters.Any()) return 0;

            double nearbyMonstersScore = 0;

            var effectiveHealthPercent = SkillAwarePlayerMoverSettings.Instance.UseESValue ? LokiPoe.Me.EnergyShieldPercent : (double)LokiPoe.Me.HealthPercent;
            var healthScore = effectiveHealthPercent == 0 ? 0d : 100 / LokiPoe.Me.EnergyShieldPercent;

            // What the actual fuck? Modifie it by 135%, itself, 90%.
            // Effectively, square it and multiply by 2.565.
            // Test runs:
            // 10%: 0.01215
            // 50%: 0.30375
            // 75%: 0.68344
            // 90%: 0.98415
            // 100%: 1.21500

            // TODO: This makes 0 fucking sense. Commenting it out because reasons.
            //healthScore = healthScore * 1.35 * (healthScore * 0.9);
            
            foreach (var monster in monsters)
            {
                var distanceToMonster = monster.Position.Distance(centerPoint) + 1;
                var modifier = GetKitingWeightModifierByMonsterRarity(monster);

                nearbyMonstersScore += modifier / distanceToMonster;
            }

            var totalScore = healthScore*nearbyMonstersScore;
            var totalScoreInt = (int)Math.Round(totalScore);

            SkillAwarePlayerMoverSettings.Instance.ActualFearValue = totalScoreInt;
            return totalScoreInt;
        }

        private static double GetKitingWeightModifierByMonsterRarity(CachedMonster monster)
        {
            double modifier;
            switch (monster.Rarity)
            {
                case Rarity.Unique:
                    modifier = 27;
                    break;
                case Rarity.Rare:
                    modifier = 9;
                    break;
                case Rarity.Magic:
                    modifier = 3;
                    break;
                default:
                    modifier = 1; // why are we modifying standard NPCs?
                    break;
            }
            return modifier;
        }

        /// <summary>
        /// Determine if we should dodge any skills, projectiles, or other threats caused by any of the given monsters.
        /// </summary>
        /// <param name="monsters"></param>
        /// <returns>"true" if we need to dodge; otherwise "false".</returns>
        public static bool ShouldDodge(IEnumerable<CachedMonster> monsters)
        {            
            var chests = LokiPoe.ObjectManager.GetObjectsByType<Chest>();
            var chestWithActorComponentWithAction = chests.FirstOrDefault(x => x.Components?.ActorComponent != null && x.Components.ActorComponent.HasCurrentAction);

            var chestCurrentAction = chestWithActorComponentWithAction?.Components.ActorComponent.CurrentAction;
            if (chestCurrentAction?.Skill != null)
            {
                Log.Info($"[{Name}:ShouldDodge] We should dodge skill {chestCurrentAction.Skill.Name} from caster {chestWithActorComponentWithAction.Name} " +
                    $"at distance {chestCurrentAction.Destination.Distance(LokiPoe.Me.Position)}.");
                return true;
            }

            // We should really not restrict this to rares - a *lot* of monsters have shit we need to dodge.
            // OOoooooookay. If I don't filter it, it spends a *ton* of time dodging random shit from normal monsters. Let's try "magic".
            // TODO: This should really be configurable.
            var uniqueActingMonsters = monsters.Where(x => x.HasCurrentAction && x.Rarity >= Rarity.Magic);
            if (!uniqueActingMonsters.Any()) return false;

            foreach (var monster in uniqueActingMonsters)
            {
                if (monster.IsDebloyingEvilness)
                {
                    var deployedThing = monster.Deployeds.FirstOrDefault(evil => evil.Position.Distance(LokiPoe.Me.Position) < evil.CharacterSize/2 + 1);
                    if (deployedThing != null)
                    {
                        Log.Error($"[{Name}:ShouldDodge] We should dodge deployed thing {deployedThing.Name} from caster {monster.Name} " +
                            $"at distance {deployedThing.Position.Distance(LokiPoe.Me.Position)}.");
                        return true;
                    }
                }
                else if (monster.SkillTragetName == LokiPoe.Me.Name && monster.SkillName != "Default Attack" && monster.SkillDestination.Distance(LokiPoe.Me.Position) < 15)
                {
                    Log.Error($"[{Name}:ShouldDodge] We should dodge skill {monster.SkillName} from caster {monster.Name} " +
                        $"at distance {monster.SkillDestination.Distance(LokiPoe.Me.Position)}.");
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Attempt to get the best available position to dodge to within a radius defined by the given <see cref="maximumDodgeRange">dodge range</see>.
        /// </summary>
        /// <param name="maximumDodgeRange"></param>
        /// <param name="monsters"></param>
        /// <param name="blacklistedPoints"></param>
        /// <returns></returns>
        public static Vector2i GetBestDodgePosition(int maximumDodgeRange, IEnumerable<CachedMonster> monsters, IEnumerable<Vector2i> blacklistedPoints)
        {
            var dodgePosition = Vector2i.Zero;

            var targets = new Targeting();
            var closestTargetMonster = targets.Targets().OfType<Monster>().FirstOrDefault();
            if (closestTargetMonster != null)
            {
                var closestMonsterDistance = (int) closestTargetMonster.Distance;
                var range = closestMonsterDistance > 20 ? closestMonsterDistance : 20;

                dodgePosition = GetBestKitingPosition(closestTargetMonster.Position, monsters, range, Rarity.Normal, 6, blacklistedPoints);
            }
            else
                dodgePosition = GetBestKitingPosition(LokiPoe.Me.Position, monsters, maximumDodgeRange, Rarity.Normal, 6, blacklistedPoints);
            return dodgePosition;
        }

        /// <summary>
        /// A multi-frame coroutine behavior for moving toward the given location.
        /// NOTES: Called twice in DodgeHelper and once in StrongBoxTask.
        /// </summary>
        /// <param name="location"></param>
        /// <returns>"false" if we can iterate to the next task; otherwise "true" to restart task iteration.</returns>
        public static async Task<bool> MoveToLocation(Vector2i location)
        {
            // NOTE: This *will* affect the player's current position. CastSkillToFlee executes outside framelock.
            // We need to use the player's original position for post-move validation.
            Vector2i originalPlayerPosition;
            using (LokiPoe.AcquireFrame())
            {
                originalPlayerPosition = LokiPoe.Me.Position;
                var movementSkill = PlayerMoverPlugin.GetUsableMovementSkill(true);
                if (movementSkill != null)
                {
                    if (MoveHelper.UseMovementSkill(movementSkill.Slot, location) && LokiPoe.MyPosition != originalPlayerPosition)
                        return true;

                    Log.Error($"[{Name}:MoveToLocation] Failed to use movement skill.");
                }
            }

            if (SkillAwarePlayerMoverSettings.Instance.UseOnlySkillValue) return true;

            var nearbyChests = LokiPoe.ObjectManager.GetObjectsByType<Chest>().Where(x => x.Position.Distance(LokiPoe.Me.Position) < 30);
            foreach (var chest in nearbyChests)
            {
                Blacklist.Add(chest.Id, new TimeSpan(0, 0, 3), "Blacklisted for 3 seconds to allow faster Dodge");
            }

            // This will move using normal movement *or* skill movement. Why aren't we just using it instead of the other?
            await CoroutinesV3.MoveToLocation(location, 4, SkillAwarePlayerMoverSettings.Instance.MaxDodgeTimeValue, () => false);
            if (LokiPoe.Me.Position != originalPlayerPosition)
                return true;

            MiscHelpers.GenericTaskExecute<object>("SurviveTask50", "BlacklistPos", new dynamic[] { location });
            return false;
        }

        /// <summary>
        /// It's time to demystify this shit.
        /// </summary>
        /// <param name="centerPoint"></param>
        /// <param name="monsters"></param>
        /// <param name="range"></param>
        /// <param name="rarity"></param>
        /// <param name="minRange"></param>
        /// <param name="blacklistedPoints"></param>
        /// <returns></returns>
        public static Vector2i GetBestKitingPosition(Vector2i centerPoint, IEnumerable<CachedMonster> monsters, int range = 65, Rarity rarity = Rarity.Magic, int minRange = 15, IEnumerable<Vector2i> blacklistedPoints = null)
        {
            var numberOfCalculations = 0;
            var safePosition = centerPoint;

            var bestLocationWeight = decimal.MaxValue;

            var mobsInArea = monsters;

            // If any of our detected monsters is holding the Karui spirit, center our safespot search on them... we can't just avoid them entirely.
            var monsterWithKaruiSpirit = mobsInArea.FirstOrDefault(m => m.HasKaruiSpirit);
            if (monsterWithKaruiSpirit != null)
            {
                range = 40;
                centerPoint = monsterWithKaruiSpirit.Position;
            }

            var monstersCurrentlyAttacking = mobsInArea.Where(x => x.HasCurrentAction && x.SkillName != "Default Attack").ToList();
            var nearbyUnopenedDoors = LokiPoe.ObjectManager.Doors.Where(d => d.Distance <= range && !d.IsOpened).ToList();

            for (var currentRange = range; currentRange >= minRange; currentRange -= 5)
            {
                for (var i = 1; i <= 360 ; i += 22)
                {
                    var pointOnCircle = MoveHelper.GetPointOnCircle(centerPoint.X, centerPoint.Y, i, currentRange);
                    if (pointOnCircle == Vector2i.Zero) continue;

                    Vector2i hitPoint;
                    if (!ExilePather.Raycast(centerPoint, centerPoint.GetPointAtDistanceAfterThis(pointOnCircle, currentRange + 5), out hitPoint, true))
                    {
                        var newPosition = (centerPoint + hitPoint) * 0.5f;
                        if (newPosition.Distance(centerPoint) > minRange)
                            pointOnCircle = newPosition;
                    }

                    // If the current candidate point is too close to a blacklisted point, skip it.
                    if (blacklistedPoints != null && blacklistedPoints.Any(x => x.Distance(pointOnCircle) < 4)) continue;

                    // If we can't walk to the point, skip it.
                    if (!ExilePather.IsWalkable(pointOnCircle)) continue;

                    // If we can't currently even see the point, skip it.
                    // This will cause us to entirely skip many, *many* possible routes that are just around a corner. This has to go.
                    //if (!ExilePather.CanObjectSee(LokiPoe.Me, pointOnCircle, true)) continue;

                    // Not 100% sure what this is actually doing.
                    if (nearbyUnopenedDoors.Count > 0 && nearbyUnopenedDoors.Any(door => MiscHelpers.ClosedDoorBetween(door, centerPoint, pointOnCircle, 10, 10, true)))
                        continue;

                    numberOfCalculations++;

                    var midPoint = (pointOnCircle + LokiPoe.Me.Position) * 0.5f;
                    
                    var tempLocationWeight = CalculateMonsterWeight(pointOnCircle, midPoint, mobsInArea) + CalculateMonsterMinionWeight(pointOnCircle, midPoint, monstersCurrentlyAttacking, rarity);

                    if (tempLocationWeight >= bestLocationWeight) continue;

                    float degree;

                    if (LokiPoe.Me.Components.PositionedComponent.Rotation > 3.14)
                        degree = LokiPoe.Me.Components.PositionedComponent.Rotation - 3.14f;
                    else
                        degree = LokiPoe.Me.Components.PositionedComponent.Rotation + 3.14f;

                    var pointBehinde = MathEx.GetPointAt(LokiPoe.Me.Position.ToVector3(), currentRange, degree);

                    tempLocationWeight += 1000 / (decimal)(pointOnCircle.Distance(new Vector2i((int)pointBehinde.X, (int)pointBehinde.Y)) + 1);

                    if (tempLocationWeight >= bestLocationWeight) continue;                    

                    if (tempLocationWeight <= 1)
                    {
                        var formattedTempLocationWeight = tempLocationWeight.ToString("N3");
                        var pointDistance = pointOnCircle.Distance(centerPoint);
                        Log.Info($"[{Name}:CalcAdvSafePosition] Possibility: weight {formattedTempLocationWeight}, distance {pointDistance}, # calculation {numberOfCalculations}, " +
                                 $"nearby monsters {mobsInArea.Count()}, evil crap {monstersCurrentlyAttacking.Count}.");
                        return pointOnCircle;
                    }                    

                    bestLocationWeight = tempLocationWeight;

                    safePosition = pointOnCircle;
                }
            }

            var formattedBestLocationWeight = bestLocationWeight.ToString("N3");
            var safePositionDistance = safePosition.Distance(centerPoint);
            Log.Info($"[{Name}:CalcAdvSafePosition] Safe position: weight {formattedBestLocationWeight}, distance {safePositionDistance}, # calculation {numberOfCalculations}, " +
                     $"nearby monsters {mobsInArea.Count()}, evil crap {monstersCurrentlyAttacking.Count}.");

            return safePosition;
        }

        /// <summary>
        /// Calculate the "weight" of minions summoned by monsters near the given points. Larger numbers are an indicator of more pain and suffering near a point.
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="midSpot"></param>
        /// <param name="monsters"></param>
        /// <param name="rarity"></param>
        /// <returns></returns>
        private static decimal CalculateMonsterMinionWeight(Vector2i spot, Vector2i midSpot, List<CachedMonster> monsters, Rarity rarity = Rarity.Magic)
        {
            if (monsters == null) throw new ArgumentNullException(nameof(monsters));

            var result = (decimal)0;
            var monstersOfSufficientRarity = monsters.Where(x => x != null && x.Rarity >= rarity);
            foreach (var monster in monstersOfSufficientRarity)
            {
                if (!monster.HasCurrentAction) continue;

                var modifier = GetFearWeightModifierByMonsterRarity(monster.Rarity);

                // If the monster doesn't summon things, continue.
                if (!monster.IsDebloyingEvilness) continue;
                
                foreach (var summonedMinion in monster.Deployeds)
                {
                    result += modifier / (decimal)(summonedMinion.Position.Distance(spot) + 1);
                    result += modifier / (decimal)(summonedMinion.Position.Distance(midSpot) + 1);
                }
            }
            return result;
        }

        /// <summary>
        /// Calculate the "weight" of monsters near the given points. Larger numbers are an indicator of more pain and suffering near that point.
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="midSpot"></param>
        /// <param name="monsters"></param>
        /// <returns></returns>
        private static decimal CalculateMonsterWeight(Vector2i spot, Vector2i midSpot, IEnumerable<CachedMonster> monsters)
        {
            var result = (decimal)0;

            foreach (var monster in monsters.Where(x => x != null))
            {
                var modifier = GetFearWeightModifierByMonsterRarity(monster.Rarity);

                result += modifier/(decimal) (monster.Position.Distance(spot) + 1);
                result += modifier/(decimal) (monster.Position.Distance(midSpot) + 1);
            }

            return result;
        }

        private static int GetFearWeightModifierByMonsterRarity(Rarity rarity)
        {
            int modifier;
            switch (rarity)
            {
                case Rarity.Unique:
                    modifier = 27;
                    break;
                case Rarity.Rare:
                    modifier = 9;
                    break;
                case Rarity.Magic:
                    modifier = 3;
                    break;
                default:
                    modifier = 1;
                    break;
            }
            return modifier;
        }
    }
}
