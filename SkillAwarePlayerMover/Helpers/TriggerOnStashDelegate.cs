using Loki.Game.Objects;

namespace SkillAwarePlayerMover.Helpers
{
    /// <summary>
    /// A delegate for triggering an item being stashed.
    /// </summary>
    /// <param name="item"></param>
    public delegate void TriggerOnStashDelegate(Item item);
}