using Loki.Bot;

namespace SkillAwarePlayerMover.Helpers
{
    public class RoutineHelpers
    {
        private const string Name = nameof(RoutineHelpers);
        private IRoutine currentRoutine;

        public RoutineHelpers()
        {
            currentRoutine = RoutineManager.CurrentRoutine;
        }

        public bool IsCrDisabled { get { return GetCombatRange() < 5; } }

        public int GetCombatRange()
        {
            var value = currentRoutine.Execute("GetCombatRange", null);

            if (value == null)
            {
                //Alcor75PlayerMover.Log.DebugFormat("[Alcor75PlayerMover] {0} CombatRoutine doesn't implement the GetCombatRange check in the Execute() part.", LoadedCR.Name);
                return 9999;
            }

            //Main.Log.DebugFormat("[FlaskHelperRoutineInfos] CombatRange : {0}", (int) value);
            return (int)value;
        }

        public int GetMaxMeleeRange()
        {
            var value = currentRoutine.Execute("GetMaxMeleeRange", null);

            if (value == null)
            {
                //Alcor75PlayerMover.Log.DebugFormat("[Alcor75PlayerMover] {0} CombatRoutine doesn't implement the GetMaxMeleeRange check in the Execute() part.", LoadedCR.Name);
                return 9999;
            }

            //Main.Log.DebugFormat("[FlaskHelperRoutineInfos] CombatRange : {0}", (int) value);
            return (int)value;
        }

        public int GetMaxRangedRange()
        {
            var value = currentRoutine.Execute("GetMaxRangeRange", null);

            if (value == null)
            {
                //Alcor75PlayerMover.Log.DebugFormat("[Alcor75PlayerMover] {0} CombatRoutine doesn't implement the GetMaxRangeRange check in the Execute() part.", LoadedCR.Name);
                return 9999;
            }

            //Main.Log.DebugFormat("[FlaskHelperRoutineInfos] CombatRange : {0}", (int) value);
            return (int)value;
        }

        public int GetFallbackSlot()
        {
            var value = currentRoutine.Execute("GetFallbackSlot", null);

            if (value == null)
            {
                //Alcor75PlayerMover.Log.DebugFormat("[Alcor75PlayerMover] {0} CombatRoutine doesn't implement the GetFallbackSlot check in the Execute() part.", LoadedCR.Name);
                return 9999;
            }

            //Main.Log.DebugFormat("[FlaskHelperRoutineInfos] CombatRange : {0}", (int) value);
            return (int)value;
        }

        public int GetAoeMeleeSlot()
        {
            var value = currentRoutine.Execute("GetAoeMeleeSlot", null);

            if (value == null)
            {
                //Alcor75PlayerMover.Log.DebugFormat("[Alcor75PlayerMover] {0} CombatRoutine doesn't implement the GetAoeMeleeSlot check in the Execute() part.", LoadedCR.Name);
                return 9999;
            }

            //Main.Log.DebugFormat("[FlaskHelperRoutineInfos] CombatRange : {0}", (int) value);
            return (int)value;
        }

        public int GetAoeRangedSlot()
        {
            var value = currentRoutine.Execute("GetAoeRangedSlot", null);

            if (value == null)
            {
                //Alcor75PlayerMover.Log.DebugFormat("[Alcor75PlayerMover] {0} CombatRoutine doesn't implement the GetAoeRangedSlot check in the Execute() part.", LoadedCR.Name);
                return 9999;
            }

            //Main.Log.DebugFormat("[FlaskHelperRoutineInfos] CombatRange : {0}", (int) value);
            return (int)value;
        }

        public int GetSingleTargetMeleeSlot()
        {
            var value = currentRoutine.Execute("GetSingleTargetMeleeSlot", null);

            if (value == null)
            {
                //Alcor75PlayerMover.Log.DebugFormat("[Alcor75PlayerMover] {0} CombatRoutine doesn't implement the GetSingleTargetMeleeSlot check in the Execute() part.", LoadedCR.Name);
                return 9999;
            }

            //Main.Log.DebugFormat("[FlaskHelperRoutineInfos] CombatRange : {0}", (int) value);
            return (int)value;
        }

        public int GetSingleTargetRangedSlot()
        {
            var value = currentRoutine.Execute("GetSingleTargetRangedSlot", null);

            if (value == null)
            {
                //Alcor75PlayerMover.Log.DebugFormat("[Alcor75PlayerMover] {0} CombatRoutine doesn't implement the GetSingleTargetRangedSlot check in the Execute() part.", LoadedCR.Name);
                return 9999;
            }

            //Main.Log.DebugFormat("[FlaskHelperRoutineInfos] CombatRange : {0}", (int) value);
            return (int)value;
        }

        public bool LeaveFrame()
        {
            bool result = false; 
            var test = currentRoutine.Execute("GetLeaveFrame");
            if (test != null)
                result = (bool) test;
            return result;
        }

        public string GetRoutineName()
        {
            var value = currentRoutine.Name;

            if (string.IsNullOrEmpty(value))
            {
                PlayerMoverPlugin.Log.Debug($"[{Name}:GetRoutineName] Routine not loaded ...");
                return "";
            }
            return value;
        }

        public void Save()
        {
            currentRoutine.Settings.Save();
        }
    }
}
