using Loki.Common;
using Loki.Game.Objects;

namespace SkillAwarePlayerMover.Class
{
    public class CachedDeployed
    {
        public string Name { get; set; }
        public Vector2i Position { get; set; }
        public int CharacterSize { get; set; }

        public CachedDeployed(NetworkObject networkObject)
        {
            if (networkObject == null)
            {
                Name = "";
                Position = Vector2i.Zero;
                CharacterSize = 0;
            }
            Name = networkObject.Name;
            Position = networkObject.Position;
            CharacterSize = networkObject.CharacterSize;
        }
    }
}
