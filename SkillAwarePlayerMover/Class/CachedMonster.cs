using System.Collections.Generic;
using Loki.Common;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace SkillAwarePlayerMover.Class
{
    public class CachedMonster
    {
        public string Name { get; set; }
        public Vector2i Position { get; set; }
        public Rarity Rarity { get; set; }
        public bool HasKaruiSpirit { get; set; }
        public bool HasCurrentAction { get; set; }
        public string SkillName { get; set; }

        public Vector2i SkillDestination { get; set; }
        public string SkillTragetName { get; set; }
        public bool IsDebloyingEvilness { get; set; }
        public List<CachedDeployed> Deployeds { get; set; }

        public CachedMonster(Monster monster)
        {
            if (monster == null)
            {
                Name = "";
                Position = Vector2i.Zero;
                Rarity = Rarity.Normal;
                HasKaruiSpirit = false;
                HasCurrentAction = false;
                SkillName = "";
                SkillDestination = Vector2i.Zero;
                SkillTragetName = "";
                IsDebloyingEvilness = false;
                Deployeds = new List<CachedDeployed>();
            }
            else
            {
                Name = monster.Name;
                Position = monster.Position;
                Rarity = monster.Rarity;
                HasKaruiSpirit = monster.HasKaruiSpirit;
                if (monster.HasCurrentAction && monster.CurrentAction != null && monster.CurrentAction.Skill != null)
                {
                    HasCurrentAction = true;
                    SkillName = monster.CurrentAction.Skill.Name;
                    SkillDestination = monster.CurrentAction.Destination;
                    if (monster.CurrentAction.Target != null)
                        SkillTragetName = monster.CurrentAction.Target.Name;
                    else
                        SkillTragetName = "";
                    if (monster.CurrentAction.Skill.NumberDeployed > 0)
                    {
                        IsDebloyingEvilness = true;
                        Deployeds = new List<CachedDeployed>();
                        foreach (var obj in monster.CurrentAction.Skill.DeployedObjects)
                        {
                            Deployeds.Add(new CachedDeployed(obj));
                        }
                    }
                    else
                    {
                        IsDebloyingEvilness = false;
                        Deployeds = new List<CachedDeployed>();
                    }
                }
                else
                {
                    HasCurrentAction = false;
                    SkillName = "";
                    SkillDestination = Vector2i.Zero;
                    SkillTragetName = "";
                    IsDebloyingEvilness = false;
                    Deployeds = new List<CachedDeployed>();
                }
            }
        }
    }
}