﻿using System.ComponentModel;
using System.Windows.Media;
using Loki;
using Loki.Common;
using Newtonsoft.Json;

namespace SkillAwarePlayerMover
{
    public class SkillAwarePlayerMoverSettings : JsonSettings
    {
        private static SkillAwarePlayerMoverSettings _instance;
        /// <summary>The current instance for this class. </summary>
        public static SkillAwarePlayerMoverSettings Instance
        {
            get { return _instance ?? (_instance = new SkillAwarePlayerMoverSettings()); }
        }

        /// <summary>The default ctor. Will use the settings path "QuestPlugin".</summary>
        public SkillAwarePlayerMoverSettings()
            : base(GetSettingsFilePath(Configuration.Instance.Name, string.Format("{0}.json", "SkillAwarePlayerMover")))
        {
        }

        private int _moveMinMana;

        private bool _ignoreMobs;

        private bool _useBloodMagicValue;

        private bool _useESValue;

        private bool _useOnlySkillValue;

        private int _sensibilityValue;

        private bool _strongBoxLogic;

        private bool _chestLogic;

        private bool _dodgeLogicValue;

        private bool _kitineLogicValue;

        private int _kitineRange;

        private int _fearLevelValue;
        private int _actualfear;
        private double _warningFearValue;
        private double _dangerFearValue;
        private Brush _IsDodgeInCoolDownValue;
        private Brush _IsKitineInCoolDownValue;
        private int _frequencyValue;

        private int _maxDodgeTime;
        private int _escapeCooldown;

        private bool _enableLwValue;
        private int _lwMinDist;
        private int _lwMaxDist;

        private bool _enablePrValue;
        private int _prMinDist;
        private int _prMaxDist;

        private bool _enableLsValue;
        private int _lsMinDist;
        private int _lsMaxDist;

        private bool _enableFdValue;
        private int _fdMinDist;
        private int _fdMaxDist;

        private bool _enableBaValue;
        private int _baMinDist;
        private int _baMaxDist;

        private bool _enableWbValue;
        private int _wbMinDist;
        private int _wbMaxDist;

        private bool _enableScValue;
        private int _scMinDist;
        private int _scMaxDist;

        [DefaultValue(20)]
        public int MoveMinManaValue
        {
            get { return _moveMinMana; }
            set
            {
                if (value.Equals(_moveMinMana))
                {
                    return;
                }
                _moveMinMana = value;
                NotifyPropertyChanged(() => MoveMinManaValue);
            }
        }

        [DefaultValue(10)]
        public int SensibilityValue
        {
            get { return _sensibilityValue; }
            set
            {
                if (value.Equals(_sensibilityValue))
                {
                    return;
                }
                _sensibilityValue = value;
                NotifyPropertyChanged(() => SensibilityValue);
            }
        }

        [DefaultValue(false)]
        public bool IgnoreMobsValue
        {
            get { return _ignoreMobs; }
            set
            {
                if (value.Equals(_ignoreMobs))
                {
                    return;
                }
                _ignoreMobs = value;
                NotifyPropertyChanged(() => IgnoreMobsValue);
            }
        }

        [DefaultValue(false)]
        public bool UseBloodMagicValue
        {
            get { return _useBloodMagicValue; }
            set
            {
                if (value.Equals(_useBloodMagicValue))
                {
                    return;
                }
                _useBloodMagicValue = value;
                NotifyPropertyChanged(() => UseBloodMagicValue);
            }
        }
        [DefaultValue(false)]
        public bool UseESValue
        {
            get { return _useESValue; }
            set
            {
                if (value.Equals(_useESValue))
                {
                    return;
                }
                _useESValue = value;
                NotifyPropertyChanged(() => UseESValue);
            }
        }
        [DefaultValue(false)]
        public bool UseOnlySkillValue
        {
            get { return _useOnlySkillValue; }
            set
            {
                if (value.Equals(_useOnlySkillValue))
                {
                    return;
                }
                _useOnlySkillValue = value;
                NotifyPropertyChanged(() => UseOnlySkillValue);
            }
        }

        [DefaultValue(false)]
        public bool StrongBoxLogicValue
        {
            get { return _strongBoxLogic; }
            set
            {
                if (value.Equals(_strongBoxLogic))
                {
                    return;
                }
                _strongBoxLogic = value;
                NotifyPropertyChanged(() => StrongBoxLogicValue);
            }
        }

        [DefaultValue(true)]
        public bool DodgeLogicValue
        {
            get { return _dodgeLogicValue; }
            set
            {
                if (value.Equals(_dodgeLogicValue))
                {
                    return;
                }
                _dodgeLogicValue = value;
                NotifyPropertyChanged(() => DodgeLogicValue);
            }
        }

        [DefaultValue(true)]
        public bool KitineLogicValue
        {
            get { return _kitineLogicValue; }
            set
            {
                if (value.Equals(_kitineLogicValue))
                {
                    return;
                }
                _kitineLogicValue = value;
                NotifyPropertyChanged(() => KitineLogicValue);
            }
        }

        [DefaultValue(5)]
        public int FearLevelValue
        {
            get { return _fearLevelValue; }
            set
            {
                if (value.Equals(_fearLevelValue))
                {
                    return;
                }
                _fearLevelValue = value;
                DangerFearValue = (double) 1/30*_fearLevelValue;
                WarningFearValue = ((double)DangerFearValue * 2 / 3);
                NotifyPropertyChanged(() => FearLevelValue);
            }
        }

        [JsonIgnore]
        public double WarningFearValue
        {
            get { return _warningFearValue; }
            set
            {
                if (value.Equals(_warningFearValue))
                {
                    return;
                }
                _warningFearValue = value;
                NotifyPropertyChanged(() => WarningFearValue);
            }
        }
        [JsonIgnore]
        public double DangerFearValue
        {
            get { return _dangerFearValue; }
            set
            {
                if (value.Equals(_dangerFearValue))
                {
                    return;
                }
                _dangerFearValue = value;
                NotifyPropertyChanged(() => DangerFearValue);
            }
        }

        [JsonIgnore]
        public int ActualFearValue
        {
            get { return _actualfear; }
            set
            {
                if (value.Equals(_actualfear))
                {
                    return;
                }
                _actualfear = value;
                NotifyPropertyChanged(() => ActualFearValue);
            }
        }

        [JsonIgnore]
        public Brush IsDodgeInCoolDownValue
        {
            get { return _IsDodgeInCoolDownValue; }
            set
            {
                if (value.Equals(_IsDodgeInCoolDownValue))
                {
                    
                    return;
                }
                _IsDodgeInCoolDownValue = value;
                NotifyPropertyChanged(() => IsDodgeInCoolDownValue);
            }
        }
        [JsonIgnore]
        public Brush IsKitineInCoolDownValue
        {
            get { return _IsKitineInCoolDownValue; }
            set
            {
                if (value.Equals(_IsKitineInCoolDownValue))
                {

                    return;
                }
                _IsKitineInCoolDownValue = value;
                NotifyPropertyChanged(() => IsKitineInCoolDownValue);
            }
        }

        [DefaultValue(550)]
        public int FrequencyValue
        {
            get { return _frequencyValue; }
            set
            {
                if (value.Equals(_frequencyValue))
                {
                    return;
                }
                _frequencyValue = value;
                NotifyPropertyChanged(() => FrequencyValue);
            }
        }

        [DefaultValue(1500)]
        public int MaxDodgeTimeValue
        {
            get { return _maxDodgeTime; }
            set
            {
                if (value.Equals(_maxDodgeTime))
                {
                    return;
                }
                _maxDodgeTime = value;
                NotifyPropertyChanged(() => MaxDodgeTimeValue);
            }
        }

        [DefaultValue(3000)]
        public int EscapeCooldownValue
        {
            get { return _escapeCooldown; }
            set
            {
                if (value.Equals(_escapeCooldown))
                {
                    return;
                }
                _escapeCooldown = value;
                NotifyPropertyChanged(() => EscapeCooldownValue);
            }
        }

        [DefaultValue(50)]
        public int KitineRangeValue
        {
            get { return _kitineRange; }
            set
            {
                if (value.Equals(_kitineRange))
                {
                    return;
                }
                _kitineRange = value;
                NotifyPropertyChanged(() => KitineRangeValue);
            }
        }

        [DefaultValue(true)]
        public bool EnableLwValue
        {
            get { return _enableLwValue; }
            set
            {
                if (value.Equals(_enableLwValue))
                {
                    return;
                }
                _enableLwValue = value;
                NotifyPropertyChanged(() => EnableLwValue);
            }
        }

        [DefaultValue(30)]
        public int LwMinDistValue
        {
            get { return _lwMinDist; }
            set
            {
                if (value.Equals(_lwMinDist))
                {
                    return;
                }
                _lwMinDist = value;
                NotifyPropertyChanged(() => LwMinDistValue);
            }
        }
        [DefaultValue(70)]
        public int LwMaxDistValue
        {
            get { return _lwMaxDist; }
            set
            {
                if (value.Equals(_lwMaxDist))
                {
                    return;
                }
                _lwMaxDist = value;
                NotifyPropertyChanged(() => LwMaxDistValue);
            }
        }

        [DefaultValue(true)]
        public bool EnableBaValue
        {
            get { return _enableBaValue; }
            set
            {
                if (value.Equals(_enableBaValue))
                {
                    return;
                }
                _enableBaValue = value;
                NotifyPropertyChanged(() => EnableBaValue);
            }
        }

        [DefaultValue(25)]
        public int BaMinDistValue
        {
            get { return _baMinDist; }
            set
            {
                if (value.Equals(_baMinDist))
                {
                    return;
                }
                _baMinDist = value;
                NotifyPropertyChanged(() => BaMinDistValue);
            }
        }

        [DefaultValue(70)]
        public int BaMaxDistValue
        {
            get { return _baMaxDist; }
            set
            {
                if (value.Equals(_baMaxDist))
                {
                    return;
                }
                _baMaxDist = value;
                NotifyPropertyChanged(() => BaMaxDistValue);
            }
        }

        [DefaultValue(true)]
        public bool EnableFdValue
        {
            get { return _enableFdValue; }
            set
            {
                if (value.Equals(_enableFdValue))
                {
                    return;
                }
                _enableFdValue = value;
                NotifyPropertyChanged(() => EnableFdValue);
            }
        }

        [DefaultValue(25)]
        public int FdMinDistValue
        {
            get { return _fdMinDist; }
            set
            {
                if (value.Equals(_fdMinDist))
                {
                    return;
                }
                _fdMinDist = value;
                NotifyPropertyChanged(() => FdMinDistValue);
            }
        }

        [DefaultValue(70)]
        public int FdMaxDistValue
        {
            get { return _fdMaxDist; }
            set
            {
                if (value.Equals(_fdMaxDist))
                {
                    return;
                }
                _fdMaxDist = value;
                NotifyPropertyChanged(() => FdMaxDistValue);
            }
        }

        [DefaultValue(true)]
        public bool EnableLsValue
        {
            get { return _enableLsValue; }
            set
            {
                if (value.Equals(_enableLsValue))
                {
                    return;
                }
                _enableLsValue = value;
                NotifyPropertyChanged(() => EnableLsValue);
            }
        }

        [DefaultValue(30)]
        public int LsMinDistValue
        {
            get { return _lsMinDist; }
            set
            {
                if (value.Equals(_lsMinDist))
                {
                    return;
                }
                _lsMinDist = value;
                NotifyPropertyChanged(() => LsMinDistValue);
            }
        }

        [DefaultValue(50)]
        public int LsMaxDistValue
        {
            get { return _lsMaxDist; }
            set
            {
                if (value.Equals(_lsMaxDist))
                {
                    return;
                }
                _lsMaxDist = value;
                NotifyPropertyChanged(() => LsMaxDistValue);
            }
        }

        [DefaultValue(true)]
        public bool EnablePrValue
        {
            get { return _enablePrValue; }
            set
            {
                if (value.Equals(_enablePrValue))
                {
                    return;
                }
                _enablePrValue = value;
                NotifyPropertyChanged(() => EnablePrValue);
            }
        }

        [DefaultValue(10)]
        public int PrMinDistValue
        {
            get { return _prMinDist; }
            set
            {
                if (value.Equals(_prMinDist))
                {
                    return;
                }
                _prMinDist = value;
                NotifyPropertyChanged(() => PrMinDistValue);
            }
        }

        [DefaultValue(70)]
        public int PrMaxDistValue
        {
            get { return _prMaxDist; }
            set
            {
                if (value.Equals(_prMaxDist))
                {
                    return;
                }
                _prMaxDist = value;
                NotifyPropertyChanged(() => PrMaxDistValue);
            }
        }

        [DefaultValue(true)]
        public bool EnableWbValue
        {
            get { return _enableWbValue; }
            set
            {
                if (value.Equals(_enableWbValue))
                {
                    return;
                }
                _enableWbValue = value;
                NotifyPropertyChanged(() => EnableWbValue);
            }
        }

        [DefaultValue(10)]
        public int WbMinDistValue
        {
            get { return _wbMinDist; }
            set
            {
                if (value.Equals(_wbMinDist))
                {
                    return;
                }
                _wbMinDist = value;
                NotifyPropertyChanged(() => WbMinDistValue);
            }
        }

        [DefaultValue(70)]
        public int WbMaxDistValue
        {
            get { return _wbMaxDist; }
            set
            {
                if (value.Equals(_wbMaxDist))
                {
                    return;
                }
                _wbMaxDist = value;
                NotifyPropertyChanged(() => WbMaxDistValue);
            }
        }

        [DefaultValue(true)]
        public bool EnableScValue
        {
            get { return _enableScValue; }
            set
            {
                if (value.Equals(_enableScValue))
                {
                    return;
                }
                _enableScValue = value;
                NotifyPropertyChanged(() => EnableScValue);
            }
        }

        [DefaultValue(10)]
        public int ScMinDistValue
        {
            get { return _scMinDist; }
            set
            {
                if (value.Equals(_scMinDist))
                {
                    return;
                }
                _scMinDist = value;
                NotifyPropertyChanged(() => ScMinDistValue);
            }
        }

        [DefaultValue(70)]
        public int ScMaxDistValue
        {
            get { return _scMaxDist; }
            set
            {
                if (value.Equals(_scMaxDist))
                {
                    return;
                }
                _scMaxDist = value;
                NotifyPropertyChanged(() => ScMaxDistValue);
            }
        }
    }
}
