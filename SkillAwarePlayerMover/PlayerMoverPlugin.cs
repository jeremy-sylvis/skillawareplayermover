﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using log4net;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.Objects;
using SkillAwarePlayerMover.Tasks;

namespace SkillAwarePlayerMover
{
    public class PlayerMoverPlugin : IPlugin
    {
        public static readonly ILog Log = Logger.GetLoggerInstanceForType();
        private bool _enabled;
        private Gui _instance;

        private Stopwatch _tickGovernorStopwatch;

        public static double CachedDangerLevel;
        
        public static TaskManager TaskManager;        
        private readonly SkillAwarePlayerMover _skillAwarePlayerMover = new SkillAwarePlayerMover();

        public static List<Vector2i> BlacklistedLocations = new List<Vector2i>();

        #region IAuthored implementation

        /// <summary> The name of the plugin. </summary>
        public string Name => "Skill-Aware Player Mover";

        /// <summary> The description of the plugin. </summary>
        public string Description => "A plugin that try to use movement skills to speed up exploration and facilitate kiting and dodging.";

        /// <summary>The author of the plugin.</summary>
        public string Author => "stealthy (forked from Alcor75's plugin; based on BosslandGmbH's basic PlayerMover)";

        /// <summary>The version of the plugin.</summary>
        public string Version => "0.0.1.5";

        #endregion

        #region IBase implementation

        /// <summary>Initializes this plugin.</summary>
        public void Initialize()
        {
            Log.Debug($"[{Name}] Initialize");
        }

        public void Deinitialize()
        {
            Log.Debug($"[{Name}] Deinitialize");
        }

        #endregion

        #region IRunnable implementation

        /// <summary> The plugin start callback. Do any initialization here. </summary>
        public void Start()
        {
            Log.Debug($"[{Name}] Start");

            _tickGovernorStopwatch = Stopwatch.StartNew();

            TaskManager = (TaskManager)BotManager.CurrentBot.Execute("GetTaskManager");
            CachedDangerLevel = 0.0;      

            // This is all that needs to be done. All movement logic that uses PlayerMover.MoveTowards
            // will now use this new instance's implementation.
            
            Log.Info($"[{Name}] Now setting the CustomPlayerMover.");
            PlayerMover.Instance = _skillAwarePlayerMover;
            _skillAwarePlayerMover.MoveSkillSlot = -1; // reset the skill slot in case the user changed something
        }

        /// <summary> The plugin tick callback. Do any update logic here. </summary>
        public void Tick()
        {
            if (!_enabled) return;

            // Don't update while we are not in the game.
            if (!LokiPoe.IsInGame || LokiPoe.Me.IsDead || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout)
                return;

            // Reduce CPU Usage
            if (_tickGovernorStopwatch.ElapsedMilliseconds <= 1000)
                return;

            // Reset cpu CD
            _tickGovernorStopwatch.Restart();

            var surviveTask50 = TaskManager.GetTaskByName("SurviveTask50");
            var doesTaskManagerContainSurviveTask = TaskManager.TaskList.Contains(surviveTask50);

            var shouldEnableSurviveTask = SkillAwarePlayerMoverSettings.Instance.DodgeLogicValue || SkillAwarePlayerMoverSettings.Instance.KitineLogicValue ||
                                          SkillAwarePlayerMoverSettings.Instance.StrongBoxLogicValue;

            // Default to fetching the non-range-restricted combat task
            var combatTask = TaskManager.TaskList.FirstOrDefault(t => t.Name == "CombatTask");

            // Fall back to the range-restricted combat task names
            if (combatTask == null)
                combatTask = TaskManager.TaskList.First(t => t.Name.StartsWith("CombatTask (Leash "));

            var taskName = combatTask.Name;

            if (shouldEnableSurviveTask)
            {                
                if (!doesTaskManagerContainSurviveTask)
                {
                    var successfullyAddedTask = TaskManager.AddBefore(new SurviveTask50(), taskName);
                    if (successfullyAddedTask)
                        Log.Info($"[{Name}] Task SurviveTask Added before {taskName}.");
                    else
                        Log.Error($"[{Name}] Add SurviveTask before {taskName} failed.");
                }
            }
            else
            {                
                if (doesTaskManagerContainSurviveTask)
                {
                    var successfullyRemovedTask = TaskManager.Remove("SurviveTask50");
                    if (successfullyRemovedTask)
                        Log.Info($"[{Name}] Task SurviveTask50 Removed.");
                    else
                        Log.Error($"[{Name}] Remove SurviveTask50 failed.");
                }
            }
        }

        /// <summary> The plugin stop callback. Do any pre-dispose cleanup here. </summary>
        public void Stop()
        {
            Log.Debug($"[{Name}] Stop");
        }

        #endregion

        #region Implementation of IEnableable


        /// <summary> The plugin is being enabled.</summary>
        public void Enable()
        {
            Log.Debug($"[{Name}] Enable");
            _enabled = true;
        }

        /// <summary> The plugin is being disabled.</summary>
        public void Disable()
        {
            Log.Debug("[{Name}] Disable");
            _enabled = false;
        }

        #endregion

        #region Implementation of IConfigurable

        public JsonSettings Settings
        {
            get { return SkillAwarePlayerMoverSettings.Instance; }
        }

        /// <summary> The plugin's settings control. This will be added to the Exilebuddy Settings tab.</summary>
        public UserControl Control
        {
            get { return (_instance ?? (_instance = new Gui())); }
        }

        #endregion
        
        #region Implementation of ILogic

        /// <summary>
        /// Coroutine logic to execute.
        /// </summary>
        /// <param name="type">The requested type of logic to execute.</param>
        /// <param name="param">Data sent to the object from the caller.</param>
        /// <returns>true if logic was executed to handle this type and false otherwise.</returns>
#pragma warning disable 1998
        public async Task<bool> Logic(string type, params dynamic[] param)
#pragma warning restore 1998
        {
            return false;

            //if (!LokiPoe.IsInGame || LokiPoe.Me.IsDead || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout)
            //{
            //    return false;
            //}

            //if (!Alcor75PlayerMoverSettings.Instance.KitineLogicValue && Alcor75PlayerMoverSettings.Instance.StrongBoxLogicValue && _sw.ElapsedMilliseconds > 1000)
            //{
            //    // Check for any active shrines.
            //    var strongboxes =
            //        LokiPoe.ObjectManager.Objects.OfType<Chest>()
            //            .Where(c => !c.IsOpened && 
            //                !c.IsLocked && 
            //                c.IsStrongBox && 
            //                ExilePather.CanObjectSee(LokiPoe.Me, c, true) &&
            //                !Utility.ClosedDoorBetween(LokiPoe.Me, c, 10, 10, true) && 
            //                ExilePather.PathDistance(LokiPoe.Me.Position, c.Position, true, true) < 50)
            //                .OrderBy(c => c.Distance)
            //            .ToList();

            //    if (strongboxes.Any())
            //    {                    
            //        var task = GrindBotTaskManager.GetTaskByName("StrongBoxTask");
            //        if (!GrindBotTaskManager.TaskList.Contains(task))
            //        {
            //            if (!GrindBotTaskManager.AddAtFront(new StrongBoxTask()))
            //            {
            //                Log.ErrorFormat("[StrongBoxTask] Task StrongBoxTask Added at front.");
            //            }
            //            else
            //            {
            //                Log.InfoFormat("[StrongBoxTask] Add StrongBoxTask at front failed.");
            //            }
            //        }
            //    }
            //    _sw.Restart();
            //}
            //return false;
        }

        /// <summary>
        /// Non-coroutine logic to execute.
        /// </summary>
        /// <param name="name">The name of the logic to invoke.</param>
        /// <param name="param">The data passed to the logic.</param>
        /// <returns>Data from the executed logic.</returns>
        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        #endregion

        /// <summary>
        /// Get the slot # of the first usable configured movement skill.
        /// </summary>
        /// <param name="ignoreCurrentMana"></param>
        /// <returns></returns>
        public static Skill GetUsableMovementSkill(bool ignoreCurrentMana = false)
        {
            var playerMovementSkills = GetPlayerMovementSkills();
            var firstUsableSkill = playerMovementSkills.FirstOrDefault(s => IsSkillCurrentlyUsable(s.Slot, ignoreCurrentMana));
            return firstUsableSkill;
        }

        private static IEnumerable<Skill> GetPlayerMovementSkills()
        {
            var availableMovementSkills = LokiPoe.Me.AvailableSkills.Where(
                s => s.IsOnSkillBar && (
                    (SkillAwarePlayerMoverSettings.Instance.EnablePrValue && s.Name == "Phase Run") ||
                    (SkillAwarePlayerMoverSettings.Instance.EnableLwValue && s.Name == "Lightning Warp") ||
                    (SkillAwarePlayerMoverSettings.Instance.EnableBaValue && s.Name == "Blink Arrow") ||
                    (SkillAwarePlayerMoverSettings.Instance.EnableFdValue && s.Name == "Flame Dash") ||
                    (SkillAwarePlayerMoverSettings.Instance.EnableLsValue && s.Name == "Leap Slam") ||
                    (SkillAwarePlayerMoverSettings.Instance.EnableWbValue && s.Name == "Whirling Blades") ||
                    (SkillAwarePlayerMoverSettings.Instance.EnableScValue && s.Name == "Shield Charge"))
                );
            return availableMovementSkills;
        }

        /// <summary>
        /// Get the slot # of the first found configured movement skill.
        /// </summary>
        /// <returns></returns>
        public static int GetMovementSkillSlot()
        {
            var playerMovementSkills = GetPlayerMovementSkills();
            var firstSkill = playerMovementSkills.FirstOrDefault();
            return firstSkill != null ? firstSkill.Slot : -1;
        }

        private static bool IsSkillCurrentlyUsable(int skillSlot, bool ignoreManaCost = false)
        {
            var skill = LokiPoe.InGameState.SkillBarHud.Slot(skillSlot);
            if (skill.IsOnCooldown) return false;

            if (LokiPoe.Me.EquippedItems.Any(x => x.FullName == "Soul Taker")) return true;
            if (SkillAwarePlayerMoverSettings.Instance.UseBloodMagicValue) return true;

            if (!ignoreManaCost && (LokiPoe.Me.ManaReservedPercent == 100 || LokiPoe.Me.ManaPercent < SkillAwarePlayerMoverSettings.Instance.MoveMinManaValue))
                return false;

            return skill.CanUse();
        }
    }
}
