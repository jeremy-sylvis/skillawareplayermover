using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using SkillAwarePlayerMover.Helpers;
using Utility = SkillAwarePlayerMover.Helpers.Utility;

namespace SkillAwarePlayerMover
{
    /// <summary>
    /// A player mover which takes advantage of movement skills available to the player.
    /// </summary>
    public class SkillAwarePlayerMover : IPlayerMover
    {
        private const string Name = "SkillAwarePlayerMover";

        private static readonly ILog Log = Logger.GetLoggerInstanceForType();
        private PathfindingCommand _pathfindingCommand;
        private readonly Stopwatch _pathRefreshGovernanceStopwatch = new Stopwatch();
        private bool _doAdjustments = true;
        private LokiPoe.ConfigManager.NetworkingType _networkingMode = LokiPoe.ConfigManager.NetworkingType.Unknown;

        private int _moveRange = 13;
        private int _pathRefreshRate = 1000;
        private int _singleUseDistance = 25;
        private bool _avoidWallHugging = true;

        private LokiPoe.TerrainDataEntry[,] _tgts;
        private uint _tgtSeed;
        private LokiPoe.TerrainDataEntry TgtUnderPlayer
        {
            get
            {
                var myPos = LokiPoe.LocalData.MyPosition;
                return _tgts[myPos.X / 23, myPos.Y / 23];
            }
        }

        internal int MoveSkillSlot = -1;
        private readonly Stopwatch _skillBlacklistStopwatch = Stopwatch.StartNew();
        private bool _hasUsedMovementSkill;
        private Vector2i _cachePosition = Vector2i.Zero;

        private int _maxStucks = 2;
        private int _timesStuck = 0;

        /// <summary>
        /// Non-coroutine logic to execute.
        /// </summary>
        /// <param name="name">The name of the logic to invoke.</param>
        /// <param name="param">The data passed to the logic.</param>
        /// <returns>Data from the executed logic.</returns>
        public object Execute(string name, params dynamic[] param)
        {
            if (name.StartsWith("Set"))
                Log.Info($"[{Name}:Execute] {name} => {_avoidWallHugging}");

            if (name == "SetMoveRange")
            {
                _moveRange = (int)param[0];
                return null;
            }

            if (name == "GetMoveRange")
            {
                return _moveRange;
            }

            if (name == "SetSingleUseDistance")
            {
                _singleUseDistance = (int)param[0];
                return null;
            }

            if (name == "GetSingleUseDistance")
            {
                return _singleUseDistance;
            }

            if (name == "SetAvoidWallHugging")
            {
                _avoidWallHugging = (bool)param[0];
                return null;
            }

            if (name == "GetAvoidWallHugging")
            {
                return _avoidWallHugging;
            }

            if (name == "SetDoAdjustments")
            {
                _doAdjustments = (bool)param[0];
                return null;
            }

            if (name == "GetDoAdjustments")
            {
                return _doAdjustments;
            }

            if (name == "GetPathfindingCommand")
            {
                return _pathfindingCommand;
            }

            if (name == "SetNetworkingMode")
            {
                _networkingMode = (LokiPoe.ConfigManager.NetworkingType)param[0];
                return null;
            }

            return null;
        }

        /// <summary>
        /// Attempts to move towards a position. This function will perform pathfinding logic and take into consideration move distance
        /// to try and smoothly move towards a point.
        /// </summary>
        /// <param name="position">The position to move towards.</param>
        /// <param name="user">A user object passed.</param>
        /// <returns>true if the position was moved towards, and false if there was a pathfinding error.</returns>
        public bool MoveTowards(Vector2i position, params object[] user)
        {
            var myPosition = new Vector2i(LokiPoe.Me.Position.X, LokiPoe.Me.Position.Y);

            // Generate new paths in predictive more frequently to avoid back and forth issues from the new movement model
            // Bump the non-Predictive refresh rate threshold from 1k ms to 100 ms
            // 1000 -> 200 -> 100
            // TODO: I may want to make this configurable as well.
            _pathRefreshRate = _networkingMode == LokiPoe.ConfigManager.NetworkingType.Predictive ? 16 : 100;

            if ( _hasUsedMovementSkill || 
                 _pathfindingCommand?.Path == null || 
                 _pathfindingCommand.Path.Count == 0 || 
                 _pathfindingCommand.EndPoint != position || 
                 LokiPoe.CurrentWorldArea.IsTown ||
                 (_pathRefreshGovernanceStopwatch.IsRunning && _pathRefreshGovernanceStopwatch.ElapsedMilliseconds > _pathRefreshRate) || // New paths on interval
                 _pathfindingCommand.Path.Count <= 2 || // Not enough points
                 _pathfindingCommand.Path.All(p => myPosition.Distance(p) > 7))  // Try and find a better path to follow since we're off course
            {
                _pathfindingCommand = new PathfindingCommand(myPosition, position, 3, _avoidWallHugging);
                if (!ExilePather.FindPath(ref _pathfindingCommand))
                {
                    Log.Error($"[{Name}:MoveTowards] ExilePather.FindPath failed from {myPosition} to {position}.");
                    return false;
                }
                
                _pathRefreshGovernanceStopwatch.Restart();
            }

            // Don't try to custom move in these areas, or if we have the Karui Spirit.
            var currentWorldArea = LokiPoe.CurrentWorldArea;

            var canUseMoveSkill = !(currentWorldArea.IsTown || currentWorldArea.IsHideoutArea || currentWorldArea.IsMapRoom || LokiPoe.Me.HasKaruiSpirit || MiscHelpers.HasMinionFollower);
            if (canUseMoveSkill)
            {
                var movementSkill = PlayerMoverPlugin.GetUsableMovementSkill();
                // If we couldn't find a valid movement skill, un-set the cached slot # and indicate we can't use a movement skill.
                if (movementSkill == null)
                {
                    MoveSkillSlot = -1;
                    canUseMoveSkill = false;
                }
                else
                {
                    MoveSkillSlot = movementSkill.Slot;

                    // If our skill has charges (e.g. Flame Dash), ensure we don't deplete all of our charges on non-emergency movement.
                    if (movementSkill.CooldownsActive > 1)
                        canUseMoveSkill = false;
                }
            }

            // Eliminate points until we find one within a good moving range.
            while (_pathfindingCommand.Path.Count > 1)
            {
                var pathPoint = _pathfindingCommand.Path.First();
                if (!PlayerMoverPlugin.BlacklistedLocations.Contains(pathPoint) && pathPoint.Distance(myPosition) >= _moveRange)
                    break;

                _pathfindingCommand.Path.RemoveAt(0);
            }

            var pointOnPath = _pathfindingCommand.Path.First();
            pointOnPath += new Vector2i(LokiPoe.Random.Next(-2, 3), LokiPoe.Random.Next(-2, 3));

            // Adjust the point
            if (!currentWorldArea.IsTown && !currentWorldArea.IsHideoutArea && _doAdjustments)
            {
                var validNegativeAdjustments = 0;
                var validPositiveAdjustments = 0;

                var negativeAdjustedPoint = pointOnPath;
                var positiveAdjustedPoint = pointOnPath;

                for (var i = 0; i < 10; i++)
                {
                    negativeAdjustedPoint.X--;
                    if (!ExilePather.IsWalkable(negativeAdjustedPoint))
                    {
                        validNegativeAdjustments++;
                    }

                    positiveAdjustedPoint.X++;
                    if (!ExilePather.IsWalkable(positiveAdjustedPoint))
                    {
                        validPositiveAdjustments++;
                    }
                }

                if (validNegativeAdjustments > 5 && validPositiveAdjustments == 0)
                {
                    pointOnPath.X += 15;
                    Log.Info($"[{Name}:MoveTowards] Adjustments being made!");
                    _pathfindingCommand.Path[0] = pointOnPath;
                }
                else if (validPositiveAdjustments > 5 && validNegativeAdjustments == 0)
                {
                    pointOnPath.X -= 15;
                    Log.Info($"[{Name}:MoveTowards] Adjustments being made!");
                    _pathfindingCommand.Path[0] = pointOnPath;
                }
            }

            // Keep adjusting the point
            // Le sigh...
            if (currentWorldArea.IsTown && currentWorldArea.Act == 3)
            {
                var seed = LokiPoe.LocalData.AreaHash;
                if (_tgtSeed != seed || _tgts == null)
                {
                    Log.Info($"[{Name}:MoveTowards] Now building TGT info.");
                    _tgts = LokiPoe.TerrainData.TgtEntries;
                    _tgtSeed = seed;
                }
                if (TgtUnderPlayer.TgtName.Equals("Art/Models/Terrain/Act3Town/Act3_town_01_01_c16r7.tgt"))
                {
                    Log.Info($"[{Name}:MoveTowards] Act 3 Town force adjustment being made!");
                    pointOnPath.Y += 5;
                }
            }

            // If we think we used a movement skill but haven't actually moved, we're stuck.
            if (_hasUsedMovementSkill && _cachePosition == myPosition)
            {
                _hasUsedMovementSkill = false;
                _timesStuck ++;
            }

            // If we've tried too many times to hit the given point on path with no success, blacklist it and move on.
            if (_timesStuck >= _maxStucks)
            {
                PlayerMoverPlugin.BlacklistedLocations.Add(pointOnPath);

                _skillBlacklistStopwatch.Restart();
                _timesStuck = 0;
            }

            if (_cachePosition == myPosition)
            {
                // If we're blacklisting it, shouldn't we be NOT moving to it?
                // This appears to be some sort of attempt to prevent us from moving back to the original spot - 
                // "if we haven't moved from the starting point, add the destination as a blacklisted spot"
                // I think this is supposed to be part of the stuck handling logic. Moving it there.
                //PlayerMoverPlugin.BlacklistedLocations.Add(pointOnPath);
            }
            // If we've actually moved, clear the blacklist and move on.
            else
            {
                _timesStuck = 0;
                PlayerMoverPlugin.BlacklistedLocations.Clear();
            }

            // Cache actual position
            _cachePosition = myPosition;

            // Try to use move skill.
            var elapsedMilliseconds = _skillBlacklistStopwatch.ElapsedMilliseconds;
            if (elapsedMilliseconds <= 1500)
            {
                var remainingMilliseconds = 1500 - elapsedMilliseconds;
                Log.Info($"[{Name}:MoveTowards] skill is still blacklisted for {remainingMilliseconds}ms.");
            }
            else if (canUseMoveSkill)
            {
                var skillMovePoint = FindSkillPosition(MoveSkillSlot, position, _pathfindingCommand, PlayerMoverPlugin.BlacklistedLocations);
                if (skillMovePoint != Vector2i.Zero)
                {
                    var useMovementSkillResult = MoveHelper.UseMovementSkill(MoveSkillSlot, skillMovePoint);
                    if (useMovementSkillResult)
                    {
                        _hasUsedMovementSkill = true;
                        return true;
                    }
                }
            }
            
            var lastMoveSkill = LokiPoe.InGameState.SkillBarHud.LastBoundMoveSkill;
            if (lastMoveSkill == null)
            {
                Log.Error($"[{Name}:MoveTowards] Please assign the \"Move\" skill to your skillbar!");
                return false;
            }
            
            // If I'm in the middle of using a skill and am "mouse down"...
            var lastMoveSkillSlot = lastMoveSkill.Slots.Last();
            if (LokiPoe.Me.HasCurrentAction && (LokiPoe.ProcessHookManager.GetKeyState(lastMoveSkill.BoundKeys.Last()) & 0x8000) != 0)
            {
                // If I'm close enough to use it, use it at the exact point... otherwise just move the mcursor
                if (myPosition.Distance(position) < _singleUseDistance)
                {
                    LokiPoe.ProcessHookManager.ClearAllKeyStates();
                    LokiPoe.InGameState.SkillBarHud.UseAt(lastMoveSkillSlot, false, pointOnPath);
                }
                else
                {
                    LokiPoe.Input.SetMousePos(pointOnPath, false);
                }
            }
            else
            {
                LokiPoe.ProcessHookManager.ClearAllKeyStates();
                if (myPosition.Distance(position) < _singleUseDistance)
                {
                    LokiPoe.InGameState.SkillBarHud.UseAt(lastMoveSkillSlot, false, pointOnPath);
                }
                else
                {
                    LokiPoe.InGameState.SkillBarHud.BeginUseAt(lastMoveSkillSlot, false, pointOnPath);
                }
            }           
             
            return true;
        }

        private static Vector2i FindSkillPosition(int moveSkillSlot, Vector2i originalDestination, PathfindingCommand pathfindingCommand, List<Vector2i> blacklistedLocations)
        {
            var myPosition = LokiPoe.MyPosition;

            var skillName = LokiPoe.InGameState.SkillBarHud.Slot(moveSkillSlot).Name;

            var configuredMaxSkillRange = MoveHelper.GetSkillMaxRange(skillName);
            var configuredMinSkillRange = MoveHelper.GetSkillMinRange(skillName);

            var destinationVectors = pathfindingCommand.Path.Where(
                pathVector => !blacklistedLocations.Contains(pathVector) &&
                              pathVector.Distance(myPosition) <= configuredMaxSkillRange && pathVector.Distance(myPosition) <= originalDestination.Distance(myPosition) &&
                              pathVector.Distance(myPosition) >= configuredMinSkillRange &&
                              ExilePather.CanObjectSee(LokiPoe.Me, pathVector, true) && !Utility.IsAClosedDoorBetween(myPosition, pathVector, 10, 10, true))
                .OrderByDescending(x => ExilePather.PathDistance(myPosition, x, false, true))
                .ToList();

            if (!SkillAwarePlayerMoverSettings.Instance.IgnoreMobsValue) return destinationVectors.FirstOrDefault();

            var destinationVector = destinationVectors.FirstOrDefault(MoveHelper.IsPathToVectorSafe);
            return destinationVector;
        }
    }
}