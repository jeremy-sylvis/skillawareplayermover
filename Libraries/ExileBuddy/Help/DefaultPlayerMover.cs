﻿using System.Diagnostics;
using System.Linq;
using log4net;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;

namespace Loki.Bot
{
	/// <summary>
	/// The default player mover.
	/// </summary>
	public class DefaultPlayerMover : IPlayerMover
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();
		private PathfindingCommand _cmd;
		private readonly Stopwatch _sw = new Stopwatch();
		private bool _doAdjustments = false;
		private LokiPoe.ConfigManager.NetworkingType _networkingMode = LokiPoe.ConfigManager.NetworkingType.Unknown;
		private IndexedList<Vector2i> _originalPath;
		private int _moveRange = 13;
		private int _pathRefreshRate = 1000;
		private int _singleUseDistance = 25;
		private bool _avoidWallHugging = true;

		/// <summary>
		/// These are areas that always have issues with stock pathfinding, so adjustments will be made.
		/// </summary>
		private readonly string[] _forcedAdjustmentAreas = new[]
		{
			"The City of Sarn",
			"The Slums",
		};

		private LokiPoe.TerrainDataEntry[,] _tgts;
		private uint _tgtSeed;

		private LokiPoe.TerrainDataEntry TgtUnderPlayer
		{
			get
			{
				var myPos = LokiPoe.LocalData.MyPosition;
				return _tgts[myPos.X/23, myPos.Y/23];
			}
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			if (name == "SetMoveRange")
			{
				_moveRange = (int) param[0];
				Log.InfoFormat("[DefaultPlayerMover::Execute] {0} => {1}", name, _moveRange);
				return null;
			}

			if (name == "GetMoveRange")
			{
				return _moveRange;
			}

			if (name == "SetSingleUseDistance")
			{
				_singleUseDistance = (int) param[0];
				Log.InfoFormat("[DefaultPlayerMover::Execute] {0} => {1}", name, _singleUseDistance);
				return null;
			}

			if (name == "GetSingleUseDistance")
			{
				return _singleUseDistance;
			}

			if (name == "SetAvoidWallHugging")
			{
				_avoidWallHugging = (bool) param[0];
				Log.InfoFormat("[DefaultPlayerMover::Execute] {0} => {1}", name, _avoidWallHugging);
				return null;
			}

			if (name == "GetAvoidWallHugging")
			{
				return _avoidWallHugging;
			}

			if (name == "SetDoAdjustments")
			{
				_doAdjustments = (bool) param[0];
				Log.InfoFormat("[DefaultPlayerMover::Execute] {0} => {1}", name, _doAdjustments);
				return null;
			}

			if (name == "GetDoAdjustments")
			{
				return _doAdjustments;
			}

			if (name == "GetPathfindingCommand")
			{
				return _cmd;
			}

			if (name == "SetNetworkingMode")
			{
				_networkingMode = (LokiPoe.ConfigManager.NetworkingType) param[0];
				return null;
			}

			return null;
		}

		/// <summary>
		/// Attempts to move towards a position. This function will perform pathfinding logic and take into consideration move distance
		/// to try and smoothly move towards a point.
		/// </summary>
		/// <param name="position">The position to move towards.</param>
		/// <param name="user">A user object passed.</param>
		/// <returns>true if the position was moved towards, and false if there was a pathfinding error.</returns>
		public bool MoveTowards(Vector2i position, params dynamic[] user)
		{
			var myPosition = LokiPoe.MyPosition;

			if (_networkingMode == LokiPoe.ConfigManager.NetworkingType.Predictive)
			{
				// Generate new paths in predictive more frequently to avoid back and forth issues from the new movement model
				_pathRefreshRate = 16;
			}
			else
			{
				_pathRefreshRate = 1000;
			}

			if (
				_cmd == null || // No command yet
				_cmd.Path == null ||
				_cmd.EndPoint != position || // Moving to a new poisition
				LokiPoe.CurrentWorldArea.IsTown || // In town, always generate new paths
				(_sw.IsRunning && _sw.ElapsedMilliseconds > _pathRefreshRate) || // New paths on interval
				_cmd.Path.Count <= 2 || // Not enough points
				_cmd.Path.All(p => myPosition.Distance(p) > 7))
				// Try and find a better path to follow since we're off course
			{
				_cmd = new PathfindingCommand(myPosition, position, 3, _avoidWallHugging);
				if (!ExilePather.FindPath(ref _cmd))
				{
					Log.ErrorFormat("[DefaultPlayerMover::MoveTowards] ExilePather.FindPath failed from {0} to {1}.",
						myPosition, position);
					return false;
				}
				//Log.InfoFormat("[DefaultPlayerMover::MoveTowards] Finding new path.");
				_sw.Restart();
				_originalPath = new IndexedList<Vector2i>(_cmd.Path);
			}

			// Eliminate points until we find one within a good moving range.
			while (_cmd.Path.Count > 1)
			{
				if (_cmd.Path[0].Distance(myPosition) < _moveRange)
				{
					_cmd.Path.RemoveAt(0);
				}
				else
				{
					break;
				}
			}

			var point = _cmd.Path[0];

			point += new Vector2i(LokiPoe.Random.Next(-2, 3), LokiPoe.Random.Next(-2, 3));

			var cwa = LokiPoe.CurrentWorldArea;
			if (!cwa.IsTown && !cwa.IsHideoutArea && (_doAdjustments || _forcedAdjustmentAreas.Contains(cwa.Name)))
			{
				var negX = 0;
				var posX = 0;

				var tmp1 = point;
				var tmp2 = point;

				for (var i = 0; i < 10; i++)
				{
					tmp1.X--;
					if (!ExilePather.IsWalkable(tmp1))
					{
						negX++;
					}

					tmp2.X++;
					if (!ExilePather.IsWalkable(tmp2))
					{
						posX++;
					}
				}

				if (negX > 5 && posX == 0)
				{
					point.X += 15;
					Log.InfoFormat("[DefaultPlayerMover::MoveTowards] Adjustments being made!");
					_cmd.Path[0] = point;
				}
				else if (posX > 5 && negX == 0)
				{
					point.X -= 15;
					Log.InfoFormat("[DefaultPlayerMover::MoveTowards] Adjustments being made!");
					_cmd.Path[0] = point;
				}
			}

			// Le sigh...
			if (cwa.IsTown && cwa.Act == 3)
			{
				var seed = LokiPoe.LocalData.AreaHash;
				if (_tgtSeed != seed || _tgts == null)
				{
					Log.InfoFormat("[DefaultPlayerMover::MoveTowards] Now building TGT info.");
					_tgts = LokiPoe.TerrainData.TgtEntries;
					_tgtSeed = seed;
				}
				if (TgtUnderPlayer.TgtName.Equals("Art/Models/Terrain/Act3Town/Act3_town_01_01_c16r7.tgt"))
				{
					Log.InfoFormat("[DefaultPlayerMover::MoveTowards] Act 3 Town force adjustment being made!");
					point.Y += 5;
				}
			}

			var move = LokiPoe.InGameState.SkillBarHud.LastBoundMoveSkill;
			if (move == null)
			{
				Log.ErrorFormat("[DefaultPlayerMover::MoveTowards] Please assign the \"Move\" skill to your skillbar!");
				return false;
			}

			if ((LokiPoe.ProcessHookManager.GetKeyState(move.BoundKeys.Last()) & 0x8000) != 0 &&
				LokiPoe.Me.HasCurrentAction)
			{
				if (myPosition.Distance(position) < _singleUseDistance)
				{
					LokiPoe.ProcessHookManager.ClearAllKeyStates();
					LokiPoe.InGameState.SkillBarHud.UseAt(move.Slots.Last(), false, point);
					//Log.WarnFormat("[SkillBarHud.UseAt]");
				}
				else
				{
					LokiPoe.Input.SetMousePos(point, false);
					//Log.WarnFormat("[Input.SetMousePos]");
				}
			}
			else
			{
				LokiPoe.ProcessHookManager.ClearAllKeyStates();
				if (myPosition.Distance(position) < _singleUseDistance)
				{
					LokiPoe.InGameState.SkillBarHud.UseAt(move.Slots.Last(), false, point);
					//Log.WarnFormat("[SkillBarHud.UseAt]");
				}
				else
				{
					LokiPoe.InGameState.SkillBarHud.BeginUseAt(move.Slots.Last(), false, point);
					//Log.WarnFormat("[BeginUseAt]");
				}
			}

			return true;
		}
	}
}