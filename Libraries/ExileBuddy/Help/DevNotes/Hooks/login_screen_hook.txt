﻿"login_screen_hook" is a hook implemented in OldGrindBot. 

It will be invoked when the bot is in the login screen (LokiPoe.IsInLoginScreen).

Upon invocation, all enabled plugins (via PluginManager.OrderedEnabledPlugins) will have their Logic coroutine executed like so:
	await plugin.Logic("login_screen_hook")

If the plugin implements logic to handle the hook, it should return a bool with a value of true.
If the plugin does not implement logic to handle the hook, it should return a bool with the value of false.

Only 1 plugin can process this hook at a time, so it's first come, first served.