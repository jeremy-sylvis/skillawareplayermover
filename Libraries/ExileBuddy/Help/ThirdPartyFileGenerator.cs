using System;
using System.IO;
using System.Text;

namespace ThirdPartyFileGenerator
{
	class Program
	{
		static void Main(string[] args)
		{
			var sb = new StringBuilder();
			var baseDir = AppDomain.CurrentDomain.BaseDirectory;
			foreach (var file in Directory.GetFiles(baseDir, "*.*", SearchOption.AllDirectories))
			{
				if (file.ToLowerInvariant().Contains("3rdparty"))
					continue;

				var ext = Path.GetExtension(file).ToLowerInvariant();
				if (ext.Equals(".dll") || ext.Equals(".exe"))
					continue;

				sb.Append(file.Replace(baseDir, ""));
				sb.AppendLine();
			}
			File.WriteAllText("3rdparty-files.txt", sb.ToString());
		}
	}
}