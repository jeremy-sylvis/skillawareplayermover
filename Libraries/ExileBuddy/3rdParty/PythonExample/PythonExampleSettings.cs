﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using Loki;
using Loki.Bot;
using Loki.Common;
using Newtonsoft.Json;

namespace nsPythonExample
{
	/// <summary>Settings for the Dev tab. </summary>
	public class PythonExampleSettings : JsonSettings
	{
		private static PythonExampleSettings _instance;

		/// <summary>The current instance for this class. </summary>
		public static PythonExampleSettings Instance
		{
			get
			{
				return _instance ?? (_instance = new PythonExampleSettings());
			}
		}

		private string _code;

		/// <summary>The default ctor. Will use the settings path "PythonExample".</summary>
		public PythonExampleSettings()
			: base(GetSettingsFilePath(Configuration.Instance.Name, string.Format("{0}.json", "PythonExample")))
		{
		}

		/// <summary>The data in the Text control.</summary>
		[DefaultValue(
			"import sys\r\nsys.stdout=ioproxy\r\n\r\ndef Execute():\r\n\tprint LokiPoe.Me.Name"
			)]
		public string Code
		{
			get
			{
				return _code;
			}
			set
			{
				if (value.Equals(_code))
				{
					return;
				}
				_code = value;
				NotifyPropertyChanged(() => Code);
			}
		}
	}
}
