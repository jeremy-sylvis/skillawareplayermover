﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using log4net;
using Loki;
using Loki.Common;
using Loki.Game;
using Loki.Game.Objects;
using Microsoft.Win32;

namespace nsPythonExample
{
	/// <summary>
	/// Interaction logic for Gui.xaml
	/// </summary>
	public partial class Gui : UserControl
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private readonly PythonExample _pythonExample;

		internal Gui(PythonExample @this)
		{
			InitializeComponent();

			_pythonExample = @this;
		}

		private void ExecutePythonButton_Click(object sender, RoutedEventArgs e)
		{
			using (LokiPoe.AcquireFrame())
			{
				// For total control, GetStatement logic is this:
				var scope = _pythonExample.ScriptManager.Scope;
				var scriptSource =
					_pythonExample.ScriptManager.Engine.CreateScriptSourceFromString(PyInputTextBox.Text);
				scope.SetVariable("ioproxy", _pythonExample.ScriptManager.IoProxy);
				scriptSource.Execute(scope);
				scope.GetVariable<Action>("Execute")();
			}
		}
	}
}