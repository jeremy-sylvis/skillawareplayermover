﻿using System;

namespace EXtensions
{
    public static class Events
    {
        public static event EventHandler<AreaChangedArgs> AreaChanged;
        public static event EventHandler<AreaChangedArgs> CombatAreaChanged;
        public static event Action PlayerDied;

        internal static void RaiseAreaChangedEvent(AreaChangedArgs args)
        {
            var handler = AreaChanged;
            handler?.Invoke(null, args);
        }

        internal static void RaiseCombatAreaChangedEvent(AreaChangedArgs e)
        {
            var handler = CombatAreaChanged;
            handler?.Invoke(null, e);
        }

        internal static void RaisePlayerDiedEvent()
        {
            var handler = PlayerDied;
            handler?.Invoke();
        }
    }
}