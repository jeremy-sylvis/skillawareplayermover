﻿using System.Linq;
using Loki.Bot;

namespace EXtensions
{
    public static class BotStructure
    {
        public static TaskManager TaskManager
        {
            get
            {
                var taskManager = (TaskManager) BotManager.CurrentBot.Execute("GetTaskManager");
                if (taskManager != null) return taskManager;
                GlobalLog.Error("[BotStructure] GetTaskManager returned null.");
                BotManager.Stop();
                return null;
            }
        }

        public static void AddTask(ITask task, string name, TaskPosition type)
        {
            bool added = false;
            switch (type)
            {
                case TaskPosition.Before:
                    added = TaskManager.AddBefore(task, name);
                    break;

                case TaskPosition.After:
                    added = TaskManager.AddAfter(task, name);
                    break;

                case TaskPosition.Replace:
                    added = TaskManager.Replace(name, task);
                    break;

                default:
                    GlobalLog.Error($"[BotStructure] Unknown task add type \"{type}\".");
                    break;
            }
            if (!added)
            {
                GlobalLog.Error($"[BotStructure] Fail to add \"{name}\".");
                BotManager.Stop();
            }
        }

        public static void RemoveTask(string name)
        {
            if (!TaskManager.Remove(name))
            {
                GlobalLog.Error($"[BotStructure] Fail to remove \"{name}\".");
                BotManager.Stop();
            }
        }

        public static IPlugin GetPlugin(string name)
        {
            return PluginManager.Plugins.FirstOrDefault(p => p.Name == name);
        }

        public static IPlugin GetEnabledPlugin(string name)
        {
            return PluginManager.EnabledPlugins.FirstOrDefault(p => p.Name == name);
        }

        public static bool PluginEnabled(string name)
        {
            return PluginManager.EnabledPlugins.Any(p => p.Name == name);
        }

        public static void CheckIncompatiblePlugins(string caller, params string[] incompatibles)
        {
            bool found = false;
            foreach (var incompatible in incompatibles)
            {
                if (PluginManager.EnabledPlugins.Any(p => p.Name == incompatible))
                {
                    GlobalLog.Error($"[{caller}] Detected incompatible plugin \"{incompatible}\".");
                    found = true;
                }
            }
            if (found) BotManager.Stop();
        }
    }
}