﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.Objects;
using Cursor = Loki.Game.LokiPoe.InGameState.CursorItemOverlay;
using InventoryUi = Loki.Game.LokiPoe.InGameState.InventoryUi;
using StashUi = Loki.Game.LokiPoe.InGameState.StashUi;

namespace EXtensions
{
    public static class Inventories
    {
        public static async Task<bool> OpenStash()
        {
            WalkablePosition stashPos;
            if (LokiPoe.Me.IsInTown)
            {
                stashPos = StaticPositions.GetStashPosByAct();
            }
            else
            {
                var stashObj = LokiPoe.ObjectManager.Stash;
                if (stashObj == null)
                {
                    GlobalLog.Error("[OpenStash] Fail to find any Stash nearby.");
                    return false;
                }
                stashPos = stashObj.WalkablePosition();
            }
            await stashPos.ComeAtOnce();
            if (!await PlayerAction.Interact(LokiPoe.ObjectManager.Stash, () => StashUi.IsOpened && StashUi.StashTabInfo != null, "stash opening")) return false;
            await Wait.SleepSafe(200);
            return true;
        }

        public static async Task<bool> OpenStashTab(string tabName)
        {
            if (!StashUi.IsOpened)
            {
                if (!await OpenStash()) return false;
            }

            if (StashUi.TabControl.CurrentTabName != tabName)
            {
                GlobalLog.Debug($"[OpenStashTab] Now switching to tab \"{tabName}\".");
                int currentTabId = StashUi.StashTabInfo.InventoryId;
                var isSwitched = StashUi.TabControl.SwitchToTabMouse(tabName);
                if (isSwitched != SwitchToTabResult.None)
                {
                    GlobalLog.Error($"[OpenStashTab] Fail to switch to tab \"{tabName}\". Error \"{isSwitched}\".");
                    return false;
                }
                if (!await Wait.For(() =>
                        StashUi.StashTabInfo != null &&
                        StashUi.StashTabInfo.InventoryId != currentTabId,
                    "stash tab switching")) return false;

                await Wait.SleepSafe(200);
            }
            return true;
        }

        private static async Task<bool> NextStashTab()
        {
            int currentTabId = StashUi.StashTabInfo.InventoryId;
            var switched = StashUi.TabControl.NextTabKeyboard();
            if (switched != SwitchToTabResult.None)
            {
                GlobalLog.Error($"[NextStashTab] Fail to switch to next stash tab. Error \"{switched}\".");
                return false;
            }
            if (!await Wait.For(() =>
                    StashUi.StashTabInfo != null &&
                    StashUi.StashTabInfo.InventoryId != currentTabId,
                "stash tab switching")) return false;

            await Coroutines.ReactionWait();

            return true;
        }

        public static async Task<Item> FindItemInStash(Func<Item> findItem, bool critcal)
        {
            if (!StashUi.StashTabInfo.IsPremiumCurrency)
            {
                var item = findItem();
                if (item != null) return item;
            }
            if (StashUi.TabControl.IsOnFirstTab)
            {
                if (!await NextStashTab())
                {
                    GlobalLog.Error("[FindItemInStash] Fail to switch to next stash tab.");
                    return null;
                }
            }
            else
            {
                if (!await OpenStashTab(StashUi.TabControl.TabNames[0]))
                {
                    GlobalLog.Error("[FindItemInStash] Fail to switch to first stash tab.");
                    return null;
                }
            }

            while (true)
            {
                if (!StashUi.StashTabInfo.IsPremiumCurrency)
                {
                    GlobalLog.Info($"[FindItemInStash] Looking in \"{StashUi.TabControl.CurrentTabName}\" tab.");
                    var item = findItem();
                    if (item != null) return item;
                }

                if (StashUi.TabControl.IsOnLastTab) break;

                if (!await NextStashTab())
                {
                    GlobalLog.Error("[FindItemInStash] Fail to switch to next stash tab.");
                    return null;
                }
            }

            GlobalLog.Error("[FindItemInStash] Fail to find requested item in entire stash.");
            if (critcal) BotManager.Stop();
            return null;
        }

        public static async Task<bool> StashToFirstAvailableTab(Vector2i itemPos)
        {
            if (!StashUi.IsOpened)
            {
                if (!await OpenStash()) return false;
            }

            var item = InventoryUi.InventoryControl_Main.Inventory.FindItemByPos(itemPos);
            if (item == null)
            {
                GlobalLog.Error($"[StashToFirstAvailableTab] Fail to find item at {itemPos}.");
                return false;
            }

            var size = item.Size;
            var name = string.Copy(item.FullName);

            if (!StashUi.StashTabInfo.IsPremiumCurrency && !StashUi.StashTabInfo.IsRemoveOnly)
            {
                GlobalLog.Debug($"[StashToFirstAvailableTab] Now stashing \"{name}\" to \"{StashUi.TabControl.CurrentTabName}\" name.");
                if (StashUi.InventoryControl.Inventory.CanFitItem(size))
                {
                    return await FastMoveFromInventory(itemPos);
                }
                GlobalLog.Error($"[StashToFirstAvailableTab] \"{StashUi.TabControl.CurrentTabName}\" has not enough free space.");
            }
            if (StashUi.TabControl.IsOnFirstTab)
            {
                if (!await NextStashTab()) return false;
            }
            else
            {
                if (!await OpenStashTab(StashUi.TabControl.TabNames[0])) return false;
            }

            while (true)
            {
                if (!StashUi.StashTabInfo.IsPremiumCurrency && !StashUi.StashTabInfo.IsRemoveOnly)
                {
                    GlobalLog.Debug($"[StashToFirstAvailableTab] Now stashing \"{name}\" to \"{StashUi.TabControl.CurrentTabName}\" name.");
                    if (StashUi.InventoryControl.Inventory.CanFitItem(size))
                    {
                        return await FastMoveFromInventory(itemPos);
                    }
                    GlobalLog.Error($"[StashToFirstAvailableTab] \"{StashUi.TabControl.CurrentTabName}\" has not enough free space.");
                }
                if (StashUi.TabControl.IsOnLastTab) break;
                if (!await NextStashTab()) return false;
            }

            GlobalLog.Error("[StashToFirstAvailableTab] Stash is full. Now stopping the bot because it cannot continue");
            BotManager.Stop();
            return false;
        }

        public static async Task<bool> OpenInventory()
        {
            if (InventoryUi.IsOpened &&
                !LokiPoe.InGameState.PurchaseUi.IsOpened &&
                !LokiPoe.InGameState.SellUi.IsOpened) return true;

            await Coroutines.CloseBlockingWindows();
            LokiPoe.Input.SimulateKeyEvent(LokiPoe.Input.Binding.open_inventory_panel, true, false, false);
            if (!await Wait.For(() => InventoryUi.IsOpened, "inventory panel opening")) return false;
            if (Settings.Instance.GlobalUseReactionWait) await Coroutines.ReactionWait();
            return true;
        }

        public static List<Vector2i> GetExcessCurrency(string itemName)
        {
            var items = InventoryUi.InventoryControl_Main.Inventory.Items
                .Where(i => i.Name == itemName)
                .ToList();

            if (items.Count > 1)
            {
                return items
                    .OrderByDescending(i => i.StackCount)
                    .Skip(1)
                    .Select(i => i.LocationTopLeft)
                    .ToList();
            }
            return null;
        }

        public static bool StashTabCanFitItem(Vector2i itemPos)
        {
            var item = InventoryUi.InventoryControl_Main.Inventory.FindItemByPos(itemPos);
            if (item == null)
            {
                GlobalLog.Error($"[StashCanFitItem] Fail to find item at {itemPos} in player's inventory.");
                return false;
            }
            if (StashUi.StashTabInfo.IsPremiumCurrency)
            {
                var name = item.Name;
                var stackCount = item.StackCount;
                var control = GetCurrencyControl(name);
                if (control != null && control.CanHold(name, stackCount)) return true;
                return StashUi.CurrencyTabInventoryControlsMisc.Any(miscControl => miscControl.CanHold(name, stackCount));
            }
            return StashUi.InventoryControl.Inventory.CanFitItem(item.Size);
        }

        public static InventoryControlWrapper GetControlWithCurrency(string currencyName)
        {
            var control = GetCurrencyControl(currencyName);
            if (control?.CurrencyTabItem != null) return control;
            return StashUi.CurrencyTabInventoryControlsMisc.FirstOrDefault(c => c.CurrencyTabItem?.Name == currencyName);
        }

        #region Fast moving

        public static async Task<bool> FastMoveFromInventory(Vector2i itemPos)
        {
            var item = InventoryUi.InventoryControl_Main.Inventory.FindItemByPos(itemPos);
            if (item == null)
            {
                GlobalLog.Error($"[FastMoveFromInventory] Fail to find item at {itemPos} in player's inventory.");
                return false;
            }

            var itemName = item.FullName;
            GlobalLog.Debug($"[FastMoveFromInventory] Fast moving \"{itemName}\" at {itemPos} from player's inventory.");

            var moved = InventoryUi.InventoryControl_Main.FastMove(item.LocalId);
            if (moved != FastMoveResult.None)
            {
                GlobalLog.Error($"[FastMoveFromInventory] Fast move error: \"{moved}\".");
                return false;
            }

            if (await Wait.For(() => InventoryUi.InventoryControl_Main.Inventory.FindItemByPos(itemPos) == null, "fast move"))
            {
                GlobalLog.Debug($"[FastMoveFromInventory] \"{itemName}\" at {itemPos} has been successfully fast moved from player's inventory.");
                if (Settings.Instance.GlobalUseReactionWait) await Coroutines.ReactionWait();
                return true;
            }
            GlobalLog.Error($"[FastMoveFromInventory] Fast move timeout for \"{itemName}\" at {itemPos} in player's inventory.");
            return false;
        }

        public static async Task<bool> FastMoveToVendor(Vector2i itemPos)
        {
            var item = InventoryUi.InventoryControl_Main.Inventory.FindItemByPos(itemPos);
            if (item == null)
            {
                GlobalLog.Error($"[FastMoveToVendor] Fail to find item at {itemPos} in player's inventory.");
                return false;
            }

            var itemName = item.FullName;
            GlobalLog.Debug($"[FastMoveToVendor] Fast moving \"{itemName}\" at {itemPos} from player's inventory.");

            var moved = InventoryUi.InventoryControl_Main.FastMove(item.LocalId);
            if (moved != FastMoveResult.None && moved != FastMoveResult.ItemTransparent)
            {
                GlobalLog.Error($"[FastMoveToVendor] Fast move error: \"{moved}\".");
                return false;
            }

            if (await Wait.For(() =>
            {
                var movedItem = InventoryUi.InventoryControl_Main.Inventory.FindItemByPos(itemPos);
                if (movedItem == null)
                {
                    GlobalLog.Error("[FastMoveToVendor] Unexpected error. Item became null instead of transparent.");
                    return false;
                }
                return InventoryUi.InventoryControl_Main.IsItemTransparent(movedItem.LocalId);
            }, "fast move"))
            {
                GlobalLog.Debug($"[FastMoveToVendor] \"{itemName}\" at {itemPos} has been successfully fast moved from player's inventory.");
                if (Settings.Instance.GlobalUseReactionWait) await Coroutines.ReactionWait();
                return true;
            }
            GlobalLog.Error($"[FastMoveToVendor] Fast move timeout for \"{itemName}\" at {itemPos} in player's inventory.");
            return false;
        }

        public static async Task<bool> FastMoveFromStashTab(Vector2i itemPos)
        {
            var tabName = StashUi.TabControl.CurrentTabName;
            var item = StashUi.InventoryControl.Inventory.FindItemByPos(itemPos);
            if (item == null)
            {
                GlobalLog.Error($"[FastMoveFromStashTab] Fail to find item at {itemPos} in \"{tabName}\" tab.");
                return false;
            }

            var itemName = item.FullName;
            GlobalLog.Debug($"[FastMoveFromStashTab] Fast moving \"{itemName}\" at {itemPos} from \"{tabName}\" tab.");

            var moved = StashUi.InventoryControl.FastMove(item.LocalId);
            if (moved != FastMoveResult.None)
            {
                GlobalLog.Error($"[FastMoveFromStashTab] Fast move error: \"{moved}\".");
                return false;
            }

            if (await Wait.For(() => StashUi.InventoryControl.Inventory.FindItemByPos(itemPos) == null, "fast move"))
            {
                GlobalLog.Debug($"[FastMoveFromStashTab] \"{itemName}\" at {itemPos} has been successfully fast moved from \"{tabName}\" tab.");
                if (Settings.Instance.GlobalUseReactionWait) await Coroutines.ReactionWait();
                return true;
            }
            GlobalLog.Error($"[FastMoveFromStashTab] Fast move timeout for \"{itemName}\" at {itemPos} in \"{tabName}\" tab.");
            return false;
        }

        public static async Task<bool> FastMoveFromCurrencyTab(InventoryControlWrapper currencyControl)
        {
            if (currencyControl == null)
            {
                GlobalLog.Error("[FastMoveFromCurrencyTab] Currency inventory control is null.");
                return false;
            }
            var item = currencyControl.CurrencyTabItem;
            if (item == null)
            {
                GlobalLog.Error("[FastMoveFromCurrencyTab] Currency inventory control is empty.");
                return false;
            }

            var itemName = item.Name;
            var stackCount = item.StackCount;
            var tabName = StashUi.TabControl.CurrentTabName;
            GlobalLog.Debug($"[FastMoveFromCurrencyTab] Fast moving \"{itemName}\" from \"{tabName}\" tab.");

            var moved = currencyControl.FastMove();
            if (moved != FastMoveResult.None)
            {
                GlobalLog.Error($"[FastMoveFromCurrencyTab] Fast move error: \"{moved}\".");
                return false;
            }
            if (await Wait.For(() =>
            {
                var i = currencyControl.CurrencyTabItem;
                return i == null || i.StackCount < stackCount;
            }, "fast move"))
            {
                GlobalLog.Debug($"[FastMoveFromCurrencyTab] \"{itemName}\" has been successfully fast moved from \"{tabName}\" tab.");
                if (Settings.Instance.GlobalUseReactionWait) await Coroutines.ReactionWait();
                return true;
            }
            GlobalLog.Error($"[FastMoveFromCurrencyTab] Fast move timeout for \"{itemName}\" in \"{tabName}\" tab.");
            return false;
        }

        #endregion

        #region Extension methods

        public static int ItemAmount(this Inventory inventory, string itemName)
        {
            int amount = 0;
            foreach (var item in inventory.Items)
            {
                if (item.Name == itemName) amount += item.StackCount;
            }
            return amount;
        }

        public static async Task<bool> PickItemToCursor(this InventoryControlWrapper inventory, Vector2i itemPos, bool rightClick = false)
        {
            var item = inventory.Inventory.FindItemByPos(itemPos);
            if (item == null)
            {
                GlobalLog.Error($"[PickItemToCursor] Cannot find item at {itemPos}");
                return false;
            }

            GlobalLog.Debug($"[PickItemToCursor] Now going to pick \"{item.Name}\" at {itemPos} to cursor.");
            int id = item.LocalId;

            if (rightClick)
            {
                var used = inventory.UseItem(id);
                if (used != UseItemResult.None)
                {
                    GlobalLog.Error($"[PickItemToCursor] Fail to pick item to cursor. Error: \"{used}\".");
                    return false;
                }
            }
            else
            {
                var picked = inventory.Pickup(id);
                if (picked != PickupResult.None)
                {
                    GlobalLog.Error($"[PickItemToCursor] Fail to pick item to cursor. Error: \"{picked}\".");
                    return false;
                }
            }
            if (!await Wait.For(() => Cursor.Item != null, "item appear under cursor")) return false;
            return true;
        }

        public static async Task<bool> PlaceItemFromCursor(this InventoryControlWrapper inventory, Vector2i pos)
        {
            var cursorItem = Cursor.Item;
            if (cursorItem == null)
            {
                GlobalLog.Error("[PlaceItemFromCursor] Cursor item is null.");
                return false;
            }

            GlobalLog.Debug($"[PlaceItemFromCursor] Now going to place \"{cursorItem.Name}\" from cursor to {pos}.");

            //apply item on another item, if we are in VirtualUse mode
            if (Cursor.Mode == LokiPoe.InGameState.CursorItemModes.VirtualUse)
            {
                var destItem = inventory.Inventory.FindItemByPos(pos);
                if (destItem == null)
                {
                    GlobalLog.Error("[PlaceItemFromCursor] Destination item is null.");
                    return false;
                }
                int destItemId = destItem.LocalId;
                var applied = inventory.ApplyCursorTo(destItem.LocalId);
                if (applied != ApplyCursorResult.None)
                {
                    GlobalLog.Error($"[PlaceItemFromCursor] Fail to place item from cursor. Error: \"{applied}\".");
                    return false;
                }
                //wait for destination item change, it cannot become null, ID should change
                return await Wait.For(() =>
                {
                    var item = inventory.Inventory.FindItemByPos(pos);
                    return item != null && item.LocalId != destItemId;
                }, "destination item change");
            }

            //in other cases, place item in empty inventory or swap it with another item
            int cursorItemId = cursorItem.LocalId;
            var placed = inventory.PlaceCursorInto(pos.X, pos.Y, true);
            if (placed != PlaceCursorIntoResult.None)
            {
                GlobalLog.Error($"[PlaceItemFromCursor] Fail to place item from cursor. Error: \"{placed}\".");
                return false;
            }

            //wait for cursor item change, if we placed - it should become null, if we swapped - ID should change
            if (!await Wait.For(() =>
            {
                var item = Cursor.Item;
                return item == null || item.LocalId != cursorItemId;
            }, "cursor item change")) return false;

            if (Settings.Instance.GlobalUseReactionWait) await Coroutines.ReactionWait();
            return true;
        }

        #endregion

        #region Helpers/private

        private static bool CanHold(this InventoryControlWrapper control, string itemName, int amount)
        {
            var item = control.CurrencyTabItem;
            return item == null || (item.Name == itemName && item.StackCount + amount <= 5000);
        }

        private static InventoryControlWrapper GetCurrencyControl(string currencyName)
        {
            Func<InventoryControlWrapper> getControl;
            return CurrencyControlMap.TryGetValue(currencyName, out getControl) ? getControl() : null;
        }

        private static readonly Dictionary<string, Func<InventoryControlWrapper>> CurrencyControlMap = new Dictionary<string, Func<InventoryControlWrapper>>
        {
            [CurrencyNames.ScrollFragment] = () => StashUi.InventoryControl_ScrollFragment,
            [CurrencyNames.TransmutationShard] = () => StashUi.InventoryControl_TransmutationShard,
            [CurrencyNames.AlterationShard] = () => StashUi.InventoryControl_AlterationShard,
            [CurrencyNames.AlchemyShard] = () => StashUi.InventoryControl_AlchemyShard,
            [CurrencyNames.Wisdom] = () => StashUi.InventoryControl_ScrollOfWisdom,
            [CurrencyNames.Portal] = () => StashUi.InventoryControl_PortalScroll,
            [CurrencyNames.Transmutation] = () => StashUi.InventoryControl_OrbOfTransmutation,
            [CurrencyNames.Augmentation] = () => StashUi.InventoryControl_OrbOfAugmentation,
            [CurrencyNames.Alteration] = () => StashUi.InventoryControl_OrbOfAlteration,
            [CurrencyNames.Scrap] = () => StashUi.InventoryControl_ArmourersScrap,
            [CurrencyNames.Whetstone] = () => StashUi.InventoryControl_BlacksmithsWhetstone,
            [CurrencyNames.Glassblower] = () => StashUi.InventoryControl_GlassblowersBauble,
            [CurrencyNames.Chisel] = () => StashUi.InventoryControl_CartographersChisel,
            [CurrencyNames.Chromatic] = () => StashUi.InventoryControl_ChromaticOrb,
            [CurrencyNames.Chance] = () => StashUi.InventoryControl_OrbOfChance,
            [CurrencyNames.Alchemy] = () => StashUi.InventoryControl_OrbOfAlchemy,
            [CurrencyNames.Jeweller] = () => StashUi.InventoryControl_JewellersOrb,
            [CurrencyNames.Fusing] = () => StashUi.InventoryControl_OrbOfFusing,
            [CurrencyNames.Scouring] = () => StashUi.InventoryControl_OrbOfScouring,
            [CurrencyNames.Blessed] = () => StashUi.InventoryControl_BlessedOrb,
            [CurrencyNames.Regal] = () => StashUi.InventoryControl_RegalOrb,
            [CurrencyNames.Chaos] = () => StashUi.InventoryControl_ChaosOrb,
            [CurrencyNames.Vaal] = () => StashUi.InventoryControl_VaalOrb,
            [CurrencyNames.Regret] = () => StashUi.InventoryControl_OrbOfRegret,
            [CurrencyNames.Gemcutter] = () => StashUi.InventoryControl_GemcuttersPrism,
            [CurrencyNames.Divine] = () => StashUi.InventoryControl_DivineOrb,
            [CurrencyNames.Exalted] = () => StashUi.InventoryControl_ExaltedOrb,
            [CurrencyNames.Mirror] = () => StashUi.InventoryControl_MirrorOfKalandra,
            [CurrencyNames.SilverCoin] = () => StashUi.InventoryControl_SilverCoin,
            [CurrencyNames.SextantApprentice] = () => StashUi.InventoryControl_ApprenticeSextant,
            [CurrencyNames.SextantJourneyman] = () => StashUi.InventoryControl_JourneymanSextant,
            [CurrencyNames.SextantMaster] = () => StashUi.InventoryControl_MasterSextant,
        };

        #endregion
    }
}