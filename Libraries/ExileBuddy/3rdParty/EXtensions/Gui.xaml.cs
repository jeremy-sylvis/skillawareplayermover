﻿using System.Windows;
using System.Windows.Controls;

namespace EXtensions
{
    /// <summary>
    /// Interaction logic for Gui.xaml
    /// </summary>
    public partial class Gui : UserControl
    {
        public Gui()
        {
            InitializeComponent();
            if (CurrencyRulesCheckbox.IsChecked != null && CurrencyRulesCheckbox.IsChecked.Value)
            {
                CurrencyRulesExpander.IsExpanded = true;
            }
        }

        private void CurrencyRulesCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            if (CurrencyRulesExpander != null) CurrencyRulesExpander.IsExpanded = true;
        }

        private void CurrencyRulesCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (CurrencyRulesExpander != null) CurrencyRulesExpander.IsExpanded = false;
        }
    }
}