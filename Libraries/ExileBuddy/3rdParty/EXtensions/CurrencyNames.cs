﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using Loki.Game.GameData;

namespace EXtensions
{
    [SuppressMessage("ReSharper", "UnassignedReadonlyField")]
    public static class CurrencyNames
    {
        [ItemMetadata("Metadata/Items/Currency/CurrencyIdentificationShard")]
        public static readonly string ScrollFragment;

        [ItemMetadata("Metadata/Items/Currency/CurrencyUpgradeToMagicShard")]
        public static readonly string TransmutationShard;

        [ItemMetadata("Metadata/Items/Currency/CurrencyRerollMagicShard")]
        public static readonly string AlterationShard;

        [ItemMetadata("Metadata/Items/Currency/CurrencyUpgradeToRareShard")]
        public static readonly string AlchemyShard;

        [ItemMetadata("Metadata/Items/Currency/CurrencyIdentification")]
        public static readonly string Wisdom;

        [ItemMetadata("Metadata/Items/Currency/CurrencyPortal")]
        public static readonly string Portal;

        [ItemMetadata("Metadata/Items/Currency/CurrencyUpgradeToMagic")]
        public static readonly string Transmutation;

        [ItemMetadata("Metadata/Items/Currency/CurrencyAddModToMagic")]
        public static readonly string Augmentation;

        [ItemMetadata("Metadata/Items/Currency/CurrencyRerollMagic")]
        public static readonly string Alteration;

        [ItemMetadata("Metadata/Items/Currency/CurrencyArmourQuality")]
        public static readonly string Scrap;

        [ItemMetadata("Metadata/Items/Currency/CurrencyWeaponQuality")]
        public static readonly string Whetstone;

        [ItemMetadata("Metadata/Items/Currency/CurrencyFlaskQuality")]
        public static readonly string Glassblower;

        [ItemMetadata("Metadata/Items/Currency/CurrencyMapQuality")]
        public static readonly string Chisel;

        [ItemMetadata("Metadata/Items/Currency/CurrencyRerollSocketColours")]
        public static readonly string Chromatic;

        [ItemMetadata("Metadata/Items/Currency/CurrencyUpgradeRandomly")]
        public static readonly string Chance;

        [ItemMetadata("Metadata/Items/Currency/CurrencyUpgradeToRare")]
        public static readonly string Alchemy;

        [ItemMetadata("Metadata/Items/Currency/CurrencyRerollSocketNumbers")]
        public static readonly string Jeweller;

        [ItemMetadata("Metadata/Items/Currency/CurrencyRerollSocketLinks")]
        public static readonly string Fusing;

        [ItemMetadata("Metadata/Items/Currency/CurrencyConvertToNormal")]
        public static readonly string Scouring;

        [ItemMetadata("Metadata/Items/Currency/CurrencyRerollImplicit")]
        public static readonly string Blessed;

        [ItemMetadata("Metadata/Items/Currency/CurrencyUpgradeMagicToRare")]
        public static readonly string Regal;

        [ItemMetadata("Metadata/Items/Currency/CurrencyRerollRare")]
        public static readonly string Chaos;

        [ItemMetadata("Metadata/Items/Currency/CurrencyCorrupt")]
        public static readonly string Vaal;

        [ItemMetadata("Metadata/Items/Currency/CurrencyPassiveRefund")]
        public static readonly string Regret;

        [ItemMetadata("Metadata/Items/Currency/CurrencyGemQuality")]
        public static readonly string Gemcutter;

        [ItemMetadata("Metadata/Items/Currency/CurrencyModValues")]
        public static readonly string Divine;

        [ItemMetadata("Metadata/Items/Currency/CurrencyAddModToRare")]
        public static readonly string Exalted;

        [ItemMetadata("Metadata/Items/Currency/CurrencyImprintOrb")]
        public static readonly string Eternal;

        [ItemMetadata("Metadata/Items/Currency/CurrencyDuplicate")]
        public static readonly string Mirror;

        [ItemMetadata("Metadata/Items/Currency/CurrencyPerandusCoin")]
        public static readonly string PerandusCoin;

        [ItemMetadata("Metadata/Items/Currency/CurrencySilverCoin")]
        public static readonly string SilverCoin;

        [ItemMetadata("Metadata/Items/Currency/CurrencyAddAtlasMod")]
        public static readonly string SextantApprentice;

        [ItemMetadata("Metadata/Items/Currency/CurrencyAddAtlasModMid")]
        public static readonly string SextantJourneyman;

        [ItemMetadata("Metadata/Items/Currency/CurrencyAddAtlasModHigh")]
        public static readonly string SextantMaster;

        static CurrencyNames()
        {
            Dictionary<string, FieldInfo> namesMap = new Dictionary<string, FieldInfo>();
            foreach (var field in typeof (CurrencyNames).GetFields())
            {
                var metadataAttribute = field.GetCustomAttribute<ItemMetadata>();
                if (metadataAttribute == null) continue;
                namesMap.Add(metadataAttribute.Metadata, field);
            }

            int processed = 0;
            int total = namesMap.Count;
            foreach (var item in Dat.BaseItemTypes)
            {
                if (processed >= total) break;
                FieldInfo field;
                if (namesMap.TryGetValue(item.Metadata, out field))
                {
                    field.SetValue(null, item.Name);
                    ++processed;
                }
            }
        }

        [AttributeUsage(AttributeTargets.Field)]
        public class ItemMetadata : Attribute
        {
            public ItemMetadata(string metadata)
            {
                Metadata = metadata;
            }

            public string Metadata { get; set; }
        }
    }
}