﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Buddy.Coroutines;
using Loki.Bot;
using Loki.Game;

namespace EXtensions
{
    public static class Wait
    {
        public static async Task<bool> For(Func<bool> condition, string desc, int step = 100, int timeout = 5000)
        {
            return await For(condition, desc, () => step, timeout);
        }

        public static async Task<bool> For(Func<bool> condition, string desc, Func<int> step, int timeout = 5000)
        {
            if (condition()) return true;
            Stopwatch timer = Stopwatch.StartNew();
            while (timer.ElapsedMilliseconds < timeout)
            {
                await Coroutine.Sleep(step());
                GlobalLog.Debug($"[WaitFor] Waiting for {desc} ({Math.Round(timer.ElapsedMilliseconds/1000f, 2)}/{timeout/1000f})");
                if (condition()) return true;
            }
            GlobalLog.Error($"[WaitFor] Wait for {desc} timeout.");
            return false;
        }

        public static async Task<bool> ForAreaChange(uint areaHash, int timeout = 40000)
        {
            return await For(() => LokiPoe.LocalData.AreaHash != areaHash, "area change", 500, timeout);
        }

        public static async Task Sleep(int ms)
        {
            await Coroutine.Sleep(ms);
        }

        public static async Task SleepSafe(int ms)
        {
            await Coroutine.Sleep(Math.Max(LatencyTracker.Current, ms));
        }

        public static async Task SleepSafe(int min, int max)
        {
            int latency = LatencyTracker.Current;
            if (latency >= max)
            {
                await Coroutine.Sleep(latency);
            }
            else
            {
                await Coroutine.Sleep(LokiPoe.Random.Next(min, max));
            }
        }
    }
}