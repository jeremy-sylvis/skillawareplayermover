﻿namespace EXtensions
{
    public enum TaskPosition
    {
        Before,
        After,
        Replace
    }

    public enum ItemTypes
    {
        Unknown,
        Currency,
        Armor,
        Weapon,
        Belt,
        Quiver,
        Ring,
        Amulet,
        Flask,
        Gem,
        Map,
        MapFragment,
        Jewel,
        DivinationCard,
        Quest,
        LabyrinthKey,
        FishingRod
    }

    public enum ArmorTypes
    {
        NonArmor,
        BodyArmor,
        Helmet,
        Gloves,
        Boots,
        Shield
    }

    public enum WeaponTypes
    {
        NonWeapon,
        Dagger,
        Claw,
        Bow,
        Wand,
        Staff,
        Axe1H,
        Axe2H,
        Mace1H,
        Sceptre,
        Mace2H,
        Sword1H,
        SwordThrusting,
        Sword2H
    }

    public enum WeaponHandTypes
    {
        NonWeapon,
        OneHanded,
        TwoHanded
    }

    public enum FlaskTypes
    {
        NonFlask,
        Life,
        Mana,
        Hybrid,
        Utility
    }
}