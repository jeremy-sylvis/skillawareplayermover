﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace EXtensions
{
    public class ArenaExplorer
    {
        private readonly List<TransitionEntry> _transitions = new List<TransitionEntry>();
        private TransitionEntry _current;
        private bool _bossRoomReached;

        private static Monster Daresso => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Daresso_King_of_Swords)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        private static Monster MapBoss => LokiPoe.ObjectManager
            .GetObjects(
                LokiPoe.ObjectManager.PoeObjectEnum.Avatar_of_the_Forge,
                LokiPoe.ObjectManager.PoeObjectEnum.Avatar_of_the_Huntress,
                LokiPoe.ObjectManager.PoeObjectEnum.Avatar_of_the_Skies)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        public void Tick()
        {
            foreach (var transition in LokiPoe.ObjectManager.Objects.OfType<AreaTransition>().Where(t => t.TransitionType == TransitionTypes.Local))
            {
                if (_transitions.Any(t => t.Id == transition.Id)) continue;
                Register(transition, Types.Next);
            }
            if (_current == null)
            {
                var next = GetClosestByType(Types.Next);
                if (next != null)
                {
                    _current = next;
                }
            }
        }

        public async Task<bool> Execute()
        {
            if (_bossRoomReached) return false;

            if (_current != null)
            {
                var pos = _current.Position;
                if (!pos.PathExists)
                {
                    Loki.Bot.Explorer.GetCurrent().Reset();
                    _current = null;
                    return true;
                }

                if (pos.IsFar)
                {
                    pos.Come();
                }
                else
                {
                    if (await PlayerAction.TakeTransition(_current.Object))
                    {
                        PostEnter();
                    }
                    else
                    {
                        ErrorManager.ReportError();
                    }
                }
                return true;
            }

            if (!await ExplorationLogic.Execute(95))
            {
                GlobalLog.Warn("[ArenaExplorer] It seems we are in the dead end room. Now going to the previous arena level.");

                var back = GetClosestByType(Types.Back);
                if (back == null)
                {
                    GlobalLog.Error("[ArenaExplorer] There is no suitable back transition.");
                    return false;
                }
                _current = back;
            }
            return true;
        }

        public void Reset()
        {
            _transitions.Clear();
            _current = null;
            _bossRoomReached = false;
            Explorer.GetCurrent().Reset();
        }

        private void PostEnter()
        {
            Loki.Bot.Explorer.GetCurrent().Reset();

            if (LokiPoe.CurrentWorldArea.IsOverworldArea)
            {
                if (Daresso != null)
                {
                    GlobalLog.Warn("[ArenaExplorer] Daresso detected. Boss room has been reached.");
                    _bossRoomReached = true;
                    return;
                }
            }
            else if (LokiPoe.CurrentWorldArea.IsMap)
            {
                if (MapBoss != null)
                {
                    GlobalLog.Warn("[ArenaExplorer] Map boss detected. Boss room has been reached.");
                    _bossRoomReached = true;
                    return;
                }
            }

            if (_current.Type == Types.Next)
            {
                MarkBackTransition();
            }
            else
            {
                MarkDeadEndTransition();
            }
            _current = null;
        }

        private void Register(AreaTransition transition, Types type)
        {
            var pos = transition.WalkablePosition();
            if (!pos.Initialize())
            {
                type = Types.Unwalkable;
            }
            GlobalLog.Debug($"[ArenaExplorer] Registeting area transition {pos}. Type: {type}.");
            _transitions.Add(new TransitionEntry(transition.Id, type, pos));
        }

        private void MarkBackTransition()
        {
            var closest = LokiPoe.ObjectManager.Objects
                .OfType<AreaTransition>()
                .OrderBy(t => t.Distance)
                .FirstOrDefault();

            if (closest == null)
            {
                GlobalLog.Error("[ArenaExplorer] Unexpected error. There is no back area transition nearby.");
                return;
            }

            var id = closest.Id;
            var registered = _transitions.FirstOrDefault(t => t.Id == id);
            if (registered != null)
            {
                if (registered.Type != Types.Back)
                {
                    registered.Type = Types.Back;
                    GlobalLog.Debug($"[ArenaExplorer] {registered.Position} has been marked as back transition.");
                }
            }
            else
            {
                Register(closest, Types.Back);
            }
        }

        private void MarkDeadEndTransition()
        {
            var closest = _transitions.OrderBy(t => t.Position.DistanceSqr).First();
            closest.Type = Types.DeadEnd;
            GlobalLog.Warn($"[ArenaExplorer] {closest.Position} has been marked as dead end transition.");
        }

        private TransitionEntry GetClosestByType(Types type)
        {
            return _transitions
                .Where(t => t.Type == type)
                .OrderBy(t => t.Position.DistanceSqr)
                .Take(3)
                .FirstOrDefault(t => t.Position.PathExists);
        }

        private class TransitionEntry
        {
            public int Id { get; }
            public Types Type { get; set; }
            public WalkablePosition Position { get; }

            public AreaTransition Object => LokiPoe.ObjectManager.Objects.OfType<AreaTransition>().FirstOrDefault(t => t.Id == Id);

            public TransitionEntry(int id, Types type, WalkablePosition position)
            {
                Id = id;
                Type = type;
                Position = position;
            }
        }

        private enum Types
        {
            Next,
            Back,
            DeadEnd,
            Unwalkable
        }
    }
}