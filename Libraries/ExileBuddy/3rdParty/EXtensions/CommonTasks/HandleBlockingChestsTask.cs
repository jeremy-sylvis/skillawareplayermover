﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.Objects;

namespace EXtensions.CommonTasks
{
    public class HandleBlockingChestsTask : ITask
    {
        private readonly HashSet<int> _processed = new HashSet<int>();

        public static bool Enabled;

        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type == "core_area_changed_event")
            {
                Reset();
                return true;
            }

            if (type != "task_execute")
                return false;

            if (!Enabled || LokiPoe.Me.IsDead || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout || LokiPoe.Me.IsInMapRoom)
                return false;

            var list = LokiPoe.ObjectManager.Objects.OfType<Chest>().ToList();
            var chests = list.Where(
                    c => c.Distance <= 10 && !c.IsOpened && c.IsStompable && !_processed.Contains(c.Id))
                .OrderBy(c => c.Distance)
                .ToList();

            if (chests.Count < 2 && !chests.Any(c => c.Distance < 8))
            {
                Enabled = false; //run this task only once, by StuckDetection demand
                return false;
            }

            LokiPoe.ProcessHookManager.Reset();

            await Coroutines.CloseBlockingWindows();

            GlobalLog.Info("[HandleBlockingChests] There are breakable chest clusters close by. We need to break them in order to avoid getting blocked.");

            LokiPoe.Input.SetMousePos(LokiPoe.MyWorldPosition);

            await Coroutines.LatencyWait();

            if (LokiPoe.InGameState.CurrentTarget != null)
            {
                GlobalLog.Info($"[HandleBlockingChests] The object {LokiPoe.InGameState.CurrentTarget.Id} is under the cursor. Now interacting with it.");

                LokiPoe.Input.ClickLMB();

                await Coroutines.FinishCurrentAction(false);
            }

            var positions1 = new List<Vector2i>
            {
                LokiPoe.MyPosition
            };
            var positions2 = new List<Vector2>
            {
                LokiPoe.MyWorldPosition
            };

            foreach (var chest in chests)
            {
                _processed.Add(chest.Id);
                positions1.Add(chest.Position);
                positions2.Add(chest.WorldPosition);
            }

            foreach (var position in positions1)
            {
                LokiPoe.Input.SetMousePos(position);

                await Coroutines.LatencyWait();

                if (LokiPoe.InGameState.CurrentTarget != null)
                {
                    GlobalLog.Info($"[HandleBlockingChests] The object {LokiPoe.InGameState.CurrentTarget.Id} is under the cursor. Now interacting with it.");

                    LokiPoe.Input.ClickLMB();

                    await Coroutines.LatencyWait();

                    await Coroutines.FinishCurrentAction(false);
                }
            }

            foreach (var position in positions2)
            {
                LokiPoe.Input.SetMousePos(position);

                await Coroutines.LatencyWait();

                if (LokiPoe.InGameState.CurrentTarget != null)
                {
                    GlobalLog.Info($"[HandleBlockingChests] The object {LokiPoe.InGameState.CurrentTarget.Id} is under the cursor. Now interacting with it.");

                    LokiPoe.Input.ClickLMB();

                    await Coroutines.LatencyWait();

                    await Coroutines.FinishCurrentAction(false);
                }
            }

            return true;
        }

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public void Start()
        {
            Reset();
        }

        public void Tick()
        {
        }

        public void Stop()
        {
        }

        private void Reset()
        {
            GlobalLog.Info("[HandleBlockingChestsTask] Now resetting task state.");
            _processed.Clear();
        }

        public string Name => "HandleBlockingChestsTask";
        public string Description => "This task will handle breaking any blocking chests that interfere with movement.";
        public string Author => "Bossland GmbH";
        public string Version => "0.0.1.1";
    }
}