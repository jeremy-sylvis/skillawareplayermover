﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Game;
using Loki.Game.Objects;

namespace EXtensions.CommonTasks
{
    public class OpenChestTask : ITask
    {
        private const int OpenChestRange = 50;
        private const int OpenChestRangeEarly = 15;

        //there is no range check for strogboxes in non-early mode
        private const int StrongboxOpenRangeEarly = 25;

        private readonly bool _early;
        private readonly int _chestOpenRange;

        private KeyValuePair<int, WalkablePosition>? _currentChest;
        private KeyValuePair<int, WalkablePosition>? _currentStrongbox;
        private KeyValuePair<int, WalkablePosition>? _currentShrine;

        public OpenChestTask(bool early)
        {
            _early = early;
            _chestOpenRange = early ? OpenChestRangeEarly : OpenChestRange;
        }

        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type == "core_area_changed_event")
            {
                _currentChest = null;
                _currentStrongbox = null;
                _currentShrine = null;
                return true;
            }

            if (type != "task_execute") return false;

            if (LokiPoe.Me.IsDead || !LokiPoe.CurrentWorldArea.IsCombatArea())
                return false;

            if (CombatAreaCache.Current.ChestPositions.Count > 0)
            {
                if (_currentChest != null || InitCurrentChest())
                {
                    await ProcessChest();
                    return true;
                }
            }

            //take shrines before strongboxes to have the shrine-buff advantage upon opening strongbox
            if (!_early)
            {
                if (CombatAreaCache.Current.ShrinePositions.Count > 0)
                {
                    await ProcessShrine();
                    return true;
                }
            }

            if (Settings.Instance.OpenStrongboxes)
            {
                if (CombatAreaCache.Current.StrongboxPositions.Count > 0)
                {
                    if (_currentStrongbox != null || InitCurrentStrongbox())
                    {
                        await ProcessStrongbox();
                        return true;
                    }
                }
            }

            return false;
        }

        private async Task ProcessChest()
        {
            // ReSharper disable once PossibleInvalidOperationException
            var chestPos = _currentChest.Value.Value;

            //stop processing if other logic (combat/looting) moved us too far away from current chest
            if (chestPos.Distance > _chestOpenRange + 10)
            {
                _currentChest = null;
                return;
            }

            var id = _currentChest.Value.Key;

            var chestObj = LokiPoe.ObjectManager.GetObjectById<Chest>(id);
            if (chestObj != null && chestObj.IsOpened)
            {
                ClearCurrentChest(false);
                return;
            }
            if (chestPos.IsFar)
            {
                chestPos.Come();
                return;
            }
            if (chestObj == null)
            {
                ClearCurrentChest(false);
                return;
            }
            if (await PlayerAction.Interact(chestObj, () => chestObj.IsOpened, "chest opening", 500))
            {
                ClearCurrentChest(false);
            }
            else
            {
                GlobalLog.Error($"[OpenChestTask] Fail to open \"{chestPos.Name}\". Now blacklisting it.");
                ClearCurrentChest(true, "fail to interact");
            }
        }

        private async Task ProcessStrongbox()
        {
            // ReSharper disable once PossibleInvalidOperationException
            var boxPos = _currentStrongbox.Value.Value;

            var id = _currentStrongbox.Value.Key;

            var boxObj = LokiPoe.ObjectManager.GetObjectById<Chest>(id);
            if (boxObj != null && (boxObj.IsOpened || boxObj.IsLocked))
            {
                ClearCurrentStrongbox(false);
                return;
            }
            if (boxPos.IsFar)
            {
                boxPos.Come();
                return;
            }
            if (boxObj == null)
            {
                ClearCurrentStrongbox(false);
                return;
            }
            if (await PlayerAction.Interact(boxObj, () => boxObj.IsLocked || boxObj.IsOpened, "strongbox opening"))
            {
                ClearCurrentStrongbox(false);
            }
            else
            {
                GlobalLog.Error($"[OpenChestTask] Fail to open \"{boxPos.Name}\". Now blacklisting it.");
                ClearCurrentStrongbox(true, "fail to interact");
            }
        }

        private async Task ProcessShrine()
        {
            if (_currentShrine == null)
            {
                _currentShrine = CombatAreaCache.Current.ShrinePositions.OrderBy(kvp => kvp.Value.DistanceSqr).First();
            }

            var shrinePos = _currentShrine.Value.Value;
            var id = _currentShrine.Value.Key;

            if (!shrinePos.IsInitialized)
            {
                if (!shrinePos.Initialize())
                {
                    ClearCurrentShrine(true, "no walkable position");
                    return;
                }
            }
            var shrineObj = LokiPoe.ObjectManager.GetObjectById<Shrine>(id);
            if (shrineObj != null && (shrineObj.IsDeactivated || Blacklist.Contains(id)))
            {
                ClearCurrentShrine(false);
                return;
            }
            if (shrinePos.IsFar)
            {
                shrinePos.Come();
                return;
            }
            if (shrineObj == null)
            {
                ClearCurrentShrine(false);
                return;
            }
            if (await PlayerAction.Interact(shrineObj, () => shrineObj.IsDeactivated, "shrine activation"))
            {
                ClearCurrentShrine(false);
            }
            else
            {
                GlobalLog.Error($"[OpenChestTask] Fail to take \"{shrinePos.Name}\". Now blacklisting it.");
                ClearCurrentShrine(true, "fail to interact");
            }
        }

        private bool InitCurrentChest()
        {
            var closestChest = CombatAreaCache.Current.ChestPositions.OrderBy(kvp => kvp.Value.DistanceSqr).First();
            var chestPos = closestChest.Value;
            if (chestPos.Distance > _chestOpenRange) return false;
            if (!chestPos.IsInitialized)
            {
                if (!chestPos.Initialize())
                {
                    Blacklist.Add(closestChest.Key, TimeSpan.FromDays(1), "no walkable position");
                    CombatAreaCache.Current.ChestPositions.Remove(closestChest.Key);
                    return false;
                }
            }
            if (chestPos.PathDistance > _chestOpenRange + 5) return false;
            _currentChest = closestChest;
            return true;
        }

        private bool InitCurrentStrongbox()
        {
            var closestBox = CombatAreaCache.Current.StrongboxPositions.OrderBy(kvp => kvp.Value.DistanceSqr).First();
            var boxPos = closestBox.Value;
            if (_early && boxPos.Distance > StrongboxOpenRangeEarly) return false;
            if (!boxPos.IsInitialized)
            {
                if (!boxPos.Initialize())
                {
                    Blacklist.Add(closestBox.Key, TimeSpan.FromDays(1), "no walkable position");
                    CombatAreaCache.Current.StrongboxPositions.Remove(closestBox.Key);
                    return false;
                }
            }
            if (_early && boxPos.PathDistance > StrongboxOpenRangeEarly + 5) return false;
            _currentStrongbox = closestBox;
            return true;
        }

        public void ClearCurrentChest(bool blacklist, string reason = null)
        {
            if (_currentChest == null) return;

            var id = _currentChest.Value.Key;
            CombatAreaCache.Current.ChestPositions.Remove(id);
            _currentChest = null;

            if (blacklist) Blacklist.Add(id, TimeSpan.FromDays(1), reason ?? "");
        }

        public void ClearCurrentStrongbox(bool blacklist, string reason = null)
        {
            if (_currentStrongbox == null) return;

            var id = _currentStrongbox.Value.Key;
            CombatAreaCache.Current.StrongboxPositions.Remove(id);
            _currentStrongbox = null;

            if (blacklist) Blacklist.Add(id, TimeSpan.FromDays(1), reason ?? "");
        }

        public void ClearCurrentShrine(bool blacklist, string reason = null)
        {
            if (_currentShrine == null) return;

            var id = _currentShrine.Value.Key;
            CombatAreaCache.Current.ShrinePositions.Remove(id);
            _currentShrine = null;

            if (blacklist) Blacklist.Add(id, TimeSpan.FromDays(1), reason ?? "");
        }

        #region Unused interface methods

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public void Tick()
        {
        }

        public void Enable()
        {
        }

        public void Disable()
        {
        }

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public string Name => $"OpenChestTask{(_early ? "(Early)" : "")}";
        public string Description => "Task for opening chests, strongboxes and moving to shrines.";
        public string Author => "ExVault";
        public string Version => "1.0";

        #endregion
    }
}