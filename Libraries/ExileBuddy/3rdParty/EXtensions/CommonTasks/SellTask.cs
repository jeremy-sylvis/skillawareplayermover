﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;

namespace EXtensions.CommonTasks
{
    public class SellTask : ITask
    {
        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type != "task_execute") return false;
            if (!LokiPoe.Me.IsInTown && !LokiPoe.Me.IsInHideout) return false;

            var itemsToSell = new List<Vector2i>();
            var itemFilter = ItemEvaluator.Instance;

            foreach (var item in LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.Items)
            {
                if (item.Rarity == Rarity.Quest || item.HasMicrotransitionAttachment || item.HasSkillGemsEquipped)
                    continue;

                IItemFilter dummyFilter;

                if (!itemFilter.Match(item, EvaluationType.Sell, out dummyFilter))
                    continue;

                if (itemFilter.Match(item, EvaluationType.Save, out dummyFilter))
                    continue;

                itemsToSell.Add(item.LocationTopLeft);
            }

            if (Settings.Instance.SellExcessPortals)
            {
                var excessPortals = Inventories.GetExcessCurrency(CurrencyNames.Portal);
                if (excessPortals != null)
                {
                    foreach (var portal in excessPortals)
                    {
                        itemsToSell.Add(portal);
                    }
                }
            }

            if (itemsToSell.Count == 0)
            {
                GlobalLog.Info("[SellTask] No items to sell.");
                return false;
            }

            GlobalLog.Info($"[SmartSellTask] {itemsToSell.Count} items to sell.");

            if (!await TownNpcs.SellItems(itemsToSell)) ErrorManager.ReportError();

            return true;
        }

        #region Unused interface methods

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public void Tick()
        {
        }


        public void Enable()
        {
        }

        public void Disable()
        {
        }

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public string Name => "SellTask";

        public string Description => "Task that handles item selling.";

        public string Author => "ExVault";

        public string Version => "1.0";

        #endregion
    }
}