﻿using System.Threading.Tasks;
using Loki.Bot;

namespace EXtensions.CommonTasks
{
    public class FallbackTask : ITask
    {
        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type != "task_execute") return false;

            GlobalLog.Error("[FallbackTask] The Fallback task is executing. The bot does not know what to do.");
            await Wait.Sleep(200);
            return true;
        }

        #region Unused inteface methods

        public void Start()
        {
        }

        public void Tick()
        {
        }

        public void Stop()
        {
        }

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public string Name => "FallbackTask";
        public string Description => "This task is the last task executed. It should not execute.";
        public string Author => "Bossland GmbH";
        public string Version => "1.0";

        #endregion
    }
}