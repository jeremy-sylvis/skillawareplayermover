﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Game;
using Loki.Game.Objects;

namespace EXtensions.CommonTasks
{
    public class ReturnAfterTownrunTask : ITask
    {
        public static bool Enabled;

        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type != "task_execute") return false;
            if (!LokiPoe.Me.IsInTown && !LokiPoe.Me.IsInHideout) return false;
            if (!Enabled) return false;

            await StaticPositions.GetCommonPortalSpotByAct().ComeAtOnce(distance: 10);

            var portalObj = LokiPoe.LocalData.TownPortals.FirstOrDefault(p => p.NetworkObject.IsTargetable && p.OwnerName == LokiPoe.Me.Name);
            if (portalObj == null)
            {
                GlobalLog.Error("[ReturnAfterTownrunTask] There is no portal to enter.");
                Enabled = false;
                return true;
            }

            var portal = portalObj.NetworkObject as Portal;
            await portal.WalkablePosition().ComeAtOnce();

            if (!await PlayerAction.TakePortal(portal))
            {
                ErrorManager.ReportError();
                return true;
            }
            Enabled = false;
            return true;
        }

        #region Unused interface methods

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public void Tick()
        {
        }


        public void Enable()
        {
        }

        public void Disable()
        {
        }

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public string Name => "ReturnAfterTownrunTask";

        public string Description => "Task for returning to overworld area after townrun.";

        public string Author => "ExVault";

        public string Version => "1.0";

        #endregion
    }
}