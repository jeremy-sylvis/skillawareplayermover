﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using InventoryUi = Loki.Game.LokiPoe.InGameState.InventoryUi;
using StashUi = Loki.Game.LokiPoe.InGameState.StashUi;

namespace EXtensions.CommonTasks
{
    public class StashTask : ITask
    {
        private static bool _checkInvalidTabs = true;
        private static bool _checkFullTabs = true;

        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type != "task_execute") return false;
            if (!LokiPoe.Me.IsInTown && !LokiPoe.Me.IsInHideout) return false;

            var itemsToStash = new List<ItemForStashing>();

            foreach (var item in InventoryUi.InventoryControl_Main.Inventory.Items)
            {
                var compositeType = item.CompositeType();
                var itemType = compositeType.ItemType;
                var name = item.Name;

                if (itemType == ItemTypes.Quest)
                    continue;

                if (itemType == ItemTypes.Currency && (name == CurrencyNames.Wisdom || name == CurrencyNames.Portal))
                    continue;

                itemsToStash.Add(new ItemForStashing(itemType, item.LocationTopLeft, name, item.RarityLite()));
            }

            var excessWisdoms = Inventories.GetExcessCurrency(CurrencyNames.Wisdom);
            if (excessWisdoms != null)
            {
                foreach (var wisdomPos in excessWisdoms)
                {
                    itemsToStash.Add(new ItemForStashing(ItemTypes.Currency, wisdomPos, CurrencyNames.Wisdom, Rarity.Currency));
                }
            }

            if (!Settings.Instance.SellExcessPortals)
            {
                var excessPortals = Inventories.GetExcessCurrency(CurrencyNames.Portal);
                if (excessPortals != null)
                {
                    foreach (var portalPos in excessPortals)
                    {
                        itemsToStash.Add(new ItemForStashing(ItemTypes.Currency, portalPos, CurrencyNames.Portal, Rarity.Currency));
                    }
                }
            }

            if (itemsToStash.Count == 0)
            {
                GlobalLog.Info("[StashTask] No items to stash.");
                return false;
            }

            if (_checkInvalidTabs)
            {
                if (!await Inventories.OpenStash())
                {
                    ErrorManager.ReportError();
                    return true;
                }
                var wrongTabs = GetNonexistentTabs();
                if (wrongTabs.Count > 0)
                {
                    GlobalLog.Error("[StashTask] The following tabs are specified in stashing rules, but do not exist in stash:");
                    GlobalLog.Error($"{string.Join(" ,", wrongTabs)}");
                    GlobalLog.Error("[StashTask] Please provide correct tab names.");
                    BotManager.Stop();
                    return true;
                }
                GlobalLog.Debug("[StashTask] All tabs specified in stashing rules exist in stash.");
                _checkInvalidTabs = false;
            }

            if (_checkFullTabs)
            {
                if (Settings.Instance.FullTabs.Count > 0)
                {
                    var cleanedTabs = new List<string>();
                    foreach (var tab in Settings.Instance.FullTabs)
                    {
                        if (!await Inventories.OpenStashTab(tab))
                        {
                            ErrorManager.ReportError();
                            return true;
                        }
                        var inventory = StashUi.InventoryControl.Inventory;
                        if (inventory.InventorySpacePercent >= 25 &&
                            inventory.CanFitItem(new Vector2i(2, 4)))
                        {
                            GlobalLog.Warn($"[StashTask] Full tab check: \"{tab}\" tab is not full. Bot will stash items to this tab again.");
                            cleanedTabs.Add(tab);
                        }
                        else
                        {
                            GlobalLog.Warn($"[StashTask] Full tab check: \"{tab}\" tab is still full.");
                        }
                    }
                    foreach (var tab in cleanedTabs)
                    {
                        Settings.Instance.FullTabs.Remove(tab);
                    }
                }
                _checkFullTabs = false;
            }

            GlobalLog.Info($"[SmartStashTask] {itemsToStash.Count} items to stash.");

            AssignStashTabs(itemsToStash);

            foreach (var item in itemsToStash.OrderBy(i => i.StashTab).ThenBy(i => i.Position, Position.Comparer.Instance))
            {
                GlobalLog.Debug($"[StashTask] Now going to stash \"{item.Name}\" to \"{item.StashTab}\" tab.");

                if (!await Inventories.OpenStashTab(item.StashTab))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                if (!Inventories.StashTabCanFitItem(item.Position))
                {
                    GlobalLog.Warn($"[StashTask] Cannot fit \"{item.Name}\" to \"{item.StashTab}\" tab. Now marking that tab as full.");
                    Settings.Instance.FullTabs.Add(item.StashTab);
                    return true;
                }
                if (!await Inventories.FastMoveFromInventory(item.Position))
                {
                    ErrorManager.ReportError();
                    return true;
                }
            }
            await Coroutines.CloseBlockingWindows();
            return true;
        }

        private static void AssignStashTabs(IEnumerable<ItemForStashing> items)
        {
            foreach (var item in items)
            {
                if (item.Type == ItemTypes.Currency)
                {
                    //essences are currency, but have their own stashing catogory
                    var name = item.Name;
                    if (name.Contains("Essence") || name == "Remnant of Corruption")
                    {
                        item.AssignStashTab(Settings.Instance.GetTabForCategory(Settings.StashingCategory.Essence));
                        continue;
                    }
                    item.AssignStashTab(Settings.Instance.GetTabForCurrency(name));
                    continue;
                }

                if (item.Type == ItemTypes.Gem)
                {
                    item.AssignStashTab(Settings.Instance.GetTabForCategory(Settings.StashingCategory.Gem));
                    continue;
                }

                if (item.Type == ItemTypes.DivinationCard)
                {
                    item.AssignStashTab(Settings.Instance.GetTabForCategory(Settings.StashingCategory.Card));
                    continue;
                }

                if (item.Type == ItemTypes.Jewel)
                {
                    item.AssignStashTab(Settings.Instance.GetTabForCategory(Settings.StashingCategory.Jewel));
                    continue;
                }

                if (item.Type == ItemTypes.Map)
                {
                    item.AssignStashTab(Settings.Instance.GetTabForCategory(Settings.StashingCategory.Map));
                    continue;
                }

                if (item.Type == ItemTypes.MapFragment)
                {
                    item.AssignStashTab(Settings.Instance.GetTabForCategory(Settings.StashingCategory.Fragment));
                    continue;
                }

                if (item.Rarity == Rarity.Rare)
                {
                    item.AssignStashTab(Settings.Instance.GetTabForCategory(Settings.StashingCategory.Rare));
                    continue;
                }

                if (item.Rarity == Rarity.Unique)
                {
                    item.AssignStashTab(Settings.Instance.GetTabForCategory(Settings.StashingCategory.Unique));
                    continue;
                }

                GlobalLog.Error($"[StashTask] Cannot determine stash tab for \"{item.Name}\". It will be stashed to \"Other\" tabs.");
                item.AssignStashTab(Settings.Instance.GetTabForCategory(Settings.StashingCategory.Other));
            }
        }

        private static List<string> GetNonexistentTabs()
        {
            var result = new List<string>();
            var actualTabs = StashUi.TabControl.TabNames;

            foreach (var rule in Settings.Instance.GeneralStashingRules)
            {
                foreach (var tab in rule.TabList)
                {
                    if (!actualTabs.Contains(tab))
                    {
                        result.Add(tab);
                    }
                }
            }
            foreach (var rule in Settings.Instance.CurrencyStashingRules)
            {
                foreach (var tab in rule.TabList)
                {
                    if (!actualTabs.Contains(tab))
                    {
                        result.Add(tab);
                    }
                }
            }
            return result;
        }

        public static void RequestInvalidTabCheck()
        {
            _checkInvalidTabs = true;
        }

        private class ItemForStashing
        {
            public ItemTypes Type { get; }
            public Vector2i Position { get; }
            public string Name { get; }
            public Rarity Rarity { get; }
            public string StashTab { get; private set; }

            public ItemForStashing(ItemTypes type, Vector2i position, string name, Rarity rarity)
            {
                Type = type;
                Position = position;
                Name = name;
                Rarity = rarity;
            }

            public void AssignStashTab(string tabName)
            {
                StashTab = tabName;
            }
        }

        #region Unused inteface methods

        public void Tick()
        {
        }

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }


        public void Enable()
        {
        }

        public void Disable()
        {
        }


        public string Name => "StashTask";

        public string Description => "Task that handles item stashing.";

        public string Author => "ExVault";

        public string Version => "1.0";

        #endregion
    }
}