﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Loki.Bot;
using Loki.Game;
using Loki.Game.Objects;

namespace EXtensions.CommonTasks
{
    public class HandleDoorsTask : ITask
    {
        private const int MaxOpenAttempts = 7;

        private int _lastDoorId;
        private int _lastDoorAttempts;

        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type == "core_area_changed_event")
            {
                _lastDoorId = 0;
                _lastDoorAttempts = 0;
                return true;
            }

            if (type != "task_execute")
                return false;

            if (LokiPoe.Me.IsDead || !LokiPoe.CurrentWorldArea.IsCombatArea())
                return false;

            var areaName = AreaNames.Current;
            if (areaName == AreaNames.Harvest || areaName == AreaNames.BellyOfTheBeast2)
            {
                var transition = BlockingTransitionBelly;
                if (transition != null)
                {
                    if (!await PlayerAction.Interact(transition, 2)) ErrorManager.ReportError();
                    return true;
                }
            }
            if (areaName == AreaNames.DaressoDream)
            {
                var transition = BlockingTransitionPit;
                if (transition != null)
                {
                    if (!await PlayerAction.Interact(transition, 2)) ErrorManager.ReportError();
                    return true;
                }
            }

            var door = ClosestDoor;

            if (door == null) return false;

            var id = door.Id;

            if (_lastDoorId == id)
            {
                ++_lastDoorAttempts;
                if (_lastDoorAttempts >= MaxOpenAttempts)
                {
                    Blacklist.Add(id, TimeSpan.FromSeconds(30), "Fail to open.");
                    _lastDoorAttempts = 0;
                    return true;
                }
            }
            else
            {
                _lastDoorAttempts = 0;
                _lastDoorId = id;
            }

            //in most cases, at 15 distance we can interact with object no matter what
            //so, we can (safely?) omit all pathfinding checks
            await Coroutines.FinishCurrentAction();
            await PlayerAction.EnableAlwaysHighlight();
            await Coroutines.InteractWith(door);

            return true;
        }

        private static TriggerableBlockage ClosestDoor
        {
            get
            {
                return LokiPoe.ObjectManager.Doors
                    .Where(d => !d.IsOpened && d.Distance <= 15 && !Blacklist.Contains(d.Id))
                    .OrderBy(d => d.Distance)
                    .FirstOrDefault();
            }
        }

        private static NetworkObject BlockingTransitionPit
        {
            get
            {
                return LokiPoe.ObjectManager.Objects
                    .FirstOrDefault(o => o.Distance <= 15 && o.IsTargetable && o.Metadata.Contains("PitGateTransition"));
            }
        }

        private static NetworkObject BlockingTransitionBelly
        {
            get
            {
                return LokiPoe.ObjectManager.Objects
                    .FirstOrDefault(o => o.Distance < 20 && o.IsTargetable && o.Metadata.Contains("BellyArenaTransition"));
            }
        }

        #region Unused inteface methods

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public void Start()
        {
        }

        public void Tick()
        {
        }

        public void Stop()
        {
        }

        public string Name => "HandleDoorsTask";
        public string Description => "This task handles opening doors.";
        public string Author => "Bossland GmbH";
        public string Version => "1.0";

        #endregion
    }
}