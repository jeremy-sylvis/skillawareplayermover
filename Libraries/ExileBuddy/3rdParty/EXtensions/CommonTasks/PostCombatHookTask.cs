﻿using System.Threading.Tasks;
using Loki.Bot;

namespace EXtensions.CommonTasks
{
    public class PostCombatHookTask : ITask
    {
        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type != "task_execute") return false;

            foreach (var plugin in PluginManager.EnabledPlugins)
            {
                if (await plugin.Logic("post_combat_hook"))
                {
                    GlobalLog.Info($"[PostCombatHookTask] \"{plugin.Name}\" returned true.");
                    return true;
                }
            }
            return false;
        }

        #region Unused inteface methods

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public void Start()
        {
        }

        public void Tick()
        {
        }

        public void Stop()
        {
        }

        public string Name => "PostCombatHookTask";
        public string Description => "This task provides a coroutine hook for executing user logic after combat has completed.";
        public string Author => "Bossland GmbH";
        public string Version => "1.0";

        #endregion
    }
}