﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Common;
using Loki.Game;

namespace EXtensions.CommonTasks
{
    public class IdTask : ITask
    {
        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type != "task_execute") return false;
            if (!LokiPoe.Me.IsInTown && !LokiPoe.Me.IsInHideout) return false;

            var itemsToId = new List<Vector2i>();
            var itemFilter = ItemEvaluator.Instance;
            foreach (var item in LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.Items)
            {
                if (item.IsIdentified || item.IsCorrupted || item.IsMirrored) continue;
                IItemFilter dummyFilter;
                if (!itemFilter.Match(item, EvaluationType.Id, out dummyFilter)) continue;
                itemsToId.Add(item.LocationTopLeft);
            }

            if (itemsToId.Count == 0)
            {
                GlobalLog.Info("[IdTask] No items to identify.");
                return false;
            }

            GlobalLog.Info($"[IdTask] {itemsToId.Count} items to id.");

            int scrollsAmount = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.ItemAmount(CurrencyNames.Wisdom);
            if (scrollsAmount == 0 && LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.AvailableInventorySquares == 0)
            {
                GlobalLog.Error("[IdTask] No id scrolls and no free space in inventory. Now stopping the bot because it cannot continue.");
                BotManager.Stop();
                return true;
            }

            GlobalLog.Info($"[IdTask] {scrollsAmount} wisdom scrolls.");

            if (scrollsAmount < itemsToId.Count)
            {
                GlobalLog.Warn("[IdTask] Not enough wisdom scrolls to identify all items, now going to take them from stash.");
                if (!await TakeIdScrolls())
                {
                    ErrorManager.ReportError();
                    return true;
                }
            }

            if (!await Inventories.OpenInventory())
            {
                GlobalLog.Error("[IdTask] Fail to open inventory panel.");
                ErrorManager.ReportError();
                return true;
            }

            itemsToId.Sort(Position.Comparer.Instance);

            foreach (var pos in itemsToId)
            {
                if (!await Identify(pos))
                {
                    ErrorManager.ReportError();
                    return true;
                }
            }
            await Coroutines.CloseBlockingWindows();
            return true;
        }

        private static async Task<bool> Identify(Vector2i itemPos)
        {
            var item = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.FindItemByPos(itemPos);
            if (item == null)
            {
                GlobalLog.Error($"[Identify] Fail to find item at {itemPos} in player's inventory.");
                return false;
            }
            string name = string.Copy(item.Name);

            var scroll = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.Items
                .Where(s => s.Name == CurrencyNames.Wisdom)
                .OrderBy(s => s.StackCount)
                .FirstOrDefault();

            if (scroll == null)
            {
                GlobalLog.Error("[Identify] No id scrolls.");
                return false;
            }

            GlobalLog.Debug($"[Identify] Now using id scroll on \"{name}\".");

            if (!await LokiPoe.InGameState.InventoryUi.InventoryControl_Main.PickItemToCursor(scroll.LocationTopLeft, true)) return false;
            if (!await LokiPoe.InGameState.InventoryUi.InventoryControl_Main.PlaceItemFromCursor(itemPos)) return false;
            if (!await Wait.For(() => IsIdentified(itemPos), "item identification")) return false;

            GlobalLog.Debug($"[Identify] \"{name}\" has been successfully identified.");
            return true;
        }

        private static async Task<bool> TakeIdScrolls()
        {
            var tab = Settings.Instance.GetTabForCurrency(CurrencyNames.Wisdom);
            GlobalLog.Debug($"[TakeIdScrolls] Now going to take id scrolls from \"{tab}\" tab.");

            if (!await Inventories.OpenStashTab(tab)) return false;

            if (LokiPoe.InGameState.StashUi.StashTabInfo.IsPremiumCurrency)
            {
                var control = Inventories.GetControlWithCurrency(CurrencyNames.Wisdom);
                if (control == null)
                {
                    GlobalLog.Debug($"[TakeIdScrolls] \"{tab}\" tab does not contain any id scrolls. Now stopping the bot because it cannot continue.");
                    BotManager.Stop();
                    return false;
                }
                if (!await Inventories.FastMoveFromCurrencyTab(control)) return false;
            }
            else
            {
                var wisdoms = LokiPoe.InGameState.StashUi.InventoryControl.Inventory.Items
                    .Where(i => i.Name == CurrencyNames.Wisdom)
                    .OrderByDescending(i => i.StackCount)
                    .FirstOrDefault();

                if (wisdoms == null)
                {
                    GlobalLog.Debug($"[TakeIdScrolls] \"{tab}\" tab does not contain any id scrolls. Now stopping the bot because it cannot continue.");
                    BotManager.Stop();
                    return false;
                }
                if (!await Inventories.FastMoveFromStashTab(wisdoms.LocationTopLeft)) return false;
            }

            GlobalLog.Debug("[TakeIdScrolls] Id scrolls have been successfully taken.");
            return true;
        }

        private static bool IsIdentified(Vector2i pos)
        {
            var item = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.FindItemByPos(pos);
            if (item == null)
            {
                GlobalLog.Error("[Identify] Unexpected error. Item became null while waiting for identification.");
                ErrorManager.ReportError();
                return true;
            }
            return item.IsIdentified;
        }

        #region Unused interface methods

        public void Tick()
        {
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }


        public void Enable()
        {
        }

        public void Disable()
        {
        }

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public string Name => "IdTask";
        public string Description => "Task that handles item identification.";
        public string Author => "ExVault";
        public string Version => "1.0";

        #endregion
    }
}