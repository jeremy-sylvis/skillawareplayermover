﻿using System.Threading.Tasks;
using Loki.Bot;
using Loki.Game;

namespace EXtensions.CommonTasks
{
    public class ResurrectTask : ITask
    {
        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type != "task_execute") return false;

            if (!LokiPoe.Me.IsDead) return false;

            if (!await Resurrect(true))
            {
                if (!await Resurrect(false))
                {
                    GlobalLog.Error("[ResurrectTask] Ressurection failed. Now going to logout.");
                    if (!await PlayerAction.Logout())
                    {
                        GlobalLog.Error("[ResurrectTask] Logout failed. Now stopping the bot.");
                        BotManager.Stop();
                        return true;
                    }
                }
            }
            GlobalLog.Info("[ResurrectTask] Broadcasting \"player_resurrected_event\".");
            await Utility.BroadcastEventLogic("player_resurrected_event");
            return true;
        }

        public static async Task<bool> Resurrect(bool toCheckpoint, int attempts = 3)
        {
            GlobalLog.Debug($"[ResurrectTask] Now going to resurrect to {(toCheckpoint ? "checkpoint" : "town")}.");

            LokiPoe.ProcessHookManager.Reset();

            for (int i = 1; i <= attempts; ++i)
            {
                await Wait.SleepSafe(1000, 1500);

                var err = toCheckpoint
                    ? LokiPoe.InGameState.ResurrectPanel.ResurrectToCheckPoint()
                    : LokiPoe.InGameState.ResurrectPanel.ResurrectToTown();

                if (err != LokiPoe.InGameState.ResurrectResult.None)
                {
                    GlobalLog.Error($"[ResurrectTask] Fail to resurrect. Error: \"{err}\".");
                    continue;
                }
                if (await Wait.For(() => !LokiPoe.Me.IsDead, "resurrection"))
                {
                    GlobalLog.Debug("[ResurrectTask] Player has been successfully resurrected.");
                    await Wait.SleepSafe(250);
                    return true;
                }
            }
            GlobalLog.Error("[ResurrectTask] All resurrection attempts have been spent.");
            return false;
        }

        #region Unused inteface methods

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public void Start()
        {
        }

        public void Tick()
        {
        }

        public void Stop()
        {
        }

        public string Name => "ResurrectTask";
        public string Description => "This task handles resurrection.";
        public string Author => "Bossland GmbH";
        public string Version => "1.0";

        #endregion
    }
}