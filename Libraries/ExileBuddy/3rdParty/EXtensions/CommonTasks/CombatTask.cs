﻿using System.Threading.Tasks;
using Loki.Bot;
using Loki.Game;

namespace EXtensions.CommonTasks
{
    public class CombatTask : ITask
    {
        private readonly int _leashRange;

        public CombatTask(int leashRange)
        {
            _leashRange = leashRange;
        }

        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type != "task_execute") return false;
            if (LokiPoe.Me.IsDead || !LokiPoe.CurrentWorldArea.IsCombatArea()) return false;

            var routine = RoutineManager.CurrentRoutine;

            routine.Execute("SetLeash", _leashRange);

            return await routine.Logic("combat");
        }

        #region Unused inteface methods

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public void Start()
        {
        }

        public void Tick()
        {
        }

        public void Stop()
        {
        }

        public string Name => "CombatTask (Leash " + _leashRange + ")";

        public string Description => "This task executes routine logic for combat.";

        public string Author => "Bossland GmbH";

        public string Version => "1.0";

        #endregion
    }
}