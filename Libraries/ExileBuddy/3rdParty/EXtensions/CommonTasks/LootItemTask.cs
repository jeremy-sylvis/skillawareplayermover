﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EXtensions.Global;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.Objects;

namespace EXtensions.CommonTasks
{
    public class LootItemTask : ITask
    {
        private const int MaxAttempts = 10;

        private readonly Interval _logInterval = new Interval(1000);

        private KeyValuePair<int, CombatAreaCache.ItemPosition>? _currentItem;

        private bool _isInPreTownrunMode;

        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type == "core_area_changed_event")
            {
                _currentItem = null;
                _isInPreTownrunMode = false;
                return true;
            }

            if (type != "task_execute") return false;
            if (LokiPoe.Me.IsDead || !LokiPoe.CurrentWorldArea.IsCombatArea()) return false;

            var itemPositions = CombatAreaCache.Current.ItemPositions;

            if (itemPositions.Count == 0) return false;

            if (_currentItem == null)
            {
                if (_isInPreTownrunMode)
                {
                    _currentItem = itemPositions
                        .Where(kvp => kvp.Value.Position.Distance <= 20 && CanFit(kvp.Value.Size))
                        .OrderBy(kvp => kvp.Value.Position.DistanceSqr)
                        .FirstOrDefault();

                    if (_currentItem.Value.Equals(default(KeyValuePair<int, CombatAreaCache.ItemPosition>)))
                    {
                        if (!await PlayerAction.TpToTown()) ErrorManager.ReportError();
                        else ReturnAfterTownrunTask.Enabled = true;
                        return true;
                    }
                }
                else
                {
                    _currentItem = itemPositions.OrderBy(kvp => kvp.Value.Position.DistanceSqr).First();
                }
            }

            var item = _currentItem.Value.Value;
            var itemPos = item.Position;

            if (!itemPos.IsInitialized)
            {
                if (!itemPos.Initialize())
                {
                    ClearCurrentItem(true, "no walkable position");
                    return true;
                }
            }

            if (_logInterval.Elapsed)
            {
                GlobalLog.Debug($"[LootItemTask] Items to pick up: {itemPositions.Count}");
            }

            if (itemPos.IsFar || itemPos.PathDistance > 15)
            {
                itemPos.Come();
                return true;
            }

            var itemId = _currentItem.Value.Key;

            var itemObj = LokiPoe.ObjectManager.GetObjectById<WorldItem>(itemId);
            if (itemObj == null)
            {
                ClearCurrentItem(false);
                return true;
            }

            if (!CanFit(item.Size))
            {
                _isInPreTownrunMode = true;
                _currentItem = null;
                return true;
            }

            ++item.PickupAttempts;

            if (item.PickupAttempts > MaxAttempts)
            {
                if (itemPos.Name == CurrencyNames.Mirror)
                {
                    GlobalLog.Error("[LootItemTask] Fail to pick up the Mirror of Kalandra. Now stopping the bot.");
                    BotManager.Stop();
                }
                else
                {
                    GlobalLog.Error($"[LootItemTask] Fail to pick up {itemPos.Name}. Now blacklisting it.");
                    ClearCurrentItem(true, "fail to pick up");
                }
                return true;
            }

            GlobalLog.Info($"[LootItemTask] Pick up attempt: {item.PickupAttempts}");

            if (item.PickupAttempts%3 == 0)
            {
                await PlayerAction.DisableAlwaysHighlight();
            }

            //if (item.PickupAttempts == 4)
            //{
            //    await ReopenPortal
            //}

            await PlayerAction.EnableAlwaysHighlight();

            GlobalLog.Debug($"[LootItemTask] Now going to pick up {itemPos}");

            if (!await Coroutines.InteractWith(itemObj))
                return true;

            if (!await Wait.For(() => LokiPoe.ObjectManager.GetObjectById<WorldItem>(itemId) == null, "item disappear", 100, 1000))
                return true;

            GlobalLog.Debug($"[LootItemTask] \"{itemPos.Name}\" has been successfully picked up.");
            ClearCurrentItem(false);
            return true;
        }

        public void ClearCurrentItem(bool blacklist, string reason = null)
        {
            if (_currentItem == null) return;

            var id = _currentItem.Value.Key;
            CombatAreaCache.Current.ItemPositions.Remove(id);
            _currentItem = null;

            if (blacklist) Blacklist.Add(id, TimeSpan.FromDays(1), reason ?? "");
        }

        public static bool CanFit(Vector2i size)
        {
            return LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.CanFitItem(size);
        }

        #region Unused inteface methods

        public void Tick()
        {
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public string Name => "LootItemTask";
        public string Description => "Task that handles item looting.";
        public string Author => "ExVault";
        public string Version => "1.0";

        #endregion
    }
}