﻿using JetBrains.Annotations;
using log4net;
using Loki.Common;

namespace EXtensions
{
    public static class GlobalLog
    {
        private static readonly ILog Log = Logger.GetLoggerInstanceForType();

        public static void Debug(object message)
        {
            Log.Debug(message);
        }

        public static void Debug(string message)
        {
            Log.Debug(message);
        }

        [StringFormatMethod("format")]
        public static void Debug(string format, object arg0)
        {
            Log.DebugFormat(format, arg0);
        }

        [StringFormatMethod("format")]
        public static void Debug(string format, object arg0, object arg1)
        {
            Log.DebugFormat(format, arg0, arg1);
        }

        [StringFormatMethod("format")]
        public static void Debug(string format, params object[] args)
        {
            Log.DebugFormat(format, args);
        }

        public static void Info(object message)
        {
            Log.Info(message);
        }

        public static void Info(string message)
        {
            Log.Info(message);
        }

        [StringFormatMethod("format")]
        public static void Info(string format, object arg0)
        {
            Log.InfoFormat(format, arg0);
        }

        [StringFormatMethod("format")]
        public static void Info(string format, object arg0, object arg1)
        {
            Log.InfoFormat(format, arg0, arg1);
        }

        [StringFormatMethod("format")]
        public static void Info(string format, params object[] args)
        {
            Log.InfoFormat(format, args);
        }

        public static void Warn(object message)
        {
            Log.Warn(message);
        }

        public static void Warn(string message)
        {
            Log.Warn(message);
        }

        [StringFormatMethod("format")]
        public static void Warn(string format, object arg0)
        {
            Log.WarnFormat(format, arg0);
        }

        [StringFormatMethod("format")]
        public static void Warn(string format, object arg0, object arg1)
        {
            Log.WarnFormat(format, arg0, arg1);
        }

        [StringFormatMethod("format")]
        public static void Warn(string format, params object[] args)
        {
            Log.WarnFormat(format, args);
        }

        public static void Error(object message)
        {
            Log.Error(message);
        }

        public static void Error(string message)
        {
            Log.Error(message);
        }

        [StringFormatMethod("format")]
        public static void Error(string format, object arg0)
        {
            Log.ErrorFormat(format, arg0);
        }

        [StringFormatMethod("format")]
        public static void Error(string format, object arg0, object arg1)
        {
            Log.ErrorFormat(format, arg0, arg1);
        }

        [StringFormatMethod("format")]
        public static void Error(string format, params object[] args)
        {
            Log.ErrorFormat(format, args);
        }
    }
}