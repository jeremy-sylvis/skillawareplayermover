﻿using System.Linq;
using Loki.Game;
using Loki.Game.GameData;

namespace EXtensions
{
    public static class World
    {
        public static DatWorldAreaWrapper LastOpenedAct
        {
            get
            {
                var act = LokiPoe.InstanceInfo.AvailableWaypoints.Values
                    .Where(a => a.IsTown)
                    .OrderByDescending(a => a.Difficulty)
                    .ThenByDescending(a => a.Act)
                    .FirstOrDefault();

                if (act != null) return act;

                GlobalLog.Error("[GetLastOpenedAct] Fail to get any opened act.");
                ErrorManager.ReportCriticalError();
                return null;
            }
        }

        public static bool HasWaypoint(string name, Difficulty difficulty = Difficulty.Unknown)
        {
            var diff = difficulty == Difficulty.Unknown ? LokiPoe.CurrentWorldArea.Difficulty : difficulty;
            return LokiPoe.InstanceInfo.AvailableWaypoints.Values.Any(a => a.Difficulty == diff && a.Name == name);
        }
    }
}