﻿using System;
using Loki.Bot;

namespace EXtensions
{
    public static class ErrorManager
    {
        private const int MaxErrors = 10;
        private static int _errorCount;

        static ErrorManager()
        {
            Events.AreaChanged += OnAreaChange;
        }

        private static void OnAreaChange(object sender, AreaChangedArgs args)
        {
            Reset();
        }

        public static void Reset()
        {
            GlobalLog.Info("[ErrorManager] Error count has been reset.");
            _errorCount = 0;
        }

        public static void ReportError()
        {
            ++_errorCount;
            GlobalLog.Error($"[ErrorManager] Error count: {_errorCount}/{MaxErrors}.");

            if (_errorCount >= MaxErrors)
            {
                GlobalLog.Error("[ErrorManager] Errors threshold has been reached. Now requesting bot to stop.");
                if (BotManager.Stop()) _errorCount = 0;
                throw new Exception("MAX_ERRORS");
            }
        }

        public static void ReportCriticalError()
        {
            GlobalLog.Error("[CRITICAL ERROR] Now requesting bot to stop.");
            BotManager.Stop();
            throw new Exception("CRITICAL_ERROR");
        }
    }
}