﻿using System;
using Loki.Game.GameData;

namespace EXtensions
{
    public class AreaChangedArgs : EventArgs
    {
        public uint OldHash { get; private set; }
        public uint NewHash { get; private set; }
        public DatWorldAreaWrapper OldArea { get; private set; }
        public DatWorldAreaWrapper NewArea { get; private set; }

        public AreaChangedArgs(uint oldHash, uint newHash, DatWorldAreaWrapper oldArea, DatWorldAreaWrapper newArea)
        {
            OldHash = oldHash;
            NewHash = newHash;
            OldArea = oldArea;
            NewArea = newArea;
        }
    }
}