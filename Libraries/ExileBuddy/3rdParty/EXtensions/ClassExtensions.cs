﻿using System;
using System.Linq;
using EXtensions.Positions;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace EXtensions
{
    public static class ClassExtensions
    {
        public static bool EqualsIgnorecase(this string thisStr, string str)
        {
            return thisStr.Equals(str, StringComparison.OrdinalIgnoreCase);
        }

        public static bool ContainsIgnorecase(this string thisStr, string str)
        {
            return thisStr.IndexOf(str, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        public static Rarity RarityLite(this Item item)
        {
            var mods = item.Components.ModsComponent;
            return mods == null ? Rarity.Normal : mods.Rarity;
        }

        public static Item FindItemByPos(this Inventory inventory, Vector2i pos)
        {
            return inventory.Items.FirstOrDefault(i => i.LocationTopLeft == pos);
        }

        public static T FindItemByPos<T>(this Inventory inventory, Vector2i pos) where T : Item
        {
            return inventory.Items.OfType<T>().FirstOrDefault(i => i.LocationTopLeft == pos);
        }

        public static Item FindItemById(this Inventory inventory, int id)
        {
            return inventory.Items.FirstOrDefault(i => i.LocalId == id);
        }

        public static T FindItemById<T>(this Inventory inventory, int id) where T : Item
        {
            return inventory.Items.OfType<T>().FirstOrDefault(i => i.LocalId == id);
        }

        public static CompositeItemType CompositeType(this Item item)
        {
            CompositeItemType type;
            return CompositeItemType.TypeMap.TryGetValue(item.Class, out type) ? type : CompositeItemType.Unknown;
        }

        public static bool IsCombatArea(this DatWorldAreaWrapper area)
        {
            return !area.IsTown && !area.IsHideoutArea && !area.IsMapRoom;
        }

        public static bool IsMasterQuestArea(this DatWorldAreaWrapper area)
        {
            return area.IsMissionArea || area.IsDailyArea || area.IsRelicArea || area.IsDenArea;
        }

        public static bool IsInTown(this LocalPlayer player, int act)
        {
            return player.IsInTown && LokiPoe.CurrentWorldArea.Act == act;
        }

        public static WorldPosition WorldPosition(this NetworkObject obj)
        {
            return new WorldPosition(obj.Position);
        }

        public static WalkablePosition WalkablePosition(this NetworkObject obj, int step = 3, int radius = 30)
        {
            return new WalkablePosition(obj.Name, obj.Position, step, radius);
        }

        public static TownNpcs.TownNpc AsTownNpc(this NetworkObject npc)
        {
            return TownNpcs.CreateCustom(npc);
        }

        public static bool IsPortalToTown(this Portal p)
        {
            if (!p.IsTargetable) return false;
            var area = p.Components.PortalComponent.Area;
            return area.IsTown || area.IsHideoutArea || area.IsMapRoom;
        }
    }
}