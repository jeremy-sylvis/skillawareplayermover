﻿using System.Threading.Tasks;
using System.Windows.Controls;
using log4net;
using Loki.Bot;
using Loki.Common;
using Loki.Game.GameData;
using settings = EXtensions.Settings;

namespace EXtensions
{
    public class EXtensions : IPlugin
    {
        private static readonly ILog Log = Logger.GetLoggerInstanceForType();
        private Gui _gui;

        private static uint _currentCombatAreaHash;
        private static DatWorldAreaWrapper _currentCombatArea;

        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type == "core_area_changed_event")
            {
                uint oldHash = (uint) param[0];
                uint newHash = (uint) param[1];
                DatWorldAreaWrapper oldArea = (DatWorldAreaWrapper) param[2];
                DatWorldAreaWrapper newArea = (DatWorldAreaWrapper) param[3];
                string oldAreaName = oldArea == null ? "null" : oldArea.Name;
                string newAreaName = newArea == null ? "null" : newArea.Name;
                GlobalLog.Info($"[AreaChanged] {oldAreaName} -> {newAreaName}");
                Events.RaiseAreaChangedEvent(new AreaChangedArgs(oldHash, newHash, oldArea, newArea));

                if (_currentCombatAreaHash != newHash && newArea != null && newArea.IsCombatArea())
                {
                    GlobalLog.Info($"[CombatAreaChanged] {(_currentCombatArea == null ? "null" : _currentCombatArea.Name)} -> {newAreaName}");
                    Events.RaiseCombatAreaChangedEvent(new AreaChangedArgs(oldHash, newHash, _currentCombatArea, newArea));
                    _currentCombatAreaHash = newHash;
                    _currentCombatArea = newArea;
                }
            }
            else if (type == "core_player_died_event")
            {
                Events.RaisePlayerDiedEvent();
            }
            return false;
        }

        #region Unused interface methods

        public void Start()
        {
            Log.DebugFormat("[EXtensions] Start");
        }

        public void Stop()
        {
            Log.DebugFormat("[EXtensions] Stop");
        }

        public void Tick()
        {
        }

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public void Initialize()
        {
            Log.DebugFormat("[EXtensions] Initialize");
        }

        public void Deinitialize()
        {
            Log.DebugFormat("[EXtensions] Deinitialize");
        }

        public void Enable()
        {
            Log.DebugFormat("[EXtensions] Enable");
        }

        public void Disable()
        {
            Log.DebugFormat("[EXtensions] Disable");
        }

        public void Dispose()
        {
        }

        public string Name => "EXtensions";
        public string Description => "Convenient wrappers for Exilebuddy API.";
        public string Author => "ExVault";
        public string Version => "1.0.8";
        public JsonSettings Settings => settings.Instance;
        public UserControl Control => _gui ?? (_gui = new Gui());

        #endregion
    }
}