﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace EXtensions
{
    public static class PlayerAction
    {
        public static async Task<bool> Interact(NetworkObject obj, int attempts = 3)
        {
            if (obj == null)
            {
                GlobalLog.Error("[Interact] Object for interaction is null.");
                return false;
            }

            string name = string.Copy(obj.Name);
            GlobalLog.Debug($"[Interact] Now going to interact with \"{name}\".");
            for (int i = 1; i <= attempts; i++)
            {
                if (LokiPoe.Me.IsDead) return false;
                await Coroutines.CloseBlockingWindows();
                await Coroutines.FinishCurrentAction();

                if (await Coroutines.InteractWith(obj))
                {
                    GlobalLog.Debug($"[Interact] \"{name}\" has been successfully interacted.");
                    return true;
                }
                GlobalLog.Error($"[Interact] Fail to interact with \"{name}\". Attempt: {i}/{attempts}.");
                await Wait.SleepSafe(200, 300);
            }
            return false;
        }

        public static async Task<bool> Interact(NetworkObject obj, Func<bool> success, string desc, int timeout = 3000, int attempts = 3)
        {
            if (obj == null)
            {
                GlobalLog.Error("[Interact] Object for interaction is null.");
                return false;
            }

            string name = string.Copy(obj.Name);
            GlobalLog.Debug($"[Interact] Now going to interact with \"{name}\".");
            for (int i = 1; i <= attempts; i++)
            {
                if (LokiPoe.Me.IsDead) return false;
                await Coroutines.CloseBlockingWindows();
                await Coroutines.FinishCurrentAction();

                if (await Coroutines.InteractWith(obj))
                {
                    if (await Wait.For(success, desc, 100, timeout))
                    {
                        GlobalLog.Debug($"[Interact] \"{name}\" has been successfully interacted.");
                        return true;
                    }
                }
                GlobalLog.Error($"[Interact] Fail to interact with \"{name}\". Attempt: {i}/{attempts}.");
                await Wait.SleepSafe(200, 300);
            }
            return false;
        }

        public static async Task<bool> TakeTransition(AreaTransition transition, bool newInstance = false)
        {
            if (transition == null)
            {
                GlobalLog.Error("[TakeTransition] Transition object is null.");
                return false;
            }

            var pos = transition.WalkablePosition();
            var type = transition.TransitionType;

            GlobalLog.Debug($"[TakeTransition] Now going to enter \"{pos.Name}\".");

            await pos.ComeAtOnce();

            uint hash = LokiPoe.LocalData.AreaHash;

            bool entered = newInstance
                ? await CreateNewInstance(transition)
                : await Interact(transition);

            if (!entered) return false;

            if (type == TransitionTypes.Local)
            {
                if (!await Wait.For(() => pos.FarFromMe(15), "local transition")) return false;
                await Wait.SleepSafe(250);
            }
            else
            {
                if (!await Wait.ForAreaChange(hash)) return false;
            }
            GlobalLog.Debug($"[TakeTransition] \"{pos.Name}\" has been successfully entered.");
            return true;
        }


        public static async Task<bool> TakeClosestTransition()
        {
            var transition = LokiPoe.ObjectManager.Objects
                .OfType<AreaTransition>()
                .OrderBy(a => a.Distance)
                .FirstOrDefault();

            return await TakeTransition(transition);
        }

        public static async Task<bool> TakeTransitionByName(string name)
        {
            var transition = LokiPoe.ObjectManager.Objects
                .OfType<AreaTransition>()
                .Where(a => a.Name == name)
                .OrderBy(a => a.Distance)
                .FirstOrDefault();

            return await TakeTransition(transition);
        }

        public static async Task<bool> TakePortal(Portal portal)
        {
            if (portal == null)
            {
                GlobalLog.Error("[TakePortal] Portal object is null.");
                return false;
            }

            var pos = portal.WalkablePosition();
            await pos.ComeAtOnce();

            GlobalLog.Debug($"[TakePortal] Now going to take portal to \"{pos.Name}\".");

            uint hash = LokiPoe.LocalData.AreaHash;
            await DisableAlwaysHighlight();
            if (!await Interact(portal)) return false;
            if (!await Wait.ForAreaChange(hash)) return false;

            GlobalLog.Debug($"[TakePortal] Portal to \"{pos.Name}\" has been successfully taken.");
            return true;
        }

        public static async Task<bool> CreateTownPortal()
        {
            var portalSkill = LokiPoe.InGameState.SkillBarHud.Skills.FirstOrDefault(s => s.Name == "Portal");
            if (portalSkill != null && portalSkill.IsOnSkillBar)
            {
                await Coroutines.FinishCurrentAction();
                var used = LokiPoe.InGameState.SkillBarHud.Use(portalSkill.Slot, false);
                if (used != LokiPoe.InGameState.UseResult.None)
                {
                    GlobalLog.Error($"[TpToTown] Fail to cast portal skill. Error: \"{used}\".");
                    return false;
                }
                await Coroutines.FinishCurrentAction();
            }
            else
            {
                var portalScroll = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.Items
                    .Where(i => i.Name == CurrencyNames.Portal)
                    .OrderBy(i => i.StackCount)
                    .FirstOrDefault();

                if (portalScroll == null)
                {
                    GlobalLog.Error("[TpToTown] Out of portal scrolls.");
                    return false;
                }

                int itemId = portalScroll.LocalId;

                if (!await Inventories.OpenInventory()) return false;

                var used = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.UseItem(itemId);
                if (used != UseItemResult.None)
                {
                    GlobalLog.Error($"[TpToTown] Fail to use a Portal Scroll. Error: \"{used}\".");
                    return false;
                }
                if (Settings.Instance.GlobalUseReactionWait) await Coroutines.ReactionWait();
                await Coroutines.CloseBlockingWindows();
            }
            return await Wait.For(() => ClosestTargetablePortal != null, "portal spawning");
        }

        public static async Task<bool> TpToTown(bool forceNewPortal = false)
        {
            GlobalLog.Debug("[TpToTown] Now going to open and take portal to town.");

            var area = LokiPoe.CurrentWorldArea;

            if (area.IsTown || area.IsHideoutArea)
            {
                GlobalLog.Error("[TpToTown] We are alredy in town/hideout.");
                return false;
            }

            if (!area.IsOverworldArea && !area.IsMap && !area.IsCorruptedArea && !area.IsMapRoom)
            {
                GlobalLog.Error("[TpToTown] Cannot create portals in this area. Now going to relogin to get to the town.");
                return await Logout();
            }

            if (!forceNewPortal)
            {
                var portal = LokiPoe.ObjectManager.Objects
                    .OfType<Portal>()
                    .Where(p => p.IsPortalToTown() && p.Distance <= 50)
                    .OrderBy(p => p.Distance)
                    .FirstOrDefault();

                if (portal != null && portal.WorldPosition().PathDistance <= 60)
                {
                    GlobalLog.Debug("[TpToTown] There is a ready to use portal nearby. Now going to take it.");
                    return await TakePortal(portal);
                }
                GlobalLog.Debug("[TpToTown] There is no ready to use portal nearby. Now going to create a new portal.");
            }
            if (!await CreateTownPortal())
            {
                GlobalLog.Error("[TpToTown] Fail to create a new town portal. Now going to relogin to get to the town.");
                return await Logout();
            }

            return await TakePortal(ClosestTargetablePortal);
        }

        public static async Task<bool> OpenWaypoint()
        {
            WalkablePosition wpPos;
            if (LokiPoe.Me.IsInTown)
            {
                wpPos = StaticPositions.GetWaypointPosByAct();
            }
            else
            {
                var wpObj = LokiPoe.ObjectManager.Waypoint;
                if (wpObj == null)
                {
                    GlobalLog.Error("[OpenWaypoint] Fail to find any Waypoint nearby.");
                    return false;
                }
                wpPos = wpObj.WalkablePosition();
            }
            await wpPos.ComeAtOnce();
            if (!await Interact(LokiPoe.ObjectManager.Waypoint, () => LokiPoe.InGameState.WorldUi.IsOpened, "wold panel opening")) return false;
            await Wait.SleepSafe(200);
            return true;
        }

        public static async Task<bool> TakeWaypoint(string areaName, Difficulty difficulty = Difficulty.Unknown, bool newInstance = false)
        {
            if (!LokiPoe.InGameState.WorldUi.IsOpened)
            {
                if (!await OpenWaypoint())
                {
                    GlobalLog.Error("[TakeWaypoint] Fail to open waypoint.");
                    return false;
                }
            }

            if (areaName == AreaNames.EternalLaboratory)
            {
                difficulty = Difficulty.Merciless;
            }
            else if (difficulty == Difficulty.Unknown)
            {
                difficulty = LokiPoe.Me.IsInHideout ? World.LastOpenedAct.Difficulty : LokiPoe.CurrentWorldArea.Difficulty;
            }

            GlobalLog.Debug($"[TakeWaypoint] Now going to take a waypoint to \"{areaName}\" ({difficulty})");
            var areaHash = LokiPoe.LocalData.AreaHash;

            LokiPoe.InGameState.TakeWaypointResult taken;
            if (areaName.EqualsIgnorecase("hideout"))
            {
                taken = LokiPoe.InGameState.WorldUi.GoToHideout();
            }
            else
            {
                string areaId = GetAreaId(areaName, difficulty);
                if (areaId == null)
                {
                    GlobalLog.Error($"[TakeWaypoint] Fail to find \"{areaName}\" on {difficulty} difficulty among available waypoints.");
                    return false;
                }
                taken = LokiPoe.InGameState.WorldUi.TakeWaypoint(areaId, newInstance, int.MaxValue);
            }

            if (taken != LokiPoe.InGameState.TakeWaypointResult.None)
            {
                GlobalLog.Error($"[TakeWaypoint] Fail to take waypoint to \"{areaName}\" on {difficulty} difficulty. Error: \"{taken}\".");
                return false;
            }
            return await Wait.ForAreaChange(areaHash);
        }

        public static async Task<bool> Logout()
        {
            GlobalLog.Debug("[Logout] Now going to logout.");

            var loggedout = LokiPoe.EscapeState.LogoutToTitleScreen();
            if (loggedout != LokiPoe.EscapeState.LogoutError.None)
            {
                GlobalLog.Error($"[Logout] Fail to logout. Error: \"{loggedout}\".");
                return false;
            }

            return await Wait.For(() => LokiPoe.IsInLoginScreen, "logout");
        }

        public static async Task<bool> TryTo(Func<Task<bool>> action, string desc, int attempts)
        {
            for (int i = 1; i <= attempts; i++)
            {
                if (LokiPoe.Me.IsDead) break;
                GlobalLog.Debug($"[TryTo] {desc} attempt: {i}/{attempts}");
                if (await action()) return true;
            }
            return false;
        }

        public static async Task EnableAlwaysHighlight()
        {
            if (LokiPoe.ConfigManager.IsAlwaysHighlightEnabled) return;
            GlobalLog.Info("[EnableAlwaysHighlight] Now enabling always highlight.");
            LokiPoe.Input.SimulateKeyEvent(LokiPoe.Input.Binding.highlight_toggle, true, false, false);
            await Wait.SleepSafe(30);
        }

        public static async Task DisableAlwaysHighlight()
        {
            if (!LokiPoe.ConfigManager.IsAlwaysHighlightEnabled) return;
            GlobalLog.Info("[DisableAlwaysHighlight] Now disabling always highlight.");
            LokiPoe.Input.SimulateKeyEvent(LokiPoe.Input.Binding.highlight_toggle, true, false, false);
            await Wait.SleepSafe(30);
        }

        private static async Task<bool> CreateNewInstance(AreaTransition transition)
        {
            var name = transition.Name;
            if (!await Coroutines.InteractWith(transition, true))
            {
                GlobalLog.Error($"[CreateNewInstance] Fail to interact with \"{name}\" transition.");
                return false;
            }

            if (!await Wait.For(() => LokiPoe.InGameState.InstanceManagerUi.IsOpened, "instance manager opening", 100, 3000))
                return false;

            if (Settings.Instance.GlobalUseReactionWait) await Coroutines.ReactionWait();

            GlobalLog.Debug($"[CreateNewInstance] Creating new instance for \"{name}\".");

            var err = LokiPoe.InGameState.InstanceManagerUi.JoinNewInstance();
            if (err != LokiPoe.InGameState.JoinInstanceResult.None)
            {
                GlobalLog.Error($"[CreateNewInstance] Fail to create a new instance. Error: \"{err}\".");
                return false;
            }
            GlobalLog.Debug($"[CreateNewInstance] New instance for \"{name}\" has been successfully created.");
            return true;
        }

        private static string GetAreaId(string areaName, Difficulty difficulty)
        {
            foreach (var kvp in LokiPoe.InstanceInfo.AvailableWaypoints)
            {
                var area = kvp.Value;
                if (area.Difficulty == difficulty && area.Name == areaName) return area.Id;
            }
            return null;
        }

        private static Portal ClosestTargetablePortal
        {
            get
            {
                return LokiPoe.ObjectManager.Objects
                    .OfType<Portal>()
                    .Where(p => p.IsPortalToTown() && p.Distance <= 30)
                    .OrderBy(p => p.Distance)
                    .FirstOrDefault();
            }
        }
    }
}