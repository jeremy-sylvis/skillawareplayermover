﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using EXtensions.CommonTasks;
using Loki;
using Loki.Bot;
using Loki.Common;
using Loki.Game.GameData;
using Newtonsoft.Json;

namespace EXtensions
{
    public class Settings : JsonSettings
    {
        private static Settings _instance;
        public static Settings Instance => _instance ?? (_instance = new Settings());

        private Settings()
            : base(GetSettingsFilePath(Configuration.Instance.Name, "EXtensions.json"))
        {
            if (GeneralStashingRules.Count == 0)
            {
                InitGeneralStashingRules();
            }
            if (CurrencyStashingRules.Count == 0)
            {
                InitCurrencyStashingRules();
            }
            foreach (var rule in GeneralStashingRules)
            {
                rule.FillTabList();
            }
            foreach (var rule in CurrencyStashingRules)
            {
                rule.FillTabList();
            }
        }

        #region Stashing

        private bool _useCurrencyStashingRules;

        [DefaultValue(false)]
        public bool UseCurrencyStashingRules
        {
            get { return _useCurrencyStashingRules; }
            set
            {
                if (value == _useCurrencyStashingRules) return;
                _useCurrencyStashingRules = value;
                NotifyPropertyChanged(() => UseCurrencyStashingRules);
            }
        }

        public List<StashingRule> GeneralStashingRules { get; set; } = new List<StashingRule>();
        public List<StashingRule> CurrencyStashingRules { get; set; } = new List<StashingRule>();

        public HashSet<string> FullTabs = new HashSet<string>();

        public string GetTabForCategory(string category)
        {
            return GetTab(GeneralStashingRules, category);
        }

        public string GetTabForCurrency(string currencyName)
        {
            if (!UseCurrencyStashingRules)
            {
                return GetTab(GeneralStashingRules, StashingCategory.Currency);
            }
            if (currencyName == CurrencyNames.ScrollFragment)
            {
                return GetTab(CurrencyStashingRules, CurrencyNames.Wisdom);
            }
            if (currencyName == CurrencyNames.TransmutationShard)
            {
                return GetTab(CurrencyStashingRules, CurrencyNames.Transmutation);
            }
            if (currencyName == CurrencyNames.AlterationShard)
            {
                return GetTab(CurrencyStashingRules, CurrencyNames.Alteration);
            }
            if (currencyName == CurrencyNames.AlchemyShard)
            {
                return GetTab(CurrencyStashingRules, CurrencyNames.Alchemy);
            }
            if (currencyName.Contains("Sextant"))
            {
                return GetTab(CurrencyStashingRules, StashingCategory.Sextant);
            }
            return GetTab(CurrencyStashingRules, currencyName);
        }

        private string GetTab(List<StashingRule> list, string name)
        {
            var rule = list.Find(r => r.Name == name);
            if (rule == null)
            {
                GlobalLog.Error($"[EXtensions] Unknown stashing rule: \"{name}\".");
                ErrorManager.ReportCriticalError();
                return null;
            }

            var tab = rule.TabList.FirstOrDefault(t => !FullTabs.Contains(t));
            if (tab == null)
            {
                GlobalLog.Warn($"[EXtensions] All tabs for \"{name}\" are full. Current item will be stashed to \"Other\" tabs.");
                rule = list.Find(r => r.Name == StashingCategory.Other);
                tab = rule.TabList.FirstOrDefault(t => !FullTabs.Contains(t));
                if (tab == null)
                {
                    GlobalLog.Error($"[EXtensions] All tabs for \"{name}\" are full and all \"Other\" tabs are full. Now stopping the bot because it cannot continue.");
                    ErrorManager.ReportCriticalError();
                    return null;
                }
            }
            return tab;
        }

        private void InitGeneralStashingRules()
        {
            GeneralStashingRules.Add(new StashingRule(StashingCategory.Rare, "3"));
            GeneralStashingRules.Add(new StashingRule(StashingCategory.Unique, "3"));
            GeneralStashingRules.Add(new StashingRule(StashingCategory.Gem, "2"));
            GeneralStashingRules.Add(new StashingRule(StashingCategory.Card, "2"));
            GeneralStashingRules.Add(new StashingRule(StashingCategory.Essence, "1"));
            GeneralStashingRules.Add(new StashingRule(StashingCategory.Jewel, "2"));
            GeneralStashingRules.Add(new StashingRule(StashingCategory.Map, "4"));
            GeneralStashingRules.Add(new StashingRule(StashingCategory.Fragment, "4"));
            GeneralStashingRules.Add(new StashingRule(StashingCategory.Other, "4"));
            GeneralStashingRules.Add(new StashingRule(StashingCategory.Currency, "1"));
        }

        private void InitCurrencyStashingRules()
        {
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Wisdom, "1"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Portal, "1"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Transmutation, "1"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Augmentation, "1"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Alteration, "1"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Scrap, "1"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Whetstone, "1"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Glassblower, "1"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Chisel, "1"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Chromatic, "1"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Chance, "1"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Alchemy, "1"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Jeweller, "1"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Scouring, "1"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Fusing, "2"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Blessed, "2"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Regal, "2"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Chaos, "2"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Vaal, "2"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Regret, "2"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Gemcutter, "2"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Divine, "2"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Exalted, "2"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Eternal, "2"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.Mirror, "2"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.PerandusCoin, "1"));
            CurrencyStashingRules.Add(new StashingRule(CurrencyNames.SilverCoin, "1"));
            CurrencyStashingRules.Add(new StashingRule(StashingCategory.Sextant, "4"));
        }

        public static class StashingCategory
        {
            public const string Rare = "Rares";
            public const string Unique = "Uniques";
            public const string Gem = "Gems";
            public const string Card = "Cards";
            public const string Essence = "Essences";
            public const string Jewel = "Jewels";
            public const string Map = "Maps";
            public const string Fragment = "Fragments";
            public const string Other = "Other";
            public const string Currency = "Currency";
            public const string Sextant = "Sextants";
        }

        public class StashingRule
        {
            public string Name { get; }

            private string _tabs;

            public string Tabs
            {
                get { return _tabs; }
                set
                {
                    if (value == _tabs) return;
                    _tabs = value;
                    FillTabList();
                    StashTask.RequestInvalidTabCheck();
                }
            }

            [JsonIgnore]
            public List<string> TabList { get; set; }

            public StashingRule(string name, string tabs)
            {
                Name = name;
                _tabs = tabs;
                TabList = new List<string>();
            }

            public void FillTabList()
            {
                try
                {
                    Parse(_tabs, TabList);
                }
                catch (Exception ex)
                {
                    if (BotManager.IsRunning)
                    {
                        GlobalLog.Error($"Parsing error in \"{_tabs}\".");
                        GlobalLog.Error(ex.Message);
                        BotManager.Stop();
                    }
                    else
                    {
                        MessageBoxes.Error($"Parsing error in \"{_tabs}\".\n{ex.Message}");
                    }
                }
            }

            private static void Parse(string str, ICollection<string> list)
            {
                if (str == string.Empty)
                    throw new Exception("Stashing setting cannot be empty.");

                list.Clear();

                var commaParams = str.Split(',');
                foreach (var param in commaParams)
                {
                    var trimmed = param.Trim();
                    if (trimmed == string.Empty)
                        throw new Exception("Remove double commas and/or commas from the start/end of the string.");

                    if (!ParseRange(trimmed, list))
                    {
                        list.Add(trimmed);
                    }
                }
            }

            private static bool ParseRange(string str, ICollection<string> list)
            {
                var hyphenParams = str.Split('-');
                if (hyphenParams.Length == 2)
                {
                    var start = hyphenParams[0].Trim();
                    var end = hyphenParams[1].Trim();

                    int first;
                    if (!int.TryParse(start, out first))
                        throw new Exception($"Invalid parameter \"{start}\". Only numeric values are supperted with range delimeter.");

                    int last;
                    if (!int.TryParse(end, out last))
                        throw new Exception($"Invalid parameter \"{end}\". Only numeric values are supperted with range delimeter.");
                    list.Add(start);

                    for (int i = first + 1; i < last; ++i)
                    {
                        list.Add(i.ToString());
                    }
                    list.Add(end);
                    return true;
                }
                if (hyphenParams.Length == 1) return false;
                throw new Exception($"Invalid range string: \"{str}\". Supported format: \"X-Y\".");
            }
        }

        #endregion

        #region Stuck detection

        private int _maxStucksPerInstance;
        private int _maxStuckCountSmall;
        private int _maxStuckCountMedium;
        private int _maxStuckCountLarge;

        [DefaultValue(3)]
        public int MaxStucksPerInstance
        {
            get { return _maxStucksPerInstance; }
            set
            {
                if (value == _maxStucksPerInstance) return;
                _maxStucksPerInstance = value;
                NotifyPropertyChanged(() => MaxStucksPerInstance);
            }
        }

        [DefaultValue(8)]
        public int MaxStuckCountSmall
        {
            get { return _maxStuckCountSmall; }
            set
            {
                if (value == _maxStuckCountSmall) return;
                _maxStuckCountSmall = value;
                NotifyPropertyChanged(() => MaxStuckCountSmall);
            }
        }

        [DefaultValue(15)]
        public int MaxStuckCountMedium
        {
            get { return _maxStuckCountMedium; }
            set
            {
                if (value == _maxStuckCountMedium) return;
                _maxStuckCountMedium = value;
                NotifyPropertyChanged(() => MaxStuckCountMedium);
            }
        }

        [DefaultValue(30)]
        public int MaxStuckCountLarge
        {
            get { return _maxStuckCountLarge; }
            set
            {
                if (value == _maxStuckCountLarge) return;
                _maxStuckCountLarge = value;
                NotifyPropertyChanged(() => MaxStuckCountLarge);
            }
        }

        #endregion

        #region Misc

        private bool _globalUseReactionWait;
        private bool _sellExcessPortals;
        private bool _openStrongboxes;
        private Rarity _maxStrongboxRarity;

        [DefaultValue(true)]
        public bool GlobalUseReactionWait
        {
            get { return _globalUseReactionWait; }
            set
            {
                if (value == _globalUseReactionWait) return;
                _globalUseReactionWait = value;
                NotifyPropertyChanged(() => GlobalUseReactionWait);
            }
        }

        [DefaultValue(true)]
        public bool SellExcessPortals
        {
            get { return _sellExcessPortals; }
            set
            {
                if (value == _sellExcessPortals) return;
                _sellExcessPortals = value;
                NotifyPropertyChanged(() => SellExcessPortals);
            }
        }

        [DefaultValue(true)]
        public bool OpenStrongboxes
        {
            get { return _openStrongboxes; }
            set
            {
                if (value == _openStrongboxes) return;
                _openStrongboxes = value;
                NotifyPropertyChanged(() => OpenStrongboxes);
            }
        }

        [DefaultValue(Rarity.Rare)]
        public Rarity MaxStrongboxRarity
        {
            get { return _maxStrongboxRarity; }
            set
            {
                if (value == _maxStrongboxRarity) return;
                _maxStrongboxRarity = value;
                NotifyPropertyChanged(() => MaxStrongboxRarity);
            }
        }

        [JsonIgnore]
        public List<Rarity> RarityList { get; } = new List<Rarity> {Rarity.Normal, Rarity.Magic, Rarity.Rare, Rarity.Unique};

        #endregion
    }
}