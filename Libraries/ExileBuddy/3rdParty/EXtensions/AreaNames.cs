﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using Loki.Game;
using Loki.Game.GameData;

namespace EXtensions
{
    [SuppressMessage("ReSharper", "UnassignedReadonlyField")]
    public static class AreaNames
    {
        /************************************
         *             Act 1
         ***********************************/

        [AreaId("1_1_town")]
        public static readonly string LioneyeWatch;

        [AreaId("1_1_1")]
        public static readonly string TwilightStrand;

        [AreaId("1_1_2")]
        public static readonly string Coast;

        [AreaId("1_1_2a")]
        public static readonly string TidalIsland;

        [AreaId("1_1_3")]
        public static readonly string MudFlats;

        [AreaId("1_1_3a")]
        public static readonly string FetidPool;

        [AreaId("1_1_4_0")]
        public static readonly string FloodedDepths;

        [AreaId("1_1_4_1")]
        public static readonly string SubmergedPassage;

        [AreaId("1_1_5")]
        public static readonly string Ledge;

        [AreaId("1_1_6")]
        public static readonly string Climb;

        [AreaId("1_1_7_1")]
        public static readonly string LowerPrison;

        [AreaId("1_1_7_2")]
        public static readonly string UpperPrison;

        [AreaId("1_1_8")]
        public static readonly string PrisonerGate;

        [AreaId("1_1_9")]
        public static readonly string ShipGraveyard;

        [AreaId("1_1_9a")]
        public static readonly string ShipGraveyardCave;

        [AreaId("1_1_11_1")]
        public static readonly string CavernOfWrath;

        [AreaId("1_1_11_2")]
        public static readonly string CavernOfAnger;


        /************************************
         *             Act 2
         ***********************************/

        [AreaId("1_2_town")]
        public static readonly string ForestEncampment;

        [AreaId("1_2_1")]
        public static readonly string SouthernForest;

        [AreaId("1_2_2")]
        public static readonly string OldFields;

        [AreaId("1_2_2a")]
        public static readonly string Den;

        [AreaId("1_2_3")]
        public static readonly string Crossroads;

        [AreaId("1_2_4")]
        public static readonly string BrokenBridge;

        [AreaId("1_2_5_1")]
        public static readonly string Crypt1;

        [AreaId("1_2_5_2")]
        public static readonly string Crypt2;

        [AreaId("1_2_6_1")]
        public static readonly string ChamberOfSins1;

        [AreaId("1_2_6_3")]
        public static readonly string ChamberOfSins2;

        [AreaId("1_2_7")]
        public static readonly string Riverways;

        [AreaId("1_2_8")]
        public static readonly string NorthernForest;

        [AreaId("1_2_9")]
        public static readonly string WesternForest;

        [AreaId("1_2_10")]
        public static readonly string WeaverChambers;

        [AreaId("1_2_11_1")]
        public static readonly string VaalRuins;

        [AreaId("1_2_12")]
        public static readonly string Wetlands;

        [AreaId("1_2_13")]
        public static readonly string DreadThicket;

        [AreaId("1_2_14_2")]
        public static readonly string Caverns;

        [AreaId("1_2_14_3")]
        public static readonly string AncientPyramid;

        [AreaId("1_2_15")]
        public static readonly string FellshrineRuins;


        /************************************
         *             Act 3
         ***********************************/

        [AreaId("1_3_town")]
        public static readonly string SarnEncampment;

        [AreaId("1_3_1")]
        public static readonly string CityOfSarn;

        [AreaId("1_3_2")]
        public static readonly string Slums;

        [AreaId("1_3_3_1")]
        public static readonly string Crematorium;

        [AreaId("1_3_4")]
        public static readonly string WarehouseDistrict;

        [AreaId("1_3_5")]
        public static readonly string Marketplace;

        [AreaId("1_3_6_1")]
        public static readonly string Catacombs;

        [AreaId("1_3_7")]
        public static readonly string Battlefront;

        [AreaId("1_3_8_1")]
        public static readonly string SolarisTemple1;

        [AreaId("1_3_8_2")]
        public static readonly string SolarisTemple2;

        [AreaId("3_3_8_4")]
        public static readonly string EternalLaboratory;

        [AreaId("1_3_9")]
        public static readonly string Docks;

        [AreaId("1_3_10_0")]
        public static readonly string SlumSewers;

        [AreaId("1_3_10_1")]
        public static readonly string WarehouseSewers;

        [AreaId("1_3_10_2")]
        public static readonly string MarketSewers;

        [AreaId("1_3_13")]
        public static readonly string EbonyBarracks;

        [AreaId("1_3_14_1")]
        public static readonly string LunarisTemple1;

        [AreaId("1_3_14_3")]
        public static readonly string LunarisTemple2;

        [AreaId("1_3_15")]
        public static readonly string ImperialGardens;

        [AreaId("1_3_16")]
        public static readonly string HedgeMaze;

        [AreaId("1_3_17_1")]
        public static readonly string Library;

        [AreaId("1_3_17_2")]
        public static readonly string Archives;

        [AreaId("1_3_18_1")]
        public static readonly string SceptreOfGod;

        [AreaId("1_3_18_2")]
        public static readonly string UpperSceptreOfGod;


        /************************************
         *             Act 4
         ***********************************/

        [AreaId("1_4_town")]
        public static readonly string Highgate;

        [AreaId("1_4_1")]
        public static readonly string Aqueduct;

        [AreaId("1_4_2")]
        public static readonly string DriedLake;

        [AreaId("1_4_3_1")]
        public static readonly string Mines1;

        [AreaId("1_4_3_2")]
        public static readonly string Mines2;

        [AreaId("1_4_3_3")]
        public static readonly string CrystalVeins;

        [AreaId("1_4_4_1")]
        public static readonly string KaomDream;

        [AreaId("1_4_4_2")]
        public static readonly string KaomPath;

        [AreaId("1_4_4_3")]
        public static readonly string KaomStronghold;

        [AreaId("1_4_5_1")]
        public static readonly string DaressoDream;

        [AreaId("1_4_5_2")]
        public static readonly string GrandArena;

        [AreaId("1_4_6_1")]
        public static readonly string BellyOfTheBeast1;

        [AreaId("1_4_6_2")]
        public static readonly string BellyOfTheBeast2;

        [AreaId("1_4_6_3")]
        public static readonly string Harvest;

        public static string Current => LokiPoe.CurrentWorldArea.Name;

        static AreaNames()
        {
            Dictionary<string, FieldInfo> namesMap = new Dictionary<string, FieldInfo>();
            foreach (var field in typeof (AreaNames).GetFields())
            {
                var idAttribute = field.GetCustomAttribute<AreaId>();
                if (idAttribute == null) continue;
                namesMap.Add(idAttribute.Id, field);
            }

            int processed = 0;
            int total = namesMap.Count;
            foreach (var area in Dat.WorldAreas)
            {
                if (processed >= total) break;
                FieldInfo field;
                if (namesMap.TryGetValue(area.Id, out field))
                {
                    field.SetValue(null, area.Name);
                    ++processed;
                }
            }
        }

        [AttributeUsage(AttributeTargets.Field)]
        public class AreaId : Attribute
        {
            public AreaId(string id)
            {
                Id = id;
            }

            public string Id { get; set; }
        }
    }
}