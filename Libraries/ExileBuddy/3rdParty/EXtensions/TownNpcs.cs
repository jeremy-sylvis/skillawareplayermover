﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.Objects;
using DialogUi = Loki.Game.LokiPoe.InGameState.NpcDialogUi;

namespace EXtensions
{
    public static class TownNpcs
    {
        public static readonly TownNpc Nessa = new TownNpc(StaticPositions.Nessa);
        public static readonly TownNpc Bestel = new TownNpc(StaticPositions.Bestel);
        public static readonly TownNpc Tarkleigh = new TownNpc(StaticPositions.Tarkleigh);
        public static readonly TownNpc Greust = new TownNpc(StaticPositions.Greust);
        public static readonly TownNpc Eramir = new TownNpc(StaticPositions.Eramir);
        public static readonly TownNpc Yeena = new TownNpc(StaticPositions.Yeena);
        public static readonly TownNpc Silk = new TownNpc(StaticPositions.Silk);
        public static readonly TownNpc Helena = new TownNpc(StaticPositions.Helena);
        public static readonly TownNpc Clarissa = new TownNpc(StaticPositions.Clarissa);
        public static readonly TownNpc Hargan = new TownNpc(StaticPositions.Hargan);
        public static readonly TownNpc Maramoa = new TownNpc(StaticPositions.Maramoa);
        public static readonly TownNpc Grigor = new TownNpc(StaticPositions.Grigor);
        public static readonly TownNpc Kira = new TownNpc(StaticPositions.Kira);
        public static readonly TownNpc PetarusAndVanja = new TownNpc(StaticPositions.PetarusAndVanja);
        public static readonly TownNpc Tasuni = new TownNpc(StaticPositions.Tasuni);
        public static readonly TownNpc Dialla = new TownNpc(StaticPositions.Dialla);
        public static readonly TownNpc Oyun = new TownNpc(StaticPositions.Oyun);

        public static TownNpc CreateCustom(NetworkObject npc)
        {
            return new TownNpc(npc);
        }

        public static TownNpc GetSellVendor()
        {
            if (LokiPoe.Me.IsInHideout)
            {
                var master = LokiPoe.ObjectManager.Objects
                    .OfType<Npc>()
                    .Where(n => n.IsTargetable && n.Name != "Navali")
                    .OrderBy(npc => npc.Distance)
                    .FirstOrDefault();

                if (master == null)
                {
                    GlobalLog.Error("[GetSellVendor] Cannot find any targetable master in hideout.");
                    return null;
                }
                return new TownNpc(master.WalkablePosition());
            }

            if (LokiPoe.Me.IsInTown)
            {
                switch (LokiPoe.CurrentWorldArea.Act)
                {
                    case 4:
                        return Kira;
                    case 3:
                        return Clarissa;
                    case 2:
                        return Greust;
                    case 1:
                        return Nessa;
                }
            }
            GlobalLog.Error($"[GetSellVendor] Unsupported area for sell vendor \"{LokiPoe.CurrentWorldArea.Name}\".");
            return null;
        }

        public static TownNpc GetCurrencyVendor()
        {
            switch (LokiPoe.CurrentWorldArea.Act)
            {
                case 4:
                    return PetarusAndVanja;

                case 3:
                    return Clarissa;

                case 2:
                    return Yeena;

                case 1:
                    return Nessa;
            }

            if (LokiPoe.Me.IsInHideout)
            {
                GlobalLog.Error("[GetCurrencyVendor] Cannot buy currency in hideout.");
                return null;
            }
            GlobalLog.Error($"[GetCurrencyVendor] Unsupported area for currency vendor \"{LokiPoe.CurrentWorldArea.Name}\".");
            return null;
        }

        public class TownNpc
        {
            //used only for custom NPCs
            private readonly string _metadata;

            public WalkablePosition Position { get; }

            public NetworkObject GetNpcObject()
            {
                if (_metadata != null)
                {
                    return LokiPoe.ObjectManager.Objects
                        .FirstOrDefault(o => o.Metadata == _metadata);
                }
                return LokiPoe.ObjectManager.Objects
                    .OfType<Npc>()
                    .FirstOrDefault(n => n.Name == Position.Name);
            }

            internal TownNpc(WalkablePosition position)
            {
                Position = position;
            }

            internal TownNpc(NetworkObject obj)
            {
                Position = obj.WalkablePosition();
                _metadata = obj.Metadata;
            }

            public async Task<bool> Talk()
            {
                await Position.ComeAtOnce();
                var npc = GetNpcObject();
                if (npc == null)
                {
                    GlobalLog.Error($"[Talk] Cannot find NPC with name \"{Position.Name}\".");
                    return false;
                }
                return await PlayerAction.Interact(npc, () => DialogUi.IsOpened || LokiPoe.InGameState.RewardUi.IsOpened, "dialog panel opening");
            }

            public async Task<bool> OpenDialogPanel()
            {
                if (!await Talk()) return false;

                if (LokiPoe.InGameState.RewardUi.IsOpened || DialogUi.DialogDepth != 1)
                {
                    const int attempts = 15;
                    for (int i = 1; i <= attempts; ++i)
                    {
                        GlobalLog.Debug($"[OpenDialogPanel] Pressing ESC to close the topmost NPC dialog ({i}/{attempts}).");

                        LokiPoe.Input.SimulateKeyEvent(Keys.Escape, true, false, false);
                        await Wait.SleepSafe(200);

                        if (Settings.Instance.GlobalUseReactionWait) await Coroutines.ReactionWait();

                        if (!LokiPoe.InGameState.RewardUi.IsOpened && DialogUi.DialogDepth == 1)
                            return true;
                    }
                    GlobalLog.Error("[OpenDialogPanel] Fail to bring dialog panel to the top.");
                    return false;
                }
                if (Settings.Instance.GlobalUseReactionWait) await Coroutines.ReactionWait();
                return true;
            }

            private async Task<bool> OpenRewardPanel(string partialDialogName = null)
            {
                if (!await OpenDialogPanel()) return false;

                if (partialDialogName == null) partialDialogName = "reward";

                var dialog = DialogUi.DialogEntries.FirstOrDefault(d => d.Text.ContainsIgnorecase(partialDialogName))?.Text;
                if (dialog == null)
                {
                    GlobalLog.Error($"[OpenRewardPanel] Fail to find any dialog with \"{partialDialogName}\" in it.");
                    return false;
                }

                var conversed = DialogUi.Converse(dialog);
                if (conversed != LokiPoe.InGameState.ConverseResult.None)
                {
                    GlobalLog.Error($"[OpenRewardPanel] Fail to converse \"{dialog}\". Error: \"{conversed}\".");
                    return false;
                }

                if (!await Wait.For(() => LokiPoe.InGameState.RewardUi.IsOpened, "reward panel opening"))
                    return false;

                GlobalLog.Debug($"[OpenRewardPanel] \"{dialog}\" has need successfully opened.");
                if (Settings.Instance.GlobalUseReactionWait) await Coroutines.ReactionWait();
                return true;
            }

            public async Task<bool> TakeReward(string reward = null, string dialogName = null)
            {
                GlobalLog.Debug($"[TakeReward] Now going to take \"{reward ?? "Any"}\" from {Position.Name}.");

                if (!await OpenRewardPanel(dialogName)) return false;

                int itemId;
                if (string.IsNullOrEmpty(reward) || reward.EqualsIgnorecase("any"))
                {
                    var item = LokiPoe.InGameState.RewardUi.InventoryControl.Inventory.Items.First();
                    reward = string.Copy(item.FullName);
                    itemId = item.LocalId;
                }
                else
                {
                    var item = LokiPoe.InGameState.RewardUi.InventoryControl.Inventory.Items.FirstOrDefault(i => i.FullName == reward || i.Name == reward);
                    if (item == null)
                    {
                        GlobalLog.Error($"[TakeReward] Fail to find \"{reward}\" in reward inventory.");
                        ErrorManager.ReportCriticalError();
                        return false;
                    }
                    itemId = item.LocalId;
                }

                int itemsAmount = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.Items.Count;

                var moved = LokiPoe.InGameState.RewardUi.InventoryControl.FastMove(itemId);
                if (moved != FastMoveResult.None)
                {
                    GlobalLog.Error($"[TakeReward] Fail to take \"{reward}\" from {Position.Name}. Error: \"{moved}\".");
                    return false;
                }
                if (await Wait.For(() => LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.Items.Count == itemsAmount + 1,
                    "quest reward appear in inventory"))
                {
                    GlobalLog.Debug($"[TakeReward] \"{reward}\" has been successfully taken from {Position.Name}.");
                    if (Settings.Instance.GlobalUseReactionWait) await Coroutines.ReactionWait();
                    await Coroutines.CloseBlockingWindows();
                    return true;
                }
                return false;
            }

            public async Task<bool> OpenSellPanel()
            {
                if (LokiPoe.InGameState.SellUi.IsOpened) return true;

                GlobalLog.Debug($"[OpenSellPanel] Now going to open sell dialog with {Position.Name}.");

                if (!await OpenDialogPanel()) return false;

                var conversed = DialogUi.Converse("Sell Items");
                if (conversed != LokiPoe.InGameState.ConverseResult.None)
                {
                    GlobalLog.Error($"[OpenSellPanel] Fail to converse \"Sell Items\". Error: \"{conversed}\".");
                    return false;
                }

                if (!await Wait.For(() => LokiPoe.InGameState.SellUi.IsOpened, "sell panel opening"))
                    return false;

                GlobalLog.Debug("[OpenSellPanel] Sell panel has been successfully opened.");
                if (Settings.Instance.GlobalUseReactionWait) await Coroutines.ReactionWait();
                return true;
            }

            public async Task<bool> OpenPurchasePanel()
            {
                if (LokiPoe.InGameState.PurchaseUi.IsOpened) return true;

                GlobalLog.Debug($"[OpenPurchasePanel] Now going to open purchase dialog with {Position.Name}.");

                if (!await OpenDialogPanel()) return false;

                var conversed = DialogUi.Converse("Purchase Items");
                if (conversed != LokiPoe.InGameState.ConverseResult.None)
                {
                    GlobalLog.Error($"[OpenPurchasePanel] Fail to converse \"Purchase Items\". Error: \"{conversed}\".");
                    return false;
                }
                if (await Wait.For(() => LokiPoe.InGameState.PurchaseUi.IsOpened, "purchase panel opening"))
                {
                    GlobalLog.Debug("[OpenPurchasePanel] Purchase panel has been successfully opened.");
                    if (Settings.Instance.GlobalUseReactionWait) await Coroutines.ReactionWait();
                    return true;
                }
                return false;
            }
        }

        public static async Task<bool> SellItems(List<Vector2i> itemPositions)
        {
            if (itemPositions == null)
            {
                GlobalLog.Error("[SellItems] Item list for sell is null.");
                return false;
            }
            if (itemPositions.Count == 0)
            {
                GlobalLog.Error("[SellItems] Item list for sell is empty.");
                return false;
            }

            var vendor = GetSellVendor();
            if (vendor == null) return false;

            if (!await vendor.OpenSellPanel()) return false;

            itemPositions.Sort(Position.Comparer.Instance);
            foreach (var itemPos in itemPositions)
            {
                var item = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.FindItemByPos(itemPos);
                if (item == null)
                {
                    GlobalLog.Error($"[SellItems] Fail to find item at {itemPos}. Skipping it.");
                    continue;
                }
                if (!LokiPoe.InGameState.SellUi.TradeControl.InventoryControl_YourOffer.Inventory.CanFitItem(item.Size)) break;
                if (!await Inventories.FastMoveToVendor(itemPos)) return false;
            }
            var accepted = LokiPoe.InGameState.SellUi.TradeControl.Accept();
            if (accepted != TradeResult.None)
            {
                GlobalLog.Error($"[SellItems] Fail to accept sell. Error: \"{accepted}\".");
                return false;
            }
            if (!await Wait.For(() => !LokiPoe.InGameState.SellUi.IsOpened, "sell panel closing"))
            {
                await Coroutines.CloseBlockingWindows();
                return false;
            }
            // we have to do a static delay here, wait for server response that items are despawned from inventory
            // if we do not - weird errors can occur
            await Wait.SleepSafe(200);
            return true;
        }
    }
}