﻿using System.Threading.Tasks;
using Loki.Bot;
using Loki.Common;
using Loki.Game;

namespace EXtensions
{
    public static class Move
    {
        private static readonly Interval LogInterval = new Interval(1000);

        public static bool Towards(Vector2i pos, string destionation)
        {
            if (LogInterval.Elapsed)
            {
                GlobalLog.Debug($"[MoveTowards] Moving towards {destionation} at {pos} (distance: {LokiPoe.MyPosition.Distance(pos)})");
            }

            if (!PlayerMover.MoveTowards(pos))
            {
                GlobalLog.Error($"[MoveTowards] Fail to move towards {destionation} at {pos}");
                return false;
            }
            return true;
        }

        public static void TowardsWalkable(Vector2i pos, string destionation)
        {
            if (!Towards(pos, destionation))
            {
                GlobalLog.Error($"[MoveTowardsWalkable] Unexpected error. Fail to move towards {destionation} at {pos}");
                ErrorManager.ReportError();
            }
        }

        public static async Task AtOnce(Vector2i pos, string destionation, int minDistance = 15)
        {
            if (LokiPoe.MyPosition.Distance(pos) <= minDistance) return;
            while (LokiPoe.MyPosition.Distance(pos) > minDistance)
            {
                if (LogInterval.Elapsed)
                {
                    await Coroutines.CloseBlockingWindows();
                    GlobalLog.Debug($"[MoveAtOnce] Moving to {destionation} at {pos} (distance: {LokiPoe.MyPosition.Distance(pos)})");
                }
                if (LokiPoe.Me.IsDead || BotManager.IsStopping) return;
                TowardsWalkable(pos, destionation);
                await Wait.Sleep(50);
            }
            await Coroutines.FinishCurrentAction();
        }
    }
}