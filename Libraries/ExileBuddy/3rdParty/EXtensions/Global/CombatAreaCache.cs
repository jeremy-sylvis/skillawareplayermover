﻿using System;
using System.Collections.Generic;
using System.Linq;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace EXtensions.Global
{
    public class CombatAreaCache
    {
        private static readonly Interval ScanInterval = new Interval(200);
        private static readonly Interval ItemScanInterval = new Interval(25);

        // Overorld, Map, Labyrinth
        private static CombatAreaCache _mainArea;

        // Corrupted or Master areas
        private static CombatAreaCache _subArea;

        public static CombatAreaCache Current
        {
            get
            {
                var currentArea = LokiPoe.CurrentWorldArea;
                if (currentArea.IsOverworldArea || currentArea.IsMap || currentArea.IsLabyrinthArea)
                {
                    if (_mainArea == null || _mainArea.Hash != LokiPoe.LocalData.AreaHash)
                    {
                        _mainArea = new CombatAreaCache();

                        //upon entering a new main area, clear current sub area aswell
                        _subArea = null;
                    }
                    return _mainArea;
                }

                //return a sub area for any other area type (just in case)
                if (_subArea == null || _subArea.Hash != LokiPoe.LocalData.AreaHash)
                {
                    _subArea = new CombatAreaCache();
                }
                return _subArea;
            }
        }

        public uint Hash { get; }
        public DatWorldAreaWrapper WorldArea { get; }
        public IExplorer Explorer { get; }

        public readonly Dictionary<int, ItemPosition> ItemPositions = new Dictionary<int, ItemPosition>();
        public readonly Dictionary<int, WalkablePosition> ChestPositions = new Dictionary<int, WalkablePosition>();
        public readonly Dictionary<int, WalkablePosition> StrongboxPositions = new Dictionary<int, WalkablePosition>();
        public readonly Dictionary<int, WalkablePosition> ShrinePositions = new Dictionary<int, WalkablePosition>();
        public readonly Dictionary<int, WorldPosition> MonsterPositions = new Dictionary<int, WorldPosition>();
        public readonly ObjectDictionary Storage = new ObjectDictionary();

        private readonly HashSet<int> _processedItems = new HashSet<int>();

        private CombatAreaCache()
        {
            Hash = LokiPoe.LocalData.AreaHash;
            WorldArea = LokiPoe.CurrentWorldArea;
            Explorer = new GridExplorer { AutoResetOnAreaChange = false, TileSeenRadius = 4 };
            Explorer.Start();
            Loki.Bot.Explorer.Delegate = user => Explorer;
            GlobalLog.Info($"[CombatAreaCache] Creating cache for \"{WorldArea.Name}\".");
        }

        public static void Start()
        {
            _mainArea?.Explorer.Start();
            _subArea?.Explorer.Start();
            ItemEvaluator.OnRefreshed += ItemEvaluatorOnOnRefresh;
        }

        public static void Stop()
        {
            _mainArea?.Explorer.Stop();
            _subArea?.Explorer.Stop();
            ItemEvaluator.OnRefreshed -= ItemEvaluatorOnOnRefresh;
        }

        public static void Tick()
        {
            if (!LokiPoe.IsInGame || LokiPoe.Me.IsDead || !LokiPoe.CurrentWorldArea.IsCombatArea())
                return;

            Current.OnTick();
        }

        private void OnTick()
        {
            Current.Explorer.Tick();

            if (ItemScanInterval.Elapsed)
            {
                WorldItemScan();
            }

            if (ScanInterval.Elapsed)
            {
                foreach (var obj in LokiPoe.ObjectManager.Objects)
                {
                    var mob = obj as Monster;
                    if (mob != null)
                    {
                        try
                        {
                            ProcessMonster(mob);
                        }
                        catch (Exception ex)
                        {
                            GlobalLog.Debug($"[CombatAreaCache] Exception during monster processing.\n{ex.Message}");
                        }
                        continue;
                    }

                    var chest = obj as Chest;
                    if (chest != null)
                    {
                        try
                        {
                            ProcessChest(chest);
                        }
                        catch (Exception ex)
                        {
                            GlobalLog.Debug($"[CombatAreaCache] Exception during chest processing.\n{ex.Message}");
                        }
                        continue;
                    }

                    var shrine = obj as Shrine;
                    if (shrine != null)
                    {
                        try
                        {
                            ProcessShrine(shrine);
                        }
                        catch (Exception ex)
                        {
                            GlobalLog.Debug($"[CombatAreaCache] Exception during shrine processing.\n{ex.Message}");
                        }
                    }
                }

                var toRemove = new List<int>();
                foreach (var mobPos in MonsterPositions)
                {
                    var mob = LokiPoe.ObjectManager.GetObjectById<Monster>(mobPos.Key);
                    if (mob != null)
                    {
                        if (mob.IsDead || mob.Reaction != Reaction.Enemy)
                        {
                            toRemove.Add(mobPos.Key);
                        }
                    }
                    else
                    {
                        if (mobPos.Value.Distance <= 80)
                        {
                            toRemove.Add(mobPos.Key);
                        }
                    }
                }
                foreach (var id in toRemove)
                {
                    MonsterPositions.Remove(id);
                }
            }
        }

        private void WorldItemScan()
        {
            foreach (var worldItem in LokiPoe.ObjectManager.Objects.OfType<WorldItem>())
            {
                var id = worldItem.Id;

                if (ItemPositions.ContainsKey(id) || _processedItems.Contains(id) || Blacklist.Contains(id)) continue;
                if (worldItem.IsAllocatedToOther && DateTime.Now < worldItem.PublicTime) continue;

                IItemFilter filter;
                if (ItemEvaluator.Match(worldItem.Item, EvaluationType.PickUp, out filter))
                {
                    var itemPos = new ItemPosition
                    {
                        Position = worldItem.WalkablePosition(2, 10),
                        Size = worldItem.Item.Size,
                        PickupAttempts = 0
                    };
                    ItemPositions.Add(id, itemPos);
                }
                else
                {
                    _processedItems.Add(id);
                }
            }
        }

        private void ProcessMonster(Monster mob)
        {
            if (mob.IsDead || mob.Reaction != Reaction.Enemy) return;
            if (mob.Invincible && !mob.IsMapBoss) return;

            int id = mob.Id;
            if (MonsterPositions.ContainsKey(id) || Blacklist.Contains(id)) return;
            if (SkipThisMob(mob)) return;

            MonsterPositions.Add(id, mob.WorldPosition());
        }

        private void ProcessChest(Chest chest)
        {
            if (chest.IsOpened || chest.OpensOnDamage || !chest.IsTargetable)
                return;

            var id = chest.Id;
            if (Blacklist.Contains(id)) return;

            if (chest.IsStrongBox && chest.Rarity <= Settings.Instance.MaxStrongboxRarity)
            {
                if (!chest.IsLocked && !StrongboxPositions.ContainsKey(chest.Id))
                {
                    var pos = chest.WalkablePosition(20, 5);
                    GlobalLog.Warn($"[OpenChestTask] Registering {pos}");
                    StrongboxPositions.Add(id, pos);
                }
            }
            else
            {
                if (!ChestPositions.ContainsKey(id))
                {
                    ChestPositions.Add(id, chest.WalkablePosition(20, 5));
                }
            }
        }

        private void ProcessShrine(Shrine shrine)
        {
            if (!shrine.IsTargetable || shrine.Name == "Divine Shrine") return;

            var id = shrine.Id;
            var registered = ShrinePositions.ContainsKey(id);
            var isBad = shrine.IsDeactivated || Blacklist.Contains(id);

            if (!registered)
            {
                if (!isBad)
                {
                    var pos = shrine.WalkablePosition(20, 5);
                    GlobalLog.Warn($"[OpenChestTask] Registering {pos}");
                    ShrinePositions.Add(id, pos);
                }
            }
            // lets manually remove activated/blacklisted shrines from cache, because CR will not do that for us 
            else
            {
                if (isBad)
                {
                    GlobalLog.Debug($"[OpenChestTask] \"{shrine.Name}\" is deactivated or blacklisted. Now removing it from cache.");
                    ShrinePositions.Remove(id);
                }
            }
        }

        private static bool SkipThisMob(Monster mob)
        {
            foreach (var affix in mob.ExplicitAffixes)
            {
                string displayName = affix.DisplayName;
                if (displayName == "Necrovigil" ||
                    displayName == "Phylacteral Link" ||
                    displayName == "Voidspawn of Abaxoth" ||
                    affix.InternalName == "MonsterCannotBeDamaged")
                {
                    return true;
                }
            }

            foreach (var aura in mob.Auras)
            {
                string name = aura.Name;
                if (name == "cannot_be_damaged" || //Obelisk totem
                    name == "god_mode" || //Animated Guardian
                    name == "shrine_godmode") //Divine Shrine
                {
                    return true;
                }
            }
            return false;
        }

        private static void ItemEvaluatorOnOnRefresh(object sender, ItemEvaluatorRefreshedEventArgs args)
        {
            GlobalLog.Info("[ItemEvaluatorOnOnRefresh] Now clearing ignored items.");
            _mainArea?._processedItems.Clear();
            _subArea?._processedItems.Clear();
        }

        public class ItemPosition
        {
            public WalkablePosition Position;
            public Vector2i Size;
            public int PickupAttempts;
        }

        public class ObjectDictionary
        {
            private readonly Dictionary<string, object> _dict = new Dictionary<string, object>();

            public object this[string key]
            {
                get
                {
                    object obj;
                    _dict.TryGetValue(key, out obj);
                    return obj;
                }
                set
                {
                    if (!_dict.ContainsKey(key))
                    {
                        GlobalLog.Warn($"[Storage] Registering [{key}] = [{value ?? "null"}]");
                        _dict.Add(key, value);
                    }
                    else
                    {
                        _dict[key] = value;
                    }
                }
            }

            public bool Contains(string key)
            {
                return _dict.ContainsKey(key);
            }
        }
    }
}