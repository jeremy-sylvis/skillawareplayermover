﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EXtensions.Positions;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace EXtensions.Global
{
    public static class Travel
    {
        private static readonly Interval ScanInterval = new Interval(500);

        private static readonly Dictionary<string, WalkablePosition> AreaTransitions = new Dictionary<string, WalkablePosition>();
        private static readonly HashSet<int> ProcessedTransitions = new HashSet<int>();
        private static readonly HashSet<string> NewInstanceRequests = new HashSet<string>();

        public static async Task To(string areaName)
        {
            Func<Task> handler;
            if (Handlers.TryGetValue(areaName, out handler))
            {
                await handler();
            }
            else
            {
                GlobalLog.Error($"[Travel] Unsupported area: \"{areaName}\".");
                ErrorManager.ReportCriticalError();
            }
        }

        public static void RequestNewInstance(string areaName)
        {
            NewInstanceRequests.Add(areaName);
        }

        private static readonly Dictionary<string, Func<Task>> Handlers = new Dictionary<string, Func<Task>>
        {
            [AreaNames.LioneyeWatch] = LioneyeWatch,
            [AreaNames.TwilightStrand] = TwilightStrand,
            [AreaNames.Coast] = Coast,
            [AreaNames.TidalIsland] = TidalIsland,
            [AreaNames.MudFlats] = MudFlats,
            [AreaNames.FetidPool] = FetidPool,
            [AreaNames.SubmergedPassage] = SubmergedPassage,
            [AreaNames.FloodedDepths] = FloodedDepths,
            [AreaNames.Ledge] = Ledge,
            [AreaNames.Climb] = Climb,
            [AreaNames.LowerPrison] = LowerPrison,
            [AreaNames.UpperPrison] = UpperPrison,
            [AreaNames.PrisonerGate] = PrisonersGate,
            [AreaNames.ShipGraveyard] = ShipGraveyard,
            [AreaNames.ShipGraveyardCave] = ShipGraveyardCave,
            [AreaNames.CavernOfWrath] = CavernOfWrath,
            [AreaNames.CavernOfAnger] = CavernOfAnger,
            [AreaNames.SouthernForest] = SouthernForest,
            [AreaNames.ForestEncampment] = ForestEncampment,
            [AreaNames.Riverways] = Riverways,
            [AreaNames.WesternForest] = WesternForest,
            [AreaNames.WeaverChambers] = WeaverChambers,
            [AreaNames.OldFields] = OldFields,
            [AreaNames.Den] = Den,
            [AreaNames.Crossroads] = Crossroads,
            [AreaNames.ChamberOfSins1] = ChamberOfSins1,
            [AreaNames.ChamberOfSins2] = ChamberOfSins2,
            [AreaNames.BrokenBridge] = BrokenBridge,
            [AreaNames.FellshrineRuins] = FellshrineRuins,
            [AreaNames.Crypt1] = Crypt1,
            [AreaNames.Crypt2] = Crypt2,
            [AreaNames.Wetlands] = Wetlands,
            [AreaNames.VaalRuins] = VaalRuins,
            [AreaNames.NorthernForest] = NorthernForest,
            [AreaNames.DreadThicket] = DreadThicket,
            [AreaNames.Caverns] = Caverns,
            [AreaNames.AncientPyramid] = AncientPyramid,
            [AreaNames.CityOfSarn] = CityOfSarn,
            [AreaNames.SarnEncampment] = SarnEncampment,
            [AreaNames.Slums] = Slums,
            [AreaNames.Crematorium] = Crematorium,
            [AreaNames.SlumSewers] = SlumSewers,
            [AreaNames.WarehouseSewers] = WarehouseSewers,
            [AreaNames.WarehouseDistrict] = WarehouseDistrict,
            [AreaNames.Marketplace] = Marketplace,
            [AreaNames.MarketSewers] = MarketSewers,
            [AreaNames.Catacombs] = Catacombs,
            [AreaNames.Battlefront] = Battlefront,
            [AreaNames.Docks] = Docks,
            [AreaNames.SolarisTemple1] = Solaris1,
            [AreaNames.SolarisTemple2] = Solaris2,
            [AreaNames.EbonyBarracks] = EbonyBarracks,
            [AreaNames.LunarisTemple1] = Lunaris1,
            [AreaNames.LunarisTemple2] = Lunaris2,
            [AreaNames.ImperialGardens] = ImperialGardens,
            [AreaNames.HedgeMaze] = HedgeMaze,
            [AreaNames.Library] = Library,
            [AreaNames.Archives] = Archives,
            [AreaNames.SceptreOfGod] = SceptreOfGod,
            [AreaNames.UpperSceptreOfGod] = UpperSceptreOfGod,
            [AreaNames.Aqueduct] = Aqueduct,
            [AreaNames.Highgate] = Highgate,
            [AreaNames.DriedLake] = DriedLake,
            [AreaNames.Mines1] = Mines1,
            [AreaNames.Mines2] = Mines2,
            [AreaNames.CrystalVeins] = CrystalVeins,
            [AreaNames.KaomDream] = KaomDream,
            [AreaNames.KaomPath] = KaomPath,
            [AreaNames.KaomStronghold] = KaomStronghold,
            [AreaNames.DaressoDream] = DaressoDream,
            [AreaNames.GrandArena] = GrandArena,
            [AreaNames.BellyOfTheBeast1] = Belly1,
            [AreaNames.BellyOfTheBeast2] = Belly2,
            [AreaNames.Harvest] = Harvest
        };

        #region Handlers

        #region Act 1

        private static async Task TwilightStrand()
        {
            if (AreaNames.Current == AreaNames.TwilightStrand)
            {
                OuterLogicError(AreaNames.TwilightStrand);
                return;
            }
            if (IsInWaypointArea)
            {
                var diff = LokiPoe.CurrentWorldArea.Difficulty;

                if (diff != Difficulty.Merciless && World.HasWaypoint(AreaNames.TwilightStrand, diff + 1))
                {
                    await TakeWaypoint(AreaNames.TwilightStrand, diff + 1);
                }
                else
                {
                    await TakeWaypoint(AreaNames.TwilightStrand);
                }
                return;
            }
            await TpToTown();
        }

        private static async Task LioneyeWatch()
        {
            await WpAreaHandler(AreaNames.LioneyeWatch, LioneyeWatchTgt, AreaNames.TwilightStrand, TwilightStrand);
        }

        private static async Task Coast()
        {
            if (AreaNames.Current == AreaNames.Coast)
            {
                OuterLogicError(AreaNames.LioneyeWatch);
                return;
            }
            if (IsInWaypointArea && World.HasWaypoint(AreaNames.Coast))
            {
                await TakeWaypoint(AreaNames.Coast);
                return;
            }
            if (AreaNames.Current == AreaNames.LioneyeWatch)
            {
                await CoastTransitionPos.ComeAtOnce();
                if (!await PlayerAction.TakeTransitionByName(AreaNames.Coast)) ErrorManager.ReportError();
                return;
            }
            await LioneyeWatch();
        }

        private static async Task TidalIsland()
        {
            await NoWpAreaHandler(AreaNames.TidalIsland, TidalIslandTgt, AreaNames.Coast, Coast);
        }

        private static async Task MudFlats()
        {
            await NoWpAreaHandler(AreaNames.MudFlats, MudFlatsTgt, AreaNames.Coast, Coast);
        }

        private static async Task FetidPool()
        {
            await NoWpAreaHandler(AreaNames.FetidPool, FetidPoolTgt, AreaNames.MudFlats, MudFlats);
        }

        private static async Task SubmergedPassage()
        {
            await WpAreaHandler(AreaNames.SubmergedPassage, SubmergedPassageTgt, AreaNames.MudFlats, MudFlats);
        }

        private static async Task FloodedDepths()
        {
            await NoWpAreaHandler(AreaNames.FloodedDepths, FloodedDepthsTgt, AreaNames.SubmergedPassage, SubmergedPassage);
        }

        private static async Task Ledge()
        {
            await WpAreaHandler(AreaNames.Ledge, LedgeTgt, AreaNames.SubmergedPassage, SubmergedPassage);
        }

        private static async Task Climb()
        {
            await WpAreaHandler(AreaNames.Climb, ClimbTgt, AreaNames.Ledge, Ledge);
        }

        private static async Task LowerPrison()
        {
            await WpAreaHandler(AreaNames.LowerPrison, LowerPrisonTgt, AreaNames.Climb, Climb);
        }

        private static async Task UpperPrison()
        {
            await WpAreaHandler(AreaNames.UpperPrison, UpperPrisonTgt, AreaNames.LowerPrison, LowerPrison, () => NextPrisonLevelTgt.ResetCurrentPosition());
        }

        private static async Task PrisonersGate()
        {
            if (AreaNames.Current == AreaNames.PrisonerGate)
            {
                OuterLogicError(AreaNames.PrisonerGate);
                return;
            }
            if (AreaNames.Current == AreaNames.UpperPrison)
            {
                await MoveAndEnterMultilevel(AreaNames.PrisonerGate, NextPrisonLevelTgt);
                return;
            }
            if (World.HasWaypoint(AreaNames.PrisonerGate))
            {
                if (IsInWaypointArea)
                {
                    await TakeWaypoint(AreaNames.PrisonerGate);
                }
                else
                {
                    await TpToTown();
                }
                return;
            }
            await UpperPrison();
        }

        private static async Task ShipGraveyard()
        {
            await WpAreaHandler(AreaNames.ShipGraveyard, ShipGraveyardTgt, AreaNames.PrisonerGate, PrisonersGate);
        }

        private static async Task ShipGraveyardCave()
        {
            await NoWpAreaHandler(AreaNames.ShipGraveyardCave, ShipGraveyardCaveTgt, AreaNames.ShipGraveyard, ShipGraveyard);
        }

        private static async Task CavernOfWrath()
        {
            await WpAreaHandler(AreaNames.CavernOfWrath, CavernOfWrathTgt, AreaNames.ShipGraveyard, ShipGraveyard);
        }

        private static async Task CavernOfAnger()
        {
            await NoWpAreaHandler(AreaNames.CavernOfAnger, CavernOfAngerTgt, AreaNames.CavernOfWrath, CavernOfWrath, () => SouthernForestTgt.ResetCurrentPosition());
        }

        #endregion

        #region Act 2

        private static async Task SouthernForest()
        {
            if (AreaNames.Current == AreaNames.SouthernForest)
            {
                OuterLogicError(AreaNames.SouthernForest);
                return;
            }
            if (AreaNames.Current == AreaNames.CavernOfAnger)
            {
                await MoveAndEnterMultilevel(AreaNames.SouthernForest, SouthernForestTgt);
                return;
            }
            if (World.HasWaypoint(AreaNames.SouthernForest))
            {
                if (IsInWaypointArea)
                {
                    await TakeWaypoint(AreaNames.SouthernForest);
                }
                else
                {
                    await TpToTown();
                }
                return;
            }
            await CavernOfAnger();
        }

        private static async Task ForestEncampment()
        {
            await WpAreaHandler(AreaNames.ForestEncampment, ForestEncampmentTgt, AreaNames.SouthernForest, SouthernForest);
        }

        private static async Task Riverways()
        {
            if (AreaNames.Current == AreaNames.Riverways)
            {
                OuterLogicError(AreaNames.Riverways);
                return;
            }
            if (IsInWaypointArea && World.HasWaypoint(AreaNames.Riverways))
            {
                await TakeWaypoint(AreaNames.Riverways);
                return;
            }
            if (AreaNames.Current == AreaNames.ForestEncampment)
            {
                await RiverwaysTransitionPos.ComeAtOnce();
                if (!await PlayerAction.TakeTransitionByName(AreaNames.Riverways)) ErrorManager.ReportError();
                return;
            }
            await ForestEncampment();
        }

        private static async Task WesternForest()
        {
            await WpAreaHandler(AreaNames.WesternForest, WesternForestTgt, AreaNames.Riverways, Riverways);
        }

        private static async Task WeaverChambers()
        {
            await NoWpAreaHandler(AreaNames.WeaverChambers, WeaverChambersTgt, AreaNames.WesternForest, WesternForest);
        }

        private static async Task OldFields()
        {
            if (AreaNames.Current == AreaNames.OldFields)
            {
                OuterLogicError(AreaNames.OldFields);
                return;
            }
            if (AreaNames.Current == AreaNames.ForestEncampment)
            {
                await OldFieldsTransitionPos.ComeAtOnce();
                if (!await PlayerAction.TakeTransitionByName(AreaNames.OldFields)) ErrorManager.ReportError();
                return;
            }
            await ForestEncampment();
        }

        private static async Task Den()
        {
            await NoWpAreaHandler(AreaNames.Den, DenTgt, AreaNames.OldFields, OldFields);
        }

        private static async Task Crossroads()
        {
            await WpAreaHandler(AreaNames.Crossroads, CrossroadsTgt, AreaNames.OldFields, OldFields);
        }

        private static async Task ChamberOfSins1()
        {
            await NoWpAreaHandler(AreaNames.ChamberOfSins1, ChamberOfSins1Tgt, AreaNames.Crossroads, Crossroads);
        }

        private static async Task ChamberOfSins2()
        {
            await WpAreaHandler(AreaNames.ChamberOfSins2, ChamberOfSins2Tgt, AreaNames.ChamberOfSins1, ChamberOfSins1);
        }

        private static async Task BrokenBridge()
        {
            await WpAreaHandler(AreaNames.BrokenBridge, BrokenBridgeTgt, AreaNames.Crossroads, Crossroads);
        }

        private static async Task FellshrineRuins()
        {
            if (AreaNames.Current == AreaNames.FellshrineRuins)
            {
                OuterLogicError(AreaNames.FellshrineRuins);
                return;
            }
            if (AreaNames.Current == AreaNames.Crossroads)
            {
                WalkablePosition transitionPos;
                if (AreaTransitions.TryGetValue(AreaNames.FellshrineRuins, out transitionPos))
                {
                    if (transitionPos.IsFar)
                    {
                        transitionPos.Come();
                    }
                    else
                    {
                        if (!await PlayerAction.TakeTransitionByName(AreaNames.FellshrineRuins))
                            ErrorManager.ReportError();
                    }
                    return;
                }
                if (FellshrineTransitionPos.IsFar)
                {
                    FellshrineTransitionPos.Come();
                }
                return;
            }
            await Crossroads();
        }

        private static async Task Crypt1()
        {
            await WpAreaHandler(AreaNames.Crypt1, Crypt1Tgt, AreaNames.FellshrineRuins, FellshrineRuins);
        }

        private static async Task Crypt2()
        {
            await NoWpAreaHandler(AreaNames.Crypt2, Crypt2Tgt, AreaNames.Crypt1, Crypt1);
        }

        private static async Task Wetlands()
        {
            await WpAreaHandler(AreaNames.Wetlands, WetlandsTgt, AreaNames.Riverways, Riverways);
        }

        private static async Task VaalRuins()
        {
            if (AreaNames.Current == AreaNames.VaalRuins)
            {
                OuterLogicError(AreaNames.VaalRuins);
                return;
            }
            if (AreaNames.Current == AreaNames.Wetlands)
            {
                WalkablePosition transitionPos;
                if (AreaTransitions.TryGetValue(AreaNames.VaalRuins, out transitionPos))
                {
                    if (transitionPos.IsFar)
                    {
                        transitionPos.Come();
                    }
                    else
                    {
                        var roots = TreeRoots;
                        if (roots != null && roots.IsTargetable)
                        {
                            if (!await PlayerAction.Interact(roots)) ErrorManager.ReportError();
                            return;
                        }
                        if (!await PlayerAction.TakeTransitionByName(AreaNames.VaalRuins)) ErrorManager.ReportError();
                    }
                    return;
                }
                if (VaalRuinsTgt.IsFar)
                {
                    VaalRuinsTgt.Come();
                }
                return;
            }
            await Wetlands();
        }

        private static async Task NorthernForest()
        {
            if (AreaNames.Current == AreaNames.NorthernForest)
            {
                OuterLogicError(AreaNames.NorthernForest);
                return;
            }
            if (AreaNames.Current == AreaNames.VaalRuins)
            {
                var seal = AncientSeal;
                if (seal != null && seal.IsTargetable && seal.Distance < 20)
                {
                    if (!await PlayerAction.Interact(seal)) ErrorManager.ReportError();
                    return;
                }
                await MoveAndEnter(AreaNames.NorthernForest, NorthernForestTgt, null);
                return;
            }
            if (World.HasWaypoint(AreaNames.NorthernForest))
            {
                if (IsInWaypointArea)
                {
                    await TakeWaypoint(AreaNames.NorthernForest);
                }
                else
                {
                    await TpToTown();
                }
                return;
            }
            await VaalRuins();
        }

        private static async Task DreadThicket()
        {
            await WpAreaHandler(AreaNames.DreadThicket, DreadThicketTgt, AreaNames.NorthernForest, NorthernForest);
        }

        private static async Task Caverns()
        {
            await WpAreaHandler(AreaNames.Caverns, CavernsTgt, AreaNames.NorthernForest, NorthernForest);
        }

        private static async Task AncientPyramid()
        {
            await WpAreaHandler(AreaNames.AncientPyramid, AncientPyramidTgt, AreaNames.Caverns, Caverns, () => NextPyramidLevelTgt.ResetCurrentPosition());
        }

        #endregion

        #region Act 3

        private static async Task CityOfSarn()
        {
            if (AreaNames.Current == AreaNames.CityOfSarn)
            {
                OuterLogicError(AreaNames.CityOfSarn);
                return;
            }
            if (AreaNames.Current == AreaNames.AncientPyramid)
            {
                await MoveAndEnterMultilevel(AreaNames.CityOfSarn, NextPyramidLevelTgt);
                return;
            }
            if (World.HasWaypoint(AreaNames.CityOfSarn))
            {
                if (IsInWaypointArea)
                {
                    await TakeWaypoint(AreaNames.CityOfSarn);
                }
                else
                {
                    await TpToTown();
                }
                return;
            }
            await AncientPyramid();
        }

        private static async Task SarnEncampment()
        {
            await WpAreaHandler(AreaNames.SarnEncampment, SarnEncampmentTgt, AreaNames.CityOfSarn, CityOfSarn);
        }

        private static async Task Slums()
        {
            if (AreaNames.Current == AreaNames.Slums)
            {
                OuterLogicError(AreaNames.Slums);
                return;
            }
            if (AreaNames.Current == AreaNames.SarnEncampment)
            {
                await SlumsTransitionPos.ComeAtOnce();
                if (!await PlayerAction.TakeTransitionByName(AreaNames.Slums)) ErrorManager.ReportError();
                return;
            }
            await SarnEncampment();
        }

        private static async Task Crematorium()
        {
            await WpAreaHandler(AreaNames.Crematorium, CrematoriumTgt, AreaNames.Slums, Slums);
        }

        private static async Task SlumSewers()
        {
            if (AreaNames.Current == AreaNames.SlumSewers)
            {
                OuterLogicError(AreaNames.SlumSewers);
                return;
            }
            if (AreaNames.Current == AreaNames.Slums)
            {
                WalkablePosition transitionPos;
                if (AreaTransitions.TryGetValue(AreaNames.SlumSewers, out transitionPos))
                {
                    if (transitionPos.IsFar)
                    {
                        transitionPos.Come();
                    }
                    else
                    {
                        var grating = SewerGrating;
                        if (grating != null && grating.IsTargetable)
                        {
                            if (!await PlayerAction.Interact(grating)) ErrorManager.ReportError();
                            return;
                        }
                        if (!await PlayerAction.TakeTransitionByName(AreaNames.SlumSewers)) ErrorManager.ReportError();
                    }
                    return;
                }
                if (SlumSewersTgt.IsFar)
                {
                    SlumSewersTgt.Come();
                }
                return;
            }
            await Slums();
        }

        private static async Task WarehouseSewers()
        {
            await WpAreaHandler(AreaNames.WarehouseSewers, WarehouseSewersTgt, AreaNames.SlumSewers, SlumSewers);
        }

        private static async Task WarehouseDistrict()
        {
            await NoWpAreaHandler(AreaNames.WarehouseDistrict, WarehouseDistrictTgt, AreaNames.WarehouseSewers, WarehouseSewers);
        }

        private static async Task Marketplace()
        {
            await WpAreaHandler(AreaNames.Marketplace, MarketplaceTgt, AreaNames.WarehouseDistrict, WarehouseDistrict);
        }

        private static async Task MarketSewers()
        {
            await NoWpAreaHandler(AreaNames.MarketSewers, MarketSewersTgt, AreaNames.Marketplace, Marketplace);
        }

        private static async Task Catacombs()
        {
            await NoWpAreaHandler(AreaNames.Catacombs, CatacombsTgt, AreaNames.Marketplace, Marketplace);
        }

        private static async Task Battlefront()
        {
            await WpAreaHandler(AreaNames.Battlefront, BattlefrontTgt, AreaNames.Marketplace, Marketplace);
        }

        private static async Task Docks()
        {
            await WpAreaHandler(AreaNames.Docks, DocksTgt, AreaNames.Battlefront, Battlefront);
        }

        private static async Task Solaris1()
        {
            await NoWpAreaHandler(AreaNames.SolarisTemple1, Solaris1Tgt, AreaNames.Battlefront, Battlefront);
        }

        private static async Task Solaris2()
        {
            await WpAreaHandler(AreaNames.SolarisTemple2, Solaris2Tgt, AreaNames.SolarisTemple1, Solaris1);
        }

        private static async Task EbonyBarracks()
        {
            if (AreaNames.Current == AreaNames.EbonyBarracks)
            {
                OuterLogicError(AreaNames.EbonyBarracks);
                return;
            }
            if (AreaNames.Current == AreaNames.WarehouseSewers)
            {
                var blockage = UndyingBlockage;
                if (blockage != null && blockage.IsTargetable && blockage.Distance < 20)
                {
                    if (!await PlayerAction.Interact(blockage)) ErrorManager.ReportError();
                    return;
                }
                await MoveAndEnter(AreaNames.EbonyBarracks, EbonyBarracksTgt, null);
                return;
            }
            if (World.HasWaypoint(AreaNames.EbonyBarracks))
            {
                if (IsInWaypointArea)
                {
                    await TakeWaypoint(AreaNames.EbonyBarracks);
                }
                else
                {
                    await TpToTown();
                }
                return;
            }
            await WarehouseSewers();
        }

        private static async Task Lunaris1()
        {
            await WpAreaHandler(AreaNames.LunarisTemple1, Lunaris1Tgt, AreaNames.EbonyBarracks, EbonyBarracks);
        }

        private static async Task Lunaris2()
        {
            await NoWpAreaHandler(AreaNames.LunarisTemple2, Lunaris2Tgt, AreaNames.LunarisTemple1, Lunaris1);
        }

        private static async Task ImperialGardens()
        {
            await WpAreaHandler(AreaNames.ImperialGardens, ImperialGardensTgt, AreaNames.EbonyBarracks, EbonyBarracks);
        }

        private static async Task HedgeMaze()
        {
            await NoWpAreaHandler(AreaNames.HedgeMaze, HedgeMazeTgt, AreaNames.ImperialGardens, ImperialGardens);
        }

        private static async Task Library()
        {
            await WpAreaHandler(AreaNames.Library, LibraryTgt, AreaNames.ImperialGardens, ImperialGardens);
        }

        private static async Task Archives()
        {
            if (AreaNames.Current == AreaNames.Archives)
            {
                OuterLogicError(AreaNames.Archives);
                return;
            }
            if (AreaNames.Current == AreaNames.Library)
            {
                WalkablePosition transitionPos;
                if (AreaTransitions.TryGetValue(AreaNames.Archives, out transitionPos))
                {
                    if (transitionPos.IsFar)
                    {
                        transitionPos.Come();
                    }
                    else
                    {
                        await EnterTransition(AreaNames.Archives, ArchivesTgt, null);
                    }
                    return;
                }
                if (ArchivesTgt.IsFar)
                {
                    ArchivesTgt.Come();
                }
                else
                {
                    var candle = LooseCandle;
                    if (candle != null && candle.IsTargetable)
                    {
                        if (!await PlayerAction.Interact(candle)) ErrorManager.ReportError();
                    }
                    else
                    {
                        GlobalLog.Debug("Waiting for Archives transition.");
                        StuckDetection.Reset();
                        await Wait.Sleep(200);
                    }
                }
                return;
            }
            await Library();
        }

        private static async Task SceptreOfGod()
        {
            if (AreaNames.Current == AreaNames.SceptreOfGod)
            {
                OuterLogicError(AreaNames.SceptreOfGod);
                return;
            }
            if (AreaNames.Current == AreaNames.ImperialGardens)
            {
                WalkablePosition transitionPos;
                if (AreaTransitions.TryGetValue(AreaNames.SceptreOfGod, out transitionPos))
                {
                    var door = LockedDoor;
                    if (door != null && door.IsTargetable)
                    {
                        var doorPos = door.WalkablePosition();
                        if (doorPos.IsFar)
                        {
                            doorPos.Come();
                        }
                        else
                        {
                            if (!await PlayerAction.Interact(door))
                                ErrorManager.ReportError();
                        }
                        return;
                    }
                    if (transitionPos.IsFar)
                    {
                        transitionPos.Come();
                        return;
                    }
                    if (await PlayerAction.TakeTransitionByName(AreaNames.SceptreOfGod))
                    {
                        NextTowerLevel1Tgt.ResetCurrentPosition();
                    }
                    else
                    {
                        ErrorManager.ReportError();
                    }
                    return;
                }
                if (SceptreOfGodTgt.IsFar)
                {
                    SceptreOfGodTgt.Come();
                }
                return;
            }
            if (World.HasWaypoint(AreaNames.SceptreOfGod))
            {
                if (IsInWaypointArea)
                {
                    if (await PlayerAction.TakeWaypoint(AreaNames.SceptreOfGod))
                    {
                        NextTowerLevel1Tgt.ResetCurrentPosition();
                    }
                    else
                    {
                        ErrorManager.ReportError();
                    }
                }
                else
                {
                    await TpToTown();
                }
                return;
            }
            await ImperialGardens();
        }

        private static async Task UpperSceptreOfGod()
        {
            if (AreaNames.Current == AreaNames.UpperSceptreOfGod)
            {
                OuterLogicError(AreaNames.UpperSceptreOfGod);
                return;
            }
            if (AreaNames.Current == AreaNames.SceptreOfGod)
            {
                if (NextTowerLevel1Tgt.IsFar)
                {
                    NextTowerLevel1Tgt.Come();
                    return;
                }

                var transition = await GetTransitionObject(NextTowerLevel1Tgt, null);
                if (transition == null) return;

                if (!await PlayerAction.TakeTransition(transition))
                {
                    ErrorManager.ReportError();
                    return;
                }

                if (AreaNames.Current != AreaNames.UpperSceptreOfGod)
                {
                    NextTowerLevel1Tgt.ResetCurrentPosition();
                }
                else
                {
                    NextTowerLevel2Tgt.ResetCurrentPosition();
                }
                return;
            }
            await SceptreOfGod();
        }

        #endregion

        #region Act 4

        private static async Task Aqueduct()
        {
            if (AreaNames.Current == AreaNames.Aqueduct)
            {
                OuterLogicError(AreaNames.Aqueduct);
                return;
            }
            if (AreaNames.Current == AreaNames.UpperSceptreOfGod)
            {
                await MoveAndEnterMultilevel(AreaNames.Aqueduct, NextTowerLevel2Tgt);
                return;
            }
            if (World.HasWaypoint(AreaNames.Aqueduct))
            {
                if (IsInWaypointArea)
                {
                    await TakeWaypoint(AreaNames.Aqueduct);
                }
                else
                {
                    await TpToTown();
                }
                return;
            }
            await UpperSceptreOfGod();
        }

        private static async Task Highgate()
        {
            await WpAreaHandler(AreaNames.Highgate, HighgateTgt, AreaNames.Aqueduct, Aqueduct);
        }

        private static async Task DriedLake()
        {
            if (AreaNames.Current == AreaNames.DriedLake)
            {
                OuterLogicError(AreaNames.DriedLake);
                return;
            }
            if (AreaNames.Current == AreaNames.Highgate)
            {
                await DriedLakeTransitionPos.ComeAtOnce();
                if (!await PlayerAction.TakeTransitionByName(AreaNames.DriedLake)) ErrorManager.ReportError();
                return;
            }
            await Highgate();
        }

        private static async Task Mines1()
        {
            if (AreaNames.Current == AreaNames.Mines1)
            {
                OuterLogicError(AreaNames.Mines1);
                return;
            }
            if (AreaNames.Current == AreaNames.Highgate)
            {
                await MinesTransitionPos.ComeAtOnce();

                var seal = DeshretSeal;
                if (seal != null && seal.IsTargetable)
                {
                    if (!await PlayerAction.Interact(seal)) ErrorManager.ReportError();
                    return;
                }
                if (!await PlayerAction.TakeTransitionByName(AreaNames.Mines1)) ErrorManager.ReportError();
                return;
            }
            await Highgate();
        }

        private static async Task Mines2()
        {
            await NoWpAreaHandler(AreaNames.Mines2, Mines2Tgt, AreaNames.Mines1, Mines1);
        }

        private static async Task CrystalVeins()
        {
            await WpAreaHandler(AreaNames.CrystalVeins, CrystalVeinsTgt, AreaNames.Mines2, Mines2);
        }

        private static async Task KaomDream()
        {
            await NoWpAreaHandler(AreaNames.KaomDream, RaptureDeviceTgt, AreaNames.CrystalVeins, CrystalVeins);
        }

        private static async Task KaomPath()
        {
            await NoWpAreaHandler(AreaNames.KaomPath, KaomPathTgt, AreaNames.KaomDream, KaomDream);
        }

        private static async Task KaomStronghold()
        {
            await WpAreaHandler(AreaNames.KaomStronghold, KaomStrongholdTgt, AreaNames.KaomPath, KaomPath);
        }

        private static async Task DaressoDream()
        {
            await NoWpAreaHandler(AreaNames.DaressoDream, RaptureDeviceTgt, AreaNames.CrystalVeins, CrystalVeins);
        }

        private static async Task GrandArena()
        {
            await WpAreaHandler(AreaNames.GrandArena, GrandArenaTgt, AreaNames.DaressoDream, DaressoDream);
        }

        private static async Task Belly1()
        {
            await NoWpAreaHandler(AreaNames.BellyOfTheBeast1, RaptureDeviceTgt, AreaNames.CrystalVeins, CrystalVeins);
        }

        private static async Task Belly2()
        {
            await NoWpAreaHandler(AreaNames.BellyOfTheBeast2, Belly2Tgt, AreaNames.BellyOfTheBeast1, Belly1);
        }

        private static async Task Harvest()
        {
            await WpAreaHandler(AreaNames.Harvest, HarvestTgt, AreaNames.BellyOfTheBeast2, Belly2);
        }

        #endregion

        #endregion

        #region Helpers

        private static bool _anyWaypointNearby;

        private static bool IsInWaypointArea
        {
            get
            {
                var me = LokiPoe.Me;
                return me.IsInTown || me.IsInHideout || me.IsInMapRoom || _anyWaypointNearby;
            }
        }

        private static async Task WpAreaHandler(string areaName, TgtPosition tgtPos, string prevAreaName, Func<Task> prevAreaHandler, Action postEnter = null)
        {
            if (AreaNames.Current == areaName)
            {
                OuterLogicError(areaName);
                return;
            }
            if (AreaNames.Current == prevAreaName)
            {
                await MoveAndEnter(areaName, tgtPos, postEnter);
                return;
            }
            if (World.HasWaypoint(areaName))
            {
                if (IsInWaypointArea)
                {
                    await TakeWaypoint(areaName);
                }
                else
                {
                    await TpToTown();
                }
                return;
            }
            await prevAreaHandler();
        }

        private static async Task NoWpAreaHandler(string areaName, TgtPosition tgtPos, string prevAreaName, Func<Task> prevAreaHandler, Action postEnter = null)
        {
            if (AreaNames.Current == areaName)
            {
                OuterLogicError(areaName);
                return;
            }
            if (AreaNames.Current == prevAreaName)
            {
                await MoveAndEnter(areaName, tgtPos, postEnter);
                return;
            }
            await prevAreaHandler();
        }

        private static async Task TakeWaypoint(string areaName, Difficulty diff = Difficulty.Unknown)
        {
            bool newInstance = NewInstanceRequests.Contains(areaName);
            if (await PlayerAction.TakeWaypoint(areaName, diff, newInstance))
            {
                if (newInstance) NewInstanceRequests.Remove(areaName);
            }
            else
            {
                ErrorManager.ReportError();
            }
        }

        private static async Task TpToTown()
        {
            if (!await PlayerAction.TpToTown())
            {
                ErrorManager.ReportError();
            }
        }

        private static async Task MoveAndEnter(string areaName, TgtPosition tgtPos, Action postEnter)
        {
            WalkablePosition transitionPos;
            if (AreaTransitions.TryGetValue(areaName, out transitionPos))
            {
                if (transitionPos.IsFar)
                {
                    transitionPos.Come();
                }
                else
                {
                    await EnterTransition(areaName, tgtPos, postEnter);
                }
            }
            else
            {
                if (tgtPos.IsFar)
                {
                    tgtPos.Come();
                }
                else
                {
                    await EnterTransition(areaName, tgtPos, postEnter);
                }
            }
        }

        private static async Task MoveAndEnterMultilevel(string destination, TgtPosition tgtPos)
        {
            if (tgtPos.IsFar)
            {
                tgtPos.Come();
                return;
            }

            var transition = await GetTransitionObject(tgtPos, null);
            if (transition == null) return;

            if (await PlayerAction.TakeTransition(transition))
            {
                if (AreaNames.Current != destination)
                {
                    tgtPos.ResetCurrentPosition();
                }
            }
            else
            {
                ErrorManager.ReportError();
            }
        }

        private static async Task EnterTransition(string areaName, TgtPosition tgtPos, Action postEnter)
        {
            var transition = await GetTransitionObject(tgtPos, areaName);
            if (transition == null) return;

            bool newInstance = NewInstanceRequests.Contains(areaName);
            if (await PlayerAction.TakeTransition(transition, newInstance))
            {
                if (newInstance) NewInstanceRequests.Remove(areaName);
                postEnter?.Invoke();
            }
            else
            {
                ErrorManager.ReportError();
            }
        }

        private static async Task<AreaTransition> GetTransitionObject(TgtPosition tgtPos, string name)
        {
            var transition = LokiPoe.ObjectManager.Objects
                .OfType<AreaTransition>()
                .OrderBy(t => t.Distance)
                .FirstOrDefault();

            if (transition == null)
            {
                GlobalLog.Error("[Travel] There is no area transition near tgt position.");
                tgtPos.ProceedToNext();
                return null;
            }
            if (transition.TransitionType == TransitionTypes.NormalToCorrupted)
            {
                GlobalLog.Error("[Travel] Corrupted zone entrance has the same tgt as our destination.");
                tgtPos.ProceedToNext();
                return null;
            }
            if (!transition.IsTargetable)
            {
                GlobalLog.Debug(name == null ? "[Travel] Waiting for transition activation." : $"[Travel] Waiting for \"{name}\" transition activation.");
                StuckDetection.Reset();
                await Wait.Sleep(200);
                return null;
            }
            if (name != null)
            {
                var transitionName = transition.Name;
                if (transitionName != name)
                {
                    GlobalLog.Error($"[Travel] Area transition name is \"{transitionName}\". Expected: \"{name}\".");
                    tgtPos.ProceedToNext();
                    return null;
                }
            }
            return transition;
        }

        private static void OuterLogicError(string areaName)
        {
            GlobalLog.Error($"[Travel] Outer logic error. Travel to \"{areaName}\" has been called, but we are already here.");
            ErrorManager.ReportError();
        }

        #endregion

        #region Area transition scan

        private static uint _areaHash;
        private static bool _shouldScan;

        public static void Tick()
        {
            if (!ScanInterval.Elapsed)
                return;

            if (!LokiPoe.IsInGame || LokiPoe.Me.IsDead || !LokiPoe.CurrentWorldArea.IsOverworldArea)
                return;

            var currentHash = LokiPoe.LocalData.AreaHash;
            if (currentHash != _areaHash)
            {
                AreaTransitions.Clear();
                ProcessedTransitions.Clear();
                _areaHash = currentHash;
                _shouldScan = !ScanIgnoredAreas.Contains(AreaNames.Current);
            }

            if (!_shouldScan)
                return;

            foreach (var transition in LokiPoe.ObjectManager.Objects.OfType<AreaTransition>())
            {
                var id = transition.Id;

                if (ProcessedTransitions.Contains(id))
                    continue;

                var pos = transition.WalkablePosition(5, 20);
                if (pos.Initialize())
                {
                    GlobalLog.Debug($"[Travel] Registering area transition {pos}");
                    AreaTransitions.Add(pos.Name, pos);
                }
                else
                {
                    GlobalLog.Debug($"[Travel] Registering unwalkable area transition: {pos}");
                }
                ProcessedTransitions.Add(id);
            }

            if (LokiPoe.CurrentWorldArea.HasWaypoint)
            {
                _anyWaypointNearby = LokiPoe.ObjectManager.Objects.OfType<Waypoint>().Any(wp => wp.Distance <= 50);
            }
        }

        //areas that do not lead to any other area
        private static readonly HashSet<string> ScanIgnoredAreas = new HashSet<string>
        {
            AreaNames.FetidPool,
            AreaNames.FloodedDepths,
            AreaNames.UpperPrison,
            AreaNames.ShipGraveyardCave,
            AreaNames.CavernOfAnger,
            AreaNames.WesternForest,
            AreaNames.WeaverChambers,
            AreaNames.Den,
            AreaNames.ChamberOfSins2,
            AreaNames.BrokenBridge,
            AreaNames.Crypt2,
            AreaNames.DreadThicket,
            AreaNames.AncientPyramid,
            AreaNames.Crematorium,
            AreaNames.Catacombs,
            AreaNames.SolarisTemple2,
            AreaNames.EternalLaboratory,
            AreaNames.Docks,
            AreaNames.MarketSewers,
            AreaNames.LunarisTemple2,
            AreaNames.HedgeMaze,
            AreaNames.Archives,
            AreaNames.SceptreOfGod,
            AreaNames.UpperSceptreOfGod,
            AreaNames.DriedLake,
            AreaNames.KaomStronghold,
            AreaNames.GrandArena,
            AreaNames.Harvest
        };

        #endregion

        #region Tgt positions

        private static readonly TgtPosition LioneyeWatchTgt = new TgtPosition(AreaNames.LioneyeWatch, "beachtown_south_entrance_c2r2.tgt");
        private static readonly TgtPosition TidalIslandTgt = new TgtPosition(AreaNames.TidalIsland, "beach_island_transition_v01_01_c9r3.tgt");
        private static readonly TgtPosition MudFlatsTgt = new TgtPosition(AreaNames.MudFlats, "beach_to_swamp_v01_01_c5r1.tgt");
        private static readonly TgtPosition FetidPoolTgt = new TgtPosition(AreaNames.FetidPool, "beach_toswamp2_v01_01_c6r4.tgt");
        private static readonly TgtPosition SubmergedPassageTgt = new TgtPosition(AreaNames.SubmergedPassage, "Beach_to_watercave_c2r6.tgt");
        private static readonly TgtPosition FloodedDepthsTgt = new TgtPosition(AreaNames.FloodedDepths, "watery_depth_entrance_v01_01_c2r2.tgt");
        private static readonly TgtPosition LedgeTgt = new TgtPosition(AreaNames.Ledge, "caveup_exit_v01_01_c2r2.tgt");
        private static readonly TgtPosition ClimbTgt = new TgtPosition(AreaNames.Climb, "beach_passageway_v01_01_c3r4.tgt");
        private static readonly TgtPosition LowerPrisonTgt = new TgtPosition(AreaNames.LowerPrison, "beach_prisonback_c6r4.tgt");
        private static readonly TgtPosition UpperPrisonTgt = new TgtPosition(AreaNames.UpperPrison, "dungeon_prison_exit_up_v01_01_c1r1.tgt");
        private static readonly TgtPosition NextPrisonLevelTgt = new TgtPosition("Next prison level", "dungeon_prison_exit_up_v01_01_c1r1.tgt | dungeon_prison_boss_exit_v01_01_c1r2.tgt | dungeon_prison_door_up_v01_01_c1r1.tgt", true);
        private static readonly TgtPosition ShipGraveyardTgt = new TgtPosition(AreaNames.ShipGraveyard, "shipgraveyard_passageway_v01_01_c4r3.tgt");
        private static readonly TgtPosition ShipGraveyardCaveTgt = new TgtPosition(AreaNames.ShipGraveyardCave, "ship_entrance_v01_01_c2r6.tgt");
        private static readonly TgtPosition CavernOfWrathTgt = new TgtPosition(AreaNames.CavernOfWrath, "beach_caveentranceskeleton_v01_01_c7r7.tgt | beach_caveentranceskeleton_v01_02_c6r4.tgt");
        private static readonly TgtPosition CavernOfAngerTgt = new TgtPosition(AreaNames.CavernOfAnger, "caveup_exit_v01_01_c2r2.tgt");
        private static readonly TgtPosition SouthernForestTgt = new TgtPosition(AreaNames.SouthernForest, "caveup_exit_v01_01_c2r2.tgt | merveil_exit_clean_v01_01_c1r2.tgt", true);
        private static readonly TgtPosition ForestEncampmentTgt = new TgtPosition(AreaNames.ForestEncampment, "forestcamp_dock_v01_01_c3r3.tgt");
        private static readonly TgtPosition WesternForestTgt = new TgtPosition(AreaNames.WesternForest, "roadtothickforest_entrance_v01_01_c6r2.tgt");
        private static readonly TgtPosition WeaverChambersTgt = new TgtPosition(AreaNames.WeaverChambers, "spidergrove_entrance_v01_01_c4r2.tgt");
        private static readonly TgtPosition DenTgt = new TgtPosition(AreaNames.Den, "forestcave_entrance_hole_v01_01_c1r1.tgt");
        private static readonly TgtPosition CrossroadsTgt = new TgtPosition(AreaNames.Crossroads, "wall_gate_v01_01_c1r2.tgt");
        private static readonly TgtPosition ChamberOfSins1Tgt = new TgtPosition(AreaNames.ChamberOfSins1, "temple_entrance_v01_01_c1r5.tgt");
        private static readonly TgtPosition ChamberOfSins2Tgt = new TgtPosition(AreaNames.ChamberOfSins2, "templeruinforest_exit_down_v01_01_c2r4.tgt");
        private static readonly TgtPosition BrokenBridgeTgt = new TgtPosition(AreaNames.BrokenBridge, "bridgeconnection_v01_01_c1r1.tgt");
        private static readonly TgtPosition Crypt1Tgt = new TgtPosition(AreaNames.Crypt1, "church_dungeon_entrance_v01_01_c1r2.tgt");
        private static readonly TgtPosition Crypt2Tgt = new TgtPosition(AreaNames.Crypt2, "dungeon_church_exit_down_v01_01_c1r1.tgt");
        private static readonly TgtPosition WetlandsTgt = new TgtPosition(AreaNames.Wetlands, "bridgeconnection_v01_01_c1r3.tgt");
        private static readonly TgtPosition VaalRuinsTgt = new TgtPosition(AreaNames.VaalRuins, "forest_caveentrance_inca_v01_01_c2r4.tgt");
        private static readonly TgtPosition NorthernForestTgt = new TgtPosition(AreaNames.NorthernForest, "dungeon_inca_exit_v01_01_c1r2.tgt");
        private static readonly TgtPosition DreadThicketTgt = new TgtPosition(AreaNames.DreadThicket, "grovewall_entrance_v01_01_c2r2.tgt");
        private static readonly TgtPosition CavernsTgt = new TgtPosition(AreaNames.Caverns, "waterfall_cave_entrance_v01_01_c3r4.tgt");
        private static readonly TgtPosition AncientPyramidTgt = new TgtPosition(AreaNames.AncientPyramid, "dungeon_stairs_up_v01_01_c1r2.tgt");
        private static readonly TgtPosition NextPyramidLevelTgt = new TgtPosition("Next pyramid level", "dungeon_stairs_up_v01_01_c1r2.tgt | dungeon_huangdoor_v01_01_c2r2.tgt", true);
        private static readonly TgtPosition SarnEncampmentTgt = new TgtPosition(AreaNames.SarnEncampment, "act3_docks_to_town_lower_01_01_c4r2.tgt");
        private static readonly TgtPosition CrematoriumTgt = new TgtPosition(AreaNames.Crematorium, "act3_prison_entrance_01_01_c2r3.tgt");
        private static readonly TgtPosition SlumSewersTgt = new TgtPosition(AreaNames.SlumSewers, "slum_sewer_entrance_v0?_0?.tgt");
        private static readonly TgtPosition WarehouseSewersTgt = new TgtPosition(AreaNames.WarehouseSewers, "sewerwall_end_tunnel_v0?_0?_c2r2.tgt");
        private static readonly TgtPosition WarehouseDistrictTgt = new TgtPosition(AreaNames.WarehouseDistrict, "sewerwall_exit_v01_01_c2r1.tgt");
        private static readonly TgtPosition MarketplaceTgt = new TgtPosition(AreaNames.Marketplace, "warehouse_transition_market_v01_01_c2r3.tgt");
        private static readonly TgtPosition MarketSewersTgt = new TgtPosition(AreaNames.MarketSewers, "slum_sewer_entrance_v0?_0?.tgt");
        private static readonly TgtPosition CatacombsTgt = new TgtPosition(AreaNames.Catacombs, "markettochurchdungeon_v01_01_c4r1.tgt");
        private static readonly TgtPosition BattlefrontTgt = new TgtPosition(AreaNames.Battlefront, "market_to_battlefront_v01_01_c5r6.tgt");
        private static readonly TgtPosition DocksTgt = new TgtPosition(AreaNames.Docks, "battlefield_arch_v0?_0?_c2r3.tgt");
        private static readonly TgtPosition Solaris1Tgt = new TgtPosition(AreaNames.SolarisTemple1, "act3_temple_entrance_v01_01_c3r2.tgt");
        private static readonly TgtPosition Solaris2Tgt = new TgtPosition(AreaNames.SolarisTemple2, "templeclean_exit_down_v01_01_c1r3.tgt");
        private static readonly TgtPosition EbonyBarracksTgt = new TgtPosition(AreaNames.EbonyBarracks, "sewerexit_v01_01_c3r1.tgt");
        private static readonly TgtPosition Lunaris1Tgt = new TgtPosition(AreaNames.LunarisTemple1, "act3_temple_entrance_v01_01_c3r2.tgt");
        private static readonly TgtPosition Lunaris2Tgt = new TgtPosition(AreaNames.LunarisTemple2, "templeclean_exit_down_v01_01_c1r3.tgt");
        private static readonly TgtPosition ImperialGardensTgt = new TgtPosition(AreaNames.ImperialGardens, "garden_arch_v01_01_c2r5.tgt");
        private static readonly TgtPosition HedgeMazeTgt = new TgtPosition(AreaNames.HedgeMaze, "garden_wall_entrance_v01_01_c1r2.tgt");
        private static readonly TgtPosition LibraryTgt = new TgtPosition(AreaNames.Library, "Library_LargeBuilding_entrance_v01_01_c2r2.tgt");
        private static readonly TgtPosition ArchivesTgt = new TgtPosition(AreaNames.Archives, "library_entrance_v02_01_c2r3.tgt");
        private static readonly TgtPosition SceptreOfGodTgt = new TgtPosition(AreaNames.SceptreOfGod, "Act3_EpicDoor_v0?_0?_c7r6.tgt");
        private static readonly TgtPosition NextTowerLevel1Tgt = new TgtPosition("Next tower level", "tower_transition_up_01_01_c1r2.tgt", true);
        private static readonly TgtPosition NextTowerLevel2Tgt = new TgtPosition("Next tower level", "tower_transition_up_01_01_c1r2.tgt | tower_totowertop_v01_01_c2r2.tgt | Act3_tower_01_01_c13r9.tgt", true, 10, 40);
        private static readonly TgtPosition HighgateTgt = new TgtPosition(AreaNames.Highgate, "mountiantown_connection_c7r12.tgt");
        private static readonly TgtPosition Mines2Tgt = new TgtPosition(AreaNames.Mines2, "mine_areatransition_v0?_0?_c2r2.tgt");
        private static readonly TgtPosition CrystalVeinsTgt = new TgtPosition(AreaNames.CrystalVeins, "mine_areatransition_v03_01_c1r1.tgt");
        private static readonly TgtPosition RaptureDeviceTgt = new TgtPosition("Rapture Device", "crystals_openAnimation_v01_01_c3r3.tgt");
        private static readonly TgtPosition KaomPathTgt = new TgtPosition(AreaNames.KaomPath, "lava_abyss_transition_entrance_v0?_0?_c3r1.tgt", false, 10, 50);
        private static readonly TgtPosition KaomStrongholdTgt = new TgtPosition(AreaNames.KaomStronghold, "lava_kaom3_transition_entrance_v0?_0?_c3r1.tgt", false, 10, 50);
        private static readonly TgtPosition GrandArenaTgt = new TgtPosition(AreaNames.GrandArena, "arena_areatransition_v01_01_c2r2.tgt");
        private static readonly TgtPosition Belly2Tgt = new TgtPosition(AreaNames.BellyOfTheBeast2, "belly_tunnel_v01_01_c2r3.tgt");
        private static readonly TgtPosition HarvestTgt = new TgtPosition(AreaNames.Harvest, "belly_tunnel_level2_v01_02_c2r3.tgt");

        #endregion

        #region Static positions

        private static readonly WalkablePosition CoastTransitionPos = new WalkablePosition(AreaNames.Coast, 311, 214);
        private static readonly WalkablePosition RiverwaysTransitionPos = new WalkablePosition(AreaNames.Riverways, 81, 177);
        private static readonly WalkablePosition OldFieldsTransitionPos = new WalkablePosition(AreaNames.OldFields, 303, 174);
        private static readonly WalkablePosition FellshrineTransitionPos = new WalkablePosition(AreaNames.FellshrineRuins, 1000, 280);
        private static readonly WalkablePosition SlumsTransitionPos = new WalkablePosition(AreaNames.Slums, 507, 410);
        private static readonly WalkablePosition DriedLakeTransitionPos = new WalkablePosition(AreaNames.DriedLake, 88, 442);
        private static readonly WalkablePosition MinesTransitionPos = new WalkablePosition(AreaNames.Mines1, 330, 620);

        #endregion

        #region Blocking objects

        private static NetworkObject TreeRoots => LokiPoe.ObjectManager.Objects
            .FirstOrDefault(o => o.Metadata == "Metadata/QuestObjects/Inca/PoisonTree");

        private static NetworkObject AncientSeal => LokiPoe.ObjectManager.Objects
            .FirstOrDefault(o => o.Metadata == "Metadata/QuestObjects/Inca/IncaDarknessRelease");

        private static NetworkObject SewerGrating => LokiPoe.ObjectManager.Objects
            .FirstOrDefault(o => o.Metadata == "Metadata/QuestObjects/Sewers/SewersLockedDoor");

        private static NetworkObject UndyingBlockage => LokiPoe.ObjectManager.Objects
            .FirstOrDefault(o => o.Metadata == "Metadata/QuestObjects/Sewers/BioWall");

        private static NetworkObject LooseCandle => LokiPoe.ObjectManager.Objects
            .FirstOrDefault(o => o.Metadata == "Metadata/QuestObjects/Library/HiddenDoorTrigger");

        private static NetworkObject LockedDoor => LokiPoe.ObjectManager.Objects
            .FirstOrDefault(o => o.Metadata == "Metadata/QuestObjects/EpicDoor/EpicDoorLock");

        private static NetworkObject DeshretSeal => LokiPoe.ObjectManager.Objects
            .FirstOrDefault(o => o.Metadata == "Metadata/QuestObjects/Act4/MineEntranceSeal");

        #endregion
    }
}