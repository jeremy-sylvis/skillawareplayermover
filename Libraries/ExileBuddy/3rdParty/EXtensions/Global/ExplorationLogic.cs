﻿using System;
using System.Threading.Tasks;
using Loki.Bot;
using Loki.Game;

namespace EXtensions.Global
{
    public static class ExplorationLogic
    {
        private static readonly Interval LogInterval = new Interval(1000);

        public static async Task<bool> Execute(int maxPercent)
        {
            var explorer = Explorer.GetCurrent();

            if (!explorer.HasLocation)
            {
                GlobalLog.Warn("[ExplorationLogic] Explorer has no location left.");
                return false;
            }

            var exploredPercent = explorer.PercentComplete;
            if (exploredPercent >= maxPercent)
            {
                GlobalLog.Warn($"[ExplorationLogic] Maximum explored percent has been reached ({Math.Round(exploredPercent, 1)}%)");
                return false;
            }

            var location = explorer.Location;
            if (LogInterval.Elapsed)
            {
                GlobalLog.Debug($"[ExplorationTask] Now exploring to the location {location} ({LokiPoe.MyPosition.Distance(location)}) [{Math.Round(exploredPercent, 1)} %].");
            }
            if (!PlayerMover.MoveTowards(location))
            {
                GlobalLog.Error($"[ExplorationTask] MoveTowards failed for {location}.");
                explorer.Ignore(location);
            }
            return true;
        }
    }
}