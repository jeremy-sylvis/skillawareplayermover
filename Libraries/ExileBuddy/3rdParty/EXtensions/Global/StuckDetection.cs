﻿using System;
using System.Collections.Generic;
using System.Linq;
using EXtensions.CommonTasks;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace EXtensions.Global
{
    public static class StuckDetection
    {
        private static readonly Interval ScanInterval = new Interval(2000);
        private const int MobScanRange = 100;

        private const int SmallStuckRange = 30;
        private const int MediumStuckRange = 50;
        private const int LargeStuckRange = 70;

        private static int _smallStuckCount;
        private static int _mediumStuckCount;
        private static int _largeStuckCount;

        private static readonly Dictionary<int, int> MobData = new Dictionary<int, int>();
        private static readonly Dictionary<uint, int> StuckHistory = new Dictionary<uint, int>();

        private static bool _enabled = true;

        private static Vector2i _myLastPos;
        private static int _lastWorldItemCount;

        public static void Tick()
        {
            if (!_enabled || !ScanInterval.Elapsed)
                return;

            if (!LokiPoe.IsInGame || LokiPoe.Me.IsDead || !LokiPoe.CurrentWorldArea.IsCombatArea())
                return;

            FillMobData();

            //do not perform stuck checks if something was changed
            if (CheckMobHpDecrease() || CheckWorldItemDecrease())
                return;

            var myPos = LokiPoe.MyPosition;

            if (myPos.Distance(_myLastPos) < SmallStuckRange)
            {
                ++_smallStuckCount;

                if (_smallStuckCount >= 2)
                {
                    HandleBlockingChestsTask.Enabled = true;
                }

                if (_smallStuckCount >= 3)
                {
                    GlobalLog.Debug($"[StuckDetection] Small range stuck count: {_smallStuckCount}");
                }

                if (_smallStuckCount >= Settings.Instance.MaxStuckCountSmall)
                {
                    GlobalLog.Error($"[StuckDetection] Small range stuck count: {_smallStuckCount}. Now logging out.");
                    HandleStuck();
                    return;
                }
            }
            else
            {
                _smallStuckCount = 0;
            }

            if (myPos.Distance(_myLastPos) < MediumStuckRange)
            {
                ++_mediumStuckCount;

                if (_mediumStuckCount >= Settings.Instance.MaxStuckCountSmall)
                {
                    GlobalLog.Debug($"[StuckDetection] Medium range stuck count: {_mediumStuckCount}");
                }

                if (_mediumStuckCount >= Settings.Instance.MaxStuckCountMedium)
                {
                    GlobalLog.Error($"[StuckDetection] Medium range stuck count: {_mediumStuckCount}. Now logging out.");
                    HandleStuck();
                    return;
                }
            }
            else
            {
                _mediumStuckCount = 0;
            }


            if (myPos.Distance(_myLastPos) < LargeStuckRange)
            {
                ++_largeStuckCount;

                if (_largeStuckCount >= Settings.Instance.MaxStuckCountMedium)
                {
                    GlobalLog.Debug($"[StuckDetection] Large range stuck count: {_largeStuckCount}");
                }

                if (_largeStuckCount >= Settings.Instance.MaxStuckCountLarge)
                {
                    GlobalLog.Error($"[StuckDetection] Large range stuck count: {_largeStuckCount}. Now logging out.");
                    HandleStuck();
                }
            }
            else
            {
                _largeStuckCount = 0;
            }

            _myLastPos = myPos;
        }

        private static void FillMobData()
        {
            foreach (var mob in LokiPoe.ObjectManager.Objects.OfType<Monster>())
            {
                try
                {
                    if (mob.IsDead || mob.Reaction != Reaction.Enemy || mob.Distance > MobScanRange)
                        continue;

                    var id = mob.Id;
                    if (!MobData.ContainsKey(id)) MobData.Add(id, mob.Health);
                }
                catch (Exception ex)
                {
                    GlobalLog.Debug($"[StuckDetection] Exception during monster processing.\n{ex.Message}");
                }
            }
        }

        private static bool CheckMobHpDecrease()
        {
            bool decreased = false;
            List<int> toRemove = new List<int>();
            Dictionary<int, int> decreasedHealth = new Dictionary<int, int>();
            foreach (var kvp in MobData)
            {
                var mob = LokiPoe.ObjectManager.GetObjectById<Monster>(kvp.Key);
                if (mob == null)
                {
                    toRemove.Add(kvp.Key);
                    continue;
                }
                if (mob.IsDead)
                {
                    toRemove.Add(kvp.Key);
                    decreased = true;
                    continue;
                }
                if (mob.Health < kvp.Value)
                {
                    decreased = true;
                    decreasedHealth.Add(kvp.Key, mob.Health);
                }
            }

            foreach (var kvp in decreasedHealth)
            {
                MobData[kvp.Key] = kvp.Value;
            }

            foreach (var id in toRemove)
            {
                MobData.Remove(id);
            }

            return decreased;
        }

        private static bool CheckWorldItemDecrease()
        {
            var itemCount = CombatAreaCache.Current.ItemPositions.Count;
            bool decreased = itemCount < _lastWorldItemCount;
            _lastWorldItemCount = itemCount;
            return decreased;
        }

        private static void HandleStuck()
        {
            ++StucksInCurrentArea;
            var stucks = StucksInCurrentArea;

            GlobalLog.Error($"[StuckDetection] Stuck incidents in this area: {stucks}");

            if (stucks >= Settings.Instance.MaxStucksPerInstance)
            {
                if (BotManager.CurrentBot.Name == "QuestBot")
                {
                    var areaName = AreaNames.Current;
                    GlobalLog.Error($"[StuckDetection] Too many stucks in this area ({areaName}). Now requesting new instance.");
                    Travel.RequestNewInstance(areaName);
                }
                //add case for MapBot in the future
            }
            var err = LokiPoe.EscapeState.LogoutToTitleScreen();
            if (err != LokiPoe.EscapeState.LogoutError.None)
            {
                GlobalLog.Debug($"[StuckDetection] Logout error: \"{err}\".");
                //stuck incident was not properly handled if we did not logout
                --StucksInCurrentArea;
            }
        }

        private static int StucksInCurrentArea
        {
            get
            {
                int stucks;
                return StuckHistory.TryGetValue(LokiPoe.LocalData.AreaHash, out stucks) ? stucks : 0;
            }
            set { StuckHistory[LokiPoe.LocalData.AreaHash] = value; }
        }


        public static void Reset()
        {
            _smallStuckCount = 0;
            _mediumStuckCount = 0;
            _largeStuckCount = 0;
        }

        public static void Enable()
        {
            _enabled = true;
            GlobalLog.Info("[StuckDetection] Enabled.");
        }

        public static void Disable()
        {
            _enabled = false;
            GlobalLog.Info("[StuckDetection] Disabled.");
        }

        static StuckDetection()
        {
            Events.AreaChanged += OnAreaChanged;
        }

        private static void OnAreaChanged(object sender, AreaChangedArgs args)
        {
            Reset();
            _lastWorldItemCount = 0;
            _myLastPos = Vector2i.Zero;
            MobData.Clear();
            GlobalLog.Info("[StuckDetection] Data has been reset.");
        }
    }
}