﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Game;
using Loki.Game.Objects;

namespace EXtensions.Global
{
    public static class TrackMobLogic
    {
        private const int MaxKillAttempts = 25;

        private static readonly Interval LogInterval = new Interval(1000);

        private static KeyValuePair<int, WorldPosition>? _currentMonster;
        private static int _currentId;
        private static int _currentAttempt;

        public static async Task<bool> Execute()
        {
            var monsterPositions = CombatAreaCache.Current.MonsterPositions;

            if (monsterPositions.Count == 0)
                return false;

            if (_currentMonster == null)
            {
                _currentMonster = monsterPositions.OrderBy(kvp => kvp.Value.DistanceSqr).First();
            }
            else
            {
                if (!monsterPositions.ContainsKey(_currentMonster.Value.Key))
                {
                    _currentMonster = null;
                    return true;
                }
            }

            var mobPos = _currentMonster.Value.Value;

            if (mobPos.IsFar)
            {
                if (LogInterval.Elapsed)
                {
                    GlobalLog.Debug($"[TrackMobTask] Cached monster locations: {monsterPositions.Count}");
                    GlobalLog.Debug($"[TrackMobTask] Moving to the monster at {mobPos}");
                }

                if (!PlayerMover.MoveTowards(mobPos))
                {
                    GlobalLog.Error($"[TrackMobTask] MoveTowards has failed for {mobPos}");
                    ClearCurrentMonster(true, "Fail to move to");
                }
                return true;
            }

            var mobId = _currentMonster.Value.Key;

            var mob = LokiPoe.ObjectManager.GetObjectById<Monster>(mobId);
            if (mob == null || mob.IsDead)
            {
                ClearCurrentMonster(false);
            }
            else
            {
                monsterPositions[mobId] = mob.WorldPosition();
                _currentMonster = null;

                if (mobId != _currentId)
                {
                    _currentId = mobId;
                    _currentAttempt = 0;
                }
                else
                {
                    ++_currentAttempt;
                    if (_currentAttempt > MaxKillAttempts)
                    {
                        GlobalLog.Error("[TrackMobTask] All kill attempts have been spent. Now removing problematic mob from cache.");
                        ClearCurrentMonster(true, "Fail to kill");
                        _currentId = 0;
                        _currentAttempt = 0;
                        return true;
                    }
                    GlobalLog.Debug($"[TrackMobTask] Alive monster is nearby, this is our {_currentAttempt}/{MaxKillAttempts} attempt to kill it.");
                    await Coroutine.Sleep(200);
                }
            }
            return true;
        }

        private static void ClearCurrentMonster(bool blacklist, string reason = null)
        {
            if (_currentMonster == null) return;

            var id = _currentMonster.Value.Key;
            CombatAreaCache.Current.MonsterPositions.Remove(id);
            _currentMonster = null;

            if (blacklist) Blacklist.Add(id, TimeSpan.FromDays(1), reason ?? "");
        }
    }
}