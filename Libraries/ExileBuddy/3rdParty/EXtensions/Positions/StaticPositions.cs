﻿using Loki.Bot;
using Loki.Common;
using Loki.Game;

namespace EXtensions.Positions
{
    public static class StaticPositions
    {
        public static readonly WalkablePosition Nessa = new WalkablePosition("Nessa", 268, 255);
        public static readonly WalkablePosition Bestel = new WalkablePosition("Bestel", 283, 238);
        public static readonly WalkablePosition Tarkleigh = new WalkablePosition("Tarkleigh", 311, 186);
        public static readonly WalkablePosition Greust = new WalkablePosition("Greust", 195, 171);
        public static readonly WalkablePosition Eramir = new WalkablePosition("Eramir", 166, 178);
        public static readonly WalkablePosition Yeena = new WalkablePosition("Yeena", 162, 245);
        public static readonly WalkablePosition Silk = new WalkablePosition("Silk", 172, 238);
        public static readonly WalkablePosition Helena = new WalkablePosition("Helena", 167, 247);
        public static readonly WalkablePosition Clarissa = new WalkablePosition("Clarissa", 150, 323);
        public static readonly WalkablePosition Hargan = new WalkablePosition("Hargan", 275, 348);
        public static readonly WalkablePosition Maramoa = new WalkablePosition("Maramoa", 272, 299);
        public static readonly WalkablePosition Grigor = new WalkablePosition("Grigor", 120, 380);
        public static readonly WalkablePosition Kira = new WalkablePosition("Kira", 169, 503);
        public static readonly WalkablePosition PetarusAndVanja = new WalkablePosition("Petarus and Vanja", 204, 546);
        public static readonly WalkablePosition Tasuni = new WalkablePosition("Tasuni", 398, 449);
        public static readonly WalkablePosition Dialla = new WalkablePosition("Dialla", 544, 502);
        public static readonly WalkablePosition Oyun = new WalkablePosition("Oyun", 557, 496);

        public static readonly WalkablePosition StashPosAct1 = new WalkablePosition("stash", new Vector2i(244, 276));
        public static readonly WalkablePosition StashPosAct2 = new WalkablePosition("stash", new Vector2i(194, 206));
        public static readonly WalkablePosition StashPosAct3 = new WalkablePosition("stash", new Vector2i(200, 310));
        public static readonly WalkablePosition StashPosAct4 = new WalkablePosition("stash", new Vector2i(203, 520));

        public static readonly WalkablePosition WaypointPosAct1 = new WalkablePosition("waypoint", new Vector2i(186, 164));
        public static readonly WalkablePosition WaypointPosAct2 = new WalkablePosition("waypoint", new Vector2i(188, 116));
        public static readonly WalkablePosition WaypointPosAct3 = new WalkablePosition("waypoint", new Vector2i(219, 211));
        public static readonly WalkablePosition WaypointPosAct4 = new WalkablePosition("waypoint", new Vector2i(281, 492));

        public static readonly WalkablePosition CommonPortalSpotAct1 = new WalkablePosition("common portal spot", new Vector2i(172, 254));
        public static readonly WalkablePosition CommonPortalSpotAct2 = new WalkablePosition("common portal spot", new Vector2i(220, 168));
        public static readonly WalkablePosition CommonPortalSpotAct3 = new WalkablePosition("common portal spot", new Vector2i(231, 246));
        public static readonly WalkablePosition CommonPortalSpotAct4 = new WalkablePosition("common portal spot", new Vector2i(257, 500));

        public static WalkablePosition GetStashPosByAct()
        {
            switch (LokiPoe.CurrentWorldArea.Act)
            {
                case 4:
                    return StashPosAct4;
                case 3:
                    return StashPosAct3;
                case 2:
                    return StashPosAct2;
                case 1:
                    return StashPosAct1;
            }
            GlobalLog.Error($"[GetStashPosByAct] Unknown act: {LokiPoe.CurrentWorldArea.Act}.");
            BotManager.Stop();
            return null;
        }


        public static WalkablePosition GetWaypointPosByAct()
        {
            switch (LokiPoe.CurrentWorldArea.Act)
            {
                case 4:
                    return WaypointPosAct4;
                case 3:
                    return WaypointPosAct3;
                case 2:
                    return WaypointPosAct2;
                case 1:
                    return WaypointPosAct1;
            }
            GlobalLog.Error($"[GetWaypointPosByAct] Unknown act: {LokiPoe.CurrentWorldArea.Act}.");
            BotManager.Stop();
            return null;
        }

        public static WalkablePosition GetCommonPortalSpotByAct()
        {
            switch (LokiPoe.CurrentWorldArea.Act)
            {
                case 4:
                    return CommonPortalSpotAct4;
                case 3:
                    return CommonPortalSpotAct3;
                case 2:
                    return CommonPortalSpotAct2;
                case 1:
                    return CommonPortalSpotAct1;
            }
            GlobalLog.Error($"[GetWaypointPosByAct] Unknown act: {LokiPoe.CurrentWorldArea.Act}.");
            BotManager.Stop();
            return null;
        }
    }
}