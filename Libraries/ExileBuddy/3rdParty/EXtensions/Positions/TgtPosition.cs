﻿using System;
using System.Collections.Generic;
using System.Linq;
using Loki.Common;
using Loki.Game;

namespace EXtensions.Positions
{
    public class TgtPosition : WalkablePosition
    {
        private static readonly Vector2i Uninitialized = new Vector2i(int.MaxValue, int.MaxValue);

        private readonly string _tgtName;
        private readonly bool _closest;

        private List<WorldPosition> _tgtPositions;

        private uint _areaHash;

        public override bool IsInitialized => _areaHash == LokiPoe.LocalData.AreaHash;

        public TgtPosition(string name, string tgtName, bool closest = false, int step = 5, int radius = 30)
            : base(name, Uninitialized, step, radius)
        {
            _tgtName = tgtName;
            _closest = closest;
            _tgtPositions = new List<WorldPosition>();
        }

        public void ResetCurrentPosition()
        {
            if (!IsInitialized) InternalInitialize();
            else SetCurrentPosition();
        }

        public void ProceedToNext()
        {
            if (_tgtPositions.Count <= 1)
            {
                GlobalLog.Error("[TgtPosition] Cannot proceed to next, current one is the last.");
                ErrorManager.ReportCriticalError();
                return;
            }
            var pos = _tgtPositions.OrderBy(p => p.DistanceSqr).First();
            _tgtPositions.Remove(pos);
            GlobalLog.Debug($"[TgtPosition] {pos} has been removed.");
            SetCurrentPosition();
        }

        public override bool Initialize()
        {
            try
            {
                FindTgtPositions();
                SetCurrentPosition();
            }
            catch (Exception)
            {
                return false;
            }
            _areaHash = LokiPoe.LocalData.AreaHash;
            return true;
        }

        protected override void InternalInitialize()
        {
            FindTgtPositions();
            SetCurrentPosition();
            _areaHash = LokiPoe.LocalData.AreaHash;
        }

        private void FindTgtPositions()
        {
            _tgtPositions.Clear();

            if (_tgtName.Contains('|'))
            {
                var tgtNames = _tgtName.Split('|').Select(tgt => tgt.Trim()).ToList();
                foreach (var tgtName in tgtNames)
                {
                    var positions = Tgt.FindAll(tgtName);
                    _tgtPositions.AddRange(positions);
                }
            }
            else
            {
                var positions = Tgt.FindAll(_tgtName);
                _tgtPositions.AddRange(positions);
            }
            if (_tgtPositions.Count == 0)
            {
                GlobalLog.Error($"[TgtPosition] Fail to find any \"{_tgtName}\" tgt.");
                ErrorManager.ReportCriticalError();
            }
        }

        private void SetCurrentPosition()
        {
            bool walkableFound = _closest ? FindClosestPosition() : FindDistantPosition();
            if (!walkableFound)
            {
                GlobalLog.Error("[TgtPosition] No walkable position can be found.");
                ErrorManager.ReportCriticalError();
                return;
            }
            GlobalLog.Warn($"[TgtPosition] Registering {this}");
        }

        private bool FindDistantPosition()
        {
            _tgtPositions = _tgtPositions.OrderByDescending(p => p.DistanceSqr).ToList();
            foreach (var pos in _tgtPositions)
            {
                Vector = pos;
                if (FindWalkable(false)) return true;
            }
            return false;
        }

        private bool FindClosestPosition()
        {
            _tgtPositions = _tgtPositions.OrderBy(p => p.DistanceSqr).ToList();
            foreach (var pos in _tgtPositions.Take(3)) //first 3 should be enough
            {
                Vector = pos;
                if (FindWalkable(false)) return true;
            }
            return false;
        }
    }
}