﻿using System.Collections.Generic;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;

namespace EXtensions.Positions
{
    public class WorldPosition : Position
    {
        public WorldPosition(Vector2i vector)
            : base(vector)
        {
        }

        public WorldPosition(int x, int y)
            : base(x, y)
        {
        }

        public int Distance => LokiPoe.MyPosition.Distance(Vector);
        public int DistanceSqr => LokiPoe.MyPosition.DistanceSqr(Vector);
        public float PathDistance => ExilePather.PathDistance(LokiPoe.MyPosition, Vector);
        public bool IsNear => CloseToMe(15);
        public bool IsFar => FarFromMe(15);
        public bool PathExists => ExilePather.PathExistsBetween(LokiPoe.MyPosition, Vector);

        public bool CloseToMe(int distance)
        {
            return Distance <= distance;
        }

        public bool FarFromMe(int distance)
        {
            return Distance > distance;
        }

        public WorldPosition GetWalkable(int step = 3, int radius = 30)
        {
            return PathExists ? this : FindPositionForMove(this, step, radius);
        }

        public override string ToString()
        {
            return $"{Vector} (distance: {Distance})";
        }

        public static WorldPosition FindPositionForMove(Vector2i pos, int step = 3, int radius = 30)
        {
            var walkable = FindWalkablePosition(pos, radius);
            if (walkable != null) return walkable;
            return FindPathablePosition(pos, step, radius);
        }

        public static WorldPosition FindWalkablePosition(Vector2i pos, int radius = 15)
        {
            var walkable = ExilePather.FastWalkablePositionFor(pos, radius);
            return ExilePather.PathExistsBetween(LokiPoe.MyPosition, walkable) ? new WorldPosition(walkable) : null;
        }

        public static WorldPosition FindPathablePosition(Vector2i pos, int step = 3, int radius = 30)
        {
            var myPos = LokiPoe.MyPosition;
            int x = pos.X;
            int y = pos.Y;
            for (int r = step; r <= radius; r += step)
            {
                int minX = x - r;
                int minY = y - r;
                int maxX = x + r;
                int maxY = y + r;
                for (int i = minX; i <= maxX; i += step)
                {
                    for (int j = minY; j <= maxY; j += step)
                    {
                        if (i != minX && i != maxX && j != minY && j != maxY) continue;
                        var p = new Vector2i(i, j);
                        if (ExilePather.PathExistsBetween(myPos, p)) return new WorldPosition(p);
                    }
                }
            }
            return null;
        }

        public class ComparerByDistanceSqr : IComparer<WorldPosition>
        {
            public static readonly ComparerByDistanceSqr Instance = new ComparerByDistanceSqr();

            static ComparerByDistanceSqr()
            {
            }

            private ComparerByDistanceSqr()
            {
            }

            public int Compare(WorldPosition first, WorldPosition second)
            {
                return first.DistanceSqr - second.DistanceSqr;
            }
        }

        public class ComparerByPathDistance : IComparer<WorldPosition>
        {
            public static readonly ComparerByPathDistance Instance = new ComparerByPathDistance();

            static ComparerByPathDistance()
            {
            }

            private ComparerByPathDistance()
            {
            }

            public int Compare(WorldPosition first, WorldPosition second)
            {
                return first.PathDistance.CompareTo(second.PathDistance);
            }
        }
    }
}