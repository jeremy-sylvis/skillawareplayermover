﻿using System.ComponentModel;
using Loki;
using Loki.Common;

namespace nsOverlay
{
	/// <summary>Settings for the Dev tab. </summary>
	public class OverlaySettings : JsonSettings
	{
		private static OverlaySettings _instance;

		/// <summary>The current instance for this class. </summary>
		public static OverlaySettings Instance
		{
			get
			{
				return _instance ?? (_instance = new OverlaySettings());
			}
		}

		/// <summary>The default ctor. Will use the settings path "Overlay".</summary>
		public OverlaySettings()
			: base(GetSettingsFilePath(Configuration.Instance.Name, string.Format("{0}.json", "Overlay")))
		{
		}
	}
}
