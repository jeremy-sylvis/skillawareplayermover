﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Loki.Bot;

namespace nsOverlay
{
	/// <summary>
	/// Interaction logic for BotControlOverlayControl.xaml
	/// </summary>
	public partial class BotControlOverlayControl 
	{
		public BotControlOverlayControl()
		{
			InitializeComponent();
		}

		private void StopButton_OnClick(object sender, RoutedEventArgs e)
		{
			BotManager.Stop();
		}
	}
}
