﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace nsOverlay
{
	/// <summary>
	/// Interaction logic for CustomOverlayControl.xaml
	/// </summary>
	public partial class CustomOverlayControl
	{
		public CustomOverlayControl()
		{
			InitializeComponent();
		}

		private void UIElement_OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			//Application.Current.MainWindow.Activate();
			Keyboard.Focus((IFrameworkInputElement)sender);
			FocusManager.SetFocusedElement((DependencyObject)sender, (IFrameworkInputElement)sender);
		}
	}
}
