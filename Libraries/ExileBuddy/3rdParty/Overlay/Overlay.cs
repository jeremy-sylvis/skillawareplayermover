﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Effects;
using log4net;
using Loki.Bot;
using Loki.Common;
using Buddy.Overlay;
using Buddy.Overlay.Controls;
using Buddy.Overlay.Notifications;
using Loki.Game;

namespace nsOverlay
{
	internal class Overlay : IPlugin
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private Gui _instance;
		private bool _added;

		private BotControlComponent _botControl;
		private ButtonOverlayUIComponent _buttonControl;

		#region Implementation of IAuthored

		/// <summary> The name of the plugin. </summary>
		public string Name
		{
			get
			{
				return "Overlay";
			}
		}

		/// <summary>The author of the plugin.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary> The description of the plugin. </summary>
		public string Description
		{
			get
			{
				return "A plugin that provides a basic Buddy.Overlay example.";
			}
		}

		/// <summary>The version of the plugin.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		#endregion

		#region Implementation of IBase

		/// <summary>Initializes this plugin.</summary>
		public void Initialize()
		{
			Log.DebugFormat("[Overlay] Initialize");
		}

		/// <summary>Deinitializes this object. This is called when the object is being unloaded from the bot.</summary>
		public void Deinitialize()
		{
			Log.DebugFormat("[Overlay] Deinitialize");
		}

		#endregion

		#region Implementation of IRunnable

		/// <summary> The plugin start callback. Do any initialization here. </summary>
		public void Start()
		{
			Log.DebugFormat("[Overlay] Start");
		}

		/// <summary> The plugin tick callback. Do any update logic here. </summary>
		public void Tick()
		{
		}

		/// <summary> The plugin stop callback. Do any pre-dispose cleanup here. </summary>
		public void Stop()
		{
			Log.DebugFormat("[Overlay] Stop");
		}

		#endregion

		#region Implementation of IConfigurable

		/// <summary>The settings object. This will be registered in the current configuration.</summary>
		public JsonSettings Settings
		{
			get
			{
				return OverlaySettings.Instance;
			}
		}

		/// <summary> The plugin's settings control. This will be added to the Exilebuddy Settings tab.</summary>
		public UserControl Control
		{
			get
			{
				return (_instance ?? (_instance = new Gui()));
			}
		}

		#endregion

		#region Implementation of ILogic

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		#endregion

		#region Implementation of IEnableable

		/// <summary> The plugin is being enabled.</summary>
		public void Enable()
		{
			Log.DebugFormat("[Overlay] Enable");

			if (LokiPoe.OverlayManager != null)
			{
				Console.WriteLine("[Overlay] Desktop Composition: {0}.", OverlayManager.IsDesktopCompositionEnabled);
				if (_botControl == null)
				{
					//_buttonControl = new ButtonOverlayUIComponent();
					//LokiPoe.OverlayManager.AddUIComponent(_buttonControl);

					_botControl = new BotControlComponent();
					LokiPoe.OverlayManager.AddUIComponent(_botControl);
				}
			}
		}

		/// <summary> The plugin is being disabled.</summary>
		public void Disable()
		{
			Log.DebugFormat("[Overlay] Disable");

			if (LokiPoe.OverlayManager != null)
			{
				if (_botControl != null)
				{
					LokiPoe.OverlayManager.RemoveUIComponent(_botControl);
					_botControl = null;
				}
			}
		}

		#endregion

		#region Override of Object

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			return Name + ": " + Description;
		}

		#endregion

		private class MyCustomComponent : OverlayUIComponent
		{
			private OverlayControl _frame;

			public MyCustomComponent() : base(true)
			{
			}

			public override OverlayControl Control
			{
				get
				{
					return _frame ?? (_frame = new CustomOverlayControl());
				}
			}
		}

		private class BotControlComponent : OverlayUIComponent
		{
			private OverlayControl _frame;

			public BotControlComponent() : base(true)
			{
			}

			public override OverlayControl Control
			{
				get
				{
					return _frame ?? (_frame = new BotControlOverlayControl());
				}
			}
		}

		private class MyOverlayUIComponentComponent : OverlayUIComponent
		{
			private OverlayControl _control;

			public MyOverlayUIComponentComponent()
				: base(true /* pass in 'false' if control should not be hit tested, mouse clicks will pass through */)
			{
			}

			public override OverlayControl Control
			{
				get
				{
					return _control ?? (_control = new MyOverlayControl());
				}
			}

		}

		// Shows a button that can clicked
		private class ButtonOverlayUIComponent : OverlayUIComponent
		{
			public ButtonOverlayUIComponent()
				: base(true /*isHitTestable, determines whether UI component can handle mouse clicks */)
			{
			}

			private OverlayControl _control;

			public override OverlayControl Control
			{
				get
				{
					if (_control == null)
					{
						var button = new Button
						{
							Content = "Click Me"
						};
						button.Click += (sender, args) => button.Content = "Clicked!";
						_control = new OverlayControl
						{
							X = 300,
							Y = 300,
							Content = button
						};
					}
					return _control;
				}
			}
		}

		private class MyOverlayControl : OverlayControl
		{
			public MyOverlayControl()
			{
				AllowMoving = true;
				AllowResizing = true;
				BorderBrush = Brushes.Black;
				BorderThickness = new Thickness(1);
				X = 900;
				Y = 10;
				MinHeight = 90;
				MinWidth = 200;
				Height = 100;
				Width = 200;

				Background = new SolidColorBrush(Colors.LightBlue)
				{
					Opacity = 0.4
				};
				Background.Freeze();

				var mainGrid = new Grid();
				Content = mainGrid;
				mainGrid.RowDefinitions.Add(new RowDefinition
				{
					Height = GridLength.Auto
				});
				mainGrid.RowDefinitions.Add(new RowDefinition
				{
					Height = new GridLength(1, GridUnitType.Star)
				});

				var titleBar = new Border
				{
					Background = new SolidColorBrush(Colors.Blue)
					{
						Opacity = 0.9
					},
					Child = new TextBlock
					{
						Text = "My Settings",
						HorizontalAlignment = HorizontalAlignment.Center,
						Foreground = Brushes.White,
						Effect = new DropShadowEffect
						{
							Color = Colors.Black,
							BlurRadius = 2,
							Opacity = 0.9,
							ShadowDepth = 1
						}
					}
				};
				mainGrid.Children.Add(titleBar);

				var contentGrid = new Grid
				{
					VerticalAlignment = VerticalAlignment.Stretch
				};
				mainGrid.Children.Add(contentGrid);

				contentGrid.RowDefinitions.Add(new RowDefinition
				{
					Height = GridLength.Auto
				});
				contentGrid.RowDefinitions.Add(new RowDefinition
				{
					Height = GridLength.Auto
				});
				contentGrid.RowDefinitions.Add(new RowDefinition
				{
					Height = GridLength.Auto
				});
				Grid.SetRow(contentGrid, 1);

				contentGrid.ColumnDefinitions.Add(new ColumnDefinition
				{
					Width = GridLength.Auto
				});
				contentGrid.ColumnDefinitions.Add(new ColumnDefinition
				{
					Width = new GridLength(1, GridUnitType.Star)
				});

				contentGrid.Children.Add(new TextBlock
				{
					Text = "Setting 1"
				});

				var check1 = new CheckBox
				{
					HorizontalAlignment = HorizontalAlignment.Right
				};
				Grid.SetColumn(check1, 1);
				contentGrid.Children.Add(check1);

				var setting2Text = new TextBlock
				{
					Text = "Setting 2",
				};
				Grid.SetRow(setting2Text, 1);
				contentGrid.Children.Add(setting2Text);

				var check2 = new CheckBox
				{
					HorizontalAlignment = HorizontalAlignment.Right
				};
				Grid.SetColumn(check2, 1);
				Grid.SetRow(check2, 1);
				contentGrid.Children.Add(check2);

				var setting3Text = new TextBlock
				{
					Text = "Setting 3",
				};
				Grid.SetRow(setting3Text, 2);
				contentGrid.Children.Add(setting3Text);

				var editBox = new TextBox
				{
					Text = "Type something",
					HorizontalAlignment = HorizontalAlignment.Stretch
				};
				Grid.SetColumn(editBox, 1);
				Grid.SetRow(editBox, 2);
				contentGrid.Children.Add(editBox);
			}
		}
	}
}