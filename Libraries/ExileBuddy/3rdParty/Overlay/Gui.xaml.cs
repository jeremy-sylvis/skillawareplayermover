﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Loki.Game;

namespace nsOverlay
{
	/// <summary>
	/// Interaction logic for Gui.xaml
	/// </summary>
	public partial class Gui : UserControl
	{
		public Gui()
		{
			InitializeComponent();
		}

		private void ToastButton_OnClick(object sender, RoutedEventArgs e)
		{
			if (LokiPoe.OverlayManager != null)
			{
				LokiPoe.OverlayManager.Dispatcher.Invoke(delegate
				{
					Stopwatch sw = Stopwatch.StartNew();
					LokiPoe.OverlayManager.AddToast(
					() =>
					{
						var timeLeft = Math.Max(3000 - sw.ElapsedMilliseconds, 0);
						return string.Format("Self Destruct in {0}.{1:000}", (int)timeLeft / 1000, timeLeft % 1000);
					},
					TimeSpan.FromSeconds(3), Colors.Lime, Colors.Blue, new FontFamily("Consolas"));
				});
			}
		}
	}
}
