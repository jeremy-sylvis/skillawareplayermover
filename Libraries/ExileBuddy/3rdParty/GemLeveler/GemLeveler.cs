﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using log4net;
using Loki.Bot;
using System;
using Loki.Game;
using Loki.Common;
using Loki.Game.GameData;

namespace nsGemLeveler
{
	internal class GemLeveler : IPlugin
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();
		private readonly TaskManager _taskManager = new TaskManager();

		private Gui _instance;

		private bool _ranOnce;

		/// <summary> The name of the plugin. </summary>
		public string Name
		{
			get
			{
				return "GemLeveler";
			}
		}

		/// <summary> The description of the plugin. </summary>
		public string Description
		{
			get
			{
				return "A plugin to automatically level skill gems.";
			}
		}

		/// <summary>The author of the plugin.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of the plugin.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>Initializes this plugin.</summary>
		public void Initialize()
		{
			Log.DebugFormat("[GemLeveler] Initialize");
		}

		/// <summary>Deinitializes this object. This is called when the object is being unloaded from the bot.</summary>
		public void Deinitialize()
		{
			Log.DebugFormat("[GemLeveler] Deinitialize");
		}

		/// <summary> The plugin start callback. Do any initialization here. </summary>
		public void Start()
		{
			Log.DebugFormat("[GemLeveler] Start");

			Reset();

			_taskManager.Reset();
			_taskManager.Add(new LevelSkillGemTask());
			_taskManager.Freeze();

			_taskManager.Start();
		}

		/// <summary> The plugin tick callback. Do any update logic here. </summary>
		public void Tick()
		{
			_taskManager.Tick(); // TaskManager will check IsInGame

			// Don't update while we are not in the game.
			if (!LokiPoe.IsInGame)
				return;

			// Update the current skillgems once per major event.
			if (!_ranOnce)
			{
				// This can trigger gui code from a non-gui thread, so we need to run it on a gui thread.
				LokiPoe.BeginDispatchIfNecessary(new Action(() => GemLevelerSettings.Instance.RefreshSkillGemsList()));
				_ranOnce = true;
				return;
			}
		}

		/// <summary> The plugin stop callback. Do any pre-dispose cleanup here. </summary>
		public void Stop()
		{
			Log.DebugFormat("[GemLeveler] Stop");

			_taskManager.Stop();
		}

		private void Reset()
		{
			Log.DebugFormat("[GemLeveler] Now resetting task state.");
			_ranOnce = false;
		}

		#region Implementation of IEnableable

		/// <summary>Called when the task should be enabled.</summary>
		public void Enable()
		{
			Log.DebugFormat("[GemLeveler] Enable");
		}

		/// <summary>Called when the task should be disabled.</summary>
		public void Disable()
		{
			Log.DebugFormat("[GemLeveler] Enable");
		}

		#endregion

		#region Implementation of IConfigurable

		/// <summary>The settings object. This will be registered in the current configuration.</summary>
		public JsonSettings Settings
		{
			get
			{
				return GemLevelerSettings.Instance;
			}
		}

		/// <summary> The plugin's settings control. This will be added to the Exilebuddy Settings tab.</summary>
		public UserControl Control
		{
			get
			{
				return (_instance ?? (_instance = new Gui()));
			}
		}

		#endregion

		#region Implementation of ILogic

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "post_combat_hook")
			{
				return await _taskManager.Logic(type, param);
			}

			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();

				// We want to pass this event to our task manager so those tasks can receive events.
				return await _taskManager.Logic(type, param);
			}

			return await _taskManager.Logic(type, param);
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		#endregion

		#region Override of Object

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			return Name + ": " + Description;
		}

		#endregion
	}
}
