﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Forms;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.Objects;
using Loki.Game.Objects.Items;

namespace nsGemLeveler
{
	public class LevelSkillGemTask : ITask
	{
		#region Temp Compatibility 

		/// <summary>
		/// Opens the inventory panel.
		/// </summary>
		/// <returns></returns>
		public static async Task<bool> OpenInventoryPanel(int timeout = 5000)
		{
			Log.DebugFormat("[OpenInventoryPanel]");

			var sw = Stopwatch.StartNew();

			// Make sure we close all blocking windows so we can actually open the inventory.
			if (!LokiPoe.InGameState.InventoryUi.IsOpened)
			{
				await Coroutines.CloseBlockingWindows();
			}

			// Open the inventory panel like a player would.
			while (!LokiPoe.InGameState.InventoryUi.IsOpened)
			{
				Log.DebugFormat("[OpenInventoryPanel] The InventoryUi is not opened. Now opening it.");

				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[OpenInventoryPanel] Timeout.");
					return false;
				}

				if (LokiPoe.Me.IsDead)
				{
					Log.ErrorFormat("[OpenInventoryPanel] We are now dead.");
					return false;
				}

				LokiPoe.Input.SimulateKeyEvent(LokiPoe.Input.Binding.open_inventory_panel, true, false, false);

				await Coroutines.ReactionWait();
			}

			return true;
		}

		#endregion

		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private readonly WaitTimer _levelWait = WaitTimer.FiveSeconds;
		private bool _needsToUpdate = true;
		private bool _needsToCloseInventory;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "LevelSkillGemTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task will level skillgems after close range combat is taken care of.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		private bool ContainsHelper(string name)
		{
			foreach (var entry in GemLevelerSettings.Instance.GlobalNameIgnoreList)
			{
				if (entry.Equals(name, StringComparison.OrdinalIgnoreCase))
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_player_leveled_event")
			{
				_needsToUpdate = true;
				return true;
			}

			if (type != "post_combat_hook")
				return false;

			// Don't try to do anything when the escape state is active.
			if (LokiPoe.StateManager.IsEscapeStateActive)
				return false;

			// Don't level skill gems if we're dead.
			if (LokiPoe.Me.IsDead)
				return false;

			// Only check for skillgem leveling at a fixed interval.
			if (!_needsToUpdate && !_levelWait.IsFinished)
				return false;

			Func<Inventory, Item, SkillGem, bool> eval = (inv, holder, gem) =>
			{
				// Ignore any "globally ignored" gems. This just lets the user move gems around
				// equipment, without having to worry about where or what it is.
				if (ContainsHelper(gem.Name))
				{
					if (GemLevelerSettings.Instance.DebugStatements)
					{
						Log.DebugFormat("[LevelSkillGemTask] {0} => {1}.", gem.Name, "GlobalNameIgnoreList");
					}
					return false;
				}

				// Now look though the list of skillgem strings to level, and see if the current gem matches any of them.
				var ss = string.Format("{0} [{1}: {2}]", gem.Name, inv.PageSlot, holder.GetSocketIndexOfGem(gem));
				foreach (var str in GemLevelerSettings.Instance.SkillGemsToLevelList)
				{
					if (str.Equals(ss, StringComparison.OrdinalIgnoreCase))
					{
						if (GemLevelerSettings.Instance.DebugStatements)
						{
							Log.DebugFormat("[LevelSkillGemTask] {0} => {1}.", gem.Name, str);
						}
						return true;
					}
				}

				// No match, we shouldn't level this gem.
				return false;
			};

			// If we have icons on the hud to process.
			if (LokiPoe.InGameState.SkillGemHud.AreIconsDisplayed)
			{
				// If the InventoryUi is already opened, skip this logic and let the next set run.
				if (!LokiPoe.InGameState.InventoryUi.IsOpened)
				{
					// We need to close blocking windows.
					await Coroutines.CloseBlockingWindows();

					var res = LokiPoe.InGameState.SkillGemHud.HandlePendingLevelUps(eval);

					Log.InfoFormat("[LevelSkillGemTask] SkillGemHud.HandlePendingLevelUps returned {0}.", res);

					if (res == LokiPoe.InGameState.HandlePendingLevelUpResult.GemDismissed ||
						res == LokiPoe.InGameState.HandlePendingLevelUpResult.GemLeveled)
					{
						await Coroutines.LatencyWait();

						await Coroutines.ReactionWait();

						return true;
					}
				}
			}

			if (_needsToUpdate || LokiPoe.InGameState.InventoryUi.IsOpened)
			{
				if (LokiPoe.InGameState.InventoryUi.IsOpened)
				{
					_needsToCloseInventory = false;
				}
				else
				{
					_needsToCloseInventory = true;
				}

				// We need the inventory panel open.
				if (!await OpenInventoryPanel())
				{
					Log.ErrorFormat("[LevelSkillGemTask] OpenInventoryPanel failed.");
					return true;
				}

				// If we have icons on the inventory ui to process.
				// This is only valid when the inventory panel is opened.
				if (LokiPoe.InGameState.InventoryUi.AreIconsDisplayed)
				{
					var res = LokiPoe.InGameState.InventoryUi.HandlePendingLevelUps(eval);

					Log.InfoFormat("[LevelSkillGemTask] InventoryUi.HandlePendingLevelUps returned {0}.", res);

					if (res == LokiPoe.InGameState.HandlePendingLevelUpResult.GemDismissed ||
						res == LokiPoe.InGameState.HandlePendingLevelUpResult.GemLeveled)
					{
						await Coroutines.LatencyWait();

						await Coroutines.ReactionWait();

						return true;
					}
				}
			}

			// Just wait 5-10s between checks.
			_levelWait.Reset(TimeSpan.FromMilliseconds(LokiPoe.Random.Next(5000, 10000)));

			if (_needsToCloseInventory)
			{
				await Coroutines.CloseBlockingWindows();
				_needsToCloseInventory = false;
			}

			_needsToUpdate = false;

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			_needsToUpdate = true;
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}
	}
}