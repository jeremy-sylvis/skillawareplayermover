﻿using System.Windows;
using System.Windows.Controls;

namespace nsStats
{
	/// <summary>
	/// Interaction logic for Gui.xaml
	/// </summary>
	public partial class Gui : UserControl
	{
		public Gui()
		{
			InitializeComponent();
		}

	    private void ResetButton_OnClick(object sender, RoutedEventArgs e)
	    {
	        StatsSettings.Instance.Runs = 0;
	    }
	}
}
