﻿using System.ComponentModel;
using Loki;
using Loki.Common;

namespace nsStats
{
	/// <summary>Settings for the Dev tab. </summary>
	public class StatsSettings : JsonSettings
	{
		private static StatsSettings _instance;

		/// <summary>The current instance for this class. </summary>
		public static StatsSettings Instance
		{
			get
			{
				return _instance ?? (_instance = new StatsSettings());
			}
		}

		/// <summary>The default ctor. Will use the settings path "Stats".</summary>
		public StatsSettings()
			: base(GetSettingsFilePath(Configuration.Instance.Name, string.Format("{0}.json", "Stats")))
		{
		}

        private int _runs;

        /// <summary>
        /// The current number of runs.
        /// </summary>
        [DefaultValue(0)]
        public int Runs
        {
            get
            {
                return _runs;
            }
            set
            {
                if (value.Equals(_runs))
                {
                    return;
                }
                _runs = value;
                NotifyPropertyChanged(() => Runs);
            }
        }
	}
}
