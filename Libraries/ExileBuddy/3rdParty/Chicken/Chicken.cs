﻿using System.Threading.Tasks;
using System.Windows.Controls;
using log4net;
using Loki.Bot;
using Loki.Common;
using Loki.Game;

namespace nsChicken
{
	internal class Chicken : IPlugin
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();
		private Gui _instance;

		/// <summary> The name of the plugin. </summary>
		public string Name
		{
			get { return "Chicken"; }
		}

		/// <summary> The description of the plugin. </summary>
		public string Description
		{
			get { return "A plugin that provides chickening."; }
		}

		/// <summary>The author of the plugin.</summary>
		public string Author
		{
			get { return "Bossland GmbH"; }
		}

		/// <summary>The version of the plugin.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>Initializes this plugin.</summary>
		public void Initialize()
		{
			Log.DebugFormat("[Chicken] Initialize");
		}

		/// <summary>Deinitializes this object. This is called when the object is being unloaded from the bot.</summary>
		public void Deinitialize()
		{
			Log.DebugFormat("[GemLeveler] Deinitialize");
		}

		/// <summary> The plugin start callback. Do any initialization here. </summary>
		public void Start()
		{
			Log.DebugFormat("[Chicken] Start");
		}

		/// <summary> The plugin tick callback. Do any update logic here. </summary>
		public void Tick()
		{
			if (!LokiPoe.IsInGame)
				return;

			if (LokiPoe.Me.IsInTown)
				return;

			if (LokiPoe.Me.IsDead)
				return;

			if (ChickenSettings.Instance.ChickenHeathPercent > 0)
			{
				if (LokiPoe.Me.HealthPercent <= ChickenSettings.Instance.ChickenHeathPercent)
				{
					Log.DebugFormat("[Chicken] Now chickening because our health ({0}) <= chicken threshold ({1}).",
						LokiPoe.Me.HealthPercent, ChickenSettings.Instance.ChickenHeathPercent);
					DoChicken();
					return;
				}
			}

			if (ChickenSettings.Instance.ChickenEsPercent > 0)
			{
				if (LokiPoe.Me.EnergyShieldPercent <= ChickenSettings.Instance.ChickenEsPercent)
				{
					Log.DebugFormat(
						"[Chicken] Now chickening because our energy shield ({0}) <= chicken threshold ({1}).",
						LokiPoe.Me.EnergyShieldPercent, ChickenSettings.Instance.ChickenEsPercent);
					DoChicken();
					// ReSharper disable once RedundantJumpStatement
					return;
				}
			}
		}

		/// <summary> The plugin stop callback. Do any pre-dispose cleanup here. </summary>
		public void Stop()
		{
			Log.DebugFormat("[Chicken] Stop");
		}

		#region Implementation of IEnableable

		/// <summary>Called when the task should be enabled.</summary>
		public void Enable()
		{
			Log.DebugFormat("[Chicken] Enable");
		}

		/// <summary>Called when the task should be disabled.</summary>
		public void Disable()
		{
			Log.DebugFormat("[Chicken] Enable");
		}

		#endregion

		#region Implementation of IConfigurable

		/// <summary>The settings object. This will be registered in the current configuration.</summary>
		public JsonSettings Settings
		{
			get
			{
				return ChickenSettings.Instance;
			}
		}

		/// <summary> The plugin's settings control. This will be added to the Exilebuddy Settings tab.</summary>
		public UserControl Control
		{
			get
			{
				return (_instance ?? (_instance = new Gui()));
			}
		}

		#endregion

		#region Implementation of ILogic

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		#endregion

		#region Override of Object

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			return Name + ": " + Description;
		}

		#endregion

		private void DoChicken()
		{
			var lttserr = LokiPoe.EscapeState.LogoutToTitleScreen();
			if (lttserr != LokiPoe.EscapeState.LogoutError.None)
			{
				Log.ErrorFormat("[Chicken] LogoutToTitleScreen returned {0}.", lttserr);
			}
		}
	}
}
