﻿using System.Collections.Generic;
using System.ComponentModel;
using Loki;
using Loki.Common;
using Newtonsoft.Json;

namespace nsChicken
{
    /// <summary>The method to use for chicken logout.</summary>
    public enum ChickenMethods
    {
        /// <summary>Logout to character selection.</summary>
        CharacterSelection,

        /// <summary>Logout to title screen.</summary>
        TitleScreen,
    }

    /// <summary>Settings for the Chicken. </summary>
    public class ChickenSettings : JsonSettings
    {
        private static ChickenSettings _instance;

        /// <summary>The current instance for this class. </summary>
        public static ChickenSettings Instance
        {
            get { return _instance ?? (_instance = new ChickenSettings()); }
        }

        /// <summary>The default ctor. Will use the settings path "Chicken".</summary>
        public ChickenSettings()
            : base(GetSettingsFilePath(Configuration.Instance.Name, string.Format("{0}.json", "Chicken")))
        {
            // Setup defaults here if needed for properties that don't support DefaultValue.
        }

        private int _chickenHeathPercent;
        private int _chickenEsPercent;

        /// <summary>The % of life to trigger chickening on. Will only trigger when set above 0.</summary>
        [DefaultValue(35)]
        public int ChickenHeathPercent
        {
            get { return _chickenHeathPercent; }
            set
            {
                if (value.Equals(_chickenHeathPercent))
                {
                    return;
                }
                _chickenHeathPercent = value;
                NotifyPropertyChanged(() => ChickenHeathPercent);
            }
        }

        /// <summary>The % of energy shield to trigger chickening on. Will only trigger when set above 0.</summary>
        [DefaultValue(0)]
        public int ChickenEsPercent
        {
            get { return _chickenEsPercent; }
            set
            {
                if (value.Equals(_chickenEsPercent))
                {
                    return;
                }
                _chickenEsPercent = value;
                NotifyPropertyChanged(() => ChickenEsPercent);
            }
        }
    }
}
