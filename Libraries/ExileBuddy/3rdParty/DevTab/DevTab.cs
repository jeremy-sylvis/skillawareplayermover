﻿using System;
using System.Threading.Tasks;
using System.Windows.Controls;
using log4net;
using Loki.Bot;
using Loki.Common;

namespace nsDevTab
{
	internal class DevTab : IPlugin
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private Gui _instance;

		#region Implementation of IAuthored

		/// <summary> The name of the plugin. </summary>
		public string Name
		{
			get
			{
				return "DevTab";
			}
		}

		/// <summary>The author of the plugin.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary> The description of the plugin. </summary>
		public string Description
		{
			get
			{
				return "A plugin that provides a dev tab for runtime code execution.";
			}
		}

		/// <summary>The version of the plugin.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		#endregion

		#region Implementation of IBase

		/// <summary>Initializes this plugin.</summary>
		public void Initialize()
		{
			Log.DebugFormat("[DevTab] Initialize");

			// Make life easier!

			Hotkeys.Register("DevTab.RunCode", System.Windows.Forms.Keys.Q,
				ModifierKeys.Alt | ModifierKeys.Shift,
				h =>
				{
					if (!PluginManager.IsEnabled(this))
						return;

					if (_instance != null)
					{
						_instance.Dispatcher.BeginInvoke(new Action(() => _instance.ButtonExecuteText_Click(null, null)));
					}
				});

			Hotkeys.Register("DevTab.RunFile", System.Windows.Forms.Keys.Z,
				ModifierKeys.Alt | ModifierKeys.Shift,
				h =>
				{
					if (!PluginManager.IsEnabled(this))
						return;

					if (_instance != null)
					{
						_instance.Dispatcher.BeginInvoke(new Action(() => _instance.ButtonExecuteFile_Click(null, null)));
					}
				});
		}

		/// <summary>Deinitializes this object. This is called when the object is being unloaded from the bot.</summary>
		public void Deinitialize()
		{
			Log.DebugFormat("[DevTab] Deinitialize");
		}

		#endregion

		#region Implementation of IRunnable

		/// <summary> The plugin start callback. Do any initialization here. </summary>
		public void Start()
		{
			Log.DebugFormat("[DevTab] Start");
		}

		/// <summary> The plugin tick callback. Do any update logic here. </summary>
		public void Tick()
		{
		}

		/// <summary> The plugin stop callback. Do any pre-dispose cleanup here. </summary>
		public void Stop()
		{
			Log.DebugFormat("[DevTab] Stop");
		}

		#endregion

		#region Implementation of IConfigurable

		/// <summary>The settings object. This will be registered in the current configuration.</summary>
		public JsonSettings Settings
		{
			get
			{
				return DevTabSettings.Instance;
			}
		}

		/// <summary> The plugin's settings control. This will be added to the Exilebuddy Settings tab.</summary>
		public UserControl Control
		{
			get
			{
				return (_instance ?? (_instance = new Gui()));
			}
		}

		#endregion

		#region Implementation of ILogic

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		#endregion

		#region Implementation of IEnableable

		/// <summary> The plugin is being enabled.</summary>
		public void Enable()
		{
			Log.DebugFormat("[DevTab] Enable");
		}

		/// <summary> The plugin is being disabled.</summary>
		public void Disable()
		{
			Log.DebugFormat("[DevTab] Disable");
		}

		#endregion

		#region Override of Object

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			return Name + ": " + Description;
		}

		#endregion
	}
}