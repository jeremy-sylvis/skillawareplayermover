﻿#pragma checksum "..\..\..\Gui.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "23B600A20AEED481DDD6E727DBD6371F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using nsDevTab;


namespace nsDevTab {
    
    
    /// <summary>
    /// Gui
    /// </summary>
    public partial class Gui : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\..\Gui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TextBoxCode;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\Gui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TextBoxFileName;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\Gui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TextBoxClassName;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\Gui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ButtonExecuteText;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\Gui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ButtonExecuteFile;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\Gui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TextBoxAssemblies;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\Gui.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ButtonChooseFile;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DevTab;component/gui.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Gui.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.TextBoxCode = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.TextBoxFileName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.TextBoxClassName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.ButtonExecuteText = ((System.Windows.Controls.Button)(target));
            
            #line 22 "..\..\..\Gui.xaml"
            this.ButtonExecuteText.Click += new System.Windows.RoutedEventHandler(this.ButtonExecuteText_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.ButtonExecuteFile = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\..\Gui.xaml"
            this.ButtonExecuteFile.Click += new System.Windows.RoutedEventHandler(this.ButtonExecuteFile_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.TextBoxAssemblies = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.ButtonChooseFile = ((System.Windows.Controls.Button)(target));
            
            #line 36 "..\..\..\Gui.xaml"
            this.ButtonChooseFile.Click += new System.Windows.RoutedEventHandler(this.ButtonChooseFile_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

