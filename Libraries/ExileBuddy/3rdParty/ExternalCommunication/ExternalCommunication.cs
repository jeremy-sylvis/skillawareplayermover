﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Controls;
using log4net;
using Loki.Bot;
using Loki.Common;
using Loki.Game;

namespace nsExternalCommunication
{
	internal class ExternalCommunication : IPlugin
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private Gui _instance;

		#region Implementation of IAuthored

		/// <summary> The name of the plugin. </summary>
		public string Name
		{
			get
			{
				return "ExternalCommunication";
			}
		}

		/// <summary>The author of the plugin.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary> The description of the plugin. </summary>
		public string Description
		{
			get
			{
				return "A plugin that shows an example of external communication with the bot.";
			}
		}

		/// <summary>The version of the plugin.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		#endregion

		#region Implementation of IBase

		/// <summary>Initializes this plugin.</summary>
		public void Initialize()
		{
			Log.DebugFormat("[ExternalCommunication] Initialize");
		}

		/// <summary>Deinitializes this object. This is called when the object is being unloaded from the bot.</summary>
		public void Deinitialize()
		{
			Log.DebugFormat("[ExternalCommunication] Deinitialize");
		}

		#endregion

		#region Implementation of IRunnable

		/// <summary> The plugin start callback. Do any initialization here. </summary>
		public void Start()
		{
			Log.DebugFormat("[ExternalCommunication] Start");
		}

		/// <summary> The plugin tick callback. Do any update logic here. </summary>
		public void Tick()
		{
		}

		/// <summary> The plugin stop callback. Do any pre-dispose cleanup here. </summary>
		public void Stop()
		{
			Log.DebugFormat("[ExternalCommunication] Stop");
		}

		#endregion

		#region Implementation of IConfigurable

		/// <summary>The settings object. This will be registered in the current configuration.</summary>
		public JsonSettings Settings
		{
			get
			{
				return ExternalCommunicationSettings.Instance;
			}
		}

		/// <summary> The plugin's settings control. This will be added to the Exilebuddy Settings tab.</summary>
		public UserControl Control
		{
			get
			{
				return (_instance ?? (_instance = new Gui()));
			}
		}

		#endregion

		#region Implementation of ILogic

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			if (name == "wndproc_hook")
			{
				var hwnd = (IntPtr) param[0];
				var msg = (int) param[1];
				var wParam = ((IntPtr) param[2]).ToInt32();
				var lParam = (IntPtr) param[3];

				// WM_USER + 1: Allow programs to check various state.
				if (msg == 0x401)
				{
					// Keep track of any messages encountred.
					//Log.InfoFormat("[WndProc] {0}: {1}", msg, wParam);

					// Is the bot fully loaded. This always returns 1, but when users query it, the message will
					// return 0 since it's not processed.
					if (wParam == 0)
					{
						return LokiPoe.IsBotFullyLoaded ? (IntPtr) 1 : IntPtr.Zero;
					}

					// Is the bot running.
					if (wParam == 1)
					{
						return BotManager.IsRunning ? (IntPtr) 1 : IntPtr.Zero;
					}

					// Is the bot stopping.
					if (wParam == 2)
					{
						return BotManager.IsStopping ? (IntPtr) 1 : IntPtr.Zero;
					}

					// Is the client detected as being frozen.
					if (wParam == 3)
					{
						return BotManager.ClientFrozen ? (IntPtr) 1 : IntPtr.Zero;
					}

					// Time since last tick.
					if (wParam == 4)
					{
						if (!BotManager.IsRunning)
							return IntPtr.Zero;

						return (IntPtr) (DateTime.Now - BotManager.TimeOfLastTick).TotalMilliseconds;
					}

					// PoE client id attached to
					if (wParam == 5)
					{
						return (IntPtr)LokiPoe.Memory.Process.Id;
					}

					// The actual bot window handle. 
					if (wParam == 6)
					{
						return LokiPoe.BotWindowHandle;
					}

					// Is the client in game.
					if (wParam == 7)
					{
						return LokiPoe.IsInGame ? (IntPtr)1 : IntPtr.Zero;
					}

                    // Number of exceptions in the last minute of execution.
                    if (wParam == 8)
                    {
                        return (IntPtr)BotManager.ExceptionCount;
                    }

                    // Log unhandled stuff instead.
                    Log.InfoFormat("[WndProc] {0}: {1}", msg, wParam);

					// Unhandled.
					return IntPtr.Zero;
				}

				return null;
			}

			return null;
		}

		#endregion

		#region Implementation of IEnableable

		/// <summary> The plugin is being enabled.</summary>
		public void Enable()
		{
			Log.DebugFormat("[ExternalCommunication] Enable");
		}

		/// <summary> The plugin is being disabled.</summary>
		public void Disable()
		{
			Log.DebugFormat("[ExternalCommunication] Disable");
		}

		#endregion

		#region Override of Object

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			return Name + ": " + Description;
		}

		#endregion
	}
}