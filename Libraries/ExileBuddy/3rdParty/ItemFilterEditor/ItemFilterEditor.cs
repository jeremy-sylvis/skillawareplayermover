﻿using System.Threading.Tasks;
using System.Windows.Controls;
using log4net;
using Loki.Bot;
using Loki.Common;

namespace nsItemFilterEditor
{
	internal class ItemFilterEditor : IPlugin
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private Gui _instance;

		/// <summary> The name of the plugin. </summary>
		public string Name
		{
			get
			{
				return "ItemFilterEditor";
			}
		}

		/// <summary> The description of the plugin. </summary>
		public string Description
		{
			get
			{
				return "A plugin that provides a basic item filter editor.";
			}
		}

		/// <summary>The author of the plugin.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of the plugin.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>Initializes this plugin.</summary>
		public void Initialize()
		{
			Log.DebugFormat("[ItemFilterEditor] Initialize");
		}

		/// <summary>Deinitializes this object. This is called when the object is being unloaded from the bot.</summary>
		public void Deinitialize()
		{
			Log.DebugFormat("[ItemFilterEditor] Deinitialize");
		}

		/// <summary> The plugin start callback. Do any initialization here. </summary>
		public void Start()
		{
			Log.DebugFormat("[ItemFilterEditor] Start");

			// Set the new item eval.
			ItemEvaluator.Instance = ConfigurableItemEvaluator.Instance;
		}

		/// <summary> The plugin tick callback. Do any update logic here. </summary>
		public void Tick()
		{
		}

		/// <summary> The plugin stop callback. Do any pre-dispose cleanup here. </summary>
		public void Stop()
		{
			Log.DebugFormat("[ItemFilterEditor] Stop");
		}

		#region Implementation of IEnableable

		/// <summary>Called when the task should be enabled.</summary>
		public void Enable()
		{
			Log.DebugFormat("[ItemFilterEditor] Enable");

			// Set the new item eval. Set it here in case the user wants to mess with it before starting the bot!
			ItemEvaluator.Instance = ConfigurableItemEvaluator.Instance;
		}

		/// <summary>Called when the task should be disabled.</summary>
		public void Disable()
		{
			Log.DebugFormat("[ItemFilterEditor] Enable");
		}

		#endregion

		#region Implementation of IConfigurable

		/// <summary>The settings object. This will be registered in the current configuration.</summary>
		public JsonSettings Settings
		{
			get
			{
				return ItemFilterEditorSettings.Instance;
			}
		}

		/// <summary> The plugin's settings control. This will be added to the Exilebuddy Settings tab.</summary>
		public UserControl Control
		{
			get
			{
				return (_instance ?? (_instance = new Gui()));
			}
		}

		#endregion

		#region Implementation of ILogic

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		#endregion

		#region Override of Object

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			return Name + ": " + Description;
		}

		#endregion
	}
}