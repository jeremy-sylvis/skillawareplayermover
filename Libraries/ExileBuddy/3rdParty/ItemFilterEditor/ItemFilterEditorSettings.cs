﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Loki;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;
using Loki.Game.Objects.Items;
using Newtonsoft.Json;

namespace nsItemFilterEditor
{
	/// <summary>Settings for the ItemFilterEditor plugin. </summary>
	public class ItemFilterEditorSettings : JsonSettings
	{
		private static ItemFilterEditorSettings _instance;

		/// <summary>The current instance for this class. </summary>
		public static ItemFilterEditorSettings Instance
		{
			get
			{
				return _instance ?? (_instance = new ItemFilterEditorSettings());
			}
		}

		/// <summary>The default ctor. Will use the settings path "ItemFilterEditor".</summary>
		public ItemFilterEditorSettings()
			: base(GetSettingsFilePath(Configuration.Instance.Name, string.Format("{0}.json", "ItemFilterEditor")))
		{
		}
	}
}