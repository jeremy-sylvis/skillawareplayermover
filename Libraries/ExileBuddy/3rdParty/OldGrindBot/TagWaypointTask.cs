﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Common;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary></summary>
	public class TagWaypointTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private AreaStateCache.Location _waypointLocation;
		private int _wpTries;
		private readonly Stopwatch _displayStopwatch = Stopwatch.StartNew();
		private bool _skip;
		private bool _checkPath;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "TagWaypointTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task will tag a waypoint if it is not already unlocked.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint) param[0];
				var newSeed = (uint) param[1];
				var oldArea = (DatWorldAreaWrapper) param[2];
				var newArea = (DatWorldAreaWrapper) param[3];

				Reset();

				return true;
			}

			if (type == "oldgrindbot_local_area_changed_event")
			{
				Reset();

				return true;
			}

			if (type == "core_player_died_event")
			{
				var totalDeathsForInstance = (int) param[0];

				Reset();

				return true;
			}

			if (type != "task_execute")
				return false;

			if (_skip)
				return false;

			// Don't keep trying.
			if (_wpTries > 3)
				return false;

			// We don't need to execute in town or when we are dead.
			if (LokiPoe.Me.IsInTown || LokiPoe.Me.IsDead || LokiPoe.Me.IsInHideout)
				return false;

			// If the current area doesn't have a waypoint, then we don't need to execute.
			if (!LokiPoe.CurrentWorldArea.HasWaypoint)
			{
				Log.InfoFormat("[TagWaypointTask] Now skipping the task because the current area doesn't have a waypoint.");
				_skip = true;
				return false;
			}

			if (AreaStateCache.Current.HasWaypointEntry)
			{
				Log.InfoFormat("[TagWaypointTask] Now skipping the task because we already have the waypoint for this area.");
				_skip = true;
				return false;
			}

			var display = false;
			if (_displayStopwatch.ElapsedMilliseconds > 1000)
			{
				display = true;
				_displayStopwatch.Restart();
			}

			// If we don't know where the WP is yet, then look for it.
			if (_waypointLocation == null)
			{
				// If we have the waypoint for the current town, use it.
				if (AreaStateCache.Current.HasLocation("Waypoint"))
				{
					_waypointLocation = AreaStateCache.Current.GetLocations("Waypoint").FirstOrDefault();
					Log.DebugFormat(
						"[TagWaypointTask] The waypoint's location has been found from AreaStateCache.");
					return true;
				}

				// Wait for AreaStateCache to update.
				if (LokiPoe.ObjectManager.Waypoint != null)
				{
					if (display)
					{
						Log.DebugFormat(
							"[TagWaypointTask] A waypoint is in range, but we do not have its location yet. Now waiting for the AreaStateCache to update.");
					}
					return true;
				}

				var wpLocations = StaticLocationManager.StaticWaypointLocations;
				if (!wpLocations.Any())
				{
					return false;
				}

				if (display)
				{
					Log.DebugFormat(
						"[TagWaypointTask] A waypoint is not in range. Now moving to a location where it should come into view.");
				}

				var position = wpLocations.First();

				// Need this for Sceptre of God.
				if (_checkPath)
				{
					if (!ExilePather.PathExistsBetween(LokiPoe.MyPosition, position))
					{
						Log.InfoFormat("[TagWaypointTask] Now skipping the task because there is no path to the waypoint from this floor.");
						_skip = true;
						return false;
					}
					_checkPath = false;
				}

				await Coroutines.CloseBlockingWindows();

				if (!PlayerMover.MoveTowards(position))
				{
					Log.ErrorFormat("[TagWaypointTask] MoveTowards failed for {0}.", position);
					await Coroutines.FinishCurrentAction();
				}

				return true;
			}

			if (display)
			{
				Log.DebugFormat("[TagWaypointTask] The waypoint to move to is {0} at {1}.",
					_waypointLocation.Id, _waypointLocation.Position);
			}

			//var pathDistance = ExilePather.PathDistance(LokiPoe.MyPosition, _waypointLocation.Position);
			var pathDistance = LokiPoe.MyPosition.Distance(_waypointLocation.Position);
			if (pathDistance > 20)
			{
				if (display)
				{
					Log.DebugFormat(
						"[TagWaypointTask] Now moving towards the waypoint {0} because it is {1} away.",
						_waypointLocation.Name, pathDistance);
				}

				await Coroutines.CloseBlockingWindows();

				if (!PlayerMover.MoveTowards(_waypointLocation.Position))
				{
					Log.ErrorFormat("[TagWaypointTask] MoveTowards failed for {0}.",
						_waypointLocation.Position);
					_waypointLocation = null;
					await Coroutines.FinishCurrentAction();
				}

				return true;
			}

			++_wpTries;

			if (_wpTries > 3)
			{
				Log.ErrorFormat(
					"[TagWaypointTask] We have failed 3 times to interact with the waypoint. This task will now be skipped.");
				return false;
			}

			await Coroutines.CloseBlockingWindows();

			await Coroutines.FinishCurrentAction();

			var wp = LokiPoe.ObjectManager.Waypoint;

			if (!await Coroutines.InteractWith(wp))
			{
				Log.ErrorFormat("[TagWaypointTask] InteractWith failed for the waypoint.");
				return true;
			}

			if (!await CoroutinesV3.WaitForWorldPanel())
			{
				Log.ErrorFormat("[TagWaypointTask] WaitForWorldPanel failed for the waypoint.");
				return true;
			}

			await Coroutine.Sleep(1000);

			await Coroutines.CloseBlockingWindows();

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[TagWaypointTask] Now resetting task state.");
			_waypointLocation = null;
			_wpTries = 0;
			_skip = false;
			_checkPath = true;
		}
	}
}