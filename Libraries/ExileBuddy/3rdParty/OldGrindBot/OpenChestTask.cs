﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;
using Loki.Common;

namespace OldGrindBot
{
	/// <summary>
	/// This task handles opening a non-dangerous chest.
	/// </summary>
	public class OpenChestTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private readonly int _leashRange;
		private readonly bool _onlyExecuteInGrindZone;

		private AreaStateCache.ChestLocation _chestLocation;
		private readonly Stopwatch _chestStopwatch = new Stopwatch();
		private readonly Stopwatch _interactStopwatch = new Stopwatch();
		private readonly Stopwatch _moveStopwatch = new Stopwatch();
		private int _interactTries;
		private readonly Stopwatch _displayStopwatch = Stopwatch.StartNew();
		private Vector2i _startLocation;

		/// <summary>
		/// A ctor that sets a user defined leash value.
		/// </summary>
		public OpenChestTask(int leashRange, bool onlyExecuteInGrindZone)
		{
			_leashRange = leashRange;
			_onlyExecuteInGrindZone = onlyExecuteInGrindZone;
		}

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "OpenChestTask (Leash " + _leashRange + ")";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task handles opening chests.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				_chestLocation = null;
				_chestStopwatch.Reset();

				return true;
			}

			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsDead || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout)
				return false;

			if (LokiPoe.CurrentWorldArea.IsOverworldArea && _onlyExecuteInGrindZone &&
			    LokiPoe.CurrentWorldArea.Id != OldGrindBotSettings.Instance.GrindZoneId)
			{
				return false;
			}

			//----------

			// This causes a looping issue
			// https://gyazo.com/408c55a10d7a2f54bdd496ee26e5fa80

			var boss = LokiPoe.ObjectManager.Shavronne;
			var arena = LokiPoe.ObjectManager.ShavronneArena;
			if (boss != null && !boss.IsDead && arena != null && arena.Distance < 100)
		    {
				Log.InfoFormat("[" + Name + "] Not executing because of Shavronne.");
				return false;
		    }

		    boss = LokiPoe.ObjectManager.Doedre;
			arena = LokiPoe.ObjectManager.DoedreArena;
			if (boss != null && !boss.IsDead && arena != null && arena.Distance < 100)
            {
				Log.InfoFormat("[" + Name + "] Not executing because of Doedre.");
				return false;
            }

            boss = LokiPoe.ObjectManager.Maligaro;
			arena = LokiPoe.ObjectManager.MaligaroArena;
			if (boss != null && !boss.IsDead && arena != null && arena.Distance < 100)
            {
				Log.InfoFormat("[" + Name + "] Not executing because of Maligaro.");
				return false;
            }

			//----------

			if (_chestLocation == null || Blacklist.Contains(_chestLocation.Id))
			{
				var myPos = LokiPoe.MyPosition;

				_chestLocation =
					AreaStateCache.Current.ChestLocations.Where(
						c =>
							!c.IsBlacklisted &&
							!c.IsOpened &&
							!c.IsLocked &&
							c.IsTargetable &&
							!OldGrindBotSettings.Instance.GlobalChestsToIgnore.Any(wrapper => wrapper.IsEnabled && wrapper.Name == c.Name) &&
							(_leashRange == -1 || ExilePather.PathDistance(myPos, c.Position) < _leashRange))
						.OrderBy(c => c.Position.Distance(myPos))
						.FirstOrDefault();

				_chestStopwatch.Reset();
				_interactStopwatch.Reset();

				_interactTries = 0;

				_startLocation = myPos;
				_moveStopwatch.Reset();

				if (_chestLocation != null)
				{
					Log.InfoFormat("[{0}] _itemLocation set to: {1} with id {2} at {3}.", Name, _chestLocation.Name, _chestLocation.Id,
						_chestLocation.Position);
				}
			}

			// We need a chest location to execute!
			if (_chestLocation == null)
				return false;

			// Make sure we handle this state change as we move towards the chest.
			var chest = LokiPoe.ObjectManager.GetObjectById<Chest>(_chestLocation.Id);
			if (chest != null)
			{
				if (chest.IsOpened)
				{
					_chestLocation.IsOpened = true;
					_chestLocation = null;
					return true;
				}
				// Handle strongboxes, but not perandus chests.
				if (chest.IsLocked && chest.Components.TransitionableComponent == null)
				{
					_chestLocation = null;
					return true;
				}
			}

			var display = false;

			if (_displayStopwatch.ElapsedMilliseconds > 1000)
			{
				display = true;
				_displayStopwatch.Restart();
			}

			if (display)
			{
				Log.DebugFormat("[" +
					Name + "] The current chest to open is [{0}] {1} at {2}. We have been on this task for {3} and have been attempting to interact for {4}.",
					_chestLocation.Id, _chestLocation.Name, _chestLocation.Position, _chestStopwatch.Elapsed,
					_interactStopwatch.Elapsed);
			}

			if (_moveStopwatch.ElapsedMilliseconds > 1000)
			{
				var myPos = LokiPoe.Me.Position;
				if (_startLocation == myPos)
				{
					Blacklist.Add(_chestLocation.Id, TimeSpan.FromHours(1),
						string.Format("{0} elapsed while trying to move towards the chest, but we're stuck.", _moveStopwatch.Elapsed));

					_chestLocation = null;

					return true;
				}
				_startLocation = myPos;
				_moveStopwatch.Reset();
			}

			// Catch a chest issue seen in Library.
		    if (_chestStopwatch.Elapsed.TotalMinutes >= 1)
		    {
                Blacklist.Add(_chestLocation.Id, TimeSpan.FromHours(1),
                    string.Format("{0} elapsed while moving towards the chest.", _chestStopwatch.Elapsed));

                _chestLocation = null;

                return true;
		    }

			_chestStopwatch.Start();

			var dd = LokiPoe.MyPosition.Distance(_chestLocation.Position);
            if (dd > 20)
			{
				if (display)
				{
					Log.DebugFormat("[" + Name + "] Now moving towards the chest {1} because it is {0} away (euclidean).", dd, _chestLocation.Id);
					_displayStopwatch.Restart();
				}

				_moveStopwatch.Start();

				if (!PlayerMover.MoveTowards(_chestLocation.Position))
				{
					Log.ErrorFormat("[" + Name + "] MoveTowards failed for {0}.", _chestLocation.Position);

					Blacklist.Add(_chestLocation.Id, TimeSpan.FromHours(1),
						"Could not pathfind to the chest location.");

					_chestLocation = null;

					await Coroutines.FinishCurrentAction();
				}

				_moveStopwatch.Stop();

				_chestStopwatch.Stop();

				return true;
			}

			var pathDistance = ExilePather.PathDistance(LokiPoe.MyPosition, _chestLocation.Position);
			var canSee = ExilePather.CanObjectSee(LokiPoe.Me, _chestLocation.Position);
			if (pathDistance > 20 || (!canSee && pathDistance > 10))
			{
				if (display)
				{
					Log.DebugFormat("[" + Name + "] Now moving towards the chest {1} because it is {0} away ({2}).",
						pathDistance,
						_chestLocation.Id, canSee ? "can see" : "can't see");
					_displayStopwatch.Restart();
				}

				_moveStopwatch.Start();

				if (!PlayerMover.MoveTowards(_chestLocation.Position))
				{
					Log.ErrorFormat("[" + Name + "] MoveTowards failed for {0}.", _chestLocation.Position);

					Blacklist.Add(_chestLocation.Id, TimeSpan.FromHours(1),
						"Could not pathfind to the chest location.");

					_chestLocation = null;

					await Coroutines.FinishCurrentAction();
				}

				_moveStopwatch.Stop();

				_chestStopwatch.Stop();

				return true;
			}

			chest = LokiPoe.ObjectManager.GetObjectById<Chest>(_chestLocation.Id);
			if (chest == null)
			{
				Blacklist.Add(_chestLocation.Id, TimeSpan.FromHours(1),
					"The chest does not exist anymore.");

				_chestLocation = null;

				return true;
			}

			// Make sure we handle this state change.
			if (chest.IsOpened)
			{
				_chestLocation.IsOpened = true;
				_chestLocation = null;
				return true;
			}
			// Handle strongboxes, but not perandus chests.
			if (chest.IsLocked && chest.Components.TransitionableComponent == null)
			{
				_chestLocation = null;
				return true;
			}

			if (_interactStopwatch.ElapsedMilliseconds > 3000 || _interactTries > 5)
			{
				Blacklist.Add(_chestLocation.Id, TimeSpan.FromHours(1),
					"Timed out when trying to interact with the chest.");

				_chestLocation = null;

				return true;
			}

			await Coroutines.FinishCurrentAction();

			await Coroutines.CloseBlockingWindows();

			_interactStopwatch.Start();

			++_interactTries;

            Log.DebugFormat("[" + Name + "] Now attempting to interact with the chest. This is the #{0} attempt.",
				_interactTries);

			await Coroutines.InteractWith<Chest>(chest);

			_chestStopwatch.Stop();
			_interactStopwatch.Stop();

			// Smooth out chest opening as well. We don't want to wait for strongboxes though.
			if (!chest.IsStrongBox)
			{
				var sw = Stopwatch.StartNew();
				while (!chest.IsOpened)
				{
					await Coroutine.Yield();

					if (sw.ElapsedMilliseconds > 1000)
						break;
				}
			}

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
			_chestLocation = null;
			_chestStopwatch.Reset();
		}
	}
}