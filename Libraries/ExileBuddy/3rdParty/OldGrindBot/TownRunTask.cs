﻿using System;
using System.Threading.Tasks;
using log4net;
using Loki;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Common;

namespace OldGrindBot
{
	/// <summary>
	/// A task to go back to town during the middle of a bot run.
	/// </summary>
	public class TownRunTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private readonly SetSettingsDelegate<bool> _setCameFromPortal;
		private readonly GetSettingsDelegate<int> _getNeedsTownRun;
		private readonly SetSettingsDelegate<int> _setNeedsTownRun;

		/// <summary>
		/// Ctor that sets the delegate for obtaining current settings.
		/// </summary>
		/// <param name="setCameFromPortal"></param>
		/// <param name="getNeedsTownRun"></param>
		/// <param name="setNeedsTownRun"></param>
		public TownRunTask(SetSettingsDelegate<bool> setCameFromPortal, GetSettingsDelegate<int> getNeedsTownRun,
			SetSettingsDelegate<int> setNeedsTownRun)
		{
			_setCameFromPortal = setCameFromPortal;
			_getNeedsTownRun = getNeedsTownRun;
			_setNeedsTownRun = setNeedsTownRun;
		}

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "TownRunTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "A task to go back to town during the middle of a bot run.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout || LokiPoe.Me.IsDead)
				return false;

			var type_ = _getNeedsTownRun();

			if (type_ == 0)
				return false;

			// We do not need to return to the grind zone. We can take a waypoint to town if one is nearby.
			if (type_ == 2)
			{
				foreach (var location in AreaStateCache.Current.ItemLocations)
				{
					Log.InfoFormat("[TownRunTask] The location {0} [{1}] will be abadoned.", location.Id, location.Name);
				}

				// We have to have a wp close by.
				var wp = LokiPoe.ObjectManager.Waypoint;
				if (wp != null)
				{
					// It also needs to be within walking distance to avoid issues.
					if (ExilePather.PathDistance(LokiPoe.MyPosition, ExilePather.FastWalkablePositionFor(wp)) < 250)
					{
						var twterr = await CoroutinesV3.TakeWaypointTo(LokiPoe.CurrentWorldArea.GoverningTown.Id, false, -1);
						if (twterr == CoroutinesV3.TakeWaypointToError.None)
						{
							_setNeedsTownRun(0);
							return true;
						}

						Log.ErrorFormat("[TownRunTask] TakeWaypointTo returned {0}.", twterr);
					}
				}
			}

			Log.InfoFormat("[TownRunTask] Now calling CreatePortalToTown.");
			if (await CoroutinesV3.CreatePortalToTown())
			{
				if (await CoroutinesV3.TakeClosestPortal())
				{
					_setCameFromPortal(type_ == 1);
					_setNeedsTownRun(0);

					return true;
				}
			}

			Log.ErrorFormat("[TownRunTask] There is no way to get back to town. Now logging out to get back to town.");

			await CoroutinesV3.LogoutToTitleScreen();

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}
	}
}