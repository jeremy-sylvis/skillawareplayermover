﻿using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace OldGrindBot
{
	/// <summary>
	/// This task helps the bot enter the 3 arenas in The Harvest.
	/// </summary>
	public class EnterArenaTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();
		private bool _skip;
		private bool _enteredMaligaro;
		private bool _enteredShavronne;
		private bool _enteredDoedre;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get { return "EnterArenaTask"; }
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get { return "This task helps the bot enter the 3 arenas in The Harvest."; }
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get { return "Bossland GmbH"; }
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get { return "0.0.1.1"; }
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();
				return true;
			}

			if (type == "core_player_died_event")
			{
				var totalDeathsForInstance = (int) param[0];

				Reset();
				return true;
			}

			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsDead)
				return false;

			if (_skip)
				return false;

			if (LokiPoe.CurrentWorldArea.Name != "The Harvest")
			{
				Log.InfoFormat("[EnterArenaTask] The current area does not contain arenas. Skipping execution until a restart or area change.");
				_skip = true;
				return false;
			}

			if (!_enteredMaligaro)
			{
				var arena = LokiPoe.ObjectManager.MaligaroArena;
				if (arena != null && arena.IsTargetable)
				{
					if (await TakeArena(arena))
					{
						_enteredMaligaro = true;
						Log.InfoFormat("[EnterArenaTask] _enteredMaligaro = true");
					}
					return true;
				}
				if (arena != null)
				{
					_enteredMaligaro = true;
					Log.InfoFormat("[EnterArenaTask] _enteredMaligaro = true (not targetable)");
				}
			}

			if (!_enteredShavronne)
			{
				var arena = LokiPoe.ObjectManager.ShavronneArena;
				if (arena != null && arena.IsTargetable)
				{
					if (await TakeArena(arena))
					{
						_enteredShavronne = true;
						Log.InfoFormat("[EnterArenaTask] _enteredShavronne = true");
					}
					return true;
				}
				if (arena != null)
				{
					_enteredShavronne = true;
					Log.InfoFormat("[EnterArenaTask] _enteredShavronne = true (not targetable)");
				}
			}

			if (!_enteredDoedre)
			{
				var arena = LokiPoe.ObjectManager.DoedreArena;
				if (arena != null && arena.IsTargetable)
				{
					if (await TakeArena(arena))
					{
						_enteredDoedre = true;
						Log.InfoFormat("[EnterArenaTask] _enteredDoedre = true");
					}
					return true;
				}
				if (arena != null)
				{
					_enteredDoedre = true;
					Log.InfoFormat("[EnterArenaTask] _enteredDoedre = true (not targetable)");
				}
			}

			var boss = LokiPoe.ObjectManager.Shavronne;
			if (boss != null)
			{
				if (!boss.IsDead)
				{
					if (!PlayerMover.MoveTowards(boss.Position))
					{
						Log.ErrorFormat("[EnterArenaTask] MoveTowards failed for {0}.", boss.Position);
						await Coroutines.FinishCurrentAction();
					}
					return true;
				}
			}

			boss = LokiPoe.ObjectManager.Doedre;
			if (boss != null)
			{
				if (!boss.IsDead)
				{
					if (!PlayerMover.MoveTowards(boss.Position))
					{
						Log.ErrorFormat("[EnterArenaTask] MoveTowards failed for {0}.", boss.Position);
						await Coroutines.FinishCurrentAction();
					}
					return true;
				}
			}

			boss = LokiPoe.ObjectManager.Maligaro;
			if (boss != null)
			{
				if (!boss.IsDead)
				{
					if (!PlayerMover.MoveTowards(boss.Position))
					{
						Log.ErrorFormat("[EnterArenaTask] MoveTowards failed for {0}.", boss.Position);
						await Coroutines.FinishCurrentAction();
					}
					return true;
				}
			}

			return false;
		}

		private async Task<bool> TakeArena(NetworkObject arena)
		{
			if (arena.Distance < 50)
			{
				var res = await CoroutinesV3.TakeAreaTransition(arena, false, -1, true);
				Log.InfoFormat("[TakeArena] TakeAreaTransition returned {0} for {1}.", res, arena.Name);
				return res == CoroutinesV3.TakeAreaTransitionError.None;
			}

			if (!PlayerMover.MoveTowards(arena.Position))
			{
				Log.ErrorFormat("[TakeArena] MoveTowards failed for {0}.", arena.Position);
				await Coroutines.FinishCurrentAction();
			}

			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[EnterArenaTask] Now resetting task state.");
			_skip = false;
			_enteredMaligaro = false;
			_enteredShavronne = false;
			_enteredDoedre = false;
		}
	}
}
