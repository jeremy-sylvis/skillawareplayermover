﻿using System;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Game;
using Loki.Common;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary>
	/// A task to vendor items.
	/// </summary>
	public class SellTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private readonly CoroutinesV3.NeedsToSellDelegate _needsToSell;
		private readonly CoroutinesV3.SellingCompleteDelegate _sellingComplete;
		private readonly CoroutinesV3.SellItemsDelegate _shouldSellItem;
		private readonly CoroutinesV3.ShouldAcceptSellOfferDelegate _shouldAcceptOffer;
		private readonly object _user;
		private int _tries;
		private readonly TriggerOnSellDelegate _triggerOnSell;

		private static int _delayAfterFailureMs = 15000;

		/// <summary>
		/// How long to wait after SellItemsCoroutine fails before leaving task execution to try again.
		/// </summary>
		public static int DelayAfterFailureMs
		{
			get
			{
				return _delayAfterFailureMs;
			}
			set
			{
				_delayAfterFailureMs = value;
				if (_delayAfterFailureMs < 1000)
					_delayAfterFailureMs = 1000;
				Log.InfoFormat("[SellTask] DelayAfterFailureMs = {0}", _delayAfterFailureMs);
			}
		}

		/// <summary>
		/// Ctor that sets the delegate for obtaining current settings.
		/// </summary>
		/// <param name="needsToSell">The delegate to determine if the task needs to run.</param>
		/// <param name="sellingComplete">The delegate to run after selling has completed.</param>
		/// <param name="shouldAcceptOffer"></param>
		/// <param name="shouldSellItem">The delegate for determining if an item needs to be sold.</param>
		/// <param name="triggerOnSell">The delegate to invoke when an item is about to be sold.</param>
		/// <param name="user">The user object to pass to SellItemsCoroutine.</param>
		public SellTask(
			CoroutinesV3.NeedsToSellDelegate needsToSell,
			CoroutinesV3.SellingCompleteDelegate sellingComplete,
			CoroutinesV3.ShouldAcceptSellOfferDelegate shouldAcceptOffer,
			CoroutinesV3.SellItemsDelegate shouldSellItem,
			object user = null,
			TriggerOnSellDelegate triggerOnSell = null)
		{
			_needsToSell = needsToSell;
			_sellingComplete = sellingComplete;
			_shouldSellItem = shouldSellItem;
			_shouldAcceptOffer = shouldAcceptOffer;
			_user = user;
			_triggerOnSell = triggerOnSell;
		}

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "SellTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "A task to vendor items in the inventory.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();

				return true;
			}

			if (type != "task_execute")
				return false;

			// Only sell in town.
			if (!LokiPoe.Me.IsInTown && !LokiPoe.Me.IsInHideout)
			{
				return false;
			}

			// Make sure we don't sit around, hammering selling all day long.
			if (_tries > 3)
			{
				Log.ErrorFormat(
					"[SellTask] The bot has failed to sell items 3 times. Now stopping the bot because it cannot continue.");
				BotManager.Stop();
				return true;
			}

			// If we don't need to sell, don't keep executing.
			if (!_needsToSell())
			{
				return false;
			}

			++_tries;

			var res = await CoroutinesV3.SellItemsCoroutine(_shouldAcceptOffer, _shouldSellItem, _user, _triggerOnSell);
			if (res != CoroutinesV3.SellItemsCoroutineError.None)
			{
				_sellingComplete(true);
				Loki.Bot.Utility.BroadcastEventExecute("oldgrindbot_sell_complete", true); // Doing this here so user funcs don't need to.

				Log.DebugFormat("[SellTask] SellItemsCoroutine returned {0}. This is try #{1}. Waiting {2} ms and then trying again.",
					res, _tries, DelayAfterFailureMs);

				await Coroutine.Sleep(DelayAfterFailureMs);

				return true;
			}

			_tries = 0;

			_sellingComplete(false);
			Loki.Bot.Utility.BroadcastEventExecute("oldgrindbot_sell_complete", false); // Doing this here so user funcs don't need to.

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[SellTask] Now resetting task state.");
			_tries = 0;
		}
	}
}