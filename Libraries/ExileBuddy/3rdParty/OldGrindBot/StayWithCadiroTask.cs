﻿using System;
using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Game;
using Loki.Common;

namespace OldGrindBot
{
	/// <summary>
	/// This task makes the bot stay near cadiro.
	/// </summary>
	public class StayWithCadiroTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "StayWithCadiroTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task makes the bot stay near cadiro.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsDead || LokiPoe.Me.IsInTown)
				return false;

			var cadiro = LokiPoe.ObjectManager.Cadiro;
			if (cadiro == null)
			{
				return false;
			}

			// Only care about the first time we see him.
			if (!cadiro.HasNpcFloatingIcon)
			{
				return false;
			}

			if (cadiro.Distance < 30)
			{
				return true;
			}

			if (!PlayerMover.MoveTowards(cadiro.Position))
			{
				Log.ErrorFormat("[StayWithCadiroTask] MoveTowards failed for {0}.", cadiro.Position);
				await Coroutines.FinishCurrentAction();
			}
			else
			{
				Log.DebugFormat("[StayWithCadiroTask] Moving towards Cadiro.");
			}

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}
	}
}
