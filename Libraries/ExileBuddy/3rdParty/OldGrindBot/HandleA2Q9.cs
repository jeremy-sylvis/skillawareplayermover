﻿using System;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary>
	/// This task handles an aspect of A2Q9.
	/// </summary>
	public class HandleA2Q9 : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private bool _skip;

		private bool _hasRootsLocation;
		private Vector2i _rootsLocation;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "HandleA2Q9";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task handles an aspect of A2Q9.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();
				return true;
			}

			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsDead)
				return false;

			if (_skip)
				return false;

			if (LokiPoe.CurrentWorldArea.Name != "The Wetlands")
			{
				Log.InfoFormat("[HandleA2Q9] The current area does not contain the quest. Skipping execution until a restart or area change.");
				_skip = true;
				return false;
			}

			if (!_hasRootsLocation)
				return false;

			// Check to see if we have the inventory item yet.
			var qi = LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(i => i.FullName == "Maligaro's Spike");
			if (qi == null)
			{
				Log.InfoFormat("[HandleA2Q9] We do not have the necessary quest item (Maligaro's Spike) in the inventory yet. Skipping execution until a restart or area change.");
				_skip = true;
				return false;
			}

			qi = LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(i => i.FullName == "Baleful Gem");
			if (qi == null)
			{
				Log.InfoFormat("[HandleA2Q9] We do not have the necessary quest item (Baleful Gem) in the inventory yet. Skipping execution until a restart or area change.");
				_skip = true;
				return false;
			}

			if (LokiPoe.MyPosition.Distance(_rootsLocation) > 20)
			{
				if (!PlayerMover.MoveTowards(_rootsLocation))
				{
					Log.ErrorFormat("[HandleA2Q9] MoveTowards failed for {0}.", _rootsLocation);
					await Coroutines.FinishCurrentAction();
				}
				return true;
			}

			var roots = LokiPoe.ObjectManager.TreeRoots;
			if (roots == null)
			{
				Log.ErrorFormat("[HandleA2Q9] TreeRoots == null. This should NOT happen.");
				return true;
			}

			var res = await Coroutines.InteractWith(roots);
			Log.InfoFormat("[HandleA2Q9] InteractWith returned {0}.", res);

			Reset();

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
			if (_skip)
				return;

			if (LokiPoe.CurrentWorldArea.Name != "The Wetlands")
			{
				Log.InfoFormat("[HandleA2Q9] The current area does not contain the quest. Skipping execution until a restart or area change.");
				_skip = true;
				return;
			}

			if (!_hasRootsLocation)
			{
				var obj = LokiPoe.ObjectManager.TreeRoots;
				if (obj != null)
				{
					_rootsLocation = ExilePather.FastWalkablePositionFor(obj);
					_hasRootsLocation = true;
					Log.InfoFormat("[HandleA2Q9] The Tree Roots has been found around {0}.", _rootsLocation);
				}
			}
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[HandleA2Q9] Now resetting task state.");
			_skip = false;
			_hasRootsLocation = false;
			_rootsLocation = Vector2i.Zero;
		}
	}
}