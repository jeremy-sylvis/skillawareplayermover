﻿using System;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Loki;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Common;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary>
	/// This task will return the bot to the previous grind zone after a town run.
	/// </summary>
	public class ReturnToGrindZoneTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private int _portalTries;

		private readonly GetSettingsDelegate<bool> _getCameFromPortal;
		private readonly SetSettingsDelegate<bool> _setCameFromPortal;

		/// <summary>
		/// Ctor that sets the delegate for obtaining current settings.
		/// </summary>
		/// <param name="getCameFromPortal"></param>
		/// <param name="setCameFromPortal"></param>
		public ReturnToGrindZoneTask(GetSettingsDelegate<bool> getCameFromPortal,
			SetSettingsDelegate<bool> setCameFromPortal)
		{
			_getCameFromPortal = getCameFromPortal;
			_setCameFromPortal = setCameFromPortal;
		}

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "ReturnToGrindZoneTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task will return the bot to the previous grind zone after a town run.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();
				return true;
			}

			if (type != "task_execute")
				return false;

			if (!LokiPoe.Me.IsInTown)
				return false;

			if (!_getCameFromPortal())
				return false;

			// Close blocking windows, so the world panel gets reset.
			await Coroutines.CloseBlockingWindows();

			++_portalTries;

			if (_portalTries > 3)
			{
				Log.ErrorFormat(
					"[ReturnToGrindZoneTask] The bot has failed 3 times to take a portal back. Now clearing ReturnToGrindAreaViaPortal to start a new run.");
				_setCameFromPortal(false);
				return true;
			}

			Vector2i pos;
			var portal = LokiPoe.ObjectManager.InTownPortals.FirstOrDefault();

			// If we don't know where the waypoint location is yet, try to find it.
			if (portal == null)
			{
				Log.DebugFormat(
					"[ReturnToGrindZoneTask] A portal is not in range. Now moving to a location where it should come into view.");

				pos = Utility.GuessPortalLocation();
			}
			else
			{
				pos = ExilePather.FastWalkablePositionFor(portal);
				Log.DebugFormat(
					"[ReturnToGrindZoneTask] The walkable position for the portal is: {0}", pos);
			}

			if (
				!await
					CoroutinesV3.MoveToLocation(pos, LokiPoe.Random.Next(10, 15), 60000,
						() => LokiPoe.ObjectManager.InTownPortals.FirstOrDefault() != null))
			{
				Log.ErrorFormat(
					"[ReturnToGrindZoneTask] MoveToLocation failed. Now clearing ReturnToGrindAreaViaPortal to start a new run.");
				_setCameFromPortal(false);
				return true;
			}

			portal = LokiPoe.ObjectManager.InTownPortals.FirstOrDefault();
			if (portal != null)
			{
				if (
					!await
						CoroutinesV3.MoveToLocation(ExilePather.FastWalkablePositionFor(portal.Position), LokiPoe.Random.Next(10, 15), 60000, () => false))
				{
					Log.ErrorFormat(
						"[ReturnToGrindZoneTask] MoveToLocation failed. Now clearing ReturnToGrindAreaViaPortal to start a new run.");
					_setCameFromPortal(false);
					return true;
				}

				portal = LokiPoe.ObjectManager.InTownPortals.FirstOrDefault();

				var hash = LokiPoe.LocalData.AreaHash;

				Log.DebugFormat("[ReturnToGrindZoneTask] The portal to interact with to is {0} at {1}.",
					portal.Id, portal.Position);

				if (await Coroutines.InteractWith(portal))
				{
					if (await CoroutinesV3.WaitForAreaChange(hash))
					{
						Log.DebugFormat(
							"[ReturnToGrindZoneTask] The portal has been taken. Now clearing ReturnToGrindAreaViaPortal.");
						_setCameFromPortal(false);
					}
				}
			}

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[ReturnToGrindZoneTask] Now resetting task state.");
			_portalTries = 0;
		}
	}
}