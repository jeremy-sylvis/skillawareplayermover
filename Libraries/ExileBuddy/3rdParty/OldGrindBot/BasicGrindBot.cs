﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Buddy.Coroutines;
using log4net;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.NativeWrappers;
using Loki.Game.Objects;
using Loki.Common;
using System.Windows.Markup;
using System.IO;
using System;
using System.Threading.Tasks;
using Loki.Bot;
using UserControl = System.Windows.Controls.UserControl;

namespace OldGrindBot
{
	/// <summary>
	/// A basic grind bot.
	/// </summary>
	public class OldGrindBot : IBot
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private UserControl _userControl;

		private Coroutine _coroutine;
		private int _botRestarts;
		private DateTime _lastBotRestart = DateTime.Now;

		private readonly TaskManager _taskManager = new TaskManager();

		private int _dndTries = 0;

		#region Implementation of IAuthored

		/// <summary> The name of this bot. </summary>
		public string Name
		{
			get
			{
				return "OldGrindBot";
			}
		}

		/// <summary> The description of this bot. </summary>
		public string Description
		{
			get
			{
				return "The old basic grind bot implemenation provided for Exilebuddy.";
			}
		}

		/// <summary>The author of this bot.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		#endregion

		#region Implementation of IBase

		/// <summary>Initializes this object. This is called when the object is loaded into the bot.</summary>
		public void Initialize()
		{
			BotManager.OnBotChanged += BotManagerOnOnBotChanged;
		}

		/// <summary> </summary>
		public void Deinitialize()
		{
		}

		private void BotManagerOnOnBotChanged(object sender, BotChangedEventArgs botChangedEventArgs)
		{
			if (botChangedEventArgs.New == this)
			{
				Log.DebugFormat("[OldGrindBot] This class is the new bot. Now setting up bot specific stuff.");
				ItemEvaluator.Instance = DefaultItemEvaluator.Instance;
				Explorer.Delegate = (user) => AreaStateCache.Current.Explorer;
			}
		}

		#endregion

		#region Implementation of IRunnable

		/// <summary> The bot start callback. Do any initialization here. </summary>
		public void Start()
		{
			Explorer.Delegate = (user) => AreaStateCache.Current.Explorer;

			Log.DebugFormat("[OldGrindBot] Start");

			BotManager.OnBotTickException += BotManagerOnOnBotTickException;

			// Reset the item evaluator to avoid issues with people using a plugin.
			ItemEvaluator.Instance = DefaultItemEvaluator.Instance;

			// Cache all bound keys.
			LokiPoe.Input.Binding.Update();

			// Reset the default MsBetweenTicks on start.
			Log.DebugFormat("[Start] MsBetweenTicks: {0}.", BotManager.MsBetweenTicks);

			var nm = LokiPoe.ConfigManager.NetworkingMode;
			Log.DebugFormat("[Start] NetworkingMode: {0}.", nm);
			Log.DebugFormat("[Start] EngineMultithreadingMode: {0}.", LokiPoe.ConfigManager.EngineMultithreadingMode);
			Log.DebugFormat("[Start] Language: {0}.", LokiPoe.ConfigManager.Language);

			Log.DebugFormat("[Start] KeyPickup: {0}.", LokiPoe.ConfigManager.KeyPickup);
			Log.DebugFormat("[Start] IsAutoEquipEnabled: {0}.", LokiPoe.ConfigManager.IsAutoEquipEnabled);

			Log.DebugFormat("[Start] PlayerMover.Instance: {0}.", PlayerMover.Instance.GetType().ToString());

			Log.DebugFormat("[Start] MaxInstances: {0}.", OldGrindBotSettings.Instance.MaxInstances);

			Log.DebugFormat("[Start] LootVisibleItemsOverride: {0}.", AreaStateCache.LootVisibleItemsOverride);

			OldGrindBotSettings.Instance.NeedsTownRun = 0;
			OldGrindBotSettings.Instance.CameFromPortal = false;

			// Since this bot will be performing client actions, we need to enable the process hook manager.
			LokiPoe.ProcessHookManager.Enable();

			_coroutine = null;

			ExilePather.Reload();

			_taskManager.Reset();

			_taskManager.Add(new ResurrectTask());

			_taskManager.Add(new ClearCursorTask());

			_taskManager.Add(new AssignMoveSkillTask());

			_taskManager.Add(new HandleBlockingChestsTask());
			_taskManager.Add(new HandleDoorsTask());

			// Handle stuff close by to us first.
			_taskManager.Add(new CombatTask(50, false));

			_taskManager.Add(new PostCombatHookTask());

			_taskManager.Add(new TownRunTask(
				v => OldGrindBotSettings.Instance.CameFromPortal = v,
				() => OldGrindBotSettings.Instance.NeedsTownRun,
				v => OldGrindBotSettings.Instance.NeedsTownRun = v)
				);

			_taskManager.Add(new LootItemsTask(
				50,
				v => OldGrindBotSettings.Instance.NeedsTownRun = v,
				v => OldGrindBotSettings.Instance.NeedsToStash = v,
				TriggerOnLoot));
			_taskManager.Add(new OpenChestTask(50, false));

			// Always loot items before leaving the area.
			_taskManager.Add(new LootItemsTask(
				-1,
				v => OldGrindBotSettings.Instance.NeedsTownRun = v,
				v => OldGrindBotSettings.Instance.NeedsToStash = v,
				TriggerOnLoot));

			_taskManager.Add(new IdTask(NeedsToId, IdingComplete, ShouldIdItem));

			_taskManager.Add(new SellTask(NeedsToSell, SellingComplete, ShouldAcceptSellOffer, ShouldSellItem, null,
				TriggerOnSell));

			_taskManager.Add(new MergeInventoryTask());

			_taskManager.Add(new StashTask(NeedsToStash, StashingComplete, ShouldStashItem, BestStashTabForItem, null,
				TriggerOnStash));

			_taskManager.Add(new WithdrawTask(TriggerOnWithdraw));

			_taskManager.Add(new HandleInventoryItemsTask());

			_taskManager.Add(new SortInventoryTask());

			_taskManager.Add(new ReturnToGrindZoneTask(
				() => OldGrindBotSettings.Instance.CameFromPortal,
				v => OldGrindBotSettings.Instance.CameFromPortal = v
				));

			_taskManager.Add(new TagWaypointTask());

			_taskManager.Add(new UseLooseCandleTask());
			_taskManager.Add(new UnblockCorruptedAreaTransitionTask());

			_taskManager.Add(new TakeCorruptedAreaTask());

			// This goes here since progress is blocked until it's done.
			_taskManager.Add(new HandleA2Q9());
			_taskManager.Add(new HandleA2Q9b());
			_taskManager.Add(new HandleA2Q11());
			_taskManager.Add(new HandleA3Q1());

			_taskManager.Add(new HandleSewers());
			_taskManager.Add(new HandleUndyingBlockage());
			_taskManager.Add(new HandleLockedDoor());
			_taskManager.Add(new HandleDeshretSeal());
			_taskManager.Add(new HandleGlyphWall());
			_taskManager.Add(new HandleDeshretSpirit());
			_taskManager.Add(new HandleAllFlame());

			//_taskManager.Add(new StayWithCadiroTask()); // TODO: Temp

			_taskManager.Add(new TravelToGrindZoneTask());
			_taskManager.Add(new TravelThroughBossAreasTask(true));

			// Now handle everything else in order.
			_taskManager.Add(new OpenChestTask(-1, true));
			_taskManager.Add(new CombatTask(-1, true));

			_taskManager.Add(new EnterArenaTask());

			_taskManager.Add(new TravelThroughBossAreasTask(false));

			_taskManager.Add(new ExplorationCompleteTask(true));

			// Lastly, explore the rest of the map.
			_taskManager.Add(new ExploreTask());

			_taskManager.Add(new TrackMoreMobs());

			// Help the bot stay in various boss areas.
			_taskManager.Add(new HandleMerveilArea());
			_taskManager.Add(new HandleVaalArea());
			_taskManager.Add(new HandlePietyArea());
			_taskManager.Add(new HandleDominusArea());
			_taskManager.Add(new HandleKaomArea());
			_taskManager.Add(new HandleDaressoArea());
			_taskManager.Add(new HandleWeaverArea());
			_taskManager.Add(new HandleVaalVessel());

			// Once we are done, find an area transition to leave though.
			//_taskManager.Add(new LeaveCurrentBossAreaTask());
			//_taskManager.Add(new LeaveCurrentAreaTask());

			_taskManager.Add(new ExplorationCompleteTask(false));

			_taskManager.Add(new FallbackTask());

			//_taskManager.Freeze();

			PluginManager.Start();
			RoutineManager.Start();

			AreaStateCache.Start();

			_taskManager.Start();

			foreach (var plugin in PluginManager.EnabledPlugins)
			{
				Log.DebugFormat("[Start] The plugin {0} is enabled.", plugin.Name);
			}

			Log.DebugFormat("[Start] PlayerMover.Instance: {0}.", PlayerMover.Instance.GetType().ToString());

			//OnStash += OnOnStash;

			Log.DebugFormat("[Start] ReactionMinSleepDelay: {0}.", Coroutines.ReactionMinSleepDelay);
			Log.DebugFormat("[Start] ReactionMaxSleepDelay: {0}.", Coroutines.ReactionMaxSleepDelay);

			PlayerMover.Execute("SetNetworkingMode", nm);

			if (ExilePather.BlockTrialOfAscendancy == FeatureEnum.Unset)
			{
				ExilePather.BlockTrialOfAscendancy = FeatureEnum.Enabled;
			}
		}

		private void BotManagerOnOnBotTickException(object sender, BotTickExceptionEventArgs botTickExceptionEventArgs)
		{
			if (OldGrindBotSettings.Instance.StopAfterTickExceptions > 0 &&
				BotManager.ExceptionCount >= OldGrindBotSettings.Instance.StopAfterTickExceptions)
			{
				Log.InfoFormat(
					"[BotManagerOnOnBotTickException] Now stopping the bot because too many exceptions ({0} / {1}) have occurred in a minute.",
					BotManager.ExceptionCount, OldGrindBotSettings.Instance.StopAfterTickExceptions);
				BotManager.Stop();
				if (OldGrindBotSettings.Instance.LogoutAfterTickExceptionThreshold)
				{
					// We won't be inside the framelock in this handler, so enter it, make sure we are in game, and logout.
					using (LokiPoe.AcquireFrame())
					{
						if (LokiPoe.IsInGame)
						{
							var res = LokiPoe.EscapeState.LogoutToTitleScreen();
							Log.DebugFormat("[BotManagerOnOnBotTickException] LogoutToTitleScreen returned {0}.", res);
						}
					}
				}
			}
		}

		/*private void OnOnStash(object sender, OnStashEventArgs onStashEventArgs)
		{
			Log.InfoFormat("[OnOnStash] {1}x {0}.", onStashEventArgs.FullName, onStashEventArgs.Count);
		}*/

		/// <summary> The bot tick callback. Do any update logic here. </summary>
		public void Tick()
		{
			if (_coroutine == null)
			{
				_coroutine = new Coroutine(() => MainCoroutine());
			}

			ExilePather.Reload();

			StaticLocationManager.Tick();

			AreaStateCache.Tick();
			_taskManager.Tick();

			PluginManager.Tick();
			RoutineManager.Tick();

			// Check to see if the coroutine is finished. If it is, stop the bot.
			if (_coroutine.IsFinished)
			{
				Log.DebugFormat("[Tick] The bot coroutine has finished in a state of {0}", _coroutine.Status);
				BotManager.Stop();

				// If an exception happend, try to restart the bot.
				/*if (_coroutine.Status == CoroutineStatus.Faulted)
				{
					if ((DateTime.Now - _lastBotRestart).TotalMilliseconds > 15000)
					{
						_botRestarts = 0;
					}

					++_botRestarts;
					_lastBotRestart = DateTime.Now;

					if (_botRestarts < 15)
					{
						Log.DebugFormat("[Tick] Attempting to start the bot again. This is bot restart # {0}.",
							_botRestarts);

						ThreadPool.QueueUserWorkItem(o =>
						{
							while (!BotManager.Start())
							{
								Thread.Sleep(500);
							}
						});
					}
				}*/

				return;
			}

			// We need to be in game, but not when the escape menu is opened.
			if (LokiPoe.IsInGame && !LokiPoe.StateManager.IsEscapeStateActive)
			{
				// If the user wants Auto-DnD
				if (OldGrindBotSettings.Instance.AutoDoNotDisturb)
				{
					// If DnD is not currently enabled in the client.
					if (!LokiPoe.InGameState.IsDoNotDisturbedEnabled &&
						!LokiPoe.InGameState.GlobalWarningDialog.IsPassiveWarningOverlayOpen)
					{
						// Make sure to prevent chat spamming issues.
						if (_dndTries > 3)
						{
							Log.ErrorFormat(
								"[Tick] The bot has tried 3 times to enable DnD and failed. Now stopping the bot. Please report this issue.");
							BotManager.Stop();
							return;
						}

						Log.InfoFormat(
							"[Tick] DnD is not enabled and should be. Now attempting to enable it. The current Tick will be skipped for coroutines.");

						var result = LokiPoe.InGameState.ChatPanel.Commands.dnd();
						Log.InfoFormat("[Tick] dnd returned {0}.", result);

						++_dndTries;
						BotManager.MsBeforeNextTick = 500;
						return;
					}
					_dndTries = 0;
				}
				else
				{
					_dndTries = 0;
				}
			}
			else
			{
				_dndTries = 0;
			}

			try
			{
				_coroutine.Resume();
			}
			catch
			{
				var c = _coroutine;
				_coroutine = null;
				try
				{
					c.Dispose();
				}
				catch
				{
				}

				// It is important we throw exceptions to Tick so proper exception tracking can be done!
				throw;
			}
		}

		/// <summary> The bot stop callback. Do any pre-dispose cleanup here. </summary>
		public void Stop()
		{
			Log.DebugFormat("[OldGrindBot] OnStop");

		    var reason = BotManager.StopReason;
		    if (reason != null)
		    {
		        Log.DebugFormat("\t{0} => {1}.", reason.Id, reason.Reason);
		    }

			BotManager.OnBotTickException -= BotManagerOnOnBotTickException;

			// We have to be in-game to trigger dnd.
			if (LokiPoe.IsInGame && !LokiPoe.StateManager.IsEscapeStateActive)
			{
				// Only disable it if the setting is enabled.
				if (OldGrindBotSettings.Instance.DisableDoNotDisturbOnBotStop)
				{
					// If DnD is not currently enabled in the client, and the passive warning overlay is not opened, type dnd again.
					if (LokiPoe.InGameState.IsDoNotDisturbedEnabled &&
						!LokiPoe.InGameState.GlobalWarningDialog.IsPassiveWarningOverlayOpen)
					{
						var result = LokiPoe.InGameState.ChatPanel.Commands.dnd();
						Log.InfoFormat("[Stop] dnd returned {0}.", result);
					}
				}
			}

			AreaStateCache.Stop();
			_taskManager.Stop();

			PluginManager.Stop();
			RoutineManager.Stop();

			// When the bot is stopped, we want to remove the process hook manager.
			LokiPoe.ProcessHookManager.Disable();

			// Cleanup the coroutine.
			if (_coroutine != null)
			{
				try
				{
					_coroutine.Dispose();
				}
				catch
				{
				}
				
				_coroutine = null;
			}

			//OnStash -= OnOnStash;

			// Good idea to reset this.
			_dndTries = 0;
		}

		#endregion

		#region Implementation of ILogic

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var cwa = LokiPoe.CurrentWorldArea;
				if (cwa.Name.Equals("The City of Sarn") || cwa.Name.Equals("The Slums"))
				{
					PlayerMover.Execute("SetDoAdjustments", true);
				}
				else
				{
					PlayerMover.Execute("SetDoAdjustments", false);
				}
			}

			return await _taskManager.Logic(type, param);
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			if (name == "GetTaskManager")
			{
				return _taskManager;
			}
			if (name == "oldgrindbot_get_current_area_state_cache")
			{
				return AreaStateCache.Current;
			}
			if (name == "oldgrindbot_change_grind_zone")
			{
				OldGrindBotSettings.Instance.GrindZoneName = (string) param[0];
				OldGrindBotSettings.Instance.GrindZoneDifficulty = (string) param[1];
				Log.InfoFormat("[GrindZoneChanger] New grind zone being set to {0}:{1}.", OldGrindBotSettings.Instance.GrindZoneName,
					OldGrindBotSettings.Instance.GrindZoneDifficulty);
				return null;
			}
			if (name == "oldgrindbot_get_time_in_instance")
			{
				return new Nullable<TimeSpan>(AreaStateCache.Current.TimeInInstance);
			}
			if (name == "oldgrindbot_get_time_in_area")
			{
				return new Nullable<TimeSpan>(AreaStateCache.Current.TimeInArea);
			}
			if (name == "oldgrindbot_set_new_instance_override")
			{
				// If we aren't in a corrupted area, just blacklist the id. If we are, we need to blacklist the parent's id.
				if (!LokiPoe.CurrentWorldArea.IsCorruptedArea)
				{
					AreaStateCache.SetNewInstanceOverride(LokiPoe.CurrentWorldArea.Id, true);
				}
				else
				{
					if (!string.IsNullOrEmpty(AreaStateCache.CorruptedAreaParentId))
						AreaStateCache.SetNewInstanceOverride(AreaStateCache.CorruptedAreaParentId, true);
				}
				return null;
			}
			if (name == "oldgrindbot_sell_complete")
			{
				bool failed = param[0];

				Log.InfoFormat("[Execute][{0}] {1}.", name, failed);
				return null;
			}
			if (name == "oldgrindbot_on_stash")
			{
				string Name = param[0];
				string FullName = param[1];
				int Count = param[2];
				string Metadata = param[3];
				int ItemLevel = param[4];
				Rarity Rarity = param[5];

				Log.InfoFormat("[Execute][{0}] {1}x {2}.", name, Count, FullName);
				return null;
			}
			if (name == "oldgrindbot_on_withdraw")
			{
				string Name = param[0];
				string FullName = param[1];
				int Count = param[2];
				string Metadata = param[3];
				int ItemLevel = param[4];
				Rarity Rarity = param[5];

				Log.InfoFormat("[Execute][{0}] {1}x {2}.", name, Count, FullName);
				return null;
			}
			if (name == "oldgrindbot_on_sell")
			{
				string Name = param[0];
				string FullName = param[1];
				int Count = param[2];
				string Metadata = param[3];
				int ItemLevel = param[4];
				Rarity Rarity = param[5];

				Log.InfoFormat("[Execute][{0}] {1}x {2}.", name, Count, FullName);
				return null;
			}
			if (name == "oldgrindbot_on_loot")
			{
				string Name = param[0];
				string Type = param[1];
				int ItemLevel = param[2];
				Rarity Rarity = param[3];
				int Id = param[4];

				Log.InfoFormat("[Execute][{0}] {1} {2} {3} {4} {5}.", name, Name, Type, ItemLevel, Rarity, Id);
				return null;
			}
			return null;
		}

		#endregion

		#region Implementation of IConfigurable

		/// <summary>The settings object. This will be registered in the current configuration.</summary>
		public JsonSettings Settings
		{
			get
			{
				return OldGrindBotSettings.Instance;
			}
		}

		/// <summary> The bot's settings control. This will be added to the Exilebuddy Settings tab.</summary>
		public UserControl Control
		{
			get
			{
				if (_userControl != null)
				{
					return _userControl;
				}

				using (
					var fs = new FileStream(Path.Combine(ThirdPartyLoader.GetInstance("OldGrindBot").ContentPath, "SettingsGui.xaml"),
						FileMode.Open))
				{
					var root = (UserControl) XamlReader.Load(fs);

					// Your settings binding here.

					if (
						!Wpf.SetupCheckBoxBinding(root, "LootVisibleItemsOverrideCheckBox",
							"LootVisibleItemsOverride",
							BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'LootVisibleItemsOverrideCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupCheckBoxBinding(root, "AutoDoNotDisturbCheckBox",
							"AutoDoNotDisturb",
							BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'AutoDoNotDisturbCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupCheckBoxBinding(root, "DisableDoNotDisturbOnBotStopCheckBox", "DisableDoNotDisturbOnBotStop",
							BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'DisableDoNotDisturbOnBotStopCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxItemsBinding(root, "GrindZoneNameComboBox", "AllGrindZoneNames",
							BindingMode.OneWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxItemsBinding failed for 'GrindZoneNameComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxSelectedItemBinding(root, "GrindZoneNameComboBox",
							"GrindZoneName", BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxSelectedItemBinding failed for 'GrindZoneNameComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxItemsBinding(root, "GrindZoneDifficultyComboBox", "AllGrindZoneDifficulty",
							BindingMode.OneWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxItemsBinding failed for 'GrindZoneDifficultyComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxSelectedItemBinding(root, "GrindZoneDifficultyComboBox",
							"GrindZoneDifficulty", BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxSelectedItemBinding failed for 'GrindZoneDifficultyComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "DeathsBeforeNewInstanceTextBox", "DeathsBeforeNewInstance",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'DeathsBeforeNewInstanceTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupListBoxItemsBinding(root, "GlobalChestsToIgnoreListBox", "GlobalChestsToIgnore",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupListBoxBinding failed for 'GlobalChestsToIgnoreListBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupCheckBoxBinding(root, "EnableStashingOnFreeSpacePercentCheckBox",
							"EnableStashingOnFreeSpacePercent",
							BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'EnableStashingOnFreeSpacePercentCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupTextBoxBinding(root, "FreeSpaceToTriggerStashingPercentTextBox",
							"FreeSpaceToTriggerStashingPercent",
							BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'FreeSpaceToTriggerStashingPercentTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (
						!Wpf.SetupCheckBoxBinding(root, "EnableEarlyExplorationCompleteCheckBox",
							"EnableEarlyExplorationComplete",
							BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'EnableEarlyExplorationCompleteCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupTextBoxBinding(root, "ExplorationCompletePercentTextBox",
							"ExplorationCompletePercent",
							BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'ExplorationCompletePercentTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (!Wpf.SetupCheckBoxBinding(root, "DontStashAlchemyShardsCheckBox", "DontStashAlchemyShards",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'DontStashAlchemyShardsCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "AlchemyShardsCellXTextBox", "AlchemyShardsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iXConverter(() => OldGrindBotSettings.Instance.AlchemyShardsCell)))
					{
						Log.DebugFormat("[SettingsControl] SetupTextBoxBinding failed for 'AlchemyShardsCellXTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "AlchemyShardsCellYTextBox", "AlchemyShardsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iYConverter(() => OldGrindBotSettings.Instance.AlchemyShardsCell)))
					{
						Log.DebugFormat("[SettingsControl] SetupTextBoxBinding failed for 'AlchemyShardsCellYTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (
						!Wpf.SetupCheckBoxBinding(root, "DontStashTransmutationShardsCheckBox",
							"DontStashTransmutationShards",
							BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'DontStashTransmutationShardsCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "TransmutationShardsCellXTextBox", "TransmutationShardsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iXConverter(() => OldGrindBotSettings.Instance.TransmutationShardsCell)))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'TransmutationShardsCellXTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "TransmutationShardsCellYTextBox", "TransmutationShardsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iYConverter(() => OldGrindBotSettings.Instance.TransmutationShardsCell)))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'TransmutationShardsCellYTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (!Wpf.SetupCheckBoxBinding(root, "DontStashScrollFragmentsCheckBox", "DontStashScrollFragments",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'DontStashScrollFragmentsCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "ScrollFragmentsCellXTextBox", "ScrollFragmentsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iXConverter(() => OldGrindBotSettings.Instance.ScrollFragmentsCell)))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'ScrollFragmentsCellXTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "ScrollFragmentsCellYTextBox", "ScrollFragmentsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iYConverter(() => OldGrindBotSettings.Instance.ScrollFragmentsCell)))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'ScrollFragmentsCellYTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (
						!Wpf.SetupCheckBoxBinding(root, "DontStashAlterationShardsCheckBox", "DontStashAlterationShards",
							BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'DontStashAlterationShardsCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "AlterationShardsCellXTextBox", "AlterationShardsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iXConverter(() => OldGrindBotSettings.Instance.AlterationShardsCell)))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'AlterationShardsCellXTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "AlterationShardsCellYTextBox", "AlterationShardsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iYConverter(() => OldGrindBotSettings.Instance.AlterationShardsCell)))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'AlterationShardsCellYTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (
						!Wpf.SetupCheckBoxBinding(root, "DontStashSilverCoinsCheckBox", "DontStashSilverCoins",
							BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'DontStashSilverCoinsCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "SilverCoinsCellXTextBox", "SilverCoinsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iXConverter(() => OldGrindBotSettings.Instance.SilverCoinsCell)))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'SilverCoinsCellXTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "SilverCoinsCellYTextBox", "SilverCoinsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iYConverter(() => OldGrindBotSettings.Instance.SilverCoinsCell)))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'SilverCoinsCellYTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (
						!Wpf.SetupCheckBoxBinding(root, "DontStashPerandusCoinsCheckBox", "DontStashPerandusCoins",
							BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'DontStashPerandusCoinsCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "PerandusCoinsCellXTextBox", "PerandusCoinsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iXConverter(() => OldGrindBotSettings.Instance.PerandusCoinsCell)))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'PerandusCoinsCellXTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "PerandusCoinsCellYTextBox", "PerandusCoinsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iYConverter(() => OldGrindBotSettings.Instance.PerandusCoinsCell)))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'PerandusCoinsCellYTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (
						!Wpf.SetupTextBoxBinding(root, "MaxInstancesTextBox",
							"MaxInstances",
							BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'MaxInstancesTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (
						!Wpf.SetupTextBoxBinding(root, "MaxWithdrawTabsTextBox",
							"MaxWithdrawTabs",
							BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'MaxWithdrawTabsTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (!Wpf.SetupCheckBoxBinding(root, "FillAlchemyStacksCheckBox", "FillAlchemyStacks",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'FillAlchemyStacksCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (!Wpf.SetupCheckBoxBinding(root, "FillChaosStacksCheckBox", "FillChaosStacks",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'FillChaosStacksCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (!Wpf.SetupCheckBoxBinding(root, "FillChanceStacksCheckBox", "FillChanceStacks",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'FillChanceStacksCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (!Wpf.SetupCheckBoxBinding(root, "FillAlterationStacksCheckBox", "FillAlterationStacks",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'FillAlterationStacksCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (!Wpf.SetupCheckBoxBinding(root, "FillAugmentationStacksCheckBox", "FillAugmentationStacks",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'FillAugmentationStacksCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (!Wpf.SetupCheckBoxBinding(root, "FillTransmutationStacksCheckBox", "FillTransmutationStacks",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'FillTransmutationStacksCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (!Wpf.SetupCheckBoxBinding(root, "FillScourStacksCheckBox", "FillScourStacks",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'FillScourStacksCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (!Wpf.SetupCheckBoxBinding(root, "FillIdScrollStacksCheckBox", "FillIdScrollStacks",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'FillIdScrollStacksCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupCheckBoxBinding(root, "DontStashIdScrollsCheckBox", "DontStashIdScrolls",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'DontStashIdScrollsCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "IdScrollsCellXTextBox", "IdScrollsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iXConverter(() => OldGrindBotSettings.Instance.IdScrollsCell)))
					{
						Log.DebugFormat("[SettingsControl] SetupTextBoxBinding failed for 'IdScrollsCellXTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "IdScrollsCellYTextBox", "IdScrollsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iYConverter(() => OldGrindBotSettings.Instance.IdScrollsCell)))
					{
						Log.DebugFormat("[SettingsControl] SetupTextBoxBinding failed for 'IdScrollsCellYTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (!Wpf.SetupCheckBoxBinding(root, "FillTpScrollStacksCheckBox", "FillTpScrollStacks",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'FillTpScrollStacksCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupCheckBoxBinding(root, "DontStashTpScrollsCheckBox", "DontStashTpScrolls",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'DontStashTpScrollsCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "TpScrollsCellXTextBox", "TpScrollsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iXConverter(() => OldGrindBotSettings.Instance.TpScrollsCell)))
					{
						Log.DebugFormat("[SettingsControl] SetupTextBoxBinding failed for 'TpScrollsCellXTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "TpScrollsCellYTextBox", "TpScrollsCell",
						BindingMode.TwoWay, OldGrindBotSettings.Instance,
						new WpfVector2iYConverter(() => OldGrindBotSettings.Instance.TpScrollsCell)))
					{
						Log.DebugFormat("[SettingsControl] SetupTextBoxBinding failed for 'TpScrollsCellYTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (
						!Wpf.SetupComboBoxItemsBinding(root, "ExplorationCompleteBehavior1ComboBox",
							"AllExplorationCompleteBehaviors",
							BindingMode.OneWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxItemsBinding failed for 'ExplorationCompleteBehavior1ComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxSelectedItemBinding(root, "ExplorationCompleteBehavior1ComboBox",
							"ExplorationCompleteBehavior1", BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxSelectedItemBinding failed for 'ExplorationCompleteBehavior1ComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxItemsBinding(root, "ExplorationCompleteBehavior2ComboBox",
							"AllExplorationCompleteBehaviors",
							BindingMode.OneWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxItemsBinding failed for 'ExplorationCompleteBehavior2ComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxSelectedItemBinding(root, "ExplorationCompleteBehavior2ComboBox",
							"ExplorationCompleteBehavior2", BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxSelectedItemBinding failed for 'ExplorationCompleteBehavior2ComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxItemsBinding(root, "ExplorationCompleteBehavior3ComboBox",
							"AllExplorationCompleteBehaviors",
							BindingMode.OneWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxItemsBinding failed for 'ExplorationCompleteBehavior3ComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxSelectedItemBinding(root, "ExplorationCompleteBehavior3ComboBox",
							"ExplorationCompleteBehavior3", BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxSelectedItemBinding failed for 'ExplorationCompleteBehavior3ComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxItemsBinding(root, "ExplorationCompleteBehavior4ComboBox",
							"AllExplorationCompleteBehaviors",
							BindingMode.OneWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxItemsBinding failed for 'ExplorationCompleteBehavior4ComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxSelectedItemBinding(root, "ExplorationCompleteBehavior4ComboBox",
							"ExplorationCompleteBehavior4", BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxSelectedItemBinding failed for 'ExplorationCompleteBehavior4ComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxItemsBinding(root, "ExplorationCompleteBehavior5ComboBox",
							"AllExplorationCompleteBehaviors",
							BindingMode.OneWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxItemsBinding failed for 'ExplorationCompleteBehavior5ComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxSelectedItemBinding(root, "ExplorationCompleteBehavior5ComboBox",
							"ExplorationCompleteBehavior5", BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxSelectedItemBinding failed for 'ExplorationCompleteBehavior5ComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					if (
						!Wpf.SetupCheckBoxBinding(root, "NewRunAfterCorruptedAreaExploredCheckBox",
							"NewRunAfterCorruptedAreaExplored",
							BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'NewRunAfterCorruptedAreaExploredCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupCheckBoxBinding(root, "TakeCorruptedAreasCheckBox", "TakeCorruptedAreas",
						BindingMode.TwoWay, OldGrindBotSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'TakeCorruptedAreasCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					//-----

					// Your settings event handlers here.

					var randomizeLocationsButton = Wpf.FindControlByName<Button>(root, "RandomizeLocationsButton");
					randomizeLocationsButton.Click += RandomizeLocationsButtonOnClick;

					_userControl = root;
				}

				return _userControl;
			}
		}

		internal void RandomizeLocationsButtonOnClick(object sender, RoutedEventArgs routedEventArgs)
		{
			OldGrindBotSettings.Instance.RandomizeItemLocations();
		}

		#endregion

		#region Override of Object

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			return Name + ": " + Description;
		}

		#endregion

		#region Coroutine Logic

		private async Task MainCoroutine()
		{
			while (true)
			{
				if (LokiPoe.IsInLoginScreen)
				{
					// Offload auto login logic to a plugin.
					foreach (var plugin in PluginManager.EnabledPlugins)
					{
						if (await plugin.Logic("login_screen_hook"))
							break;
					}
				}
				else if (LokiPoe.IsInCharacterSelectionScreen)
				{
					// Offload character selection logic to a plugin.
					foreach (var plugin in PluginManager.EnabledPlugins)
					{
						if (await plugin.Logic("character_selection_hook"))
							break;
					}
				}
				else if (LokiPoe.IsInGame)
				{
					foreach (var plugin in PluginManager.EnabledPlugins)
					{
						await plugin.Logic("plugin_coroutine_event");
					}

					// What the bot does now is up to the registered tasks.
					await _taskManager.Logic("task_execute");
				}
				else
				{
					// Most likely in a loading screen, which will cause us to block on the executor, 
					// but just in case we hit something else that would cause us to execute...
					await Coroutine.Sleep(1000);
					continue;
				}

				// End of the tick.
				await Coroutine.Yield();
			}
			// ReSharper disable once FunctionNeverReturns
		}

		#endregion

		private bool NeedsToId()
		{
			if (
				!LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).Any(
					i => i.Rarity != Rarity.Quest && !i.IsLabyrinthType && !i.IsIdentified && ShouldIdItem(i, null)))
			{
				return false;
			}

			return true;
		}

		private void IdingComplete()
		{
		}

		private bool ShouldSellItem(Item item, object user)
		{
			IItemFilter filter;
			if (!ItemEvaluator.Match(item, EvaluationType.Save, out filter))
			{
				if (ItemEvaluator.Match(item, EvaluationType.Sell, out filter))
				{
					return true;
				}
				else
				{

				}
			}
			else
			{

			}

			return false;
		}

		private bool NeedsToSell()
		{
			if (
				LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main)
					.Where(i => i.Rarity != Rarity.Quest && !i.IsLabyrinthType && !i.HasSkillGemsEquipped && !i.HasMicrotransitionAttachment)
					.Any(i => ShouldSellItem(i, null)))
			{
				return true;
			}

			return false;
		}

		private bool ShouldAcceptSellOffer()
		{
			Log.DebugFormat("[ShouldAcceptSellOffer] The npc is offering us:");
			foreach (var item in LokiPoe.InGameState.SellUi.TradeControl.InventoryControl_OtherOffer.Inventory.Items)
			{
				Log.DebugFormat("   {1}x {0}", item.FullName, item.StackCount);
			}

			return true;
		}

		private void SellingComplete(bool failed)
		{
		}

		private bool ShouldIdItem(Item item, object user)
		{
			IItemFilter filter;
			if (ItemEvaluator.Match(item, EvaluationType.Id, out filter))
			{
				return true;
			}

			return false;
		}

		private bool NeedsToStash()
		{
			if (!OldGrindBotSettings.Instance.NeedsToStash)
			{
				if (OldGrindBotSettings.Instance.EnableStashingOnFreeSpacePercent)
				{
					if (LokiPoe.InstanceInfo.GetPlayerInventoryBySlot(InventorySlot.Main).InventorySpacePercent >
						OldGrindBotSettings.Instance.FreeSpaceToTriggerStashingPercent)
					{
						return false;
					}
				}
			}

			if (
				!LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).Where(i => i.Rarity != Rarity.Quest && !i.IsLabyrinthType)
					.Any(i => ShouldStashItem(i, null)))
			{
				return false;
			}

			return true;
		}

		private void StashingComplete()
		{
			OldGrindBotSettings.Instance.NeedsToStash = false;
		}

		private bool BestStashTabForItem(StashTabInfo tab, Inventory inventory, Item item, object user)
		{
			// Don't stash to public tabs!
			if (tab.IsPublic)
			{
				return false;
			}

			/*if (tab.IsPremiumCurrency)
			{
				return false;
			}*/

			var name = item.FullName;

			// This tab holds junk currency
			/*if (tab.DisplayName == "Usable Currency")
            {
                if (name == "Armourer's Scrap" || 
                    name == "Orb of Augmentation" ||
                    name == "Orb of Chance" ||
                    name == "Orb of Transmutation" ||
                    name == "Blacksmith's Whetstone" ||
                    name == "Glassblower's Bauble")
                {
                    return true;
                }

                return false;
            }

            // This tab holds valuable currency.
            if (tab.DisplayName == "Tradeable Currency")
            {
                if (item.IsCurrencyType)
                {
                    // Exclude these currency items.
                    if (name == "Orb of Augmentation" ||
                        name == "Orb of Chance" ||
                        name == "Orb of Transmutation" ||
                        name == "Blacksmith's Whetstone" ||
                        name == "Glassblower's Bauble" ||
                        name == "Scroll of Wisdom" ||
                        name == "Portal Scroll" ||
                        name == "Orb of Alteration")
                    {
                        return false;
                    }

                    // Take everything else.
                    return true;
                }

                return false;
            }

            // This tab only holds alts.
            if (tab.DisplayName == "Alterations")
            {
                return name == "Orb of Alteration";
            }

            // This tab only holds scrolls.
            if (tab.DisplayName == "9" || tab.DisplayName == "Scrolls 1" || tab.DisplayName == "Scrolls 2")
            {
                return name == "Scroll of Wisdom" || name == "Portal Scroll";
            }*/

			return true;
		}

		private bool ShouldStashItem(Item item, object user)
		{
			var name = item.FullName;

			var items = new List<KeyValuePair<string, bool>>
			{
				new KeyValuePair<string, bool>("Scroll of Wisdom", OldGrindBotSettings.Instance.DontStashIdScrolls),
				new KeyValuePair<string, bool>("Portal Scroll", OldGrindBotSettings.Instance.DontStashTpScrolls),
				new KeyValuePair<string, bool>("Chaos Orb", OldGrindBotSettings.Instance.FillChaosStacks),
				new KeyValuePair<string, bool>("Orb of Alchemy", OldGrindBotSettings.Instance.FillAlchemyStacks),
				new KeyValuePair<string, bool>("Orb of Chance", OldGrindBotSettings.Instance.FillChanceStacks),
				new KeyValuePair<string, bool>("Orb of Transmutation", OldGrindBotSettings.Instance.FillTransmutationStacks),
				new KeyValuePair<string, bool>("Orb of Alteration", OldGrindBotSettings.Instance.FillAlterationStacks),
				new KeyValuePair<string, bool>("Orb of Scouring", OldGrindBotSettings.Instance.FillScourStacks),
				new KeyValuePair<string, bool>("Orb of Augmentation", OldGrindBotSettings.Instance.FillAugmentationStacks),
				new KeyValuePair<string, bool>("Vaal Orb", OldGrindBotSettings.Instance.FillVaalStacks),
				new KeyValuePair<string, bool>("Alchemy Shard", OldGrindBotSettings.Instance.DontStashAlchemyShards),
				new KeyValuePair<string, bool>("Transmutation Shard", OldGrindBotSettings.Instance.DontStashTransmutationShards),
				new KeyValuePair<string, bool>("Alteration Shard", OldGrindBotSettings.Instance.DontStashAlterationShards),
				new KeyValuePair<string, bool>("Scroll Fragment", OldGrindBotSettings.Instance.DontStashScrollFragments),
				new KeyValuePair<string, bool>("Perandus Coin", OldGrindBotSettings.Instance.DontStashPerandusCoins),
				new KeyValuePair<string, bool>("Silver Coin", OldGrindBotSettings.Instance.DontStashSilverCoins),
			};

			foreach (var kvp in items)
			{
				if (name == kvp.Key)
				{
					if (kvp.Value)
					{
						if (
							LokiPoe.InstanceInfo.GetPlayerInventoryBySlot(InventorySlot.Main).GetTotalItemStacksByFullName(name) ==
							1)
						{
							return false;
						}

						var bestItem =
							LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).Where(i => i.FullName == name)
								.OrderByDescending(i => i.StackCount)
								.ThenBy(i => i.BaseAddress.ToInt32())
								.First();

						if (bestItem.BaseAddress == item.BaseAddress)
							return false;
					}

					return true;
				}
			}

			return true;
		}

		/// <summary> </summary>
		/*public class OnLootEventArgs : EventArgs
		{
			/// <summary>The name of the item.</summary>
			public string Name { get; set; }

			/// <summary>The type of the item.</summary>
			public string Type { get; set; }

			/// <summary>The ilvl of the item.</summary>
			public int ItemLevel { get; set; }

			/// <summary>The rarity of the item.</summary>
			public Rarity Rarity { get; set; }

			/// <summary>The id of the object we are about to loot.</summary>
			public int Id { get; set; }

			/// <summary>
			/// Ctor.
			/// </summary>
			/// <param name="name">The item's name.</param>
			/// <param name="type">The item's metdata type.</param>
			/// <param name="itemLevel">The item's item level.</param>
			/// <param name="rarity">The item's rarity.</param>
			public OnLootEventArgs(string name, string type, int itemLevel, Rarity rarity, int id)
			{
				Name = name;
				Type = type;
				ItemLevel = itemLevel;
				Rarity = rarity;
				Id = id;
			}
		}*/

		/*public class OnStashEventArgs : EventArgs
		{
			/// <summary>The name of the item.</summary>
			public string Name { get; set; }

			/// <summary>The full name of the item.</summary>
			public string FullName { get; set; }

			/// <summary>The count of the item.</summary>
			public int Count { get; set; }

			/// <summary>The type of the item.</summary>
			public string Metadata { get; set; }

			/// <summary>The ilvl of the item.</summary>
			public int ItemLevel { get; set; }

			/// <summary>The rarity of the item.</summary>
			public Rarity Rarity { get; set; }

			/// <summary>
			/// Ctor.
			/// </summary>
			/// <param name="name">The item's name.</param>
			/// <param name="fullName">The item's full name.</param>
			/// <param name="count">The number of the item.</param>
			/// <param name="metadata">The item's metdata type.</param>
			/// <param name="itemLevel">The item's item level.</param>
			/// <param name="rarity">The item's rarity.</param>
			public OnStashEventArgs(string name, string fullName, int count, string metadata, int itemLevel, Rarity rarity)
			{
				Name = name;
				FullName = fullName;
				Count = count;
				Metadata = metadata;
				ItemLevel = itemLevel;
				Rarity = rarity;
			}
		}*/

		/// <summary>
		/// Manually triggers the OnLoot event. This is for users writing their own LootItemsTask.
		/// </summary>
		/// <param name="item"></param>
		public static void TriggerOnLoot(WorldItem item)
		{
			//LokiPoe.InvokeEvent(OnLoot, null, new OnLootEventArgs(item.Name, item.Type, item.Item.ItemLevel, item.Item.Rarity, item.Id));

			Loki.Bot.Utility.BroadcastEventExecute("oldgrindbot_on_loot", item.Name, item.Type, item.Item.ItemLevel,
				item.Item.Rarity, item.Id);
		}

		/// <summary>
		/// Manually triggers the OnStash event. This is for users writing their own StashItemsTask.
		/// </summary>
		/// <param name="item"></param>
		public static void TriggerOnStash(Item item)
		{
			//LokiPoe.InvokeEvent(OnStash, null, new OnStashEventArgs(item.Name, item.FullName, item.StackCount, item.Metadata, item.ItemLevel, item.Rarity));

			Loki.Bot.Utility.BroadcastEventExecute("oldgrindbot_on_stash", item.Name, item.FullName, item.StackCount,
				item.Metadata, item.ItemLevel, item.Rarity);
		}

		/// <summary>
		/// Manually triggers the OnWithdraw event. This is for users writing their own WithdrawItemsTask.
		/// </summary>
		/// <param name="item"></param>
		public static void TriggerOnWithdraw(Item item)
		{
			Loki.Bot.Utility.BroadcastEventExecute("oldgrindbot_on_withdraw", item.Name, item.FullName,
				Math.Min(item.StackCount, item.MaxStackCount), item.Metadata, item.ItemLevel, item.Rarity);
		}

		/// <summary>
		/// Manually triggers the OnSell event. This is for users writing their own SellItemsTask.
		/// </summary>
		/// <param name="item"></param>
		public static void TriggerOnSell(Item item)
		{
			Loki.Bot.Utility.BroadcastEventExecute("oldgrindbot_on_sell", item.Name, item.FullName,
				Math.Min(item.StackCount, item.MaxStackCount), item.Metadata, item.ItemLevel, item.Rarity);
		}

		/// <summary>An event handler for when the bot loots an item.</summary>
		//public static event EventHandler<OnLootEventArgs> OnLoot;

		/// <summary>An event handler for when the bot stashes an item.</summary>
		//public static event EventHandler<OnStashEventArgs> OnStash;
	}
}