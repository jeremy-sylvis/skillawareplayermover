﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;
using Loki.Common;

namespace OldGrindBot
{
	/// <summary>
	/// This task will use the Loose Grate and Sarcophagus covers for corrupted area transitions.
	/// </summary>
	public class UnblockCorruptedAreaTransitionTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private bool _skip;
		private Vector2i _position;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "UnblockCorruptedAreaTransitionTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task will use the Loose Grate and Sarcophagus covers for corrupted area transitions.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();

				return true;
			}

			if (type != "task_execute")
				return false;

			if (_skip)
				return false;

			// Only execute while we are alive.
			if (LokiPoe.Me.IsDead)
				return false;

			// Don't bother opening these if we aren't taking corrupted area transitions.
			if (!OldGrindBotSettings.Instance.TakeCorruptedAreas)
				return false;

			// If we don't have the position for the objects yet, we can't execute.
			if (_position == Vector2i.Zero)
				return false;

			var looseGrate = LooseGrate;
			var sarcophagus = Sarcophagus;

			if (looseGrate == null && sarcophagus == null)
			{
				if (!PlayerMover.MoveTowards(_position))
				{
					Log.ErrorFormat("[UnblockCorruptedAreaTransitionTask] MoveTowards failed for {0}.", _position);
					await Coroutines.FinishCurrentAction();
				}
				return true;
			}

			var obj = looseGrate ?? sarcophagus;

			if (!obj.IsTargetable)
			{
				_skip = true;
				return true;
			}

			var pd = ExilePather.PathDistance(LokiPoe.MyPosition, _position);
			if (pd > 20)
			{
				if (!PlayerMover.MoveTowards(_position))
				{
					Log.ErrorFormat("[UnblockCorruptedAreaTransitionTask] MoveTowards failed for {0}.", _position);
					await Coroutines.FinishCurrentAction();
				}
				return true;
			}

			if (!await Coroutines.InteractWith(obj))
			{
				Log.ErrorFormat("[UnblockCorruptedAreaTransitionTask] InteractWith failed for the object.");
			}

			await Coroutine.Sleep(3000);

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		private NetworkObject LooseGrate
		{
			get
			{
				return LokiPoe.ObjectManager.GetObjectByName("Loose Grate");
			}
		}

		private NetworkObject Sarcophagus
		{
			get
			{
				return LokiPoe.ObjectManager.GetObjectsByName("Sarcophagus").FirstOrDefault(o => !(o is Chest));
			}
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
			if (_position == Vector2i.Zero)
			{
				var looseGrate = LooseGrate;
				if (looseGrate != null)
				{
					_position = ExilePather.FastWalkablePositionFor(looseGrate);
					Log.InfoFormat(
						"[UnblockCorruptedAreaTransitionTask] The Loose Grate position has been set to: {0}.", _position);
					return;
				}

				var sarcophagus = Sarcophagus;
				if (sarcophagus != null)
				{
					_position = ExilePather.FastWalkablePositionFor(sarcophagus);
					Log.InfoFormat(
						"[UnblockCorruptedAreaTransitionTask] The Sarcophagus position has been set to: {0}.", _position);
					return;
				}
			}
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[UnblockCorruptedAreaTransitionTask] Now resetting task state.");
			_position = Vector2i.Zero;
			_skip = false;
		}
	}
}