﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Game;
using Loki.Common;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary>
	/// A task to sort the main inventory by moving items towards the top left corner.
	/// </summary>
	public class SortInventoryTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "SortInventoryTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "A task to sort the main inventory by moving items towards the top left corner.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type != "task_execute")
				return false;

			if (!LokiPoe.Me.IsInTown && !LokiPoe.Me.IsInHideout)
				return false;

			if (!NeedsToSortInventory(LokiPoe.InstanceInfo.GetPlayerInventoryBySlot(InventorySlot.Main)))
				return false;

			// Close windows that prevent us from iding. Other tasks should be executing in a way where this doesn't interefere.
			if (LokiPoe.InGameState.PurchaseUi.IsOpened || LokiPoe.InGameState.SellUi.IsOpened ||
				LokiPoe.InGameState.TradeUi.IsOpened || !LokiPoe.InGameState.InventoryUi.IsOpened)
			{
				await Coroutines.CloseBlockingWindows();
			}

			// We need the inventory panel open.
			if (!await CoroutinesV3.OpenInventoryPanel())
			{
				return true;
			}

			// Take a list of all non-quest items that are stackable and aren't already in full stacks.
			// We can't shuffle this.
			var items =
				LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).OrderBy(i => i.LocationTopLeft.X)
					.ThenBy(i => i.LocationTopLeft.Y)
					.ToList();

			foreach (var item in items)
			{
				if (LokiPoe.InstanceInfo.GetPlayerInventoryBySlot(InventorySlot.Main).CanSlideItemUpOrLeft(item))
				{
					var name = item.FullName;
					if (name.Equals("Perandus Coin"))
					{
						if (OldGrindBotSettings.Instance.DontStashPerandusCoins)
						{
							continue;
						}
					}
					else if (name.Equals("Silver Coin"))
					{
						if (OldGrindBotSettings.Instance.DontStashSilverCoins)
						{
							continue;
						}
					}
					else if (name.Equals("Alchemy Shard"))
					{
						if (OldGrindBotSettings.Instance.DontStashAlchemyShards)
						{
							continue;
						}
					}
					else if (name.Equals("Transmutation Shard"))
					{
						if (OldGrindBotSettings.Instance.DontStashTransmutationShards)
						{
							continue;
						}
					}
					else if (name.Equals("Alteration Shard"))
					{
						if (OldGrindBotSettings.Instance.DontStashAlterationShards)
						{
							continue;
						}
					}
					else if (name.Equals("Scroll Fragment"))
					{
						if (OldGrindBotSettings.Instance.DontStashScrollFragments)
						{
							continue;
						}
					}
					else if (name.Equals("Portal Scroll"))
					{
						if (OldGrindBotSettings.Instance.DontStashTpScrolls)
						{
							continue;
						}
					}
					else if (name.Equals("Scroll of Wisdom"))
					{
						if (OldGrindBotSettings.Instance.DontStashIdScrolls)
						{
							continue;
						}
					}

					// Tell the user what's going on.
					Log.DebugFormat("[SortInventoryTask] Attempting to pickup the item {0} to the cursor.", name);
					
					var pmtcerr = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Pickup(item.LocalId);
					if (pmtcerr != PickupResult.None)
					{
						Log.ErrorFormat("[SortInventoryTask] InventoryUi.PickupMainToCursor returned {0}.", pmtcerr);
					}

					await Coroutines.LatencyWait();

					if (!await CoroutinesV3.WaitForCursorToHaveItem())
					{
						Log.ErrorFormat("[SortInventoryTask] WaitForCursorToHaveItem failed.");
					}

					await Coroutines.ReactionWait();

					// Start the logic from the top again.
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		/// <summary>Returns true if we need to sort the inventory (re-arrange items) or false otherwise.</summary>
		private bool NeedsToSortInventory(Inventory inv)
		{
			var items = inv.Items.Where(inv.CanSlideItemUpOrLeft);
			foreach (var item in items)
			{
				var name = item.FullName;
				if (name.Equals("Perandus Coin"))
				{
					if (OldGrindBotSettings.Instance.DontStashPerandusCoins)
					{
						continue;
					}
				}
				else if (name.Equals("Silver Coin"))
				{
					if (OldGrindBotSettings.Instance.DontStashSilverCoins)
					{
						continue;
					}
				}
				else if (name.Equals("Alchemy Shard"))
				{
					if (OldGrindBotSettings.Instance.DontStashAlchemyShards)
					{
						continue;
					}
				}
				else if (name.Equals("Transmutation Shard"))
				{
					if (OldGrindBotSettings.Instance.DontStashTransmutationShards)
					{
						continue;
					}
				}
				else if (name.Equals("Alteration Shard"))
				{
					if (OldGrindBotSettings.Instance.DontStashAlterationShards)
					{
						continue;
					}
				}
				else if (name.Equals("Scroll Fragment"))
				{
					if (OldGrindBotSettings.Instance.DontStashScrollFragments)
					{
						continue;
					}
				}
				else if (name.Equals("Portal Scroll"))
				{
					if (OldGrindBotSettings.Instance.DontStashTpScrolls)
					{
						continue;
					}
				}
				else if (name.Equals("Scroll of Wisdom"))
				{
					if (OldGrindBotSettings.Instance.DontStashIdScrolls)
					{
						continue;
					}
				}
				return true;
			}
			return false;
		}
	}
}