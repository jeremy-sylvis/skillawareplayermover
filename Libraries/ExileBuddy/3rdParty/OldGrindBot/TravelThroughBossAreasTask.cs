﻿using System;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Common;
using Loki.Game.Objects;

namespace OldGrindBot
{
	/// <summary>
	/// This task will travel though the local area transitions in boss zones.
	/// </summary>
	public class TravelThroughBossAreasTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private string _grindZone;
		private bool _override;
		private Vector2i? _exploreLocation;
		private bool _inBossArea = true;
		private readonly bool _onlyExecuteInGrindZone;
		private int _index;
		private bool _deathReset;
		private bool _tookBowls;

		/// <summary>
		/// Ctor.
		/// </summary>
		/// <param name="onlyExecuteInGrindZone">Should this task only execute in the grind zone?</param>
		public TravelThroughBossAreasTask(bool onlyExecuteInGrindZone)
		{
			_onlyExecuteInGrindZone = onlyExecuteInGrindZone;
		}

		private string GrindZoneId
		{
			get
			{
				if (_grindZone == null)
				{
					_grindZone = OldGrindBotSettings.Instance.GrindZoneId;
					Log.DebugFormat("[TravelThroughBossAreasTask] GrindZoneId = {0}.", _grindZone);
				}
				return _grindZone;
			}
		}

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "TravelThroughBossAreasTask" + (_onlyExecuteInGrindZone ? "-BossFarm" : "-PassThrough");
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return
					"This task will travel though the local area transitions in boss zones.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_player_died_event")
			{
				var totalDeathsForInstance = (int)param[0];

				Reset();

				// We only need to perform a full reset if we die inside the boss area. When we get respawned, we now have to
				// travel back to the boss.
				var wid = AreaStateCache.Current.WorldArea.Id.Substring(2);
				if (wid == "1_7_2" || wid == "1_11_2" || wid == "3_18_1" || wid == "3_18_2" || wid == "3_14_3" || wid == "2_14_3" || wid == "2_10" || wid == "4_4_3" || wid == "4_5_2" || wid == "4_6_2")
				{
					_deathReset = true;
				}

				return true;
			}

			if (type == "core_area_changed_event")
			{
				Reset();

				return true;
			}

			if (type != "task_execute")
				return false;

			if (_onlyExecuteInGrindZone)
			{
				// If we're not in the area we want to grind, we don't need to execute.
				if (LokiPoe.CurrentWorldArea.Id != GrindZoneId)
					return false;
			}

			// If we've been to the boss area, don't go back if we're leaving.
			// This is for Weavers and Piety.
			if (OldGrindBotSettings.Instance.IsLeavingArea)
				return false;

			if (!_inBossArea)
				return false;

			var worldAreaId = AreaStateCache.Current.WorldArea.Id.Substring(2);

			if (_deathReset)
			{
				Log.InfoFormat("[TravelThroughBossAreasTask] Death reset.");
				Explorer.GetCurrent().Reset();
				AreaStateCache.Current.ResetCurrentAnchorPoint();
				AreaStateCache.Current.ResetTimeInArea();
				StaticLocationManager.UpdateStaticLocations();
				AreaStateCache.Current.ClearChestLocations();
				AreaStateCache.Current.ClearItemLocations();
				Reset();
				return true;
			}

			if (_exploreLocation == null)
			{
				if (worldAreaId == "1_7_2") // The Upper Prison
				{
					var location = StaticLocationManager.GetStaticLocation("The Warden's Quarters (Up)");
					var cmd = new PathfindingCommand(LokiPoe.MyPosition, location, 15, false, 9);

					var pf = ExilePather.FindPath(ref cmd);
					if (pf)
					{
						_exploreLocation = location;
						Log.DebugFormat("[TravelThroughBossAreasTask] Explore location set to {0}.", _exploreLocation);
					}
					else
					{
						location = StaticLocationManager.GetStaticLocation("The Warden's Chambers");
						cmd = new PathfindingCommand(LokiPoe.MyPosition, location, 15, false, 9);
						pf = ExilePather.FindPath(ref cmd);
						if (pf)
						{
							_exploreLocation = location;
							Log.DebugFormat("[TravelThroughBossAreasTask] Explore location set to {0}.",
								_exploreLocation);
						}
						else
						{
							var b = LokiPoe.ObjectManager.Brutus;
							if (b != null && b.IsActive)
							{
								_exploreLocation = b.Position;
								Log.DebugFormat("[TravelThroughBossAreasTask] Explore location set to {0}.",
									_exploreLocation);
							}
						}
					}

					return (_exploreLocation != null);
				}

				if (worldAreaId == "1_11_2") // The Cavern of Anger
				{
					var location = StaticLocationManager.GetStaticLocation("Merveil's Lair");
					var cmd = new PathfindingCommand(LokiPoe.MyPosition, location, 15, false, 9);

					var pf = ExilePather.FindPath(ref cmd);
					if (pf)
					{
						_exploreLocation = location;
						Log.DebugFormat("[TravelThroughBossAreasTask] Explore location set to {0}.", _exploreLocation);
					}
					else
					{
						var b = LokiPoe.ObjectManager.MerveilTheSiren;
						if (b != null && b.IsActive)
						{
							_exploreLocation = b.Position;
							Log.DebugFormat("[TravelThroughBossAreasTask] Explore location set to {0}.",
								_exploreLocation);
						}
						else
						{
							b = LokiPoe.ObjectManager.MerveilTheTwisted;
							if (b != null && b.IsActive)
							{
								_exploreLocation = b.Position;
								Log.DebugFormat("[TravelThroughBossAreasTask] Explore location set to {0}.",
									_exploreLocation);
							}
						}
					}

					return (_exploreLocation != null);
				}

				if (worldAreaId == "3_18_1") // The Sceptre of God
				{
					var location = StaticLocationManager.GetStaticLocation("Stairs Up");
					var cmd = new PathfindingCommand(LokiPoe.MyPosition, location, 15, false, 9);

					var pf = ExilePather.FindPath(ref cmd);
					if (pf)
					{
						_exploreLocation = location;
						Log.DebugFormat("[TravelThroughBossAreasTask] Explore location set to {0}.", _exploreLocation);
					}

					return (_exploreLocation != null);
				}

				if (worldAreaId == "3_18_2") // The Upper Sceptre of God
				{
					var locations = StaticLocationManager.GetStaticLocations("Stairs Up");
					if (_index >= locations.Count)
						return false;
					var location = locations[_index];
					var cmd = new PathfindingCommand(LokiPoe.MyPosition, location, 15, false, 9);

					var pf = ExilePather.FindPath(ref cmd);
					if (pf)
					{
						_exploreLocation = location;
						Log.DebugFormat("[TravelThroughBossAreasTask] Explore location set to {0}.", _exploreLocation);
					}
					else
					{
						++_index;
						return true;
					}

					return (_exploreLocation != null);
				}

				if (worldAreaId == "3_14_3") // The Lunaris Temple Level 3
				{
					var location = StaticLocationManager.GetStaticLocation("Piety (Up)");
					var cmd = new PathfindingCommand(LokiPoe.MyPosition, location, 15, false, 9);

					var pf = ExilePather.FindPath(ref cmd);
					if (pf)
					{
						_exploreLocation = location;
						Log.DebugFormat("[TravelThroughBossAreasTask] Explore location set to {0}.", _exploreLocation);
					}

					return (_exploreLocation != null);
				}

				if (worldAreaId == "2_14_3") // The Ancient Pyramid
				{
					var location = StaticLocationManager.GetStaticLocation("Stairs Up");
					var cmd = new PathfindingCommand(LokiPoe.MyPosition, location, 15, false, 9);

					var pf = ExilePather.FindPath(ref cmd);
					if (pf)
					{
						_exploreLocation = location;
						Log.DebugFormat("[TravelThroughBossAreasTask] Explore location set to {0}.", _exploreLocation);
					}

					return (_exploreLocation != null);
				}

				if (worldAreaId == "2_10") // The Weaver's Chambers
				{
					if (AreaStateCache.Current.HasLocation("The Weaver's Nest"))
					{
						var location = AreaStateCache.Current.GetLocations("The Weaver's Nest").First();
						var cmd = new PathfindingCommand(LokiPoe.MyPosition, location.Position, 15, false, 9);

						var pf = ExilePather.FindPath(ref cmd);
						if (pf)
						{
							_exploreLocation = location.Position;
							Log.DebugFormat("[TravelThroughBossAreasTask] Explore location set to {0}.",
								_exploreLocation);
						}
					}

					return (_exploreLocation != null);
				}

				if (worldAreaId == "4_4_3") // Kaom's Stronghold
				{
					var location = StaticLocationManager.GetStaticLocation("Caldera of The King");
					var cmd = new PathfindingCommand(LokiPoe.MyPosition, location, 15, false, 9);

					var pf = ExilePather.FindPath(ref cmd);
					if (pf)
					{
						_exploreLocation = location;
						Log.DebugFormat("[TravelThroughBossAreasTask] Explore location set to {0}.", _exploreLocation);
					}

					return (_exploreLocation != null);
				}

				if (worldAreaId == "4_5_2") // The Grand Arena
				{
					var location = StaticLocationManager.GetStaticLocation("Arena");
					var cmd = new PathfindingCommand(LokiPoe.MyPosition, location, 15, false, 9);

					var pf = ExilePather.FindPath(ref cmd);
					if (pf)
					{
						_exploreLocation = location;
						Log.DebugFormat("[TravelThroughBossAreasTask] Explore location set to {0}.", _exploreLocation);
					}

					return (_exploreLocation != null);
				}

				if (worldAreaId == "4_6_2") // The Belly of the Beast Level 2
				{
					if (_tookBowls)
						return false;

					var npc = LokiPoe.ObjectManager.GetObjectByName<Npc>("Piety");
					var at = LokiPoe.ObjectManager.AreaTransition("The Bowels of the Beast");

					if (npc != null && npc.HasNpcFloatingIcon)
					{
						if (at != null && npc.Distance < at.Distance)
						{
							_exploreLocation = npc.Position;
							_tookBowls = true;
							return true;
						}
					}
					
					if (at != null && at.IsTargetable)
					{
						_exploreLocation = ExilePather.FastWalkablePositionFor(at);
					}

					return (_exploreLocation != null);
				}

				Log.DebugFormat(
					"[TravelThroughBossAreasTask] We are not currently in a boss area, or a supported boss area.");
				_inBossArea = false;
			}
			else
			{
				if (worldAreaId == "1_7_2") // The Upper Prison
				{
					if (_exploreLocation.Value.Distance(LokiPoe.MyPosition) < 30)
					{
						var at = LokiPoe.ObjectManager.AreaTransition("The Warden's Quarters");
						if (at != null && at.Distance < 35)
						{
							var loc1 = StaticLocationManager.GetStaticLocation("The Warden's Quarters (Up)");
							var loc2 = StaticLocationManager.GetStaticLocation("The Warden's Quarters (Down)");

							if (at.Position.Distance(loc1) < at.Position.Distance(loc2))
							{
								var taterr = await CoroutinesV3.TakeAreaTransition(at.Name, false, -1, true);
								Log.DebugFormat(
									"[TravelThroughBossAreasTask] TakeAreaTransitionCoroutine returned {0}.",
									taterr);

								_exploreLocation = null;
								Explorer.GetCurrent().Reset();
								AreaStateCache.Current.ResetCurrentAnchorPoint();
								AreaStateCache.Current.ResetTimeInArea();
								StaticLocationManager.UpdateStaticLocations();
								AreaStateCache.Current.ClearChestLocations();
								AreaStateCache.Current.ClearItemLocations();

								await Loki.Bot.Utility.BroadcastEventLogic("oldgrindbot_local_area_changed_event");

								return true;
							}
						}

						at = LokiPoe.ObjectManager.AreaTransition("The Warden's Chambers");
						if (at != null && at.Distance < 35)
						{
							var taterr = await CoroutinesV3.TakeAreaTransition(at.Name, false, -1, true);
							Log.DebugFormat("[TravelThroughBossAreasTask] TakeAreaTransition returned {0}.", taterr);

							_exploreLocation = null;
							Explorer.GetCurrent().Reset();
							AreaStateCache.Current.ResetCurrentAnchorPoint();
							AreaStateCache.Current.ResetTimeInArea();
							StaticLocationManager.UpdateStaticLocations();
							AreaStateCache.Current.ClearChestLocations();
							AreaStateCache.Current.ClearItemLocations();

							await Loki.Bot.Utility.BroadcastEventLogic("oldgrindbot_local_area_changed_event");

							return true;
						}
					}
				}
				else if (worldAreaId == "1_11_2") // The Cavern of Anger
				{
					if (_exploreLocation.Value.Distance(LokiPoe.MyPosition) < 30)
					{
						var at = LokiPoe.ObjectManager.AreaTransition("Merveil's Lair");
						if (at != null && at.Distance < 75)
						{
							var taterr = await CoroutinesV3.TakeAreaTransition(at.Name, false, -1, true);
							Log.DebugFormat("[TravelThroughBossAreasTask] TakeAreaTransitionCoroutine returned {0}.",
								taterr);

							_exploreLocation = null;
							Explorer.GetCurrent().Reset();
							AreaStateCache.Current.ResetCurrentAnchorPoint();
							AreaStateCache.Current.ResetTimeInArea();
							StaticLocationManager.UpdateStaticLocations();
							AreaStateCache.Current.ClearChestLocations();
							AreaStateCache.Current.ClearItemLocations();

							await Loki.Bot.Utility.BroadcastEventLogic("oldgrindbot_local_area_changed_event");

							return true;
						}
					}
				}
				else if (worldAreaId == "3_14_3") // The Lunaris Temple Level 3
				{
					if (_exploreLocation.Value.Distance(LokiPoe.MyPosition) < 30)
					{
						var at = LokiPoe.ObjectManager.PietyArenaPortal1;
						if (at != null && at.Distance < 75)
						{
							var taterr = await CoroutinesV3.TakeAreaTransition(at, false, -1, true);
							Log.DebugFormat("[TravelThroughBossAreasTask] TakeAreaTransitionCoroutine returned {0}.",
								taterr);

							_exploreLocation = null;
							Explorer.GetCurrent().Reset();
							AreaStateCache.Current.ResetCurrentAnchorPoint();
							AreaStateCache.Current.ResetTimeInArea();
							StaticLocationManager.UpdateStaticLocations();
							AreaStateCache.Current.ClearChestLocations();
							AreaStateCache.Current.ClearItemLocations();

							await Loki.Bot.Utility.BroadcastEventLogic("oldgrindbot_local_area_changed_event");

							return true;
						}
					}
				}
				else if (worldAreaId == "2_10") // The Weaver's Chambers
				{
					if (_exploreLocation.Value.Distance(LokiPoe.MyPosition) < 30)
					{
						var at = LokiPoe.ObjectManager.AreaTransition("The Weaver's Nest");
						if (at != null && at.Distance < 75)
						{
							var taterr = await CoroutinesV3.TakeAreaTransition(at.Name, false, -1, true);
							Log.DebugFormat("[TravelThroughBossAreasTask] TakeAreaTransitionCoroutine returned {0}.",
								taterr);

							_exploreLocation = null;
							Explorer.GetCurrent().Reset();
							AreaStateCache.Current.ResetCurrentAnchorPoint();
							AreaStateCache.Current.ResetTimeInArea();
							StaticLocationManager.UpdateStaticLocations();
							AreaStateCache.Current.ClearChestLocations();
							AreaStateCache.Current.ClearItemLocations();

							await Loki.Bot.Utility.BroadcastEventLogic("oldgrindbot_local_area_changed_event");

							return true;
						}
					}
				}
				else if (worldAreaId == "3_18_2") // The Upper Sceptre of God
				{
					if (_exploreLocation.Value.Distance(LokiPoe.MyPosition) < 30)
					{
						var at = LokiPoe.ObjectManager.AreaTransition("Stairs");
						if (at != null && at.Distance < 75)
						{
							var taterr = await CoroutinesV3.TakeAreaTransition(at.Name, false, -1, true);
							Log.DebugFormat("[TravelThroughBossAreasTask] TakeAreaTransitionCoroutine returned {0}.",
								taterr);

							_exploreLocation = null;
							_index = 0;
							Explorer.GetCurrent().Reset();
							AreaStateCache.Current.ResetCurrentAnchorPoint();
							AreaStateCache.Current.ResetTimeInArea();
							StaticLocationManager.UpdateStaticLocations();
							AreaStateCache.Current.ClearChestLocations();
							AreaStateCache.Current.ClearItemLocations();

							await Loki.Bot.Utility.BroadcastEventLogic("oldgrindbot_local_area_changed_event");

							return true;
						}
						else
						{
							Log.InfoFormat(
								"[TravelThroughBossAreasTask] We are most likely at a corrupted area transition. Skipping this location.");
							++_index;
							_exploreLocation = null;
							return true;
						}
					}
				}
				else if (worldAreaId == "3_18_1") // The Sceptre of God
				{
					if (_exploreLocation.Value.Distance(LokiPoe.MyPosition) < 30)
					{
						var at = LokiPoe.ObjectManager.AreaTransition("Stairs");
						if (at != null && at.Distance < 75)
						{
							var taterr = await CoroutinesV3.TakeAreaTransition(at.Name, false, -1, true);
							Log.DebugFormat("[TravelThroughBossAreasTask] TakeAreaTransitionCoroutine returned {0}.",
								taterr);

							_exploreLocation = null;
							Explorer.GetCurrent().Reset();
							AreaStateCache.Current.ResetCurrentAnchorPoint();
							AreaStateCache.Current.ResetTimeInArea();
							StaticLocationManager.UpdateStaticLocations();
							AreaStateCache.Current.ClearChestLocations();
							AreaStateCache.Current.ClearItemLocations();

							await Loki.Bot.Utility.BroadcastEventLogic("oldgrindbot_local_area_changed_event");

							return true;
						}

						Log.InfoFormat(
							"[TravelThroughBossAreasTask] We are at stairs, but they are not Stairs. This task will no longer execute for this area.");

						_inBossArea = false;

						// We're most likely at The Upper Sceptre of God transition.
						return false;
					}
				}
				else if (worldAreaId == "2_14_3") // The Ancient Pyramid
				{
					if (_exploreLocation.Value.Distance(LokiPoe.MyPosition) < 30)
					{
						var at1 = LokiPoe.ObjectManager.AreaTransition("Stairs");
						var at2 = LokiPoe.ObjectManager.AreaTransition("Pyramid Apex");
						if (at1 != null && at1.Distance < 75)
						{
							var taterr = await CoroutinesV3.TakeAreaTransition(at1.Name, false, -1, true);
							Log.DebugFormat("[TravelThroughBossAreasTask] TakeAreaTransitionCoroutine returned {0}.",
								taterr);

							_exploreLocation = null;
							Explorer.GetCurrent().Reset();
							AreaStateCache.Current.ResetCurrentAnchorPoint();
							AreaStateCache.Current.ResetTimeInArea();
							StaticLocationManager.UpdateStaticLocations();
							AreaStateCache.Current.ClearChestLocations();
							AreaStateCache.Current.ClearItemLocations();

							await Loki.Bot.Utility.BroadcastEventLogic("oldgrindbot_local_area_changed_event");

							return true;
						}
						if (at2 != null && at2.Distance < 75)
						{
							var taterr = await CoroutinesV3.TakeAreaTransition(at2.Name, false, -1, true);
							Log.DebugFormat("[TravelThroughBossAreasTask] TakeAreaTransitionCoroutine returned {0}.",
								taterr);

							_exploreLocation = null;
							Explorer.GetCurrent().Reset();
							AreaStateCache.Current.ResetCurrentAnchorPoint();
							AreaStateCache.Current.ResetTimeInArea();
							StaticLocationManager.UpdateStaticLocations();
							AreaStateCache.Current.ClearChestLocations();
							AreaStateCache.Current.ClearItemLocations();

							await Loki.Bot.Utility.BroadcastEventLogic("oldgrindbot_local_area_changed_event");

							return true;
						}
					}
				}
				else if (worldAreaId == "4_4_3") // Kaom's Stronghold
				{
					var at = LokiPoe.ObjectManager.AreaTransition("Caldera of The King");
					if (at != null)
					{
						if (at.Distance < 32)
						{
							var taterr = await CoroutinesV3.TakeAreaTransition(at.Name, false, -1, true);
							Log.DebugFormat("[TravelThroughBossAreasTask] TakeAreaTransitionCoroutine returned {0}.",
								taterr);

							_exploreLocation = null;
							Explorer.GetCurrent().Reset();
							AreaStateCache.Current.ResetCurrentAnchorPoint();
							AreaStateCache.Current.ResetTimeInArea();
							StaticLocationManager.UpdateStaticLocations();
							AreaStateCache.Current.ClearChestLocations();
							AreaStateCache.Current.ClearItemLocations();
							_override = false;

							await Loki.Bot.Utility.BroadcastEventLogic("oldgrindbot_local_area_changed_event");

							return true;
						}
						else
						{
							if (!_override)
							{
								_exploreLocation = ExilePather.FastWalkablePositionFor(at);
								_override = true;
							}
						}
					}
				}
				else if (worldAreaId == "4_5_2") // The Grand Arena
				{
					var at = LokiPoe.ObjectManager.AreaTransition("Arena");
					if (at != null)
					{
						if (at.Distance < 32)
						{
							var taterr = await CoroutinesV3.TakeAreaTransition(at.Name, false, -1, true);
							Log.DebugFormat("[TravelThroughBossAreasTask] TakeAreaTransitionCoroutine returned {0}.",
								taterr);

							_exploreLocation = null;
							Explorer.GetCurrent().Reset();
							AreaStateCache.Current.ResetCurrentAnchorPoint();
							AreaStateCache.Current.ResetTimeInArea();
							StaticLocationManager.UpdateStaticLocations();
							AreaStateCache.Current.ClearChestLocations();
							AreaStateCache.Current.ClearItemLocations();
							_override = false;

							await Loki.Bot.Utility.BroadcastEventLogic("oldgrindbot_local_area_changed_event");

							return true;
						}
						else
						{
							if (!_override)
							{
								_exploreLocation = ExilePather.FastWalkablePositionFor(at);
								_override = true;
							}
						}
					}
				}
				else if (worldAreaId == "4_6_2") // The Belly of the Beast Level 2
				{
					if (!_tookBowls)
					{
						var at = LokiPoe.ObjectManager.AreaTransition("The Bowels of the Beast");
						if (at != null)
						{
							if (at.Distance < 32)
							{
								var taterr = await CoroutinesV3.TakeAreaTransition(at.Name, false, -1, true);
								Log.DebugFormat("[TravelThroughBossAreasTask] TakeAreaTransitionCoroutine returned {0}.",
									taterr);

								_exploreLocation = null;
								Explorer.GetCurrent().Reset();
								AreaStateCache.Current.ResetCurrentAnchorPoint();
								AreaStateCache.Current.ResetTimeInArea();
								StaticLocationManager.UpdateStaticLocations();
								AreaStateCache.Current.ClearChestLocations();
								AreaStateCache.Current.ClearItemLocations();
								_override = false;

								await Loki.Bot.Utility.BroadcastEventLogic("oldgrindbot_local_area_changed_event");

								_tookBowls = true;

								return true;
							}
							else
							{
								if (!_override)
								{
									_exploreLocation = ExilePather.FastWalkablePositionFor(at);
									_override = true;
								}
							}
						}
					}
					else
					{
						var npc = LokiPoe.ObjectManager.GetObjectByName<Npc>("Piety");
						if (npc != null && npc.Distance < 20 && npc.HasNpcFloatingIcon)
						{
							var res = await CoroutinesV3.TalkToNpc(npc.Name);
							Log.InfoFormat("[TravelThroughBossAreasTask] TalkToNpc returned {0}.", res);
							return true;
						}
					}
				}
				else
				{
					return false;
				}

				var pos = _exploreLocation.Value;
				if (LokiPoe.MyPosition.Distance(pos) < 12)
				{
					Log.DebugFormat("[TravelThroughBossAreasTask] We are in range of the location {0}.",
						pos);
					_exploreLocation = null;
				}
				else
				{
					if (!PlayerMover.MoveTowards(pos))
					{
						Log.ErrorFormat("[TravelThroughBossAreasTask] MoveTowards failed for {0}.",
							pos);
						_exploreLocation = null;
						await Coroutines.FinishCurrentAction();
					}
				}

				return true;
			}

			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[TravelThroughBossAreasTask] Now resetting task state.");
			_grindZone = null;
			_exploreLocation = null;
			_inBossArea = true;
			_index = 0;
			_deathReset = false;
			_override = false;
			_tookBowls = false;
		}
	}
}