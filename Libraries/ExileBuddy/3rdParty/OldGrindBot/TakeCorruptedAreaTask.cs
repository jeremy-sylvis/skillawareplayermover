﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Common;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary>A task to take corrupted areas.</summary>
	public class TakeCorruptedAreaTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

	    private static AreaStateCache.Location _cachedLocation;
		private static bool _skip;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "TakeCorruptedAreaTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "A task to take corrupted areas.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
            if (type == "core_area_changed_event")
            {
                var oldSeed = (uint)param[0];
                var newSeed = (uint)param[1];
                var oldArea = (DatWorldAreaWrapper)param[2];
                var newArea = (DatWorldAreaWrapper)param[3];

                Reset();
                return true;
            }

			if (type != "task_execute")
				return false;

			if (_skip)
				return false;

			// We don't need to execute in town or when we are dead.
			if (LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout || LokiPoe.Me.IsDead)
				return false;

			if (!OldGrindBotSettings.Instance.TakeCorruptedAreas)
				return false;

			if (_cachedLocation == null)
		    {
				// First check to see if we see a corrupted area transition.
				var at = LokiPoe.ObjectManager.CorruptedAreaTransition();
				if (at == null)
				{
					return false;
				}

				// Check the state to see if we need to blacklist the corrupted area.
				if (OldGrindBotSettings.Instance.BlacklistCorruptedAreaTransition)
                {
                    // Make sure the area transition exists.
                    if (LokiPoe.ObjectManager.GetObjectById(OldGrindBotSettings.Instance.CorruptedAreaTransitionId) != null)
                    {
                        Blacklist.Add(OldGrindBotSettings.Instance.CorruptedAreaTransitionId, TimeSpan.FromHours(24),
                            "BlacklistCorruptedAreaTransition");
                        OldGrindBotSettings.Instance.BlacklistCorruptedAreaTransition = false;
                        OldGrindBotSettings.Instance.CorruptedAreaTransitionId = 0;
                        AreaStateCache.CorruptedAreaParentId = "";
                    }
                }

                // If we do, make sure it's not blacklisted.
                if (Blacklist.Contains(at.Id))
                {
                    Log.InfoFormat("[TakeCorruptedAreaTask] The corrupted area transition ({0}) is blacklisted.", at.Id);
                    return false;
                }

                // Set global state since we need to affect a different area state later.
                OldGrindBotSettings.Instance.BlacklistCorruptedAreaTransition = false;
                OldGrindBotSettings.Instance.CorruptedAreaTransitionId = at.Id;
                AreaStateCache.CorruptedAreaParentId = LokiPoe.CurrentWorldArea.Id;

                _cachedLocation = AreaStateCache.Current.GetLocations(at.Name).FirstOrDefault();
                if (_cachedLocation == null)
                {
                    await Coroutine.Sleep(100);
                    return true;
                }

                Log.InfoFormat("[TakeCorruptedAreaTask] Location set to {0}: {1}.", _cachedLocation.Name, _cachedLocation.Position);
		    }

			// Check to see if this corrupted area should be taken.
			/*if (BotState.HandleDangerousArea != null)
            {
                if (!BotState.HandleDangerousArea.ShouldExplore(at))
                {
                    Blacklist.Add(at.Id, TimeSpan.FromHours(24), "!HandleDangerousArea.ShouldExplore");
                    return false;
                }
            }*/

			// Try to move closer to the AT to prevent issues of combat being ignored.
            if (ExilePather.PathDistance(LokiPoe.MyPosition, _cachedLocation.Position) > 30)
			{
                if (!PlayerMover.MoveTowards(_cachedLocation.Position))
				{
                    Log.ErrorFormat("[TakeCorruptedAreaTask] MoveTowards failed for {0}.", _cachedLocation.Position);
					_skip = true;
					await Coroutines.FinishCurrentAction();
				}
				return true;
			}

			var at2 = LokiPoe.ObjectManager.CorruptedAreaTransition();
			if (at2 == null)
			{
				Log.ErrorFormat("[TakeCorruptedAreaTask] CorruptedAreaTransition is null when it should not be.");
				_skip = true;
				return false;
			}

			if (await CoroutinesV3.WaitForAreaTransition(_cachedLocation.Name))
			{
                var taterr = await CoroutinesV3.TakeAreaTransition(_cachedLocation.Name, false, -1);
				if (taterr == CoroutinesV3.TakeAreaTransitionError.None)
				{
					return true;
				}
				Log.ErrorFormat("[TakeCorruptedAreaTask] TakeAreaTransition returned {0}.", taterr);
				_skip = true;
			}
			else
			{
				Log.ErrorFormat("[TakeCorruptedAreaTask] WaitForAreaTransition failed.");
				_skip = true;
			}

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

	    private void Reset()
	    {
            Log.DebugFormat("[TakeCorruptedAreaTask] Now resetting task state.");
	        _cachedLocation = null;
		    _skip = false;
	    }
	}
}