﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;
using Loki.Common;

namespace OldGrindBot
{
	/// <summary>
	/// This task handles looting items.
	/// </summary>
	public class LootItemsTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private readonly int _leashRange;
		private readonly SetSettingsDelegate<int> _setNeedsTownRun;
		private readonly TriggerOnLootDelegate _triggerOnLoot;
		private readonly SetSettingsDelegate<bool> _setNeedsStash;

		private AreaStateCache.ItemLocation _itemLocation;
		private readonly Stopwatch _itemStopwatch = new Stopwatch();
		private readonly Stopwatch _interactStopwatch = new Stopwatch();
		private int _interactTries;
		private readonly Stopwatch _displayStopwatch = Stopwatch.StartNew();
		private readonly Stopwatch _lastInteractStopwatch = Stopwatch.StartNew();

		private readonly Dictionary<int, int> _lootInteractions = new Dictionary<int, int>();

		private int _movementTimeout = 1000*60*3;

		/// <summary>
		/// The movement timeout for items in MS. If this timeout elapses while moving towards an item, it will be blacklisted.
		/// </summary>
		private int MovementTimeOut
		{
			set
			{
				_movementTimeout = value;
				if (_movementTimeout < 1)
					_movementTimeout = 1;
				Log.InfoFormat("[LootItemsTask] MovementTimeOut = {0}.", _movementTimeout);
			}
			get
			{
				return _movementTimeout;
			}
		}

		/// <summary>
		/// A ctor that sets a user defined leash value.
		/// </summary>
		public LootItemsTask(int leashRange,
			SetSettingsDelegate<int> setNeedsTownRun,
			SetSettingsDelegate<bool> setNeedsStash,
			TriggerOnLootDelegate triggerOnLoot = null)
		{
			if (setNeedsTownRun == null)
				throw new NullReferenceException("setNeedsTownRun");

			if (setNeedsStash == null)
				throw new NullReferenceException("setNeedsStash");

			_setNeedsTownRun = setNeedsTownRun;
			_triggerOnLoot = triggerOnLoot;
			_setNeedsStash = setNeedsStash;

			_leashRange = leashRange;
		}

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "LootItemTask (Leash " + _leashRange + ")";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task handles looting items.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				_itemLocation = null;
				_itemStopwatch.Reset();
				_lootInteractions.Clear();

				return true;
			}

			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsDead || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout)
				return false;

			if (_itemLocation == null)
			{
				var myPos = LokiPoe.MyPosition;

				_itemLocation =
					AreaStateCache.Current.ItemLocations.Where(
						i => !Blacklist.Contains(i.Id) &&
							(_leashRange == -1 ||
							(i.Position.Distance(myPos) < _leashRange && ExilePather.PathDistance(myPos, i.Position) < _leashRange)))
						.OrderBy(l => myPos.Distance(l.Position))
						.FirstOrDefault();

				_itemStopwatch.Reset();
				_interactStopwatch.Reset();

				_interactTries = 0;

				if (_itemLocation != null)
				{
					Log.InfoFormat("[{0}] _itemLocation set to: {1} with id {2} at {3}.", Name, _itemLocation.Name, _itemLocation.Id,
						_itemLocation.Position);
				}
			}

			// We need an item location to execute!
			if (_itemLocation == null)
				return false;

			var display = false;

			if (_displayStopwatch.ElapsedMilliseconds > 1000)
			{
				display = true;
				_displayStopwatch.Restart();
			}

			if (display)
			{
				Log.DebugFormat(
					"[LootItemTask] The current item to loot is [{0}] {1} at {2}. We have been on this task for {3} and have been attempting to interact for {4}.",
					_itemLocation.Id, _itemLocation.Name, _itemLocation.Position, _itemStopwatch.Elapsed,
					_interactStopwatch.Elapsed);
			}

			// Track travel time.
			if (_itemStopwatch.ElapsedMilliseconds >= MovementTimeOut)
			{
				Log.ErrorFormat("[LootItemTask] {0} has elapsed while moving towards the item position. Now ignoning the item.", _itemStopwatch.ElapsedMilliseconds);

				Blacklist.Add(_itemLocation.Id, TimeSpan.FromHours(1), "Travel time too long.");
                AreaStateCache.Current.RemoveItemLocation(_itemLocation);

				_itemLocation = null;

				_itemStopwatch.Stop();

				return true;
			}

			// Check for blacklist
			if (Blacklist.Contains(_itemLocation.Id))
			{
				Log.ErrorFormat("[LootItemTask] The item {0} at {1} has been blacklisted.", _itemLocation.Id, _itemLocation.Position);

				AreaStateCache.Current.RemoveItemLocation(_itemLocation);

				_itemLocation = null;

				return true;
			}

			_itemStopwatch.Start();

			var pathDistance = ExilePather.PathDistance(LokiPoe.MyPosition, _itemLocation.Position);
			var canSee = ExilePather.CanObjectSee(LokiPoe.Me, _itemLocation.Position);
			if (pathDistance > 20 || (!canSee && pathDistance > 10))
			{
				if (display)
				{
					Log.DebugFormat("[LootItemTask] Now moving towards the item {1} because it is {0} away ({2}).",
						pathDistance,
						_itemLocation.Id, canSee ? "can see" : "can't see");
				}

				if (!PlayerMover.MoveTowards(_itemLocation.Position))
				{
					Log.ErrorFormat("[LootItemTask] MoveTowards failed for {0}.", _itemLocation.Position);

					AreaStateCache.Current.RemoveItemLocation(_itemLocation);

					_itemLocation = null;

					await Coroutines.FinishCurrentAction();
				}

				_itemStopwatch.Stop();

				return true;
			}

			if (_interactStopwatch.ElapsedMilliseconds >= 10000 || _interactTries >= 5)
			{
				Log.DebugFormat("[LootItemTask] Timed out when trying to pickup the item.");

				AreaStateCache.Current.RemoveItemLocation(_itemLocation);

				_itemLocation = null;

				return true;
			}

			await Coroutines.FinishCurrentAction();

			await Coroutines.CloseBlockingWindows();

			_interactStopwatch.Start();

			if (_interactTries >= 2)
			{
				if (LokiPoe.ConfigManager.IsAlwaysHighlightEnabled)
				{
					Log.InfoFormat("[LootItemTask] Now toggling Always Highlight on and off to reset the position of loot labels.");
					LokiPoe.Input.SimulateKeyEvent(LokiPoe.Input.Binding.highlight_toggle, true, false, false);
					LokiPoe.Input.SimulateKeyEvent(LokiPoe.Input.Binding.highlight_toggle, true, false, false);
				}
			}

			if (!LokiPoe.ConfigManager.IsAlwaysHighlightEnabled)
			{
				Log.InfoFormat("[LootItemTask] Now enabling Always Highlight to avoid highlight issues.");
				LokiPoe.Input.SimulateKeyEvent(LokiPoe.Input.Binding.highlight_toggle, true, false, false);
				await Coroutine.Sleep(16);
			}

			var item = LokiPoe.ObjectManager.GetObjectById<WorldItem>(_itemLocation.Id);
			if (item == null || !item.IsTargetable)
			{
				AreaStateCache.Current.RemoveItemLocation(_itemLocation);

				_itemLocation = null;

				return true;
			}

			if (!LokiPoe.InstanceInfo.GetPlayerInventoryBySlot(InventorySlot.Main).CanFitItem(item.Item))
			{
				_setNeedsTownRun(1); // We want to return to this instance.
				_setNeedsStash(true); // We have to stash.
				Log.DebugFormat(
					"[LootItemsTask] We cannot fit an item on the ground in our inventory. Now triggering a town run.");
				return true;
			}

			++_interactTries;

			Log.DebugFormat("[LootItemTask] Now attempting to interact with the item. This is the #{0} attempt.",
				_interactTries);

			if (!_lootInteractions.ContainsKey(_itemLocation.Id))
			{
				_lootInteractions.Add(_itemLocation.Id, 0);
				if (_triggerOnLoot != null)
				{
					_triggerOnLoot(item);
				}
			}
			_lootInteractions[_itemLocation.Id]++;

			var last = (int)_lastInteractStopwatch.ElapsedMilliseconds;
			if (last < 200)
			{
				await Coroutine.Sleep(200 - last);
			}

			if (!await Coroutines.InteractWith(item))
			{
				Log.ErrorFormat("[LootItemTask] InteractWith failed.");
			}

			var sw = Stopwatch.StartNew();
			item = LokiPoe.ObjectManager.GetObjectById<WorldItem>(_itemLocation.Id);
			while (item != null)
			{
				await Coroutine.Yield();
				item = LokiPoe.ObjectManager.GetObjectById<WorldItem>(_itemLocation.Id);
				if (sw.ElapsedMilliseconds > 1000)
					break;
			}

			_itemStopwatch.Stop();
			_interactStopwatch.Stop();

			_lastInteractStopwatch.Restart();

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			if (name == "GetItem")
			{
				return _itemLocation;
			}

			if (name == "SetMovementTimeOut")
			{
				MovementTimeOut = (int) param[0];
				return null;
			}

			if (name == "GetMovementTimeOut")
			{
				return MovementTimeOut;
			}

			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
			_itemLocation = null;
			_itemStopwatch.Reset();
		}
	}
}