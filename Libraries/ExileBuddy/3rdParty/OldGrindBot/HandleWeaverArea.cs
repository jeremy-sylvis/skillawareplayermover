﻿using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace OldGrindBot
{
	/// <summary>
	/// This task helps OldGrindBot stay in the Weaver boss fight area, resetting exploration to 
	/// keep the bot moving around to trigger different phases.
	/// </summary>
	public class HandleWeaverArea : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();
		private bool _skip;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get { return "HandleWeaverArea"; }
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get { return "This task helps OldGrindBot stay in the Weaver boss fight area, resetting exploration to keep the bot moving around to trigger different phases."; }
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get { return "Bossland GmbH"; }
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get { return "0.0.1.1"; }
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();
				return true;
			}

			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsDead)
				return false;

			if (_skip)
				return false;

			if (LokiPoe.CurrentWorldArea.Name != "The Weaver's Chambers")
			{
				Log.InfoFormat("[WeaverFight] The current area does not contain The Weaver. Skipping execution until a restart or area change.");
				_skip = true;
				return false;
			}

			// Simply move towards Weaver if nothing else is running.
			var weaver = LokiPoe.ObjectManager.Weaver;
			if (weaver != null)
			{
				if (!weaver.IsDead)
				{
					PlayerMover.MoveTowards(weaver.Position);
					return true;
				}
				else
				{
					Log.InfoFormat("[WeaverFight] Weaver is dead. Now marking this task to skip executing.");
					//BasicGrindBotSettings.Instance.NeedsTownRun = 2;
					_skip = true;
					return true;
				}
			}

			// Reset the explorer, so we move around the area looking for things that might be out of view.
			// It's up to the CR to avoid getting stuck killing Miscreations near their spawner.
			var explorer = Explorer.GetCurrent();
			if (explorer != null)
			{
				Log.InfoFormat("[WeaverFight] Now resetting the explorer.");
				explorer.Reset();
			}

			// Don't execute any tasks afterwards. This forces the OldGrindBot to stay in the boss area.
			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[HandleWeaverArea] Now resetting task state.");
			_skip = false;
		}
	}
}
