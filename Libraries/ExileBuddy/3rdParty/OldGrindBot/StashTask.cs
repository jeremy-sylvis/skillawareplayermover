﻿using System;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Game;
using Loki.Common;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary>
	/// This task will stash all applicable items in town.
	/// </summary>
	public class StashTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private readonly CoroutinesV3.NeedsToStashDelegate _needsToStash;
		private readonly CoroutinesV3.StashingCompleteDelegate _stashingComplete;
		private readonly CoroutinesV3.StashItemsDelegate _shouldStashItem;
		private readonly CoroutinesV3.BestStashTabForItem _bestStashTabForItem;
		private readonly object _user;
		private int _tries;
	    private bool _isFull;
        private readonly TriggerOnStashDelegate _triggerOnStash;

		private static int _delayAfterFailureMs = 15000;

		/// <summary>
		/// How long to wait after StashItemsCoroutine fails before leaving task execution to try again.
		/// </summary>
		public static int DelayAfterFailureMs
		{
			get
			{
				return _delayAfterFailureMs;
			}
			set
			{
				_delayAfterFailureMs = value;
				if (_delayAfterFailureMs < 1000)
					_delayAfterFailureMs = 1000;
                Log.InfoFormat("[StashTask] DelayAfterFailureMs = {0}", _delayAfterFailureMs);
			}
		}

		/// <summary>
		/// Ctor that sets the delegate for obtaining current settings.
		/// </summary>
		/// <param name="needsToStash">The delegate to determine if the task needs to run.</param>
		/// <param name="stashingComplete">The delegate to run after stashing has completed.</param>
		/// <param name="shouldStashItem">The delegate for determining if an item needs to be stashed.</param>
		/// <param name="bestStashTabForItem">The delegate for determining if an item should be stashed to a specific tab.</param>
		/// <param name="user">The user object to pass to StashItemsCoroutine.</param>
		/// <param name="triggerOnStash">The delegate to call when an item is stashed.</param>
		public StashTask(
			CoroutinesV3.NeedsToStashDelegate needsToStash,
			CoroutinesV3.StashingCompleteDelegate stashingComplete,
			CoroutinesV3.StashItemsDelegate shouldStashItem,
			CoroutinesV3.BestStashTabForItem bestStashTabForItem,
			object user = null,
			TriggerOnStashDelegate triggerOnStash = null
			)
		{
			_needsToStash = needsToStash;
			_stashingComplete = stashingComplete;
			_shouldStashItem = shouldStashItem;
			_bestStashTabForItem = bestStashTabForItem;
			_user = user;
			_triggerOnStash = triggerOnStash;
		}

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "StashTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task will stash all applicable items in town.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

                Reset();
                return true;
			}

			if (type != "task_execute")
				return false;

			if (!LokiPoe.Me.IsInTown && !LokiPoe.Me.IsInHideout)
			{
                Reset();
                return false;
			}

			// Make sure we don't sit around, hammering stash all day long.
			if (_tries > 3)
			{
				Log.ErrorFormat(
					"[StashTask] The bot has failed to stash items 3 times. Now stopping the bot because it cannot continue.");
				BotManager.Stop();
				return true;
			}

		    if (_tries >= 1 && _isFull)
		    {
                Loki.Bot.Utility.BroadcastEventExecute("oldgrindbot_stash_full");
                BotManager.Stop();
                return true;
            }

			if (!_needsToStash())
			{
                Reset();
                return false;
			}

			++_tries;

			var res = await CoroutinesV3.StashItemsCoroutine(_shouldStashItem, _bestStashTabForItem, _user, _triggerOnStash);
			if (res != CoroutinesV3.StashItemsCoroutineError.None)
			{
			    if (res == CoroutinesV3.StashItemsCoroutineError.CouldNotFitAllItems)
			    {
			        _isFull = true;
                    Log.DebugFormat("[StashTask] StashItemsCoroutine returned {0}. This is try #{1}.", res, _tries);
			        return true;
			    }

				Log.DebugFormat(
					"[StashTask] StashItemsCoroutine returned {0}. This is try #{1}. Waiting {2} ms and then trying again.", res, _tries, DelayAfterFailureMs);

				await Coroutine.Sleep(DelayAfterFailureMs);

				return true;
			}

		    Reset();

            _stashingComplete();

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
            Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

	    private void Reset()
	    {
	        _tries = 0;
	        _isFull = false;
	    }
	}
}