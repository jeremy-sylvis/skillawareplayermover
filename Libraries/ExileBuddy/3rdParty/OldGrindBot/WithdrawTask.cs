﻿using System;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.NativeWrappers;
using Loki.Game.Objects;
using Loki.Common;

namespace OldGrindBot
{
	/// <summary>A task to withdraw items from stash when we are in town.</summary>
	public class WithdrawTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private bool _skip;

		private bool _needsId;
		private bool _needsTp;

		private bool _needsAlchemy;
		private bool _needsChaos;
		private bool _needsChance;
		private bool _needsAlteration;
		private bool _needsAugmentation;
		private bool _needsTransmutation;
		private bool _needsScour;
		private bool _needsVaal;

		private int _tabsProcessed;

		private readonly object _user;
		private readonly TriggerOnWithdrawDelegate _triggerOnWithdraw;

		public WithdrawTask(TriggerOnWithdrawDelegate triggerOnWithdraw = null)
		{
			_triggerOnWithdraw = triggerOnWithdraw;
		}

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "WithdrawTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "A task to withdraw items from stash when we are in town.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();

				return true;
			}

			if (type != "task_execute")
				return false;

			if (_skip)
				return false;

			// Only id in town.
			if (!LokiPoe.Me.IsInTown && !LokiPoe.Me.IsInHideout)
			{
				Log.InfoFormat(
					"[WithdrawTask] We cannot withdraw items out of town/hideout. Skipping this task until an area change or restart.");
				_skip = true;
				return false;
			}

			UpdateNeeds();

			if (!_needsId &&
			    !_needsTp &&
			    !_needsAlchemy &&
			    !_needsChaos &&
			    !_needsChance &&
			    !_needsAlteration &&
			    !_needsAugmentation &&
			    !_needsTransmutation &&
			    !_needsScour &&
			    !_needsVaal)
			{
				Log.InfoFormat(
					"[WithdrawTask] We do not need to withdraw any currency items. Skipping this task until an area change or restart.");
				_skip = true;
				return false;
			}

			_tabsProcessed = 0;

			var wierr =
				await CoroutinesV3.WithdrawItemsCoroutine(ShouldWithdrawFromTab, ShouldWithdrawItem, ContinueWithdrawing, _user, _triggerOnWithdraw);
			if (wierr != CoroutinesV3.WithdrawItemsCoroutineError.None)
			{
				Log.ErrorFormat(
					"[WithdrawTask] WithdrawItemsCoroutine returned {0}. Skipping this task until an area change or restart.",
					wierr);
			}
			else
			{
				Log.InfoFormat(
					"[WithdrawTask] Withdrawing has completed. Skipping this task until an area change or restart.");
			}

			_skip = true;

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[WithdrawTask] Now resetting task state.");
			_skip = false;
			_needsTp = false;
			_needsId = false;
			_needsAlchemy = false;
			_needsChaos = false;
			_needsChance = false;
			_needsAlteration = false;
			_needsAugmentation = false;
			_needsTransmutation = false;
			_needsScour = false;
			_needsVaal = false;
		}

		private bool ShouldWithdrawItem(Item item, out int count, object user)
		{
			count = item.StackCount;

			UpdateNeeds();

			if (_needsTp)
			{
				if (item.FullName.Equals("Portal Scroll"))
				{
					return true;
				}
			}

			if (_needsId)
			{
				if (item.FullName.Equals("Scroll of Wisdom"))
				{
					return true;
				}
			}

			if (_needsAlchemy)
			{
				if (item.FullName.Equals("Orb of Alchemy"))
				{
					return true;
				}
			}

			if (_needsChaos)
			{
				if (item.FullName.Equals("Chaos Orb"))
				{
					return true;
				}
			}

			if (_needsChance)
			{
				if (item.FullName.Equals("Orb of Chance"))
				{
					return true;
				}
			}

			if (_needsAlteration)
			{
				if (item.FullName.Equals("Orb of Alteration"))
				{
					return true;
				}
			}

			if (_needsAugmentation)
			{
				if (item.FullName.Equals("Orb of Augmentation"))
				{
					return true;
				}
			}

			if (_needsTransmutation)
			{
				if (item.FullName.Equals("Orb of Transmutation"))
				{
					return true;
				}
			}

			if (_needsScour)
			{
				if (item.FullName.Equals("Orb of Scouring"))
				{
					return true;
				}
			}

			if (_needsVaal)
			{
				if (item.FullName.Equals("Vaal Orb"))
				{
					return true;
				}
			}

			return false;
		}

		private bool ShouldWithdrawFromTab(StashTabInfo tab, object user)
		{
			// Don't withdraw from these, even though the coroutine already skips them.
			if (tab.IsPublic || tab.IsRemoveOnly)
			{
				return false;
			}

			// Only increment if true is being returned.
			++_tabsProcessed;

			// If this tab would be past our max tab count, don't withdraw from it.
			if (_tabsProcessed > OldGrindBotSettings.Instance.MaxWithdrawTabs)
			{
				return false;
			}

			return true;
		}

		private bool ContinueWithdrawing(object user)
		{
			// If we're past the number of tabs we want to process, don't continue to withdraw.
			if (_tabsProcessed > OldGrindBotSettings.Instance.MaxWithdrawTabs)
			{
				return false;
			}

			UpdateNeeds();

			if (_needsId)
				return true;

			if (_needsTp)
				return true;

			if (_needsAlchemy)
				return true;

			if (_needsChaos)
				return true;

			if (_needsChance)
				return true;

			if (_needsAlteration)
				return true;

			if (_needsAugmentation)
				return true;

			if (_needsTransmutation)
				return true;

			if (_needsScour)
				return true;

			if (_needsVaal)
				return true;

			return false;
		}

		private void UpdateNeeds()
		{
			if (OldGrindBotSettings.Instance.FillIdScrollStacks)
			{
				var item =
					LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(
						i => i.FullName.Equals("Scroll of Wisdom"));
				if (item == null || (item.StackCount < item.MaxStackCount/2))
				{
					_needsId = true;
				}
				else
				{
					_needsId = false;
				}
			}

			if (OldGrindBotSettings.Instance.FillTpScrollStacks)
			{
				var item =
					LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(
						i => i.FullName == "Portal Scroll");
				if (item == null || (item.StackCount < item.MaxStackCount / 2))
				{
					_needsTp = true;
				}
				else
				{
					_needsTp = false;
				}
			}

			if (OldGrindBotSettings.Instance.FillAlchemyStacks)
			{
				var item =
					LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(
						i => i.FullName.Equals("Orb of Alchemy"));
				if (item == null || (item.StackCount < item.MaxStackCount))
				{
					_needsAlchemy = true;
				}
				else
				{
					_needsAlchemy = false;
				}
			}

			if (OldGrindBotSettings.Instance.FillChaosStacks)
			{
				var item =
					LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(
						i => i.FullName.Equals("Chaos Orb"));
				if (item == null || (item.StackCount < item.MaxStackCount))
				{
					_needsChaos = true;
				}
				else
				{
					_needsChaos = false;
				}
			}

			if (OldGrindBotSettings.Instance.FillChanceStacks)
			{
				var item =
					LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(
						i => i.FullName.Equals("Orb of Chance"));
				if (item == null || (item.StackCount < item.MaxStackCount))
				{
					_needsChance = true;
				}
				else
				{
					_needsChance = false;
				}
			}

			if (OldGrindBotSettings.Instance.FillAlterationStacks)
			{
				var item =
					LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(
						i => i.FullName.Equals("Orb of Alteration"));
				if (item == null || (item.StackCount < item.MaxStackCount))
				{
					_needsAlteration = true;
				}
				else
				{
					_needsAlteration = false;
				}
			}

			if (OldGrindBotSettings.Instance.FillAugmentationStacks)
			{
				var item =
					LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main)
						.FirstOrDefault(i => i.FullName.Equals("Orb of Augmentation"));
				if (item == null || (item.StackCount < item.MaxStackCount))
				{
					_needsAugmentation = true;
				}
				else
				{
					_needsAugmentation = false;
				}
			}

			if (OldGrindBotSettings.Instance.FillTransmutationStacks)
			{
				var item =
					LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(
						i => i.FullName.Equals("Orb of Transmutation"));
				if (item == null || (item.StackCount < item.MaxStackCount))
				{
					_needsTransmutation = true;
				}
				else
				{
					_needsTransmutation = false;
				}
			}

			if (OldGrindBotSettings.Instance.FillScourStacks)
			{
				var item =
					LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(
						i => i.FullName.Equals("Orb of Scouring"));
				if (item == null || (item.StackCount < item.MaxStackCount))
				{
					_needsScour = true;
				}
				else
				{
					_needsScour = false;
				}
			}

			if (OldGrindBotSettings.Instance.FillVaalStacks)
			{
				var item =
					LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(
						i => i.FullName.Equals("Vaal Orb"));
				if (item == null || (item.StackCount < item.MaxStackCount))
				{
					_needsVaal = true;
				}
				else
				{
					_needsVaal = false;
				}
			}
		}
	}
}