﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Common;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary>
	/// This task handles leaving the current area after exploration and all other tasks are complete.
	/// This task does not handle local area transitions, or mission areas.
	/// </summary>
	public class LeaveCurrentAreaTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private AreaStateCache.Location _transitionLocation;
		private int _atTries;
		private readonly Stopwatch _displayStopwatch = Stopwatch.StartNew();

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "LeaveCurrentAreaTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task handles leaving the current area after exploration and all other tasks are complete.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();
				return true;
			}

			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsDead || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout)
				return false;

			var display = false;
			if (_displayStopwatch.ElapsedMilliseconds > 1000)
			{
				display = true;
				_displayStopwatch.Restart();
			}

			if (_transitionLocation != null)
			{
				if (display)
				{
					Log.DebugFormat("[LeaveCurrentAreaTask] The area transition to move to is {0} at {1}.",
						_transitionLocation.Name, _transitionLocation.Position);
				}

				var pathDistance = ExilePather.PathDistance(LokiPoe.MyPosition, _transitionLocation.Position);
				if (pathDistance > 20)
				{
					if (display)
					{
						Log.DebugFormat(
							"[LeaveCurrentAreaTask] Now moving towards the area transition {0} because it is {1} away.",
							_transitionLocation.Name, pathDistance);
					}

					if (!PlayerMover.MoveTowards(_transitionLocation.Position))
					{
						Log.ErrorFormat("[LeaveCurrentAreaTask] MoveTowards failed for {0}.",
							_transitionLocation.Position);
						await Coroutines.FinishCurrentAction();
					}

					return true;
				}

				++_atTries;

				if (_atTries > 3)
				{
					Log.ErrorFormat(
						"[LeaveCurrentAreaTask] The bot has failed 3 times to take an area transition to another area. Now returning to town.");

					await CoroutinesV3.CreateAndTakePortalToTown();

					return true;
				}

				if (!await CoroutinesV3.WaitForAreaTransition(_transitionLocation.Name))
				{
					Log.ErrorFormat(
						"[TravelToGrindZoneTask] WaitForAreaTransition failed for {0}. This is the #{1} try.",
						_transitionLocation.Name, _atTries);

					await Coroutine.Sleep(3000);

					return true;
				}

				var taterr = await CoroutinesV3.TakeAreaTransition(_transitionLocation.Name, false, -1);
				if (taterr != CoroutinesV3.TakeAreaTransitionError.None)
				{
					Log.ErrorFormat("[LeaveCurrentAreaTask] TakeAreaTransition returned {0}. This is the #{1} try.",
						taterr, _atTries);

					await Coroutine.Sleep(3000);
				}

				return true;
			}

			var knownTransitions = new List<AreaStateCache.Location>();

			var transitions = LokiPoe.CurrentWorldArea.Connections.ToList();
			foreach (var transition in transitions)
			{
				if (AreaStateCache.Current.HasLocation(transition.Name))
				{
					var loc = AreaStateCache.Current.GetLocations(transition.Name).FirstOrDefault();
					if (ExilePather.PathExistsBetween(LokiPoe.MyPosition, loc.Position))
					{
						knownTransitions.Add(loc);
					}
				}
			}

			if (knownTransitions.Count != 0)
			{
				var myPos = LokiPoe.MyPosition;
				_transitionLocation = knownTransitions.OrderBy(l => l.Position.Distance(myPos)).First();
				Log.DebugFormat(
					"[LeaveCurrentAreaTask] The area transition's location has been found from AreaStateCache.");
				return true;
			}

			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
			Reset();
		}

		private void Reset()
		{
			Log.DebugFormat("[LeaveCurrentAreaTask] Now resetting task state.");
			_transitionLocation = null;
			_atTries = 0;
		}
	}
}