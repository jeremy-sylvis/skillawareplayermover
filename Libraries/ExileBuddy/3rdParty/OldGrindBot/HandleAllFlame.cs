﻿using System;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary>
	/// This task handles an aspect of giving Fairgraves the Allflame.
	/// </summary>
	public class HandleAllFlame : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private bool _skip;

		private bool _hasLocation;
		private Vector2i _location;

		private bool _hasLocation2;
		private Vector2i _location2;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "HandleAllFlame";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task handles an aspect of giving Fairgraves the Allflame.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();
				return true;
			}

			if (type != "task_execute")
				return false;

			if (_skip)
				return false;

			if (LokiPoe.Me.IsDead)
				return false;

			if (LokiPoe.CurrentWorldArea.Name != "The Ship Graveyard")
			{
				Log.InfoFormat("[HandleAllFlame] The current area does not contain the quest. Skipping execution until a restart or area change.");
				_skip = true;
				return false;
			}

			if (!_hasLocation)
			{
				if (_hasLocation2 && _location2 != Vector2i.Zero)
				{
					if (!PlayerMover.MoveTowards(_location2))
					{
						Log.ErrorFormat("[HandleAllFlame] MoveTowards failed for {0}.", _location2);
						_location2 = Vector2i.Zero;
						await Coroutines.FinishCurrentAction();
					}
					return true;
				}

				return false;
			}

			// Close blocking windows
			await Coroutines.CloseBlockingWindows();

			if (LokiPoe.MyPosition.Distance(_location) > 30)
			{
				if (!PlayerMover.MoveTowards(_location))
				{
					Log.ErrorFormat("[HandleAllFlame] MoveTowards failed for {0}.", _location);
					await Coroutines.FinishCurrentAction();
				}
				return true;
			}

			var npc = LokiPoe.ObjectManager.CaptainFairgraves;
			if (npc == null)
			{
				Log.ErrorFormat("[HandleAllFlame] CaptainFairgraves == null. This should NOT happen.");
				return true;
			}

			var res = await Coroutines.InteractWith(npc);
			Log.InfoFormat("[HandleAllFlame] InteractWith returned {0}.", res);

			Reset();

			if (res)
			{
				_skip = true;
			}

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
			if (_skip)
				return;

			if (LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(i => i.FullName == "Allflame") == null)
			{
				_skip = true;
				Log.InfoFormat("[HandleAllFlame] We don't have the necessary Allflame to give to Fairgraves.");
				return;
			}

			if (!_hasLocation)
			{
				var obj = LokiPoe.ObjectManager.CaptainFairgraves;
				if (obj != null)
				{
					if (obj.IsTargetable)
					{
						_location = ExilePather.FastWalkablePositionFor(obj);
						_hasLocation = true;
						Log.InfoFormat("[HandleAllFlame] Captain Fairgraves has been found around {0}.", _location);
					}
					else
					{
						_skip = true;
						Log.InfoFormat(
							"[HandleAllFlame] Captain Fairgraves has been found around {0}, but we don't need to interact with him.",
							_location);
					}
				}
				else if(!_hasLocation2)
				{
					_location2 = StaticLocationManager.GetStaticLocation("Captain Fairgraves");
					_hasLocation2 = true;
				}
			}
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[HandleAllFlame] Now resetting task state.");
			_skip = false;
			_hasLocation = false;
			_location = Vector2i.Zero;
			_location2 = Vector2i.Zero;
		}
	}
}