﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Common.MVVM;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.NativeWrappers;
using Loki.Game.Objects;
using Loki.Game.Objects.Items;
using Newtonsoft.Json;

namespace OldGrindBot
{
	/// <summary>A class that holds useful area state information for bots.</summary>
	public class AreaStateCache
	{
		/// <summary>
		/// When set to true, AreaStateCache will not automatically create a GridExplorer instance for the current area.
		/// </summary>
		public static bool DisableDefaultExplorer;

		/// <summary>Should items be looted based on being visible?</summary>
		public static bool LootVisibleItemsOverride
		{
			get
			{
				return _lootVisibleItemsOverride;
			}
			set
			{
				_lootVisibleItemsOverride = value;
				Log.InfoFormat("[AreaStateCache] LootVisibleItemsOverride = {0}.", _lootVisibleItemsOverride);
			}
		}

		public static bool _lootVisibleItemsOverride = false;

		/// <summary> </summary>
		public class OnLocationAddedEventArgs : EventArgs
		{
			/// <summary>The location being added.</summary>
			public Location Location { get; private set; }

			/// <summary>
			/// 
			/// </summary>
			/// <param name="location"></param>
			internal OnLocationAddedEventArgs(Location location)
			{
				Location = location;
			}
		}

		/// <summary> </summary>
		public class OnChestLocationAddedEventArgs : EventArgs
		{
			/// <summary>The location being added.</summary>
			public ChestLocation ChestLocation { get; private set; }

			/// <summary>
			/// 
			/// </summary>
			/// <param name="location"></param>
			internal OnChestLocationAddedEventArgs(ChestLocation location)
			{
				ChestLocation = location;
			}
		}

		private static readonly Dictionary<uint, AreaStateCache> AreaStates = new Dictionary<uint, AreaStateCache>();

		/// <summary>
		/// The current AreaStateCache for the instance we are in.
		/// </summary>
		public static AreaStateCache Current
		{
			get
			{
				var hash = LokiPoe.LocalData.AreaHash;

				AreaStateCache state;
				if (!AreaStates.TryGetValue(hash, out state))
				{
					state = new AreaStateCache(hash);

					AreaStates.Add(hash, state);

					if (BotManager.IsRunning)
						state.OnStart();
				}

				return state;
			}
		}

		/// <summary>
		/// A wrapper class to hold an item location entry
		/// </summary>
		public class ItemLocation
		{
			/// <summary> </summary>
			public int Id;

			/// <summary> </summary>
			public string Name;

			/// <summary>The position of this location.</summary>
			public Vector2i Position;

			/// <summary>The cached rarity of the item stored by this location.</summary>
			public Rarity Rarity;

			/// <summary>The cached metadata type of the item stored by this location.</summary>
			public string Metadata;
		}

		/// <summary>
		/// A wrapper class to hold a location entry
		/// </summary>
		public class Location
		{
			/// <summary> </summary>
			public int Id;

			/// <summary> </summary>
			public string Name;

			/// <summary> </summary>
			public Vector2i Position;
		}

		/// <summary>
		/// A wrapper class to hold a cached chest location entry
		/// </summary>
		public class ChestLocation
		{
			/// <summary>Has this chest been blacklisted?</summary>
			public bool IsBlacklisted => Blacklist.Contains(Id);

			/// <summary>The chest's id.</summary>
			public int Id;

			/// <summary>The chest's name.</summary>
			public string Name;

			/// <summary>The chest's type.</summary>
			public string Metadata;

			/// <summary>The chest's position.</summary>
			public Vector2i Position;

			/// <summary>Is the chest corrupted.</summary>
			public bool IsCorrupted;

			/// <summary>Is the chest able to be targeted.</summary>
			public bool IsTargetable;

			/// <summary>Is the chest opened.</summary>
			public bool IsOpened;

			/// <summary>Is the chest locked.</summary>
			public bool IsLocked;

			/// <summary>Is the chest a vaal vessel.</summary>
			public bool IsVaalVessel;

			/// <summary>Is the chest a strongbox.</summary>
			public bool IsStrongBox;

			/// <summary>Is this chest breakable.</summary>
			public bool IsBreakable;

			/// <summary>Does the chest open on damage.</summary>
			public bool OpensOnDamage;

			/// <summary>The chest's rarity.</summary>
			public Rarity Rarity;

			/// <summary>Is the chest identified.</summary>
			public bool IsIdentified;

			/// <summary>A list of chest stats.</summary>
			public List<KeyValuePair<StatTypeGGG, int>> Stats;

			/// <summary>
			/// Returns the chest object associated with this location. If it is not in view, it will return null.
			/// </summary>
			public Chest Chest
			{
				get
				{
					return LokiPoe.ObjectManager.GetObjectById(Id) as Chest;
				}
			}

			/// <summary>User data for how many tries we have opened the chest.</summary>
			public int OpenAttempts;
		}

		static AreaStateCache()
		{
			ThrottleMs = 500;
			ChestThrottleMs = 250;
			QuestThrottleMs = 500;
			ItemThrottleMs = 25;
			CorruptedAreaParentId = "";
		}

		/// <summary>The rate at which to Tick. Defaults to 500 ms.</summary>
		public static int ThrottleMs { get; set; }

		/// <summary>The rate at which to Tick quest logic. Defaults to 500 ms.</summary>
		public static int QuestThrottleMs { get; set; }

		/// <summary>The rate at which to Tick chest logic. Defaults to 500 ms.</summary>
		public static int ChestThrottleMs { get; set; }

		/// <summary>The rate at which to Tick item logic. Defaults to 100 ms.</summary>
		public static int ItemThrottleMs { get; set; }

		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		/// <summary>A list of locations that are deemed useful. </summary>
		private readonly List<Location> _locations = new List<Location>();

		/// <summary>A list of cached chest locations. </summary>
		private readonly Dictionary<int, ChestLocation> _chestLocations = new Dictionary<int, ChestLocation>();

		/// <summary>A list of cached item locations. </summary>
		private readonly Dictionary<int, ItemLocation> _itemLocations = new Dictionary<int, ItemLocation>();

		/// <summary>Items we have processed for filtering, and don't need to again.</summary>
		private readonly Dictionary<int, Vector2i> _ignoreItems = new Dictionary<int, Vector2i>();

		/// <summary>Do we need to run waypoint logic.</summary>
		private bool _updateCheckForWaypoint = true;

		/// <summary>Do we need to run area transition logic.</summary>
		// ReSharper disable once ConvertToConstant.Local
		// ReSharper disable once FieldCanBeMadeReadOnly.Local
		private bool _updateAreaTransition = true;

		/// <summary>Should we check for area transitions.</summary>
		public bool ShouldCheckForAreaTransition { get; private set; }

		/// <summary>Should we check for waypoints.</summary>
		public bool ShouldCheckForWaypoint { get; private set; }

		/// <summary>Should we check for stash (town).</summary>
		public bool ShouldCheckForStash { get; private set; }

		/// <summary>Do we have the stash location for this area.</summary>
		public bool HasStashLocation { get; private set; }

		/// <summary>Do we have the waypoint location for this area.</summary>
		public bool HasWaypointLocation { get; private set; }

		/// <summary>Do we have the waypoint entry for this area.</summary>
		public bool HasWaypointEntry { get; private set; }

		/// <summary>Do we have this quest object's location for this area.</summary>
		public bool HasKaruiSpiritLocation { get; private set; }

		/// <summary>A list of seen area transition names for this area.</summary>
		public List<string> SeenAreaTransitions { get; private set; }

		/// <summary>The starting area transition;s name found when we entered the area.</summary>
		public string StartingAreaTransitionName { get; private set; }

		/// <summary>The starting area transition's location found when we entered the area.</summary>
		public Vector2i StartingAreaTransitionLocation { get; private set; }

		/// <summary>The starting portal's location found when we entered the area.</summary>
		public Vector2i StartingPortalLocation { get; private set; }

		/// <summary>The parent area id of the corrupted area.</summary>
		public static string CorruptedAreaParentId { get; set; }

		/// <summary>
		/// The explorer for the current area.
		/// </summary>
		public IExplorer Explorer { get; set; }

		/// <summary> </summary>
		public uint Hash { get; private set; }

		private readonly Stopwatch _throttle = new Stopwatch();
		private readonly Stopwatch _chestThrottle = new Stopwatch();
		private readonly Stopwatch _questThrottle = new Stopwatch();
		private readonly Stopwatch _itemThrottle = new Stopwatch();

		private readonly Stopwatch _timeInInstance = new Stopwatch();
		private readonly Stopwatch _timeInArea = new Stopwatch();

		private bool _hasAnchorPoint;
		private Vector2i _anchorPoint;
		private Vector2i _currentAnchorPoint;
		private uint _anchorPointSeed;

		private bool? _hasBurningGround;
		private bool? _hasLightningGround;
		private bool? _hasIceGround;

		/// <summary>
		/// The world area entry for this area.
		/// </summary>
		public DatWorldAreaWrapper WorldArea { get; private set; }

		/// <summary></summary>
		private static readonly Dictionary<string, bool> NewInstanceOverrides = new Dictionary<string, bool>();

		/// <summary>
		/// Does this area have the burning ground effect?
		/// </summary>
		public bool HasBurningGround
		{
			get
			{
				if (_hasBurningGround == null)
				{
					_hasBurningGround = false;

					int val;
					if (LokiPoe.LocalData.MapMods.TryGetValue(StatTypeGGG.MapBaseGroundFireDamageToDealPerMinute, out val))
					{
						if (val != 0)
						{
							_hasBurningGround = true;
						}
					}
				}
				return _hasBurningGround.Value;
			}
		}

		/// <summary>
		/// Does this area have the lightning ground effect?
		/// </summary>
		public bool HasLightningGround
		{
			get
			{
				if (_hasLightningGround == null)
				{
					_hasLightningGround = false;

					int val;
					if (LokiPoe.LocalData.MapMods.TryGetValue(StatTypeGGG.MapGroundLightning, out val))
					{
						if (val != 0)
						{
							_hasLightningGround = true;
						}
					}
				}
				return _hasLightningGround.Value;
			}
		}

		/// <summary>
		/// Does this area have the ice ground effect?
		/// </summary>
		public bool HasIceGround
		{
			get
			{
				if (_hasIceGround == null)
				{
					_hasIceGround = false;

					int val;
					if (LokiPoe.LocalData.MapMods.TryGetValue(StatTypeGGG.MapGroundIce, out val))
					{
						if (val != 0)
						{
							_hasIceGround = true;
						}
					}
				}
				return _hasIceGround.Value;
			}
		}

		/// <summary>
		/// The initial starting position for this area.
		/// </summary>
		public Vector2i AnchorPoint
		{
			get
			{
				return _anchorPoint;
			}
		}

		/// <summary>
		/// The current starting position for this area (can be changed).
		/// </summary>
		public Vector2i CurrentAnchorPoint
		{
			get
			{
				return _currentAnchorPoint;
			}
		}

		/// <summary>
		/// Updates the anchor point as the current player's position. 
		/// This is required for local area transitions.
		/// </summary>
		public void ResetCurrentAnchorPoint()
		{
			_currentAnchorPoint = LokiPoe.MyPosition;
			Log.DebugFormat("[ResetAnchorPoint] Setting CurrentAnchorPoint to {0} for {1}.", _currentAnchorPoint,
				_anchorPointSeed);
		}

		private void ResetAnchorPoint()
		{
			_anchorPoint = LokiPoe.MyPosition;
			_hasAnchorPoint = true;
			_anchorPointSeed = LokiPoe.LocalData.AreaHash;
			Log.DebugFormat("[ResetAnchorPoint] Setting AnchorPoint to {0} for {1}.", _anchorPoint, _anchorPointSeed);
		}

		/// <summary>Returns how long the character has been in this instance.</summary>
		public TimeSpan TimeInInstance
		{
			get
			{
				return _timeInInstance.Elapsed;
			}
		}

		/// <summary>Returns how long the character has been in the current area.</summary>
		public TimeSpan TimeInArea
		{
			get
			{
				return _timeInArea.Elapsed;
			}
		}

		/// <summary>
		/// Resets the time in the area.
		/// </summary>
		public void ResetTimeInArea()
		{
			if (_timeInArea.IsRunning)
			{
				_timeInArea.Restart();
			}
			else
			{
				_timeInArea.Reset();
			}
		}

		private AreaStateCache(uint hash)
		{
			Hash = hash;

			WorldArea = LokiPoe.CurrentWorldArea;

			ShouldCheckForWaypoint = LokiPoe.CurrentWorldArea.HasWaypoint;

			ShouldCheckForStash = LokiPoe.CurrentWorldArea.IsTown;
			HasStashLocation = false;

			HasWaypointLocation = false;

			var areaId = WorldArea.Id;
			HasWaypointEntry = LokiPoe.InstanceInfo.AvailableWaypoints.ContainsKey(areaId);

			SeenAreaTransitions = new List<string>();

			//GenerateStaticLocations();

			ResetStartingAreaTransition();

			var portal = LokiPoe.ObjectManager.GetObjectsByType<Portal>().OrderBy(a => a.Distance).FirstOrDefault();
			StartingPortalLocation = portal != null
				? ExilePather.FastWalkablePositionFor(portal)
				: Vector2i.Zero;

			if (!DisableDefaultExplorer)
			{
				var ge = new GridExplorer
				{
					AutoResetOnAreaChange = false
				};
				Explorer = ge;
			}
		}

		/// <summary>
		/// Used to reset the starting area transition. This is for handling local area transitions.
		/// </summary>
		public void ResetStartingAreaTransition()
		{
			var at = LokiPoe.ObjectManager.GetObjectsByType<AreaTransition>().OrderBy(a => a.Distance).FirstOrDefault();
			StartingAreaTransitionName = at != null ? at.Name : "";
			StartingAreaTransitionLocation = at != null
				? ExilePather.FastWalkablePositionFor(at)
				: Vector2i.Zero;
		}

		/// <summary>The bot Start event.</summary>
		public static void Start()
		{
			foreach (var kvp in AreaStates)
			{
				kvp.Value.OnStart();
			}
			ItemEvaluator.OnRefreshed += ItemEvaluatorOnOnRefresh;
		}

		private static void ItemEvaluatorOnOnRefresh(object sender,
			ItemEvaluatorRefreshedEventArgs itemEvaluatorRefreshedEventArgs)
		{
			Log.InfoFormat("[ItemEvaluatorOnOnRefresh] Now clearing ignored items.");
			Current._ignoreItems.Clear();
		}

		/// <summary>
		/// Removes an item location.
		/// </summary>
		/// <param name="location"></param>
		public void RemoveItemLocation(ItemLocation location)
		{
			Log.InfoFormat("[RemoveItemLocation] The location {0} [{1}] is being removed.", location.Id, location.Name);
			_itemLocations.Remove(location.Id);
		}

		/// <summary> </summary>
		private void OnStart()
		{
			_updateCheckForWaypoint = true;
			_throttle.Reset();
			_chestThrottle.Reset();
			_questThrottle.Reset();
			_itemThrottle.Reset();
			if (Explorer != null)
			{
				Explorer.Start();
			}
		}

		/// <summary>The bot Tick event.</summary>
		public static void Tick()
		{
			if (!LokiPoe.IsInGame)
				return;

			var unload = new List<uint>();

			Current.OnTick();
			foreach (var kvp in AreaStates)
			{
				if (kvp.Value != Current)
				{
					if (kvp.Value.OnInactiveTick())
					{
						unload.Add(kvp.Key);
					}
				}
			}

			// Remove memory from unloaded areas.
			foreach (var id in unload)
			{
				AreaStates.Remove(id);
			}
		}

		/// <summary></summary>
		private bool OnInactiveTick()
		{
			if (_timeInInstance.IsRunning)
				_timeInInstance.Stop();

			if (_timeInArea.IsRunning)
				_timeInArea.Stop();

			// Unload other instances of the same area we're currently in.
			if (WorldArea == Current.WorldArea)
			{
				if (Explorer != null)
				{
					Explorer.Unload();
				}
				return true;
			}

			// If we're in an area of the same type, but a different area, we can remove it to avoid memory stacking up.
			if ((WorldArea.IsOverworldArea && Current.WorldArea.IsOverworldArea) ||
				(WorldArea.IsMap && Current.WorldArea.IsMap) ||
				(WorldArea.IsTown && Current.WorldArea.IsTown) ||
				(WorldArea.IsCorruptedArea && Current.WorldArea.IsCorruptedArea) ||
				(WorldArea.IsRelicArea && Current.WorldArea.IsRelicArea) ||
				(WorldArea.IsDenArea && Current.WorldArea.IsDenArea) ||
				(WorldArea.IsDailyArea && Current.WorldArea.IsDailyArea) ||
				(WorldArea.IsMissionArea && Current.WorldArea.IsMissionArea) ||
				(WorldArea.IsHideoutArea && Current.WorldArea.IsHideoutArea)
				)
			{
				if (Explorer != null)
				{
					Explorer.Unload();
				}
				return true;
			}

			return false;
		}

		private void OnTick()
		{
			if (Explorer != null)
			{
				Explorer.Tick();
			}

			var update = false;
			if (!_itemThrottle.IsRunning)
			{
				_itemThrottle.Start();
				update = true;
			}
			else
			{
				if (_itemThrottle.ElapsedMilliseconds >= ItemThrottleMs)
				{
					update = true;
				}
			}

			//using (new PerformanceTimer("Tick::WorldItem", 1))
			{
				if (update)
				{
					var myPos = LokiPoe.MyPosition;

					var added = 0;
					foreach (var worldItem in LokiPoe.ObjectManager.GetObjectsByType<WorldItem>())
					{
						var doAdd = false;

						Vector2i pos;
						if (!_ignoreItems.TryGetValue(worldItem.Id, out pos))
						{
							doAdd = true;
						}
						else
						{
							if (pos != worldItem.Position)
							{
								Log.InfoFormat("[AreaStateCache] An item collision has been detected! Item id {0}.", worldItem.Id);
								_ignoreItems.Remove(worldItem.Id);
								doAdd = true;
							}
						}

						if (doAdd)
						{
							if (added > 10)
								break;

							++added;

							var item = worldItem.Item;

							if (worldItem.IsAllocatedToOther)
							{
								if (DateTime.Now < worldItem.PublicTime)
								{
									//Log.InfoFormat("[AreaStateCache] The item {0} is not being marked for pickup because it is allocated to another player.", item.FullName);
									//_ignoreItems.Add(worldItem.Id, worldItem.Position);
									continue;
								}
							}

							var visibleOverride = false;
							if (LootVisibleItemsOverride)
							{
								// We can only consider items when they are visible, otherwise we ignore stuff we might want.
								if(!LokiPoe.ConfigManager.IsAlwaysHighlightEnabled)
									continue;

								if (LokiPoe.Input.GetClickableHighlightLabelPosition(worldItem) != Vector2.Zero)
								{
									visibleOverride = true;
								}
							}

							IItemFilter filter = null;
							if (visibleOverride || ItemEvaluator.Match(item, EvaluationType.PickUp, out filter))
							{
								var location = new ItemLocation
								{
									Id = worldItem.Id,
									Name = worldItem.Name,
									Position = worldItem.Position,
									Rarity = worldItem.Item.Rarity,
									Metadata = worldItem.Item.Metadata
								};

								if (_itemLocations.ContainsKey(location.Id))
								{
									_itemLocations[location.Id] = location;
								}
								else
								{
									_itemLocations.Add(location.Id, location);
								}

								Log.InfoFormat("[AreaStateCache] The location {0} [{1}] is being added from filter [{3}].{2}", location.Id,
									location.Name,
									worldItem.HasAllocation ? " [Allocation " + worldItem.PublicTime + "]" : "",
									filter != null ? filter.Name : "(null)");
							}

							_ignoreItems.Add(worldItem.Id, worldItem.Position);
						}
					}

					var toRemove = new List<int>();
					foreach (var kvp in _itemLocations)
					{
						if (Blacklist.Contains(kvp.Key))
						{
							Log.InfoFormat("[AreaStateCache] The location {0} [{1}] is being removed because the id has been Blacklisted.",
								kvp.Value.Id, kvp.Value.Name);
							toRemove.Add(kvp.Value.Id);
						}
						else if (myPos.Distance(kvp.Value.Position) < 30)
						{
							if (LokiPoe.ObjectManager.GetObjectById<WorldItem>(kvp.Value.Id) == null)
							{
								Log.InfoFormat("[AreaStateCache] The location {0} [{1}] is being removed because the WorldItem does not exist.",
									kvp.Value.Id, kvp.Value.Name);
								toRemove.Add(kvp.Value.Id);
							}
						}
					}

					foreach (var id in toRemove)
					{
						_itemLocations.Remove(id);
					}

					_itemThrottle.Restart();
				}
			}

			if (!_chestThrottle.IsRunning)
			{
				_chestThrottle.Start();
			}
			else
			{
				if (_chestThrottle.ElapsedMilliseconds >= ChestThrottleMs)
				{
					//using (new PerformanceTimer("Tick::Chest", 1))
					{
						var addedChests = new List<ChestLocation>();
						foreach (var chest in LokiPoe.ObjectManager.GetObjectsByType<Chest>().ToList())
						{
							ChestLocation location;
							if (!_chestLocations.TryGetValue(chest.Id, out location))
							{
								location = new ChestLocation
								{
									Id = chest.Id,
									Name = chest.Name,
									IsTargetable = chest.IsTargetable,
									IsOpened = chest.IsOpened,
									IsStrongBox = chest.IsStrongBox,
									IsVaalVessel = chest.IsVaalVessel,
									OpensOnDamage = chest.OpensOnDamage,
									Position = chest.Position,
									Stats = chest.Stats.ToList(),
									IsIdentified = chest.IsIdentified,
									IsBreakable = chest.OpensOnDamage,
									Rarity = chest.Rarity,
									Metadata = chest.Type
								};

								_chestLocations.Add(location.Id, location);

								addedChests.Add(location);
							}

							if (!location.IsOpened)
							{
								location.IsOpened = chest.IsOpened;
								location.IsLocked = chest.IsLocked;
								location.IsTargetable = chest.IsTargetable;
								// Support for chests that change locked state, without the lock state updating.
								var tc = chest.Components.TransitionableComponent;
								if (tc != null)
								{
									if ((tc.Flag1 & 2) != 0)
									{
										location.IsLocked = false;
									}
								}
								if (chest.IsVaalVessel)
								{
									location.IsLocked = false;
								}
								if (!location.IsCorrupted && chest.IsCorrupted)
								{
									location.IsCorrupted = chest.IsCorrupted;
									location.Stats = chest.Stats.ToList();
								}
								if (!location.IsIdentified && chest.IsIdentified)
								{
									location.IsIdentified = chest.IsIdentified;
									location.Stats = chest.Stats.ToList();
								}
							}

							if (addedChests.Count > 10)
								break;
						}

						foreach (var location in addedChests)
						{
							if (!location.IsBreakable)
							{
								location.Position = ExilePather.FastWalkablePositionFor(location.Position);
							}

							LokiPoe.InvokeEvent(OnChestLocationAdded, null, new OnChestLocationAddedEventArgs(location));
						}

						addedChests.Clear();

						_chestThrottle.Restart();
					}
				}
			}

			if (!_questThrottle.IsRunning)
			{
				_questThrottle.Start();
			}
			else
			{
				if (_questThrottle.ElapsedMilliseconds >= QuestThrottleMs)
				{
					if (LokiPoe.CurrentWorldArea.IsMissionArea)
					{
						if (!HasKaruiSpiritLocation)
						{
							var obj = LokiPoe.ObjectManager.GetObjectByName("Karui Spirit");
							if (obj != null)
							{
								AddLocation(ExilePather.FastWalkablePositionFor(obj), obj.Id, obj.Name);
								HasKaruiSpiritLocation = true;
							}
						}
					}

					_questThrottle.Restart();
				}
			}

			if (!_throttle.IsRunning)
			{
				_throttle.Start();
			}
			else
			{
				if (_throttle.ElapsedMilliseconds >= ThrottleMs)
				{
					if (!_timeInInstance.IsRunning)
						_timeInInstance.Start();

					if (!_timeInArea.IsRunning)
						_timeInArea.Start();

					// Do we need to update wp state flags.
					if (_updateCheckForWaypoint)
					{
						// If the current area doesn't have a wp, we do not want to do any more logic processing.
						if (!LokiPoe.CurrentWorldArea.HasWaypoint)
						{
							_updateCheckForWaypoint = false;
							ShouldCheckForWaypoint = false;
							HasWaypointLocation = false;
							HasWaypointEntry = false;
						}
						else
						{
							ShouldCheckForWaypoint = true;
						}
					}

					// Do we need to update at state flags.
					if (_updateAreaTransition)
					{
						ShouldCheckForAreaTransition = true;
					}

					if (ShouldCheckForStash)
					{
						//using (new PerformanceTimer("ShouldCheckForStash", 1))
						{
							if (!HasStashLocation)
							{
								var stash = LokiPoe.ObjectManager.Stash;
								if (stash != null)
								{
									// Save the location so we know where it is when the entity isn't in view.
									AddLocation(ExilePather.FastWalkablePositionFor(stash), stash.Id, "Stash");

									// We now have the waypoint location.
									HasStashLocation = true;
								}
							}
							else
							{
								ShouldCheckForStash = false;
							}
						}
					}

					// If we need to handle wps.
					if (ShouldCheckForWaypoint)
					{
						//using (new PerformanceTimer("ShouldCheckForWaypoint", 1))
						{
							// If we don't have the wp location yet, check to see if we see one.
							if (!HasWaypointLocation)
							{
								var wp = LokiPoe.ObjectManager.Waypoint;
								if (wp != null)
								{
									// Save the location so we know where it is when the entity isn't in view.
									AddLocation(ExilePather.FastWalkablePositionFor(wp), wp.Id, "Waypoint");

									// We now have the waypoint location.
									HasWaypointLocation = true;
								}
							}

							// If we don't have the wp entry yet, poll for us having it now.
							// But only if we've seen the waypoint, since otherwise there's no way we have it.
							if (HasWaypointLocation && !HasWaypointEntry)
							{
								var areaId = WorldArea.Id;
								HasWaypointEntry = LokiPoe.InstanceInfo.AvailableWaypoints.ContainsKey(areaId);
							}

							// Once we have both the location and the entry, we do not need to execute wp logic anymore.
							if (HasWaypointLocation && HasWaypointEntry)
							{
								_updateCheckForWaypoint = false;
								ShouldCheckForWaypoint = false;
							}
						}
					}

					// If we need to handle ats.
					if (ShouldCheckForAreaTransition)
					{
						//using (new PerformanceTimer("ShouldCheckForAreaTransition", 1))
						{
							// If there are any area transitions on screen, add them if we don't already know of them.
							foreach (var transition in LokiPoe.ObjectManager.Objects.OfType<AreaTransition>().ToList())
							{
								var name = transition.Name;

								// We have to check all this in order to handle the areas that have transitions with the same name, but different
								// entity ids.
								if (HasLocation(name, transition.Id))
								{
									continue;
								}

								AddLocation(ExilePather.FastWalkablePositionFor(transition), transition.Id, name);

								if (!SeenAreaTransitions.Contains(name))
								{
									SeenAreaTransitions.Add(name);
								}
							}
						}
					}

					// Check to see if we need a new anchor point to kite back towards.
					if (!_hasAnchorPoint || LokiPoe.LocalData.AreaHash != _anchorPointSeed)
					{
						ResetAnchorPoint();
						ResetCurrentAnchorPoint();
					}

					_throttle.Restart();
				}
			}
		}

		/// <summary>The bot Stop event.</summary>
		public static void Stop()
		{
			foreach (var kvp in AreaStates)
			{
				kvp.Value.OnStop();
			}

			ItemEvaluator.OnRefreshed -= ItemEvaluatorOnOnRefresh;
		}

		/// <summary> </summary>
		private void OnStop()
		{
			_throttle.Reset();
			_chestThrottle.Reset();
			_timeInInstance.Stop();
			_timeInArea.Stop();
			if (Explorer != null)
			{
				Explorer.Stop();
			}
		}

		/// <summary>An event handler for when a location is added.</summary>
		public static event EventHandler<OnLocationAddedEventArgs> OnLocationAdded;

		/// <summary>An event handler for when a chest location is added.</summary>
		public static event EventHandler<OnChestLocationAddedEventArgs> OnChestLocationAdded;

		/// <summary>
		/// This function will add a location that we can reference later. An example of a location
		/// would be area transitions or NPCs which can despawn.
		/// </summary>
		public void AddLocation(Vector2i position, int id, string name)
		{
			var location = new Location
			{
				Position = position,
				Id = id,
				Name = name
			};
			_locations.Add(location);

			Log.DebugFormat("Adding location [\"{0}\"][{1}] = {2} for area [0x{3:X}]", location.Name,
				location.Id, location.Position, Hash);

			LokiPoe.InvokeEvent(OnLocationAdded, null, new OnLocationAddedEventArgs(location));
		}

		/// <summary>
		/// Clears the list of locations.
		/// </summary>
		public void ClearLocations()
		{
			_locations.Clear();
		}

		/// <summary>
		/// Clears the list of chest locations.
		/// </summary>
		public void ClearChestLocations()
		{
			_chestLocations.Clear();
		}

		/// <summary>
		/// Clears the list of item locations.
		/// </summary>
		public void ClearItemLocations()
		{
			foreach (var kvp in _itemLocations)
			{
				Log.InfoFormat("[ClearItemLocations] The location {0} [{1}] is being removed.", kvp.Value.Id, kvp.Value.Name);
			}

			_itemLocations.Clear();
			_ignoreItems.Clear();
		}

		/// <summary>
		/// Returns an IEnumerable of chest locations.
		/// </summary>
		public IEnumerable<ChestLocation> ChestLocations
		{
			get
			{
				return _chestLocations.Select(kvp => kvp.Value);
			}
		}

		/// <summary>
		/// Returns an IEnumerable of item locations.
		/// </summary>
		public IEnumerable<ItemLocation> ItemLocations
		{
			get
			{
				return _itemLocations.Select(kvp => kvp.Value);
			}
		}

		/// <summary>
		/// Returns the list of locations in the specified area that have the name.
		/// </summary>
		public IEnumerable<Location> GetLocations(string name)
		{
			return _locations.Where(l => l.Name == name);
		}

		/// <summary>
		/// Returns true if the specified area hash has the location by name.
		/// </summary>
		public bool HasLocation(string name)
		{
			return _locations.Any(l => l.Name == name);
		}

		/// <summary>
		/// Returns true if the specified area hash has the location by name.
		/// </summary>
		public bool HasLocation(string name, int id)
		{
			return _locations.Any(l => l.Name == name && l.Id == id);
		}

		/// <summary>
		/// A function to set the new instance override for a specific area by id.
		/// </summary>
		/// <param name="id">The area's id.</param>
		/// <param name="value">The new instance override flag.</param>
		public static void SetNewInstanceOverride(string id, bool value)
		{
			bool flag;
			if (NewInstanceOverrides.TryGetValue(id, out flag))
			{
				NewInstanceOverrides[id] = value;
			}
			else
			{
				NewInstanceOverrides.Add(id, value);
			}
			Log.InfoFormat("[SetNewInstanceOverride] {0} = {1}", id, value);
		}

		/// <summary>
		/// A function to get the new instance override for a specific area by id.
		/// </summary>
		/// <param name="id">The area's id.</param>
		/// <param name="flag">The area's set value.</param>
		/// <returns>Returns true if there is a new instance override and false otherwise.</returns>
		public static bool HasNewInstanceOverride(string id, out bool flag)
		{
			if (NewInstanceOverrides.TryGetValue(id, out flag))
			{
				return true;
			}
			return false;
		}

		/// <summary>
		/// Removes the new instance override for a specific area by id.
		/// </summary>
		/// <param name="id">The area's id.</param>
		public static void RemoveNewInstanceOverride(string id)
		{
			NewInstanceOverrides.Remove(id);
			Log.InfoFormat("[RemoveNewInstanceOverride] {0}", id);
		}
	}

	/// <summary>
	/// A wrapper class for settings bindings.
	/// </summary>
	public class SettingNameEnabledWrapper : NotificationObject
	{
		private bool _isEnabled;
		private string _name;

		/// <summary>
		/// Is this setting enabled.
		/// </summary>
		public bool IsEnabled
		{
			get
			{
				return _isEnabled;
			}
			set
			{
				if (value.Equals(_isEnabled))
				{
					return;
				}
				_isEnabled = value;
				NotifyPropertyChanged(() => IsEnabled);
			}
		}

		/// <summary>
		/// The name of this setting.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				if (value == _name)
				{
					return;
				}
				_name = value;
				NotifyPropertyChanged(() => Name);
			}
		}
	}

	/// <summary>
	/// A delegate for triggering an item being looted.
	/// </summary>
	/// <param name="item">The item about to be processed.</param>
	public delegate void TriggerOnLootDelegate(WorldItem item);

	/// <summary>
	/// A delegate for triggering an item being stashed.
	/// </summary>
	/// <param name="item">The item about to be processed.</param>
	public delegate void TriggerOnStashDelegate(Item item);

	/// <summary>
	/// A delegate for triggering an item being withdrawn.
	/// </summary>
	/// <param name="item">The item about to be processed.</param>
	public delegate void TriggerOnWithdrawDelegate(Item item);

	/// <summary>
	/// A delegate for triggering an item being sold.
	/// </summary>
	/// <param name="item">The item about to be processed.</param>
	public delegate void TriggerOnSellDelegate(Item item);

	/// <summary>A class for commonly used coroutines.</summary>
	public static class CoroutinesV3
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();
		
		/// <summary>
		/// Opens the skills panel and will NOT close the skill reset dialog if present.
		/// </summary>
		/// <returns>True on success and false on failure.</returns>
		public static async Task<bool> OpenSkillsPanel(int timeout = 5000)
		{
			Log.DebugFormat("[OpenSkillsPanel]");

			var sw = Stopwatch.StartNew();

			// Make sure we close all blocking windows so we can actually open the inventory.
			if (!LokiPoe.InGameState.SkillsUi.IsOpened)
			{
				await Coroutines.CloseBlockingWindows();
			}

			// Open the passive skills panel like a player would.
			while (!LokiPoe.InGameState.SkillsUi.IsOpened)
			{
				Log.DebugFormat("[OpenSkillsPanel] The SkillsPanel is not opened. Now opening it.");

				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[OpenSkillsPanel] Timeout.");
					return false;
				}

				if (LokiPoe.Me.IsDead)
				{
					Log.ErrorFormat("[OpenSkillsPanel] We are now dead.");
					return false;
				}

				LokiPoe.Input.SimulateKeyEvent(LokiPoe.Input.Binding.open_passive_skills_panel, true, false, false);

				await Coroutines.ReactionWait();
			}

			return true;
		}

		/// <summary>
		/// Opens the social panel.
		/// </summary>
		/// <returns></returns>
		public static async Task<bool> OpenSocialPanel(int timeout = 5000)
		{
			Log.DebugFormat("[OpenSocialPanel]");

			var sw = Stopwatch.StartNew();

			// Make sure we close all blocking windows so we can actually open the inventory.
			if (!LokiPoe.InGameState.SocialUi.IsOpened)
			{
				await Coroutines.CloseBlockingWindows();
			}

			// Open the social panel like a player would.
			while (!LokiPoe.InGameState.SocialUi.IsOpened)
			{
				Log.DebugFormat("[OpenSocialPanel] The SocialPanel is not opened. Now opening it.");

				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[OpenSocialPanel] Timeout.");
					return false;
				}

				if (LokiPoe.Me.IsDead)
				{
					Log.ErrorFormat("[OpenSocialPanel] We are now dead.");
					return false;
				}

				LokiPoe.Input.SimulateKeyEvent(LokiPoe.Input.Binding.open_social_panel, true, false, false);

				await Coroutines.ReactionWait();
			}

			return true;
		}

		/// <summary>
		/// Opens the inventory panel.
		/// </summary>
		/// <returns></returns>
		public static async Task<bool> OpenInventoryPanel(int timeout = 5000)
		{
			Log.DebugFormat("[OpenInventoryPanel]");

			var sw = Stopwatch.StartNew();

			// Make sure we close all blocking windows so we can actually open the inventory.
			if (!LokiPoe.InGameState.InventoryUi.IsOpened)
			{
				await Coroutines.CloseBlockingWindows();
			}

			// Open the inventory panel like a player would.
			while (!LokiPoe.InGameState.InventoryUi.IsOpened)
			{
				Log.DebugFormat("[OpenInventoryPanel] The InventoryUi is not opened. Now opening it.");

				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[OpenInventoryPanel] Timeout.");
					return false;
				}

				if (LokiPoe.Me.IsDead)
				{
					Log.ErrorFormat("[OpenInventoryPanel] We are now dead.");
					return false;
				}

				LokiPoe.Input.SimulateKeyEvent(LokiPoe.Input.Binding.open_inventory_panel, true, false, false);

				await Coroutines.ReactionWait();
			}

			return true;
		}

		/// <summary>
		/// This coroutine waits for the character to not be dead anymore.
		/// </summary>
		/// <param name="timeout">How long to wait before the coroutine fails.</param>
		/// <returns>true on success, and false on failure.</returns>
		public static async Task<bool> WaitForResurrect(int timeout = 5000)
		{
			Log.DebugFormat("[WaitForResurrect]");

			var sw = Stopwatch.StartNew();

			while (LokiPoe.Me.IsDead)
			{
				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[WaitForResurrect] Timeout.");
					return false;
				}

				Log.DebugFormat("[WaitForResurrect] We have been waiting {0} for the character to resurrect.",
					sw.Elapsed);

				await Coroutines.LatencyWait();
			}

			return true;
		}

		/// <summary>
		/// This coroutines waits for the character to change positions from a local area transition.
		/// </summary>
		/// <param name="position">The starting position.</param>
		/// <param name="delta">The change in position required.</param>
		/// <param name="timeout">How long to wait before the coroutine fails.</param>
		/// <returns>true on success, and false on failure.</returns>
		public static async Task<bool> WaitForPositionChange(Vector2i position, int delta = 30, int timeout = 5000)
		{
			Log.DebugFormat("[WaitForPositionChange]");

			var sw = Stopwatch.StartNew();

			while (LokiPoe.MyPosition.Distance(position) < delta)
			{
				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[WaitForLargerPositionChange] Timeout.");
					return false;
				}

				Log.DebugFormat("[WaitForLargerPositionChange] We have been waiting {0} for an area change.", sw.Elapsed);

				await Coroutines.LatencyWait();
			}

			return true;
		}

		/// <summary>
		/// This coroutines waits for the character to change areas.
		/// </summary>
		/// <param name="original">The starting area's hash.</param>
		/// <param name="timeout">How long to wait before the coroutine fails.</param>
		/// <returns>true on success, and false on failure.</returns>
		public static async Task<bool> WaitForAreaChange(uint original, int timeout = 30000)
		{
			Log.DebugFormat("[WaitForAreaChange]");

			var sw = Stopwatch.StartNew();

			while (LokiPoe.LocalData.AreaHash == original)
			{
				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[WaitForAreaChange] Timeout.");
					return false;
				}

				Log.DebugFormat("[WaitForAreaChange] We have been waiting {0} for an area change.", sw.Elapsed);

				await Coroutines.LatencyWait();
				await Coroutine.Sleep(1000);
			}

			return true;
		}

		/// <summary>
		/// This coroutine waits for the instance manager to open.
		/// </summary>
		/// <param name="timeout">How long to wait before the coroutine fails.</param>
		/// <returns>true on success and false on failure.</returns>
		public static async Task<bool> WaitForInstanceManager(int timeout = 1000)
		{
			Log.DebugFormat("[WaitForInstanceManager]");

			var sw = Stopwatch.StartNew();

			while (!LokiPoe.InGameState.InstanceManagerUi.IsOpened)
			{
				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[WaitForInstanceManager] Timeout.");
					return false;
				}

				Log.DebugFormat("[WaitForInstanceManager] We have been waiting {0} for the instance manager to open.",
					sw.Elapsed);

				await Coroutines.LatencyWait();
			}

			return true;
		}

		/// <summary>
		/// This coroutine waits for an area transition to be usable.
		/// </summary>
		/// <param name="name">The name of the area transition.</param>
		/// <param name="timeout">How long to wait before the coroutine fails.</param>
		/// <returns>true on success and false on failure.</returns>
		public static async Task<bool> WaitForAreaTransition(string name, int timeout = 3000)
		{
			Log.DebugFormat("[WaitForAreaTransition]");

			await Coroutines.FinishCurrentAction();

			var sw = Stopwatch.StartNew();

			while (true)
			{
				var at = LokiPoe.ObjectManager.GetObjectByName<AreaTransition>(name);
				if (at != null)
				{
					if (at.IsTargetable)
					{
						break;
					}
				}

				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[WaitForAreaTransition] Timeout.");
					return false;
				}

				Log.DebugFormat(
					"[WaitForAreaTransition] We have been waiting {0} for the area transition {1} to be usable.",
					sw.Elapsed, name);

				await Coroutines.LatencyWait();
			}

			return true;
		}

		/// <summary>
		/// This coroutine waits for the world panel to open after clicking on a waypoint.
		/// </summary>
		/// <param name="timeout">How long to wait before the coroutine fails.</param>
		/// <returns>true on success and false on failure.</returns>
		public static async Task<bool> WaitForWorldPanel(int timeout = 10000)
		{
			Log.DebugFormat("[WaitForWorldPanel]");

			var sw = Stopwatch.StartNew();

			while (!LokiPoe.InGameState.WorldUi.IsOpened)
			{
				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[WaitForWorldPanel] Timeout.");
					return false;
				}

				Log.DebugFormat("[WaitForWorldPanel] We have been waiting {0} for the world panel to open.", sw.Elapsed);

				await Coroutines.ReactionWait();
			}

			return true;
		}

		/// <summary>
		/// This coroutine waits for the npc dialog panel to open after clicking on a npc to interact with it.
		/// </summary>
		/// <param name="timeout">How long to wait before the coroutine fails.</param>
		/// <returns>true on success and false on failure.</returns>
		public static async Task<bool> WaitForNpcDialogPanel(int timeout = 3000)
		{
			Log.DebugFormat("[WaitForNpcDialogPanel]");

			var sw = Stopwatch.StartNew();

			while (!LokiPoe.InGameState.NpcDialogUi.IsOpened)
			{
				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[WaitForNpcDialogPanel] Timeout.");
					return false;
				}

				Log.DebugFormat("[WaitForNpcDialogPanel] We have been waiting {0} for the npc dialog panel to open.", sw.Elapsed);

				await Coroutines.ReactionWait();
			}

			return true;
		}

		/// <summary>
		/// This coroutine waits for the sell panel to open after clicking Sell Items.
		/// </summary>
		/// <param name="timeout">How long to wait before the coroutine fails.</param>
		/// <returns>true on success and false on failure.</returns>
		public static async Task<bool> WaitForSellPanel(int timeout = 5000)
		{
			Log.DebugFormat("[WaitForSellPanel]");

			var sw = Stopwatch.StartNew();

			while (!LokiPoe.InGameState.SellUi.IsOpened)
			{
				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[WaitForSellPanel] Timeout.");
					return false;
				}

				Log.DebugFormat("[WaitForSellPanel] We have been waiting {0} for the sell panel to open.", sw.Elapsed);

				await Coroutines.ReactionWait();
			}

			return true;
		}

		/// <summary>
		/// This coroutine waits for the purchase panel to open after buy interaction.
		/// </summary>
		/// <param name="timeout">How long to wait before the coroutine fails.</param>
		/// <returns>true on success and false on failure.</returns>
		public static async Task<bool> WaitForPurchasePanel(int timeout = 1000)
		{
			Log.DebugFormat("[WaitForPurchasePanel]");

			var sw = Stopwatch.StartNew();

			while (!LokiPoe.InGameState.PurchaseUi.IsOpened)
			{
				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[WaitForPurchasePanel] Timeout.");
					return false;
				}

				Log.DebugFormat("[WaitForPurchasePanel] We have been waiting {0} for the stash panel to open.", sw.Elapsed);

				await Coroutines.ReactionWait();
			}

			return true;
		}

		/// <summary>
		/// This coroutine waits for the stash panel to open after stash interaction.
		/// </summary>
		/// <param name="timeout">How long to wait before the coroutine fails.</param>
		/// <returns>true on success and false on failure.</returns>
		public static async Task<bool> WaitForStashPanel(int timeout = 3000)
		{
			Log.DebugFormat("[WaitForStashPanel]");

			var sw = Stopwatch.StartNew();

			while (!LokiPoe.InGameState.StashUi.IsOpened)
			{
				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[WaitForStashPanel] Timeout.");
					return false;
				}

				Log.DebugFormat("[WaitForStashPanel] We have been waiting {0} for the stash panel to open.", sw.Elapsed);

				await Coroutines.ReactionWait();
			}

			return true;
		}

		/// <summary>
		/// This coroutine waits for the guild stash panel to open after guild stash interaction.
		/// </summary>
		/// <param name="timeout">How long to wait before the coroutine fails.</param>
		/// <returns>true on success and false on failure.</returns>
		public static async Task<bool> WaitForGuildStashPanel(int timeout = 3000)
		{
			Log.DebugFormat("[WaitForGuildStashPanel]");

			var sw = Stopwatch.StartNew();

			while (!LokiPoe.InGameState.GuildStashUi.IsOpened)
			{
				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[WaitForGuildStashPanel] Timeout.");
					return false;
				}

				Log.DebugFormat("[WaitForGuildStashPanel] We have been waiting {0} for the stash panel to open.", sw.Elapsed);

				await Coroutines.ReactionWait();
			}

			return true;
		}

		/// <summary>
		/// This coroutine will create a portal to town and take it. If the process fails, the coroutine will logout.
		/// </summary>
		/// <returns></returns>
		public static async Task CreateAndTakePortalToTown()
		{
			if (await CreatePortalToTown())
			{
				if (await TakeClosestPortal())
				{
					return;
				}
			}

			Log.ErrorFormat(
				"[CreateAndTakePortalToTown] A portal to town could not be made/taken. Now logging out to get back to town.");

			await LogoutToTitleScreen();
		}

		/// <summary>
		/// This coroutine creates a portal to town from a Portal Scroll in the inventory.
		/// </summary>
		/// <returns>true if the Portal Scroll was used and false otherwise.</returns>
		public static async Task<bool> CreatePortalToTown()
		{
			if (LokiPoe.Me.IsInTown)
			{
				Log.ErrorFormat("[CreatePortalToTown] Town portals are not allowed in town.");
				return false;
			}

			/*if (LokiPoe.Me.IsInMapRoom)
			{
				Log.ErrorFormat("[CreatePortalToTown] Town portals are not allowed in the map room.");
				return false;
			}*/

			if (LokiPoe.Me.IsInHideout)
			{
				Log.ErrorFormat("[CreatePortalToTown] Town portals are not allowed in hideouts.");
				return false;
			}

			if (LokiPoe.CurrentWorldArea.IsMissionArea || LokiPoe.CurrentWorldArea.IsDenArea ||
				LokiPoe.CurrentWorldArea.IsRelicArea || LokiPoe.CurrentWorldArea.IsDailyArea)
			{
				Log.ErrorFormat("[CreatePortalToTown] Town Portals are not allowed in mission areas.");
				return false;
			}

			await Coroutines.FinishCurrentAction();

			await Coroutines.CloseBlockingWindows();

			var portalSkill = LokiPoe.InGameState.SkillBarHud.Skills.FirstOrDefault(s => s.Name == "Portal");
			if (portalSkill != null && portalSkill.CanUse(true))
			{
				Log.DebugFormat("[CreatePortalToTown] We have a Portal skill on the skill bar. Now using it.");

				var err = LokiPoe.InGameState.SkillBarHud.Use(portalSkill.Slot, false);
				Log.InfoFormat("[CreatePortalToTown] SkillBarHud.Use returned {0}.", err);

				await Coroutines.LatencyWait();

				await Coroutines.FinishCurrentAction();

				if (err == LokiPoe.InGameState.UseResult.None)
				{
					var sw = Stopwatch.StartNew();
					while (sw.ElapsedMilliseconds < 3000)
					{
						var portal = LokiPoe.ObjectManager.Objects.OfType<Portal>().FirstOrDefault(p => p.Distance < 50);
						if (portal != null)
						{
							return true;
						}

						Log.DebugFormat("[CreatePortalToTown] No portal was detected yet, waiting...");

						await Coroutines.LatencyWait();
					}
				}
			}

			Log.DebugFormat("[CreatePortalToTown] Now opening the inventory panel.");

			// We need the inventory panel open.
			if (!await OpenInventoryPanel())
			{
				return false;
			}

			await Coroutines.ReactionWait();

			Log.DebugFormat("[CreatePortalToTown] Now searching the main inventory for a Portal Scroll.");

			var item =
				LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(
					i => i.Name == "Portal Scroll");
			if (item == null)
			{
				Log.ErrorFormat("[CreatePortalToTown] There are no Portal Scrolls in the inventory.");
				return false;
			}

			Log.DebugFormat("[CreatePortalToTown] Now using the Portal Scroll.");

			var err2 = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.UseItem(item.LocalId);
			if (err2 != UseItemResult.None)
			{
				Log.ErrorFormat("[CreatePortalToTown] UseItem returned {0}.", err2);
				return false;
			}

			await Coroutines.LatencyWait();
			await Coroutines.ReactionWait();

			await Coroutines.CloseBlockingWindows();

			return true;
		}

		/// <summary>
		/// This coroutine will attempt to take a portal
		/// </summary>
		/// <returns>true if the portal was taken, and an area change occurred, and false otherwise.</returns>
		public static async Task<bool> TakeClosestPortal()
		{
			var sw = Stopwatch.StartNew();

			if (LokiPoe.ConfigManager.IsAlwaysHighlightEnabled)
			{
				Log.InfoFormat("[TakeClosestPortal] Now disabling Always Highlight to avoid label issues.");
				LokiPoe.Input.SimulateKeyEvent(LokiPoe.Input.Binding.highlight_toggle, true, false, false);
				await Coroutine.Sleep(16);
			}

			NetworkObject portal = null;
			while (portal == null || !portal.IsTargetable)
			{
				Log.DebugFormat("[TakeClosestPortal] Now waiting for the portal to spawn. {0} elapsed.",
					sw.Elapsed);

				await Coroutines.LatencyWait();

				portal =
					LokiPoe.ObjectManager.GetObjectsByType<Portal>()
						.Where(p => p.Distance < 50)
						.OrderBy(p => p.Distance)
						.FirstOrDefault();

				if (sw.ElapsedMilliseconds > 10000)
				{
					break;
				}
			}

			if (portal == null)
			{
				Log.ErrorFormat("[TakeClosestPortal] A portal was not found.");
				return false;
			}

			var pos = ExilePather.FastWalkablePositionFor(portal);

			Log.DebugFormat("[TakeClosestPortal] The portal was found at {0}.", pos);

			if (!await MoveToLocation(pos, 5, 10000, () => false))
			{
				return false;
			}

			var hash = LokiPoe.LocalData.AreaHash;

			// Try to interact 3 times.
			for (var i = 0; i < 3; i++)
			{
				await Coroutines.FinishCurrentAction();

				Log.DebugFormat("[TakeClosestPortal] The portal to interact with is {0} at {1}.",
					portal.Id, pos);

				if (await InteractWith(portal))
				{
					if (await WaitForAreaChange(hash))
					{
						Log.DebugFormat("[TakeClosestPortal] The portal has been taken.");
						return true;
					}
				}

				await Coroutine.Sleep(1000);
			}

			Log.ErrorFormat(
				"[TakeClosestPortal] We have failed to take the portal 3 times.");

			return false;
		}

		/// <summary>
		/// This coroutine will attempt to take an area transition
		/// </summary>
		/// <returns>true if the area transition was taken, and an area change occurred, and false otherwise.</returns>
		public static async Task<bool> TakeClosestAreaTransition()
		{
			var sw = Stopwatch.StartNew();

			NetworkObject at = null;
			while (at == null)
			{
				Log.DebugFormat("[TakeClosestAreaTransition] Now waiting for the portal to spawn. {0} elapsed.",
					sw.Elapsed);

				await Coroutines.LatencyWait();

				at =
					LokiPoe.ObjectManager.GetObjectsByType<AreaTransition>()
						.Where(
							p =>
								ExilePather.PathDistance(LokiPoe.MyPosition, ExilePather.FastWalkablePositionFor(p)) <
								70)
						.OrderBy(p => p.Distance)
						.FirstOrDefault();

				if (sw.ElapsedMilliseconds > 3000)
				{
					break;
				}
			}

			if (at == null)
			{
				Log.ErrorFormat("[TakeClosestAreaTransition] An area transition was not found.");
				return false;
			}

			var pos = ExilePather.FastWalkablePositionFor(at);

			Log.DebugFormat("[TakeClosestAreaTransition] The portal was found at {0}.", pos);

			if (!await MoveToLocation(pos, 5, 10000, () => false))
			{
				return false;
			}

			// Try to interact 3 times.
			for (var i = 0; i < 3; i++)
			{
				Log.DebugFormat("[TakeClosestAreaTransition] The are transition to interact with is {0} at {1}.",
					at.Id, pos);

				var result = await TakeAreaTransition(at, false, -1);
				if (result != TakeAreaTransitionError.None)
				{
					Log.ErrorFormat("[TakeClosestAreaTransition] TakeAreaTransition returned {0}.", result);
				}
				else
				{
					return true;
				}

				await Coroutine.Sleep(1000);
			}

			Log.ErrorFormat(
				"[TakeClosestAreaTransition] We have failed to take the area transition 3 times.");

			return false;
		}

		/// <summary>
		/// This coroutine will logout the character to character selection.
		/// </summary>
		/// <returns></returns>
		public static async Task LogoutToTitleScreen(int timeout = 5000)
		{
			Log.DebugFormat("[LogoutToTitleScreen] Now logging out to Character Selection screen.");

			var err = LokiPoe.EscapeState.LogoutToTitleScreen();
			if (err != LokiPoe.EscapeState.LogoutError.None)
			{
				Log.ErrorFormat(
					"[LogoutToTitleScreen] EscapeState.LogoutToTitleScreen returned {0}. Now stopping the bot because it cannot continue.");
				BotManager.Stop();
			}

			var sw = Stopwatch.StartNew();

			while (!LokiPoe.IsInLoginScreen)
			{
				Log.DebugFormat("[LogoutToTitleScreen] We have been waiting {0} to get out of game.", sw.Elapsed);

				await Coroutines.ReactionWait();

                if (sw.ElapsedMilliseconds >= timeout)
                {
                    Log.DebugFormat("[LogoutToTitleScreen] Timeout while waiting to logout.");
                    break;
                }
			}
		}

		/// <summary>
		/// This coroutine moves towards a position until it is within the specified stop distance.
		/// </summary>
		/// <param name="position">The position to move ot.</param>
		/// <param name="stopDistance">How close to the location should we get.</param>
		/// <param name="timeout">How long should the coroutine execute for before stopping due to timeout.</param>
		/// <returns></returns>
		public static async Task<bool> MoveToLocation(Vector2i position, int stopDistance, int timeout, Func<bool> stopCondition)
		{
			var sw = Stopwatch.StartNew();
			var dsw = Stopwatch.StartNew();

			var da = (bool)PlayerMover.Execute("GetDoAdjustments");
			PlayerMover.Execute("SetDoAdjustments", false);

			while (LokiPoe.MyPosition.Distance(position) > stopDistance)
			{
				if (LokiPoe.Me.IsDead)
				{
					Log.ErrorFormat("[MoveToLocation] The player is dead.");
					PlayerMover.Execute("SetDoAdjustments", da);
					return false;
				}

				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.ErrorFormat("[MoveToLocation] Timeout.");
					PlayerMover.Execute("SetDoAdjustments", da);
					return false;
				}

				if(stopCondition())
				{
					break;
				}

				if (dsw.ElapsedMilliseconds > 100)
				{
					Log.DebugFormat(
						"[MoveToLocation] Now moving towards {0}. We have been performing this task for {1}.",
						position,
						sw.Elapsed);
					dsw.Restart();
				}

				if (!PlayerMover.MoveTowards(position))
				{
					Log.ErrorFormat("[MoveToLocation] MoveTowards failed for {0}.", position);
					await Coroutines.FinishCurrentAction();
				}

				await Coroutine.Yield();
			}

			PlayerMover.Execute("SetDoAdjustments", da);

			await Coroutines.FinishCurrentAction();

			return true;
		}

		/// <summary>
		/// This coroutine attempts to highlight and interact with an object.
		/// Interaction only takes place if the object is highlighted.
		/// </summary>
		/// <param name="obj">The object to interact with.</param>
		/// <param name="holdCtrl">Should control be held? For area transitions.</param>
		/// <returns>true on success and false on failure.</returns>
		public static async Task<bool> InteractWith(NetworkObject obj, bool holdCtrl = false)
		{
			return await InteractWith<NetworkObject>(obj, holdCtrl);
		}

		/// <summary>
		/// This coroutine attempts to highlight and interact with an object.
		/// Interaction only takes place if the object is highlighted or an object of type T is.
		/// </summary>
		/// <typeparam name="T">The type of object acceptable to be highlighted if the intended target is not highlighted.</typeparam>
		/// <param name="holdCtrl">Should control be held? For area transitions.</param>
		/// <param name="obj">The object to interact with.</param>
		/// <returns>true on success and false on failure.</returns>
		public static async Task<bool> InteractWith<T>(NetworkObject obj, bool holdCtrl = false)
		{
			if (obj == null)
			{
				Log.ErrorFormat("[InteractWith] The object is null.");
				return false;
			}

			var id = obj.Id;

			Log.DebugFormat("[InteractWith] Now attempting to highlight {0}.", id);

			await Coroutines.FinishCurrentAction();

			if (!LokiPoe.Input.HighlightObject(obj))
			{
				Log.ErrorFormat("[InteractWith] The target could not be highlighted.");
				return false;
			}

			var target = LokiPoe.InGameState.CurrentTarget;
			if (target != obj && !(target is T))
			{
				Log.ErrorFormat("[InteractWith] The target highlight has been lost.");
				return false;
			}

			Log.DebugFormat("[InteractWith] Now attempting to interact with {0}.", id);

			if (holdCtrl)
			{
				LokiPoe.ProcessHookManager.SetKeyState(Keys.ControlKey, 0x8000);
			}

			LokiPoe.Input.ClickLMB();

			await Coroutines.LatencyWait();

			await Coroutines.FinishCurrentAction(false);

			LokiPoe.ProcessHookManager.ClearAllKeyStates();

			return true;
		}

		/// <summary>
		/// Errors for the TakeWaypointTo coroutine.
		/// </summary>
		public enum TakeAreaTransitionError
		{
			/// <summary>None, the area transition has been taken to the desired area.</summary>
			None,

			/// <summary>No area transition object was detected.</summary>
			NoAreaTransitions,

			/// <summary>Interaction with the area transition failed.</summary>
			InteractFailed,

			/// <summary>The instance manager panel did not open.</summary>
			InstanceManagerDidNotOpen,

			/// <summary>The JoinNew function failed with an error.</summary>
			JoinNewFailed,

			/// <summary>An area change did not happen after taking the area transition.</summary>
			WaitForAreaChangeFailed,

			/// <summary>Too many instances are listed based on user configuration.</summary>
			TooManyInstances,
		}

		/// <summary>
		/// This coroutine interacts with an area transition in order to change areas. It assumes
		/// you are in interaction range with the area transition itself. It can be used both in town,
		/// and out of town, given the previous conditions are met.
		/// </summary>
		/// <param name="obj">The area transition object to take.</param>
		/// <param name="newInstance">Should a new instance be created.</param>
		/// <param name="isLocal">Is the area transition local? In other words, should the couroutine not wait for an area change.</param>
		/// <param name="maxInstances">The max number of instance entries allowed to Join a new instance or -1 to not check.</param>
		/// <returns>A TakeAreaTransitionError that describes the result.</returns>
		public static async Task<TakeAreaTransitionError> TakeAreaTransition(NetworkObject obj, bool newInstance,
			int maxInstances,
			bool isLocal = false)
		{
			Log.InfoFormat("[TakeAreaTransition] {0} {1} {2}", obj.Name, newInstance ? "(new instance)" : "",
				isLocal ? "(local)" : "");

			await Coroutines.CloseBlockingWindows();

			await Coroutines.FinishCurrentAction();

			var hash = LokiPoe.LocalData.AreaHash;
			var pos = LokiPoe.MyPosition;

			if (!await InteractWith(obj, newInstance))
				return TakeAreaTransitionError.InteractFailed;

			if (newInstance)
			{
				if (!await WaitForInstanceManager(5000))
				{
					LokiPoe.ProcessHookManager.ClearAllKeyStates();

					return TakeAreaTransitionError.InstanceManagerDidNotOpen;
				}

				LokiPoe.ProcessHookManager.ClearAllKeyStates();

				await Coroutines.LatencyWait();

				await Coroutine.Sleep(1000); // Let the gui stay open a bit before clicking too fast.

				if (LokiPoe.InGameState.InstanceManagerUi.InstanceCount >= maxInstances)
				{
					return TakeAreaTransitionError.TooManyInstances;
				}

				var nierr = LokiPoe.InGameState.InstanceManagerUi.JoinNewInstance();
				if (nierr != LokiPoe.InGameState.JoinInstanceResult.None)
				{
					Log.ErrorFormat("[TakeAreaTransition] InstanceManagerUi.JoinNew returned {0}.", nierr);
					return TakeAreaTransitionError.JoinNewFailed;
				}

				// Wait for the action to take place first.
				await Coroutines.LatencyWait();

				await Coroutines.ReactionWait();
			}

			if (isLocal)
			{
				if (!await WaitForPositionChange(pos))
				{
					Log.ErrorFormat("[TakeAreaTransition] WaitForPositionChange failed.");
					return TakeAreaTransitionError.WaitForAreaChangeFailed;
				}
			}
			else
			{
				if (!await WaitForAreaChange(hash))
				{
					Log.ErrorFormat("[TakeAreaTransition] WaitForAreaChange failed.");
					return TakeAreaTransitionError.WaitForAreaChangeFailed;
				}
			}

			return TakeAreaTransitionError.None;
		}

		public static async Task<bool> WaitForCursorToBeEmpty(int timeout = 5000)
		{
			var sw = Stopwatch.StartNew();
			while (LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Cursor).Any())
			{
				Log.InfoFormat("[WaitForCursorToBeEmpty] Waiting for the cursor to be empty.");
				await Coroutines.LatencyWait();
				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.InfoFormat("[WaitForCursorToBeEmpty] Timeout while waiting for the cursor to become empty.");
					return false;
				}
			}
			return true;
		}

		public static async Task<bool> WaitForCursorToHaveItem(int timeout = 5000)
		{
			var sw = Stopwatch.StartNew();
			while (!LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Cursor).Any())
			{
				Log.InfoFormat("[WaitForCursorToHaveItem] Waiting for the cursor to have an item.");
				await Coroutines.LatencyWait();
				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.InfoFormat("[WaitForCursorToHaveItem] Timeout while waiting for the cursor to contain an item.");
					return false;
				}
			}
			return true;
		}

		/// <summary>
		/// This coroutine interacts with an area transition in order to change areas. It assumes
		/// you are in interaction range with the area transition itself. It can be used both in town,
		/// and out of town, given the previous conditions are met.
		/// </summary>
		/// <param name="name">The name of the area transition to take.</param>
		/// <param name="newInstance">Should a new instance be created.</param>
		/// <param name="isLocal">Is the area transition local? In other words, should the couroutine not wait for an area change.</param>
		/// <param name="maxInstances">The max number of instance entries allowed to Join a new instance or -1 to not check.</param>
		/// <returns>A TakeAreaTransitionError that describes the result.</returns>
		public static async Task<TakeAreaTransitionError> TakeAreaTransition(string name, bool newInstance, int maxInstances,
			bool isLocal = false)
		{
			var at = LokiPoe.ObjectManager.AreaTransition(name);
			if (at == null)
				return TakeAreaTransitionError.NoAreaTransitions;

			return await TakeAreaTransition(at, newInstance, maxInstances, isLocal);
		}

		/// <summary>
		/// Errors for the OpenStash function.
		/// </summary>
		public enum OpenStashError
		{
			/// <summary>None, the stash has been interacted with and the stash panel is opened.</summary>
			None,

			/// <summary>There was an error moving to stash.</summary>
			CouldNotMoveToStash,

			/// <summary>No stash object was detected.</summary>
			NoStash,

			/// <summary>Interaction with the stash failed.</summary>
			InteractFailed,

			/// <summary>The stash panel did not open.</summary>
			StashPanelDidNotOpen,
		}

		/// <summary>
		/// This coroutine interacts with stash and waits for the stash panel to open. When called from a hideout,
		/// the stash must be in spawn range, otherwise the coroutine will fail.
		/// </summary>
		/// <returns>An OpenStashError that describes the result.</returns>
		public static async Task<OpenStashError> OpenGuildStash()
		{
			return await OpenStash(true);
		}

		private static NetworkObject DetermineStash(bool guild)
		{
			var stash = LokiPoe.ObjectManager.Stash;
			if (guild)
			{
				stash = LokiPoe.ObjectManager.GuildStash;
			}
			return stash;
		}

		/// <summary>
		/// This coroutine interacts with stash and waits for the stash panel to open. When called from a hideout,
		/// the stash must be in spawn range, otherwise the coroutine will fail.
		/// </summary>
		///<param name="guild">Should the guild stash be opened?</param>
		/// <returns>An OpenStashError that describes the result.</returns>
		public static async Task<OpenStashError> OpenStash(bool guild = false)
		{
			await Coroutines.CloseBlockingWindows();

			await Coroutines.FinishCurrentAction();

			var stash = DetermineStash(guild);
			if (stash == null)
			{
				if (LokiPoe.Me.IsInHideout)
				{
					return OpenStashError.NoStash;
				}

				if (
					!await
						MoveToLocation(ExilePather.FastWalkablePositionFor(Utility.GuessStashLocation()), 25,
							60000, () => DetermineStash(guild) != null && DetermineStash(guild).Distance < 75))
				{
					return OpenStashError.CouldNotMoveToStash;
				}

				stash = DetermineStash(guild);
				if (stash == null)
				{
					return OpenStashError.NoStash;
				}
			}

			if (stash.Distance > 30)
			{
				var p = stash.Position;
				if (!await MoveToLocation(ExilePather.FastWalkablePositionFor(p), 25, 15000, () => false))
				{
					return OpenStashError.CouldNotMoveToStash;
				}
			}

			await Coroutines.FinishCurrentAction();

			stash = DetermineStash(guild);
			if (stash == null)
			{
				return OpenStashError.NoStash;
			}

			if (!await InteractWith(stash))
				return OpenStashError.InteractFailed;

			if (guild)
			{
				if (!await WaitForGuildStashPanel())
					return OpenStashError.StashPanelDidNotOpen;

				await WaitForGuildStashTabChange(-1);
			}
			else
			{
				if (!await WaitForStashPanel())
					return OpenStashError.StashPanelDidNotOpen;

				await WaitForStashTabChange(-1);
			}

			return OpenStashError.None;
		}

		/// <summary>
		/// Errors for the OpenWaypoint function.
		/// </summary>
		public enum OpenWaypointError
		{
			/// <summary>None, the waypoint has been interacted with and the world panel is opened.</summary>
			None,

			/// <summary>There was an error moving to the waypoint.</summary>
			CouldNotMoveToWaypoint,

			/// <summary>No waypoint object was detected.</summary>
			NoWaypoint,

			/// <summary>Interaction with the waypoint failed.</summary>
			InteractFailed,

			/// <summary>The world panel did not open.</summary>
			WorldPanelDidNotOpen,
		}

		/// <summary>
		/// This coroutine interacts with the waypoint and waits for the world panel to open. When called from a hideout,
		/// the waypoint must be in spawn range, otherwise the coroutine will fail. The movement is done without returning,
		/// so this should be carefully used when not in town.
		/// </summary>
		/// <returns>An OpenStashError that describes the result.</returns>
		public static async Task<OpenWaypointError> OpenWaypoint()
		{
			await Coroutines.CloseBlockingWindows();

			await Coroutines.FinishCurrentAction();

			var waypoint = LokiPoe.ObjectManager.Waypoint;
			if (waypoint == null)
			{
				if (!LokiPoe.Me.IsInTown)
				{
					return OpenWaypointError.NoWaypoint;
				}

				if (
					!await
						MoveToLocation(ExilePather.FastWalkablePositionFor(Utility.GuessWaypointLocation()), 25,
							60000, () => LokiPoe.ObjectManager.Waypoint != null))
				{
					return OpenWaypointError.CouldNotMoveToWaypoint;
				}

				waypoint = LokiPoe.ObjectManager.Waypoint;
				if (waypoint == null)
				{
					return OpenWaypointError.NoWaypoint;
				}
			}

			if (ExilePather.PathDistance(LokiPoe.MyPosition, waypoint.Position) > 30)
			{
				if (!await MoveToLocation(ExilePather.FastWalkablePositionFor(waypoint.Position), 25, 15000, () => false))
				{
					return OpenWaypointError.CouldNotMoveToWaypoint;
				}
			}

			await Coroutines.FinishCurrentAction();

			waypoint = LokiPoe.ObjectManager.Waypoint;
			if (waypoint == null)
			{
				return OpenWaypointError.NoWaypoint;
			}

			if (!await InteractWith(waypoint))
				return OpenWaypointError.InteractFailed;

			if (!await WaitForWorldPanel())
				return OpenWaypointError.WorldPanelDidNotOpen;

			await Coroutine.Sleep(1000); // Adding this in to let the gui load more

			return OpenWaypointError.None;
		}

		/// <summary>
		/// Errors for the TakeWaypointTo function.
		/// </summary>
		public enum TakeWaypointToError
		{
			/// <summary>
			/// The waypoint has been taken, and the area changed.
			/// </summary>
			None,

			/// <summary>The OpenWaypoint function failed.</summary>
			OpenWaypointFailed,

			/// <summary>The TakeWaypoint function failed.</summary>
			TakeWaypointFailed,

			/// <summary>The WaitForAreaChange function failed.</summary>
			WaitForAreaChangeFailed,

			/// <summary>Too many instances are listed based on user configuration.</summary>
			TooManyInstances,
		}

		/// <summary>
		/// This coroutine will attempt to move to a waypoint and take it to the desired area.
		/// </summary>
		/// <param name="id">The id of the world area to take a waypoint to.</param>
		/// <param name="newInstance">Should a new instance be made.</param>
		/// <param name="maxInstances">The max number of instance entries allowed to Join a new instance or -1 to not check.</param>
		/// <returns>A TakeWaypointToError that describes the result.</returns>
		public static async Task<TakeWaypointToError> TakeWaypointTo(string id, bool newInstance, int maxInstances)
		{
			Log.InfoFormat("[TakeWaypointTo] {0} {1}", id, newInstance ? "(new instance)" : "");
			var hash = LokiPoe.LocalData.AreaHash;

			// Try to walk to it and open it.
			var owerr = await OpenWaypoint();
			if (owerr != OpenWaypointError.None)
				return TakeWaypointToError.OpenWaypointFailed;

			// Try to return to town.
			var twerr = LokiPoe.InGameState.WorldUi.TakeWaypoint(id, newInstance, maxInstances);
			if (twerr != LokiPoe.InGameState.TakeWaypointResult.None)
			{
				Log.ErrorFormat("[TakeWaypointTo] TakeWaypoint returned {0}.", twerr);
				if (twerr == LokiPoe.InGameState.TakeWaypointResult.TooManyInstances)
				{
					return TakeWaypointToError.TooManyInstances;
				}
				return TakeWaypointToError.TakeWaypointFailed;
			}

			await Coroutines.LatencyWait();

			// If we are in town, we're all done!
			if (!await WaitForAreaChange(hash))
				return TakeWaypointToError.WaitForAreaChangeFailed;

			return TakeWaypointToError.None;
		}

		/// <summary>
		/// This coroutine takes a waypoint to town.
		/// </summary>
		/// <returns></returns>
		internal static async Task<bool> TakeWaypointToTown()
		{
			var twterr = await TakeWaypointTo(LokiPoe.CurrentWorldArea.GoverningTown.Id, false, -1);
			if (twterr != TakeWaypointToError.None)
				Log.ErrorFormat("[TakeWaypointToTown] TakeWaypointTo returned {0}.", twterr);
			return twterr == TakeWaypointToError.None;
		}

		/// <summary>
		/// Errors for the TalkToNpc function.
		/// </summary>
		public enum TalkToNpcError
		{
			/// <summary>None, the npc has been interacted with and the npc dialog panel is opened.</summary>
			None,

			/// <summary>There was an error moving to the npc.</summary>
			CouldNotMoveToNpc,

			/// <summary>No waypoint object was detected.</summary>
			NoNpc,

			/// <summary>Interaction with the npc failed.</summary>
			InteractFailed,

			/// <summary>The npc's dialog panel did not open.</summary>
			NpcDialogPanelDidNotOpen,
		}

		/// <summary>
		/// This coroutine interacts with a npc and waits for the npc dialog panel to open. When called for a non-main town npc 
		/// the npc must be in spawn range, otherwise the coroutine will fail. The movement is done without returning,
		/// so this should be carefully used when not in town.
		/// </summary>
		/// <returns>An OpenStashError that describes the result.</returns>
		public static async Task<TalkToNpcError> TalkToNpc(string name)
		{
			await Coroutines.CloseBlockingWindows();

			await Coroutines.FinishCurrentAction();

			var npc = LokiPoe.ObjectManager.GetObjectByName(name);
			if (npc == null)
			{
				var pos = Utility.GuessNpcLocation(name);
				if (pos == Vector2i.Zero)
				{
					return TalkToNpcError.NoNpc;
				}

				if (
					!await
						MoveToLocation(ExilePather.FastWalkablePositionFor(pos), 25,
							60000, () => LokiPoe.ObjectManager.GetObjectByName(name) != null))
				{
					return TalkToNpcError.CouldNotMoveToNpc;
				}

				npc = LokiPoe.ObjectManager.GetObjectByName(name);
				if (npc == null)
				{
					return TalkToNpcError.NoNpc;
				}
			}

			if (ExilePather.PathDistance(LokiPoe.MyPosition, npc.Position) > 30)
			{
				if (!await MoveToLocation(ExilePather.FastWalkablePositionFor(npc.Position), 25, 15000, () => false))
				{
					return TalkToNpcError.CouldNotMoveToNpc;
				}

				npc = LokiPoe.ObjectManager.GetObjectByName(name);
				if (npc == null)
				{
					return TalkToNpcError.NoNpc;
				}
			}

			await Coroutines.FinishCurrentAction();

			if (!await InteractWith(npc))
				return TalkToNpcError.InteractFailed;

			if (!await WaitForNpcDialogPanel())
				return TalkToNpcError.NpcDialogPanelDidNotOpen;

			return TalkToNpcError.None;
		}

		/// <summary>
		/// This delegate is used to determine if the bot needs to id.
		/// </summary>
		/// <returns>true if the bot needs to id, and false otherwise.</returns>
		public delegate bool NeedsToIdDelegate();

		/// <summary>
		/// This delegate is called when stashing is completed.
		/// </summary>
		public delegate void IdingCompleteDelegate();

		/// <summary>
		/// The delegate used to determine if an item should be ided.
		/// </summary>
		/// <param name="item">The item to check.</param>
		/// <param name="user">The user object passed to IdItemsCoroutine.</param>
		/// <returns>true if the item should be ided and false otherwise.</returns>
		public delegate bool IdItemsDelegate(Item item, object user);

		/// <summary>Coroutine results for IdItemsCoroutine.</summary>
		public enum IdItemsCoroutineError
		{
			/// <summary>All items were ided, or there are none to id.</summary>
			None,

			/// <summary>The OpenStash function did not complete successfully.</summary>
			OpenStashFailed,

			/// <summary>The GoToFirstTab function failed.</summary>
			GoToFirstTabFailed,

			/// <summary>There are no id scrolls to use.</summary>
			NoMoreIdScrolls,

			/// <summary>The id operation has been interrupted by the inventory window being closed.</summary>
			InventoryClosed,

			/// <summary>The id operation has been interrupted by the stash window being closed.</summary>
			StashClosed,

			/// <summary>The UseItem function failed.</summary>
			UseItemFailed,

			/// <summary>We have id scrolls to use, but iding has finished and there's still an unid item.</summary>
			UnidentifiedItemsRemaining
		}

		/// <summary>
		/// This coroutine will id all items in the inventory that match the shouldIdItem
		/// delegate until no more items remain, or there are no more id scrolls to use. It looks
		/// for id scrolls in the main inventory.
		/// </summary>
		/// <param name="shouldIdItem">The delegate that determines if an item should be ided.</param>
		/// <param name="user">The user object that should be passed through the IdItemsDelegate.</param>
		/// <returns>An IdItemsCoroutineError error that describes the result.</returns>
		public static async Task<IdItemsCoroutineError> IdItemsUsingInventoryCoroutine(
			IdItemsDelegate shouldIdItem = null,
			object user = null)
		{
			// The default id coroutine will id all unidentified items.
			if (shouldIdItem == null)
			{
				shouldIdItem = (i, u) => !i.IsIdentified;
			}

			// If we don't have any items to id, we don't need to continue.
			if (
				!LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main)
					.Any(i => !i.IsIdentified && shouldIdItem(i, user)))
			{
				return IdItemsCoroutineError.None;
			}

			if (
				LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main)
					.FirstOrDefault(i => i.Name.Equals("Scroll of Wisdom")) == null)
			{
				return IdItemsCoroutineError.NoMoreIdScrolls;
			}

			// Close windows that prevent us from iding. Other tasks should be executing in a way where this doesn't interefere.
			if (LokiPoe.InGameState.PurchaseUi.IsOpened || LokiPoe.InGameState.SellUi.IsOpened ||
				LokiPoe.InGameState.TradeUi.IsOpened)
			{
				await Coroutines.CloseBlockingWindows();
			}

			if (!await OpenInventoryPanel())
			{
				return IdItemsCoroutineError.InventoryClosed;
			}

			await Coroutines.ReactionWait();

			for (int l = 0; l < 2; l++)
			{
				foreach (
					var item in
						LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main)
							.Where(i => !i.IsIdentified && shouldIdItem(i, user))
							.ToList())
				{
					// Handle the stash window closing.
					if (!LokiPoe.InGameState.InventoryUi.IsOpened)
					{
						return IdItemsCoroutineError.InventoryClosed;
					}

					var sow =
						LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(
							i => i.FullName.Equals("Scroll of Wisdom"));
					if (sow == null)
					{
						return IdItemsCoroutineError.NoMoreIdScrolls;
					}

					Log.InfoFormat("[IdItemsUsingInventoryCoroutine] Now using {0} on {1}.", sow.Name, item.Name);

					var ba = item.BaseAddress;

					var err1 = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.UseItem(sow.LocalId);
					if (err1 != UseItemResult.None)
					{
						Log.InfoFormat("[IdItemsUsingInventoryCoroutine] UseItem returned {0}.", err1);
						return IdItemsCoroutineError.UseItemFailed;
					}

					await Coroutines.LatencyWait();
					await Coroutines.ReactionWait();

					var err = InventoryControlWrapper.BeginApplyCursor();
					if (err != ApplyCursorResult.None)
					{
						Log.InfoFormat("[IdItemsUsingInventoryCoroutine] BeginApplyCursor returned {0}.", err);
						return IdItemsCoroutineError.UseItemFailed;
					}

					err = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.ApplyCursorTo(item.LocalId);
					if (err != ApplyCursorResult.None)
					{
						Log.InfoFormat("[IdItemsUsingInventoryCoroutine] ApplyCursorTo returned {0}.", err);
					}

					await Coroutines.LatencyWait();
					await Coroutines.ReactionWait();

					var err2 = InventoryControlWrapper.EndApplyCursor();
					if (err2 != ApplyCursorResult.None)
					{
						Log.InfoFormat("[IdItemsUsingInventoryCoroutine] EndApplyCursor returned {0}.", err2);
						return IdItemsCoroutineError.UseItemFailed;
					}

					if (err != ApplyCursorResult.None)
					{
						return IdItemsCoroutineError.UseItemFailed;
					}

					Stopwatch sw = Stopwatch.StartNew();
					var wait = Math.Max(LatencyTracker.Highest, 1500);
					while (LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).Any(i => i.BaseAddress == ba))
					{
						Log.InfoFormat("[IdItemsUsingInventoryCoroutine] Waiting for the item to be moved.");
						await Coroutine.Sleep(1);
						if (sw.ElapsedMilliseconds > wait)
						{
							Log.InfoFormat("[IdItemsUsingInventoryCoroutine] Timeout while waiting for the item to be moved.");
							break;
						}
					}
				}
			}

			if (
				LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main)
					.Any(i => !i.IsIdentified && shouldIdItem(i, user)))
			{
				return IdItemsCoroutineError.UnidentifiedItemsRemaining;
			}

			return IdItemsCoroutineError.None;
		}

		/// <summary>
		/// This coroutine will id all items in the inventory that match the shouldIdItem
		/// delegate until no more items remain, or there are no more id scrolls to use. It 
		/// looks for id scrolls in the Stash.
		/// </summary>
		/// <param name="shouldIdItem">The delegate that determines if an item should be ided.</param>
		/// <param name="user">The user object that should be passed through the IdItemsDelegate.</param>
		/// <returns>An IdItemsCoroutineError error that describes the result.</returns>
		public static async Task<IdItemsCoroutineError> IdItemsUsingStashCoroutine(IdItemsDelegate shouldIdItem = null,
			object user = null)
		{
			// The default stashing coroutine will stash all items.
			if (shouldIdItem == null)
			{
				shouldIdItem = (i, u) => !i.IsIdentified;
			}

			// If we don't have any items to id, we don't need to continue.
			if (
				!LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main)
					.Any(i => !i.IsIdentified && shouldIdItem(i, user)))
			{
				return IdItemsCoroutineError.None;
			}

			// We need both opened.
			if (!LokiPoe.InGameState.StashUi.IsOpened || !LokiPoe.InGameState.InventoryUi.IsOpened)
			{
				await Coroutines.CloseBlockingWindows();

				var otserr = await OpenStash();
				if (otserr != OpenStashError.None)
				{
					Log.ErrorFormat("[IdItemsUsingStashCoroutine] OpenStash returned {0}.", otserr);
					return IdItemsCoroutineError.OpenStashFailed;
				}

				await Coroutines.ReactionWait();
			}

			if (!LokiPoe.InGameState.StashUi.IsOpened)
			{
				return IdItemsCoroutineError.StashClosed;
			}

			if (!LokiPoe.InGameState.InventoryUi.IsOpened)
			{
				return IdItemsCoroutineError.InventoryClosed;
			}

			// We want to go to the first tab, since once we've requested tab contents, it's free.
			var res = LokiPoe.InGameState.StashUi.TabControl.SwitchToTabMouse(0);
			if (res != SwitchToTabResult.None)
			{
				return IdItemsCoroutineError.GoToFirstTabFailed;
			}

			await Coroutines.ReactionWait();

			await WaitForStashTabChange(-1);

			// Loop until something breaks.
			while (true)
			{
				// Handle window closing.
				if (!LokiPoe.InGameState.StashUi.IsOpened)
				{
					return IdItemsCoroutineError.StashClosed;
				}
				if (!LokiPoe.InGameState.InventoryUi.IsOpened)
				{
					return IdItemsCoroutineError.InventoryClosed;
				}

				// As long as the tab stays the same, we should be able to cache these.
				var invTab = LokiPoe.InGameState.StashUi.StashTabInfo;
				if (invTab.IsRemoveOnly || invTab.IsPublic) // TODO: Handle this better
				{
					if (invTab.IsRemoveOnly)
						Log.DebugFormat("[IdItemsUsingStashCoroutine] The current tab is Remove only. Skipping it.");
					else
						Log.DebugFormat("[IdItemsUsingStashCoroutine] The current tab is Public. Skipping it.");

					var oldId = invTab.InventoryId;

					var result = LokiPoe.InGameState.StashUi.TabControl.NextTabKeyboard();
					if (result != SwitchToTabResult.None)
					{
						Log.ErrorFormat("[IdItemsUsingStashCoroutine] NextTabKeyboard returned {0}.", result);

						break;
					}

					await WaitForStashTabChange(oldId);

					await Coroutines.ReactionWait();

					continue;
				}

				Item sow;

				if (invTab.IsPremiumCurrency)
				{
					sow = LokiPoe.InGameState.StashUi.InventoryControl_ScrollOfWisdom.CurrencyTabItem;
				}
				else
				{
					sow = LokiPoe.InGameState.StashUi.InventoryControl.Inventory.FindItemByFullName("Scroll of Wisdom");
				}
				
				if (sow == null)
				{
					var oldId = invTab.InventoryId;

					var result = LokiPoe.InGameState.StashUi.TabControl.NextTabKeyboard();
					if (result != SwitchToTabResult.None)
					{
						Log.ErrorFormat("[IdItemsUsingStashCoroutine] NextTabKeyboard returned {0}.", result);
						break;
					}

					await WaitForStashTabChange(oldId);

					await Coroutines.ReactionWait();

					continue;
				}

				var item =
					LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main)
						.FirstOrDefault(i => !i.IsIdentified && shouldIdItem(i, user));
				if (item == null)
					break;

				Log.InfoFormat("[IdItemsUsingStashCoroutine] Now using {0} on {1}.", sow.Name, item.Name);

				var ba = item.BaseAddress;

				if (invTab.IsPremiumCurrency)
				{
					var err1 = LokiPoe.InGameState.StashUi.InventoryControl_ScrollOfWisdom.UseItem();
					if (err1 != UseItemResult.None)
					{
						Log.InfoFormat("[IdItemsUsingStashCoroutine] UseItem returned {0}.", err1);
						return IdItemsCoroutineError.UseItemFailed;
					}
				}
				else
				{
					var err1 = LokiPoe.InGameState.StashUi.InventoryControl.UseItem(sow.LocalId);
					if (err1 != UseItemResult.None)
					{
						Log.InfoFormat("[IdItemsUsingStashCoroutine] UseItem returned {0}.", err1);
						return IdItemsCoroutineError.UseItemFailed;
					}
				}

				await Coroutines.LatencyWait();
				await Coroutines.ReactionWait();

				var err = InventoryControlWrapper.BeginApplyCursor();
				if (err != ApplyCursorResult.None)
				{
					Log.InfoFormat("[IdItemsUsingStashCoroutine] BeginApplyCursor returned {0}.", err);
					return IdItemsCoroutineError.UseItemFailed;
				}

				err = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.ApplyCursorTo(item.LocalId);
				if (err != ApplyCursorResult.None)
				{
					Log.InfoFormat("[IdItemsUsingStashCoroutine] ApplyCursorTo returned {0}.", err);
				}

				var err2 = InventoryControlWrapper.EndApplyCursor();
				if (err2 != ApplyCursorResult.None)
				{
					Log.InfoFormat("[IdItemsUsingStashCoroutine] EndApplyCursor returned {0}.", err2);
					return IdItemsCoroutineError.UseItemFailed;
				}

				if (err != ApplyCursorResult.None)
				{
					return IdItemsCoroutineError.UseItemFailed;
				}

				await Coroutines.LatencyWait();
				await Coroutines.ReactionWait();

				Stopwatch sw = Stopwatch.StartNew();
				var wait = Math.Max(LatencyTracker.Highest, 1500);
				while (LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).Any(i => i.BaseAddress == ba))
				{
					Log.InfoFormat("[IdItemsUsingStashCoroutine] Waiting for the item to be moved.");
					await Coroutine.Sleep(1);
					if (sw.ElapsedMilliseconds > wait)
					{
						Log.InfoFormat("[IdItemsUsingStashCoroutine] Timeout while waiting for the item to be moved.");
						break;
					}
				}

				if (
					!LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main)
						.Any(i => !i.IsIdentified && shouldIdItem(i, user)))
				{
					break;
				}
			}

			// If we don't have any items to id, we don't need to continue.
			if (
				LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main)
					.Any(i => !i.IsIdentified && shouldIdItem(i, user)))
			{
				return IdItemsCoroutineError.NoMoreIdScrolls;
			}

			return IdItemsCoroutineError.None;
		}

		/// <summary>
		/// This delegate is used to determine if the bot needs to sell.
		/// </summary>
		/// <returns>true if the bot needs to sell, and false otherwise.</returns>
		public delegate bool NeedsToSellDelegate();

		/// <summary>
		/// This delegate is called when selling is completed.
		/// </summary>
		public delegate void SellingCompleteDelegate(bool failed);

		/// <summary>
		/// The delegate used to determine if an item should be sold.
		/// </summary>
		/// <param name="item">The item to check.</param>
		/// <param name="user">The user object passed to SellItemsCoroutine.</param>
		/// <returns>true if the item should be sold and false otherwise.</returns>
		public delegate bool SellItemsDelegate(Item item, object user);

		/// <summary>
		/// This delegate is used to determine if the bot should accept the current sell offer.
		/// </summary>
		/// <returns>true if the bot should accept the sell offer, and false otherwise.</returns>
		public delegate bool ShouldAcceptSellOfferDelegate();

		/// <summary>Coroutine results for SellItemsCoroutine.</summary>
		public enum SellItemsCoroutineError
		{
			/// <summary>All items were sold, or there are none more to sell.</summary>
			None,

			/// <summary>No NPC to vendor to was found.</summary>
			NoNpc,

			/// <summary>The sell operation has been interrupted by the sell window being closed.</summary>
			SellClosed,

			/// <summary>The sell operation has been interrupted by the inventory window being closed.</summary>
			InventoryClosed,

			/// <summary>The TalkToNpc function failed.</summary>
			TalkToNpcFailed,

			/// <summary>The Converse function failed.</summary>
			ConverseFailed,

			/// <summary>The Sell panel did not open.</summary>
			SellPanelDidNotopen,

			/// <summary>The user refused the offer.</summary>
			OfferRefused,

			/// <summary>The Accept function failed.</summary>
			AcceptFailed,

			/// <summary>The FastMove function failed.</summary>
			FastMoveFailed,
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="shouldAcceptOffer"></param>
		/// <param name="shouldSellItem">The delegate that determines if an item should be sold.</param>
		/// <param name="user">The user object that should be passed through the SellItemsDelegate.</param>
		/// <param name="triggerOnSell">The delegate to invoke when an item is about to be sold.</param>
		/// <returns>An SellItemsCoroutineError error that describes the result.</returns>
		public static async Task<SellItemsCoroutineError> SellItemsCoroutine(
			ShouldAcceptSellOfferDelegate shouldAcceptOffer,
			SellItemsDelegate shouldSellItem,
			object user = null,
			TriggerOnSellDelegate triggerOnSell = null
			)
		{
			Tuple<string, Vector2i> npcInfo;

			if (LokiPoe.CurrentWorldArea.IsHideoutArea)
			{
				NetworkObject master = LokiPoe.ObjectManager.ClosestMaster;
				if (master == null)
					return SellItemsCoroutineError.NoNpc;
				npcInfo = new Tuple<string, Vector2i>(master.Name, master.Position);
			}
			else
			{
				npcInfo = Utility.GuessWeaponsNpcLocation();
				if (LokiPoe.CurrentWorldArea.Act != 3)
				{
					if (LokiPoe.Random.Next(0, 2) == 1)
					{
						npcInfo = Utility.GuessAccessoryNpcLocation();
					}
				}
			}

			// The default selling coroutine will sell no items.
			if (shouldSellItem == null)
			{
				shouldSellItem = (i, u) => false;
			}

			// The default selling coroutine will accept any offer.
			if (shouldAcceptOffer == null)
			{
				shouldAcceptOffer = () => true;
			}

			// If we don't have any items to sell, we don't need to continue.
			if (
				!LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main)
					.Where(i => i.Rarity != Rarity.Quest && !i.IsLabyrinthType && !i.HasSkillGemsEquipped && !i.HasMicrotransitionAttachment)
					.Any(i => shouldSellItem(i, null)))
			{
				return SellItemsCoroutineError.None;
			}

			// Handle opening the sell panel.
			if (!LokiPoe.InGameState.SellUi.IsOpened)
			{
				Log.DebugFormat(
					"[SellItemsCoroutine] The Npc sell window is not open. Now moving to a vendor to open it.");

				var ttnerr = await TalkToNpc(npcInfo.Item1);
				if (ttnerr != TalkToNpcError.None)
				{
					Log.ErrorFormat("[SellItemsCoroutine] TalkToNpc returned {0}.", ttnerr);
					return SellItemsCoroutineError.TalkToNpcFailed;
				}

				await Coroutines.LatencyWait();
				await Coroutines.ReactionWait();

				while (LokiPoe.InGameState.NpcDialogUi.DialogDepth != 1)
				{
					if (LokiPoe.InGameState.NpcDialogUi.DialogDepth == 2)
					{
						Log.DebugFormat("[SellItemsCoroutine] Now closing a dialog/reward window.");
						LokiPoe.Input.SimulateKeyEvent(Keys.Escape, true, false, false);
						// Give the client enough time to close the gui itself. It waits for the server to show the new one.
						await Coroutine.Sleep(1000);
					}
					else
					{
						Log.InfoFormat("[SellItemsCoroutine] Waiting for the Npc window to open.");
						await Coroutines.ReactionWait();
					}
				}

				var cerr = LokiPoe.InGameState.NpcDialogUi.SellItems();
				if (cerr != LokiPoe.InGameState.ConverseResult.None)
				{
					Log.ErrorFormat("[SellItemsCoroutine] NpcDialogUi.SellItems returned {0}.", ttnerr);
					return SellItemsCoroutineError.ConverseFailed;
				}

				await Coroutines.LatencyWait();
				await Coroutines.ReactionWait();

				if (!await WaitForSellPanel())
				{
					return SellItemsCoroutineError.SellPanelDidNotopen;
				}
			}

			Log.DebugFormat("[SellItemsCoroutine] The Sell window is open. Now moving items to sell.");

			foreach (
				var item in
					LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main)
						.Where(i => i.Rarity != Rarity.Quest && !i.IsLabyrinthType && !i.HasSkillGemsEquipped && !i.HasMicrotransitionAttachment)
						.ToList()
						.OrderBy(i => i.LocationTopLeft.X)
						.ThenBy(i => i.LocationTopLeft.Y))
			{
				// Handle window closing.
				if (!LokiPoe.InGameState.SellUi.IsOpened)
				{
					return SellItemsCoroutineError.SellClosed;
				}
				if (!LokiPoe.InGameState.InventoryUi.IsOpened)
				{
					return SellItemsCoroutineError.InventoryClosed;
				}

				if (!shouldSellItem(item, user))
					continue;

				Log.DebugFormat("[SellItemsCoroutine] Now fast moving the item {0} into the current vendor tab.",
					item.Name);

				var ba = item.BaseAddress;

				var fmerr = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.FastMove(item.LocalId);
				if (fmerr != FastMoveResult.None)
				{
					Log.ErrorFormat("[SellItemsCoroutine] FastMove returned {0}.", fmerr);

					var cerr = LokiPoe.InGameState.SellUi.TradeControl.Cancel();
					if (cerr != TradeResult.None)
					{
						Log.ErrorFormat("[SellItemsCoroutine] SellUi.Cancel returned {0}.", cerr);
					}

					await Coroutines.LatencyWait();
					await Coroutines.ReactionWait();

					while (LokiPoe.InGameState.SellUi.IsOpened)
					{
						cerr = LokiPoe.InGameState.SellUi.TradeControl.Cancel();
						Log.ErrorFormat(
							"[SellItemsCoroutine] Cancel returned {0}. Waiting for the SellUi to close (FastMove failed).", cerr);
						await Coroutines.ReactionWait();
					}

					return SellItemsCoroutineError.FastMoveFailed;
				}

				await Coroutines.LatencyWait();

				Stopwatch sw = Stopwatch.StartNew();
				var wait = 3000;
				while (LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).Any(i => i.BaseAddress == ba))
				{
					Log.InfoFormat("[SellItemsCoroutine] Waiting for the item to be moved.");
					await Coroutine.Sleep(1);
					if (sw.ElapsedMilliseconds > wait)
					{
						Log.InfoFormat("[SellItemsCoroutine] Timeout while waiting for the item to be moved.");
						break;
					}
				}

				await Coroutines.ReactionWait();
			}

			Log.DebugFormat("[SellItemsCoroutine] Moving items to sell has completed.");

			if (!shouldAcceptOffer())
			{
				var cerr = LokiPoe.InGameState.SellUi.TradeControl.Cancel();
				if (cerr != TradeResult.None)
				{
					Log.ErrorFormat("[SellItemsCoroutine] Cancel returned {0}.", cerr);
				}

				await Coroutines.LatencyWait();
				await Coroutines.ReactionWait();

				while (LokiPoe.InGameState.SellUi.IsOpened)
				{
					cerr = LokiPoe.InGameState.SellUi.TradeControl.Cancel();
					Log.ErrorFormat(
						"[SellItemsCoroutine] Cancel returned {0}. Waiting for the SellUi to close (Offer refused).", cerr);
					await Coroutines.ReactionWait();
				}

				return SellItemsCoroutineError.OfferRefused;
			}

			// Let's try to accept, without clicking the button to make sure everything should work before calling our delegate.
			var aerr = LokiPoe.InGameState.SellUi.TradeControl.Accept(false);
			if(aerr != TradeResult.None)
			{
				Log.ErrorFormat("[SellItemsCoroutine] Accept returned {0}.", aerr);
				return SellItemsCoroutineError.AcceptFailed;
			}

			// TODO: The case of the vendor offer not fitting in the current inventory is not handled, but should be rare.

			foreach (var item in LokiPoe.InGameState.SellUi.TradeControl.InventoryControl_OtherOffer.Inventory.Items)
			{
				triggerOnSell?.Invoke(item);
			}

			aerr = LokiPoe.InGameState.SellUi.TradeControl.Accept();
			if (aerr != TradeResult.None)
			{
				Log.ErrorFormat("[SellItemsCoroutine] Accept returned {0}.", aerr);
				return SellItemsCoroutineError.AcceptFailed;
			}

			await Coroutines.LatencyWait();
			await Coroutines.ReactionWait();

			var tries = 0;
			while (LokiPoe.InGameState.SellUi.IsOpened)
			{
				if (tries > 5)
				{
					Log.ErrorFormat("[SellItemsCoroutine] Too many tries. Something is wrong.");
					return SellItemsCoroutineError.AcceptFailed;
				}

				aerr = LokiPoe.InGameState.SellUi.TradeControl.Accept();
				++tries;
				if (aerr != TradeResult.None)
				{
					Log.ErrorFormat("[SellItemsCoroutine] Accept returned {0}.", aerr);
					return SellItemsCoroutineError.AcceptFailed;
				}

				Log.DebugFormat("[SellItemsCoroutine] Waiting for the SellUi to close (Offer Accepted).");

				await Coroutines.LatencyWait();
				await Coroutines.ReactionWait();
			}

			return SellItemsCoroutineError.None;
		}

		/// <summary>
		/// This delegate is used to determine if the bot needs to stash.
		/// </summary>
		/// <returns>true if the bot needs to stash, and false otherwise.</returns>
		public delegate bool NeedsToStashDelegate();

		/// <summary>
		/// This delegate is called when stashing is completed.
		/// </summary>
		public delegate void StashingCompleteDelegate();

		/// <summary>
		/// The delegate used to determine if an item should be stashed.
		/// </summary>
		/// <param name="item">The item to check.</param>
		/// <param name="user">The user object passed to StashItemsCoroutine.</param>
		/// <returns>true if the item should be stashed and false otherwise.</returns>
		public delegate bool StashItemsDelegate(Item item, object user);

		/// <summary>
		/// The delegate used to determine where an item should be stashed.
		/// </summary>
		/// <param name="tab">The tab to check.</param>
		/// <param name="inventory">The tab's inventory.</param>
		/// <param name="item">The item being stashed.</param>
		/// <param name="user">The user object passed to StashItemsCoroutine.</param>
		/// <returns>true if the item should be stashed to this tab and false otherwise.</returns>
		public delegate bool BestStashTabForItem(StashTabInfo tab, Inventory inventory, Item item, object user);

		/// <summary>Coroutine results for StashItemsCoroutine.</summary>
		public enum StashItemsCoroutineError
		{
			/// <summary>All items were stashed.</summary>
			None,

			/// <summary>An item on the cursor could not be placed into the stash tab.</summary>
			CursorItemCouldNotBePlaced,

			/// <summary>An item did not fit in the inventory.</summary>
			CouldNotFitAllItems,

			/// <summary>The OpenStash function did not complete successfully.</summary>
			OpenStashFailed,

			/// <summary>The GoToFirstTab function failed.</summary>
			GoToFirstTabFailed,

			/// <summary>The stash operation has been interrupted by the stash window being closed.</summary>
			StashClosed,

			/// <summary>The stash operation has been interrupted by the inventory window being closed.</summary>
			InventoryClosed,
		}

		/// <summary>
		/// Waits for a stash tab to change. Pass -1 to lastId to wait for the initial tab.
		/// </summary>
		/// <param name="lastId">The last InventoryId before changing tabs.</param>
		/// <param name="timeout">The timeout of the function.</param>
		/// <returns>true if the tab was changed and false otherwise.</returns>
		public static async Task<bool> WaitForStashTabChange(int lastId, int timeout=5000)
		{
			var sw = Stopwatch.StartNew();
			var invTab = LokiPoe.InGameState.StashUi.StashTabInfo;
			while (invTab == null || invTab.InventoryId == lastId)
			{
				Log.InfoFormat("[WaitForStashTabChange] Waiting...");
				await Coroutine.Sleep(1);
				invTab = LokiPoe.InGameState.StashUi.StashTabInfo;
				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.InfoFormat("[WaitForStashTabChange] Timeout...");
					return false;
				}
			}
			return true;
		}

		/// <summary>
		/// Waits for a guild stash tab to change. Pass -1 to lastId to wait for the initial tab.
		/// </summary>
		/// <param name="lastId">The last InventoryId before changing tabs.</param>
		/// <param name="timeout">The timeout of the function.</param>
		/// <returns>true if the tab was changed and false otherwise.</returns>
		public static async Task<bool> WaitForGuildStashTabChange(int lastId, int timeout = 5000)
		{
			var sw = Stopwatch.StartNew();
			var invTab = LokiPoe.InGameState.GuildStashUi.StashTabInfo;
			while (invTab == null || invTab.InventoryId == lastId)
			{
				Log.InfoFormat("[WaitForGuildStashTabChange] Waiting...");
				await Coroutine.Sleep(1);
				invTab = LokiPoe.InGameState.GuildStashUi.StashTabInfo;
				if (sw.ElapsedMilliseconds > timeout)
				{
					Log.InfoFormat("[WaitForGuildStashTabChange] Timeout...");
					return false;
				}
			}
			return true;
		}

		private static InventoryControlWrapper BestInventoryControlForItem(Item item)
		{
			var matches = new List<Tuple<InventoryControlWrapper, int>>();

			// Look at primary slots for a match by type first.
			var invControl = LokiPoe.InGameState.StashUi.GetInventoryControlForMetadata(item.Metadata, false);
			if (invControl != null)
			{
				// Check if the slot has any inventory items yet.
				var existing = invControl.CurrencyTabItem;
				if (existing != null)
				{
					// An item, store how many spaces left.
					if (existing.StackCount != existing.MaxCurrencyTabStackCount)
					{
						matches.Add(new Tuple<InventoryControlWrapper, int>(invControl, existing.MaxCurrencyTabStackCount - existing.StackCount));
					}
				}
				else
				{
					// No item, full spaces to use.
					matches.Add(new Tuple<InventoryControlWrapper, int>(invControl, 5000));
				}
			}

			// Now check all utility slots
			foreach (var ic in LokiPoe.InGameState.StashUi.CurrencyTabInventoryControlsMisc)
			{
				var existing = ic.CurrencyTabItem;
				if (existing != null && existing.Metadata.Equals(item.Metadata))
				{
					// We need to exact match items for utility slots, so see if there's space or not.
					if (existing.StackCount != existing.MaxCurrencyTabStackCount)
					{
						matches.Add(new Tuple<InventoryControlWrapper, int>(ic, existing.MaxCurrencyTabStackCount - existing.StackCount));
					}
				}
			}

			// Endless exceptions if we don't have any items to process in linq.
			if (!matches.Any())
				return null;

			// Simply return the best slot based on most spaces are left.
			return matches.OrderByDescending(t => t.Item2).FirstOrDefault().Item1;
		}

		/// <summary>
		/// This coroutine will deposit all items into Stash that match the specified
		/// StashItemsDelegate. If no StashItemsDelegate is used, the default delegate
		/// will stash all non-quest items.
		/// </summary>
		/// <param name="shouldStashItem">The delegate that determines if an item should be stashed.</param>
		/// <param name="bestStashTabForItem">The delegate that determines where an item should be stashed.</param>
		/// <param name="user">The user object that should be passed through the StashItemsDelegate.</param>
		/// <param name="triggerOnStash">The delegate to call when an item is stashed.</param>
		/// <returns>A StashItemsCoroutineError that describes the result.</returns>
		public static async Task<StashItemsCoroutineError> StashItemsCoroutine(
			StashItemsDelegate shouldStashItem = null,
			BestStashTabForItem bestStashTabForItem = null,
			object user = null,
			TriggerOnStashDelegate triggerOnStash = null
			)
		{
			// The default stashing coroutine will stash all items.
			if (shouldStashItem == null)
			{
				shouldStashItem = (i, u) => true;
			}

			// The default stashing coroutine will stash at the first page available.
			if (bestStashTabForItem == null)
			{
				bestStashTabForItem = (t, iv, i, u) => true;
			}

			// Handle opening the stash.
			if (!LokiPoe.InGameState.StashUi.IsOpened)
			{
				Log.DebugFormat("[StashItemsCoroutine] The Stash window is not open. Now moving to Stash to open it.");

				var otserr = await OpenStash();
				if (otserr != OpenStashError.None)
				{
					Log.ErrorFormat("[StashItemsCoroutine] OpenStash returned {0}.", otserr);
					return StashItemsCoroutineError.OpenStashFailed;
				}

				await Coroutines.ReactionWait();
			}

			Log.DebugFormat("[StashItemsCoroutine] The Stash window is open. Now going to the first tab.");

			// We want to go to the first tab, since once we've requested tab contents, it's free.
			var result = LokiPoe.InGameState.StashUi.TabControl.SwitchToTabMouse(0);
			if (result != SwitchToTabResult.None)
			{
				Log.DebugFormat("[StashItemsCoroutine] GoToFirstTab failed.");

				return StashItemsCoroutineError.GoToFirstTabFailed;
			}

			await Coroutines.ReactionWait();

			await WaitForStashTabChange(-1);

			// Loop until something breaks.
			while (true)
			{
				// Handle window closing.
				if (!LokiPoe.InGameState.StashUi.IsOpened)
				{
					return StashItemsCoroutineError.StashClosed;
				}
				if (!LokiPoe.InGameState.InventoryUi.IsOpened)
				{
					return StashItemsCoroutineError.InventoryClosed;
				}

				// As long as the tab stays the same, we should be able to cache these.
				var invTab = LokiPoe.InGameState.StashUi.StashTabInfo;
				if (invTab.IsRemoveOnly || invTab.IsPublic) // TODO: Handle this better
				{
					if(invTab.IsRemoveOnly)
						Log.DebugFormat("[StashItemsCoroutine] The current tab is Remove only. Skipping it.");
					else
						Log.DebugFormat("[IdItemsUsingStashCoroutine] The current tab is Public. Skipping it.");

					var oldId = invTab.InventoryId;

					result = LokiPoe.InGameState.StashUi.TabControl.NextTabKeyboard();
					if (result != SwitchToTabResult.None)
					{
						Log.ErrorFormat("[IdItemsUsingStashCoroutine] NextTabKeyboard returned {0}.", result);

						break;
					}

					await WaitForStashTabChange(oldId);

					await Coroutines.ReactionWait();

					continue;
				}

				var inv = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory;

				if (invTab.IsPremiumCurrency)
				{
					foreach (var item in inv.Items.Where(i => i.Rarity == Rarity.Currency && !i.IsLabyrinthType).ToList())
					{
						// Handle window closing.
						if (!LokiPoe.InGameState.StashUi.IsOpened)
						{
							return StashItemsCoroutineError.StashClosed;
						}
						if (!LokiPoe.InGameState.InventoryUi.IsOpened)
						{
							return StashItemsCoroutineError.InventoryClosed;
						}

						// Make sure this item should be stashed.
						if (!shouldStashItem(item, user))
							continue;

						// Lookup the inventory control for this particular currency item. If there is none, it's not compatbile with this tab.
						var invControl = BestInventoryControlForItem(item);
						if (invControl == null)
							continue;

						// Make sure the user wants to stash currency to this tab.
						if (!bestStashTabForItem(invTab, invControl.Inventory, item, user))
							continue;

						// Now taken care of in BestInventoryControlForItem
						// Make sure the item will fit in the currency tab.
						/*var existing = invControl.CurrencyTabItem;
						if (existing != null)
						{
							if (existing.StackCount == existing.MaxCurrencyTabStackCount)
							{
								continue;
							}
						}*/

						Log.DebugFormat("[StashItemsCoroutine] Now fast moving the item {0} into the current currency stash tab.",
							item.Name);

						if (triggerOnStash != null)
						{
							triggerOnStash(item);
						}

						var ba = item.BaseAddress;

						var fmerr = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.FastMove(item.LocalId);
						if (fmerr != FastMoveResult.None)
						{
							Log.ErrorFormat("[StashItemsCoroutine] FastMove returned {0}.", fmerr);
						}

						await Coroutines.LatencyWait();

						Stopwatch sw = Stopwatch.StartNew();
						var wait = 3000;
						while (LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).Any(i => i.BaseAddress == ba))
						{
							Log.InfoFormat("[StashItemsCoroutine] Waiting for the item to be moved.");
							await Coroutine.Sleep(1);
							if (sw.ElapsedMilliseconds > wait)
							{
								Log.InfoFormat("[StashItemsCoroutine] Timeout while waiting for the item to be moved.");
								break;
							}
						}

						await Coroutines.ReactionWait();
					}
				}
				else
				{
					var invControl = LokiPoe.InGameState.StashUi.InventoryControl;

					foreach (
						var item in inv.Items.Where(i => i.Rarity != Rarity.Quest && !i.IsLabyrinthType).ToList())
					{
						// Handle window closing.
						if (!LokiPoe.InGameState.StashUi.IsOpened)
						{
							return StashItemsCoroutineError.StashClosed;
						}
						if (!LokiPoe.InGameState.InventoryUi.IsOpened)
						{
							return StashItemsCoroutineError.InventoryClosed;
						}

						if (!shouldStashItem(item, user))
							continue;

						if (!bestStashTabForItem(invTab, invControl.Inventory, item, user))
							continue;

						// Handle currency items
						if (item.IsStackable)
						{
							// First, see if we can merge stacks automatically.
							var name = item.Name;
							if (!invControl.Inventory.Items.Any(i => i.Name == name && i.StackCount < i.MaxStackCount))
							{
								// Now check for space.
								if (!LokiPoe.InGameState.StashUi.InventoryControl.Inventory.CanFitItem(item))
								{
									continue;
								}
							}
						}
						else
						{
							if (!LokiPoe.InGameState.StashUi.InventoryControl.Inventory.CanFitItem(item))
							{
								continue;
							}
						}

						Log.DebugFormat("[StashItemsCoroutine] Now fast moving the item {0} into the current stash tab.",
							item.Name);

						if (triggerOnStash != null)
						{
							triggerOnStash(item);
						}

						var ba = item.BaseAddress;

						var fmerr = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.FastMove(item.LocalId);
						if (fmerr != FastMoveResult.None)
						{
							Log.ErrorFormat("[StashItemsCoroutine] FastMove returned {0}.", fmerr);
						}

						await Coroutines.LatencyWait();

						Stopwatch sw = Stopwatch.StartNew();
						var wait = 3000;
						while (LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).Any(i => i.BaseAddress == ba))
						{
							Log.InfoFormat("[StashItemsCoroutine] Waiting for the item to be moved.");
							await Coroutine.Sleep(1);
							if (sw.ElapsedMilliseconds > wait)
							{
								Log.InfoFormat("[StashItemsCoroutine] Timeout while waiting for the item to be moved.");
								break;
							}
						}

						await Coroutines.ReactionWait();
					}
				}

				if (
					LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).Where(i => i.Rarity != Rarity.Quest && !i.IsLabyrinthType)
						.Any(i => shouldStashItem(i, null)))
				{
					var oldId = invTab.InventoryId;

					result = LokiPoe.InGameState.StashUi.TabControl.NextTabKeyboard();
					if (result != SwitchToTabResult.None)
					{
						Log.ErrorFormat("[StashItemsCoroutine] NextTabKeyboard returned {0}.", result);
						break;
					}

					await Coroutines.LatencyWait();

					await WaitForStashTabChange(oldId);

					await Coroutines.ReactionWait();
				}
				else
				{
					break;
				}
			}

			if (
				LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).Where(i => i.Rarity != Rarity.Quest && !i.IsLabyrinthType)
					.Any(i => shouldStashItem(i, null)))
			{
				return StashItemsCoroutineError.CouldNotFitAllItems;
			}

			return StashItemsCoroutineError.None;
		}

		/// <summary>
		/// The delegate used to determine if an item should be withdrawn.
		/// </summary>
		/// <param name="item">The item to check.</param>
		/// <param name="count">The number of items.</param>
		/// <param name="user">The user object passed to WithdrawItemsCoroutine.</param>
		/// <returns>true if the item should be withdrawn and false otherwise.</returns>
		public delegate bool WithdrawItemsDelegate(Item item, out int count, object user);

		/// <summary>
		/// The delegate used to determine if a tab should be withdrawn from.
		/// </summary>
		/// <param name="tab">The current tab being checked.</param>
		/// <param name="user">The user object passed to WithdrawItemsCoroutine.</param>
		/// <returns>true if items should be withdrawn from this tab, and false otherwise.</returns>
		public delegate bool WithdrawFromTabDelegate(StashTabInfo tab, object user);

		/// <summary>
		/// The delegate used to determine if withdrawing should continue.
		/// </summary>
		/// <param name="user">The user object passed to WithdrawItemsCoroutine.</param>
		/// <returns>true if withdrawing should continue and false otherwise.</returns>
		public delegate bool ContinueWithdrawingDelegate(object user);

		/// <summary>Coroutine results for withdrawItemsCoroutine.</summary>
		public enum WithdrawItemsCoroutineError
		{
			/// <summary>All items have been withdrawn.</summary>
			None,

			/// <summary>The OpenStash function did not complete successfully.</summary>
			OpenStashFailed,

			/// <summary>The GoToFirstTab function failed.</summary>
			GoToFirstTabFailed,

			/// <summary>The id operation has been interrupted by the inventory window being closed.</summary>
			InventoryClosed,

			/// <summary>The FastMove function failed.</summary>
			FastMoveFailed,

			/// <summary>The id operation has been interrupted by the stash window being closed.</summary>
			StashClosed,

			/// <summary>Withdrawing cannot continue, because the main inventory is full.</summary>
			InventoryFull
		}

		/// <summary>
		/// This coroutine will withdraw items from stash that meet user requirements. Only full stacks of items can
		/// be withdrawn for the time being, as the logic for handling split stacks and properly inventory mergining
		/// is not implemented.
		/// </summary>
		/// <param name="shouldWithdrawFromTab">The delegate that determines if the tab should be used for withdrawing.</param>
		/// <param name="shouldWithdrawItem">The delegate that determines if an item should be withdrawn.</param>
		/// <param name="shouldContinue">The delegate that determines if withdrawing should continue.</param>
		/// <param name="user">The user object that should be passed through the delegates.</param>
		/// <param name="triggerOnWithdraw">The delegate to call when an item is withdrawn.</param>
		/// <returns>A WithdrawItemsCoroutineError that describes the result.</returns>
		public static async Task<WithdrawItemsCoroutineError> WithdrawItemsCoroutine(
			WithdrawFromTabDelegate shouldWithdrawFromTab,
			WithdrawItemsDelegate shouldWithdrawItem,
			ContinueWithdrawingDelegate shouldContinue,
			object user = null,
			TriggerOnWithdrawDelegate triggerOnWithdraw = null)
		{
			// The default withdraw coroutine will not withdraw anything when passed a null item delegate.
			if (shouldWithdrawItem == null)
			{
				return WithdrawItemsCoroutineError.None;
			}

			// The default withdraw coroutine will only process non-remove only tabs.
			if (shouldWithdrawFromTab == null)
			{
				shouldWithdrawFromTab = (t, u) => !t.IsRemoveOnly && !t.IsPublic;
			}

			// We need both opened.
			if (!LokiPoe.InGameState.StashUi.IsOpened || !LokiPoe.InGameState.InventoryUi.IsOpened)
			{
				await Coroutines.CloseBlockingWindows();

				var otserr = await OpenStash();
				if (otserr != OpenStashError.None)
				{
					Log.ErrorFormat("[WithdrawItemsCoroutine] OpenStash returned {0}.", otserr);
					return WithdrawItemsCoroutineError.OpenStashFailed;
				}

				await Coroutines.ReactionWait();
			}

			if (!LokiPoe.InGameState.StashUi.IsOpened)
			{
				return WithdrawItemsCoroutineError.StashClosed;
			}

			if (!LokiPoe.InGameState.InventoryUi.IsOpened)
			{
				return WithdrawItemsCoroutineError.InventoryClosed;
			}

			// We want to go to the first tab, since once we've requested tab contents, it's free.
			var result = LokiPoe.InGameState.StashUi.TabControl.SwitchToTabMouse(0);
			if (result != SwitchToTabResult.None)
			{
				return WithdrawItemsCoroutineError.GoToFirstTabFailed;
			}

			await Coroutines.ReactionWait();

			await WaitForStashTabChange(-1);
			var oldId = -1;

			// Loop until something breaks.
			while (true)
			{
				// Handle window closing.
				if (!LokiPoe.InGameState.StashUi.IsOpened)
				{
					return WithdrawItemsCoroutineError.StashClosed;
				}
				if (!LokiPoe.InGameState.InventoryUi.IsOpened)
				{
					return WithdrawItemsCoroutineError.InventoryClosed;
				}

				// As long as the tab stays the same, we should be able to cache these.
				var invTab = LokiPoe.InGameState.StashUi.StashTabInfo;
				if (invTab.IsRemoveOnly || invTab.IsPublic) // TODO: Handle this better
				{
					if (invTab.IsRemoveOnly)
						Log.DebugFormat("[WithdrawItemsCoroutine] The current tab is Remove only. Skipping it.");
					else
						Log.DebugFormat("[WithdrawItemsCoroutine] The current tab is Public. Skipping it.");

					oldId = invTab.InventoryId;

					result = LokiPoe.InGameState.StashUi.TabControl.NextTabKeyboard();
					if (result != SwitchToTabResult.None)
					{
						Log.ErrorFormat("[IdItemsUsingStashCoroutine] NextTabKeyboard returned {0}.", result);

						break;
					}

					await WaitForStashTabChange(oldId);

					await Coroutines.ReactionWait();

					continue;
				}

				// Check to see if this tab should be withdrawn from.
				if (!shouldWithdrawFromTab(invTab, user))
				{
					Log.DebugFormat(
						"[WithdrawItemsCoroutine] The current tab should not be withdrawn from. Skipping it.");

					if (!shouldContinue(user))
					{
						Log.InfoFormat(
							"[WithdrawItemsCoroutine] shouldContinue returned false. Now breaking out of the tab loop.");
						break;
					}

					oldId = invTab.InventoryId;

					result = LokiPoe.InGameState.StashUi.TabControl.NextTabKeyboard();
					if (result != SwitchToTabResult.None)
					{
						Log.ErrorFormat("[WithdrawItemsCoroutine] NextTabKeyboard returned {0}.", result);

						break;
					}

					await WaitForStashTabChange(oldId);

					await Coroutines.ReactionWait();

					continue;
				}

				var doBreak = false;

				if (invTab.IsPremiumCurrency)
				{
					foreach (var inv in LokiPoe.InGameState.StashUi.CurrencyTabInventoryControls)
					{
						var item = inv.CurrencyTabItem;
						if(item == null)
							continue;

						int count;
						if (shouldWithdrawItem(item, out count, user))
						{
							if (!LokiPoe.InstanceInfo.GetPlayerInventoryBySlot(InventorySlot.Main).CanFitItem(item))
							{
								return WithdrawItemsCoroutineError.InventoryFull;
							}

							var name = item.Name;

							var ba = item.BaseAddress;

							if (triggerOnWithdraw != null)
							{
								triggerOnWithdraw(item);
							}

							var fmerr = inv.FastMove();
							if (fmerr != FastMoveResult.None)
							{
								Log.ErrorFormat("[WithdrawItemsCoroutine] FastMove returned {0} for {1}.", fmerr, name);
								return WithdrawItemsCoroutineError.FastMoveFailed;
							}

							/*if (count == item.StackCount)
							{
								if (item.MaxStackCount == 1)
								{
								}
								else
								{
								}
							}
							else
							{
							}*/

							await Coroutines.LatencyWait();

							Stopwatch sw = Stopwatch.StartNew();
							var wait = 3000;
							while (inv.Inventory.Items.Any(i => i.BaseAddress == ba))
							{
								Log.InfoFormat("[WithdrawItemsCoroutine] Waiting for the item to be moved.");
								await Coroutine.Sleep(1);
								if (sw.ElapsedMilliseconds > wait)
								{
									Log.InfoFormat("[WithdrawItemsCoroutine] Timeout while waiting for the item to be moved.");
									break;
								}
							}

							await Coroutines.ReactionWait();

							if (!shouldContinue(user))
							{
								Log.InfoFormat(
									"[WithdrawItemsCoroutine] shouldContinue returned false. Now breaking out of the withdraw loop.");
								doBreak = true;
								break;
							}
						}
					}
				}
				else
				{
					var inv = LokiPoe.InGameState.StashUi.InventoryControl.Inventory;

					// Process all items on the tab.
					foreach (var item in inv.Items.ToList())
					{
						int count;
						if (shouldWithdrawItem(item, out count, user))
						{
							if (!LokiPoe.InstanceInfo.GetPlayerInventoryBySlot(InventorySlot.Main).CanFitItem(item))
							{
								return WithdrawItemsCoroutineError.InventoryFull;
							}

							var name = item.Name;

							var ba = item.BaseAddress;

							if (triggerOnWithdraw != null)
							{
								triggerOnWithdraw(item);
							}

							var fmerr = LokiPoe.InGameState.StashUi.InventoryControl.FastMove(item.LocalId);
							if (fmerr != FastMoveResult.None)
							{
								Log.ErrorFormat("[WithdrawItemsCoroutine] FastMove returned {0} for {1}.", fmerr, name);
								return WithdrawItemsCoroutineError.FastMoveFailed;
							}

							/*if (count == item.StackCount)
							{
								if (item.MaxStackCount == 1)
								{
								}
								else
								{
								}
							}
							else
							{
							}*/

							await Coroutines.LatencyWait();

							Stopwatch sw = Stopwatch.StartNew();
							var wait = 3000;
							while (inv.Items.Any(i => i.BaseAddress == ba))
							{
								Log.InfoFormat("[WithdrawItemsCoroutine] Waiting for the item to be moved.");
								await Coroutine.Sleep(1);
								if (sw.ElapsedMilliseconds > wait)
								{
									Log.InfoFormat("[WithdrawItemsCoroutine] Timeout while waiting for the item to be moved.");
									break;
								}
							}

							await Coroutines.ReactionWait();

							if (!shouldContinue(user))
							{
								Log.InfoFormat(
									"[WithdrawItemsCoroutine] shouldContinue returned false. Now breaking out of the withdraw loop.");
								doBreak = true;
								break;
							}
						}
					}
				}

				if (doBreak)
				{
					break;
				}

				if (!shouldContinue(user))
				{
					Log.InfoFormat(
						"[WithdrawItemsCoroutine] shouldContinue returned false. Now breaking out of the tab loop.");
					break;
				}

				oldId = invTab.InventoryId;

				result = LokiPoe.InGameState.StashUi.TabControl.NextTabKeyboard();
				if (result != SwitchToTabResult.None)
				{
					Log.ErrorFormat("[WithdrawItemsCoroutine] NextTabKeyboard returned {0}.", result);

					break;
				}

				await WaitForStashTabChange(oldId);

				await Coroutines.ReactionWait();
			}

			return WithdrawItemsCoroutineError.None;
		}
	}

	/// <summary>
	/// Shared utility functions.
	/// </summary>
	public static class Utility
	{
		/// <summary>
		/// Checks for a closed door between start and end.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="distanceFromPoint">How far to check around each point for a door object.</param>
		/// <param name="stride">The distance between points to check in the path.</param>
		/// <param name="dontLeaveFrame">Should the current frame not be left?</param>
		/// <returns>true if there's a closed door and false otherwise.</returns>
		public static bool ClosedDoorBetween(NetworkObject start, NetworkObject end, int distanceFromPoint = 10,
			int stride = 10, bool dontLeaveFrame = false)
		{
			return ClosedDoorBetween(start.Position, end.Position, distanceFromPoint, stride, dontLeaveFrame);
		}

		/// <summary>
		/// Checks for a closed door between start and end.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="distanceFromPoint">How far to check around each point for a door object.</param>
		/// <param name="stride">The distance between points to check in the path.</param>
		/// <param name="dontLeaveFrame">Should the current frame not be left?</param>
		/// <returns>true if there's a closed door and false otherwise.</returns>
		public static bool ClosedDoorBetween(NetworkObject start, Vector2i end, int distanceFromPoint = 10,
			int stride = 10, bool dontLeaveFrame = false)
		{
			return ClosedDoorBetween(start.Position, end, distanceFromPoint, stride, dontLeaveFrame);
		}

		/// <summary>
		/// Checks for a closed door between start and end.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="distanceFromPoint">How far to check around each point for a door object.</param>
		/// <param name="stride">The distance between points to check in the path.</param>
		/// <param name="dontLeaveFrame">Should the current frame not be left?</param>
		/// <returns>true if there's a closed door and false otherwise.</returns>
		public static bool ClosedDoorBetween(Vector2i start, NetworkObject end, int distanceFromPoint = 10,
			int stride = 10, bool dontLeaveFrame = false)
		{
			return ClosedDoorBetween(start, end.Position, distanceFromPoint, stride, dontLeaveFrame);
		}

		/// <summary>
		/// Checks for a closed door between start and end.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="distanceFromPoint">How far to check around each point for a door object.</param>
		/// <param name="stride">The distance between points to check in the path.</param>
		/// <param name="dontLeaveFrame">Should the current frame not be left?</param>
		/// <returns>true if there's a closed door and false otherwise.</returns>
		public static bool ClosedDoorBetween(Vector2i start, Vector2i end, int distanceFromPoint = 10, int stride = 10,
			bool dontLeaveFrame = false)
		{
			var doors = LokiPoe.ObjectManager.Doors.Where(d => !d.IsOpened).ToList();

			if (!doors.Any())
				return false;

			var path = ExilePather.GetPointsOnSegment(start, end, dontLeaveFrame);

			for (var i = 0; i < path.Count; i += stride)
			{
				foreach (var door in doors)
				{
					if (door.Position.Distance(path[i]) <= distanceFromPoint)
					{
						return true;
					}
				}
			}

			return false;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="distanceFromPoint"></param>
		/// <param name="dontLeaveFrame">Should the current frame not be left?</param>
		/// <returns></returns>
		public static int NumberOfMobsBetween(NetworkObject start, NetworkObject end, int distanceFromPoint = 5,
			bool dontLeaveFrame = false)
		{
			var mobs = LokiPoe.ObjectManager.GetObjectsByType<Monster>().Where(d => d.IsActive).ToList();
			if (!mobs.Any())
				return 0;

			var path = ExilePather.GetPointsOnSegment(start.Position, end.Position, dontLeaveFrame);

			var count = 0;
			for (var i = 0; i < path.Count; i += 10)
			{
				foreach (var mob in mobs)
				{
					if (mob.Position.Distance(path[i]) <= distanceFromPoint)
					{
						++count;
					}
				}
			}

			return count;
		}

		/// <summary>
		/// Returns the number of mobs near a target.
		/// </summary>
		/// <param name="target"></param>
		/// <param name="distance"></param>
		/// <param name="dead"></param>
		/// <returns></returns>
		public static int NumberOfMobsNear(NetworkObject target, float distance, bool dead = false)
		{
			var mpos = target.Position;

			var curCount = 0;

			foreach (var mob in LokiPoe.ObjectManager.Objects.OfType<Monster>())
			{
				if (mob.Id == target.Id)
				{
					continue;
				}

				// If we're only checking for dead mobs... then... yeah...
				if (dead)
				{
					if (!mob.IsDead)
					{
						continue;
					}
				}
				else if (!mob.IsActive)
				{
					continue;
				}

				if (mob.Position.Distance(mpos) < distance)
				{
					curCount++;
				}
			}

			return curCount;
		}

		/// <summary>
		/// Returns a location where a portal should be if we're in a town.
		/// </summary>
		/// <returns>A location where a portal should come into view.</returns>
		public static Vector2i GuessPortalLocation()
		{
			var curArea = LokiPoe.LocalData.WorldArea.Id.ToLowerInvariant();

			if (curArea.EndsWith("1_town"))
			{
				return new Vector2i(151, 246);
			}

			if (curArea.EndsWith("2_town"))
			{
				return new Vector2i(246, 165);
			}

			if (curArea.EndsWith("3_town"))
			{
				return new Vector2i(217, 226);
			}

			if (curArea.EndsWith("4_town"))
			{
				return new Vector2i(286, 491);
			}

			throw new Exception(String.Format("GuessPortalLocation called when curArea = {0}", curArea));
		}

		/// <summary>
		/// Returns a location where the stash should be if we're in a town.
		/// </summary>
		/// <returns>A location where the stash should come into view.</returns>
		public static Vector2i GuessStashLocation()
		{
			var curArea = LokiPoe.LocalData.WorldArea.Id.ToLowerInvariant();

			if (curArea.EndsWith("1_town"))
			{
				return new Vector2i(246, 266);
			}

			if (curArea.EndsWith("2_town"))
			{
				return new Vector2i(178, 195);
			}

			if (curArea.EndsWith("3_town"))
			{
				return new Vector2i(206, 306);
			}

			if (curArea.EndsWith("4_town"))
			{
				return new Vector2i(199, 509);
			}

			throw new Exception(string.Format("GuessStashLocation called when curArea = {0}", curArea));
		}

		/// <summary>
		/// Returns a location where the waypoint should be if we're in a town.
		/// </summary>
		/// <returns>A location where the waypoint should come into view.</returns>
		public static Vector2i GuessWaypointLocation()
		{
			var curArea = LokiPoe.LocalData.WorldArea.Id.ToLowerInvariant();

			if (curArea.EndsWith("1_town"))
			{
				return new Vector2i(196, 172);
			}

			if (curArea.EndsWith("2_town"))
			{
				return new Vector2i(188, 135);
			}

			if (curArea.EndsWith("3_town"))
			{
				return new Vector2i(217, 226);
			}

			if (curArea.EndsWith("4_town"))
			{
				return new Vector2i(286, 491);
			}

			throw new Exception(String.Format("GuessWaypointLocation called when curArea = {0}", curArea));
		}

		/// <summary>
		/// Returns a location where the area transition should be if we're in a town.
		/// </summary>
		/// <returns>A location where the waypoint should come into view.</returns>
		public static Vector2i GuessAreaTransitionLocation(string townExitName)
		{
			var curArea = LokiPoe.LocalData.WorldArea.Id.ToLowerInvariant();

			if (curArea.EndsWith("1_town"))
			{
				if (townExitName == "The Coast")
				{
					return new Vector2i(319, 212);
				}
			}

			if (curArea.EndsWith("2_town"))
			{
				if (townExitName == "The Old Fields")
				{
					return new Vector2i(299, 173);
				}

				if (townExitName == "The Riverways")
				{
					return new Vector2i(78, 172);
				}

				if (townExitName == "The Southern Forest")
				{
					return new Vector2i(186, 89);
				}
			}

			if (curArea.EndsWith("3_town"))
			{
				if (townExitName == "The Slums")
				{
					return new Vector2i(511, 408);
				}

				if (townExitName == "The City of Sarn")
				{
					return new Vector2i(296, 75);
				}
			}

			if (curArea.EndsWith("4_town"))
			{
				if (townExitName == "The Aqueduct")
				{
					return new Vector2i(259, 394);
				}

				if (townExitName == "The Dried Lake")
				{
					return new Vector2i(94, 441);
				}

				if (townExitName == "The Mines Level 1")
				{
					return new Vector2i(328, 624);
				}
			}

			throw new Exception(String.Format(
				"GuessAreaTransitionLocation called when curArea = {0} for exit {1}", curArea, townExitName));
		}

		/// <summary>
		/// Returns hardcoded locations for npcs in a town. We need to make sure these don't change while we aren't looking!
		/// Ideally, we'd explore town to find the location if the npc object was not in view.
		/// </summary>
		public static Tuple<string, Vector2i> GuessWeaponsNpcLocation()
		{
			var curArea = LokiPoe.LocalData.WorldArea.Id.ToLowerInvariant();

			var npcName = "";

			if (curArea.EndsWith("1_town"))
			{
				npcName = "Tarkleigh";
			}

			if (curArea.EndsWith("2_town"))
			{
				npcName = "Greust";
			}

			if (curArea.EndsWith("3_town"))
			{
				npcName = "Hargan";
			}

			if (curArea.EndsWith("4_town"))
			{
				npcName = "Kira";
			}

			if (npcName != "")
			{
				return new Tuple<string, Vector2i>(npcName, GuessNpcLocation(npcName));
			}

			throw new Exception(String.Format("GuessWeaponsNpcLocation called when curArea = {0}.", curArea));
		}

		/// <summary>
		/// Returns hardcoded locations for npcs in a town. We need to make sure these don't change while we aren't looking!
		/// Ideally, we'd explore town to find the location if the npc object was not in view.
		/// </summary>
		public static Vector2i GuessNpcLocation(string npcName)
		{
			var curArea = LokiPoe.LocalData.WorldArea.Id.ToLowerInvariant();

			if (curArea.EndsWith("1_town"))
			{
				if (npcName == "Nessa")
					return new Vector2i(268, 253);

				if (npcName == "Tarkleigh")
					return new Vector2i(312, 189);
			}

			if (curArea.EndsWith("2_town"))
			{
				if (npcName == "Greust")
					return new Vector2i(192, 173);

				if (npcName == "Yeena")
					return new Vector2i(162, 240);
			}

			if (curArea.EndsWith("3_town"))
			{
				if (npcName == "Clarissa")
					return new Vector2i(147, 326);

				if (npcName == "Hargan")
					return new Vector2i(281, 357);
			}

			if (curArea.EndsWith("4_town"))
			{
				if (npcName == "Kira")
					return new Vector2i(169, 500);

				if (npcName == "Petarus and Vanja")
					return new Vector2i(204, 546);
			}

			return Vector2i.Zero;
		}

		/// <summary>
		/// Returns hardcoded locations for npcs in a town. We need to make sure these don't change while we aren't looking!
		/// Ideally, we'd explore town to find the location if the npc object was not in view.
		/// </summary>
		public static Tuple<string, Vector2i> GuessAccessoryNpcLocation()
		{
			var curArea = LokiPoe.LocalData.WorldArea.Id.ToLowerInvariant();

			var npcName = "";

			if (curArea.EndsWith("1_town"))
			{
				npcName = "Nessa";
			}

			if (curArea.EndsWith("2_town"))
			{
				npcName = "Yeena";
			}

			if (curArea.EndsWith("3_town"))
			{
				npcName = "Clarissa";
			}

			if (curArea.EndsWith("4_town"))
			{
				npcName = "Petarus and Vanja";
			}

			if (npcName != "")
			{
				return new Tuple<string, Vector2i>(npcName, GuessNpcLocation(npcName));
			}

			throw new Exception(String.Format("GuessAccessoryNpcLocation called when curArea = {0}.", curArea));
		}
	}
}