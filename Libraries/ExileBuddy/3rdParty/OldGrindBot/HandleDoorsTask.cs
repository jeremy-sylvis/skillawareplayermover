﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;
using Loki.Common;

namespace OldGrindBot
{
	/// <summary>
	/// This task handles opening doors.
	/// </summary>
	public class HandleDoorsTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private int _lastDoorId;
		private int _lastDoorTries;
		private bool _lastDoorLeave;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "HandleDoorsTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task handles opening doors.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();

				return true;
			}

			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsDead || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout || LokiPoe.Me.IsInMapRoom)
				return false;

			var door = LokiPoe.ObjectManager.Doors.Where(
				d => d.Distance < 20 && !d.IsOpened && !Blacklist.Contains(d.Id))
				.OrderBy(d => d.Distance)
				.FirstOrDefault();
			if (door == null)
			{
				return false;
			}

			var id = door.Id;

			var pos = ExilePather.FastWalkablePositionFor(door);

			await Coroutines.FinishCurrentAction();

			await Coroutines.CloseBlockingWindows();

			if (_lastDoorId == id)
			{
				_lastDoorTries++;
				if (_lastDoorTries > 5)
				{
					if (_lastDoorLeave)
					{
						Log.ErrorFormat(
							"[HandleDoorsTask] We tried many times to open this door, but have failed. Now returning to town for a new run.");
						await CoroutinesV3.CreateAndTakePortalToTown();
						return true;
					}
					else
					{
						Blacklist.Add(id, TimeSpan.FromSeconds(5), "Issue opening door.");
						_lastDoorLeave = true;
						_lastDoorTries = 0;
						return true;
					}
				}
			}
			else
			{
				_lastDoorTries = 0;
				_lastDoorId = id;
				_lastDoorLeave = false;
			}

			Log.InfoFormat("[HandleDoorsTask] A door was found at {0}. Now walking towards it to open it.", pos);
			if (_lastDoorTries != 0)
			{
				Log.InfoFormat("[HandleDoorsTask] This is attempt #{0} to open this door.", _lastDoorTries);
			}

			var sw = Stopwatch.StartNew();

			var pathDistance = ExilePather.PathDistance(LokiPoe.MyPosition, pos);
			while (pathDistance > 20 && door.Distance > 20)
			{
				Log.InfoFormat("[HandleDoorsTask] Now moving towards the door since it is {0} ({1}) away.", pathDistance,
					door.Distance);

				if (!PlayerMover.MoveTowards(pos))
				{
					Log.ErrorFormat("[HandleDoorsTask] MoveTowards returned false for {0}.", pos);
					await Coroutines.FinishCurrentAction();
				}

				pathDistance = ExilePather.PathDistance(LokiPoe.MyPosition, pos);

				if (sw.ElapsedMilliseconds > 5000)
				{
					Log.ErrorFormat(
						"[HandleDoorsTask] We have spent {0} trying to move in range of the door. Now returning to town for a new run.",
						sw.Elapsed);
					await CoroutinesV3.CreateAndTakePortalToTown();
					return true;
				}

				await Coroutines.LatencyWait();
			}

			await Coroutines.FinishCurrentAction();

			await Coroutines.CloseBlockingWindows();

			if (!LokiPoe.ConfigManager.IsAlwaysHighlightEnabled)
			{
				LokiPoe.Input.SimulateKeyEvent(LokiPoe.Input.Binding.highlight_toggle, true, false, false);

				await Coroutines.ReactionWait();
			}

			door = LokiPoe.ObjectManager.GetObjectById<TriggerableBlockage>(id);
			if (!await Coroutines.InteractWith(door))
			{
				Log.ErrorFormat("[HandleDoorsTask] The door could not be opened.");
			}

			await Coroutines.ReactionWait();

			// Smooth out chest opening as well. We don't want to wait for strongboxes though.
			sw = Stopwatch.StartNew();
			while (!door.IsOpened)
			{
				await Coroutine.Yield();

				if (sw.ElapsedMilliseconds > 1000)
					break;
			}

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[HandleDoorsTask] Now resetting task state.");
			_lastDoorId = 0;
			_lastDoorTries = 0;
		}
	}
}