﻿using System;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Common;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary>
	/// This task handles leaving the current area after exploration and all other tasks are complete.
	/// This task only handles local area transitions for boss areas.
	/// </summary>
	public class LeaveCurrentBossAreaTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private Vector2i? _exploreLocation;
		private bool _inBossArea = true;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "LeaveCurrentBossAreaTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task handles leaving the current area after exploration and all other tasks are complete.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();

				if (!newArea.IsTown)
				{
					OldGrindBotSettings.Instance.IsLeavingArea = false;
				}

				return true;
			}

			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsDead || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout)
				return false;

			if (!_inBossArea)
				return false;

			var worldAreaId = AreaStateCache.Current.WorldArea.Id.Substring(2);
			if (_exploreLocation == null)
			{
				if (worldAreaId == "1_7_2") // The Upper Prison
				{
				}
				if (worldAreaId == "1_11_2") // The Cavern of Anger
				{
				}
				if (worldAreaId == "3_18_2") // The Upper Sceptre of God
				{
				}
				if (worldAreaId == "2_10") // The Weaver's Chambers
				{
					if (AreaStateCache.Current.HasLocation("The Weaver's Chambers"))
					{
						var location = AreaStateCache.Current.GetLocations("The Weaver's Chambers").First();
						var cmd = new PathfindingCommand(LokiPoe.MyPosition, location.Position, 15, false, 9);

						var pf = ExilePather.FindPath(ref cmd);
						if (pf)
						{
							_exploreLocation = location.Position;
							Log.DebugFormat("[TravelThroughBossAreasTask] Explore location set to {0}.",
								_exploreLocation);
						}
					}

					return (_exploreLocation != null);
				}
				if (worldAreaId == "3_14_3") // The Lunaris Temple Level 3
				{
					var location = StaticLocationManager.GetStaticLocation("Piety (Down)");
					var cmd = new PathfindingCommand(LokiPoe.MyPosition, location, 15, false, 9);

					var pf = ExilePather.FindPath(ref cmd);
					if (pf)
					{
						_exploreLocation = location;
						Log.DebugFormat("[TravelThroughBossAreasTask] Explore location set to {0}.",
							_exploreLocation);
					}

					return (_exploreLocation != null);
				}

				Log.DebugFormat(
					"[LeaveCurrentBossAreaTask] We are not currently in a boss area, or a supported boss area.");
				_inBossArea = false;
			}
			else
			{
				if (worldAreaId == "1_7_2") // The Upper Prison
				{
				}
				else if (worldAreaId == "1_11_2") // The Cavern of Anger
				{
				}
				else if (worldAreaId == "2_10") // The Weaver's Chambers
				{
					var at = LokiPoe.ObjectManager.AreaTransition("The Weaver's Chambers");
					if (at != null && at.Distance < 35)
					{
						var taterr = await CoroutinesV3.TakeAreaTransition(at.Name, false, -1, true);
						Log.DebugFormat("[LeaveCurrentBossAreaTask] TakeAreaTransitionCoroutine returned {0}.", taterr);

						_exploreLocation = null;
						Explorer.GetCurrent().Reset();
						AreaStateCache.Current.ResetCurrentAnchorPoint();
						AreaStateCache.Current.ResetTimeInArea();
						StaticLocationManager.UpdateStaticLocations();
						AreaStateCache.Current.ClearChestLocations();
						AreaStateCache.Current.ClearItemLocations();

						OldGrindBotSettings.Instance.IsLeavingArea = true;
						Log.DebugFormat("[LeaveCurrentBossAreaTask] IsLeavingArea = true.");

						await Loki.Bot.Utility.BroadcastEventLogic("oldgrindbot_local_area_changed_event");

						return true;
					}
				}
				else if (worldAreaId == "3_14_3") // The Lunaris Temple Level 3
				{
					var at = LokiPoe.ObjectManager.PietyArenaPortal2;
					if (at != null && at.Distance < 35)
					{
						var taterr = await CoroutinesV3.TakeAreaTransition(at, false, -1, true);
						Log.DebugFormat("[LeaveCurrentBossAreaTask] TakeAreaTransitionCoroutine returned {0}.", taterr);

						_exploreLocation = null;
						Explorer.GetCurrent().Reset();
						AreaStateCache.Current.ResetCurrentAnchorPoint();
						AreaStateCache.Current.ResetTimeInArea();
						StaticLocationManager.UpdateStaticLocations();
						AreaStateCache.Current.ClearChestLocations();
						AreaStateCache.Current.ClearItemLocations();

						OldGrindBotSettings.Instance.IsLeavingArea = true;
						Log.DebugFormat("[LeaveCurrentBossAreaTask] IsLeavingArea = true.");

						await Loki.Bot.Utility.BroadcastEventLogic("oldgrindbot_local_area_changed_event");

						return true;
					}
				}
				else if (worldAreaId == "3_18_2") // The Upper Sceptre of God
				{
				}
				else if (worldAreaId == "2_14_3") // The Ancient Pyramid
				{
				}
				else
				{
					return false;
				}

				var pos = _exploreLocation.Value;
				if (LokiPoe.MyPosition.Distance(pos) < 12)
				{
					Log.DebugFormat("[TravelThroughBossAreasTask] We are in range of the location {0}.",
						pos);
					_exploreLocation = null;
				}
				else
				{
					if (!PlayerMover.MoveTowards(pos))
					{
						Log.ErrorFormat("[TravelThroughBossAreasTask] MoveTowards failed for {0}.",
							pos);
						_exploreLocation = null;
						await Coroutines.FinishCurrentAction();
					}
				}

				return true;
			}
			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
			Reset();
		}

		private void Reset()
		{
			Log.DebugFormat("[LeaveCurrentBossAreaTask] Now resetting task state.");
			_exploreLocation = null;
			_inBossArea = true;
		}
	}
}