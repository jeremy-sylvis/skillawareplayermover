﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Game;
using Loki.Common;

namespace OldGrindBot
{
	/// <summary>
	/// This task handles exploring an area.
	/// </summary>
	public class ExploreTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private readonly Stopwatch _displayStopwatch = Stopwatch.StartNew();

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "ExploreTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task handles exploring an area.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsDead || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout)
				return false;

			var explorer = Explorer.GetCurrent();
			if (explorer != null && explorer.HasLocation)
			{
				var location = explorer.Location;

				if (_displayStopwatch.ElapsedMilliseconds > 1000)
				{
					Log.DebugFormat("[ExploreTask] Now exploring to the location {0} ({1}) [{2} %].", location,
						LokiPoe.MyPosition.Distance(location), explorer.PercentComplete);
					_displayStopwatch.Restart();
				}

				if (!PlayerMover.MoveTowards(location))
				{
					Log.ErrorFormat("[ExploreTask] MoveTowards failed for {0}.", location);
					explorer.Ignore(location);
					await Coroutines.FinishCurrentAction();
				}

				return true;
			}

			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}
	}
}