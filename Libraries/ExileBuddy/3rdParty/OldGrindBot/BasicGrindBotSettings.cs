﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using log4net;
using Loki.Common;
using System.Collections.Generic;
using Loki;
using Loki.Game;
using Newtonsoft.Json;

namespace OldGrindBot
{
	/// <summary>Settings for the example bot. </summary>
	public class OldGrindBotSettings : JsonSettings
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private static OldGrindBotSettings _instance;

		/// <summary>The current instance for this class. </summary>
		public static OldGrindBotSettings Instance
		{
			get
			{
				return _instance ?? (_instance = new OldGrindBotSettings());
			}
		}

		/// <summary>The default ctor. Will use the settings path "OldGrindBot".</summary>
		public OldGrindBotSettings()
			: base(GetSettingsFilePath(Configuration.Instance.Name, string.Format("{0}.json", "OldGrindBot")))
		{
			// Setup defaults here if needed for properties that don't support DefaultValue.

			if (_transmutationShardsCell == default(WpfVector2i) || 
				_alchemyShardsCell == default(WpfVector2i) || 
				_scrollFragmentsCell == default(WpfVector2i) || 
				_alterationShardsCell == default(WpfVector2i) || 
				_tpScrollsCell == default(WpfVector2i) || 
				_idScrollsCell == default(WpfVector2i) || 
				_perandusCoinsCell == default(WpfVector2i) || 
				_silverCoinsCell == default(WpfVector2i))
			{
				RandomizeItemLocations();
			}

			if (GlobalChestsToIgnore == null)
			{
				GlobalChestsToIgnore = new ObservableCollection<SettingNameEnabledWrapper>
				{
					new SettingNameEnabledWrapper
					{
						Name = "Chest",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Trunk",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Copper Chest",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Large Chest",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Golden Chest",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Tribal Chest",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Sarcophagus",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Barrel",
						IsEnabled = true
					},
					new SettingNameEnabledWrapper
					{
						Name = "Basket",
						IsEnabled = true
					},
					new SettingNameEnabledWrapper
					{
						Name = "Boulder",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Cairn",
						IsEnabled = true
					},
					new SettingNameEnabledWrapper
					{
						Name = "Crate",
						IsEnabled = true
					},
					new SettingNameEnabledWrapper
					{
						Name = "Pot",
						IsEnabled = true
					},
					new SettingNameEnabledWrapper
					{
						Name = "Gold Pot",
						IsEnabled = true
					},
					new SettingNameEnabledWrapper
					{
						Name = "Urn",
						IsEnabled = true
					},
					new SettingNameEnabledWrapper
					{
						Name = "Bone Pile",
						IsEnabled = true
					},
					new SettingNameEnabledWrapper
					{
						Name = "Corpse",
						IsEnabled = true
					},
					new SettingNameEnabledWrapper
					{
						Name = "Weapon Rack",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Armour Rack",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Scribe's Rack",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Crucified Corpse",
						IsEnabled = true
					},
					new SettingNameEnabledWrapper
					{
						Name = "Bound Corpse",
						IsEnabled = true
					},
					new SettingNameEnabledWrapper
					{
						Name = "Impaled Corpse",
						IsEnabled = true
					},
					new SettingNameEnabledWrapper
					{
						Name = "Cocoon",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Rhoa Nest",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Slave Girl",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Tolman",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Strange Device",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Altar",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Plum",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Ornate Chest",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Stash",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Blackguard Chest",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Supply Container",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Book Stand",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Booty Chest",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Arcanist's Strongbox",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Armourer's Strongbox",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Blacksmith's Strongbox",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Artisan's Strongbox",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Cartographer's Strongbox",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Chemist's Strongbox",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Gemcutter's Strongbox",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Jeweller's Strongbox",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Large Strongbox",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Ornate Strongbox",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Diviner's Strongbox",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Strongbox",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Vaal Vessel",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Perandus Chest",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Perandus Cache",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Perandus Hoard",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Perandus Coffer",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Perandus Jewellery Box",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Perandus Safe",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Perandus Treasury",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Perandus Wardrobe",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Perandus Catalogue",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Perandus Trove",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Perandus Locker",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Perandus Archive",
						IsEnabled = false
					},
					new SettingNameEnabledWrapper
					{
						Name = "Perandus Tackle Box",
						IsEnabled = false
					},
				};
			}
		}

		private ObservableCollection<SettingNameEnabledWrapper> _globalChestsToIgnore;

		private bool _autoDoNotDisturb;
		private bool _disableDoNotDisturbOnBotStop;

		private int _stopAfterTickExceptions;
		private bool _logoutAfterTickExceptionThreshold;

		private bool _isLeavingArea;
		private bool _needsToStash;
		private bool _cameFromPortal;
		private int _needsTownRun;

		private string _grindZoneName;
		private string _grindZoneDifficulty;
		private int _deathsBeforeNewInstance;

		private bool _enableStashingOnFreeSpacePercent;
		private int _freeSpaceToTriggerStashingPercent;

		private bool _enableEarlyExplorationComplete;
		private int _explorationCompletePercent;

		private bool _fillIdScrollStacks;
		private bool _dontStashIdScrolls;
		private WpfVector2i _idScrollsCell;

		private bool _fillTpScrollStacks;
		private bool _dontStashTpScrolls;
		private WpfVector2i _tpScrollsCell;

		private bool _dontStashPerandusCoins;
		private WpfVector2i _perandusCoinsCell;

		private bool _dontStashSilverCoins;
		private WpfVector2i _silverCoinsCell;

		private bool _fillAlchemy;
		private bool _fillChaos;
		private bool _fillChance;
		private bool _fillAlteration;
		private bool _fillAugmentation;
		private bool _fillTransmutation;
		private bool _fillScour;
		private bool _fillVaal;

		private bool _dontStashTransmutationShards;
		private WpfVector2i _transmutationShardsCell;

		private bool _dontStashAlchemyShards;
		private WpfVector2i _alchemyShardsCell;

		private bool _dontStashScrollFragments;
		private WpfVector2i _scrollFragmentsCell;

		private bool _dontStashAlterationShards;
		private WpfVector2i _alterationShardsCell;

		private ExplorationCompleteBehavior _explorationCompleteBehavior1;
		private ExplorationCompleteBehavior _explorationCompleteBehavior2;
		private ExplorationCompleteBehavior _explorationCompleteBehavior3;
		private ExplorationCompleteBehavior _explorationCompleteBehavior4;
		private ExplorationCompleteBehavior _explorationCompleteBehavior5;

		private bool _takeCorruptedAreas;
		private bool _newRunAfterCorruptedAreaExplored;

		private bool _blacklistCorruptedAreaTransition;
		private int _corruptedAreaTransitionId;

		private int _maxWithdrawTabs;

		private int _maxInstances;

		/// <summary>A bot setting for making it loot based on the in game filter rather than a loot filter.</summary>
		public bool LootVisibleItemsOverride
		{
			get
			{
				return AreaStateCache.LootVisibleItemsOverride;
			}
			set
			{
				if (value.Equals(AreaStateCache.LootVisibleItemsOverride))
				{
					return;
				}
				AreaStateCache.LootVisibleItemsOverride = value;
				NotifyPropertyChanged(() => LootVisibleItemsOverride);
			}
		}

		/// <summary>
		/// Should the bot logout after the tick exception threshold is reached?
		/// </summary>
		[DefaultValue(false)]
		public bool LogoutAfterTickExceptionThreshold
		{
			get
			{
				return _logoutAfterTickExceptionThreshold;
			}
			set
			{
				if (value.Equals(_logoutAfterTickExceptionThreshold))
				{
					return;
				}
				_logoutAfterTickExceptionThreshold = value;
				NotifyPropertyChanged(() => LogoutAfterTickExceptionThreshold);
				Save();
			}
		}

		/// <summary>
		/// The number of tick exceptions to stop the bot at..
		/// </summary>
		[DefaultValue(15)]
		public int StopAfterTickExceptions
		{
			get
			{
				return _stopAfterTickExceptions;
			}
			set
			{
				if (value.Equals(_stopAfterTickExceptions))
				{
					return;
				}
				_stopAfterTickExceptions = value;
				NotifyPropertyChanged(() => StopAfterTickExceptions);
				Save();
			}
		}

		/// <summary>
		/// The max instance count to stop the bot to ensure too many instances are not created.
		/// </summary>
		[DefaultValue(10)]
		public int MaxInstances
		{
			get
			{
				return _maxInstances;
			}
			set
			{
				if (value.Equals(_maxInstances))
				{
					return;
				}
				_maxInstances = value;
				NotifyPropertyChanged(() => MaxInstances);
				Save();
			}
		}

		/// <summary>
		/// How many tabs should the bot use to withdraw items from stash.
		/// </summary>
		[DefaultValue(8)]
		public int MaxWithdrawTabs
		{
			get
			{
				return _maxWithdrawTabs;
			}
			set
			{
				if (value.Equals(_maxWithdrawTabs))
				{
					return;
				}
				_maxWithdrawTabs = value;
				NotifyPropertyChanged(() => MaxWithdrawTabs);
				Save();
			}
		}

		/// <summary>
		/// Should the bot withdraw this type of currency. 
		/// This is to useful for rolling items/strongboxes outside of town.
		/// </summary>
		[DefaultValue(false)]
		public bool FillAlchemyStacks
		{
			get
			{
				return _fillAlchemy;
			}
			set
			{
				if (value.Equals(_fillAlchemy))
				{
					return;
				}
				_fillAlchemy = value;
				NotifyPropertyChanged(() => FillAlchemyStacks);
				Save();
			}
		}

		/// <summary>
		/// Should the bot withdraw this type of currency. 
		/// This is to useful for rolling items/strongboxes outside of town.
		/// </summary>
		[DefaultValue(false)]
		public bool FillChaosStacks
		{
			get
			{
				return _fillChaos;
			}
			set
			{
				if (value.Equals(_fillChaos))
				{
					return;
				}
				_fillChaos = value;
				NotifyPropertyChanged(() => FillChaosStacks);
				Save();
			}
		}

		/// <summary>
		/// Should the bot withdraw this type of currency. 
		/// This is to useful for rolling items/strongboxes outside of town.
		/// </summary>
		[DefaultValue(false)]
		public bool FillChanceStacks
		{
			get
			{
				return _fillChance;
			}
			set
			{
				if (value.Equals(_fillChance))
				{
					return;
				}
				_fillChance = value;
				NotifyPropertyChanged(() => FillChanceStacks);
				Save();
			}
		}

		/// <summary>
		/// Should the bot withdraw this type of currency. 
		/// This is to useful for rolling items/strongboxes outside of town.
		/// </summary>
		[DefaultValue(false)]
		public bool FillAlterationStacks
		{
			get
			{
				return _fillAlteration;
			}
			set
			{
				if (value.Equals(_fillAlteration))
				{
					return;
				}
				_fillAlteration = value;
				NotifyPropertyChanged(() => FillAlterationStacks);
				Save();
			}
		}

		/// <summary>
		/// Should the bot withdraw this type of currency. 
		/// This is to useful for rolling items/strongboxes outside of town.
		/// </summary>
		[DefaultValue(false)]
		public bool FillAugmentationStacks
		{
			get
			{
				return _fillAugmentation;
			}
			set
			{
				if (value.Equals(_fillAugmentation))
				{
					return;
				}
				_fillAugmentation = value;
				NotifyPropertyChanged(() => FillAugmentationStacks);
				Save();
			}
		}

		/// <summary>
		/// Should the bot withdraw this type of currency. 
		/// This is to useful for rolling items/strongboxes outside of town.
		/// </summary>
		[DefaultValue(false)]
		public bool FillTransmutationStacks
		{
			get
			{
				return _fillTransmutation;
			}
			set
			{
				if (value.Equals(_fillTransmutation))
				{
					return;
				}
				_fillTransmutation = value;
				NotifyPropertyChanged(() => FillTransmutationStacks);
				Save();
			}
		}

		/// <summary>
		/// Should the bot withdraw this type of currency. 
		/// This is to useful for rolling items/strongboxes outside of town.
		/// </summary>
		[DefaultValue(false)]
		public bool FillScourStacks
		{
			get
			{
				return _fillScour;
			}
			set
			{
				if (value.Equals(_fillScour))
				{
					return;
				}
				_fillScour = value;
				NotifyPropertyChanged(() => FillScourStacks);
				Save();
			}
		}

		/// <summary>
		/// Should the bot withdraw this type of currency. 
		/// This is to useful for rolling items/strongboxes outside of town.
		/// </summary>
		[DefaultValue(false)]
		public bool FillVaalStacks
		{
			get
			{
				return _fillVaal;
			}
			set
			{
				if (value.Equals(_fillVaal))
				{
					return;
				}
				_fillVaal = value;
				NotifyPropertyChanged(() => FillVaalStacks);
				Save();
			}
		}

		/// <summary>A bot setting for keeping track of state.</summary>
		[JsonIgnore]
		public int CorruptedAreaTransitionId
		{
			get
			{
				return _corruptedAreaTransitionId;
			}
			set
			{
				if (value.Equals(_corruptedAreaTransitionId))
				{
					return;
				}
				_corruptedAreaTransitionId = value;
				NotifyPropertyChanged(() => CorruptedAreaTransitionId);
			}
		}

		/// <summary>A bot setting for keeping track of state.</summary>
		[JsonIgnore]
		[DefaultValue(false)]
		public bool BlacklistCorruptedAreaTransition
		{
			get
			{
				return _blacklistCorruptedAreaTransition;
			}
			set
			{
				if (value.Equals(_blacklistCorruptedAreaTransition))
				{
					return;
				}
				_blacklistCorruptedAreaTransition = value;
				NotifyPropertyChanged(() => BlacklistCorruptedAreaTransition);
			}
		}

		/// <summary>
		/// After exploring a corrupted area, should the bot return to town to create a new instance of the parent area? This is for
		/// corrpted area farming.
		/// </summary>
		[DefaultValue(false)]
		public bool NewRunAfterCorruptedAreaExplored
		{
			get
			{
				return _newRunAfterCorruptedAreaExplored;
			}
			set
			{
				if (value.Equals(_newRunAfterCorruptedAreaExplored))
				{
					return;
				}
				_newRunAfterCorruptedAreaExplored = value;
				NotifyPropertyChanged(() => NewRunAfterCorruptedAreaExplored);
				Save();
			}
		}

		/// <summary>Should the bot take corrupted areas?</summary>
		[DefaultValue(false)]
		public bool TakeCorruptedAreas
		{
			get
			{
				return _takeCorruptedAreas;
			}
			set
			{
				if (value.Equals(_takeCorruptedAreas))
				{
					return;
				}
				_takeCorruptedAreas = value;
				NotifyPropertyChanged(() => TakeCorruptedAreas);
				Save();
			}
		}

		/// <summary>
		/// A collection of chests the bot will ignore opening, unless absolutely necessary.
		/// </summary>
		public ObservableCollection<SettingNameEnabledWrapper> GlobalChestsToIgnore
		{
			get
			{
				return _globalChestsToIgnore;
			}
			set
			{
				if (Equals(value, _globalChestsToIgnore))
				{
					return;
				}
				_globalChestsToIgnore = value;
				NotifyPropertyChanged(() => GlobalChestsToIgnore);
			}
		}

		/// <summary>
		/// Should the bot complete exploration early when in the grind zone?
		/// </summary>
		[DefaultValue(false)]
		public bool EnableEarlyExplorationComplete
		{
			get
			{
				return _enableEarlyExplorationComplete;
			}
			set
			{
				if (value.Equals(_enableEarlyExplorationComplete))
				{
					return;
				}
				_enableEarlyExplorationComplete = value;
				NotifyPropertyChanged(() => EnableEarlyExplorationComplete);
				Save();
			}
		}

		/// <summary>
		/// The percent to trigger exploration complete whne checking early.
		/// </summary>
		[DefaultValue(75)]
		public int ExplorationCompletePercent
		{
			get
			{
				return _explorationCompletePercent;
			}
			set
			{
				if (value.Equals(_explorationCompletePercent))
				{
					return;
				}
				_explorationCompletePercent = value;
				NotifyPropertyChanged(() => ExplorationCompletePercent);
				Save();
			}
		}

		/// <summary>
		/// Should the bot use the FreeSpaceToTriggerStashingPercent setting to trigger less stashing runs while in town?
		/// </summary>
		[DefaultValue(false)]
		public bool EnableStashingOnFreeSpacePercent
		{
			get
			{
				return _enableStashingOnFreeSpacePercent;
			}
			set
			{
				if (value.Equals(_enableStashingOnFreeSpacePercent))
				{
					return;
				}
				_enableStashingOnFreeSpacePercent = value;
				NotifyPropertyChanged(() => EnableStashingOnFreeSpacePercent);
				Save();
			}
		}

		/// <summary>
		/// The percent to trigger stashing logic in town. This is to prevent frequent stashing actions.
		/// </summary>
		[DefaultValue(25)]
		public int FreeSpaceToTriggerStashingPercent
		{
			get
			{
				return _freeSpaceToTriggerStashingPercent;
			}
			set
			{
				if (value.Equals(_freeSpaceToTriggerStashingPercent))
				{
					return;
				}
				_freeSpaceToTriggerStashingPercent = value;
				NotifyPropertyChanged(() => FreeSpaceToTriggerStashingPercent);
				Save();
			}
		}

		/// <summary>Should the bot stash this type of scroll?</summary>
		[DefaultValue(true)]
		public bool DontStashIdScrolls
		{
			get
			{
				return _dontStashIdScrolls;
			}
			set
			{
				if (value.Equals(_dontStashIdScrolls))
				{
					return;
				}
				_dontStashIdScrolls = value;
				NotifyPropertyChanged(() => DontStashIdScrolls);
				Save();
			}
		}

		/// <summary>Should the bot fill a stack of ID scrolls before leaving town.</summary>
		[DefaultValue(false)]
		public bool FillIdScrollStacks
		{
			get
			{
				return _fillIdScrollStacks;
			}
			set
			{
				if (value.Equals(_fillIdScrollStacks))
				{
					return;
				}
				_fillIdScrollStacks = value;
				NotifyPropertyChanged(() => FillIdScrollStacks);
				Save();
			}
		}

		/// <summary>
		/// The col,row pair of where this item should be kept in inventory if it's not stashed.
		/// The values are 1-based, so the top left slot in your inventory is 1, 1 and the bottom
		/// right corner is 12,5
		/// </summary>
		public WpfVector2i IdScrollsCell
		{
			get
			{
				return _idScrollsCell;
			}
			set
			{
				if (value.Equals(_idScrollsCell))
				{
					return;
				}
				_idScrollsCell = value;
				NotifyPropertyChanged(() => IdScrollsCell);
				Save();
			}
		}

		/// <summary>Should the bot stash this type of scroll?</summary>
		[DefaultValue(true)]
		public bool DontStashTpScrolls
		{
			get
			{
				return _dontStashTpScrolls;
			}
			set
			{
				if (value.Equals(_dontStashTpScrolls))
				{
					return;
				}
				_dontStashTpScrolls = value;
				NotifyPropertyChanged(() => DontStashTpScrolls);
				Save();
			}
		}

		/// <summary>Should the bot fill a stack of TP scrolls before leaving town.</summary>
		[DefaultValue(true)]
		public bool FillTpScrollStacks
		{
			get
			{
				return _fillTpScrollStacks;
			}
			set
			{
				if (value.Equals(_fillTpScrollStacks))
				{
					return;
				}
				_fillTpScrollStacks = value;
				NotifyPropertyChanged(() => FillTpScrollStacks);
				Save();
			}
		}

		/// <summary>
		/// The col,row pair of where this item should be kept in inventory if it's not stashed.
		/// The values are 1-based, so the top left slot in your inventory is 1, 1 and the bottom
		/// right corner is 12,5
		/// </summary>
		public WpfVector2i TpScrollsCell
		{
			get
			{
				return _tpScrollsCell;
			}
			set
			{
				if (value.Equals(_tpScrollsCell))
				{
					return;
				}
				_tpScrollsCell = value;
				NotifyPropertyChanged(() => TpScrollsCell);
				Save();
			}
		}

		/// <summary>
		/// Should the bot stash this type of shard, regardless of item filter settings?
		/// This is to prevent wasted stash slots from merged shard orbs.
		/// </summary>
		[DefaultValue(true)]
		public bool DontStashTransmutationShards
		{
			get
			{
				return _dontStashTransmutationShards;
			}
			set
			{
				if (value.Equals(_dontStashTransmutationShards))
				{
					return;
				}
				_dontStashTransmutationShards = value;
				NotifyPropertyChanged(() => DontStashTransmutationShards);
				Save();
			}
		}

		/// <summary>
		/// The col,row pair of where this shard should be kept in inventory if it's not stashed.
		/// The values are 1-based, so the top left slot in your inventory is 1, 1 and the bottom
		/// right corner is 12,5
		/// </summary>
		public WpfVector2i TransmutationShardsCell
		{
			get
			{
				return _transmutationShardsCell;
			}
			set
			{
				if (value.Equals(_transmutationShardsCell))
				{
					return;
				}
				_transmutationShardsCell = value;
				NotifyPropertyChanged(() => TransmutationShardsCell);
				Save();
			}
		}

		/// <summary>
		/// Should the bot stash this type of shard, regardless of item filter settings?
		/// This is to prevent wasted stash slots from merged shard orbs.
		/// </summary>
		[DefaultValue(true)]
		public bool DontStashAlchemyShards
		{
			get
			{
				return _dontStashAlchemyShards;
			}
			set
			{
				if (value.Equals(_dontStashAlchemyShards))
				{
					return;
				}
				_dontStashAlchemyShards = value;
				NotifyPropertyChanged(() => DontStashAlchemyShards);
				Save();
			}
		}

		/// <summary>
		/// The col,row pair of where this shard should be kept in inventory if it's not stashed.
		/// The values are 1-based, so the top left slot in your inventory is 1, 1 and the bottom
		/// right corner is 12,5
		/// </summary>
		public WpfVector2i AlchemyShardsCell
		{
			get
			{
				return _alchemyShardsCell;
			}
			set
			{
				if (value.Equals(_alchemyShardsCell))
				{
					return;
				}
				_alchemyShardsCell = value;
				NotifyPropertyChanged(() => AlchemyShardsCell);
				Save();
			}
		}

		/// <summary>
		/// Should the bot stash this type of shard, regardless of item filter settings?
		/// This is to prevent wasted stash slots from merged shard orbs.
		/// </summary>
		[DefaultValue(true)]
		public bool DontStashScrollFragments
		{
			get
			{
				return _dontStashScrollFragments;
			}
			set
			{
				if (value.Equals(_dontStashScrollFragments))
				{
					return;
				}
				_dontStashScrollFragments = value;
				NotifyPropertyChanged(() => DontStashScrollFragments);
				Save();
			}
		}

		/// <summary>
		/// The col,row pair of where this shard should be kept in inventory if it's not stashed.
		/// The values are 1-based, so the top left slot in your inventory is 1, 1 and the bottom
		/// right corner is 12,5
		/// </summary>
		public WpfVector2i ScrollFragmentsCell
		{
			get
			{
				return _scrollFragmentsCell;
			}
			set
			{
				if (value.Equals(_scrollFragmentsCell))
				{
					return;
				}
				_scrollFragmentsCell = value;
				NotifyPropertyChanged(() => ScrollFragmentsCell);
				Save();
			}
		}

		/// <summary>
		/// Should the bot stash this type of shard, regardless of item filter settings?
		/// This is to prevent wasted stash slots from merged shard orbs.
		/// </summary>
		[DefaultValue(true)]
		public bool DontStashAlterationShards
		{
			get
			{
				return _dontStashAlterationShards;
			}
			set
			{
				if (value.Equals(_dontStashAlterationShards))
				{
					return;
				}
				_dontStashAlterationShards = value;
				NotifyPropertyChanged(() => DontStashAlterationShards);
				Save();
			}
		}

		/// <summary>
		/// The col,row pair of where this shard should be kept in inventory if it's not stashed.
		/// The values are 1-based, so the top left slot in your inventory is 1, 1 and the bottom
		/// right corner is 12,5
		/// </summary>
		public WpfVector2i AlterationShardsCell
		{
			get
			{
				return _alterationShardsCell;
			}
			set
			{
				if (value.Equals(_alterationShardsCell))
				{
					return;
				}
				_alterationShardsCell = value;
				NotifyPropertyChanged(() => AlterationShardsCell);
				Save();
			}
		}

		/// <summary>
		/// The col,row pair of where this item should be kept in inventory if it's not stashed.
		/// The values are 1-based, so the top left slot in your inventory is 1, 1 and the bottom
		/// right corner is 12,5
		/// </summary>
		public WpfVector2i PerandusCoinsCell
		{
			get
			{
				return _perandusCoinsCell;
			}
			set
			{
				if (value.Equals(_perandusCoinsCell))
				{
					return;
				}
				_perandusCoinsCell = value;
				NotifyPropertyChanged(() => PerandusCoinsCell);
				Save();
			}
		}

		/// <summary>
		/// The col,row pair of where this item should be kept in inventory if it's not stashed.
		/// The values are 1-based, so the top left slot in your inventory is 1, 1 and the bottom
		/// right corner is 12,5
		/// </summary>
		public WpfVector2i SilverCoinsCell
		{
			get
			{
				return _silverCoinsCell;
			}
			set
			{
				if (value.Equals(_silverCoinsCell))
				{
					return;
				}
				_silverCoinsCell = value;
				NotifyPropertyChanged(() => SilverCoinsCell);
				Save();
			}
		}

		/// <summary>Should the bot stash this type of currency?</summary>
		[DefaultValue(false)]
		public bool DontStashSilverCoins
		{
			get
			{
				return _dontStashSilverCoins;
			}
			set
			{
				if (value.Equals(_dontStashSilverCoins))
				{
					return;
				}
				_dontStashSilverCoins = value;
				NotifyPropertyChanged(() => DontStashSilverCoins);
				Save();
			}
		}

		/// <summary>Should the bot stash this type of currency?</summary>
		[DefaultValue(true)]
		public bool DontStashPerandusCoins
		{
			get
			{
				return _dontStashPerandusCoins;
			}
			set
			{
				if (value.Equals(_dontStashPerandusCoins))
				{
					return;
				}
				_dontStashPerandusCoins = value;
				NotifyPropertyChanged(() => DontStashPerandusCoins);
				Save();
			}
		}

		/// <summary>
		/// The number of deaths the bot can have in an area before creating a new instance into that zone again.
		/// </summary>
		[DefaultValue(5)]
		public int DeathsBeforeNewInstance
		{
			get
			{
				return _deathsBeforeNewInstance;
			}
			set
			{
				if (value.Equals(_deathsBeforeNewInstance))
				{
					return;
				}
				_deathsBeforeNewInstance = value;
				NotifyPropertyChanged(() => DeathsBeforeNewInstance);
				Save();
			}
		}

		/// <summary>A bot setting for keeping track of state.</summary>
		[JsonIgnore]
		public bool NeedsToStash
		{
			get
			{
				return _needsToStash;
			}
			set
			{
				if (value.Equals(_needsToStash))
				{
					return;
				}
				_needsToStash = value;
				NotifyPropertyChanged(() => NeedsToStash);
			}
		}

		/// <summary>A bot setting for keeping track of state.</summary>
		[JsonIgnore]
		public bool CameFromPortal
		{
			get
			{
				return _cameFromPortal;
			}
			set
			{
				if (value.Equals(_cameFromPortal))
				{
					return;
				}
				_cameFromPortal = value;
				NotifyPropertyChanged(() => CameFromPortal);
			}
		}

		/// <summary>A bot setting for keeping track of state.</summary>
		[JsonIgnore]
		public int NeedsTownRun
		{
			get
			{
				return _needsTownRun;
			}
			set
			{
				if (value.Equals(_needsTownRun))
				{
					return;
				}
				_needsTownRun = value;
				NotifyPropertyChanged(() => NeedsTownRun);
			}
		}

		/// <summary>A bot setting for keeping track of state.</summary>
		[JsonIgnore]
		public bool IsLeavingArea
		{
			get
			{
				return _isLeavingArea;
			}
			set
			{
				if (value.Equals(_isLeavingArea))
				{
					return;
				}
				_isLeavingArea = value;
				NotifyPropertyChanged(() => IsLeavingArea);
			}
		}

		/// <summary>The grind zone difficulty.</summary>
		[DefaultValue("Normal")]
		public string GrindZoneDifficulty
		{
			get
			{
				return _grindZoneDifficulty;
			}
			set
			{
				if (value.Equals(_grindZoneDifficulty))
				{
					return;
				}
				_grindZoneDifficulty = value;
				NotifyPropertyChanged(() => GrindZoneDifficulty);
				Save();
			}
		}

		/// <summary>The grind zone area the bot will travel to for grinding.</summary>
		[DefaultValue("The Mud Flats")]
		public string GrindZoneName
		{
			get
			{
				return _grindZoneName;
			}
			set
			{
				if (value.Equals(_grindZoneName))
				{
					return;
				}
				_grindZoneName = value;
				NotifyPropertyChanged(() => GrindZoneName);
				Save();
			}
		}

		/// <summary>
		/// Returns the current grind zone id based on the name and difficulty.
		/// </summary>
		[JsonIgnore]
		public string GrindZoneId
		{
			get
			{
                return LokiPoe.GetZoneId(GrindZoneDifficulty, GrindZoneName);
			}
		}

		private List<string> _allGrindZoneDifficulty;

		/// <summary> </summary>
		[JsonIgnore]
		public List<string> AllGrindZoneDifficulty
		{
			get
			{
				return _allGrindZoneDifficulty ?? (_allGrindZoneDifficulty = new List<string>
				{
					"Normal",
					"Cruel",
					"Merciless"
				});
			}
		}

		private List<string> _allGrindZoneNames;

		/// <summary> </summary>
		[JsonIgnore]
		public List<string> AllGrindZoneNames
		{
			get
			{
				return _allGrindZoneNames ?? (_allGrindZoneNames = new List<string>
				{
					"The Twilight Strand",
					"The Coast",
					"The Tidal Island",
					"The Mud Flats",
					"The Fetid Pool",
					"The Flooded Depths",
					"The Submerged Passage",
					"The Ledge",
					"The Climb",
					"The Lower Prison",
					"The Upper Prison",
					"Prisoner's Gate",
					"The Ship Graveyard",
					"The Ship Graveyard Cave",
					"The Cavern of Wrath",
					"The Cavern of Anger",
					"The Southern Forest",
					"The Old Fields",
					"The Den",
					"The Crossroads",
					"The Crypt Level 1",
					"The Crypt Level 2",
					"The Chamber of Sins Level 1",
					"The Chamber of Sins Level 2",
					"The Broken Bridge",
					"The Riverways",
					"The Northern Forest",
					"The Western Forest",
					"The Weaver's Chambers",
					"The Vaal Ruins",
					"The Wetlands",
					"The Dread Thicket",
					"The Caverns",
					"The Ancient Pyramid",
					"The Fellshrine Ruins",
					"The City of Sarn",
					"The Slums",
					"The Crematorium",
					"The Warehouse District",
					"The Marketplace",
					"The Catacombs",
					"The Battlefront",
					"The Solaris Temple Level 1",
					"The Solaris Temple Level 2",
					"The Docks",
					"The Slums Sewers",
					"The Warehouse Sewers",
					"The Market Sewers",
					"The Ebony Barracks",
					"The Lunaris Temple Level 1",
					"The Lunaris Temple Level 2",
					"The Imperial Gardens",
					"The Hedge Maze",
					"The Library",
					"The Archives",
					"The Sceptre of God",
					"The Upper Sceptre of God",
					"The Aqueduct",
					"The Dried Lake",
					"The Mines Level 1",
					"The Mines Level 2",
					"The Crystal Veins",
					"Kaom's Dream",
					"Kaom's Path",
					"Kaom's Stronghold",
					"Daresso's Dream",
					"The Grand Arena",
					"The Belly of the Beast Level 1",
					"The Belly of the Beast Level 2",
					"The Harvest",
				});
			}
		}

		/// <summary>
		/// Randomized the order of the fragment/scroll locations in the bottom right corner of the inventory.
		/// </summary>
		public void RandomizeItemLocations()
		{
			var positions = new List<Vector2i>
			{
				new Vector2i(5, 5),
				new Vector2i(6, 5),
				new Vector2i(7, 5),
				new Vector2i(8, 5),
				new Vector2i(9, 5),
				new Vector2i(10, 5),
				new Vector2i(11, 5),
				new Vector2i(12, 5)
			};

			positions.Shuffle();

			_transmutationShardsCell.X = positions[0].X;
			_transmutationShardsCell.Y = positions[0].Y;

			_alchemyShardsCell.X = positions[1].X;
			_alchemyShardsCell.Y = positions[1].Y;

			_scrollFragmentsCell.X = positions[2].X;
			_scrollFragmentsCell.Y = positions[2].Y;

			_alterationShardsCell.X = positions[3].X;
			_alterationShardsCell.Y = positions[3].Y;

			_idScrollsCell.X = positions[4].X;
			_idScrollsCell.Y = positions[4].Y;

			_tpScrollsCell.X = positions[5].X;
			_tpScrollsCell.Y = positions[5].Y;

			_perandusCoinsCell.X = positions[6].X;
			_perandusCoinsCell.Y = positions[6].Y;

			_silverCoinsCell.X = positions[7].X;
			_silverCoinsCell.Y = positions[7].Y;

			NotifyPropertyChanged(() => SilverCoinsCell);
			NotifyPropertyChanged(() => PerandusCoinsCell);
			NotifyPropertyChanged(() => AlchemyShardsCell);
			NotifyPropertyChanged(() => AlterationShardsCell);
			NotifyPropertyChanged(() => TransmutationShardsCell);
			NotifyPropertyChanged(() => ScrollFragmentsCell);
			NotifyPropertyChanged(() => IdScrollsCell);
			NotifyPropertyChanged(() => TpScrollsCell);
		}


		/// <summary>
		/// The first exploration complete behavior to use if possible.
		/// </summary>
		[DefaultValue(ExplorationCompleteBehavior.Waypoint)]
		public ExplorationCompleteBehavior ExplorationCompleteBehavior1
		{
			get
			{
				return _explorationCompleteBehavior1;
			}
			set
			{
				if (value.Equals(_explorationCompleteBehavior1))
				{
					return;
				}
				_explorationCompleteBehavior1 = value;
				NotifyPropertyChanged(() => ExplorationCompleteBehavior1);
				Save();
			}
		}

		/// <summary>
		/// The second exploration complete behavior to use if possible.
		/// </summary>
		[DefaultValue(ExplorationCompleteBehavior.AreaTransition)]
		public ExplorationCompleteBehavior ExplorationCompleteBehavior2
		{
			get
			{
				return _explorationCompleteBehavior2;
			}
			set
			{
				if (value.Equals(_explorationCompleteBehavior2))
				{
					return;
				}
				_explorationCompleteBehavior2 = value;
				NotifyPropertyChanged(() => ExplorationCompleteBehavior2);
				Save();
			}
		}

		/// <summary>
		/// The third exploration complete behavior to use if possible.
		/// </summary>
		[DefaultValue(ExplorationCompleteBehavior.AutoSelect)]
		public ExplorationCompleteBehavior ExplorationCompleteBehavior3
		{
			get
			{
				return _explorationCompleteBehavior3;
			}
			set
			{
				if (value.Equals(_explorationCompleteBehavior3))
				{
					return;
				}
				_explorationCompleteBehavior3 = value;
				NotifyPropertyChanged(() => ExplorationCompleteBehavior3);
				Save();
			}
		}

		/// <summary>
		/// The fourth exploration complete behavior to use if possible.
		/// </summary>
		[DefaultValue(ExplorationCompleteBehavior.AutoSelect)]
		public ExplorationCompleteBehavior ExplorationCompleteBehavior4
		{
			get
			{
				return _explorationCompleteBehavior4;
			}
			set
			{
				if (value.Equals(_explorationCompleteBehavior4))
				{
					return;
				}
				_explorationCompleteBehavior4 = value;
				NotifyPropertyChanged(() => ExplorationCompleteBehavior4);
				Save();
			}
		}

		/// <summary>
		/// The fifth exploration complete behavior to use if possible.
		/// </summary>
		[DefaultValue(ExplorationCompleteBehavior.AutoSelect)]
		public ExplorationCompleteBehavior ExplorationCompleteBehavior5
		{
			get
			{
				return _explorationCompleteBehavior5;
			}
			set
			{
				if (value.Equals(_explorationCompleteBehavior5))
				{
					return;
				}
				_explorationCompleteBehavior5 = value;
				NotifyPropertyChanged(() => ExplorationCompleteBehavior5);
				Save();
			}
		}

		/// <summary>
		/// An enum for selecting what to do when exploration is completed.
		/// </summary>
		public enum ExplorationCompleteBehavior
		{
			/// <summary>Do nothing.</summary>
			None,

			/// <summary>Auto selects the next best behavior to do based on distance and availability.</summary>
			AutoSelect,

			/// <summary>Take a town portal back to town.</summary>
			Portal,

			/// <summary>Travel to the waypoint and go to town.</summary>
			Waypoint,

			/// <summary>Logout.</summary>
			Logout,

			/// <summary>Find an area transition to reset the current instance.</summary>
			AreaTransition,

			/// <summary>Stops the bot.</summary>
			Stop,

			/// <summary>Take the portal in a corrupted area that spawns after opening the Vaal Vessel.</summary>
			CorruptedPortal,
		}

		/// <summary>
		/// Returns a list of the exploration complete behaviors by priority.
		/// </summary>
		[JsonIgnore]
		public List<ExplorationCompleteBehavior> ExplorationCompleteBehaviors
		{
			get
			{
				return new List<ExplorationCompleteBehavior>
				{
					ExplorationCompleteBehavior1,
					ExplorationCompleteBehavior2,
					ExplorationCompleteBehavior3,
					ExplorationCompleteBehavior4,
					ExplorationCompleteBehavior5
				};
			}
		}

		[JsonIgnore]
		private List<ExplorationCompleteBehavior> _allExplorationCompleteBehaviors;

		/// <summary> </summary>
		[JsonIgnore]
		public List<ExplorationCompleteBehavior> AllExplorationCompleteBehaviors
		{
			get
			{
				return _allExplorationCompleteBehaviors ?? (_allExplorationCompleteBehaviors = new List<ExplorationCompleteBehavior>
				{
					ExplorationCompleteBehavior.AutoSelect,
					ExplorationCompleteBehavior.Portal,
					ExplorationCompleteBehavior.Waypoint,
					ExplorationCompleteBehavior.Logout,
					ExplorationCompleteBehavior.AreaTransition,
					ExplorationCompleteBehavior.Stop
				});
			}
		}

		/// <summary>Sets the state flags to trigger a town run.</summary>
		public void TriggerTownRun()
		{
			Log.DebugFormat("[TriggerTownRun] NeedsTownRun = {0}. Now setting to 1.", NeedsTownRun);
			NeedsTownRun = 1;
		}

		/// <summary>A bot setting to auto-enable DnD. This is to prevent invite spamming which can interfere with the bot under specific scenarios.</summary>
		[DefaultValue(false)]
		public bool AutoDoNotDisturb
		{
			get
			{
				return _autoDoNotDisturb;
			}
			set
			{
				if (value.Equals(_autoDoNotDisturb))
				{
					return;
				}
				_autoDoNotDisturb = value;
				NotifyPropertyChanged(() => AutoDoNotDisturb);
				Save();
			}
		}

		/// <summary>Should the bot disable dnd on stop rather than leaving it enabled?</summary>
		[DefaultValue(false)]
		public bool DisableDoNotDisturbOnBotStop
		{
			get
			{
				return _disableDoNotDisturbOnBotStop;
			}
			set
			{
				if (value.Equals(_disableDoNotDisturbOnBotStop))
				{
					return;
				}
				_disableDoNotDisturbOnBotStop = value;
				NotifyPropertyChanged(() => DisableDoNotDisturbOnBotStop);
				Save();
			}
		}
	}
}