﻿using System;
using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Game;
using Loki.Common;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary>A task to identify items in inventory.</summary>
	public class IdTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private bool _skip;
		private readonly CoroutinesV3.NeedsToIdDelegate _needsToId;
		private readonly CoroutinesV3.IdingCompleteDelegate _idingComplete;
		private readonly CoroutinesV3.IdItemsDelegate _shouldIdItem;
		private readonly object _user;
		private int _tries;

		/// <summary>
		/// Ctor that sets the delegate for obtaining current settings.
		/// </summary>
		/// <param name="needsToId">The delegate to determine if the task needs to run.</param>
		/// <param name="idingComplete">The delegate to run after iding has completed.</param>
		/// <param name="shouldIdItem">The delegate for determining if an item needs to be ided.</param>
		/// <param name="user">The user object to pass to IdItemsUsingXCoroutine.</param>
		public IdTask(
			CoroutinesV3.NeedsToIdDelegate needsToId,
			CoroutinesV3.IdingCompleteDelegate idingComplete,
			CoroutinesV3.IdItemsDelegate shouldIdItem,
			object user = null)
		{
			_needsToId = needsToId;
			_idingComplete = idingComplete;
			_shouldIdItem = shouldIdItem;
			_user = user;
		}

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "IdTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "A task to identify items in inventory.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();
				return true;
			}

			if (type != "task_execute")
				return false;

			if (_skip)
			{
				_tries = 0;
				return false;
			}

			// Only id in town.
			if (!LokiPoe.Me.IsInTown && !LokiPoe.Me.IsInHideout)
			{
				Log.InfoFormat(
					"[IdTask] We cannot id items out of town/hideout. Skipping this task until an area change or restart.");
				_skip = true;
				return false;
			}

			// Make sure we don't sit around, hammering iding all day long.
			if (_tries > 3)
			{
				Log.ErrorFormat(
					"[StashTask] The bot has failed to id items 3 times. Now stopping the bot because it cannot continue.");
				BotManager.Stop();
				return true;
			}

			// If we don't need to id, don't keep checking the task.
			if (!_needsToId())
			{
				Log.InfoFormat("[IdTask] We do not need to id. Skipping this task until an area change or restart.");
				_skip = true;
				return false;
			}

			++_tries;

			var iiuierr = await CoroutinesV3.IdItemsUsingInventoryCoroutine(_shouldIdItem, _user);
			if (iiuierr == CoroutinesV3.IdItemsCoroutineError.None)
			{
				_idingComplete();
				Log.InfoFormat("[IdTask] Iding has completed. Skipping this task until an area change or restart.");
				_skip = true;
				return true;
			}

			Log.ErrorFormat("[IdTask] IdItemsUsingInventoryCoroutine returned {0}.", iiuierr);

			var iiuserr = await CoroutinesV3.IdItemsUsingStashCoroutine(_shouldIdItem, _user);
			if (iiuserr == CoroutinesV3.IdItemsCoroutineError.None)
			{
				_idingComplete();
				Log.InfoFormat("[IdTask] Iding has completed. Skipping this task until an area change or restart.");
				_skip = true;
				return true;
			}

			Log.ErrorFormat("[IdTask] IdItemsUsingStashCoroutine returned {0}.", iiuserr);

			if (iiuserr == CoroutinesV3.IdItemsCoroutineError.NoMoreIdScrolls)
			{
				_idingComplete();
				Log.InfoFormat(
					"[IdTask] Iding has completed because there are no more id scrolls. Skipping this task until an area change or restart.");
				_skip = true;
				return true;
			}

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[IdTask] Now resetting task state.");
			_skip = false;
			_tries = 0;
		}
	}
}