﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Common;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace OldGrindBot
{
    /// <summary>
    /// This task executes routine logic for combat.
    /// </summary>
    public class HandleVaalVessel : ITask
    {
        private static readonly ILog Log = Logger.GetLoggerInstanceForType();

        private bool _hasVesselLocation;
        private Vector2i _vesselLocation;

        private readonly Stopwatch _interactStopwatch = new Stopwatch();
        private int _interactTries;

        private bool _skip;

        /// <summary>The name of this task.</summary>
        public string Name
        {
            get
            {
                return "HandleVaalVesselTask";
            }
        }

        /// <summary>A description of what this task does.</summary>
        public string Description
        {
            get
            {
                return "This task handles extra logic so the bot opens Vaal Vessels.";
            }
        }

        /// <summary>The author of this task.</summary>
        public string Author
        {
            get
            {
                return "Bossland GmbH";
            }
        }

        /// <summary>The version of this task.</summary>
        public string Version
        {
            get
            {
                return "0.0.1.1";
            }
        }

        /// <summary>
        /// Coroutine logic to execute.
        /// </summary>
        /// <param name="type">The requested type of logic to execute.</param>
        /// <param name="param">Data sent to the object from the caller.</param>
        /// <returns>true if logic was executed to handle this type and false otherwise.</returns>
        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type == "core_area_changed_event")
            {
                var oldSeed = (uint)param[0];
                var newSeed = (uint)param[1];
                var oldArea = (DatWorldAreaWrapper)param[2];
                var newArea = (DatWorldAreaWrapper)param[3];

                Reset();
                return true;
            }

            if (type != "task_execute")
                return false;

            if (_skip)
                return false;

            if (LokiPoe.Me.IsDead)
                return false;

            if (!LokiPoe.Me.IsInCorruptedArea)
            {
                Log.InfoFormat("[HandleVaalVesselTask] The current area will not contain a Vaal Vessel. Skipping execution until a restart or area change.");
                _skip = true;
                return false;
            }

            if (!_hasVesselLocation)
                return false;

            if (_interactStopwatch.ElapsedMilliseconds > 3000 || _interactTries > 5)
            {
                Log.ErrorFormat("[HandleVaalVesselTask] Timed out when trying to interact with the chest.");
                _skip = true;
                return true;
            }

            if (LokiPoe.MyPosition.Distance(_vesselLocation) > 20)
            {
                if (!PlayerMover.MoveTowards(_vesselLocation))
                {
                    Log.ErrorFormat("[HandleVaalVesselTask] MoveTowards failed for {0}.", _vesselLocation);
					await Coroutines.FinishCurrentAction();
				}
                return true;
            }

            var obj = LokiPoe.ObjectManager.GetObjectByName("Vaal Vessel") as Chest;
            if (obj == null)
            {
                Log.ErrorFormat("[HandleVaalVesselTask] Vaal Vessel == null. This should NOT happen.");
                return true;
            }

            if (obj.IsOpened)
            {
                Log.InfoFormat("[HandleVaalVesselTask] The Vaal Vessel is already opened. Skipping execution until a restart or area change.");
                _skip = true;
                return false;
            }

            /*if (obj.IsLocked)
            {
                Log.InfoFormat("[HandleVaalVesselTask] The Vaal Vessel is still locked. Skipping execution until a restart or area change.");
                _skip = true;
                return false;
            }*/

            await Coroutines.FinishCurrentAction();

            await Coroutines.CloseBlockingWindows();

            _interactStopwatch.Start();

            ++_interactTries;

            Log.DebugFormat("[HandleVaalVesselTask] Now attempting to interact with the chest. This is the #{0} attempt.",
                _interactTries);

            await Coroutines.InteractWith<Chest>(obj);

            _interactStopwatch.Stop();

			await Coroutines.ReactionWait();

			return true;
        }

        /// <summary>
        /// Non-coroutine logic to execute.
        /// </summary>
        /// <param name="name">The name of the logic to invoke.</param>
        /// <param name="param">The data passed to the logic.</param>
        /// <returns>Data from the executed logic.</returns>
        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        /// <summary>The bot Start event.</summary>
        public void Start()
        {
        }

        /// <summary>The bot Tick event.</summary>
        public void Tick()
        {
            if (!_hasVesselLocation)
            {
                var obj = LokiPoe.ObjectManager.GetObjectByName("Vaal Vessel");
                if (obj != null)
                {
                    _vesselLocation = ExilePather.FastWalkablePositionFor(obj);
                    _hasVesselLocation = true;
                    Log.InfoFormat("[HandleVaalVesselTask] The Vaal Vessel has been found around {0}.", _vesselLocation);
                }
            }
        }

        /// <summary>The bot Stop event.</summary>
        public void Stop()
        {
        }

        private void Reset()
        {
            Log.DebugFormat("[HandleVaalVesselTask] Now resetting task state.");
            _hasVesselLocation = false;
            _vesselLocation = Vector2i.Zero;
            _skip = false;
        }
    }
}