﻿using System;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary>
	/// This task handles an aspect of -.
	/// </summary>
	public class HandleLockedDoor : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private bool _skip;

		private bool _hasDoorLocation;
		private Vector2i _doorLocation;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "HandleLockedDoor";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task handles an aspect of -.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();
				return true;
			}

			if (type != "task_execute")
				return false;

			if (_skip)
				return false;

			if (LokiPoe.Me.IsDead)
				return false;

			if (LokiPoe.CurrentWorldArea.Name != "The Imperial Gardens")
			{
				Log.InfoFormat("[HandleLockedDoor] The current area does not contain the quest. Skipping execution until a restart or area change.");
				_skip = true;
				return false;
			}

			if (!_hasDoorLocation)
				return false;

			if (LokiPoe.MyPosition.Distance(_doorLocation) > 30)
			{
				if (!PlayerMover.MoveTowards(_doorLocation))
				{
					Log.ErrorFormat("[HandleLockedDoor] MoveTowards failed for {0}.", _doorLocation);
					await Coroutines.FinishCurrentAction();
				}
				return true;
			}

			var door = LokiPoe.ObjectManager.LockedDoor;
			if (door == null)
			{
				Log.ErrorFormat("[HandleLockedDoor] LockedDoor == null. This should NOT happen.");
				return true;
			}

			var res = await Coroutines.InteractWith(door);
			Log.InfoFormat("[HandleLockedDoor] InteractWith returned {0}.", res);

			Reset();

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
			if (_skip)
				return;

			if (LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(i => i.FullName == "Tower Key") == null)
			{
				_skip = true;
				Log.InfoFormat("[HandleLockedDoor] We don't have the necessary Tower Key to interact with the Locked Door.");
				return;
			}

			if (!_hasDoorLocation)
			{
				var obj = LokiPoe.ObjectManager.LockedDoor;
				if (obj != null)
				{
					if (obj.IsTargetable)
					{
						_doorLocation = ExilePather.FastWalkablePositionFor(obj);
						_hasDoorLocation = true;
						Log.InfoFormat("[HandleLockedDoor] The Locked Door has been found around {0}.", _doorLocation);
					}
					else
					{
						_skip = true;
						Log.InfoFormat("[HandleLockedDoor] The Locked Door has been found around {0}, but we don't need to interact with it.", _doorLocation);
					}
				}
			}
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[HandleLockedDoor] Now resetting task state.");
			_skip = false;
			_hasDoorLocation = false;
			_doorLocation = Vector2i.Zero;
		}
	}
}