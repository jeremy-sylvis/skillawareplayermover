﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Game;
using Loki.Common;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary></summary>
	public class HandleInventoryItemsTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "HandleInventoryItemsTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "A task to manage inventory items that are not stashed.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type != "task_execute")
				return false;

			if (!LokiPoe.Me.IsInTown && !LokiPoe.Me.IsInHideout)
				return false;

			if (!HasInventoryShardsToManage())
				return false;

			// Close windows that prevent us from iding. Other tasks should be executing in a way where this doesn't interefere.
			if (LokiPoe.InGameState.PurchaseUi.IsOpened || LokiPoe.InGameState.SellUi.IsOpened ||
				LokiPoe.InGameState.TradeUi.IsOpened)
			{
				await Coroutines.CloseBlockingWindows();
			}

			// We need the inventory panel open.
			if (!await CoroutinesV3.OpenInventoryPanel())
			{
				return true;
			}

			if (OldGrindBotSettings.Instance.DontStashSilverCoins)
			{
				if (
					await
						PlaceInventoryItemsHelper("Silver Coin",
							OldGrindBotSettings.Instance.SilverCoinsCell))
					return true;
			}

			if (OldGrindBotSettings.Instance.DontStashPerandusCoins)
			{
				if (
					await
						PlaceInventoryItemsHelper("Perandus Coin",
							OldGrindBotSettings.Instance.PerandusCoinsCell))
					return true;
			}

			if (OldGrindBotSettings.Instance.DontStashAlchemyShards)
			{
				if (
					await
						PlaceInventoryItemsHelper("Alchemy Shard",
							OldGrindBotSettings.Instance.AlchemyShardsCell))
					return true;
			}

			if (OldGrindBotSettings.Instance.DontStashAlterationShards)
			{
				if (
					await
						PlaceInventoryItemsHelper("Alteration Shard",
							OldGrindBotSettings.Instance.AlterationShardsCell))
					return true;
			}

			if (OldGrindBotSettings.Instance.DontStashTransmutationShards)
			{
				if (
					await
						PlaceInventoryItemsHelper("Transmutation Shard",
							OldGrindBotSettings.Instance.TransmutationShardsCell))
					return true;
			}

			if (OldGrindBotSettings.Instance.DontStashScrollFragments)
			{
				if (
					await
						PlaceInventoryItemsHelper("Scroll Fragment",
							OldGrindBotSettings.Instance.ScrollFragmentsCell))
					return true;
			}

			if (OldGrindBotSettings.Instance.DontStashIdScrolls)
			{
				if (
					await
						PlaceInventoryItemsHelper("Scroll of Wisdom",
							OldGrindBotSettings.Instance.IdScrollsCell))
					return true;
			}

			if (OldGrindBotSettings.Instance.DontStashTpScrolls)
			{
				if (
					await
						PlaceInventoryItemsHelper("Portal Scroll",
							OldGrindBotSettings.Instance.TpScrollsCell))
					return true;
			}

			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private static bool Helper(string name, WpfVector2i location)
		{
			// Check to see if the right item is already in the location we want.
			var item =
				LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(
					ii =>
						ii.FullName == name && ii.LocationBottomRight.X == location.X &&
						ii.LocationBottomRight.Y == location.Y);
			if (item != null)
			{
				// If so, we don't need to run.
				return false;
			}

			// Now check to see if there's an item with the name we want in the inventory, which will
			// not be in the location we want it to be.
			item = LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main)
				.FirstOrDefault(ii => ii.FullName == name);
			if (item != null)
			{
				// If so, we need to run.
				return true;
			}

			return false;
		}

		private bool HasInventoryShardsToManage()
		{
			if (OldGrindBotSettings.Instance.DontStashPerandusCoins)
			{
				if (Helper("Perandus Coin",
					OldGrindBotSettings.Instance.PerandusCoinsCell))
					return true;
			}

			if (OldGrindBotSettings.Instance.DontStashSilverCoins)
			{
				if (Helper("Silver Coin",
					OldGrindBotSettings.Instance.SilverCoinsCell))
					return true;
			}

			if (OldGrindBotSettings.Instance.DontStashAlchemyShards)
			{
				if (Helper("Alchemy Shard",
					OldGrindBotSettings.Instance.AlchemyShardsCell))
					return true;
			}

			if (OldGrindBotSettings.Instance.DontStashAlterationShards)
			{
				if (Helper("Alteration Shard",
					OldGrindBotSettings.Instance.AlterationShardsCell))
					return true;
			}

			if (OldGrindBotSettings.Instance.DontStashTransmutationShards)
			{
				if (Helper("Transmutation Shard",
					OldGrindBotSettings.Instance.TransmutationShardsCell))
					return true;
			}

			if (OldGrindBotSettings.Instance.DontStashScrollFragments)
			{
				if (Helper("Scroll Fragment",
					OldGrindBotSettings.Instance.ScrollFragmentsCell))
					return true;
			}

			if (OldGrindBotSettings.Instance.DontStashIdScrolls)
			{
				if (Helper("Scroll of Wisdom", OldGrindBotSettings.Instance.IdScrollsCell))
					return true;
			}

			if (OldGrindBotSettings.Instance.DontStashTpScrolls)
			{
				if (Helper("Portal Scroll", OldGrindBotSettings.Instance.TpScrollsCell))
					return true;
			}

			return false;
		}

		private async Task<bool> PlaceInventoryItemsHelper(string name, WpfVector2i location)
		{
			// Check to see if there is a full stack of the item in the location we want it to be.
			var item =
				LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(
					ii =>
						ii.FullName == name && ii.LocationBottomRight.X == location.X &&
						ii.LocationBottomRight.Y == location.Y);
			if (item != null)
			{
				if (item.StackCount == item.MaxStackCount)
				{
					// If so, we don't need to do anything.
					return false;
				}
			}

			item = LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main)
				.FirstOrDefault(ii => ii.FullName == name);
			if (item == null)
				return false;

			if (item.LocationBottomRight.X == location.X && item.LocationBottomRight.Y == location.Y)
				return false;

			var existing = LokiPoe.InstanceInfo.GetPlayerInventoryBySlot(InventorySlot.Main)
				.GetItemAtLocation(location.X - 1, location.Y - 1);
			if (existing != null)
			{
				Log.ErrorFormat("[PlaceInventoryItemsHelper] The item {0} is in the way. Now attempting to move it.",
					existing.Name);

				var pmtcerr1 = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Pickup(existing.LocalId);
				if (pmtcerr1 != PickupResult.None)
				{
					Log.ErrorFormat("[PlaceInventoryItemsHelper] InventoryUi.PickupMainToCursor returned {0}.",
						pmtcerr1);
				}

				await Coroutines.LatencyWait();

				if (!await CoroutinesV3.WaitForCursorToHaveItem())
				{
					Log.ErrorFormat("[PlaceInventoryItemsHelper] WaitForCursorToHaveItem failed.");
				}

				await Coroutines.ReactionWait();

				return true;
			}

			var pmtcerr = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Pickup(item.LocalId);
			if (pmtcerr != PickupResult.None)
			{
				Log.ErrorFormat("[PlaceInventoryItemsHelper] InventoryUi.PickupMainToCursor returned {0}.", pmtcerr);
				return true;
			}

			await Coroutines.LatencyWait();

			if (!await CoroutinesV3.WaitForCursorToHaveItem())
			{
				Log.ErrorFormat("[PlaceInventoryItemsHelper] WaitForCursorToHaveItem failed.");
			}

			await Coroutines.ReactionWait();

			var cursorItem = LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Cursor).FirstOrDefault();
			if (cursorItem == null)
			{
				Log.ErrorFormat("[PlaceInventoryItemsHelper] There is no item on the cursor when there should be.");
				return true;
			}

			// Tell the user what's going on.
			Log.DebugFormat(
				"[PlaceInventoryItemsHelper] Attempting to place the item {0} at ({1}, {2}).",
				cursorItem.FullName, location.X, location.Y);

			var pfcimerr = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.PlaceCursorInto(location.X - 1, location.Y - 1);
			if (pfcimerr != PlaceCursorIntoResult.None)
			{
				Log.ErrorFormat("[PlaceInventoryItemsHelper] PlaceCursorInto returned {0}.", pfcimerr);
				return true;
			}

			await Coroutines.LatencyWait();

			if (!await CoroutinesV3.WaitForCursorToBeEmpty())
			{
				Log.ErrorFormat("[PlaceInventoryItemsHelper] WaitForCursorToBeEmpty failed.");
			}

			await Coroutines.ReactionWait();

			return true;
		}
	}
}