﻿using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace OldGrindBot
{
	/// <summary>
	/// This task helps OldGrindBot stay in the Piety boss fight area, resetting exploration to 
	/// keep the bot moving around to trigger different phases.
	/// </summary>
	public class HandlePietyArea : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();
		private bool _skip;
		private bool _seenPortal;
	    private int _resets;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get { return "HandlePietyArea"; }
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get { return "This task helps OldGrindBot stay in the Piety boss fight area, resetting exploration to keep the bot moving around to trigger different phases."; }
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get { return "Bossland GmbH"; }
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get { return "0.0.1.1"; }
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();
				return true;
			}

			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsDead)
				return false;

			if (_skip)
				return false;

			if (LokiPoe.CurrentWorldArea.Name != "The Lunaris Temple Level 2")
			{
				Log.InfoFormat("[PietyFight] The current area does not contain Piety. Skipping execution until a restart or area change.");
				_skip = true;
				return false;
			}

			// Simply move towards Piety if nothing else is running.
			var piety = LokiPoe.ObjectManager.Piety;
			if (piety != null)
			{
				if (!piety.IsDead)
				{
					if (!PlayerMover.MoveTowards(piety.Position))
					{
						Log.ErrorFormat("[PietyFight] MoveTowards failed for {0}.", piety.Position);
						await Coroutines.FinishCurrentAction();
					}
					return true;
				}
				else
				{
					Log.InfoFormat("[PietyFight] Piety is dead. Now marking this task to skip executing.");
					//BasicGrindBotSettings.Instance.NeedsTownRun = 2;
					_skip = true;
					return true;
				}
			}
			else if (_seenPortal)
			{
				Log.InfoFormat("[PietyFight] Piety is dead. Now marking this task to skip executing.");
				//BasicGrindBotSettings.Instance.NeedsTownRun = 2;
				_skip = true;
				return true;
			}

			// Reset the explorer, so we move around the area looking for things that might be out of view.
			// It's up to the CR to avoid getting stuck killing Miscreations near their spawner.
			var explorer = Explorer.GetCurrent();
			if (explorer != null)
			{
				Log.InfoFormat("[PietyFight] Now resetting the explorer.");
				explorer.Reset();
			    ++_resets;
			}

		    if (_resets > 5)
		    {
                Log.InfoFormat("[PietyFight] Something broke. Skipping this task.");
                _skip = true;
                return true;
		    }

			// Don't execute any tasks afterwards. This forces the OldGrindBot to stay in the boss area.
			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		    if (LokiPoe.CurrentWorldArea.Name != "The Lunaris Temple Level 2")
		    {
		        return;
		    }

		    if (AreaStateCache.Current.HasLocation("Piety"))
		    {
		        var portal = LokiPoe.ObjectManager.PietyArenaPortal2;
		        if (portal != null && portal.IsTargetable)
		        {
		            _seenPortal = true;
		        }
		    }
		    else
		    {
                var piety = LokiPoe.ObjectManager.Piety;
                if (piety != null)
                {
                    if (!AreaStateCache.Current.HasLocation("Piety"))
                    {
                        AreaStateCache.Current.AddLocation(piety.Position, piety.Id, "Piety");
                    }
                }
		    }
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[HandlePietyArea] Now resetting task state.");
			_skip = false;
			_seenPortal = false;
		    _resets = 0;
		}
	}
}
