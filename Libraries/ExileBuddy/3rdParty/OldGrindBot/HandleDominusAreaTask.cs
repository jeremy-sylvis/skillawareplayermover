﻿using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;
using System.Linq;

namespace OldGrindBot
{
	/// <summary>
	/// This task helps OldGrindBot stay in the Dominus boss fight area, resetting exploration to 
	/// keep the bot moving around to trigger different phases.
	/// </summary>
	public class HandleDominusArea : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();
		private bool _skip;
        private int _resets;
	    private bool _seendPhase2;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get { return "HandleDominusArea"; }
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get { return "This task helps OldGrindBot stay in the Dominus boss fight area, resetting exploration to keep the bot moving around to trigger different phases."; }
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get { return "Bossland GmbH"; }
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get { return "0.0.1.1"; }
		}

		private Vector2i _towerFightStarterPos;

		private static NetworkObject DominusFightStarter
		{
			get
			{
				return LokiPoe.ObjectManager.Objects
					.FirstOrDefault(o => o.Type.Contains("Metadata/Monsters/Demonmodular/TowerSpawners/"));
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();
				return true;
			}

			if (type != "task_execute")
				return false;

            if (_skip)
                return false;

			if (LokiPoe.Me.IsDead)
				return false;

			if (LokiPoe.CurrentWorldArea.Name != "The Upper Sceptre of God")
			{
				Log.InfoFormat("[DominusFight] The current area does not contain Dominus. Skipping execution until a restart or area change.");
				_skip = true;
				return false;
			}

            Log.InfoFormat("[HandleDominusArea]");

			// When we first spawn into the area, exploration should take us within range of everything.
			// Start out by triggering the fight itself.
			if (_towerFightStarterPos == Vector2i.Zero)
			{
				var fightStarter = DominusFightStarter;
				if (fightStarter != null)
				{
					_towerFightStarterPos = ExilePather.FastWalkablePositionFor(fightStarter);
					Log.InfoFormat("[DominusFight] _fightStarterPos: {0}.", _towerFightStarterPos);
					if (!await CoroutinesV3.MoveToLocation(_towerFightStarterPos, 10, 30000, () => false))
					{
						Log.ErrorFormat("[DominusFight] MoveToLocation failed.");
					}
					return true;
				}
			}

			// Check to see if the boss fight is over. Logic for actually completing the quest for the first time
			// is not implemented.
			var npc = LokiPoe.ObjectManager.LadyDialla;
			if (npc != null)
			{
                Log.InfoFormat("[DominusFight] Lady Dialla exists.");
				// Wait for her to spawn in and be usable.
				if (npc.IsTargetable)
				{
					Log.InfoFormat("[DominusFight] Lady Dialla was found. Now marking this task to skip executing.");
					//BasicGrindBotSettings.Instance.NeedsTownRun = 2;
					_skip = true;
				}
				return true;
			}

            var at = LokiPoe.ObjectManager.AreaTransition("The Aqueduct");
            if (at != null)
		    {
                if (at.IsTargetable)
                {
                    Log.InfoFormat("[DominusFight] The Aqueduct is usable. Now marking this task to skip executing.");
                    //BasicGrindBotSettings.Instance.NeedsTownRun = 2;
                    _skip = true;
                    return true;
                }
		    }

			// Simply move towards Dominus if nothing else is running.
			var dom1 = LokiPoe.ObjectManager.DominusHighTemplar;
			if (dom1 != null)
			{
                if (!dom1.IsDead)
				{
                    if (LokiPoe.MyPosition.Distance(dom1.Position) > 15)
				    {
				        Log.InfoFormat("[DominusFight] Now moving towards {0} because he is not dead.", dom1.Name);
					    if (!PlayerMover.MoveTowards(dom1.Position))
					    {
							Log.ErrorFormat("[DominusFight] MoveTowards failed for {0}.", dom1.Position);
							await Coroutines.FinishCurrentAction();
						}
				    }
				    return true;
				}
			}

			var dom2 = LokiPoe.ObjectManager.DominusAscendant;
            if (dom2 != null)
			{
                if (!dom2.IsDead)
				{
				    if (LokiPoe.MyPosition.Distance(dom2.Position) > 15)
				    {
				        Log.InfoFormat("[DominusFight] Now moving towards {0} because he is not dead.", dom2.Name);
					    if (!PlayerMover.MoveTowards(dom2.Position))
					    {
							Log.ErrorFormat("[DominusFight] MoveTowards failed for {0}.", dom2.Position);
							await Coroutines.FinishCurrentAction();
						}
				    }
				    return true;
				}
			}

            if (!_seendPhase2 && dom1 != null)
            {
                if (LokiPoe.MyPosition.Distance(dom1.Position) > 15)
                {
                    Log.InfoFormat(
                        "[DominusFight] Now moving towards {0} because Dominus Ascendant is null (and not been seen yet) and Dominus High Templar exists.",
                        dom1.Name);
	                if (!PlayerMover.MoveTowards(dom1.Position))
	                {
						Log.ErrorFormat("[DominusFight] MoveTowards failed for {0}.", dom1.Position);
						await Coroutines.FinishCurrentAction();
					}
                }
                return true;
            }

			// Reset the explorer, so we move around the area looking for things that might be out of view.
			// It's up to the CR to avoid getting stuck killing Miscreations near their spawner.
			var explorer = Explorer.GetCurrent();
			if (explorer != null)
		    {
		        Log.InfoFormat("[DominusFight] Now resetting the explorer.");
				explorer.Reset();
		    }
		    else
		    {
                Log.InfoFormat("[DominusFight] Explorer.GetCurrent() == null.");
		    }

            ++_resets;

            if (_resets > 5)
            {
                Log.InfoFormat("[DominusFight] Something broke. Skipping this task.");
                _skip = true;
                return true;
            }

			// Don't execute any tasks afterwards. This forces the OldGrindBot to stay in the boss area.
			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		    if (!_seendPhase2)
		    {
		        var obj = LokiPoe.ObjectManager.DominusAscendant;
		        if (obj != null)
		        {
		            _seendPhase2 = true;
                    Log.InfoFormat("[DominusFight] _seendPhase2 = true");
		        }
		    }
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[HandleDominusArea] Now resetting task state.");
			_skip = false;
            _resets = 0;
		    _seendPhase2 = false;
		}
	}
}
