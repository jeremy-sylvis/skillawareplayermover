﻿using System;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Game;
using Loki.Common;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary>
	/// This task handles resurrection.
	/// </summary>
	public class ResurrectTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();
		private bool _stopOnNextError;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "ResurrectTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task handles resurrection.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();
				return true;
			}

			if (type == "core_player_died_event")
			{
				var totalDeathsForInstance = (int)param[0];

				Log.DebugFormat("[ResurrectTask] This is the #{0} time we have died in this instance.", totalDeathsForInstance);
				if (totalDeathsForInstance > OldGrindBotSettings.Instance.DeathsBeforeNewInstance)
				{
					Log.DebugFormat(
						"[ResurrectTask] Now adding a new instance override for the current area [{0}] because DeathsBeforeNewInstance = {1}.",
						LokiPoe.CurrentWorldArea.Id, OldGrindBotSettings.Instance.DeathsBeforeNewInstance);

					// If we aren't in a corrupted area, simply mark the area to create a new instance.
					if (LokiPoe.CurrentWorldArea.IsOverworldArea)
					{
						AreaStateCache.SetNewInstanceOverride(LokiPoe.CurrentWorldArea.Id, true);
					}
					else
					{
						// Otherwise, we need to blacklist the corrupted area elsewhere.
						OldGrindBotSettings.Instance.BlacklistCorruptedAreaTransition = true;
					}
				}

				return true;
			}

			if (type != "task_execute")
				return false;

			if (!LokiPoe.Me.IsDead)
				return false;

			if (_stopOnNextError)
			{
				Log.DebugFormat("[ResurrectTask] Now stopping the bot, because something is wrong.");

				BotManager.Stop();

				return true;
			}

			Log.DebugFormat("[ResurrectTask] We are currently dead in {0}. Now attempting to resurrect.",
				LokiPoe.CurrentWorldArea.Id);

			// Clear all key states to make sure no buttons get in the way.
			LokiPoe.ProcessHookManager.Reset();

			// Wait a random time so we don't rez too fast.
			await Coroutine.Sleep(LokiPoe.Random.Next(3000, 5000));

			LokiPoe.InGameState.ResurrectResult err;

			// Check to see if we need to create a new instance for the current area now.
			// If so, don't use the checkpoint anymore.
			bool flag;
			if (!AreaStateCache.HasNewInstanceOverride(LokiPoe.CurrentWorldArea.Id, out flag) || !flag)
			{
				Log.DebugFormat(
					"[ResurrectTask] We will resurrect to the Checkpoint, because we do not need to create a new instance yet.");

				err = LokiPoe.InGameState.ResurrectPanel.ResurrectToCheckPoint();

				Log.DebugFormat("[ResurrectTask] ResurrectPanel.ResurrectToCheckPoint returned {0}.", err);

				if (err == LokiPoe.InGameState.ResurrectResult.None)
				{
					if (!await CoroutinesV3.WaitForResurrect())
					{
						Log.DebugFormat("[ResurrectTask] WaitForResurrect failed. Trying again.");

						await Coroutine.Sleep(LokiPoe.Random.Next(3000, 5000));

						err = LokiPoe.InGameState.ResurrectPanel.ResurrectToCheckPoint();

						Log.DebugFormat("[ResurrectTask] ResurrectPanel.ResurrectToCheckPoint returned {0}.", err);

						if (!await CoroutinesV3.WaitForResurrect())
						{
							Log.DebugFormat("[ResurrectTask] WaitForResurrect failed.");
						}
						else
						{
							Log.DebugFormat("[ResurrectTask] We should now be resurrected.");

							return true;
						}
					}
					else
					{
						Log.DebugFormat("[ResurrectTask] We should now be resurrected.");

						return true;
					}
				}
			}

			Log.DebugFormat(
				"[ResurrectTask] We will now attempt to resurrect to Town.");

			err = LokiPoe.InGameState.ResurrectPanel.ResurrectToTown();

			Log.DebugFormat("[ResurrectTask] ResurrectPanel.ResurrectToTown returned {0}.", err);

			if (err == LokiPoe.InGameState.ResurrectResult.None)
			{
				if (!await CoroutinesV3.WaitForResurrect())
				{
					Log.DebugFormat("[ResurrectTask] WaitForResurrect failed. Trying again.");

					await Coroutine.Sleep(LokiPoe.Random.Next(3000, 5000));

					err = LokiPoe.InGameState.ResurrectPanel.ResurrectToTown();

					Log.DebugFormat("[ResurrectTask] ResurrectPanel.ResurrectToTown returned {0}.", err);

					if (!await CoroutinesV3.WaitForResurrect())
					{
						Log.DebugFormat("[ResurrectTask] WaitForResurrect failed.");
					}
					else
					{
						Log.DebugFormat("[ResurrectTask] We should now be resurrected.");

						return true;
					}
				}
				else
				{
					Log.DebugFormat("[ResurrectTask] We should now be resurrected.");

					return true;
				}
			}

			_stopOnNextError = true;

			Log.DebugFormat("[ResurrectTask] We were unable to resurrect. Now attempting to logout.");

			var res = LokiPoe.EscapeState.LogoutToTitleScreen();
			Log.DebugFormat("[ResurrectTask] LogoutToTitleScreen returned {0}.", res);

			await Coroutine.Sleep(LokiPoe.Random.Next(3000, 5000));

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[ResurrectTask] Now resetting task state.");
			_stopOnNextError = false;
		}
	}
}