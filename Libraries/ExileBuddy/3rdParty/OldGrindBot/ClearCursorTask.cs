﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;

namespace OldGrindBot
{
	/// <summary>
	/// This task places any item left on the cursor into the inventory.
	/// </summary>
	public class ClearCursorTask : ITask, IEnableable
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private bool _enabled = true;

		/// <summary> The plugin is being enabled.</summary>
		public void Enable()
		{
			Log.DebugFormat("[ClearCursorTask] Enable");
			_enabled = true;
		}

		/// <summary> The plugin is being disabled.</summary>
		public void Disable()
		{
			Log.DebugFormat("[ClearCursorTask] Disable");
			_enabled = false;
		}

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "ClearCursorTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task places any item left on the cursor into the inventory.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type != "task_execute")
				return false;

			if (!_enabled)
				return false;

			if (LokiPoe.Me.IsDead)
				return false;

			var mode = LokiPoe.InGameState.CursorItemOverlay.Mode;
			if (mode == LokiPoe.InGameState.CursorItemModes.VirtualMove || mode == LokiPoe.InGameState.CursorItemModes.VirtualUse)
			{
				Log.DebugFormat("[ClearCursorTask] A virtual item is on the cursor. Now pressing Escape to clear it.");

				LokiPoe.Input.SimulateKeyEvent(Keys.Escape, true, false, false);

				await Coroutines.LatencyWait();

				await Coroutines.ReactionWait();

				return true;
			}

			if (mode == LokiPoe.InGameState.CursorItemModes.None)
				return false;

			Log.DebugFormat("[ClearCursorTask]");

			// Close windows that prevent us from iding. Other tasks should be executing in a way where this doesn't interefere.
			if (LokiPoe.InGameState.PurchaseUi.IsOpened || LokiPoe.InGameState.SellUi.IsOpened ||
				LokiPoe.InGameState.TradeUi.IsOpened || !LokiPoe.InGameState.InventoryUi.IsOpened)
			{
				await Coroutines.CloseBlockingWindows();
			}

			// We need the inventory panel open.
			if (!await CoroutinesV3.OpenInventoryPanel())
			{
				return true;
			}

			var cursor = LokiPoe.InGameState.CursorItemOverlay.Item;
			if (cursor == null)
			{
				Log.DebugFormat("[ClearCursorTask] There is no item on the cursor when there should be.");
				return true;
			}

			Log.DebugFormat("[ClearCursorTask] The item {0} needs to be placed into the inventory.", cursor.Name);

			// Handle currency merging.
			if (cursor.Rarity == Rarity.Currency && cursor.MaxStackCount > 1)
			{
				foreach (var item in LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.Items)
				{
					if (item.FullName == cursor.FullName)
					{
						if (item.StackCount != item.MaxStackCount)
						{
							Log.DebugFormat("[ClearCursorTask] Now attempting to merge the cursor with the item {0}.", item.Name);

							var ba = item.BaseAddress;

							var mserr = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.MergeStack(item.LocalId);
							if (mserr == MergeStackResult.None)
							{
								await Coroutines.LatencyWait();
								var sw = Stopwatch.StartNew();
								while (LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.Items.Any(i => i.BaseAddress == ba))
									// TODO: Rework this
								{
									Log.InfoFormat("[ClearCursorTask] Waiting for the item to be moved.");
									await Coroutine.Sleep(1);
									if (sw.ElapsedMilliseconds > 5000)
									{
										Log.InfoFormat("[ClearCursorTask] Timeout while waiting for the item to be moved.");
										break;
									}
								}

								return true;
							}

							Log.ErrorFormat("[ClearCursorTask] InventoryUi.MergeStack returned {0},", mserr);
						}
					}
				}
			}

			// Handle normal placement.
			int col, row;
			if (!LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.CanFitItem(cursor.Size, out col, out row))
			{
				Log.ErrorFormat("[ClearCursorTask] Now stopping the bot because it cannot continue.");
				BotManager.Stop();
				return true;
			}

			var pfcimerr = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.PlaceCursorInto(col, row);
			if (pfcimerr != PlaceCursorIntoResult.None)
			{
				Log.ErrorFormat("[ClearCursorTask] PlaceCursorInto returned {0}.", pfcimerr);
				if (pfcimerr == PlaceCursorIntoResult.ItemWontFit)
				{
					Log.ErrorFormat("[ClearCursorTask] Now stopping the bot because it cannot continue.");
					BotManager.Stop();
					return true;
				}
				return true;
			}

			await Coroutines.LatencyWait();

			if (!await CoroutinesV3.WaitForCursorToBeEmpty())
			{
				Log.ErrorFormat("[ClearCursorTask] WaitForCursorToBeEmpty failed.");
			}

			await Coroutines.ReactionWait();

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			if (name == "Enable")
			{
				_enabled = true;
				Log.InfoFormat("[ClearCursorTask] Enabled.");
			}
			else if (name == "Disable")
			{
				_enabled = false;
				Log.InfoFormat("[ClearCursorTask] Disabled.");
			}
			else if (name == "IsEnabled")
			{
				return _enabled;
			}
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}
	}
}