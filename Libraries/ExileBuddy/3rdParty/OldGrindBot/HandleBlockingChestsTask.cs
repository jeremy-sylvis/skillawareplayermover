﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;
using Loki.Common;

namespace OldGrindBot
{
	/// <summary>
	/// This task will handle breaking any blocking chests that interfere with movement.
	/// </summary>
	public class HandleBlockingChestsTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private readonly List<int> _processed = new List<int>();

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "HandleBlockingChestsTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task will handle breaking any blocking chests that interfere with movement.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint) param[0];
				var newSeed = (uint) param[1];
				var oldArea = (DatWorldAreaWrapper) param[2];
				var newArea = (DatWorldAreaWrapper) param[3];

				Reset();

				return true;
			}

			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsDead || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout || LokiPoe.Me.IsInMapRoom)
				return false;

			var list = LokiPoe.ObjectManager.Objects.OfType<Chest>().ToList();
			var chests = list.Where(
				c => c.Distance <= 10 && !c.IsOpened && c.IsStompable && !_processed.Contains(c.Id))
				.OrderBy(c => c.Distance)
				.ToList();
			if (chests.Count < 2 && !chests.Any(c => c.Distance < 8))
			{
				return false;
			}

			LokiPoe.ProcessHookManager.Reset();

			await Coroutines.CloseBlockingWindows();

			Log.InfoFormat(
				"[HandleBlockingChests] There are breakable chest clusters close by. We need to break them in order to avoid getting blocked.");

			LokiPoe.Input.SetMousePos(LokiPoe.MyWorldPosition);

			await Coroutines.LatencyWait();

			if (LokiPoe.InGameState.CurrentTarget != null)
			{
				Log.InfoFormat("[HandleBlockingChests] The object {0} is under the cursor. Now interacting with it.",
					LokiPoe.InGameState.CurrentTarget.Id);

				LokiPoe.Input.ClickLMB();

				await Coroutines.FinishCurrentAction(false);
			}

			var positions1 = new List<Vector2i>
			{
				LokiPoe.MyPosition
			};
			var positions2 = new List<Vector2>
			{
				LokiPoe.MyWorldPosition
			};

			foreach (var chest in chests)
			{
				_processed.Add(chest.Id);
				positions1.Add(chest.Position);
				positions2.Add(chest.WorldPosition);
			}

			foreach (var position in positions1)
			{
				LokiPoe.Input.SetMousePos(position);

				await Coroutines.LatencyWait();

				if (LokiPoe.InGameState.CurrentTarget != null)
				{
					Log.InfoFormat(
						"[HandleBlockingChests] The object {0} is under the cursor. Now interacting with it.",
						LokiPoe.InGameState.CurrentTarget.Id);

					LokiPoe.Input.ClickLMB();

					await Coroutines.LatencyWait();

					await Coroutines.FinishCurrentAction(false);
				}
			}

			foreach (var position in positions2)
			{
				LokiPoe.Input.SetMousePos(position);

				await Coroutines.LatencyWait();

				if (LokiPoe.InGameState.CurrentTarget != null)
				{
					Log.InfoFormat(
						"[HandleBlockingChests] The object {0} is under the cursor. Now interacting with it.",
						LokiPoe.InGameState.CurrentTarget.Id);

					LokiPoe.Input.ClickLMB();

					await Coroutines.LatencyWait();

					await Coroutines.FinishCurrentAction(false);
				}
			}

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[HandleBlockingChestsTask] Now resetting task state.");
			_processed.Clear();
		}
	}
}