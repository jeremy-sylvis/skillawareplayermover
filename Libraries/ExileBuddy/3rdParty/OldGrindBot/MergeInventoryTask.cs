﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;

namespace OldGrindBot
{
	/// <summary>
	/// A task to handle merging the inventory into full stacks.
	/// </summary>
	public class MergeInventoryTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "MergeInventoryTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "A task to handle merging the inventory into full stacks.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type != "task_execute")
				return false;

			if (!LokiPoe.Me.IsInTown && !LokiPoe.Me.IsInHideout)
				return false;

			if (!LokiPoe.InstanceInfo.GetPlayerInventoryBySlot(InventorySlot.Main).NeedsToMerge())
				return false;

			// Close windows that prevent us from iding. Other tasks should be executing in a way where this doesn't interefere.
			if (LokiPoe.InGameState.PurchaseUi.IsOpened || LokiPoe.InGameState.SellUi.IsOpened ||
			    LokiPoe.InGameState.TradeUi.IsOpened || !LokiPoe.InGameState.InventoryUi.IsOpened)
			{
				await Coroutines.CloseBlockingWindows();
			}

			// We need the inventory panel open.
			if (!await CoroutinesV3.OpenInventoryPanel())
			{
				return true;
			}

			// Take a list of all non-quest items that are stackable and aren't already in full stacks.
			// We can't shuffle this.
			var items =
				LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).Where(
					i => i.Rarity != Rarity.Quest && !i.IsLabyrinthType && i.MaxStackCount > 1 && i.StackCount != i.MaxStackCount)
					.OrderBy(i => i.FullName)
					.ToList();

			var doContinue = false;

			// Now, loop through all items. If we find two consecutive stacks with the same name, we know we have to collapse.
			for (var x = 0; x < items.Count - 1; ++x)
			{
				var item = items[x];
				var nextItem = items[x + 1];

				if (item.FullName != nextItem.FullName)
				{
					continue;
				}

				Log.DebugFormat("[MergeInventoryTask] Now picking up {0} to the cursor.", item.FullName);

				var pmtcerr = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Pickup(item.LocalId);
				if (pmtcerr != PickupResult.None)
				{
					Log.ErrorFormat("[MergeInventoryTask] InventoryUi.PickupMainToCursor returned {0}.", pmtcerr);
				}

				await Coroutines.LatencyWait();

				if (!await CoroutinesV3.WaitForCursorToHaveItem())
				{
					Log.ErrorFormat("[MergeInventoryTask] WaitForCursorToHaveItem failed.");
				}

				await Coroutines.ReactionWait();

				doContinue = true;

				break;
			}

			if (doContinue)
				return true;

			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}
	}
}