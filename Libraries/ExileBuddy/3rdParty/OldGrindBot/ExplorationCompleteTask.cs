﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;

namespace OldGrindBot
{
	/// <summary>
	/// A task to execute exploration complete logic.
	/// </summary>
	public class ExplorationCompleteTask : ITask
	{
		private readonly Dictionary<uint, bool> _explorationCompleted = new Dictionary<uint, bool>();

		/// <summary>
		/// Ctor.
		/// </summary>
		/// <param name="checkForEarlyCompletion">Should this task check BasicGrindBotSettings for early completion?</param>
		public ExplorationCompleteTask(bool checkForEarlyCompletion)
		{
			_checkForEarlyCompletion = checkForEarlyCompletion;
		}

		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private const int ViableDistanceToNotUsePortal = 250;

		private bool _disableWaypoint;
		private bool _disablePortal;
		private bool _disableAreaTransition;
		private bool _disableCorruptedPortal;

		private readonly bool _checkForEarlyCompletion;

		private bool _ledgeWaypoint;
		private bool _ledgeSubmerged;
		private bool _ledgeClimb;
		private bool _ledgeOverride;

		private Stopwatch _dsw = Stopwatch.StartNew();

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "ExplorationCompleteTask" + (_checkForEarlyCompletion ? " (Early)" : "");
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "A task to execute exploration complete logic.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				_ledgeWaypoint = false;
				_ledgeSubmerged = false;
				_ledgeClimb = false;
				_ledgeOverride = false;

				_disableWaypoint = false;
				_disablePortal = false;
				_disableAreaTransition = false;
				_disableCorruptedPortal = false;

				return true;
			}

			if (type != "task_execute")
				return false;

			// We don't need to execute in town or when we are dead.
			if (LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout || LokiPoe.Me.IsDead)
				return false;

			// If we're checking for early completion
			if (_checkForEarlyCompletion)
			{
				if (LokiPoe.CurrentWorldArea.Name != "The Ledge")
				{
					// If we're not configured to complete early, return false so we explore.
					// Never return early from non-overworld areas.
					if (!OldGrindBotSettings.Instance.EnableEarlyExplorationComplete || !LokiPoe.CurrentWorldArea.IsOverworldArea)
						return false;

					// Never return early from an area that is not the current grind zone.
					if (LokiPoe.CurrentWorldArea.Id != OldGrindBotSettings.Instance.GrindZoneId)
					{
						return false;
					}

					if (Explorer.GetCurrent().PercentComplete <
					    OldGrindBotSettings.Instance.ExplorationCompletePercent)
					{
						return false;
					}
				}
				else
				{
					// Never return early from an area that is not the current grind zone.
					if (LokiPoe.CurrentWorldArea.Id != OldGrindBotSettings.Instance.GrindZoneId)
					{
						return false;
					}

					if (LokiPoe.ObjectManager.AreaTransition("The Submerged Passage") != null && _ledgeWaypoint)
					{
						_ledgeOverride = true;
					}
					else if (LokiPoe.ObjectManager.AreaTransition("The Climb") != null && _ledgeWaypoint)
					{
						_ledgeOverride = true;
					}
					else
					{
						return false;
					}
				}
			}

			bool val;
			var hash = LokiPoe.LocalData.AreaHash;
			if (!_explorationCompleted.TryGetValue(hash, out val))
			{
				_explorationCompleted.Add(hash, true);

				// Woot, woot!
				await Loki.Bot.Utility.BroadcastEventLogic("core_exploration_complete_event");
			}

			// Always make sure we close blocking windows.
			await Coroutines.CloseBlockingWindows();

			var canUseWaypoint = true;
			var canUsePortal = true;
			var canUseAreaTransition = true;
			var canUseLogout = true;

			var waypointDistance = float.MaxValue;
			var waypointCalculated = false;

			var areaTransitionDistance = float.MaxValue;
			var areaTransitionCalculated = false;

			var behaviors = OldGrindBotSettings.Instance.ExplorationCompleteBehaviors;

			// Handle the ledge special override.
			if (_ledgeOverride)
			{
				// Use a predefined set of behaviors.
				behaviors = new List<OldGrindBotSettings.ExplorationCompleteBehavior>
					{
						OldGrindBotSettings.ExplorationCompleteBehavior.AreaTransition,
						OldGrindBotSettings.ExplorationCompleteBehavior.Stop,
					};
				Log.InfoFormat("[ExplorationCompleteTask] Changing behaviors for Ledge.");
			}

			// In a corrupted area, we want to leave the area to go back to the area we were when we left.
			if (LokiPoe.CurrentWorldArea.IsCorruptedArea)
			{
				// We don't want to go back to this area, so flag the area transition for blacklisting.
				OldGrindBotSettings.Instance.BlacklistCorruptedAreaTransition = true;

				// If the user wants to start a new grind session after completing a corrupted area transition, do so.
				if (OldGrindBotSettings.Instance.NewRunAfterCorruptedAreaExplored)
				{
					// Use a predefined set of behaviors.
					behaviors = new List<OldGrindBotSettings.ExplorationCompleteBehavior>
					{
						OldGrindBotSettings.ExplorationCompleteBehavior.Portal,
						OldGrindBotSettings.ExplorationCompleteBehavior.Logout
					};
					if (!string.IsNullOrEmpty(AreaStateCache.CorruptedAreaParentId))
						AreaStateCache.SetNewInstanceOverride(AreaStateCache.CorruptedAreaParentId, true);
				}
				else
				{
					// Use a predefined set of behaviors.
					behaviors = new List<OldGrindBotSettings.ExplorationCompleteBehavior>
					{
						OldGrindBotSettings.ExplorationCompleteBehavior.CorruptedPortal,
						OldGrindBotSettings.ExplorationCompleteBehavior.AreaTransition,
						OldGrindBotSettings.ExplorationCompleteBehavior.Portal,
						OldGrindBotSettings.ExplorationCompleteBehavior.Logout
					};
				}
			}
			else
			{
				for (var x = 0; x < behaviors.Count; ++x)
				{
					if (behaviors[x] == OldGrindBotSettings.ExplorationCompleteBehavior.Waypoint)
						canUseWaypoint = false;

					else if (behaviors[x] == OldGrindBotSettings.ExplorationCompleteBehavior.Portal)
						canUsePortal = false;

					else if (behaviors[x] == OldGrindBotSettings.ExplorationCompleteBehavior.AreaTransition)
						canUseAreaTransition = false;

					else if (behaviors[x] == OldGrindBotSettings.ExplorationCompleteBehavior.Logout)
						canUseLogout = false;

					else if (behaviors[x] == OldGrindBotSettings.ExplorationCompleteBehavior.AutoSelect)
					{
						var setAreaTransition = false;
						var setWaypoint = false;

						if (!canUseWaypoint && !canUsePortal && !canUseAreaTransition && !canUseLogout)
						{
							behaviors[x] = OldGrindBotSettings.ExplorationCompleteBehavior.None;
							//Log.DebugFormat("[ExplorationCompleteTask] behaviors[{0}] = AutoSelect => {1}.", x, behaviors[x]);
							continue;
						}

						// First, see if we can use a waypoint for this area.
						if (canUseWaypoint)
						{
							// If we don't have a waypoint location, then we can't use a waypoint.
							if (!AreaStateCache.Current.HasWaypointLocation)
							{
								canUseWaypoint = false;
							}
							else
							{
								// Get the path distance to the waypoint.
								var location = AreaStateCache.Current.GetLocations("Waypoint").FirstOrDefault();
								if (location == null)
								{
									canUseWaypoint = false;
								}
								else
								{
									if (!waypointCalculated)
									{
										var p = LokiPoe.MyPosition;
										waypointDistance = ExilePather.PathDistance(p, location.Position);
										waypointCalculated = true;
										/*Log.DebugFormat("[ExplorationCompleteTask] waypointDistance: {0}",
                                            waypointDistance);*/
									}
								}
							}
						}

						// Second, check how far the closest area transition is.
						if (canUseAreaTransition)
						{
							var transitionName = TranstionToTake;

							// We can't take an area transition if we don't know of any we can use to reset the instance.
							if (string.IsNullOrEmpty(transitionName))
							{
								canUseAreaTransition = false;
							}
							else
							{
								var location = AreaStateCache.Current.GetLocations(transitionName).FirstOrDefault();
								if (location == null)
								{
									canUseAreaTransition = false;
								}
								else
								{
									if (!areaTransitionCalculated)
									{
										var p = LokiPoe.MyPosition;
										areaTransitionDistance = ExilePather.PathDistance(p, location.Position);
										areaTransitionCalculated = true;
										//Log.DebugFormat("[ExplorationCompleteTask] areaTransitionDistance: {0}", areaTransitionDistance);
									}
								}
							}
						}

						// Now, figure out the order to do things.
						if (canUseAreaTransition && canUseWaypoint)
						{
							if (areaTransitionDistance < waypointDistance)
							{
								setAreaTransition = true;
							}
							else
							{
								setWaypoint = true;
							}
						}
						else if (canUseAreaTransition)
						{
							setAreaTransition = true;
						}
						else if (canUseWaypoint)
						{
							setWaypoint = true;
						}
						else if (canUsePortal)
						{
							behaviors[x] = OldGrindBotSettings.ExplorationCompleteBehavior.Portal;
						}
						else if (canUseLogout)
						{
							behaviors[x] = OldGrindBotSettings.ExplorationCompleteBehavior.Logout;
						}
						else
						{
							behaviors[x] = OldGrindBotSettings.ExplorationCompleteBehavior.None;
						}

						if (setAreaTransition)
						{
							if (areaTransitionDistance < ViableDistanceToNotUsePortal)
							{
								behaviors[x] = OldGrindBotSettings.ExplorationCompleteBehavior.AreaTransition;
							}
							else
							{
								if (canUsePortal)
								{
									behaviors[x] = OldGrindBotSettings.ExplorationCompleteBehavior.Portal;
								}
								else if (canUseLogout)
								{
									behaviors[x] = OldGrindBotSettings.ExplorationCompleteBehavior.Logout;
								}
								else
								{
									behaviors[x] = OldGrindBotSettings.ExplorationCompleteBehavior.None;
								}
							}
						}

						if (setWaypoint)
						{
							if (waypointDistance < ViableDistanceToNotUsePortal)
							{
								behaviors[x] = OldGrindBotSettings.ExplorationCompleteBehavior.Waypoint;
							}
							else
							{
								if (canUsePortal)
								{
									behaviors[x] = OldGrindBotSettings.ExplorationCompleteBehavior.Portal;
								}
								else if (canUseLogout)
								{
									behaviors[x] = OldGrindBotSettings.ExplorationCompleteBehavior.Logout;
								}
								else
								{
									behaviors[x] = OldGrindBotSettings.ExplorationCompleteBehavior.None;
								}
							}
						}

						//Log.DebugFormat("[ExplorationCompleteTask] behaviors[{0}] = AutoSelect => {1}.", x, behaviors[x]);

						if (behaviors[x] == OldGrindBotSettings.ExplorationCompleteBehavior.Waypoint)
							canUseWaypoint = false;

						else if (behaviors[x] == OldGrindBotSettings.ExplorationCompleteBehavior.Portal)
							canUsePortal = false;

						else if (behaviors[x] == OldGrindBotSettings.ExplorationCompleteBehavior.AreaTransition)
							canUseAreaTransition = false;

						else if (behaviors[x] == OldGrindBotSettings.ExplorationCompleteBehavior.Logout)
							canUseLogout = false;
					}
				}
			}

			foreach (var behavior in behaviors)
			{
				// Skip his behavior slot.
				if (behavior == OldGrindBotSettings.ExplorationCompleteBehavior.None)
					continue;

				// Stop the bot.
				if (behavior == OldGrindBotSettings.ExplorationCompleteBehavior.Stop)
				{
					Log.DebugFormat(
						"[ExplorationCompleteTask] Now stopping the bot because {0} is the first usable exploration complete behavior.",
						behavior);
					BotManager.Stop();
					return true;
				}

				// Logout.
				if (behavior == OldGrindBotSettings.ExplorationCompleteBehavior.Logout)
				{
					Log.DebugFormat(
						"[ExplorationCompleteTask] Now logging out because {0} is the first usable exploration complete behavior.",
						behavior);

					await CoroutinesV3.LogoutToTitleScreen();

					return true;
				}

				// Use an area transition to reset the instance.
				if (behavior == OldGrindBotSettings.ExplorationCompleteBehavior.AreaTransition &&
				    !_disableAreaTransition)
				{
					var transitionName = TranstionToTake;
					if (_ledgeOverride)
					{
						if (LokiPoe.ObjectManager.AreaTransition("The Submerged Passage") != null)
						{
							transitionName = "The Submerged Passage";
						}
						else if (LokiPoe.ObjectManager.AreaTransition("The Climb") != null)
						{
							transitionName = "The Climb";
						}
					}

					if (_dsw.ElapsedMilliseconds > 1000)
					{
						Log.InfoFormat("[ExplorationCompleteTask] TranstionToTake: {0}", transitionName);
						_dsw.Restart();
					}

					// We can't take an area transition if we don't know of any we can use to reset the instance.
					if (string.IsNullOrEmpty(transitionName))
					{
						Log.InfoFormat("[ExplorationCompleteTask] No TranstionToTake was detected. Skipping this method.");
						continue;
					}

					var location = AreaStateCache.Current.GetLocations(transitionName).FirstOrDefault();
					if (location == null)
					{
						// Wait for the location to be added.
						if (_ledgeOverride)
						{
							Log.InfoFormat("[ExplorationCompleteTask] Waiting for area transition to be added to the AreaStateCache in Ledge.");
							return true;
						}

						continue;
					}

					var dist = 20;

					// We need to move really close to trigger the AT.
					if (location.Name.Equals("Lioneye's Watch"))
					{
						dist = 10;
					}

					// Now move into range of the transition.
					var at = LokiPoe.ObjectManager.AreaTransition(location.Name);
					if (at == null || ExilePather.PathDistance(LokiPoe.MyPosition, location.Position) > dist)
					{
						if (!PlayerMover.MoveTowards(location.Position))
						{
							Log.ErrorFormat("[ExplorationCompleteTask] MoveTowards failed for {0}.", location.Position);
							_disableAreaTransition = true;
							await Coroutines.FinishCurrentAction();
						}
						return true;
					}

					var timeout = 3000;
					if (location.Name == "The City of Sarn") // gg, ggg...
					{
						timeout = 30000;
					}

					if (await CoroutinesV3.WaitForAreaTransition(location.Name, timeout))
					{
						var taterr = await CoroutinesV3.TakeAreaTransition(location.Name, false, -1);
						if (taterr == CoroutinesV3.TakeAreaTransitionError.None)
						{
							return true;
						}
						Log.ErrorFormat("[ExplorationCompleteTask] TakeAreaTransition returned {0}.", taterr);
					}

					_disableAreaTransition = true;
				}

				// Use a waypoint to get back to town.
				if (behavior == OldGrindBotSettings.ExplorationCompleteBehavior.Waypoint && !_disableWaypoint)
				{
					// If we don't know the waypoint location, then we can't take a waypoint.
					if (!AreaStateCache.Current.HasWaypointLocation)
						continue;

					// Get the location of the waypoint.
					var location = AreaStateCache.Current.GetLocations("Waypoint").FirstOrDefault();
					if (location == null)
						continue;

					// Now move into range of the transition.
					var wp = LokiPoe.ObjectManager.Waypoint;
					if (wp == null || ExilePather.PathDistance(LokiPoe.MyPosition, location.Position) > 30)
					{
						if (!PlayerMover.MoveTowards(location.Position))
						{
							Log.ErrorFormat("[ExplorationCompleteTask] MoveTowards failed for {0}.", location.Position);
							_disableWaypoint = true;
							await Coroutines.FinishCurrentAction();
						}
						return true;
					}

					var twterr = await CoroutinesV3.TakeWaypointTo(LokiPoe.CurrentWorldArea.Id, true, OldGrindBotSettings.Instance.MaxInstances);
					if (twterr != CoroutinesV3.TakeWaypointToError.None)
					{
						Log.ErrorFormat("[ExplorationCompleteTask] TakeWaypointTo returned {0}.", twterr);
						if (twterr == CoroutinesV3.TakeWaypointToError.TooManyInstances)
						{
							BotManager.Stop();
							return true;
						}
					}
					else
					{
						return true;
					}

					_disableWaypoint = true;
				}

				// Use a portal to get back to town.
				if (behavior == OldGrindBotSettings.ExplorationCompleteBehavior.Portal && !_disablePortal)
				{
					// Sometimes failed can be returned, in which case, we need to return, so we check to see
					// if we're in town afterwards.

					Log.InfoFormat("[ExplorationCompleteTask] Now calling CreatePortalToTown.");
					if (await CoroutinesV3.CreatePortalToTown())
					{
						if (await CoroutinesV3.TakeClosestPortal())
						{
							return true;
						}
					}

					_disablePortal = true;
				}

				if (behavior == OldGrindBotSettings.ExplorationCompleteBehavior.CorruptedPortal && !_disableCorruptedPortal)
				{
					Log.InfoFormat("[ExplorationCompleteTask] Now calling TakeClosestAreaTransition.");

					if (await CoroutinesV3.TakeClosestAreaTransition())
					{
						return true;
					}

					_disableCorruptedPortal = true;
				}
			}

			Log.DebugFormat(
				"[ExplorationCompleteTask] Now logging out because ExplorationCompletePoi could not do anything else.");

			await CoroutinesV3.LogoutToTitleScreen();

			await Coroutine.Sleep(1000);

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
			if (LokiPoe.CurrentWorldArea.Name == "The Ledge")
			{
				if (!_ledgeWaypoint && LokiPoe.ObjectManager.Waypoint != null)
				{
					_ledgeWaypoint = true;
				}
				if (!_ledgeSubmerged && LokiPoe.ObjectManager.AreaTransition("The Submerged Passage") != null)
				{
					_ledgeSubmerged = true;
				}
				if (!_ledgeClimb && LokiPoe.ObjectManager.AreaTransition("The Climb") != null)
				{
					_ledgeClimb = true;
				}
			}
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private string TranstionToTake
		{
			get
			{
				if (LokiPoe.CurrentWorldArea.IsCorruptedArea)
				{
					var exit = Dat.LookupWorldArea(AreaStateCache.CorruptedAreaParentId);
					if (exit != null)
					{
						return exit.Name;
					}
				}
				else
				{
					var myPos = LokiPoe.MyPosition;

					var connections =
						LokiPoe.CurrentWorldArea.Connections.Where(
							con =>
								AreaStateCache.Current.HasLocation(con.Name) &&
								ExilePather.PathExistsBetween(myPos,
									AreaStateCache.Current.GetLocations(con.Name).FirstOrDefault().Position)
									&&
									!(con.Name == "Daresso's Dream" && LokiPoe.CurrentWorldArea.Name == "The Grand Arena")
									);

					var exit =
						connections.OrderBy(
							con =>
								myPos.Distance(AreaStateCache.Current.GetLocations(con.Name).FirstOrDefault().Position))
							.FirstOrDefault();
					if (exit != null)
					{
						return exit.Name;
					}
				}
				return "";
			}
		}
	}
}