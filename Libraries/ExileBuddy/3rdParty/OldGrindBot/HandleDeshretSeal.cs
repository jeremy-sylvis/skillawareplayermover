﻿using System;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary>
	/// This task handles an aspect of -.
	/// </summary>
	public class HandleDeshretSeal : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private bool _skip;

		private bool _hasDoorLocation;
		private Vector2i _doorLocation;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "HandleDeshretSeal";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task handles an aspect of opening Deshret's Seal.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();
				return true;
			}

			if (type != "task_execute")
				return false;

			if (_skip)
				return false;

			if (LokiPoe.Me.IsDead)
				return false;

			if (LokiPoe.CurrentWorldArea.Name != "Highgate")
			{
				Log.InfoFormat("[HandleDeshretSeal] The current area does not contain the quest. Skipping execution until a restart or area change.");
				_skip = true;
				return false;
			}

			if (!_hasDoorLocation)
				return false;

			// Close blocking windows
			await Coroutines.CloseBlockingWindows();

			if (LokiPoe.MyPosition.Distance(_doorLocation) > 30)
			{
				if (!PlayerMover.MoveTowards(_doorLocation))
				{
					Log.ErrorFormat("[HandleDeshretSeal] MoveTowards failed for {0}.", _doorLocation);
					await Coroutines.FinishCurrentAction();
				}
				return true;
			}

			var seal = LokiPoe.ObjectManager.DeshretSeal;
			if (seal == null)
			{
				Log.ErrorFormat("[HandleDeshretSeal] DeshretSeal == null. This should NOT happen.");
				return true;
			}

			var res = await Coroutines.InteractWith(seal);
			Log.InfoFormat("[HandleDeshretSeal] InteractWith returned {0}.", res);

			Reset();

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
			if (_skip)
				return;

			if (LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(i => i.FullName == "Deshret's Banner") == null)
			{
				_skip = true;
				Log.InfoFormat("[HandleDeshretSeal] We don't have the necessary Deshret's Seal to interact with the Deshret's Seal.");
				return;
			}

			if (!_hasDoorLocation)
			{
				var obj = LokiPoe.ObjectManager.DeshretSeal;
				if (obj != null)
				{
					if (obj.IsTargetable)
					{
						_doorLocation = ExilePather.FastWalkablePositionFor(obj);
						_hasDoorLocation = true;
						Log.InfoFormat("[HandleDeshretSeal] The Deshret's Seal has been found around {0}.", _doorLocation);
					}
					else
					{
						_skip = true;
						Log.InfoFormat("[HandleDeshretSeal] The Deshret's Seal has been found around {0}, but we don't need to interact with it.", _doorLocation);
					}
				}
			}
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[HandleDeshretSeal] Now resetting task state.");
			_skip = false;
			_hasDoorLocation = false;
			_doorLocation = Vector2i.Zero;
		}
	}
}