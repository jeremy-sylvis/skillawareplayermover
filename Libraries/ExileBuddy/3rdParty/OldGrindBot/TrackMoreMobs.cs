﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace OldGrindBot
{
	/// <summary>
	/// This task helps BasicGrindBot seek out as many mobs as possible.
	/// </summary>
	public class TrackMoreMobs : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private readonly Stopwatch _sw = new Stopwatch();
		private readonly Dictionary<int, Vector2i> _mobs = new Dictionary<int, Vector2i>();
		private readonly List<int> _processed = new List<int>();
		private KeyValuePair<int, Vector2i>? _currentEntry;
		private Stopwatch _entryStopwatch;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get { return "TrackMoreMobs"; }
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get { return "This task helps BasicGrindBot seek out as many mobs as possible."; }
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get { return "Bossland GmbH"; }
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get { return "0.0.1.1"; }
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();
				return true;
			}

			if (type == "oldgrindbot_local_area_changed_event")
			{
				Reset();
				return true;
			}

			if (type == "core_player_died_event")
			{
				var totalDeathsForInstance = (int)param[0];

				Reset();
				return true;
			}

			if (type != "task_execute")
				return false;

			// No mobs to process.
			if (!_mobs.Any())
				return false;

			var myPos = LokiPoe.MyPosition;

			// Find a new location to move to.
			if (_currentEntry == null)
			{
				// Get the closest mob to check the position of.
				_currentEntry = _mobs.OrderBy(mkvp => myPos.DistanceSqr(mkvp.Value)).First();
				_entryStopwatch = Stopwatch.StartNew();
			}

			var kvp = _currentEntry.Value;

			// We've spent more than 5m on this entry, something is most likely wrong or we are stuck.
			if (_entryStopwatch.ElapsedMilliseconds > 1000 * 60 * 5)
			{
				Log.DebugFormat("[TrackMoreMobs] Now removing {0}.", kvp.Key);
				_mobs.Remove(kvp.Key);
				_processed.Remove(kvp.Key);
				_currentEntry = null;
				Blacklist.Add(kvp.Key, TimeSpan.FromHours(1), "[TrackMoreMobs] This entry has been active for over 5 minutes.");
				return true;
			}

			// If we aren't in range of it, move towards it so we can trigger it.
			if (myPos.Distance(kvp.Value) > 50)
			{
				if (!PlayerMover.MoveTowards(kvp.Value))
				{
					Log.ErrorFormat("[TrackMoreMobs] MoveTowards failed for {0}.", kvp.Value);
					_mobs.Remove(kvp.Key);
					_currentEntry = null;
					await Coroutines.FinishCurrentAction();
				}
				return true;
			}

			// We are now close enough to the position, check to see if the object exists or is dead.
			// We now check IsActive to avoid the case of hidden quest mobs.
			var m = LokiPoe.ObjectManager.GetObjectById<Monster>(kvp.Key);
			if (m == null || Blacklist.Contains(m.Id) || m.IsDead || m.Reaction != Reaction.Enemy || m.Name.Contains("Daemon"))
			{
                Log.DebugFormat("[TrackMoreMobs] Now removing {0}.", kvp.Key);
				_mobs.Remove(kvp.Key);
				_processed.Remove(kvp.Key);
				_currentEntry = null;
			}
			else if (!m.IsActive)
			{
				if (myPos.Distance(kvp.Value) < 15)
				{
					if (m.Rarity != Rarity.Unique)
					{
						Log.DebugFormat("[TrackMoreMobs] Now removing {0}.", kvp.Key);
						_mobs.Remove(kvp.Key);
						_processed.Remove(kvp.Key);
						_currentEntry = null;
						Blacklist.Add(kvp.Key, TimeSpan.FromHours(1), "[TrackMoreMobs] Mob did not activate while we were in range.");
					}
				}
				else if (!PlayerMover.MoveTowards(kvp.Value))
				{
					Log.ErrorFormat("[TrackMoreMobs] MoveTowards failed for {0}.", kvp.Value);
					_mobs.Remove(kvp.Key);
					_currentEntry = null;
					await Coroutines.FinishCurrentAction();
				}

				return true;
			}
			else
			{
				// The mob has moved, so update the position we need to move to instead.
				_mobs[kvp.Key] = m.Position;
				_currentEntry = null; // Find a new entry on the next run, since the mob has moved far away.
			}

			// We now are relying on the CR to kill the boss. If it doesn't, we'll just die or get
			// stuck around where it spawns.

			return true;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			_currentEntry = null;
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
			if (!LokiPoe.IsInGame || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout)
				return;

			if (!_sw.IsRunning)
			{
				_sw.Start();
			}
			else
			{
				if (_sw.ElapsedMilliseconds < 250)
					return;
			}

			var mobs =
				LokiPoe.ObjectManager.Objects.OfType<Monster>()
					.Where(m => !Blacklist.Contains(m.Id) && !m.IsDead && m.Reaction == Reaction.Enemy && !m.Name.Contains("Daemon") && !m.Invincible);
			foreach (var mob in mobs)
			{
				if (!_mobs.ContainsKey(mob.Id) && !_processed.Contains(mob.Id))
				{
				    if (mob.Name.ToLowerInvariant().Contains("golem"))
				    {

				    }
				    else
				    {
				        Log.DebugFormat("[TrackMoreMobs] Now adding {0} ({2}) at {1}.", mob.Name, mob.Position, mob.Id);
				        _mobs.Add(mob.Id, mob.Position);
				        _processed.Add(mob.Id);
				    }
				}
			}

			var toRemove = new List<int>();

			var myPos = LokiPoe.MyPosition;

			// Clear known dead enemies in range to avoid going location by location to eliminate them.
			foreach (var kvp in _mobs)
			{
				// If the position is more than 100 units away, don't bother checking it, since we wouldn't remove it anyways.
				if (kvp.Value.Distance(myPos) > 100)
					continue;

				var mob = LokiPoe.ObjectManager.GetObjectById<Monster>(kvp.Key);
				if (mob == null)
					continue;

				// Only care about dead mobs.
				if (!mob.IsDead)
					continue;

				// We want to limit how far we remove dead mobs, to ensure more coverage.
				if (mob.Distance > 75)
					continue;

				// We should remove this entry.
				toRemove.Add(kvp.Key);
			}

			// Remove the unneeded entries.
			foreach (var id in toRemove)
			{
				_mobs.Remove(id);
				_processed.Remove(id);
                Log.DebugFormat("[TrackMoreMobs] Now removing {0}.", id);

				if (_currentEntry != null)
				{
					if (_currentEntry.Value.Key == id)
					{
						Log.DebugFormat("[TrackMoreMobs] Now clearing current location because it was removed.");
						_currentEntry = null;
					}
				}
			}

			_sw.Restart();
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		private void Reset()
		{
			Log.DebugFormat("[TrackMoreMobs] Now resetting task state.");
			_mobs.Clear();
			_processed.Clear();
			_currentEntry = null;
		}
	}
}
