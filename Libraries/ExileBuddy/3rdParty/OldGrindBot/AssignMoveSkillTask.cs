﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;
using Loki;
using Loki.Bot;
using Loki.Game;
using Loki.Common;

namespace OldGrindBot
{
	/// <summary>
	/// A task to handle assigning the move skill if it's not already on the skill bar.
	/// </summary>
	public class AssignMoveSkillTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private bool _skip;

		public bool Skip
		{
			get
			{
				return _skip;
			}
			set
			{
				if (value.Equals(_skip))
				{
					return;
				}
				_skip = value;
			}
		}

		/// <summary>The name of this task.</summary>
		public string Name => "AssignMoveSkillTask";

		/// <summary>A description of what this task does.</summary>
		public string Description => "A task to handle assigning the Move skill if it's not already on the skill bar.";

		/// <summary>The author of this task.</summary>
		public string Author => "Bossland GmbH";

		/// <summary>The version of this task.</summary>
		public string Version => "0.0.1.1";

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type != "task_execute")
				return false;

			if (Skip)
				return false;

			if (LokiPoe.Me.IsDead)
				return false;

			var move = LokiPoe.InGameState.SkillBarHud.LastBoundMoveSkill;
			if (move != null)
			{
				if (move.BoundKeys.Last() == Keys.LButton)
				{
					Log.ErrorFormat(
						"[AssignMoveSkillTask] The \"Move\" skill cannot be bound to Left Mouse Button. The bot requires this skill in a different slot.");
					//BotManager.Stop();
					//return true;
				}
				else
				{
					Skip = true;
					return false;
				}
			}

			// For skill reset dialogs and such.
			await Coroutines.CloseBlockingWindows();

			move = LokiPoe.InGameState.SkillBarHud.Skills.LastOrDefault(s => s.InternalName == "Move");

			for (var x = 1; x < 8; ++x)
			{
				if (LokiPoe.InstanceInfo.SkillBarIds[x] == 0)
				{
					Log.DebugFormat("[AssignMoveSkillTask] Assigning the \"Move\" skill to slot #{0}.", x + 1);

					var sserr = LokiPoe.InGameState.SkillBarHud.SetSlot(x + 1, move);
					if (sserr != LokiPoe.InGameState.SetSlotResult.None)
					{
						Log.ErrorFormat("[AssignMoveSkillTask] SkillBarHud.SetSlot returned {0}.", sserr);

						continue;
					}

					await Coroutines.LatencyWait();

					await Coroutines.ReactionWait();

					return true;
				}
			}

			Log.ErrorFormat(
				"[AssignMoveSkillTask] No skill slots are available to assign the \"Move\" skill. The bot requires this skill on the skillbar.");
			BotManager.Stop();

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Skip = false;
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}
	}
}