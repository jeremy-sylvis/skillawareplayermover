﻿using System;
using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Game;
using Loki.Common;

namespace OldGrindBot
{
	/// <summary>
	/// This task executes routine logic for combat.
	/// </summary>
	public class CombatTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private readonly int _leashRange;
		private readonly bool _onlyExecuteInGrindZone;

		/// <summary>
		/// A ctor that sets user defined values.
		/// </summary>
		public CombatTask(int leashRange, bool onlyExecuteInGrindZone)
		{
			_leashRange = leashRange;
			_onlyExecuteInGrindZone = onlyExecuteInGrindZone;
		}

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "CombatTask (Leash " + _leashRange + ")";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task executes routine logic for combat.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsDead || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInMapRoom || LokiPoe.Me.IsInHideout)
				return false;

			if (LokiPoe.CurrentWorldArea.IsOverworldArea && _onlyExecuteInGrindZone &&
			    LokiPoe.CurrentWorldArea.Id != OldGrindBotSettings.Instance.GrindZoneId)
			{
				return false;
			}

			var routine = RoutineManager.CurrentRoutine;

			routine.Execute("SetLeash", _leashRange);

			return await routine.Logic("combat");
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}
	}
}