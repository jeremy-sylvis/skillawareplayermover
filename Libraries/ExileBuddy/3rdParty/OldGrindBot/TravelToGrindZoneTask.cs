﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;

namespace OldGrindBot
{
	/// <summary>
	/// This task will travel to the grind zone configured.
	/// </summary>
	public class TravelToGrindZoneTask : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private string _grindZone;
		private AreaStateCache.Location _transitionLocation;
		private IndexedList<DatWorldAreaWrapper> _pathToGrindZone;
		private int _wpTries;
		private int _atTries;
		private int _atMoveTries;
		private bool _useStaticLocation;
		private Vector2i _staticLocation;
		private readonly Stopwatch _displayStopwatch = Stopwatch.StartNew();

		private string GrindZoneId
		{
			get
			{
				if (_grindZone == null)
				{
					_grindZone = OldGrindBotSettings.Instance.GrindZoneId;
					Log.DebugFormat("[TravelToGrindZoneTask] GrindZoneId = {0}.", _grindZone);
				}
				return _grindZone;
			}
		}

		private IndexedList<DatWorldAreaWrapper> PathToGrindZone
		{
			get
			{
				if (_pathToGrindZone == null)
				{
					var gzi = GrindZoneId;
					using (LokiPoe.Memory.ReleaseFrame(LokiPoe.UseHardlock))
					{
						_pathToGrindZone = AreaPather.CalculateShortestPathTo(gzi, true);
					}
					foreach (var node in _pathToGrindZone)
					{
						Log.DebugFormat("[TravelToGrindZoneTask] PathToGrindZone = {0}.", node.Id);
					}
				}
				return _pathToGrindZone;
			}
		}

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "TravelToGrindZoneTask";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task will travel to the grind zone configured.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint) param[0];
				var newSeed = (uint) param[1];
				var oldArea = (DatWorldAreaWrapper) param[2];
				var newArea = (DatWorldAreaWrapper) param[3];

				Reset();
				return true;
			}

			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsDead)
				return false;

			var aw = LokiPoe.InstanceInfo.AvailableWaypoints;

			if (OldGrindBotSettings.Instance.GrindZoneId != _grindZone)
			{
				Reset();
			}

			var path = PathToGrindZone;
			if (path.Current.Id == AreaStateCache.Current.WorldArea.Id)
			{
				path.Next();
			}

			// Close blocking windows, so the world panel gets reset.
			await Coroutines.CloseBlockingWindows();

			var newInstance = false;
			bool flag;

			// Town logic needs to be handled differently.
			if (LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout)
			{
				if (path.Current.HasWaypoint && !path.IsAtEnd && path[path.Index + 1].HasWaypoint &&
					aw.ContainsKey(path[path.Index + 1].Id))
				{
					path.Next();
				}

				// If the next area has a waypoint, we're assumed to have it due to AreaPather giving the right route.
				if (path.Current.HasWaypoint && aw.ContainsKey(path.Current.Id))
				{
					if (AreaStateCache.HasNewInstanceOverride(path.Current.Id, out flag))
					{
						newInstance = flag;
						AreaStateCache.RemoveNewInstanceOverride(path.Current.Id);
						Log.DebugFormat("[TravelToGrindZoneTask] newInstance = {0} ({1}). [HasNewInstanceOverride]", newInstance,
							path.Current.Id);
					}
					else
					{
						newInstance = path.IsAtEnd;
					}

					var twterr = await CoroutinesV3.TakeWaypointTo(path.Current.Id, newInstance, OldGrindBotSettings.Instance.MaxInstances);
					if (twterr == CoroutinesV3.TakeWaypointToError.None)
						return true;

					Log.ErrorFormat("[TravelToGrindZoneTask] TakeWaypointTo returned {0}.", twterr);

					if (twterr == CoroutinesV3.TakeWaypointToError.TooManyInstances)
					{
						BotManager.Stop();
						return true;
					}

					++_wpTries;

					if (_wpTries > 5)
					{
						Log.ErrorFormat(
							"[TravelToGrindZoneTask] The bot has failed 5 times to take a waypoint to another area. Now stopping the bot because it cannot continue.");
						BotManager.Stop();
						return true;
					}

					await Coroutine.Sleep(3000);

					return true;
				}

				// If we don't have an area transition location yet.
				if (_transitionLocation == null)
				{
					// If we have the waypoint for the current town, use it.
					if (AreaStateCache.Current.HasLocation(path.Current.Name))
					{
						_transitionLocation = AreaStateCache.Current.GetLocations(path.Current.Name).FirstOrDefault();
						Log.DebugFormat(
							"[TravelToGrindZoneTask] The area transition's location has been found from AreaStateCache.");
						return true;
					}

					// Wait for AreaStateCache to update.
					if (LokiPoe.ObjectManager.AreaTransition(path.Current.Name) != null)
					{
						Log.DebugFormat(
							"[TravelToGrindZoneTask] An area transition is in range, but we do not have its location yet. Now waiting for the AreaStateCache to update.");
						return true;
					}

					Log.DebugFormat(
						"[TravelToGrindZoneTask] An area transition is not in range. Now moving to a location where it should come into view.");

					// Otherwise, move directly around the area where waypoints should be.
					if (
						!await
							CoroutinesV3.MoveToLocation(Utility.GuessAreaTransitionLocation(path.Current.Name),
								LokiPoe.Random.Next(25, 30), 60000, () => false))
					{
						Log.ErrorFormat(
							"[TravelToGrindZoneTask] MoveToLocation failed. Now stopping the bot because it cannot continue.");
						BotManager.Stop();
						return true;
					}

					return true;
				}

				++_atTries;

				if (_atTries > 5)
				{
					Log.ErrorFormat(
						"[TravelToGrindZoneTask] The bot has failed 5 times to take an area transition to another area. Now stopping the bot because it cannot continue.");
					BotManager.Stop();
					return true;
				}

				Log.DebugFormat("[TravelToGrindZoneTask] The area transition to move to is {0} at {1}.",
					_transitionLocation.Name, _transitionLocation.Position);

				if (!await CoroutinesV3.MoveToLocation(_transitionLocation.Position, 20, 60000, () => false))
				{
					Log.ErrorFormat("[TravelToGrindZoneTask] MoveToLocation failed.");
					return true;
				}

				int timeout = 3000;
				if (_transitionLocation.Name == "The City of Sarn")
				{
					timeout = 30000;
				}

				if (!await CoroutinesV3.WaitForAreaTransition(_transitionLocation.Name, timeout))
				{
					Log.ErrorFormat(
						"[TravelToGrindZoneTask] WaitForAreaTransition failed for {0}. This is the #{1} try.",
						_transitionLocation.Name, _atTries);

					await Coroutine.Sleep(3000);

					return true;
				}

				if (AreaStateCache.HasNewInstanceOverride(path.Current.Id, out flag))
				{
					newInstance = flag;
					AreaStateCache.RemoveNewInstanceOverride(path.Current.Id);
					Log.DebugFormat("[TravelToGrindZoneTask] newInstance = {0} ({1}). [HasNewInstanceOverride]", newInstance,
						path.Current.Id);
				}
				else
				{
					newInstance = path.IsAtEnd;
				}

				var taterr = await CoroutinesV3.TakeAreaTransition(_transitionLocation.Name, newInstance, OldGrindBotSettings.Instance.MaxInstances);
				if (taterr != CoroutinesV3.TakeAreaTransitionError.None)
				{
					Log.ErrorFormat("[TravelToGrindZoneTask] TakeAreaTransition returned {0}. This is the #{1} try.",
						taterr, _atTries);
					if (taterr == CoroutinesV3.TakeAreaTransitionError.TooManyInstances)
					{
						BotManager.Stop();
					}
					await Coroutine.Sleep(3000);
				}

				return true;
			}

			// If we're in the area we want to grind, we don't need to leave.
			if (LokiPoe.CurrentWorldArea.Id == GrindZoneId)
			{
				return false;
			}

			var display = false;
			if (_displayStopwatch.ElapsedMilliseconds > 1000)
			{
				display = true;
				_displayStopwatch.Restart();
			}

			if (_transitionLocation != null)
			{
				if (display)
				{
					Log.DebugFormat("[TravelToGrindZoneTask] The area transition to move to is {0} at {1}.",
						_transitionLocation.Id, _transitionLocation.Position);
				}

				var dist = 35;

				// We need to move really close to trigger the AT.
				if (_transitionLocation.Name.Equals("Lioneye's Watch"))
				{
					dist = 10;
				}

				var pathDistance = ExilePather.PathDistance(LokiPoe.MyPosition, _transitionLocation.Position);
				if (pathDistance > dist)
				{
					if (display)
					{
						Log.DebugFormat(
							"[TravelToGrindZoneTask] Now moving towards the area transition {0} because it is {1} away.",
							_transitionLocation.Name, pathDistance);
					}

					if (!PlayerMover.MoveTowards(_transitionLocation.Position))
					{
						++_atMoveTries;

						Log.ErrorFormat("[TravelToGrindZoneTask] MoveTowards failed for {0}. This is try #{1}.",
							_transitionLocation.Position, _atMoveTries);

						await Coroutines.FinishCurrentAction();

						if (_atMoveTries > 5)
						{
							AreaStateCache.SetNewInstanceOverride(LokiPoe.CurrentWorldArea.Id, true);

							Log.ErrorFormat(
								"[TravelToGrindZoneTask] The bot has failed 5 times to move towards an area transition to the next area. Now returning to town.");

							await CoroutinesV3.CreateAndTakePortalToTown();

							return true;
						}
					}

					return true;
				}

				++_atTries;

				if (_atTries > 5)
				{
					Log.ErrorFormat(
						"[TravelToGrindZoneTask] The bot has failed 5 times to take an area transition to another area. Now returning to town.");

					AreaStateCache.SetNewInstanceOverride(LokiPoe.CurrentWorldArea.Id, true);

					await CoroutinesV3.CreateAndTakePortalToTown();

					return true;
				}

				if (!await CoroutinesV3.WaitForAreaTransition(path.Current.Name))
				{
					Log.ErrorFormat(
						"[TravelToGrindZoneTask] WaitForAreaTransition failed for {0}. This is the #{1} try.",
						path.Current.Name, _atTries);

					await Coroutine.Sleep(3000);

					return true;
				}

				if (AreaStateCache.HasNewInstanceOverride(path.Current.Id, out flag))
				{
					newInstance = flag;
					AreaStateCache.RemoveNewInstanceOverride(path.Current.Id);
					Log.DebugFormat("[TravelToGrindZoneTask] newInstance = {0} ({1}). [HasNewInstanceOverride]", newInstance,
						path.Current.Id);
				}
				else
				{
					newInstance = path.IsAtEnd;
				}

				var taterr = await CoroutinesV3.TakeAreaTransition(path.Current.Name, newInstance, OldGrindBotSettings.Instance.MaxInstances);
				if (taterr != CoroutinesV3.TakeAreaTransitionError.None)
				{
					Log.ErrorFormat("[TravelToGrindZoneTask] TakeAreaTransition returned {0}. This is the #{1} try.",
						taterr, _atTries);

					if (taterr == CoroutinesV3.TakeAreaTransitionError.TooManyInstances)
					{
						BotManager.Stop();
					}

					await Coroutine.Sleep(3000);
				}

				return true;
			}

			if (path.Current.Name == "Lioneye's Watch" || path.Current.Name == "The Forest Encampment" ||
				path.Current.Name == "The Sarn Encampment" || path.Current.Name == "Highgate")
			{
				var wan = AreaStateCache.Current.WorldArea.Name;

				if (path.Current.Name == "Lioneye's Watch")
				{
					if (wan == "The Twilight Strand")
					{
						for (var x = path.Index; x < path.Count; ++x)
						{
							if (path[x].HasWaypoint)
							{
								if (aw.ContainsKey(path[x].Id))
								{
									if (!await CoroutinesV3.TakeWaypointToTown())
									{
										Log.InfoFormat("[TravelToGrindZoneTask] Now calling CreateAndTakePortalToTown.");
										await CoroutinesV3.CreateAndTakePortalToTown();
									}

									return true;
								}
								break;
							}
						}
					}
					else
					{
						if (!await CoroutinesV3.TakeWaypointToTown())
						{
							Log.InfoFormat("[TravelToGrindZoneTask] Now calling CreateAndTakePortalToTown.");
							await CoroutinesV3.CreateAndTakePortalToTown();
						}

						return true;
					}
				}
				else if (path.Current.Name == "The Forest Encampment")
				{
					if (wan == "The Southern Forest")
					{
						for (var x = path.Index; x < path.Count; ++x)
						{
							if (path[x].HasWaypoint)
							{
								if (aw.ContainsKey(path[x].Id))
								{
									if (!await CoroutinesV3.TakeWaypointToTown())
									{
										Log.InfoFormat("[TravelToGrindZoneTask] Now calling CreateAndTakePortalToTown.");
										await CoroutinesV3.CreateAndTakePortalToTown();
									}

									return true;
								}
								break;
							}
						}
					}
					else
					{
						if (!await CoroutinesV3.TakeWaypointToTown())
						{
							Log.InfoFormat("[TravelToGrindZoneTask] Now calling CreateAndTakePortalToTown.");
							await CoroutinesV3.CreateAndTakePortalToTown();
						}

						return true;
					}
				}
				else if (path.Current.Name == "The Sarn Encampment")
				{
					if (wan == "The City of Sarn")
					{
						for (var x = path.Index; x < path.Count; ++x)
						{
							if (path[x].HasWaypoint)
							{
								if (
									aw.ContainsKey(path[x].Id))
								{
									if (!await CoroutinesV3.TakeWaypointToTown())
									{
										Log.InfoFormat("[TravelToGrindZoneTask] Now calling CreateAndTakePortalToTown.");
										await CoroutinesV3.CreateAndTakePortalToTown();
									}

									return true;
								}
								break;
							}
						}
					}
					else
					{
						if (!await CoroutinesV3.TakeWaypointToTown())
						{
							Log.InfoFormat("[TravelToGrindZoneTask] Now calling CreateAndTakePortalToTown.");
							await CoroutinesV3.CreateAndTakePortalToTown();
						}

						return true;
					}
				}
				else if (path.Current.Name == "Highgate")
				{
					if (wan == "The Aqueduct")
					{
						for (var x = path.Index; x < path.Count; ++x)
						{
							if (path[x].HasWaypoint)
							{
								if (aw.ContainsKey(path[x].Id))
								{
									if (!await CoroutinesV3.TakeWaypointToTown())
									{
										Log.InfoFormat("[TravelToGrindZoneTask] Now calling CreateAndTakePortalToTown.");
										await CoroutinesV3.CreateAndTakePortalToTown();
									}

									return true;
								}
								break;
							}
						}
					}
					else
					{
						if (!await CoroutinesV3.TakeWaypointToTown())
						{
							Log.InfoFormat("[TravelToGrindZoneTask] Now calling CreateAndTakePortalToTown.");
							await CoroutinesV3.CreateAndTakePortalToTown();
						}

						return true;
					}
				}
				else
				{
					if (!await CoroutinesV3.TakeWaypointToTown())
					{
						Log.InfoFormat("[TravelToGrindZoneTask] Now calling CreateAndTakePortalToTown.");
						await CoroutinesV3.CreateAndTakePortalToTown();
					}

					return true;
				}
			}

			var location = AreaStateCache.Current.GetLocations(path.Current.Name).FirstOrDefault();
			if (location != null)
			{
				if (ExilePather.PathExistsBetween(LokiPoe.MyPosition, location.Position))
				{
					_transitionLocation = location;
					Log.DebugFormat(
						"[TravelToGrindZoneTask] The area transition's location has been found from AreaStateCache.");
					return true;
				}
				return false;
			}

			if (_useStaticLocation)
			{
				if (_staticLocation == Vector2i.Zero)
				{
					_staticLocation = StaticLocationManager.GetStaticLocation(path.Current.Name);
					if (_staticLocation == Vector2i.Zero)
					{
						_useStaticLocation = false;
					}
				}

				if (_useStaticLocation)
				{
					var dist = LokiPoe.MyPosition.Distance(_staticLocation);
					if (display)
					{
						Log.DebugFormat(
							"[TravelToGrindZoneTask] Now exploring to the static location {0} ({1}) [{2} %].",
							_staticLocation,
							dist,
							Explorer.GetCurrent().PercentComplete);
						_displayStopwatch.Restart();
					}

					if (!PlayerMover.MoveTowards(_staticLocation))
					{
						Log.ErrorFormat(
							"[TravelToGrindZoneTask] MoveTowards failed for {0}. No longer using a static location.",
							_staticLocation);
						_useStaticLocation = false;
						_staticLocation = Vector2i.Zero;
						await Coroutines.FinishCurrentAction();
					}

					// Handle static location overlaps.
					if (dist < 10)
					{
						Log.ErrorFormat(
							"[TravelToGrindZoneTask] No longer using static location {0} because we are at it.",
							_staticLocation);
						_useStaticLocation = false;
						_staticLocation = Vector2i.Zero;
					}

					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[TravelToGrindZoneTask] Now resetting task state.");
			_transitionLocation = null;
			_pathToGrindZone = null;
			_grindZone = null;
			_wpTries = 0;
			_atTries = 0;
			_atMoveTries = 0;
			_useStaticLocation = true;
			_staticLocation = Vector2i.Zero;
		}
	}
}