﻿using System;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;

namespace OldGrindBot
{
	/// <summary>
	/// This task handles A2Q11.
	/// </summary>
	public class HandleA2Q11 : ITask
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private bool _skip;

		private bool _updateQuest;
		private bool _needsQuest;
		private bool _hasSealLocation;
		private Vector2i _sealLocation;

		/// <summary>The name of this task.</summary>
		public string Name
		{
			get
			{
				return "HandleA2Q11";
			}
		}

		/// <summary>A description of what this task does.</summary>
		public string Description
		{
			get
			{
				return "This task handles A2Q11.";
			}
		}

		/// <summary>The author of this task.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary>The version of this task.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "core_area_changed_event")
			{
				var oldSeed = (uint)param[0];
				var newSeed = (uint)param[1];
				var oldArea = (DatWorldAreaWrapper)param[2];
				var newArea = (DatWorldAreaWrapper)param[3];

				Reset();
				return true;
			}

			if (type != "task_execute")
				return false;

			if (LokiPoe.Me.IsDead)
				return false;

			if (_skip)
				return false;

			if (LokiPoe.CurrentWorldArea.Name != "The Western Forest")
			{
				Log.InfoFormat("[HandleA2Q11] The current area does not contain the quest. Skipping execution until a restart or area change.");
				_skip = true;
				return false;
			}

			if (!_hasSealLocation)
				return false;

			// Check to see if we have the inventory item yet.
			var qi = LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Main).FirstOrDefault(i => i.FullName == "Thaumetic Emblem");
			if (qi == null)
			{
				return false;
			}

			if (LokiPoe.MyPosition.Distance(_sealLocation) > 20)
			{
				if (!PlayerMover.MoveTowards(_sealLocation))
				{
					Log.ErrorFormat("[HandleA2Q11] MoveTowards failed for {0}.", _sealLocation);
					await Coroutines.FinishCurrentAction();
				}
				return true;
			}

			var seal = LokiPoe.ObjectManager.ThaumeticSeal;
			if (seal == null)
			{
				Log.ErrorFormat("[HandleA2Q11] ThaumeticSeal == null. This should NOT happen.");
				return true;
			}

			var res = await Coroutines.InteractWith(seal);
			Log.InfoFormat("[HandleA2Q11] InteractWith returned {0}.", res);

			Reset();

			return true;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		/// <summary>The bot Start event.</summary>
		public void Start()
		{
			Reset();
		}

		/// <summary>The bot Tick event.</summary>
		public void Tick()
		{
			if (_skip)
				return;

			if (_updateQuest)
			{
			    if (LokiPoe.CurrentWorldArea.Act != 2)
			    {
                    _skip = true;
                    Log.InfoFormat("[HandleA2Q11] Now skipping the task since we're not in Act 2.");
			        return;
			    }

				var state = LokiPoe.InGameState.GetCurrentQuestState("a2q11", LokiPoe.CurrentWorldArea.Difficulty);
				if (state == null)
				{
					_needsQuest = false;
					Log.InfoFormat("[HandleA2Q11] Quest state for a2q11 is null.");
				}
				else
				{
					Log.InfoFormat("[HandleA2Q11] {0} => {1}: {2}", state.Id, state.QuestProgressText, state.QuestStateText);

					// [67] [0] [] [You have opened the blocked pass between the Western Forest and Prisoner's Gate. Talk to Bestel in Lioneye's Watch for a reward.]
					if (state.Id == 0)
					{
						_needsQuest = false;
						Log.InfoFormat("[HandleA2Q11] _needsQuest = false because the quest has been completed.");
					}
					else
					{
						_needsQuest = true;
						Log.InfoFormat("[HandleA2Q11] _needsQuest = true because the quest has not been completed yet.");
					}
				}

				_updateQuest = false;
			}

			if (!_needsQuest)
			{
				_skip = true;
			}
			else
			{
				if (!_hasSealLocation)
				{
					var obj = LokiPoe.ObjectManager.ThaumeticSeal;
					if (obj != null)
					{
						_sealLocation = ExilePather.FastWalkablePositionFor(obj);
						_hasSealLocation = true;
						Log.InfoFormat("[HandleA2Q11] The Thaumetic Seal has been found around {0}.", _sealLocation);
					}
				}
			}
		}

		/// <summary>The bot Stop event.</summary>
		public void Stop()
		{
		}

		private void Reset()
		{
			Log.DebugFormat("[HandleA2Q11] Now resetting task state.");
			_updateQuest = true;
			_skip = false;
			_needsQuest = false;
			_hasSealLocation = false;
			_sealLocation = Vector2i.Zero;
		}
	}
}