﻿using System.Threading.Tasks;
using System.Windows.Controls;
using log4net;
using Loki.Bot;
using Loki.Common;
using Loki.Game;

namespace nsGrindZoneChanger
{
    internal class GrindZoneChanger : IPlugin
    {
        private static readonly ILog Log = Logger.GetLoggerInstanceForType();
        
        private Gui _instance;

        #region Implementation of IAuthored

        /// <summary> The name of the plugin. </summary>
        public string Name
        {
            get
            {
                return "GrindZoneChanger";
            }
        }

        /// <summary> The description of the plugin. </summary>
        public string Description
        {
            get
            {
                return
                    "This the old plugin example showing how to change the grind zone when exploration has been completed. Users need to implement custom logic and change the code for this to work as they want.";
            }
        }

        /// <summary>The author of the plugin.</summary>
        public string Author
        {
            get
            {
                return "Bossland GmbH";
            }
        }

        /// <summary>The version of the plugin.</summary>
        public string Version
        {
            get
            {
                return "0.0.1.1";
            }
        }

        #endregion

        #region Implementation of IBase

        /// <summary>Initializes this plugin.</summary>
        public void Initialize()
        {
            Log.DebugFormat("[GrindZoneChanger] Initialize");
        }

        /// <summary>Deinitializes this object. This is called when the object is being unloaded from the bot.</summary>
        public void Deinitialize()
        {
            Log.DebugFormat("[GrindZoneChanger] Deinitialize");
        }

        #endregion

        #region Implementation of IRunnable

        /// <summary> The plugin start callback. Do any initialization here. </summary>
        public void Start()
        {
            Log.DebugFormat("[GrindZoneChanger] Start");
        }

        /// <summary> The plugin tick callback. Do any update logic here. </summary>
        public void Tick()
        {
        }

        /// <summary> The plugin stop callback. Do any pre-dispose cleanup here. </summary>
        public void Stop()
        {
            Log.DebugFormat("[GrindZoneChanger] Stop");
        }

        #endregion

        #region Implementation of ILogic

        /// <summary>
        /// Coroutine logic to execute.
        /// </summary>
        /// <param name="type">The requested type of logic to execute.</param>
        /// <param name="param">Data sent to the object from the caller.</param>
        /// <returns>true if logic was executed to handle this type and false otherwise.</returns>
        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type == "core_exploration_complete_event")
            {
                NewGrindZone();
            }

            return false;
        }

        /// <summary>
        /// Non-coroutine logic to execute.
        /// </summary>
        /// <param name="name">The name of the logic to invoke.</param>
        /// <param name="param">The data passed to the logic.</param>
        /// <returns>Data from the executed logic.</returns>
        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        #endregion

        #region Implementation of IConfigurable

        /// <summary>The settings object. This will be registered in the current configuration.</summary>
        public JsonSettings Settings
        {
            get
            {
                return GrindZoneChangerSettings.Instance;
            }
        }

        /// <summary> The plugin's settings control. This will be added to the Exilebuddy Settings tab.</summary>
        public UserControl Control
        {
            get
            {
                return (_instance ?? (_instance = new Gui()));
            }
        }

        #endregion

        #region Implementation of IEnableable

        /// <summary> The plugin is being enabled.</summary>
        public void Enable()
        {
            Log.DebugFormat("[GrindZoneChanger] Enable");
        }

        /// <summary> The plugin is being disabled.</summary>
        public void Disable()
        {
            Log.DebugFormat("[GrindZoneChanger] Disable");
        }

        #endregion

        #region Override of Object

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return Name + ": " + Description;
        }


        #endregion

        private void NewGrindZone()
        {
            string name = "";
            string difficulty = "";

            var roll = LokiPoe.Random.Next(0, 3);
            switch (roll)
            {
                case 0:
                    name = GrindZoneChangerSettings.Instance.GrindZone1Name;
                    difficulty = GrindZoneChangerSettings.Instance.GrindZone1Difficulty;
                    break;
                case 1:
                    name = GrindZoneChangerSettings.Instance.GrindZone2Name;
                    difficulty = GrindZoneChangerSettings.Instance.GrindZone2Difficulty;
                    break;
                case 2:
                    name = GrindZoneChangerSettings.Instance.GrindZone3Name;
                    difficulty = GrindZoneChangerSettings.Instance.GrindZone3Difficulty;
                    break;
            }

            Log.InfoFormat("[GrindZoneChanger] New grind zone being set to {0}:{1}.", name, difficulty);

			BotManager.CurrentBot.Execute("oldgrindbot_change_grind_zone", name, difficulty);
        }
    }
}
