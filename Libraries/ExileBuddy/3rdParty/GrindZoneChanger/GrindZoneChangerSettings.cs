﻿using System.Collections.Generic;
using System.ComponentModel;
using log4net;
using Loki;
using Loki.Common;
using Loki.Game;
using Newtonsoft.Json;

namespace nsGrindZoneChanger
{
	/// <summary>Settings for the Dev tab. </summary>
	public class GrindZoneChangerSettings : JsonSettings
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private static GrindZoneChangerSettings _instance;

		/// <summary>The current instance for this class. </summary>
		public static GrindZoneChangerSettings Instance
		{
			get { return _instance ?? (_instance = new GrindZoneChangerSettings()); }
		}

		/// <summary>The default ctor. Will use the settings path "GrindZoneChanger".</summary>
		public GrindZoneChangerSettings()
			: base(GetSettingsFilePath(Configuration.Instance.Name, string.Format("{0}.json", "GrindZoneChanger")))
		{
		}

		private string _grindZone1Name;
		private string _grindZone1Difficulty;

		private string _grindZone2Name;
		private string _grindZone2Difficulty;

		private string _grindZone3Name;
		private string _grindZone3Difficulty;

		/// <summary>The grind zone area the bot will travel to for grinding.</summary>
		[DefaultValue("The Mud Flats")]
		public string GrindZone1Name
		{
			get { return _grindZone1Name; }
			set
			{
				if (value.Equals(_grindZone1Name))
				{
					return;
				}
				_grindZone1Name = value;
				NotifyPropertyChanged(() => GrindZone1Name);
				Log.InfoFormat("[GrindZoneChanger] GrindZone1Name = {0}", _grindZone1Name);
			}
		}

		/// <summary>The grind zone area the bot will travel to for grinding.</summary>
		[DefaultValue("The Ledge")]
		public string GrindZone2Name
		{
			get { return _grindZone2Name; }
			set
			{
				if (value.Equals(_grindZone2Name))
				{
					return;
				}
				_grindZone2Name = value;
				NotifyPropertyChanged(() => GrindZone2Name);
				Log.InfoFormat("[GrindZoneChanger] GrindZone2Name = {0}", _grindZone2Name);
			}
		}

		/// <summary>The grind zone area the bot will travel to for grinding.</summary>
		[DefaultValue("The Cavern of Wrath")]
		public string GrindZone3Name
		{
			get { return _grindZone3Name; }
			set
			{
				if (value.Equals(_grindZone3Name))
				{
					return;
				}
				_grindZone3Name = value;
				NotifyPropertyChanged(() => GrindZone3Name);
				Log.InfoFormat("[GrindZoneChanger] GrindZone3Name = {0}", _grindZone3Name);
			}
		}

		/// <summary>The grind zone difficulty.</summary>
		[DefaultValue("Normal")]
		public string GrindZone1Difficulty
		{
			get { return _grindZone1Difficulty; }
			set
			{
				if (value.Equals(_grindZone1Difficulty))
				{
					return;
				}
				_grindZone1Difficulty = value;
				NotifyPropertyChanged(() => GrindZone1Difficulty);
				Log.InfoFormat("[GrindZoneChanger] GrindZone1Difficulty = {0}", _grindZone1Difficulty);
			}
		}

		/// <summary>The grind zone difficulty.</summary>
		[DefaultValue("Normal")]
		public string GrindZone2Difficulty
		{
			get { return _grindZone2Difficulty; }
			set
			{
				if (value.Equals(_grindZone2Difficulty))
				{
					return;
				}
				_grindZone2Difficulty = value;
				NotifyPropertyChanged(() => GrindZone2Difficulty);
				Log.InfoFormat("[GrindZoneChanger] GrindZone2Difficulty = {0}", _grindZone2Difficulty);
			}
		}

		/// <summary>The grind zone difficulty.</summary>
		[DefaultValue("Normal")]
		public string GrindZone3Difficulty
		{
			get { return _grindZone3Difficulty; }
			set
			{
				if (value.Equals(_grindZone3Difficulty))
				{
					return;
				}
				_grindZone3Difficulty = value;
				NotifyPropertyChanged(() => GrindZone3Difficulty);
				Log.InfoFormat("[GrindZoneChanger] GrindZone3Difficulty = {0}", _grindZone3Difficulty);
			}
		}

		/// <summary>
		/// Returns the current grind zone id based on the name and difficulty.
		/// </summary>
		[JsonIgnore]
		public string GrindZone1Id
		{
			get { return LokiPoe.GetZoneId(GrindZone1Difficulty, GrindZone1Name); }
		}

		/// <summary>
		/// Returns the current grind zone id based on the name and difficulty.
		/// </summary>
		[JsonIgnore]
		public string GrindZone2Id
		{
			get { return LokiPoe.GetZoneId(GrindZone2Difficulty, GrindZone2Name); }
		}

		/// <summary>
		/// Returns the current grind zone id based on the name and difficulty.
		/// </summary>
		[JsonIgnore]
		public string GrindZone3Id
		{
			get { return LokiPoe.GetZoneId(GrindZone3Difficulty, GrindZone3Name); }
		}

		private List<string> _allGrindZoneDifficulty;

		/// <summary> </summary>
		[JsonIgnore]
		public List<string> AllGrindZoneDifficulty
		{
			get
			{
				return _allGrindZoneDifficulty ?? (_allGrindZoneDifficulty = new List<string>
				{
					"Normal",
					"Cruel",
					"Merciless"
				});
			}
		}

		private List<string> _allGrindZoneNames;

		/// <summary> </summary>
		[JsonIgnore]
		public List<string> AllGrindZoneNames
		{
			get
			{
				return _allGrindZoneNames ?? (_allGrindZoneNames = new List<string>
				{
					"The Twilight Strand",
					"The Coast",
					"The Tidal Island",
					"The Mud Flats",
					"The Fetid Pool",
					"The Flooded Depths",
					"The Submerged Passage",
					"The Ledge",
					"The Climb",
					"The Lower Prison",
					"The Upper Prison",
					"Prisoner's Gate",
					"The Ship Graveyard",
					"The Ship Graveyard Cave",
					"The Cavern of Wrath",
					"The Cavern of Anger",
					"The Southern Forest",
					"The Old Fields",
					"The Den",
					"The Crossroads",
					"The Crypt Level 1",
					"The Crypt Level 2",
					"The Chamber of Sins Level 1",
					"The Chamber of Sins Level 2",
					"The Broken Bridge",
					"The Riverways",
					"The Northern Forest",
					"The Western Forest",
					"The Weaver's Chambers",
					"The Vaal Ruins",
					"The Wetlands",
					"The Dread Thicket",
					"The Caverns",
					"The Ancient Pyramid",
					"The Fellshrine Ruins",
					"The City of Sarn",
					"The Slums",
					"The Crematorium",
					"The Warehouse District",
					"The Marketplace",
					"The Catacombs",
					"The Battlefront",
					"The Solaris Temple Level 1",
					"The Solaris Temple Level 2",
					"The Docks",
					"The Slums Sewers",
					"The Warehouse Sewers",
					"The Market Sewers",
					"The Ebony Barracks",
					"The Lunaris Temple Level 1",
					"The Lunaris Temple Level 2",
					"The Imperial Gardens",
					"The Hedge Maze",
					"The Library",
					"The Archives",
					"The Sceptre of God",
					"The Upper Sceptre of God",
					"The Aqueduct",
					"The Dried Lake",
					"The Mines Level 1",
					"The Mines Level 2",
					"The Crystal Veins",
					"Kaom's Dream",
					"Kaom's Path",
					"Kaom's Stronghold",
					"Daresso's Dream",
					"The Grand Arena",
					"The Belly of the Beast Level 1",
					"The Belly of the Beast Level 2",
					"The Harvest",
				});
			}
		}
	}
}