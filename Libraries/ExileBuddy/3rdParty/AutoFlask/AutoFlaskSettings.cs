﻿using System.ComponentModel;
using Loki;
using Loki.Common;

namespace nsAutoFlask
{
	/// <summary>Settings for the Dev tab. </summary>
	public class AutoFlaskSettings : JsonSettings
	{
		private static AutoFlaskSettings _instance;

		/// <summary>The current instance for this class. </summary>
		public static AutoFlaskSettings Instance
		{
			get
			{
				return _instance ?? (_instance = new AutoFlaskSettings());
			}
		}

		/// <summary>The default ctor. Will use the settings path "AutoFlask".</summary>
		public AutoFlaskSettings()
			: base(GetSettingsFilePath(Configuration.Instance.Name, string.Format("{0}.json", "AutoFlask")))
		{
		}

		private int _hpFlaskPercentTrigger;
		private int _mpFlaskPercentTrigger;

		private int _hpFlaskCooldownMs;
		private int _mpFlaskCooldownMs;

		/// <summary>The hard cooldown the bot will wait before using another flask.</summary>
		[DefaultValue(100)]
		public int HpFlaskCooldownMs
		{
			get { return _hpFlaskCooldownMs; }
			set
			{
				if (value.Equals(_hpFlaskCooldownMs))
				{
					return;
				}
				_hpFlaskCooldownMs = value;
				NotifyPropertyChanged(() => HpFlaskCooldownMs);
			}
		}

		/// <summary>The hard cooldown the bot will wait before using another flask.</summary>
		[DefaultValue(100)]
		public int MpFlaskCooldownMs
		{
			get { return _mpFlaskCooldownMs; }
			set
			{
				if (value.Equals(_mpFlaskCooldownMs))
				{
					return;
				}
				_mpFlaskCooldownMs = value;
				NotifyPropertyChanged(() => MpFlaskCooldownMs);
			}
		}

		/// <summary>The % to trigger hp flasks.</summary>
		[DefaultValue(90)]
		public int HpFlaskPercentTrigger
		{
			get { return _hpFlaskPercentTrigger; }
			set
			{
				if (value.Equals(_hpFlaskPercentTrigger))
				{
					return;
				}
				_hpFlaskPercentTrigger = value;
				NotifyPropertyChanged(() => HpFlaskPercentTrigger);
			}
		}

		/// <summary>The % to trigger mp flasks.</summary>
		[DefaultValue(75)]
		public int MpFlaskPercentTrigger
		{
			get { return _mpFlaskPercentTrigger; }
			set
			{
				if (value.Equals(_mpFlaskPercentTrigger))
				{
					return;
				}
				_mpFlaskPercentTrigger = value;
				NotifyPropertyChanged(() => MpFlaskPercentTrigger);
			}
		}
	}
}
