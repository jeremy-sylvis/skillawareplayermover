﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using log4net;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;
using Loki.Game.Objects.Items;

namespace nsAutoFlask
{
	internal class AutoFlask : IPlugin
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private Gui _instance;

		private readonly Stopwatch _lifeFlaskCd = new Stopwatch();
		private readonly Stopwatch _manaFlaskCd = new Stopwatch();

		#region Implementation of IAuthored

		/// <summary> The name of the plugin. </summary>
		public string Name
		{
			get
			{
				return "AutoFlask";
			}
		}

		/// <summary>The author of the plugin.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary> The description of the plugin. </summary>
		public string Description
		{
			get
			{
				return "A plugin that provides basic auto-flask use.";
			}
		}

		/// <summary>The version of the plugin.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		#endregion

		#region Implementation of IBase

		/// <summary>Initializes this plugin.</summary>
		public void Initialize()
		{
			Log.DebugFormat("[AutoFlask] Initialize");
		}

		/// <summary>Deinitializes this object. This is called when the object is being unloaded from the bot.</summary>
		public void Deinitialize()
		{
			Log.DebugFormat("[AutoFlask] Deinitialize");
		}

		#endregion

		#region Implementation of IRunnable

		/// <summary> The plugin start callback. Do any initialization here. </summary>
		public void Start()
		{
			Log.DebugFormat("[AutoFlask] Start");
		}

		/// <summary> The plugin tick callback. Do any update logic here. </summary>
		public void Tick()
		{
			if (!LokiPoe.IsInGame || LokiPoe.Me.IsInTown || LokiPoe.Me.IsDead)
				return;

			// Life
			if (LokiPoe.Me.HealthPercent < AutoFlaskSettings.Instance.HpFlaskPercentTrigger &&
				!LokiPoe.Me.IsUsingHealthFlask)
			{
				if (FlaskHelper(_lifeFlaskCd, AutoFlaskSettings.Instance.HpFlaskCooldownMs, LifeFlasks))
				{
					return;
				}
			}

			// Mana
			if (LokiPoe.Me.ManaPercent < AutoFlaskSettings.Instance.MpFlaskPercentTrigger &&
				!LokiPoe.Me.IsUsingManaFlask)
			{
				if (FlaskHelper(_manaFlaskCd, AutoFlaskSettings.Instance.MpFlaskCooldownMs, ManaFlasks))
				{
					// ReSharper disable once RedundantJumpStatement
					return;
				}
			}
		}

		/// <summary> The plugin stop callback. Do any pre-dispose cleanup here. </summary>
		public void Stop()
		{
			Log.DebugFormat("[AutoFlask] Stop");
		}

		#endregion

		#region Implementation of IConfigurable

		/// <summary>The settings object. This will be registered in the current configuration.</summary>
		public JsonSettings Settings
		{
			get
			{
				return AutoFlaskSettings.Instance;
			}
		}

		/// <summary> The plugin's settings control. This will be added to the Exilebuddy Settings tab.</summary>
		public UserControl Control
		{
			get
			{
				return (_instance ?? (_instance = new Gui()));
			}
		}

		#endregion

		#region Implementation of ILogic

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		#endregion

		#region Implementation of IEnableable

		/// <summary> The plugin is being enabled.</summary>
		public void Enable()
		{
			Log.DebugFormat("[AutoFlask] Enable");
		}

		/// <summary> The plugin is being disabled.</summary>
		public void Disable()
		{
			Log.DebugFormat("[AutoFlask] Disable");
		}

		#endregion

		#region Override of Object

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			return Name + ": " + Description;
		}

		#endregion

		private bool FlaskHelper(Stopwatch sw, int flaskCdMs, IEnumerable<Item> flasks)
		{
			var useFlask = false;
			if (!sw.IsRunning)
			{
				sw.Start();
				useFlask = true;
			}
			else if (sw.ElapsedMilliseconds > flaskCdMs && sw.ElapsedMilliseconds > LatencyTracker.Average)
			{
				sw.Restart();
				useFlask = true;
			}

			if (useFlask)
			{
				var flask = flasks.FirstOrDefault();
				if (flask != null)
				{
					var err = LokiPoe.InGameState.QuickFlaskHud.UseFlaskInSlot(flask.LocationTopLeft.X + 1);
					if (!err)
					{
						Log.ErrorFormat("[FlaskHelper] QuickFlaskHud.UseFlaskInSlot returned {0}.", err);
					}
					return true;
				}
			}

			return false;
		}

		/// <summary>All life restoring flasks that are non-unique.</summary>
		public static IEnumerable<Item> LifeFlasks
		{
			get
			{
				var items = LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Flasks);
				return from item in items
					   let flask = item as Flask
					   where flask != null && flask.Rarity != Rarity.Unique && flask.HealthRecover > 0 && flask.CanUse
					   orderby flask.IsInstantRecovery ? flask.HealthRecover : flask.HealthRecoveredPerSecond descending
					   select item;
			}
		}

		/// <summary>All mana restoring flasks that are non-unique.</summary>
		public static IEnumerable<Item> ManaFlasks
		{
			get
			{
				var items = LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Flasks);
				return from item in items
					   let flask = item as Flask
					   where flask != null && flask.Rarity != Rarity.Unique && flask.ManaRecover > 0 && flask.CanUse
					   orderby flask.IsInstantRecovery ? flask.ManaRecover : flask.ManaRecoveredPerSecond descending
					   select item;
			}
		}
	}
}