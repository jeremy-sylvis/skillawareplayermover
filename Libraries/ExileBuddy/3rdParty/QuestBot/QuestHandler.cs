﻿using System;
using System.Threading.Tasks;

namespace QuestBot
{
    public class QuestHandler
    {
        public static QuestHandler QuestAddedToCache = new QuestHandler(null, null);
        public static QuestHandler QuestsCompleted = new QuestHandler(null, null);

        public Func<Task<bool>> Execute { get; private set; }
        public Action Tick { get; private set; }

        public QuestHandler(Func<Task<bool>> execute, Action tick)
        {
            Execute = execute;
            Tick = tick;
        }
    }
}