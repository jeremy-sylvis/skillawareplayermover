﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Buddy.Coroutines;
using EXtensions;
using EXtensions.CommonTasks;
using EXtensions.Global;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using settings = QuestBot.Settings;
using UserControl = System.Windows.Controls.UserControl;

namespace QuestBot
{
    public class QuestBot : IBot
    {
        private static readonly ILog Log = Logger.GetLoggerInstanceForType();
        private Gui _gui;

        private Coroutine _coroutine;
        private readonly TaskManager _taskManager = new TaskManager();

        private bool _checkMoveSkill;

        public void Initialize()
        {
            BotManager.OnBotChanged += BotManagerOnOnBotChanged;
        }

        public void Deinitialize()
        {
            BotManager.OnBotChanged -= BotManagerOnOnBotChanged;
        }

        private void BotManagerOnOnBotChanged(object sender, BotChangedEventArgs botChangedEventArgs)
        {
            if (botChangedEventArgs.New == this)
            {
                ItemEvaluator.Instance = DefaultItemEvaluator.Instance;
            }
        }

        public void Start()
        {
            Log.DebugFormat("[QuestBot] Start");

            CheckPlugins();

            // Reset the item evaluator to avoid issues with people using a plugin.
            ItemEvaluator.Instance = DefaultItemEvaluator.Instance;

            // Cache all bound keys.
            LokiPoe.Input.Binding.Update();

            // Reset the default MsBetweenTicks on start.
            Log.DebugFormat($"[Start] MsBetweenTicks: {BotManager.MsBetweenTicks}.");
            Log.DebugFormat($"[Start] NetworkingMode: {LokiPoe.ConfigManager.NetworkingMode}.");
            Log.DebugFormat($"[Start] KeyPickup: {LokiPoe.ConfigManager.KeyPickup}.");
            Log.DebugFormat($"[Start] IsAutoEquipEnabled: {LokiPoe.ConfigManager.IsAutoEquipEnabled}.");
            Log.DebugFormat($"[Start] PlayerMover.Instance: {PlayerMover.Instance.GetType()}.");


            // Since this bot will be performing client actions, we need to enable the process hook manager.
            LokiPoe.ProcessHookManager.Enable();

            _coroutine = null;

            ExilePather.Reload();

            _taskManager.Reset();

            AddTasks();

            PluginManager.Start();
            RoutineManager.Start();

            CombatAreaCache.Start();

            _taskManager.Start();

            foreach (var plugin in PluginManager.EnabledPlugins)
            {
                Log.DebugFormat($"[Start] The plugin {plugin.Name} is enabled.");
            }

            Log.DebugFormat($"[Start] PlayerMover.Instance: {PlayerMover.Instance.GetType()}.");
            Log.DebugFormat($"[Start] ReactionMinSleepDelay: {Coroutines.ReactionMinSleepDelay}.");
            Log.DebugFormat($"[Start] ReactionMaxSleepDelay: {Coroutines.ReactionMaxSleepDelay}.");

            PlayerMover.Execute("SetNetworkingMode", LokiPoe.ConfigManager.NetworkingMode);

            if (ExilePather.BlockTrialOfAscendancy == FeatureEnum.Unset)
            {
                ExilePather.BlockTrialOfAscendancy = FeatureEnum.Enabled;
            }

            _checkMoveSkill = true;
        }

        public void Tick()
        {
            if (_coroutine == null)
            {
                _coroutine = new Coroutine(() => MainCoroutine());
            }

            if (_checkMoveSkill && LokiPoe.IsInGame)
            {
                CheckMoveSkill();
            }

            ExilePather.Reload();

            CombatAreaCache.Tick();
            Travel.Tick();
            _taskManager.Tick();
            PluginManager.Tick();
            RoutineManager.Tick();
            StuckDetection.Tick();

            // Check to see if the coroutine is finished. If it is, stop the bot.
            if (_coroutine.IsFinished)
            {
                Log.DebugFormat($"The bot coroutine has finished in a state of {_coroutine.Status}");
                BotManager.Stop();

                return;
            }

            try
            {
                _coroutine.Resume();
            }
            catch
            {
                var c = _coroutine;
                _coroutine = null;
                c.Dispose();
                throw;
            }
        }

        public void Stop()
        {
            Log.DebugFormat("[QuestBot] Stop");

            CombatAreaCache.Stop();
            _taskManager.Stop();

            PluginManager.Stop();
            RoutineManager.Stop();

            // When the bot is stopped, we want to remove the process hook manager.
            LokiPoe.ProcessHookManager.Disable();

            // Cleanup the coroutine.
            if (_coroutine != null)
            {
                _coroutine.Dispose();
                _coroutine = null;
            }
        }

        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            return await _taskManager.Logic(type, param);
        }

        public object Execute(string name, params dynamic[] param)
        {
            if (name == "GetTaskManager")
            {
                return _taskManager;
            }
            return null;
        }

        private async Task MainCoroutine()
        {
            while (true)
            {
                if (LokiPoe.IsInLoginScreen)
                {
                    // Offload auto login logic to a plugin.
                    foreach (var plugin in PluginManager.EnabledPlugins)
                    {
                        if (await plugin.Logic("login_screen_hook"))
                            break;
                    }
                }
                else if (LokiPoe.IsInCharacterSelectionScreen)
                {
                    // Offload character selection logic to a plugin.
                    foreach (var plugin in PluginManager.EnabledPlugins)
                    {
                        if (await plugin.Logic("character_selection_hook"))
                            break;
                    }
                }
                else if (LokiPoe.IsInGame)
                {
                    foreach (var plugin in PluginManager.EnabledPlugins)
                    {
                        await plugin.Logic("plugin_coroutine_event");
                    }

                    // What the bot does now is up to the registered tasks.
                    await _taskManager.Logic("task_execute");
                }
                else
                {
                    // Most likely in a loading screen, which will cause us to block on the executor, 
                    // but just in case we hit something else that would cause us to execute...
                    await Coroutine.Sleep(1000);
                    continue;
                }

                // End of the tick.
                await Coroutine.Yield();
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private void AddTasks()
        {
            _taskManager.Add(new ResurrectTask());
            _taskManager.Add(new ClearCursorTask());
            _taskManager.Add(new HandleBlockingChestsTask());
            _taskManager.Add(new HandleDoorsTask());
            _taskManager.Add(new CombatTask(50));
            _taskManager.Add(new PostCombatHookTask());
            _taskManager.Add(new ReturnAfterDeathTask());
            _taskManager.Add(new OpenChestTask(true));
            _taskManager.Add(new LootItemTask());
            _taskManager.Add(new OpenChestTask(false));
            _taskManager.Add(new IdTask());
            _taskManager.Add(new SellTask());
            _taskManager.Add(new StashTask());
            _taskManager.Add(new SortInventoryTask());
            _taskManager.Add(new ReturnAfterTownrunTask());
            _taskManager.Add(new OpenWaypointTask());
            _taskManager.Add(new QuestTask());
            _taskManager.Add(new FallbackTask());
        }

        private static void CheckPlugins()
        {
            bool ext = false, events = false, stuck = false;
            foreach (var name in PluginManager.EnabledPlugins.Select(p => p.Name))
            {
                if (name == "EXtensions") ext = true;
                else if (name == "CommonEvents") events = true;
                else if (name == "StuckDetection") stuck = true;
            }

            bool stop = false;

            if (!ext)
            {
                GlobalLog.Error("[QuestBot] Please enable the \"EXtensions\" plugin.");
                stop = true;
            }
            if (!events)
            {
                GlobalLog.Error("[QuestBot] Please enable the \"CommonEvents\" plugin.");
                stop = true;
            }
            if (stuck)
            {
                GlobalLog.Error("[QuestBot] QuestBot has build-in stuck detection, you do not need the StuckDetection plugin, please disable it.");
                stop = true;
            }
            if (stop) BotManager.Stop();
        }

        private void CheckMoveSkill()
        {
            var skill = LokiPoe.InGameState.SkillBarHud.LastBoundMoveSkill;

            if (skill == null || skill.BoundKeys.Last() == Keys.LButton)
            {
                Log.Error("The \"Move\" skill should be present on a skillbar and bound to non left mouse button.");
                BotManager.Stop();
                return;
            }
            _checkMoveSkill = false;
        }

        public string Name => "QuestBot";
        public string Description => "Bot for doing quests.";
        public string Author => "ExVault";
        public string Version => "1.2";
        public JsonSettings Settings => settings.Instance;
        public UserControl Control => _gui ?? (_gui = new Gui());

        public override string ToString()
        {
            return $"{Name}: {Description}";
        }
    }
}