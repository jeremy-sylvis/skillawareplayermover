﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using EXtensions;
using Loki.Game.GameData;

namespace QuestBot
{
    /// <summary>
    /// Interaction logic for Gui.xaml
    /// </summary>
    public partial class Gui : UserControl
    {
        public Gui()
        {
            InitializeComponent();
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            CharClassComboBox.SelectedItem = CharacterClass.None;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ResetIfEmpty(sender as ComboBox);
        }

        private void Combobox_OnLoaded(object sender, RoutedEventArgs e)
        {
            ResetIfEmpty(sender as ComboBox);
        }

        private static void ResetIfEmpty(ComboBox box)
        {
            if (box?.SelectedIndex < 0)
            {
                box.SelectedIndex = 0;
            }
        }

        public class RewardConverter : IMultiValueConverter
        {
            public static readonly RewardConverter Instance = new RewardConverter();

            private static readonly RewardEntry Any = new RewardEntry(Settings.DefaultRewardName, Rarity.Normal);

            private static readonly List<RewardEntry> NoClass = new List<RewardEntry> {new RewardEntry(Settings.UnsetClassRewardName, Rarity.Normal)};

            private static readonly List<RewardEntry> BanditRewards = new List<RewardEntry>
            {
                new RewardEntry(BanditHelper.EramirName, Rarity.Normal),
                new RewardEntry(BanditHelper.AliraName, Rarity.Normal),
                new RewardEntry(BanditHelper.KraitynName, Rarity.Normal),
                new RewardEntry(BanditHelper.OakName, Rarity.Normal)
            };

            public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
            {
                var questId = (string) values[0];
                var diff = (Difficulty) values[1];
                var charClass = (CharacterClass) values[2];

                if (charClass == CharacterClass.None)
                {
                    return NoClass;
                }
                if (questId == Quests.DealWithBandits.Id)
                {
                    return BanditRewards;
                }

                var rewardWrappers = Dat.QuestRewards.Where(r =>
                        r.Difficulty == diff &&
                        r.Quest.Id == questId &&
                        (r.Class == charClass || r.Class == CharacterClass.None))
                    .ToList();

                if (rewardWrappers.Count == 0)
                {
                    return new List<RewardEntry> {new RewardEntry("Error, no values", Rarity.Normal)};
                }

                var rewardEntries = new List<RewardEntry> {Any};
                rewardEntries.AddRange(rewardWrappers.Select(r => new RewardEntry(r.Item.Name, r.Rarity)));
                return rewardEntries;
            }

            public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
            {
                throw new NotImplementedException();
            }
        }

        public class RarityToColorConverter : IValueConverter
        {
            public static readonly RarityToColorConverter Instance = new RarityToColorConverter();

            private static readonly HashSet<string> WhiteColorOverride = new HashSet<string>
            {
                Settings.DefaultRewardName,
                BanditHelper.EramirName,
                BanditHelper.AliraName,
                BanditHelper.KraitynName,
                BanditHelper.OakName
            };

            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                var reward = (RewardEntry) value;
                var name = reward.Name;
                var rarity = reward.Rariry;

                if (WhiteColorOverride.Contains(name))
                {
                    return Brushes.White;
                }
                //for some reason jewel rewards for "Through Sacred Ground" have Quest rarity
                if (rarity == Rarity.Quest && name.ContainsIgnorecase("jewel"))
                {
                    return RarityColors.Unique;
                }
                return RarityColors.FromRarity(rarity);
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                throw new NotImplementedException();
            }
        }

        public class CharClassToBoolConverter : IValueConverter
        {
            public static readonly CharClassToBoolConverter Instance = new CharClassToBoolConverter();

            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                var charClass = (CharacterClass) value;
                return (bool) parameter ? charClass == CharacterClass.None : charClass != CharacterClass.None;
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                throw new NotImplementedException();
            }
        }

        public class RewardEntry
        {
            public string Name { get; set; }
            public Rarity Rariry { get; set; }

            public RewardEntry(string name, Rarity rariry)
            {
                Name = name;
                Rariry = rariry;
            }

            public override string ToString()
            {
                return Name;
            }
        }

        //local copy of Loki.Game.GameData.Difficulty for xaml binding
        public enum Difficulties
        {
            Unknown = -1,
            Normal = 1,
            Cruel = 2,
            Merciless = 3
        }
    }
}