﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using EXtensions;
using Loki;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Newtonsoft.Json;

namespace QuestBot
{
    public class Settings : JsonSettings
    {
        public const string DefaultRewardName = "Any";
        public const string UnsetClassRewardName = "Select a class";

        private static Settings _instance;
        public static Settings Instance => _instance ?? (_instance = new Settings());

        private Settings()
            : base(GetSettingsFilePath(Configuration.Instance.Name, "QuestBot.json"))
        {
            if (RewardQuests.Count == 0)
            {
                InitRewardQuests();
            }
            if (OptionalQuests.Count == 0)
            {
                InitOptionalQuests();
            }
        }

        private string _currentQuestName;
        private string _currentQuestState;

        [JsonIgnore]
        public string CurrentQuestName
        {
            get { return _currentQuestName; }
            set
            {
                if (value == _currentQuestName) return;
                _currentQuestName = value;
                NotifyPropertyChanged(() => CurrentQuestName);
            }
        }

        [JsonIgnore]
        public string CurrentQuestState
        {
            get { return _currentQuestState; }
            set
            {
                if (value == _currentQuestState) return;
                _currentQuestState = value;
                NotifyPropertyChanged(() => CurrentQuestState);
            }
        }

        private CharacterClass _characterClass;

        [DefaultValue(CharacterClass.None)]
        public CharacterClass CharacterClass
        {
            get { return _characterClass; }
            set
            {
                if (value == _characterClass) return;
                _characterClass = value;
                NotifyPropertyChanged(() => CharacterClass);
            }
        }

        [JsonIgnore]
        public static List<CharacterClass> ClassList { get; } = new List<CharacterClass>
        {
            CharacterClass.None,
            CharacterClass.Witch,
            CharacterClass.Shadow,
            CharacterClass.Ranger,
            CharacterClass.Duelist,
            CharacterClass.Marauder,
            CharacterClass.Templar,
            CharacterClass.Scion
        };

        public List<RewardQuest> RewardQuests { get; set; } = new List<RewardQuest>();
        public List<OptionalQuest> OptionalQuests { get; set; } = new List<OptionalQuest>();

        public class RewardQuest
        {
            public string Name { get; set; }
            public string Id { get; set; }
            public Dictionary<Difficulty, string> SelectedRewards { get; set; }

            public RewardQuest(string name, string id)
            {
                Name = name;
                Id = id;
                SelectedRewards = new Dictionary<Difficulty, string>
                {
                    [Difficulty.Normal] = DefaultRewardName,
                    [Difficulty.Cruel] = DefaultRewardName,
                    [Difficulty.Merciless] = DefaultRewardName
                };
            }
        }

        public class OptionalQuest
        {
            public string Name { get; set; }
            public string Id { get; set; }
            public bool Enabled { get; set; }

            public OptionalQuest(string name, string id, bool enabled)
            {
                Name = name;
                Id = id;
                Enabled = enabled;
            }
        }

        public string GetQuestReward(string questId)
        {
            var quest = RewardQuests.FirstOrDefault(q => q.Id == questId);
            if (quest == null)
            {
                GlobalLog.Error($"[Settings][GetQuestReward] Unsupported quest id: \"{questId}\".");
                ErrorManager.ReportCriticalError();
                return null;
            }
            var reward = quest.SelectedRewards[LokiPoe.CurrentWorldArea.Difficulty];
            if (reward == UnsetClassRewardName) reward = DefaultRewardName;
            if (questId == Quests.DealWithBandits.Id && reward == DefaultRewardName) reward = BanditHelper.EramirName;
            return reward;
        }

        public bool IsQuestEnabled(DatQuestWrapper quest)
        {
            var optionalQuest = OptionalQuests.FirstOrDefault(q => q.Id == quest.Id);
            if (optionalQuest == null)
            {
                GlobalLog.Error($"[Settings][IsQuestEnabled] Unsupported quest id: \"{quest.Id}\".");
                ErrorManager.ReportCriticalError();
                return false;
            }
            return optionalQuest.Enabled;
        }

        private void InitRewardQuests()
        {
            RewardQuests.Add(new RewardQuest(Quests.EnemyAtTheGate.Name, Quests.EnemyAtTheGate.Id));
            RewardQuests.Add(new RewardQuest(Quests.MercyMission.Name, Quests.MercyMission.Id));
            RewardQuests.Add(new RewardQuest(Quests.BreakingSomeEggs.Name, Quests.BreakingSomeEggs.Id));
            RewardQuests.Add(new RewardQuest(Quests.CagedBrute.Name + " 1", Quests.CagedBrute.Id + "b"));
            RewardQuests.Add(new RewardQuest(Quests.CagedBrute.Name + " 2", Quests.CagedBrute.Id));
            RewardQuests.Add(new RewardQuest(Quests.SirensCadence.Name, Quests.SirensCadence.Id));
            RewardQuests.Add(new RewardQuest(Quests.SharpAndCruel.Name, Quests.SharpAndCruel.Id));
            RewardQuests.Add(new RewardQuest(Quests.GreatWhiteBeast.Name, Quests.GreatWhiteBeast.Id));
            RewardQuests.Add(new RewardQuest(Quests.IntrudersInBlack.Name, Quests.IntrudersInBlack.Id));
            RewardQuests.Add(new RewardQuest(Quests.ThroughSacredGround.Name, Quests.ThroughSacredGround.Id + "b"));
            RewardQuests.Add(new RewardQuest(Quests.DealWithBandits.Name, Quests.DealWithBandits.Id));
            RewardQuests.Add(new RewardQuest(Quests.LostInLove.Name, Quests.LostInLove.Id));
            RewardQuests.Add(new RewardQuest(Quests.RibbonSpool.Name, Quests.RibbonSpool.Id));
            RewardQuests.Add(new RewardQuest(Quests.SeverRightHand.Name, Quests.SeverRightHand.Id));
            RewardQuests.Add(new RewardQuest(Quests.SwigOfHope.Name, Quests.SwigOfHope.Id));
            RewardQuests.Add(new RewardQuest(Quests.FixtureOfFate.Name, Quests.FixtureOfFate.Id));
            RewardQuests.Add(new RewardQuest(Quests.BreakingSeal.Name, Quests.BreakingSeal.Id));
            RewardQuests.Add(new RewardQuest(Quests.EternalNightmare.Name, Quests.EternalNightmare.Id));
        }

        private void InitOptionalQuests()
        {
            OptionalQuests.Add(new OptionalQuest(Quests.MercyMission.Name, Quests.MercyMission.Id, true));
            OptionalQuests.Add(new OptionalQuest(Quests.DirtyJob.Name, Quests.DirtyJob.Id, true));
            OptionalQuests.Add(new OptionalQuest(Quests.DwellerOfTheDeep.Name, Quests.DwellerOfTheDeep.Id, true));
            OptionalQuests.Add(new OptionalQuest(Quests.MaroonedMariner.Name, Quests.MaroonedMariner.Id, true));
            OptionalQuests.Add(new OptionalQuest(Quests.WayForward.Name, Quests.WayForward.Id, true));
            OptionalQuests.Add(new OptionalQuest(Quests.GreatWhiteBeast.Name, Quests.GreatWhiteBeast.Id, true));
            OptionalQuests.Add(new OptionalQuest(Quests.ThroughSacredGround.Name, Quests.ThroughSacredGround.Id, true));
            OptionalQuests.Add(new OptionalQuest(Quests.VictarioSecrets.Name, Quests.VictarioSecrets.Id, true));
            OptionalQuests.Add(new OptionalQuest(Quests.SwigOfHope.Name, Quests.SwigOfHope.Id, true));
            OptionalQuests.Add(new OptionalQuest(Quests.FixtureOfFate.Name, Quests.FixtureOfFate.Id, true));
            OptionalQuests.Add(new OptionalQuest(Quests.IndomitableSpirit.Name, Quests.IndomitableSpirit.Id, true));
        }
    }
}