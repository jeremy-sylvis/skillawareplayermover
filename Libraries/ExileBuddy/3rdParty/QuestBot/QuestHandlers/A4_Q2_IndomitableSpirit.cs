﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Game;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A4_Q2_IndomitableSpirit
    {
        private const int FinishedStateMinimum = 2;

        private static bool _finished;

        private static NetworkObject DeshretSpirit => LokiPoe.ObjectManager.Objects
            .FirstOrDefault(o => o.Metadata == "Metadata/QuestObjects/Act4/DeshretSpirit");

        private static WalkablePosition CachedDeshretPos
        {
            get { return CombatAreaCache.Current.Storage["DeshretPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["DeshretPosition"] = value; }
        }

        public static void Tick()
        {
            _finished = QuestManager.GetStateInaccurate(Quests.IndomitableSpirit) <= FinishedStateMinimum;

            if (AreaNames.Current == AreaNames.Mines2)
            {
                if (CachedDeshretPos == null)
                {
                    var deshret = DeshretSpirit;
                    if (deshret != null)
                    {
                        CachedDeshretPos = deshret.WalkablePosition();
                    }
                }
            }
        }

        public static async Task<bool> FreeDeshret()
        {
            if (_finished)
            {
                if (AreaNames.Current == AreaNames.CrystalVeins) return false;
                await Travel.To(AreaNames.CrystalVeins);
                return true;
            }

            if (AreaNames.Current == AreaNames.Mines2)
            {
                var deshretPos = CachedDeshretPos;
                if (deshretPos != null)
                {
                    if (deshretPos.IsFar)
                    {
                        deshretPos.Come();
                    }
                    else
                    {
                        var deshret = DeshretSpirit;
                        if (deshret != null && deshret.IsTargetable)
                        {
                            if (!await PlayerAction.Interact(deshret)) ErrorManager.ReportError();
                        }
                    }
                    return true;
                }
                if (!await ExplorationLogic.Execute(100))
                {
                    GlobalLog.Error("[FreeDeshret] Mines area is fully explored, but Deshret Spirit was not found. Now going to re-explore the area.");
                    Explorer.GetCurrent().Reset();
                }
                return true;
            }
            await Travel.To(AreaNames.Mines2);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.Highgate)
            {
                if (Helpers.PlayerHasQuestItem(QuestItemMetadata.BookDeshretSpirit))
                {
                    if (!await Helpers.UseQuestItem(QuestItemMetadata.BookDeshretSpirit))
                    {
                        ErrorManager.ReportError();
                        return true;
                    }
                    return false;
                }

                if (!await TownNpcs.Tasuni.TakeReward(null, "Deshret Reward"))
                {
                    ErrorManager.ReportError();
                }
                return true;
            }
            await Travel.To(AreaNames.Highgate);
            return true;
        }
    }
}