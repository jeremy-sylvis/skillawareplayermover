﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A1_Q5_DwellerOfTheDeep
    {
        private const int FinishedStateMinimum = 3;

        private static bool _finished;

        private static Monster Dweller => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.The_Deep_Dweller)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        private static WalkablePosition CachedDwellerPos
        {
            get { return CombatAreaCache.Current.Storage["DwellerPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["DwellerPosition"] = value; }
        }

        public static void Tick()
        {
            _finished = QuestManager.GetStateInaccurate(Quests.DwellerOfTheDeep) <= FinishedStateMinimum;

            if (AreaNames.Current == AreaNames.FloodedDepths)
            {
                var dweller = Dweller;
                if (dweller != null)
                {
                    CachedDwellerPos = dweller.WalkablePosition();
                }
            }
        }

        public static async Task<bool> KillDweller()
        {
            if (_finished) return false;

            if (AreaNames.Current == AreaNames.FloodedDepths)
            {
                var dwellerPos = CachedDwellerPos;
                if (dwellerPos != null)
                {
                    dwellerPos.Come();
                }
                else
                {
                    if (!await ExplorationLogic.Execute(100))
                    {
                        if (QuestManager.GetState(Quests.DwellerOfTheDeep) <= FinishedStateMinimum) return false;
                        GlobalLog.Error("[KillDweller] Flooded Depths area is fully explored, Deep Dweller was not killed. Now going to re-explore the area.");
                        Explorer.GetCurrent().Reset();
                    }
                }
                return true;
            }
            await Travel.To(AreaNames.FloodedDepths);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.LioneyeWatch)
            {
                if (Helpers.PlayerHasQuestItem(QuestItemMetadata.BookDweller))
                {
                    if (!await Helpers.UseQuestItem(QuestItemMetadata.BookDweller))
                    {
                        ErrorManager.ReportError();
                        return true;
                    }
                    return false;
                }

                if (!await TownNpcs.Tarkleigh.TakeReward(null, "Dweller Reward"))
                {
                    ErrorManager.ReportError();
                }
                return true;
            }
            await Travel.To(AreaNames.LioneyeWatch);
            return true;
        }
    }
}