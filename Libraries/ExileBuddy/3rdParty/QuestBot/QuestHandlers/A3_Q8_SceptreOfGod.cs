﻿using System;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A3_Q8_SceptreOfGod
    {
        private static NetworkObject _dominusRoomObj;
        private static Monster _dominus;
        private static Monster _dominus2;
        private static Monster _anyActiveUniqueMob;
        private static AreaTransition _aqueductTransition;

        private static bool DominusKilled
        {
            get
            {
                var killed = CombatAreaCache.Current.Storage["DominusKilled"];
                return killed != null && (bool) killed;
            }
            set { CombatAreaCache.Current.Storage["DominusKilled"] = value; }
        }

        public static void Tick()
        {
        }

        public static async Task<bool> KillDominus()
        {
            if (AreaNames.Current == AreaNames.Aqueduct)
                return false;

            if (AreaNames.Current == AreaNames.UpperSceptreOfGod)
            {
                UpdateDominusFightObjects();

                if (DominusKilled)
                {
                    await Travel.To(AreaNames.Aqueduct);
                    return true;
                }

                if (_aqueductTransition != null && _aqueductTransition.IsTargetable)
                {
                    if (!await PlayerAction.TakeTransition(_aqueductTransition)) ErrorManager.ReportError();
                    return true;
                }

                if (_dominus2 != null)
                {
                    await Helpers.MoveAndWait(_dominus2.WalkablePosition(), "Waiting for Dominus Ascendant");
                    return true;
                }

                if (_dominus != null)
                {
                    if (!_dominus.IsTargetable && _anyActiveUniqueMob != null)
                    {
                        _anyActiveUniqueMob.WalkablePosition().Come();
                        return true;
                    }
                    if (_dominus.Health == 1)
                    {
                        var id = _dominus.Id;
                        if (!Blacklist.Contains(id))
                        {
                            Blacklist.Add(id, TimeSpan.FromDays(1), "Dominus first form waiting for death");
                        }
                    }
                    await Helpers.MoveAndWait(_dominus.WalkablePosition(), "Waiting for Dominus");
                    return true;
                }

                if (_dominusRoomObj != null)
                {
                    await Helpers.MoveAndWait(_dominusRoomObj.WalkablePosition(), "Waiting for any Dominus figth object");
                    return true;
                }
            }
            await Travel.To(AreaNames.Aqueduct);
            return true;
        }

        public static async Task<bool> EnterHighGate()
        {
            if (AreaNames.Current == AreaNames.Highgate)
            {
                if (World.HasWaypoint(AreaNames.Highgate))
                    return false;

                if (!await PlayerAction.OpenWaypoint()) ErrorManager.ReportError();
                return true;
            }

            await Travel.To(AreaNames.Highgate);
            return true;
        }

        private static void UpdateDominusFightObjects()
        {
            _dominusRoomObj = null;
            _dominus = null;
            _dominus2 = null;
            _anyActiveUniqueMob = null;
            _aqueductTransition = null;

            foreach (var obj in LokiPoe.ObjectManager.Objects)
            {
                var mob = obj as Monster;
                if (mob != null && mob.Rarity == Rarity.Unique)
                {
                    if (mob.Metadata == "Metadata/Monsters/Pope/Pope")
                    {
                        _dominus = mob;
                        continue;
                    }
                    if (mob.Metadata == "Metadata/Monsters/Dominusdemon/Dominusdemon")
                    {
                        _dominus2 = mob;
                        if (!CombatAreaCache.Current.Storage.Contains("DominusKilled") && _dominus2.IsDead)
                        {
                            DominusKilled = true;
                        }
                        continue;
                    }
                    if (mob.IsActive)
                    {
                        _anyActiveUniqueMob = mob;
                    }
                    continue;
                }

                var transition = obj as AreaTransition;
                if (transition != null && transition.Name == AreaNames.Aqueduct)
                {
                    _aqueductTransition = transition;
                    continue;
                }

                if (obj.Metadata == "Metadata/Monsters/Demonmodular/TowerSpawners/TowerSpawnerBoss2")
                {
                    _dominusRoomObj = obj;
                }
            }
        }
    }
}