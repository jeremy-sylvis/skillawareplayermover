﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A2_Q1_SharpAndCruel
    {
        private static readonly TgtPosition WeaverRoomTgt = new TgtPosition("Weaver room", "spidergrove_exit_v01_01_c1r1.tgt");

        private static Monster Weaver => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.The_Weaver)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        public static void Tick()
        {
        }

        public static async Task<bool> KillWeaver()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.MaligaroSpike))
                return false;

            if (AreaNames.Current == AreaNames.WeaverChambers)
            {
                var weaver = Weaver;
                if (weaver != null)
                {
                    var weaverPos = weaver.WalkablePosition();
                    if (weaverPos.PathExists)
                    {
                        await Helpers.MoveAndWait(weaverPos, "Waiting for Weaver");
                        return true;
                    }
                }
                await Helpers.MoveAndTakeLocalTransition(WeaverRoomTgt);
                return true;
            }
            await Travel.To(AreaNames.WeaverChambers);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.ForestEncampment)
            {
                string reward = Settings.Instance.GetQuestReward(Quests.SharpAndCruel.Id);

                if (!await TownNpcs.Silk.TakeReward(reward, "Maligaro's Spike Reward"))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                return false;
            }
            await Travel.To(AreaNames.ForestEncampment);
            return true;
        }
    }
}