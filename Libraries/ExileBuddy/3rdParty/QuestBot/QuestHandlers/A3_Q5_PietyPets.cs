﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A3_Q5_PietyPets
    {
        private static readonly TgtPosition PietyPortalTgt = new TgtPosition("Piety room", "templeclean_prepiety_roundtop_center_01_c1r3.tgt");

        private static Monster Piety => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Piety)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        public static void Tick()
        {
        }

        public static async Task<bool> KillPiety()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.TowerKey))
                return false;

            if (AreaNames.Current == AreaNames.LunarisTemple2)
            {
                var piety = Piety;
                if (piety != null)
                {
                    var pietyPos = piety.WalkablePosition();
                    if (pietyPos.PathExists)
                    {
                        await Helpers.MoveAndWait(pietyPos, "Waiting for Piety");
                        return true;
                    }
                }
                await Helpers.MoveAndTakeLocalTransition(PietyPortalTgt);
                return true;
            }
            await Travel.To(AreaNames.LunarisTemple2);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.SarnEncampment)
            {
                if (Helpers.PlayerHasQuestItem(QuestItemMetadata.BookPiety))
                {
                    if (!await Helpers.UseQuestItem(QuestItemMetadata.BookPiety))
                    {
                        ErrorManager.ReportError();
                        return true;
                    }
                    return false;
                }

                if (!await TownNpcs.Grigor.TakeReward(null, "Piety Reward"))
                {
                    ErrorManager.ReportError();
                }
                return true;
            }
            await Travel.To(AreaNames.SarnEncampment);
            return true;
        }
    }
}