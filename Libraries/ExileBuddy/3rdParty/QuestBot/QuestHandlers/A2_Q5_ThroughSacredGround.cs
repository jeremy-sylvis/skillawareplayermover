﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A2_Q5_ThroughSacredGround
    {
        private static readonly TgtPosition AltarTgt = new TgtPosition("Altar location", "dungeon_church_relic_altar_v01_01_c2r1.tgt");

        private static Monster Geofri => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Archbishop_Geofri_the_Abashed)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        private static Chest Altar => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Altar)
            .OfType<Chest>()
            .FirstOrDefault();

        private static WalkablePosition CachedGeofriPos
        {
            get { return CombatAreaCache.Current.Storage["GeofriPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["GeofriPosition"] = value; }
        }

        private static WalkablePosition CachedAltarPos
        {
            get { return CombatAreaCache.Current.Storage["AltarPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["AltarPosition"] = value; }
        }

        public static void Tick()
        {
            if (AreaNames.Current != AreaNames.Crypt2)
                return;

            var geofri = Geofri;
            if (geofri != null)
            {
                CachedGeofriPos = geofri.IsDead ? null : geofri.WalkablePosition();
            }

            if (CachedAltarPos == null)
            {
                var altar = Altar;
                if (altar != null)
                {
                    CachedAltarPos = altar.WalkablePosition();
                }
            }
        }

        public static async Task<bool> GrabGoldenHand()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.GoldenHand))
                return false;

            if (AreaNames.Current == AreaNames.Crypt2)
            {
                var geofriPos = CachedGeofriPos;
                if (geofriPos != null)
                {
                    geofriPos.Come();
                    return true;
                }

                var altarPos = CachedAltarPos;
                if (altarPos != null)
                {
                    if (altarPos.IsFar)
                    {
                        altarPos.Come();
                    }
                    else
                    {
                        var altar = Altar;
                        if (altar != null && !altar.IsOpened)
                        {
                            if (!await PlayerAction.Interact(altar)) ErrorManager.ReportError();
                        }
                    }
                    return true;
                }
                AltarTgt.Come();
                return true;
            }
            await Travel.To(AreaNames.Crypt2);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.ForestEncampment)
            {
                if (QuestManager.GetState(Quests.ThroughSacredGround) == 0)
                    return false;

                if (Helpers.PlayerHasQuestItem(QuestItemMetadata.BookThroughSacredGround))
                {
                    if (!await Helpers.UseQuestItem(QuestItemMetadata.BookThroughSacredGround)) ErrorManager.ReportError();
                    return true;
                }

                if (!await TownNpcs.Yeena.OpenDialogPanel())
                {
                    ErrorManager.ReportError();
                    return true;
                }
                if (LokiPoe.InGameState.NpcDialogUi.DialogEntries.Any(d => d.Text.EqualsIgnorecase("Jewel Reward")))
                {
                    string reward = Settings.Instance.GetQuestReward(Quests.ThroughSacredGround.Id + "b");
                    if (!await TownNpcs.Yeena.TakeReward(reward, "Jewel Reward")) ErrorManager.ReportError();
                    return true;
                }
                if (LokiPoe.InGameState.NpcDialogUi.DialogEntries.Any(d => d.Text.EqualsIgnorecase("Fellshrine Reward")))
                {
                    if (!await TownNpcs.Yeena.TakeReward(null, "Fellshrine Reward")) ErrorManager.ReportError();
                    return true;
                }
                return true;
            }
            await Travel.To(AreaNames.ForestEncampment);
            return true;
        }
    }
}