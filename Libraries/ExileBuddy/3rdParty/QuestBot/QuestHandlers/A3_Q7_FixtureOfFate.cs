﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Game;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A3_Q7_FixtureOfFate
    {
        private static bool _hasAllPages;

        private static Npc Siosa => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Siosa)
            .OfType<Npc>()
            .FirstOrDefault();

        private static Chest BookStand => LokiPoe.ObjectManager.Objects
            .OfType<Chest>()
            .FirstOrDefault(c => c.Metadata.Contains("QuestChests/Siosa/GoldenBookStand"));

        private static WalkablePosition CachedSiosaPos
        {
            get { return CombatAreaCache.Current.Storage["SiosaPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["SiosaPosition"] = value; }
        }

        private static Dictionary<int, WalkablePosition> BookStands
        {
            get
            {
                var stands = CombatAreaCache.Current.Storage["BookStands"] as Dictionary<int, WalkablePosition>;
                if (stands == null)
                {
                    stands = new Dictionary<int, WalkablePosition>(4);
                    CombatAreaCache.Current.Storage["BookStands"] = stands;
                }
                return stands;
            }
        }

        public static void Tick()
        {
            _hasAllPages = QuestManager.GetStateInaccurate(Quests.FixtureOfFate) <= 2;

            if (AreaNames.Current == AreaNames.Library)
            {
                if (CachedSiosaPos == null)
                {
                    var siosa = Siosa;
                    if (siosa != null)
                    {
                        CachedSiosaPos = siosa.WalkablePosition();
                    }
                }
                return;
            }

            if (AreaNames.Current == AreaNames.Archives)
            {
                foreach (var chest in LokiPoe.ObjectManager.Objects.OfType<Chest>())
                {
                    if (!chest.Metadata.Contains("QuestChests/Siosa/GoldenBookStand"))
                        continue;

                    var opened = chest.IsOpened;
                    var id = chest.Id;
                    var stands = BookStands;

                    if (stands.ContainsKey(id))
                    {
                        if (opened)
                        {
                            GlobalLog.Warn($"[FixtureOfFate] Removing opened {chest.WalkablePosition()}");
                            stands.Remove(id);
                        }
                    }
                    else
                    {
                        if (!opened)
                        {
                            var pos = chest.WalkablePosition();
                            GlobalLog.Warn($"[FixtureOfFate] Registering {pos}");
                            stands.Add(id, pos);
                        }
                    }
                }
            }
        }

        public static async Task<bool> GrabPages()
        {
            if (_hasAllPages) return false;

            if (AreaNames.Current == AreaNames.Archives)
            {
                var stands = BookStands;
                if (stands.Count != 0)
                {
                    var standPos = stands.First().Value;
                    if (standPos.IsFar)
                    {
                        standPos.Come();
                    }
                    else
                    {
                        var stand = BookStand;
                        if (stand != null && !stand.IsOpened)
                        {
                            if (!await PlayerAction.Interact(stand)) ErrorManager.ReportError();
                        }
                    }
                    return true;
                }
                if (!await ExplorationLogic.Execute(100))
                {
                    GlobalLog.Error("[GrabPages] Archives area is fully explored, but not all Golden Pages were found. Now going to re-explore the area.");
                    Explorer.GetCurrent().Reset();
                }
                return true;
            }
            await Travel.To(AreaNames.Archives);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.Library)
            {
                var siosaPos = CachedSiosaPos;
                if (siosaPos != null)
                {
                    if (siosaPos.IsFar)
                    {
                        siosaPos.Come();
                    }
                    else
                    {
                        string reward = Settings.Instance.GetQuestReward(Quests.FixtureOfFate.Id);
                        var npcSiosa = TownNpcs.CreateCustom(Siosa);
                        if (!await npcSiosa.TakeReward(reward, "Golden Pages Reward"))
                        {
                            ErrorManager.ReportError();
                            return true;
                        }
                        return false;
                    }
                    return true;
                }
                await ExplorationLogic.Execute(100);
                return true;
            }
            await Travel.To(AreaNames.Library);
            return true;
        }
    }
}