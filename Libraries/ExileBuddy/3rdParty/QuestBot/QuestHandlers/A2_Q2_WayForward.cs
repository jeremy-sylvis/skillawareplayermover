﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A2_Q2_WayForward
    {
        private static readonly TgtPosition ArteriTgt = new TgtPosition("Captain Arteri location", "tent_mid_square_alteri_v01_01_c2r1.tgt");
        private static readonly TgtPosition SealTgt = new TgtPosition("Thaumetic Seal location", "forest_mountainpass_v01_01_c2r4.tgt");

        private static Monster CaptainArteri => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Captain_Arteri)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        private static NetworkObject ThaumeticSeal => LokiPoe.ObjectManager.Objects
            .FirstOrDefault(o => o.Metadata == "Metadata/QuestObjects/Spikes/SpikeBlockageButton");

        private static WalkablePosition CachedArteriPos
        {
            get { return CombatAreaCache.Current.Storage["ArteriPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["ArteriPosition"] = value; }
        }

        private static WalkablePosition CachedSealPos
        {
            get { return CombatAreaCache.Current.Storage["ThaumeticSealPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["ThaumeticSealPosition"] = value; }
        }

        public static void Tick()
        {
            if (AreaNames.Current != AreaNames.WesternForest)
                return;

            var arteri = CaptainArteri;
            if (arteri != null)
            {
                CachedArteriPos = arteri.IsDead ? null : arteri.WalkablePosition();
            }

            if (CachedSealPos == null)
            {
                var seal = ThaumeticSeal;
                if (seal != null)
                {
                    CachedSealPos = seal.WalkablePosition();
                }
            }
        }

        public static async Task<bool> KillArteri()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.ThaumeticEmblem))
                return false;

            if (AreaNames.Current == AreaNames.WesternForest)
            {
                var arteriPos = CachedArteriPos;
                if (arteriPos != null)
                {
                    arteriPos.Come();
                }
                else
                {
                    ArteriTgt.Come();
                }
                return true;
            }
            await Travel.To(AreaNames.WesternForest);
            return true;
        }

        public static async Task<bool> OpenPath()
        {
            if (!Helpers.PlayerHasQuestItem(QuestItemMetadata.ThaumeticEmblem))
                return false;

            if (AreaNames.Current == AreaNames.WesternForest)
            {
                var sealPos = CachedSealPos;
                if (sealPos != null)
                {
                    if (sealPos.IsFar)
                    {
                        sealPos.Come();
                    }
                    else
                    {
                        var seal = ThaumeticSeal;
                        if (seal != null && seal.IsTargetable)
                        {
                            if (!await PlayerAction.Interact(seal)) ErrorManager.ReportError();
                        }
                    }
                    return true;
                }
                SealTgt.Come();
                return true;
            }
            await Travel.To(AreaNames.WesternForest);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.LioneyeWatch)
            {
                if (Helpers.PlayerHasQuestItem(QuestItemMetadata.BookWayForward))
                {
                    if (!await Helpers.UseQuestItem(QuestItemMetadata.BookWayForward))
                    {
                        ErrorManager.ReportError();
                        return true;
                    }
                    return false;
                }

                if (!await TownNpcs.Bestel.TakeReward(null, "Road Reward"))
                {
                    ErrorManager.ReportError();
                }
                return true;
            }
            await Travel.To(AreaNames.LioneyeWatch);
            return true;
        }
    }
}