﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A3_Q1_LostInLove
    {
        private static readonly TgtPosition TolmanTgt = new TgtPosition("Tolman location", "quest_marker.tgt");

        private static WalkablePosition _pietyPos;

        private static Monster Piety => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Piety)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        private static Chest Tolman => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Tolman)
            .OfType<Chest>()
            .FirstOrDefault();

        private static WalkablePosition CachedTolmanPos
        {
            get { return CombatAreaCache.Current.Storage["TolmanPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["TolmanPosition"] = value; }
        }

        public static void Tick()
        {
            if (AreaNames.Current != AreaNames.Crematorium)
                return;

            _pietyPos = Piety?.WalkablePosition();

            if (CachedTolmanPos == null)
            {
                var tolman = Tolman;
                if (tolman != null)
                {
                    CachedTolmanPos = tolman.WalkablePosition();
                }
            }
        }

        public static async Task<bool> GrabBracelet()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.TolmanBracelet))
                return false;

            if (AreaNames.Current == AreaNames.Crematorium)
            {
                if (_pietyPos != null)
                {
                    await Helpers.MoveAndWait(_pietyPos, "Waiting for Piety");
                    return true;
                }

                var tolmanPos = CachedTolmanPos;
                if (tolmanPos != null)
                {
                    if (tolmanPos.IsFar)
                    {
                        tolmanPos.Come();
                    }
                    else
                    {
                        var tolman = Tolman;
                        if (!tolman.IsOpened)
                        {
                            if (!await PlayerAction.Interact(tolman, () => Tolman.IsOpened, "Tolman interaction"))
                                ErrorManager.ReportError();
                        }
                    }
                    return true;
                }
                TolmanTgt.Come();
                return true;
            }
            await Travel.To(AreaNames.Crematorium);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.SarnEncampment)
            {
                if (!Helpers.PlayerHasQuestItem(QuestItemMetadata.SewerKeys))
                {
                    if (!await TownNpcs.Clarissa.TakeReward(null, "Take Sewer Keys"))
                    {
                        ErrorManager.ReportError();
                    }
                    return true;
                }

                string reward = Settings.Instance.GetQuestReward(Quests.LostInLove.Id);

                if (!await TownNpcs.Maramoa.TakeReward(reward, "Clarissa Reward"))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                return false;
            }
            await Travel.To(AreaNames.SarnEncampment);
            return true;
        }
    }
}