﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A2_Q4_IntrudersInBlack
    {
        private static readonly TgtPosition StrangeDeviceTgt = new TgtPosition("Strange Device location", "templeruinforest_questcart.tgt");

        private static Monster Fidelitas => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Fidelitas_the_Mourning)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        private static Chest StrangeDevice => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Strange_Device)
            .OfType<Chest>()
            .FirstOrDefault();

        private static WalkablePosition CachedFidelitasPos
        {
            get { return CombatAreaCache.Current.Storage["FidelitasPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["FidelitasPosition"] = value; }
        }

        private static WalkablePosition CachedStrangeDevicePos
        {
            get { return CombatAreaCache.Current.Storage["StrangeDevicePosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["StrangeDevicePosition"] = value; }
        }

        public static void Tick()
        {
            if (AreaNames.Current != AreaNames.ChamberOfSins2)
                return;

            var fidelitas = Fidelitas;
            if (fidelitas != null)
            {
                CachedFidelitasPos = fidelitas.IsDead ? null : fidelitas.WalkablePosition();
            }

            if (CachedStrangeDevicePos == null)
            {
                var device = StrangeDevice;
                if (device != null)
                {
                    CachedStrangeDevicePos = device.WalkablePosition();
                }
            }
        }

        public static async Task<bool> GrabBalefulGem()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.BalefulGem))
                return false;

            if (AreaNames.Current == AreaNames.ChamberOfSins2)
            {
                var fidelitasPos = CachedFidelitasPos;
                if (fidelitasPos != null)
                {
                    fidelitasPos.Come();
                    return true;
                }

                var devicePos = CachedStrangeDevicePos;
                if (devicePos != null)
                {
                    if (devicePos.IsFar)
                    {
                        devicePos.Come();
                    }
                    else
                    {
                        var device = StrangeDevice;
                        if (device != null && !device.IsOpened)
                        {
                            if (!await PlayerAction.Interact(device)) ErrorManager.ReportError();
                        }
                    }
                    return true;
                }
                StrangeDeviceTgt.Come();
                return true;
            }
            await Travel.To(AreaNames.ChamberOfSins2);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.ForestEncampment)
            {
                string reward = Settings.Instance.GetQuestReward(Quests.IntrudersInBlack.Id);

                if (!await TownNpcs.Greust.TakeReward(reward, "Blackguard Reward"))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                return false;
            }
            await Travel.To(AreaNames.ForestEncampment);
            return true;
        }
    }
}