﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A3_Q2_VictarioSecrets
    {
        private static readonly TgtPosition StashTgt = new TgtPosition("Victario stash location", "sewers_victariostash_v01_01_c1r1.tgt");

        private static Chest VictarioStash => LokiPoe.ObjectManager.Objects
            .OfType<Chest>()
            .FirstOrDefault(b => b.Metadata.Contains("QuestChests/Victario/Stash"));

        private static WalkablePosition CachedVictarioStashPos
        {
            get { return CombatAreaCache.Current.Storage["VictarioStashPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["VictarioStashPosition"] = value; }
        }

        public static bool HasAllBusts
        {
            get
            {
                int count = 0;
                foreach (var item in LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.Items.Where(i => i.Rarity == Rarity.Quest))
                {
                    if (item.Metadata.ContainsIgnorecase(QuestItemMetadata.Glyph))
                        ++count;
                }
                return count == 3;
            }
        }

        public static void Tick()
        {
            if (AreaNames.Current.Contains("Sewers"))
            {
                if (CachedVictarioStashPos == null)
                {
                    var stash = VictarioStash;
                    if (stash != null)
                    {
                        CachedVictarioStashPos = stash.WalkablePosition();
                    }
                }
            }
        }

        public static async Task<bool> GrabBust1()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.Bust + "1"))
                return false;

            if (AreaNames.Current == AreaNames.SlumSewers)
            {
                await ComeAndOpenVictarioStash();
                return true;
            }
            await Travel.To(AreaNames.SlumSewers);
            return true;
        }

        public static async Task<bool> GrabBust2()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.Bust + "2"))
                return false;

            if (AreaNames.Current == AreaNames.WarehouseSewers)
            {
                await ComeAndOpenVictarioStash();
                return true;
            }
            await Travel.To(AreaNames.WarehouseSewers);
            return true;
        }

        public static async Task<bool> GrabBust3()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.Bust + "3"))
                return false;

            if (AreaNames.Current == AreaNames.MarketSewers)
            {
                await ComeAndOpenVictarioStash();
                return true;
            }
            await Travel.To(AreaNames.MarketSewers);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.SarnEncampment)
            {
                if (Helpers.PlayerHasQuestItem(QuestItemMetadata.BookVictarioSecrets))
                {
                    if (!await Helpers.UseQuestItem(QuestItemMetadata.BookVictarioSecrets))
                    {
                        ErrorManager.ReportError();
                        return true;
                    }
                    return false;
                }

                if (!await TownNpcs.Hargan.TakeReward(null, "Victario Reward"))
                {
                    ErrorManager.ReportError();
                }
                return true;
            }
            await Travel.To(AreaNames.SarnEncampment);
            return true;
        }

        private static async Task ComeAndOpenVictarioStash()
        {
            var stashPos = CachedVictarioStashPos;
            if (stashPos != null)
            {
                if (stashPos.IsFar)
                {
                    stashPos.Come();
                }
                else
                {
                    var stash = VictarioStash;
                    if (!stash.IsOpened)
                    {
                        if (!await PlayerAction.Interact(stash, () => VictarioStash.IsOpened, "Victario Stash interaction"))
                            ErrorManager.ReportError();
                    }
                }
                return;
            }
            StashTgt.Come();
        }
    }
}