﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A4_Q3_KingOfFury
    {
        private static readonly TgtPosition KaomRoomTgt = new TgtPosition("Kaom room", "lava_lake_throne_room_v0?_0?_c1r2.tgt | lava_lake_throne_room_v0?_0?_c3r2.tgt", false, 10);

        private static Monster Kaom => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.King_Kaom)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        public static void Tick()
        {
        }

        public static async Task<bool> KillKaom()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.EyeOfFury))
                return false;

            if (AreaNames.Current == AreaNames.KaomStronghold)
            {
                var kaom = Kaom;
                if (kaom != null)
                {
                    var kaomPos = kaom.WalkablePosition();
                    if (kaomPos.PathExists)
                    {
                        await Helpers.MoveAndWait(kaomPos, "Waiting for Kaom");
                        return true;
                    }
                }
                await Helpers.MoveAndTakeLocalTransition(KaomRoomTgt);
                return true;
            }
            await Travel.To(AreaNames.KaomStronghold);
            return true;
        }

        public static async Task<bool> TurnInQuest()
        {
            if (!Helpers.PlayerHasQuestItem(QuestItemMetadata.EyeOfFury))
                return false;

            if (AreaNames.Current == AreaNames.CrystalVeins)
            {
                var dialla = Helpers.LadyDialla;
                if (dialla == null)
                {
                    GlobalLog.Error("[KingOfFury] Fail to detect Lady Dialla in Crystal Veins.");
                    ErrorManager.ReportCriticalError();
                    return true;
                }
                var npcDialla = TownNpcs.CreateCustom(dialla);
                if (!await npcDialla.Talk()) ErrorManager.ReportError();
                return true;
            }
            await Travel.To(AreaNames.CrystalVeins);
            return true;
        }
    }
}