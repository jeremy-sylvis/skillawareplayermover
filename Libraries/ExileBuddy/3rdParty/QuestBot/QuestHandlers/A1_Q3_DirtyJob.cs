﻿using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using Loki.Bot;

namespace QuestBot.QuestHandlers
{
    public static class A1_Q3_DirtyJob
    {
        private const int FinishedStateMinimum = 3;

        private static bool _finished;

        public static void Tick()
        {
            _finished = QuestManager.GetStateInaccurate(Quests.DirtyJob) <= FinishedStateMinimum;
        }

        public static async Task<bool> ClearFetidPool()
        {
            if (_finished) return false;

            if (AreaNames.Current == AreaNames.FetidPool)
            {
                if (await TrackMobLogic.Execute()) return true;

                if (!await ExplorationLogic.Execute(100))
                {
                    if (QuestManager.GetState(Quests.DirtyJob) <= FinishedStateMinimum) return false;
                    GlobalLog.Error("[ClearFetidPool] Fetid Pool area is fully explored, but not all monsters were killed. Now going to re-explore the area.");
                    Explorer.GetCurrent().Reset();
                }
                return true;
            }
            await Travel.To(AreaNames.FetidPool);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.LioneyeWatch)
            {
                if (Helpers.PlayerHasQuestItem(QuestItemMetadata.BookDirtyJob))
                {
                    if (!await Helpers.UseQuestItem(QuestItemMetadata.BookDirtyJob))
                    {
                        ErrorManager.ReportError();
                        return true;
                    }
                    return false;
                }

                if (!await TownNpcs.Tarkleigh.TakeReward(null, "Necromancer Reward"))
                {
                    ErrorManager.ReportError();
                }
                return true;
            }
            await Travel.To(AreaNames.LioneyeWatch);
            return true;
        }
    }
}