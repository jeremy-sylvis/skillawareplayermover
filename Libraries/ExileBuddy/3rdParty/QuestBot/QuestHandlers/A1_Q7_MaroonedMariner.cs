﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A1_Q7_MaroonedMariner
    {
        private static readonly TgtPosition AllflameTgt = new TgtPosition("Allflame location", "boat_small_damaged_allflame_v01_01_c1r1.tgt");
        private static readonly TgtPosition FairgravesTgt = new TgtPosition("Captain Fairgraves location", "shipwreck_quest_v01_01_c8r7.tgt");

        private static Chest SlaveGirl => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Slave_Girl)
            .OfType<Chest>()
            .FirstOrDefault();

        private static Npc CaptainFairgraves => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Captain_Fairgraves)
            .OfType<Npc>()
            .FirstOrDefault(n => n.Reaction == Reaction.Npc);

        private static Monster SkeleFairgraves => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Captain_Fairgraves)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique && m.Reaction == Reaction.Enemy);

        private static WalkablePosition CachedSlaveGirlPos
        {
            get { return CombatAreaCache.Current.Storage["SlaveGirlPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["SlaveGirlPosition"] = value; }
        }

        private static WalkablePosition CachedFairgravesPos
        {
            get { return CombatAreaCache.Current.Storage["FairgravesPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["FairgravesPosition"] = value; }
        }

        public static void Tick()
        {
            if (AreaNames.Current == AreaNames.ShipGraveyardCave)
            {
                if (CachedSlaveGirlPos == null)
                {
                    var girl = SlaveGirl;
                    if (girl != null)
                    {
                        CachedSlaveGirlPos = girl.WalkablePosition();
                    }
                }
                return;
            }

            if (AreaNames.Current == AreaNames.ShipGraveyard)
            {
                if (CachedFairgravesPos == null)
                {
                    var fairgraves = CaptainFairgraves;
                    if (fairgraves != null)
                    {
                        CachedFairgravesPos = fairgraves.WalkablePosition();
                    }
                }
            }
        }

        public static async Task<bool> GrabAllflame()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.Allflame))
                return false;

            if (AreaNames.Current == AreaNames.ShipGraveyardCave)
            {
                var girlPos = CachedSlaveGirlPos;
                if (girlPos != null)
                {
                    if (girlPos.IsFar)
                    {
                        girlPos.Come();
                    }
                    else
                    {
                        var girl = SlaveGirl;
                        if (!girl.IsOpened)
                        {
                            if (!await PlayerAction.Interact(girl, () => SlaveGirl.IsOpened, "Slave Girl interaction"))
                                ErrorManager.ReportError();
                        }
                    }
                    return true;
                }
                AllflameTgt.Come();
                return true;
            }
            await Travel.To(AreaNames.ShipGraveyardCave);
            return true;
        }

        public static async Task<bool> KillFairgraves()
        {
            if (AreaNames.Current == AreaNames.ShipGraveyard)
            {
                var skeleFairgraves = SkeleFairgraves;
                if (skeleFairgraves != null)
                {
                    if (skeleFairgraves.IsDead) return false;
                    skeleFairgraves.WalkablePosition().Come();
                    return true;
                }

                var fairgravesPos = CachedFairgravesPos;
                if (fairgravesPos != null)
                {
                    if (fairgravesPos.IsFar)
                    {
                        fairgravesPos.Come();
                    }
                    else
                    {
                        var fairgraves = CaptainFairgraves;
                        if (fairgraves != null)
                        {
                            if (await Coroutines.InteractWith(CaptainFairgraves))
                            {
                                await Coroutines.CloseBlockingWindows();
                            }
                            else
                            {
                                ErrorManager.ReportError();
                            }
                            return true;
                        }
                        GlobalLog.Debug("[KillFairgraves] We are near Fairgraves position, but Npc or Monster Fairgraves are not found. Bot does not know what to do.");
                        await Wait.Sleep(200);
                    }
                    return true;
                }
                FairgravesTgt.Come();
                return true;
            }

            if (AreaNames.Current == AreaNames.ShipGraveyardCave)
            {
                var transition = Helpers.FindNearbyTransition(200);
                if (transition != null)
                {
                    if (!await PlayerAction.TakeTransition(transition))
                        ErrorManager.ReportError();
                }
                else
                {
                    if (!await PlayerAction.TpToTown())
                        ErrorManager.ReportError();
                }
                return true;
            }
            await Travel.To(AreaNames.ShipGraveyard);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.LioneyeWatch)
            {
                if (Helpers.PlayerHasQuestItem(QuestItemMetadata.BookMariner))
                {
                    if (!await Helpers.UseQuestItem(QuestItemMetadata.BookMariner))
                    {
                        ErrorManager.ReportError();
                        return true;
                    }
                    return false;
                }

                if (!await TownNpcs.Bestel.TakeReward(null, "Fairgraves Reward"))
                {
                    ErrorManager.ReportError();
                }
                return true;
            }
            if (AreaNames.Current == AreaNames.ShipGraveyard)
            {
                await Travel.To(AreaNames.CavernOfWrath);
                return true;
            }
            await Travel.To(AreaNames.LioneyeWatch);
            return true;
        }
    }
}