﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A1_Q2_MercyMission
    {
        private static readonly TgtPosition MedicineChestTgt = new TgtPosition("Medicine Chest location", "kyrenia_boat_medicinequest_v01_01_c3r2.tgt");

        private static Monster Hailrake => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Hailrake)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        private static WalkablePosition CachedHailrakePos
        {
            get { return CombatAreaCache.Current.Storage["HailrakePosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["HailrakePosition"] = value; }
        }

        public static void Tick()
        {
            if (AreaNames.Current != AreaNames.TidalIsland)
                return;

            var hailrake = Hailrake;
            if (hailrake != null)
            {
                CachedHailrakePos = hailrake.IsDead ? null : hailrake.WalkablePosition();
            }
        }

        public static async Task<bool> KillHailrake()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.MedicineChest))
                return false;

            if (AreaNames.Current == AreaNames.TidalIsland)
            {
                var hailrakePos = CachedHailrakePos;
                if (hailrakePos != null)
                {
                    hailrakePos.Come();
                }
                else
                {
                    MedicineChestTgt.Come();
                }
                return true;
            }
            await Travel.To(AreaNames.TidalIsland);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.LioneyeWatch)
            {
                string reward = Settings.Instance.GetQuestReward(Quests.MercyMission.Id);

                if (!await TownNpcs.Nessa.TakeReward(reward, "Medicine Chest Reward"))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                return false;
            }
            await Travel.To(AreaNames.LioneyeWatch);
            return true;
        }
    }
}