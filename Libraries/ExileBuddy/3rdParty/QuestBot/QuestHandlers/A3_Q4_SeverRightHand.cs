﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A3_Q4_SeverRightHand
    {
        private static readonly TgtPosition GraviciusTgt = new TgtPosition("General Gravicius location", "temple_carpet_oneside_01_01.tgt");

        private const int FinishedStateMinimum = 1;
        private static bool _finished;

        private static Monster Gravicius => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.General_Gravicius)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        private static WalkablePosition CachedGraviciusPos
        {
            get { return CombatAreaCache.Current.Storage["GraviciusPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["GraviciusPosition"] = value; }
        }

        public static void Tick()
        {
            _finished = QuestManager.GetStateInaccurate(Quests.SeverRightHand) <= FinishedStateMinimum;

            if (AreaNames.Current == AreaNames.EbonyBarracks)
            {
                var gravicius = Gravicius;
                if (gravicius != null)
                {
                    CachedGraviciusPos = gravicius.WalkablePosition();
                }
            }
        }

        public static async Task<bool> KillGravicius()
        {
            if (_finished) return false;

            if (AreaNames.Current == AreaNames.EbonyBarracks)
            {
                var graviciusPos = CachedGraviciusPos;
                if (graviciusPos != null)
                {
                    graviciusPos.Come();
                }
                else
                {
                    GraviciusTgt.Come();
                }
                return true;
            }
            await Travel.To(AreaNames.EbonyBarracks);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.SarnEncampment)
            {
                string reward = Settings.Instance.GetQuestReward(Quests.SeverRightHand.Id);

                if (!await TownNpcs.Maramoa.TakeReward(reward, "Gravicius Reward"))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                return false;
            }
            if (AreaNames.Current == AreaNames.EbonyBarracks)
            {
                await Travel.To(AreaNames.LunarisTemple1);
                return true;
            }
            await Travel.To(AreaNames.SarnEncampment);
            return true;
        }
    }
}