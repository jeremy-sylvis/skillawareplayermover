﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Game;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A3_Q6_SwigOfHope
    {
        private static readonly TgtPosition DecanterTgt = new TgtPosition("Decanter Spiritus location", "market_place_straight_v01_01_unique1_c3r2.tgt");
        private static readonly TgtPosition PlumTgt = new TgtPosition("Plum tree location", "fruittree_c2r2.tgt");

        private static Chest DecanterChest => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Ornate_Chest)
            .OfType<Chest>()
            .FirstOrDefault();

        private static Chest PlumTree => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Plum)
            .OfType<Chest>()
            .FirstOrDefault();

        private static Npc Fairgraves => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Captain_Fairgraves)
            .OfType<Npc>()
            .FirstOrDefault();

        private static WalkablePosition CachedDecanterChestPos
        {
            get { return CombatAreaCache.Current.Storage["DecanterPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["DecanterPosition"] = value; }
        }

        private static WalkablePosition CachedPlumTreePos
        {
            get { return CombatAreaCache.Current.Storage["PlumTreePosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["PlumTreePosition"] = value; }
        }

        private static WalkablePosition CachedFairgravesPos
        {
            get { return CombatAreaCache.Current.Storage["FairgravesPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["FairgravesPosition"] = value; }
        }

        public static void Tick()
        {
            if (AreaNames.Current == AreaNames.Marketplace)
            {
                if (CachedDecanterChestPos == null)
                {
                    var decanter = DecanterChest;
                    if (decanter != null)
                    {
                        CachedDecanterChestPos = decanter.WalkablePosition();
                    }
                }
                return;
            }

            if (AreaNames.Current == AreaNames.HedgeMaze)
            {
                if (CachedPlumTreePos == null)
                {
                    var tree = PlumTree;
                    if (tree != null)
                    {
                        CachedPlumTreePos = tree.WalkablePosition();
                    }
                }
                return;
            }

            if (AreaNames.Current == AreaNames.Docks)
            {
                if (CachedFairgravesPos == null)
                {
                    var fairgraves = Fairgraves;
                    if (fairgraves != null)
                    {
                        CachedFairgravesPos = fairgraves.WalkablePosition();
                    }
                }
            }
        }

        public static async Task<bool> GrabDecanter()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.DecanterSpiritus))
                return false;

            if (AreaNames.Current == AreaNames.Marketplace)
            {
                var decanterPos = CachedDecanterChestPos;
                if (decanterPos != null)
                {
                    if (decanterPos.IsFar)
                    {
                        decanterPos.Come();
                    }
                    else
                    {
                        var decanter = DecanterChest;
                        if (!decanter.IsOpened)
                        {
                            if (!await PlayerAction.Interact(decanter, () => DecanterChest.IsOpened, "Decanter chest interaction"))
                                ErrorManager.ReportError();
                        }
                    }
                    return true;
                }
                DecanterTgt.Come();
                return true;
            }
            await Travel.To(AreaNames.Marketplace);
            return true;
        }

        public static async Task<bool> GrabPlum()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.ChitusPlum))
                return false;

            if (AreaNames.Current == AreaNames.HedgeMaze)
            {
                var treePos = CachedPlumTreePos;
                if (treePos != null)
                {
                    if (treePos.IsFar)
                    {
                        treePos.Come();
                    }
                    else
                    {
                        var tree = PlumTree;
                        if (!tree.IsOpened)
                        {
                            if (!await PlayerAction.Interact(tree, () => PlumTree.IsOpened, "Plum Tree interaction"))
                                ErrorManager.ReportError();
                        }
                    }
                    return true;
                }
                PlumTgt.Come();
                return true;
            }
            await Travel.To(AreaNames.HedgeMaze);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.Docks)
            {
                var fairgravesPos = CachedFairgravesPos;
                if (fairgravesPos != null)
                {
                    if (fairgravesPos.IsFar)
                    {
                        fairgravesPos.Come();
                    }
                    else
                    {
                        string reward = Settings.Instance.GetQuestReward(Quests.SwigOfHope.Id);
                        var npcFairgraves = TownNpcs.CreateCustom(Fairgraves);
                        if (!await npcFairgraves.TakeReward(reward, "Swig of Hope Reward"))
                        {
                            ErrorManager.ReportError();
                            return true;
                        }
                        return false;
                    }
                    return true;
                }
                await ExplorationLogic.Execute(100);
                return true;
            }
            await Travel.To(AreaNames.Docks);
            return true;
        }
    }
}