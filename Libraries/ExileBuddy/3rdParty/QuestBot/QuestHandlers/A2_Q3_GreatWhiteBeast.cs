﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A2_Q3_GreatWhiteBeast
    {
        private static readonly TgtPosition DenExitTgt = new TgtPosition("Den exit", "forestcaveup_exit_v01_01_c1r2.tgt");

        private const int FinishedStateMinimum = 2;

        private static bool _finished;

        private static Monster GreatWhiteBeast => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.The_Great_White_Beast)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        private static WalkablePosition CachedWhiteBeastPos
        {
            get { return CombatAreaCache.Current.Storage["WhiteBeastPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["WhiteBeastPosition"] = value; }
        }

        public static void Tick()
        {
            _finished = QuestManager.GetStateInaccurate(Quests.GreatWhiteBeast) <= FinishedStateMinimum;

            if (AreaNames.Current == AreaNames.Den)
            {
                var beast = GreatWhiteBeast;
                if (beast != null)
                {
                    CachedWhiteBeastPos = beast.WalkablePosition();
                }
            }
        }

        public static async Task<bool> KillWhiteBeast()
        {
            if (_finished) return false;

            if (AreaNames.Current == AreaNames.Den)
            {
                var beastPos = CachedWhiteBeastPos;
                if (beastPos != null)
                {
                    beastPos.Come();
                    return true;
                }

                if (DenExitTgt.IsFar)
                {
                    DenExitTgt.Come();
                }
                else
                {
                    DenExitTgt.ProceedToNext();
                }
                return true;
            }
            await Travel.To(AreaNames.Den);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.ForestEncampment)
            {
                string reward = Settings.Instance.GetQuestReward(Quests.GreatWhiteBeast.Id);

                if (!await TownNpcs.Yeena.TakeReward(reward, "Great White Beast Reward"))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                return false;
            }
            if (AreaNames.Current == AreaNames.Den)
            {
                var transition = Helpers.FindNearbyTransition(200);
                if (transition != null)
                {
                    if (!await PlayerAction.TakeTransition(transition))
                        ErrorManager.ReportError();
                }
                else
                {
                    if (!await PlayerAction.TpToTown())
                        ErrorManager.ReportError();
                }
                return true;
            }
            if (AreaNames.Current == AreaNames.OldFields)
            {
                await Travel.To(AreaNames.Crossroads);
                return true;
            }
            await Travel.To(AreaNames.ForestEncampment);
            return true;
        }
    }
}