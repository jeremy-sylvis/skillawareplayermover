﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A1_Q6_CagedBrute
    {
        private static bool _brutusKilled;

        private static Npc Navali => LokiPoe.ObjectManager.Objects
            .OfType<Npc>()
            .FirstOrDefault(n => n.Metadata == "Metadata/NPC/League/NavaliWild");

        private static TriggerableBlockage NavaliCage => LokiPoe.ObjectManager.Objects
            .OfType<TriggerableBlockage>()
            .FirstOrDefault(t => t.Metadata == "Metadata/NPC/League/NavaliCage");

        private static Monster Brutus => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Brutus_Lord_Incarcerator)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        public static void Tick()
        {
            _brutusKilled = World.HasWaypoint(AreaNames.PrisonerGate);
        }

        public static async Task<bool> EnterPrison()
        {
            if (AreaNames.Current == AreaNames.Climb)
            {
                var navali = Navali;
                if (navali != null)
                {
                    if (!navali.IsTargetable)
                    {
                        var navaliPos = navali.WalkablePosition();
                        if (navaliPos.FarFromMe(20))
                        {
                            navaliPos.Come();
                            return true;
                        }
                        var cage = NavaliCage;
                        if (cage == null)
                        {
                            GlobalLog.Error("[QuestBot] Unexpected error. We are near Navali, but Navali Cage is null.");
                            ErrorManager.ReportCriticalError();
                            return true;
                        }
                        if (cage.IsTargetable)
                        {
                            if (!await PlayerAction.Interact(cage, () => !NavaliCage.IsTargetable, "Navali Cage opening", 5000))
                                ErrorManager.ReportError();
                        }
                        GlobalLog.Debug("[QuestBot] Waiting for Navali");
                        StuckDetection.Reset();
                        await Wait.Sleep(200);
                        return true;
                    }

                    if (navali.HasNpcFloatingIcon)
                    {
                        var navaliPos = navali.WalkablePosition();
                        if (navaliPos.IsFar)
                        {
                            navaliPos.Come();
                            return true;
                        }
                        var npcNavali = TownNpcs.CreateCustom(navali);
                        if (await npcNavali.Talk()) await Coroutines.CloseBlockingWindows();
                        else ErrorManager.ReportError();
                        return true;
                    }
                }
            }

            if (AreaNames.Current == AreaNames.LowerPrison)
                return false;

            await Travel.To(AreaNames.LowerPrison);
            return true;
        }

        public static async Task<bool> KillBrutus()
        {
            if (_brutusKilled) return false;

            if (AreaNames.Current == AreaNames.UpperPrison)
            {
                var brutus = Brutus;
                if (brutus != null)
                {
                    var brutusPos = brutus.WalkablePosition();
                    if (brutusPos.PathExists)
                    {
                        brutusPos.Come();
                        return true;
                    }
                }
            }
            await Travel.To(AreaNames.PrisonerGate);
            return true;
        }

        public static async Task<bool> TakeReward1()
        {
            if (AreaNames.Current == AreaNames.LioneyeWatch)
            {
                string reward = Settings.Instance.GetQuestReward(Quests.CagedBrute.Id + "b");

                if (!await TownNpcs.Nessa.TakeReward(reward, "Prison Reward"))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                return false;
            }
            await Travel.To(AreaNames.LioneyeWatch);
            return true;
        }

        public static async Task<bool> TakeReward2()
        {
            if (AreaNames.Current == AreaNames.LioneyeWatch)
            {
                string reward = Settings.Instance.GetQuestReward(Quests.CagedBrute.Id);

                if (!await TownNpcs.Tarkleigh.TakeReward(reward, "Brutus Reward"))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                return false;
            }
            await Travel.To(AreaNames.LioneyeWatch);
            return true;
        }
    }
}