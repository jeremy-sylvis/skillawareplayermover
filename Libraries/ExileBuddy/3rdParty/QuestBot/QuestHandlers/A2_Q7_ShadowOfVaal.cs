﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A2_Q7_ShadowOfVaal
    {
        private static readonly WalkablePosition ClarissaPos = new WalkablePosition("Clarissa position", 560, 1278);

        private static TriggerableBlockage _darkAltar;
        private static Monster _vaal;
        private static AreaTransition _sarnTransition;
        private static Monster _guardCaptain;
        private static Npc _clarissa;

        //its more easy to track it via separate variable then parse quest state hell
        private static bool _clarissaFreed;

        //sometimes Vaal's corpse do not spawn, thus bot does not register it's death
        //lets try to track it via quest state, which is not accurate too (lol, rekt)
        private static bool _vaalKilledByQState;

        private static bool VaalKilled
        {
            get
            {
                var killed = CombatAreaCache.Current.Storage["VaalKilled"];
                return killed != null && (bool) killed;
            }
            set { CombatAreaCache.Current.Storage["VaalKilled"] = value; }
        }

        private static Monster AnyActiveMob => LokiPoe.ObjectManager.Objects
            .OfType<Monster>()
            .Where(m => m.IsActive && m.Rarity != Rarity.Unique && !Blacklist.Contains(m.Id))
            .OrderBy(m => m.Distance)
            .FirstOrDefault();


        public static void Tick()
        {
            if (AreaNames.Current == AreaNames.AncientPyramid)
            {
                _vaalKilledByQState = QuestManager.GetStateInaccurate(Quests.ShadowOfVaal) <= 4;
            }
        }

        public static async Task<bool> KillVaal()
        {
            if (AreaNames.Current == AreaNames.CityOfSarn)
                return false;

            if (AreaNames.Current == AreaNames.AncientPyramid)
            {
                UpdateVaalFightObjects();

                if (VaalKilled)
                {
                    await Travel.To(AreaNames.CityOfSarn);
                    return true;
                }

                if (_sarnTransition != null && _sarnTransition.IsTargetable)
                {
                    if (!await PlayerAction.TakeTransition(_sarnTransition)) ErrorManager.ReportError();
                    return true;
                }

                if (_darkAltar != null)
                {
                    if (!_darkAltar.IsOpened)
                    {
                        await _darkAltar.WalkablePosition().ComeAtOnce();

                        if (!await PlayerAction.Interact(_darkAltar, () => _darkAltar.IsOpened, "Dark Altar opening"))
                            ErrorManager.ReportError();

                        return true;
                    }

                    if (_vaal == null)
                    {
                        if (_vaalKilledByQState)
                        {
                            await Travel.To(AreaNames.CityOfSarn);
                        }
                        else
                        {
                            await Helpers.MoveAndWait(_darkAltar.WalkablePosition(), "Waiting for Vaal Oversoul.");
                        }
                        return true;
                    }

                    if (!_vaal.IsTargetable)
                    {
                        var mob = AnyActiveMob;
                        if (AnyActiveMob != null)
                        {
                            GlobalLog.Debug("[ShadowOfVaal] Vaal Oversoul is underground. Now going to the closest active monster.");
                            if (!PlayerMover.MoveTowards(mob.Position))
                            {
                                GlobalLog.Error($"[ShadowOfVaal] Fail to move towards \"{mob.Name}\" at {mob.Position}. Now blacklisting it.");
                                Blacklist.Add(mob.Id, TimeSpan.FromHours(1), "fail to move to");
                            }
                            return true;
                        }
                        await Helpers.MoveAndWait(_darkAltar.WalkablePosition(), "Waiting for Vaal Oversoul.");
                        return true;
                    }
                    await Helpers.MoveAndWait(_vaal.WalkablePosition(), "Waiting for Vaal Oversoul.");
                    return true;
                }
            }
            await Travel.To(AreaNames.CityOfSarn);
            return true;
        }

        public static async Task<bool> EnterSarnEncampment()
        {
            if (AreaNames.Current == AreaNames.SarnEncampment)
            {
                if (World.HasWaypoint(AreaNames.SarnEncampment))
                {
                    //reset it here so bot will free Clarissa on next difficulty
                    _clarissaFreed = false;
                    return false;
                }

                if (!await PlayerAction.OpenWaypoint()) ErrorManager.ReportError();
                return true;
            }

            if (!_clarissaFreed && AreaNames.Current == AreaNames.CityOfSarn)
            {
                UpdateCityOfSarnObjects();

                if (_guardCaptain != null && !_guardCaptain.IsDead)
                {
                    _guardCaptain.WalkablePosition().Come();
                    return true;
                }

                if (_clarissa != null)
                {
                    if (!_clarissa.IsTargetable)
                    {
                        await Helpers.MoveAndWait(_clarissa.WalkablePosition(), "Waiting for Clarissa");
                        return true;
                    }

                    if (_clarissa.HasNpcFloatingIcon)
                    {
                        var clarissaPos = _clarissa.WalkablePosition();
                        if (clarissaPos.IsFar)
                        {
                            clarissaPos.Come();
                        }
                        else
                        {
                            var npcClarissa = TownNpcs.CreateCustom(_clarissa);
                            if (await npcClarissa.Talk()) await Coroutines.CloseBlockingWindows();
                            else ErrorManager.ReportError();
                        }
                    }
                    else
                    {
                        _clarissaFreed = true;
                    }
                }
                ClarissaPos.Come();
                return true;
            }
            await Travel.To(AreaNames.SarnEncampment);
            return true;
        }

        private static void UpdateVaalFightObjects()
        {
            _darkAltar = null;
            _vaal = null;
            _sarnTransition = null;

            foreach (var obj in LokiPoe.ObjectManager.Objects)
            {
                var blockage = obj as TriggerableBlockage;
                if (blockage != null && blockage.Metadata == "Metadata/Monsters/IncaShadowBoss/IncaBossSpawner")
                {
                    _darkAltar = blockage;
                    continue;
                }

                var mob = obj as Monster;
                if (mob != null && mob.Rarity == Rarity.Unique && mob.Metadata == "Metadata/Monsters/IncaShadowBoss/BossIncaShadow")
                {
                    _vaal = mob;
                    if (!CombatAreaCache.Current.Storage.Contains("VaalKilled") && _vaal.IsDead)
                    {
                        VaalKilled = true;
                    }
                    continue;
                }

                var transition = obj as AreaTransition;
                if (transition != null && transition.Name == AreaNames.CityOfSarn)
                {
                    _sarnTransition = transition;
                }
            }
        }

        private static void UpdateCityOfSarnObjects()
        {
            _guardCaptain = null;
            _clarissa = null;

            foreach (var obj in LokiPoe.ObjectManager.Objects)
            {
                var mob = obj as Monster;
                if (mob != null && mob.Rarity == Rarity.Unique && mob.Metadata == "Metadata/Monsters/Axis/GuardLieutenantClarissa")
                {
                    _guardCaptain = mob;
                    continue;
                }

                var npc = obj as Npc;
                if (npc != null && npc.Metadata == "Metadata/NPC/Act3/ClarissaSlums")
                {
                    _clarissa = npc;
                }
            }
        }
    }
}