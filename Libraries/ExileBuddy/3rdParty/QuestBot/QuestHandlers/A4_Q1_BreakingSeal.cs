﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A4_Q1_BreakingSeal
    {
        private static readonly WalkablePosition DeshretSealPos = new WalkablePosition("Deshret Seal location", 330, 620);

        private static Monster Voll => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Voll_Emperor_of_Purity)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        private static NetworkObject DeshretSeal => LokiPoe.ObjectManager.Objects
            .FirstOrDefault(o => o.Metadata == "Metadata/QuestObjects/Act4/MineEntranceSeal");

        private static WalkablePosition CachedVollPos
        {
            get { return CombatAreaCache.Current.Storage["VollPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["VollPosition"] = value; }
        }

        public static void Tick()
        {
            if (AreaNames.Current != AreaNames.DriedLake)
                return;

            var voll = Voll;
            if (voll != null)
            {
                CachedVollPos = voll.IsDead ? null : voll.WalkablePosition();
            }
        }

        public static async Task<bool> KillVoll()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.DeshretBanner))
                return false;

            if (AreaNames.Current == AreaNames.DriedLake)
            {
                var vollPos = CachedVollPos;
                if (vollPos != null)
                {
                    vollPos.Come();
                }
                else
                {
                    if (!await ExplorationLogic.Execute(100))
                    {
                        GlobalLog.Error("[KillVoll] Dried Lake area is fully explored, but Voll was not killed. Now going to re-explore the area.");
                        Explorer.GetCurrent().Reset();
                    }
                }
                return true;
            }
            await Travel.To(AreaNames.DriedLake);
            return true;
        }

        public static async Task<bool> BreakSeal()
        {
            if (!Helpers.PlayerHasQuestItem(QuestItemMetadata.DeshretBanner))
                return false;

            if (AreaNames.Current == AreaNames.Highgate)
            {
                await DeshretSealPos.ComeAtOnce();

                var seal = DeshretSeal;
                if (seal != null && seal.IsTargetable)
                {
                    if (!await PlayerAction.Interact(seal)) ErrorManager.ReportError();
                }
                return true;
            }
            await Travel.To(AreaNames.Highgate);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.Highgate)
            {
                string reward = Settings.Instance.GetQuestReward(Quests.BreakingSeal.Id);

                if (!await TownNpcs.Oyun.TakeReward(reward, "Red Banner Reward"))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                return false;
            }
            await Travel.To(AreaNames.Highgate);
            return true;
        }
    }
}