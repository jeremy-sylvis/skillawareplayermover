﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A4_Q4_KingOfDesire
    {
        private static Monster AnyUniqueMob => LokiPoe.ObjectManager.Objects
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        private static ArenaExplorer ArenaExplorer
        {
            get
            {
                var explorer = CombatAreaCache.Current.Storage["ArenaExplorer"] as ArenaExplorer;
                if (explorer == null)
                {
                    explorer = new ArenaExplorer();
                    CombatAreaCache.Current.Storage["ArenaExplorer"] = explorer;
                }
                return explorer;
            }
        }

        public static void Tick()
        {
            if (AreaNames.Current == AreaNames.GrandArena)
            {
                ArenaExplorer.Tick();
            }
        }

        public static async Task<bool> KillDaresso()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.EyeOfDesire))
                return false;

            if (AreaNames.Current == AreaNames.GrandArena)
            {
                var mob = AnyUniqueMob;
                if (mob != null && !mob.IsDead)
                {
                    var mobPos = mob.WalkablePosition();
                    if (mobPos.PathExists)
                    {
                        await Helpers.MoveAndWait(mobPos, $"Waiting for {mob.Name}");
                        return true;
                    }
                }
                if (!await ArenaExplorer.Execute())
                {
                    GlobalLog.Warn("[KillDaresso] ArenaExplorer has reached the bossroom, but we do not have Eye of Desire. Now resetting ArenaExplorer.");
                    ArenaExplorer.Reset();
                }
                return true;
            }
            if (AreaNames.Current == AreaNames.DaressoDream)
            {
                if (!await TrackMobLogic.Execute())
                {
                    await Travel.To(AreaNames.GrandArena);
                }
                return true;
            }
            await Travel.To(AreaNames.GrandArena);
            return true;
        }

        public static async Task<bool> TurnInQuest()
        {
            if (!Helpers.PlayerHasQuestItem(QuestItemMetadata.EyeOfDesire))
                return false;

            if (AreaNames.Current == AreaNames.CrystalVeins)
            {
                var dialla = Helpers.LadyDialla;
                if (dialla == null)
                {
                    GlobalLog.Error("[KingOfDesire] Fail to detect Lady Dialla in Crystal Veins.");
                    ErrorManager.ReportCriticalError();
                    return true;
                }
                var npcDialla = TownNpcs.CreateCustom(dialla);
                if (!await npcDialla.Talk()) ErrorManager.ReportError();
                return true;
            }
            await Travel.To(AreaNames.CrystalVeins);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.Highgate)
            {
                string reward = Settings.Instance.GetQuestReward(Quests.EternalNightmare.Id);

                if (!await TownNpcs.Dialla.TakeReward(reward, "Rapture Reward"))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                return false;
            }
            await Travel.To(AreaNames.Highgate);
            return true;
        }
    }
}