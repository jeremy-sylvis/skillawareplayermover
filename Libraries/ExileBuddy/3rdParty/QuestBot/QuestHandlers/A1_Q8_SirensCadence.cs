﻿using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A1_Q8_SirensCadence
    {
        private static bool _merveilKilled;
        private static Monster _merveil1;
        private static Monster _merveil2;

        public static void Tick()
        {
            _merveilKilled = World.HasWaypoint(AreaNames.SouthernForest);
        }

        public static async Task<bool> KillMerveil()
        {
            if (_merveilKilled) return false;

            if (AreaNames.Current == AreaNames.CavernOfAnger)
            {
                UpdateMerveilFightObjects();

                if (_merveil2 != null)
                {
                    await Helpers.MoveAndWait(_merveil2.WalkablePosition(), "Merveil, the Twisted");
                    return true;
                }

                if (_merveil1 != null)
                {
                    await Helpers.MoveAndWait(_merveil1.WalkablePosition(10, 50), "Waiting for Merveil, the Siren");
                    return true;
                }
            }
            await Travel.To(AreaNames.SouthernForest);
            return true;
        }

        public static async Task<bool> EnterForestEncampment()
        {
            if (AreaNames.Current == AreaNames.ForestEncampment)
            {
                if (World.HasWaypoint(AreaNames.ForestEncampment))
                    return false;

                if (!await PlayerAction.OpenWaypoint()) ErrorManager.ReportError();
                return true;
            }

            await Travel.To(AreaNames.ForestEncampment);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.LioneyeWatch)
            {
                string reward = Settings.Instance.GetQuestReward(Quests.SirensCadence.Id);

                if (!await TownNpcs.Nessa.TakeReward(reward, "Merveil Reward"))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                return false;
            }
            await Travel.To(AreaNames.LioneyeWatch);
            return true;
        }

        private static void UpdateMerveilFightObjects()
        {
            _merveil1 = null;
            _merveil2 = null;

            foreach (var obj in LokiPoe.ObjectManager.Objects)
            {
                var mob = obj as Monster;
                if (mob != null && mob.Rarity == Rarity.Unique)
                {
                    if (mob.Metadata.EqualsIgnorecase("Metadata/Monsters/seawitchillusion/BossMerveil"))
                    {
                        _merveil1 = mob;
                        continue;
                    }
                    if (mob.Metadata == "Metadata/Monsters/Seawitch/BossMerveil2")
                    {
                        _merveil2 = mob;
                    }
                }
            }
        }
    }
}