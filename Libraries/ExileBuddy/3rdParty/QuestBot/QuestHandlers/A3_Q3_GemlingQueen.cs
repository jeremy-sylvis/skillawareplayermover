﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Game;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A3_Q3_GemlingQueen
    {
        private static readonly TgtPosition RibbonTgt = new TgtPosition("Ribbon spool location", "act3_brokenbridge_v01_01_c16r5.tgt");
        private static readonly TgtPosition SulphiteTgt = new TgtPosition("Thaumetic Sulphite location", "templeruinforest_questcart.tgt");
        private static readonly TgtPosition DiallaTgt = new TgtPosition("Lady Dialla location", "gemling_queen_throne_v01_01_c3r3.tgt");

        private static Chest RibbonChest => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Blackguard_Chest)
            .OfType<Chest>()
            .FirstOrDefault();

        private static Chest SulphiteChest => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Supply_Container)
            .OfType<Chest>()
            .FirstOrDefault();

        private static WalkablePosition CachedRibbonChestPos
        {
            get { return CombatAreaCache.Current.Storage["RibbonSpoolPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["RibbonSpoolPosition"] = value; }
        }

        private static WalkablePosition CachedSulphiteChestPos
        {
            get { return CombatAreaCache.Current.Storage["ThaumeticSulphitePosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["ThaumeticSulphitePosition"] = value; }
        }

        private static WalkablePosition CachedDiallaPos
        {
            get { return CombatAreaCache.Current.Storage["DiallaPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["DiallaPosition"] = value; }
        }

        public static void Tick()
        {
            if (AreaNames.Current == AreaNames.Battlefront)
            {
                if (CachedRibbonChestPos == null)
                {
                    var ribbon = RibbonChest;
                    if (ribbon != null)
                    {
                        CachedRibbonChestPos = ribbon.WalkablePosition();
                    }
                }
                return;
            }
            if (AreaNames.Current == AreaNames.Docks)
            {
                if (CachedSulphiteChestPos == null)
                {
                    var sulphite = SulphiteChest;
                    if (sulphite != null)
                    {
                        CachedSulphiteChestPos = sulphite.WalkablePosition();
                    }
                }
                return;
            }
            if (AreaNames.Current == AreaNames.SolarisTemple2)
            {
                if (CachedDiallaPos == null)
                {
                    var dialla = Helpers.LadyDialla;
                    if (dialla != null)
                    {
                        CachedDiallaPos = dialla.WalkablePosition();
                    }
                }
            }
        }

        public static async Task<bool> GrabRibbon()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.RibbonSpool))
                return false;

            if (AreaNames.Current == AreaNames.Battlefront)
            {
                var ribbonPos = CachedRibbonChestPos;
                if (ribbonPos != null)
                {
                    if (ribbonPos.IsFar)
                    {
                        ribbonPos.Come();
                    }
                    else
                    {
                        var ribbon = RibbonChest;
                        if (!ribbon.IsOpened)
                        {
                            if (!await PlayerAction.Interact(ribbon, () => RibbonChest.IsOpened, "Ribbon chest interaction"))
                                ErrorManager.ReportError();
                        }
                    }
                    return true;
                }
                RibbonTgt.Come();
                return true;
            }
            await Travel.To(AreaNames.Battlefront);
            return true;
        }

        public static async Task<bool> GrabSulphite()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.ThaumeticSulphite))
                return false;

            if (AreaNames.Current == AreaNames.Docks)
            {
                var sulphitePos = CachedSulphiteChestPos;
                if (sulphitePos != null)
                {
                    if (sulphitePos.IsFar)
                    {
                        sulphitePos.Come();
                    }
                    else
                    {
                        var sulphite = SulphiteChest;
                        if (!sulphite.IsOpened)
                        {
                            if (!await PlayerAction.Interact(sulphite, () => SulphiteChest.IsOpened, "Sulphite chest interaction"))
                                ErrorManager.ReportError();
                        }
                    }
                    return true;
                }
                SulphiteTgt.Come();
                return true;
            }
            await Travel.To(AreaNames.Docks);
            return true;
        }

        public static async Task<bool> TakeRibbonReward()
        {
            return await TakeDiallaReward(false);
        }

        public static async Task<bool> TakeTalcReward()
        {
            return await TakeDiallaReward(true);
        }

        private static async Task<bool> TakeDiallaReward(bool talc)
        {
            if (AreaNames.Current == AreaNames.SolarisTemple2)
            {
                var diallaPos = CachedDiallaPos;
                if (diallaPos != null)
                {
                    if (diallaPos.IsFar)
                    {
                        diallaPos.Come();
                    }
                    else
                    {
                        var npcDialla = TownNpcs.CreateCustom(Helpers.LadyDialla);
                        if (talc)
                        {
                            if (!await npcDialla.TakeReward(null, "Take Infernal Talc"))
                            {
                                ErrorManager.ReportError();
                                return true;
                            }
                            return false;
                        }

                        string reward = Settings.Instance.GetQuestReward(Quests.RibbonSpool.Id);
                        if (!await npcDialla.TakeReward(reward, "Ribbon Spool Reward"))
                        {
                            ErrorManager.ReportError();
                            return true;
                        }
                        return false;
                    }
                    return true;
                }
                DiallaTgt.Come();
                return true;
            }
            await Travel.To(AreaNames.SolarisTemple2);
            return true;
        }
    }
}