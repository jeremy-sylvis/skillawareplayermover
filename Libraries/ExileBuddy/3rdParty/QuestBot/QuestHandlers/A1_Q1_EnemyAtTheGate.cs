﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A1_Q1_EnemyAtTheGate
    {
        private static Npc DyingExile => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Dying_Exile)
            .OfType<Npc>()
            .FirstOrDefault(npc => npc.IsTargetable);

        private static Monster Hillock => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Hillock)
            .OfType<Monster>()
            .FirstOrDefault(m => m.Rarity == Rarity.Unique);

        private static WalkablePosition CachedHillockPos
        {
            get { return CombatAreaCache.Current.Storage["HillockPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["HillockPosition"] = value; }
        }

        public static void Tick()
        {
            if (AreaNames.Current != AreaNames.TwilightStrand)
                return;

            var hillok = Hillock;
            if (hillok != null)
            {
                CachedHillockPos = hillok.IsDead ? null : hillok.WalkablePosition();
            }
        }

        public static async Task<bool> EnterLioneyeWatch()
        {
            if (AreaNames.Current != AreaNames.TwilightStrand)
                return false;

            var dyingExile = DyingExile;
            if (dyingExile != null && dyingExile.IsTargetable && dyingExile.HasNpcFloatingIcon)
            {
                var npcExile = TownNpcs.CreateCustom(dyingExile);
                if (await npcExile.Talk()) await Coroutines.CloseBlockingWindows();
                else ErrorManager.ReportError();
                return true;
            }

            var hillockPos = CachedHillockPos;
            if (hillockPos != null)
            {
                hillockPos.Come();
            }
            else
            {
                await Travel.To(AreaNames.LioneyeWatch);
            }
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.LioneyeWatch)
            {
                string reward = Settings.Instance.GetQuestReward(Quests.EnemyAtTheGate.Id);

                if (!await TownNpcs.Tarkleigh.TakeReward(reward, "Hillock Reward"))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                return false;
            }
            await Travel.To(AreaNames.LioneyeWatch);
            return true;
        }
    }
}