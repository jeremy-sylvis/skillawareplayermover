﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Game;
using Loki.Game.Objects;

namespace QuestBot.QuestHandlers
{
    public static class A1_Q4_BreakingSomeEggs
    {
        private const string NestTgtName = "beach_rhoanest_v01_01_c2r2.tgt";

        private static Chest RhoaNest => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Rhoa_Nest)
            .OfType<Chest>()
            .OrderBy(c => c.Distance)
            .FirstOrDefault();

        private static NetworkObject GlyphWall => LokiPoe.ObjectManager.Objects
            .FirstOrDefault(o => o.Metadata == "Metadata/QuestObjects/WaterCave/GlyphWall");

        private static Queue<WalkablePosition> NestTgtQueue
        {
            get
            {
                var nests = CombatAreaCache.Current.Storage["NestTgtQueue"] as Queue<WalkablePosition>;
                if (nests == null)
                {
                    nests = FindRhoaNestTgts();
                    CombatAreaCache.Current.Storage["NestTgtQueue"] = nests;
                }
                return nests;
            }
        }

        private static WalkablePosition CachedGlyphWallPos
        {
            get { return CombatAreaCache.Current.Storage["GlyphWallPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["GlyphWallPosition"] = value; }
        }

        public static void Tick()
        {
            if (AreaNames.Current == AreaNames.MudFlats)
            {
                if (CachedGlyphWallPos == null)
                {
                    var wall = GlyphWall;
                    if (wall != null)
                    {
                        CachedGlyphWallPos = wall.WalkablePosition();
                    }
                }
            }
        }

        public static async Task<bool> CollectGlyphs()
        {
            if (Helpers.PlayerHasQuestItemAmount(QuestItemMetadata.Glyph, 3))
                return false;

            if (AreaNames.Current == AreaNames.MudFlats)
            {
                var nests = NestTgtQueue;
                if (nests.Count == 0)
                {
                    GlobalLog.Error("[OpenSubmergedPassage] Next Rhoa Nest tgt is requested, but nest queue is empty.");
                    ErrorManager.ReportError();
                    return true;
                }

                var nestPos = nests.Peek();
                if (nestPos.IsFar)
                {
                    nestPos.Come();
                }
                else
                {
                    var nest = RhoaNest;
                    if (!nest.IsOpened)
                    {
                        if (!await PlayerAction.Interact(nest, () => RhoaNest.IsOpened, "Rhoa Nest opening"))
                            ErrorManager.ReportError();
                    }
                    else
                    {
                        NestTgtQueue.Dequeue();
                    }
                }
                return true;
            }
            await Travel.To(AreaNames.MudFlats);
            return true;
        }

        public static async Task<bool> OpenSubmergedPassage()
        {
            if (!Helpers.PlayerHasQuestItemAmount(QuestItemMetadata.Glyph, 3))
                return false;

            if (AreaNames.Current == AreaNames.MudFlats)
            {
                var wallPos = CachedGlyphWallPos;
                if (wallPos != null)
                {
                    if (wallPos.IsFar)
                    {
                        wallPos.Come();
                    }
                    else
                    {
                        if (!await PlayerAction.Interact(GlyphWall, () => !GlyphWall.IsTargetable, "Glyph Wall interaction"))
                            ErrorManager.ReportError();
                    }
                    return true;
                }
                await Travel.To(AreaNames.SubmergedPassage);
                return true;
            }
            await Travel.To(AreaNames.MudFlats);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (AreaNames.Current == AreaNames.LioneyeWatch)
            {
                string reward = Settings.Instance.GetQuestReward(Quests.BreakingSomeEggs.Id);

                if (!await TownNpcs.Tarkleigh.TakeReward(reward, "Glyph Reward"))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                return false;
            }
            if (AreaNames.Current == AreaNames.MudFlats)
            {
                await Travel.To(AreaNames.SubmergedPassage);
                return true;
            }
            await Travel.To(AreaNames.LioneyeWatch);
            return true;
        }

        private static Queue<WalkablePosition> FindRhoaNestTgts()
        {
            var result = new List<WalkablePosition>();
            var tgtEntries = LokiPoe.TerrainData.TgtEntries;
            for (int i = 0; i < tgtEntries.GetLength(0); i++)
            {
                for (int j = 0; j < tgtEntries.GetLength(1); j++)
                {
                    var entry = tgtEntries[i, j];
                    if (entry == null) continue;
                    if (entry.TgtName.Contains(NestTgtName))
                    {
                        var pos = new WalkablePosition("Rhoa Nest", i*23, j*23);
                        if (result.Count == 0 || result.All(p => p.AsVector.Distance(pos) > 50))
                        {
                            result.Add(pos);
                        }
                    }
                }
            }
            if (result.Count != 3)
            {
                GlobalLog.Error($"[FindRhoaNestTgts] Abnormal tgt count: {result.Count}. Should be 3.");
                ErrorManager.ReportCriticalError();
            }
            result.Sort(WorldPosition.ComparerByDistanceSqr.Instance);
            return new Queue<WalkablePosition>(result);
        }
    }
}