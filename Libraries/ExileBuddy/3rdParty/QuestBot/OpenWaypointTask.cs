﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Game;
using Loki.Game.Objects;

namespace QuestBot
{
    public class OpenWaypointTask : ITask
    {
        private static readonly Interval ScanInterval = new Interval(500);

        private static WalkablePosition _waypointTgtPos;
        private static bool _sceptreSpecial;
        private static bool _enabled;

        private static WalkablePosition WaypointPos
        {
            get { return CombatAreaCache.Current.Storage["WaypointPosition"] as WalkablePosition; }
            set { CombatAreaCache.Current.Storage["WaypointPosition"] = value; }
        }

        static OpenWaypointTask()
        {
            Events.CombatAreaChanged += OnCombatAreaChanged;
        }

        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type != "task_execute" || !_enabled || !LokiPoe.CurrentWorldArea.IsOverworldArea)
                return false;

            var wpPos = WaypointPos;
            if (wpPos != null)
            {
                if (wpPos.IsFar)
                {
                    wpPos.Come();
                }
                else
                {
                    if (await PlayerAction.OpenWaypoint())
                    {
                        await Coroutines.CloseBlockingWindows();
                        _enabled = false;
                    }
                    else
                    {
                        ErrorManager.ReportError();
                    }
                }
                return true;
            }
            if (_waypointTgtPos == null)
            {
                var pos = Tgt.FindWaypoint();
                if (pos == null)
                {
                    GlobalLog.Error($"[OpenWaypointTask] Fail to find any walkable waypoint tgt. Skipping this task for \"{AreaNames.Current}\".");
                    _enabled = false;
                    return true;
                }
                _waypointTgtPos = new WalkablePosition("Waypoint", pos);
            }
            _waypointTgtPos.Come();
            return true;
        }

        public void Tick()
        {
            if ((!_enabled && !_sceptreSpecial) ||
                !ScanInterval.Elapsed ||
                !LokiPoe.IsInGame ||
                !LokiPoe.CurrentWorldArea.IsOverworldArea ||
                WaypointPos != null) return;

            var wp = LokiPoe.ObjectManager.Objects.OfType<Waypoint>().FirstOrDefault();
            if (wp != null)
            {
                WaypointPos = wp.WalkablePosition();
            }

            if (_sceptreSpecial)
            {
                if (wp != null)
                {
                    GlobalLog.Warn("[OpenWaypointTask] Enabled (waypoint object detected)");
                    _enabled = true;
                    _sceptreSpecial = false;
                    return;
                }
                if (Helpers.FindTransitionByName(AreaNames.UpperSceptreOfGod) != null)
                {
                    GlobalLog.Warn("[OpenWaypointTask] Enabled (Upper Sceptre of God transition detected)");
                    _enabled = true;
                    _sceptreSpecial = false;
                }
            }
        }

        private static void OnCombatAreaChanged(object sender, AreaChangedArgs args)
        {
            _waypointTgtPos = null;
            _enabled = false;
            _sceptreSpecial = false;

            var area = args.NewArea;
            if (area.IsOverworldArea && area.HasWaypoint && !World.HasWaypoint(area.Name))
            {
                if (area.Name != AreaNames.SceptreOfGod)
                {
                    GlobalLog.Warn("[OpenWaypointTask] Enabled.");
                    _enabled = true;
                }
                else
                {
                    _sceptreSpecial = true;
                }
            }
        }

        #region Unused interface methods

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public string Name => "OpenWaypointTask";
        public string Description => "Task that handles waypoint opening.";
        public string Author => "ExVault";
        public string Version => "1.0";

        #endregion
    }
}