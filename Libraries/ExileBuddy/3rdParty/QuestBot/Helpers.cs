﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using EXtensions.Positions;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot
{
    public static class Helpers
    {
        public static bool AnyWpNearby => LokiPoe.ObjectManager.Objects.OfType<Waypoint>().Any();

        public static Npc LadyDialla => LokiPoe.ObjectManager
            .GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Dialla, LokiPoe.ObjectManager.PoeObjectEnum.Lady_Dialla)
            .OfType<Npc>()
            .FirstOrDefault();

        public static Monster ActiveUniqueMob
        {
            get
            {
                return LokiPoe.ObjectManager.Objects
                    .OfType<Monster>()
                    .Where(m => m.Rarity == Rarity.Unique && m.IsActive)
                    .OrderBy(m => m.Distance)
                    .FirstOrDefault();
            }
        }

        public static Monster FindUniqueMonster(string name)
        {
            return LokiPoe.ObjectManager.Objects
                .OfType<Monster>()
                .FirstOrDefault(m => m.Rarity == Rarity.Unique && m.Name == name);
        }

        public static bool PlayerHasQuestItem(string metadata)
        {
            return LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.Items
                .Any(i => i.Rarity == Rarity.Quest && i.Metadata.ContainsIgnorecase(metadata));
        }

        public static bool PlayerHasQuestItemAmount(string metadata, int amount)
        {
            int count = 0;
            foreach (var item in LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.Items.Where(i => i.Rarity == Rarity.Quest))
            {
                if (item.Metadata.ContainsIgnorecase(metadata))
                    ++count;
            }
            return count == amount;
        }

        public static AreaTransition FindTransitionByName(string name)
        {
            return LokiPoe.ObjectManager.Objects
                .OfType<AreaTransition>()
                .FirstOrDefault(a => a.Name == name);
        }

        public static AreaTransition FindNearbyTransition(int minDistance)
        {
            return LokiPoe.ObjectManager.Objects
                .OfType<AreaTransition>()
                .Where(a => a.Distance <= minDistance)
                .OrderBy(a => a.Distance)
                .FirstOrDefault();
        }

        public static void ReportInvalidArea(string caller)
        {
            GlobalLog.Error($"[{caller}] Current area \"{AreaNames.Current}\" is invalid for this quest stage.");
            ErrorManager.ReportCriticalError();
        }

        public static async Task MoveAndTakeLocalTransition(TgtPosition tgtPos)
        {
            if (tgtPos.IsFar)
            {
                tgtPos.Come();
                return;
            }

            var transition = LokiPoe.ObjectManager.Objects
                .OfType<AreaTransition>()
                .OrderBy(t => t.Distance)
                .FirstOrDefault();

            if (transition == null)
            {
                GlobalLog.Error("[MoveAndTakeLocalTransition] There is no area transition near tgt position.");
                tgtPos.ProceedToNext();
                return;
            }

            if (transition.TransitionType != TransitionTypes.Local)
            {
                GlobalLog.Error("[MoveAndTakeLocalTransition] Area transition is not local.");
                tgtPos.ProceedToNext();
                return;
            }

            if (!await PlayerAction.TakeTransition(transition)) ErrorManager.ReportError();
        }

        public static async Task MoveAndWait(WalkablePosition pos, string log)
        {
            if (pos.IsFar)
            {
                pos.Come();
            }
            else
            {
                GlobalLog.Debug(log);
                StuckDetection.Reset();
                await Wait.Sleep(200);
            }
        }

        public static async Task<bool> UseQuestItem(string metadata)
        {
            var item = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.Items
                .Where(i => i.Rarity == Rarity.Quest)
                .FirstOrDefault(i => i.Metadata.ContainsIgnorecase(metadata));

            if (item == null)
            {
                GlobalLog.Error($"[UseQuestItem] Fail to find item with metadata \"{metadata}\" in inventory.");
                return false;
            }

            var pos = item.LocationTopLeft;
            var id = item.LocalId;
            var name = string.Copy(item.Name);

            GlobalLog.Debug($"[UseQuestItem] Now going to use \"{name}\".");

            if (!await Inventories.OpenInventory())
            {
                ErrorManager.ReportError();
                return false;
            }

            var used = LokiPoe.InGameState.InventoryUi.InventoryControl_Main.UseItem(id);
            if (used != UseItemResult.None)
            {
                GlobalLog.Error($"[UseQuestItem] Fail to use \"{name}\". Error: \"{used}\".");
                return false;
            }
            if (!await Wait.For(() => LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.FindItemByPos(pos) == null,
                "quest item despawn", 100, 2000)) return false;

            if (LokiPoe.InGameState.CursorItemOverlay.Item != null)
            {
                GlobalLog.Error($"[UseQuestItem] Error. \"{name}\" has been picked to cursor.");
                return false;
            }
            return true;
        }

        public static bool TransitionBetween(Vector2i point, int distanceFromPoint = 10, int stride = 10)
        {
            var transitions = LokiPoe.ObjectManager.Objects.OfType<AreaTransition>().ToList();
            if (transitions.Count == 0) return false;
            var pointsOnSegment = ExilePather.GetPointsOnSegment(LokiPoe.MyPosition, point, true);
            for (int i = 0; i < pointsOnSegment.Count; i += stride)
            {
                if (transitions.Any(transition => transition.Position.Distance(pointsOnSegment[i]) <= distanceFromPoint))
                    return true;
            }
            return false;
        }
    }
}