﻿using System.Linq;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Positions;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace QuestBot
{
    public class ReturnAfterDeathTask : ITask
    {
        private WalkablePosition _transitionPos;

        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type == "player_resurrected_event")
            {
                if (LokiPoe.Me.IsInTown)
                {
                    GlobalLog.Debug("[ReturnAfterDeathTask] We are in town. Skipping this task.");
                    _transitionPos = null;
                }
                else
                {
                    var transition = NearbyLocalTransition;
                    if (transition == null)
                    {
                        GlobalLog.Debug("[ReturnAfterDeathTask] There is no local area transition nearby. Skipping this task.");
                        _transitionPos = null;
                    }
                    else
                    {
                        _transitionPos = transition.WalkablePosition();
                        GlobalLog.Debug($"[ReturnAfterDeathTask] Detected local transition {_transitionPos}");
                    }
                }
                return true;
            }

            if (type != "task_execute") return false;
            if (_transitionPos == null || LokiPoe.Me.IsDead) return false;

            var areaName = AreaNames.Current;
            if (areaName == AreaNames.Harvest || areaName == AreaNames.BellyOfTheBeast2 || areaName == AreaNames.DaressoDream)
            {
                GlobalLog.Debug($"[ReturnAfterDeathTask] Transitions in {areaName} are handled by HandleDoorsTask.");
                _transitionPos = null;
                return true;
            }

            if (!_transitionPos.IsInitialized)
            {
                if (!_transitionPos.Initialize())
                {
                    GlobalLog.Debug("[ReturnAfterDeathTask] Transition is unwalkable. Skipping this task.");
                    _transitionPos = null;
                    return true;
                }
            }

            if (_transitionPos.IsFar)
            {
                _transitionPos.Come();
            }
            else
            {
                if (await PlayerAction.TakeClosestTransition()) _transitionPos = null;
                else ErrorManager.ReportError();
            }
            return true;
        }

        private static AreaTransition NearbyLocalTransition
        {
            get
            {
                return LokiPoe.ObjectManager.Objects
                    .OfType<AreaTransition>()
                    .FirstOrDefault(a => a.TransitionType == TransitionTypes.Local && a.Distance <= 50);
            }
        }

        #region Unused interface methods

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public void Tick()
        {
        }

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public string Name => "ReturnAfterDeathTask";
        public string Description => "Task for taking closest local transition after death.";
        public string Author => "ExVault";
        public string Version => "1.0";

        #endregion
    }
}