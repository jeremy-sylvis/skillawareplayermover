﻿namespace QuestBot
{
    public static class QuestItemMetadata
    {
        public const string MedicineChest = "MedicineSet";
        public const string Glyph = "Glyphs/Glyph";
        public const string Allflame = "AllFlameLantern";
        public const string MaligaroSpike = "PoisonSpear";
        public const string ThaumeticEmblem = "SpikeSealKey";
        public const string BalefulGem = "PoisonSkillGem";
        public const string GoldenHand = "GoldenHand";
        public const string Apex = "CombinedAmulet";
        public const string TolmanBracelet = "TolmanBracelet";
        public const string SewerKeys = "SewerKeys";
        public const string Bust = "Busts/Bust";
        public const string RibbonSpool = "RibbonSpool";
        public const string ThaumeticSulphite = "SulphiteFlask";
        public const string InfernalTalc = "InfernalTalc";
        public const string TowerKey = "TowerKey";
        public const string DecanterSpiritus = "Fairgraves/Decanter";
        public const string ChitusPlum = "Fairgraves/Fruit";
        public const string GoldenPage = "GoldenPages/Page";
        public const string DeshretBanner = "RedBanner";
        public const string EyeOfFury = "KaomGem";
        public const string EyeOfDesire = "DaressoGem";

        public const string BookDirtyJob = "Book-a1q8";
        public const string BookDweller = "Book-a1q7";
        public const string BookMariner = "Book-a1q6";
        public const string BookWayForward = "Book-a1q9";
        public const string BookThroughSacredGround = "Book-a2q5";
        public const string BookVictarioSecrets = "Book-a3q11";
        public const string BookPiety = "Book-a3q9";
        public const string BookDeshretSpirit = "Book-a4q6";

        public const string AmuletAlira = "IntAmulet";
        public const string AmuletKraityn = "DexAmulet";
        public const string AmuletOak = "StrAmulet";
    }
}