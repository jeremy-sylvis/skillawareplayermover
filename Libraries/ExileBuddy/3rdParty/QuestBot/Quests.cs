﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using Loki.Game.GameData;

namespace QuestBot
{
    [SuppressMessage("ReSharper", "UnassignedReadonlyField")]
    public static class Quests
    {
        /************************************
         *             Act 1
         ***********************************/

        [QuestId("a1q1")] public static readonly DatQuestWrapper EnemyAtTheGate;

        [QuestId("a1q5")] public static readonly DatQuestWrapper MercyMission;

        [QuestId("a1q8")] public static readonly DatQuestWrapper DirtyJob;

        [QuestId("a1q4")] public static readonly DatQuestWrapper BreakingSomeEggs;

        [QuestId("a1q7")] public static readonly DatQuestWrapper DwellerOfTheDeep;

        [QuestId("a1q2")] public static readonly DatQuestWrapper CagedBrute;

        [QuestId("a1q9")] public static readonly DatQuestWrapper WayForward;

        [QuestId("a1q6")] public static readonly DatQuestWrapper MaroonedMariner;

        [QuestId("a1q3")] public static readonly DatQuestWrapper SirensCadence;


        /************************************
         *             Act 2
         ***********************************/

        [QuestId("a2q4")] public static readonly DatQuestWrapper SharpAndCruel;

        //not really used anywhere
        //[QuestId("a2q11")]
        //public static readonly DatQuestWrapper WayForwardAct2;

        [QuestId("a2q10")] public static readonly DatQuestWrapper GreatWhiteBeast;

        [QuestId("a2q6")] public static readonly DatQuestWrapper IntrudersInBlack;

        [QuestId("a2q5")] public static readonly DatQuestWrapper ThroughSacredGround;

        [QuestId("a2q7")] public static readonly DatQuestWrapper DealWithBandits;

        [QuestId("a2q9")] public static readonly DatQuestWrapper RootOfTheProblem;

        [QuestId("a2q8")] public static readonly DatQuestWrapper ShadowOfVaal;


        /************************************
         *             Act 3
         ***********************************/

        [QuestId("a3q1")] public static readonly DatQuestWrapper LostInLove;

        [QuestId("a3q11")] public static readonly DatQuestWrapper VictarioSecrets;

        [QuestId("a3q4")] public static readonly DatQuestWrapper RibbonSpool;

        [QuestId("a3q5")] public static readonly DatQuestWrapper FieryDust;

        [QuestId("a3q3")] public static readonly DatQuestWrapper GemlingQueen;

        [QuestId("a3q8")] public static readonly DatQuestWrapper SeverRightHand;

        [QuestId("a3q9")] public static readonly DatQuestWrapper PietyPets;

        [QuestId("a3q13")] public static readonly DatQuestWrapper SwigOfHope;

        [QuestId("a3q12")] public static readonly DatQuestWrapper FixtureOfFate;

        [QuestId("a3q10")] public static readonly DatQuestWrapper SceptreOfGod;


        /************************************
         *             Act 4
         ***********************************/

        [QuestId("a4q2")] public static readonly DatQuestWrapper BreakingSeal;

        [QuestId("a4q6")] public static readonly DatQuestWrapper IndomitableSpirit;

        [QuestId("a4q3")] public static readonly DatQuestWrapper KingOfFury;

        [QuestId("a4q4")] public static readonly DatQuestWrapper KingOfDesire;

        [QuestId("a4q1")] public static readonly DatQuestWrapper EternalNightmare;

        static Quests()
        {
            Dictionary<string, FieldInfo> questsMap = new Dictionary<string, FieldInfo>();
            foreach (var field in typeof(Quests).GetFields())
            {
                var idAttribute = field.GetCustomAttribute<QuestId>();
                if (idAttribute == null) continue;
                questsMap.Add(idAttribute.Id, field);
            }

            int processed = 0;
            int total = questsMap.Count;
            foreach (var quest in Dat.Quests)
            {
                if (processed >= total) break;
                FieldInfo field;
                if (questsMap.TryGetValue(quest.Id, out field))
                {
                    field.SetValue(null, quest);
                    ++processed;
                }
            }
        }

        [AttributeUsage(AttributeTargets.Field)]
        public class QuestId : Attribute
        {
            public QuestId(string id)
            {
                Id = id;
            }

            public string Id { get; set; }
        }
    }
}