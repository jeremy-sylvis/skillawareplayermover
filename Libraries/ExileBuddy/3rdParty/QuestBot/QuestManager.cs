﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using EXtensions;
using EXtensions.Global;
using Loki;
using Loki.Game;
using Loki.Game.GameData;
using Newtonsoft.Json;
using QuestBot.QuestHandlers;

namespace QuestBot
{
    public static class QuestManager
    {
        private static Difficulty CurrentDiff => LokiPoe.CurrentWorldArea.Difficulty;
        private static int CurrentAct => LokiPoe.CurrentWorldArea.Act;

        public static QuestHandler GetQuestHandler()
        {
            if (LokiPoe.Me.IsInHideout)
            {
                var lastOpenedAct = World.LastOpenedAct;
                var diff = lastOpenedAct.Difficulty;
                if (diff != Difficulty.Normal)
                {
                    var name = lastOpenedAct.Name;
                    UpdateGuiAndLog("Travel", $"Go to last opened act: \"{name}\" ({diff})");
                    return new QuestHandler(() => GoToLastOpenedAct(name, diff), null);
                }
            }

            //Just enter Lioneye's Watch if we are in Twilight Strand
            if (AreaNames.Current == AreaNames.TwilightStrand)
            {
                UpdateGuiAndLog(Quests.EnemyAtTheGate.Name, "Enter Lioneye's Watch");
                return new QuestHandler(A1_Q1_EnemyAtTheGate.EnterLioneyeWatch, A1_Q1_EnemyAtTheGate.Tick);
            }

            //Enemy at the Gate           
            if (QuestIsNotCompleted(Quests.EnemyAtTheGate))
            {
                if (CurrentAct != 1)
                {
                    UpdateGuiAndLog(Quests.EnemyAtTheGate.Name, "Travel to Act 1");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.LioneyeWatch), null);
                }

                int state = GetState(Quests.EnemyAtTheGate);
                if (state == 0)
                {
                    CompletedQuests.Instance.Add(Quests.EnemyAtTheGate);
                    return QuestHandler.QuestAddedToCache;
                }
                if (state == 1)
                {
                    UpdateGuiAndLog(Quests.EnemyAtTheGate.Name, "Take reward");
                    return new QuestHandler(A1_Q1_EnemyAtTheGate.TakeReward, null);
                }
                GlobalLog.Error($"[EnemyAtTheGate] Unknown quest state and area combination. State: {state}. Area: {AreaNames.Current}.");
                return null;
            }

            //Mercy Mission
            if (Settings.Instance.IsQuestEnabled(Quests.MercyMission))
            {
                if (QuestIsNotCompleted(Quests.MercyMission))
                {
                    if (CurrentAct != 1)
                    {
                        UpdateGuiAndLog(Quests.MercyMission.Name, "Travel to Act 1");
                        return new QuestHandler(() => TravelToProperAct(AreaNames.LioneyeWatch), null);
                    }

                    int state = GetState(Quests.MercyMission);
                    if (state == 0)
                    {
                        CompletedQuests.Instance.Add(Quests.MercyMission);
                        return QuestHandler.QuestAddedToCache;
                    }
                    if (state >= 3)
                    {
                        UpdateGuiAndLog(Quests.MercyMission.Name, "Kill Hailrake");
                        return new QuestHandler(A1_Q2_MercyMission.KillHailrake, A1_Q2_MercyMission.Tick);
                    }
                    UpdateGuiAndLog(Quests.MercyMission.Name, "Take reward");
                    return new QuestHandler(A1_Q2_MercyMission.TakeReward, null);
                }
            }

            //Dirty Job
            if (Settings.Instance.IsQuestEnabled(Quests.DirtyJob))
            {
                if (QuestIsNotCompleted(Quests.DirtyJob))
                {
                    if (CurrentAct != 1)
                    {
                        UpdateGuiAndLog(Quests.DirtyJob.Name, "Travel to Act 1");
                        return new QuestHandler(() => TravelToProperAct(AreaNames.LioneyeWatch), null);
                    }

                    int state = GetState(Quests.DirtyJob);
                    if (state == 0)
                    {
                        CompletedQuests.Instance.Add(Quests.DirtyJob);
                        return QuestHandler.QuestAddedToCache;
                    }
                    if (state >= 4)
                    {
                        UpdateGuiAndLog(Quests.DirtyJob.Name, "Clear Fetid Pool");
                        return new QuestHandler(A1_Q3_DirtyJob.ClearFetidPool, A1_Q3_DirtyJob.Tick);
                    }
                    UpdateGuiAndLog(Quests.DirtyJob.Name, "Take reward");
                    return new QuestHandler(A1_Q3_DirtyJob.TakeReward, null);
                }
            }

            //Breaking Some Eggs
            if (QuestIsNotCompleted(Quests.BreakingSomeEggs))
            {
                if (Helpers.PlayerHasQuestItemAmount(QuestItemMetadata.Glyph, 3))
                {
                    UpdateGuiAndLog(Quests.BreakingSomeEggs.Name, "Open Submerged Passage");
                    return new QuestHandler(A1_Q4_BreakingSomeEggs.OpenSubmergedPassage, A1_Q4_BreakingSomeEggs.Tick);
                }
                if (CurrentAct != 1)
                {
                    UpdateGuiAndLog(Quests.BreakingSomeEggs.Name, "Travel to Act 1");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.LioneyeWatch), null);
                }

                int state = GetState(Quests.BreakingSomeEggs);
                if (state == 0)
                {
                    CompletedQuests.Instance.Add(Quests.BreakingSomeEggs);
                    return QuestHandler.QuestAddedToCache;
                }
                if (state == 1)
                {
                    UpdateGuiAndLog(Quests.BreakingSomeEggs.Name, "Take reward");
                    return new QuestHandler(A1_Q4_BreakingSomeEggs.TakeReward, null);
                }
                UpdateGuiAndLog(Quests.BreakingSomeEggs.Name, "Grab glyphs");
                return new QuestHandler(A1_Q4_BreakingSomeEggs.CollectGlyphs, A1_Q4_BreakingSomeEggs.Tick);
            }

            //Dweller of the Deep
            if (Settings.Instance.IsQuestEnabled(Quests.DwellerOfTheDeep))
            {
                if (QuestIsNotCompleted(Quests.DwellerOfTheDeep))
                {
                    if (CurrentAct != 1)
                    {
                        UpdateGuiAndLog(Quests.DwellerOfTheDeep.Name, "Travel to Act 1");
                        return new QuestHandler(() => TravelToProperAct(AreaNames.LioneyeWatch), null);
                    }

                    int state = GetState(Quests.DwellerOfTheDeep);
                    if (state == 0)
                    {
                        CompletedQuests.Instance.Add(Quests.DwellerOfTheDeep);
                        return QuestHandler.QuestAddedToCache;
                    }
                    if (state >= 4)
                    {
                        UpdateGuiAndLog(Quests.DwellerOfTheDeep.Name, "Kill Deep Dweller");
                        return new QuestHandler(A1_Q5_DwellerOfTheDeep.KillDweller, A1_Q5_DwellerOfTheDeep.Tick);
                    }
                    UpdateGuiAndLog(Quests.DwellerOfTheDeep.Name, "Take reward");
                    return new QuestHandler(A1_Q5_DwellerOfTheDeep.TakeReward, null);
                }
            }

            //Caged Brute
            if (QuestIsNotCompleted(Quests.CagedBrute))
            {
                if (CurrentAct != 1)
                {
                    UpdateGuiAndLog(Quests.CagedBrute.Name, "Travel to Act 1");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.LioneyeWatch), null);
                }

                int state = GetState(Quests.CagedBrute);
                if (state == 0)
                {
                    CompletedQuests.Instance.Add(Quests.CagedBrute);
                    return QuestHandler.QuestAddedToCache;
                }
                if (state <= 2)
                {
                    UpdateGuiAndLog(Quests.CagedBrute.Name, "Take reward 2");
                    return new QuestHandler(A1_Q6_CagedBrute.TakeReward2, null);
                }
                if (state == 4)
                {
                    UpdateGuiAndLog(Quests.CagedBrute.Name, "Take reward 1");
                    return new QuestHandler(A1_Q6_CagedBrute.TakeReward1, null);
                }
                if (state == 3 || state == 5 || state == 6)
                {
                    UpdateGuiAndLog(Quests.CagedBrute.Name, "Kill Brutus");
                    return new QuestHandler(A1_Q6_CagedBrute.KillBrutus, A1_Q6_CagedBrute.Tick);
                }
                UpdateGuiAndLog(Quests.CagedBrute.Name, "Enter Prison");
                return new QuestHandler(A1_Q6_CagedBrute.EnterPrison, A1_Q6_CagedBrute.Tick);
            }

            //Marooned Mariner
            if (Settings.Instance.IsQuestEnabled(Quests.MaroonedMariner))
            {
                if (QuestIsNotCompleted(Quests.MaroonedMariner))
                {
                    if (CurrentAct != 1)
                    {
                        UpdateGuiAndLog(Quests.MaroonedMariner.Name, "Travel to Act 1");
                        return new QuestHandler(() => TravelToProperAct(AreaNames.LioneyeWatch), null);
                    }

                    int state = GetState(Quests.MaroonedMariner);
                    if (state == 0)
                    {
                        CompletedQuests.Instance.Add(Quests.MaroonedMariner);
                        return QuestHandler.QuestAddedToCache;
                    }
                    if (state == 4 || state == 5 || Helpers.PlayerHasQuestItem(QuestItemMetadata.Allflame))
                    {
                        UpdateGuiAndLog(Quests.MaroonedMariner.Name, "Kill Captain Fairgraves");
                        return new QuestHandler(A1_Q7_MaroonedMariner.KillFairgraves, A1_Q7_MaroonedMariner.Tick);
                    }
                    if (state <= 3)
                    {
                        UpdateGuiAndLog(Quests.MaroonedMariner.Name, "Take reward");
                        return new QuestHandler(A1_Q7_MaroonedMariner.TakeReward, null);
                    }
                    UpdateGuiAndLog(Quests.MaroonedMariner.Name, "Grab Allflame");
                    return new QuestHandler(A1_Q7_MaroonedMariner.GrabAllflame, A1_Q7_MaroonedMariner.Tick);
                }
            }

            //Sirens Cadence
            if (QuestIsNotCompleted(Quests.SirensCadence))
            {
                if (World.HasWaypoint(AreaNames.SouthernForest) && !World.HasWaypoint(AreaNames.ForestEncampment))
                {
                    UpdateGuiAndLog(Quests.SirensCadence.Name, "Enter Forest Encampment");
                    return new QuestHandler(A1_Q8_SirensCadence.EnterForestEncampment, A1_Q8_SirensCadence.Tick);
                }
                if (CurrentAct != 1)
                {
                    UpdateGuiAndLog(Quests.SirensCadence.Name, "Travel to Act 1");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.LioneyeWatch), null);
                }

                int state = GetState(Quests.SirensCadence);
                if (state == 0)
                {
                    CompletedQuests.Instance.Add(Quests.SirensCadence);
                    return QuestHandler.QuestAddedToCache;
                }
                if (state == 6 || state == 1)
                {
                    UpdateGuiAndLog(Quests.SirensCadence.Name, "Take reward");
                    return new QuestHandler(A1_Q8_SirensCadence.TakeReward, null);
                }
                UpdateGuiAndLog(Quests.SirensCadence.Name, "Kill Merveil");
                return new QuestHandler(A1_Q8_SirensCadence.KillMerveil, A1_Q8_SirensCadence.Tick);
            }

            //At this stage check Act 2 waypoint
            if (!World.HasWaypoint(AreaNames.ForestEncampment))
            {
                UpdateGuiAndLog(Quests.SirensCadence.Name, "Enter Forest Encampment");
                return new QuestHandler(A1_Q8_SirensCadence.EnterForestEncampment, A1_Q8_SirensCadence.Tick);
            }

            //Sharp and Cruel
            if (QuestIsNotCompleted(Quests.SharpAndCruel, 1))
            {
                if (CurrentAct != 2)
                {
                    UpdateGuiAndLog(Quests.SharpAndCruel.Name, "Travel to Act 2");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.ForestEncampment), null);
                }

                int state = GetState(Quests.SharpAndCruel);
                if (state <= 1)
                {
                    CompletedQuests.Instance.Add(Quests.SharpAndCruel);
                    return QuestHandler.QuestAddedToCache;
                }
                if (state >= 6)
                {
                    UpdateGuiAndLog(Quests.SharpAndCruel.Name, "Kill Weaver");
                    return new QuestHandler(A2_Q1_SharpAndCruel.KillWeaver, A2_Q1_SharpAndCruel.Tick);
                }
                UpdateGuiAndLog(Quests.SharpAndCruel.Name, "Take reward");
                return new QuestHandler(A2_Q1_SharpAndCruel.TakeReward, null);
            }

            //The Way Forward
            if (Settings.Instance.IsQuestEnabled(Quests.WayForward))
            {
                if (QuestIsNotCompleted(Quests.WayForward))
                {
                    if (Helpers.PlayerHasQuestItem(QuestItemMetadata.ThaumeticEmblem))
                    {
                        UpdateGuiAndLog(Quests.WayForward.Name, "Open path");
                        return new QuestHandler(A2_Q2_WayForward.OpenPath, A2_Q2_WayForward.Tick);
                    }

                    //this quest spans across two acts, we cannot get an accurate state for it from act 2
                    int state;
                    if (CurrentAct == 1)
                    {
                        state = GetState(Quests.WayForward);
                        if (state == 0)
                        {
                            CompletedQuests.Instance.Add(Quests.WayForward);
                            return QuestHandler.QuestAddedToCache;
                        }
                    }
                    else
                    {
                        state = GetStateInaccurate(Quests.WayForward);
                    }
                    if (state <= 3)
                    {
                        UpdateGuiAndLog(Quests.WayForward.Name, "Take reward");
                        return new QuestHandler(A2_Q2_WayForward.TakeReward, null);
                    }
                    UpdateGuiAndLog(Quests.WayForward.Name, "Kill Arteri");
                    return new QuestHandler(A2_Q2_WayForward.KillArteri, A2_Q2_WayForward.Tick);
                }
            }

            //The Great White Beast
            if (Settings.Instance.IsQuestEnabled(Quests.GreatWhiteBeast))
            {
                if (QuestIsNotCompleted(Quests.GreatWhiteBeast))
                {
                    if (CurrentAct != 2)
                    {
                        UpdateGuiAndLog(Quests.GreatWhiteBeast.Name, "Travel to Act 2");
                        return new QuestHandler(() => TravelToProperAct(AreaNames.ForestEncampment), null);
                    }

                    int state = GetState(Quests.GreatWhiteBeast);
                    if (state == 0)
                    {
                        CompletedQuests.Instance.Add(Quests.GreatWhiteBeast);
                        return QuestHandler.QuestAddedToCache;
                    }
                    if (state >= 3)
                    {
                        UpdateGuiAndLog(Quests.GreatWhiteBeast.Name, "Kill Great White Beast");
                        return new QuestHandler(A2_Q3_GreatWhiteBeast.KillWhiteBeast, A2_Q3_GreatWhiteBeast.Tick);
                    }
                    UpdateGuiAndLog(Quests.GreatWhiteBeast.Name, "Take reward");
                    return new QuestHandler(A2_Q3_GreatWhiteBeast.TakeReward, null);
                }
            }

            //Intruders in Black
            if (QuestIsNotCompleted(Quests.IntrudersInBlack))
            {
                if (CurrentAct != 2)
                {
                    UpdateGuiAndLog(Quests.IntrudersInBlack.Name, "Travel to Act 2");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.ForestEncampment), null);
                }

                int state = GetState(Quests.IntrudersInBlack);
                if (state == 0)
                {
                    CompletedQuests.Instance.Add(Quests.IntrudersInBlack);
                    return QuestHandler.QuestAddedToCache;
                }
                if (state == 1 || Helpers.PlayerHasQuestItem(QuestItemMetadata.BalefulGem))
                {
                    UpdateGuiAndLog(Quests.IntrudersInBlack.Name, "Take reward");
                    return new QuestHandler(A2_Q4_IntrudersInBlack.TakeReward, null);
                }
                UpdateGuiAndLog(Quests.IntrudersInBlack.Name, "Grab Baleful Gem");
                return new QuestHandler(A2_Q4_IntrudersInBlack.GrabBalefulGem, A2_Q4_IntrudersInBlack.Tick);
            }

            //Through Sacred Ground
            if (Settings.Instance.IsQuestEnabled(Quests.ThroughSacredGround))
            {
                if (QuestIsNotCompleted(Quests.ThroughSacredGround))
                {
                    if (CurrentAct != 2)
                    {
                        UpdateGuiAndLog(Quests.ThroughSacredGround.Name, "Travel to Act 2");
                        return new QuestHandler(() => TravelToProperAct(AreaNames.ForestEncampment), null);
                    }

                    int state = GetState(Quests.ThroughSacredGround);
                    if (state == 0)
                    {
                        CompletedQuests.Instance.Add(Quests.ThroughSacredGround);
                        return QuestHandler.QuestAddedToCache;
                    }
                    if (state >= 7)
                    {
                        UpdateGuiAndLog(Quests.ThroughSacredGround.Name, "Grab Golden Hand");
                        return new QuestHandler(A2_Q5_ThroughSacredGround.GrabGoldenHand, A2_Q5_ThroughSacredGround.Tick);
                    }
                    UpdateGuiAndLog(Quests.ThroughSacredGround.Name, "Take reward");
                    return new QuestHandler(A2_Q5_ThroughSacredGround.TakeReward, null);
                }
            }

            //Deal with Bandits
            if (QuestIsNotCompleted(Quests.DealWithBandits, 1))
            {
                if (CurrentAct != 2)
                {
                    UpdateGuiAndLog(Quests.DealWithBandits.Name, "Travel to Act 2");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.ForestEncampment), null);
                }

                int state = GetState(Quests.DealWithBandits);
                if (state <= 1)
                {
                    CompletedQuests.Instance.Add(Quests.DealWithBandits);
                    return QuestHandler.QuestAddedToCache;
                }

                var handler = AnalyzeBandits();
                if (handler == null)
                {
                    GlobalLog.Error($"[DealWithBandits] Fail to analyze bandit amulets. Quest state: {state}.");
                    return null;
                }
                return handler;
            }

            //Shadow of the Vaal
            if (!World.HasWaypoint(AreaNames.CityOfSarn))
            {
                UpdateGuiAndLog(Quests.ShadowOfVaal.Name, "Kill Vaal Oversoul");
                return new QuestHandler(A2_Q7_ShadowOfVaal.KillVaal, A2_Q7_ShadowOfVaal.Tick);
            }

            //At this stage check Act 3 waypoint
            if (!World.HasWaypoint(AreaNames.SarnEncampment))
            {
                UpdateGuiAndLog(Quests.ShadowOfVaal.Name, "Enter Sarn Encampment");
                return new QuestHandler(A2_Q7_ShadowOfVaal.EnterSarnEncampment, A2_Q7_ShadowOfVaal.Tick);
            }

            //Lost in Love
            if (QuestIsNotCompleted(Quests.LostInLove, 1))
            {
                if (CurrentAct != 3)
                {
                    UpdateGuiAndLog(Quests.LostInLove.Name, "Travel to Act 3");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.SarnEncampment), null);
                }

                int state = GetState(Quests.LostInLove);
                if (state <= 1)
                {
                    CompletedQuests.Instance.Add(Quests.LostInLove);
                    return QuestHandler.QuestAddedToCache;
                }
                if (state <= 5 || Helpers.PlayerHasQuestItem(QuestItemMetadata.TolmanBracelet))
                {
                    UpdateGuiAndLog(Quests.LostInLove.Name, "Take reward");
                    return new QuestHandler(A3_Q1_LostInLove.TakeReward, null);
                }
                UpdateGuiAndLog(Quests.LostInLove.Name, "Grab Bracelet");
                return new QuestHandler(A3_Q1_LostInLove.GrabBracelet, A3_Q1_LostInLove.Tick);
            }

            //Victario Secrets
            if (Settings.Instance.IsQuestEnabled(Quests.VictarioSecrets))
            {
                if (QuestIsNotCompleted(Quests.VictarioSecrets))
                {
                    if (CurrentAct != 3)
                    {
                        UpdateGuiAndLog(Quests.VictarioSecrets.Name, "Travel to Act 3");
                        return new QuestHandler(() => TravelToProperAct(AreaNames.SarnEncampment), null);
                    }

                    int state = GetState(Quests.VictarioSecrets);
                    if (state == 0)
                    {
                        CompletedQuests.Instance.Add(Quests.VictarioSecrets);
                        return QuestHandler.QuestAddedToCache;
                    }
                    if (state <= 6 || state == 8 || state == 10 || Helpers.PlayerHasQuestItemAmount(QuestItemMetadata.Bust, 3))
                    {
                        UpdateGuiAndLog(Quests.VictarioSecrets.Name, "Take reward");
                        return new QuestHandler(A3_Q2_VictarioSecrets.TakeReward, null);
                    }

                    if (state == 32 || state == 33 || state == 34)
                    {
                        var handler = AnalyzeBusts();
                        //should never be the case because we are cheking for 3 busts before this
                        if (handler == null)
                        {
                            GlobalLog.Error($"[VictarioSecrets] Fail to analyze platinum busts. Quest state: {state}.");
                            return null;
                        }
                        return handler;
                    }

                    int bust;
                    if (StateToBustMap.TryGetValue(state, out bust))
                    {
                        if (bust == 1)
                        {
                            UpdateGuiAndLog(Quests.VictarioSecrets.Name, "Grab Bust of Hector Titucius (1)");
                            return new QuestHandler(A3_Q2_VictarioSecrets.GrabBust1, A3_Q2_VictarioSecrets.Tick);
                        }
                        if (bust == 2)
                        {
                            UpdateGuiAndLog(Quests.VictarioSecrets.Name, "Grab Bust of Gaius Sentari (2)");
                            return new QuestHandler(A3_Q2_VictarioSecrets.GrabBust2, A3_Q2_VictarioSecrets.Tick);
                        }
                        UpdateGuiAndLog(Quests.VictarioSecrets.Name, "Grab Bust of Marceus Lioneye (3)");
                        return new QuestHandler(A3_Q2_VictarioSecrets.GrabBust3, A3_Q2_VictarioSecrets.Tick);
                    }
                    GlobalLog.Error($"[VictarioSecrets] Unsupported quest state: {state}.");
                    return null;
                }
            }

            //Swig of Hope (early decanter)
            if (Settings.Instance.IsQuestEnabled(Quests.SwigOfHope))
            {
                if (GetStateInaccurate(Quests.SwigOfHope) >= 8 && !Helpers.PlayerHasQuestItem(QuestItemMetadata.DecanterSpiritus))
                {
                    UpdateGuiAndLog(Quests.SwigOfHope.Name, "Grab Decanter Spiritus");
                    return new QuestHandler(A3_Q6_SwigOfHope.GrabDecanter, A3_Q6_SwigOfHope.Tick);
                }
            }

            //RibbonSpool, FieryDust, Gemling Queen
            if (QuestIsNotCompleted(Quests.RibbonSpool))
            {
                if (CurrentAct != 3)
                {
                    UpdateGuiAndLog(Quests.RibbonSpool.Name, "Travel to Act 3");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.SarnEncampment), null);
                }

                int state = GetState(Quests.RibbonSpool);
                if (state == 0)
                {
                    CompletedQuests.Instance.Add(Quests.RibbonSpool);
                    return QuestHandler.QuestAddedToCache;
                }
                if (state != 1 && !Helpers.PlayerHasQuestItem(QuestItemMetadata.RibbonSpool))
                {
                    UpdateGuiAndLog(Quests.RibbonSpool.Name, "Grab Ribbon Spool");
                    return new QuestHandler(A3_Q3_GemlingQueen.GrabRibbon, A3_Q3_GemlingQueen.Tick);
                }
            }
            if (QuestIsNotCompleted(Quests.FieryDust, 3))
            {
                if (CurrentAct != 3)
                {
                    UpdateGuiAndLog(Quests.FieryDust.Name, "Travel to Act 3");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.SarnEncampment), null);
                }

                int dustState = GetState(Quests.FieryDust);
                if (dustState <= 3)
                {
                    CompletedQuests.Instance.Add(Quests.FieryDust);
                    return QuestHandler.QuestAddedToCache;
                }
                if (dustState != 4 && !Helpers.PlayerHasQuestItem(QuestItemMetadata.ThaumeticSulphite))
                {
                    UpdateGuiAndLog(Quests.FieryDust.Name, "Grab Thaumetic Sulphite");
                    return new QuestHandler(A3_Q3_GemlingQueen.GrabSulphite, A3_Q3_GemlingQueen.Tick);
                }
                int ribbonState = GetState(Quests.RibbonSpool);
                if (ribbonState != 0)
                {
                    UpdateGuiAndLog(Quests.RibbonSpool.Name, "Take reward");
                    return new QuestHandler(A3_Q3_GemlingQueen.TakeRibbonReward, A3_Q3_GemlingQueen.Tick);
                }
                UpdateGuiAndLog(Quests.FieryDust.Name, "Take reward");
                return new QuestHandler(A3_Q3_GemlingQueen.TakeTalcReward, A3_Q3_GemlingQueen.Tick);
            }

            //Sever the Right Hand
            if (QuestIsNotCompleted(Quests.SeverRightHand))
            {
                if (CurrentAct != 3)
                {
                    UpdateGuiAndLog(Quests.SeverRightHand.Name, "Travel to Act 3");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.SarnEncampment), null);
                }

                int state = GetState(Quests.SeverRightHand);
                if (state == 0)
                {
                    CompletedQuests.Instance.Add(Quests.SeverRightHand);
                    return QuestHandler.QuestAddedToCache;
                }
                if (state >= 2)
                {
                    UpdateGuiAndLog(Quests.SeverRightHand.Name, "Kill General Gravicius");
                    return new QuestHandler(A3_Q4_SeverRightHand.KillGravicius, A3_Q4_SeverRightHand.Tick);
                }
                UpdateGuiAndLog(Quests.SeverRightHand.Name, "Take reward");
                return new QuestHandler(A3_Q4_SeverRightHand.TakeReward, null);
            }

            //Piety Pets
            if (QuestIsNotCompleted(Quests.PietyPets))
            {
                if (CurrentAct != 3)
                {
                    UpdateGuiAndLog(Quests.PietyPets.Name, "Travel to Act 3");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.SarnEncampment), null);
                }

                int state = GetState(Quests.PietyPets);
                if (state == 0)
                {
                    CompletedQuests.Instance.Add(Quests.PietyPets);
                    return QuestHandler.QuestAddedToCache;
                }
                if (state >= 4)
                {
                    UpdateGuiAndLog(Quests.PietyPets.Name, "Kill Piety");
                    return new QuestHandler(A3_Q5_PietyPets.KillPiety, A3_Q5_PietyPets.Tick);
                }
                UpdateGuiAndLog(Quests.PietyPets.Name, "Take reward");
                return new QuestHandler(A3_Q5_PietyPets.TakeReward, null);
            }

            //Swig of Hope
            if (Settings.Instance.IsQuestEnabled(Quests.SwigOfHope))
            {
                if (QuestIsNotCompleted(Quests.SwigOfHope))
                {
                    if (CurrentAct != 3)
                    {
                        UpdateGuiAndLog(Quests.SwigOfHope.Name, "Travel to Act 3");
                        return new QuestHandler(() => TravelToProperAct(AreaNames.SarnEncampment), null);
                    }

                    int state = GetState(Quests.SwigOfHope);
                    if (state == 0)
                    {
                        CompletedQuests.Instance.Add(Quests.SwigOfHope);
                        return QuestHandler.QuestAddedToCache;
                    }
                    if (state > 6)
                    {
                        // 7 = decanter has been already delivered
                        if (state != 7 && !Helpers.PlayerHasQuestItem(QuestItemMetadata.DecanterSpiritus))
                        {
                            UpdateGuiAndLog(Quests.SwigOfHope.Name, "Grab Decanter Spiritus");
                            return new QuestHandler(A3_Q6_SwigOfHope.GrabDecanter, A3_Q6_SwigOfHope.Tick);
                        }
                        // 9 = plum has been already delivered
                        if (state != 9 && !Helpers.PlayerHasQuestItem(QuestItemMetadata.ChitusPlum))
                        {
                            UpdateGuiAndLog(Quests.SwigOfHope.Name, "Grab Chitus Plum");
                            return new QuestHandler(A3_Q6_SwigOfHope.GrabPlum, A3_Q6_SwigOfHope.Tick);
                        }
                    }
                    UpdateGuiAndLog(Quests.SwigOfHope.Name, "Take reward");
                    return new QuestHandler(A3_Q6_SwigOfHope.TakeReward, A3_Q6_SwigOfHope.Tick);
                }
            }

            //Fixture of Fate
            if (Settings.Instance.IsQuestEnabled(Quests.FixtureOfFate))
            {
                if (QuestIsNotCompleted(Quests.FixtureOfFate))
                {
                    if (CurrentAct != 3)
                    {
                        UpdateGuiAndLog(Quests.FixtureOfFate.Name, "Travel to Act 3");
                        return new QuestHandler(() => TravelToProperAct(AreaNames.SarnEncampment), null);
                    }

                    int state = GetState(Quests.FixtureOfFate);
                    if (state == 0)
                    {
                        CompletedQuests.Instance.Add(Quests.FixtureOfFate);
                        return QuestHandler.QuestAddedToCache;
                    }
                    if (state >= 3)
                    {
                        UpdateGuiAndLog(Quests.FixtureOfFate.Name, "Grab Golden Pages");
                        return new QuestHandler(A3_Q7_FixtureOfFate.GrabPages, A3_Q7_FixtureOfFate.Tick);
                    }
                    UpdateGuiAndLog(Quests.FixtureOfFate.Name, "Take reward");
                    return new QuestHandler(A3_Q7_FixtureOfFate.TakeReward, A3_Q7_FixtureOfFate.Tick);
                }
            }

            //Sceptre of God
            if (!World.HasWaypoint(AreaNames.Aqueduct))
            {
                UpdateGuiAndLog(Quests.SceptreOfGod.Name, "Kill Dominus");
                return new QuestHandler(A3_Q8_SceptreOfGod.KillDominus, A3_Q8_SceptreOfGod.Tick);
            }

            //At this stage check Act 4 waypoint
            if (!World.HasWaypoint(AreaNames.Highgate))
            {
                UpdateGuiAndLog(Quests.SceptreOfGod.Name, "Enter Highgate");
                return new QuestHandler(A3_Q8_SceptreOfGod.EnterHighGate, null);
            }

            //Breaking the Seal
            if (QuestIsNotCompleted(Quests.BreakingSeal))
            {
                if (Helpers.PlayerHasQuestItem(QuestItemMetadata.DeshretBanner))
                {
                    UpdateGuiAndLog(Quests.BreakingSeal.Name, "Break Deshret Seal");
                    return new QuestHandler(A4_Q1_BreakingSeal.BreakSeal, A4_Q1_BreakingSeal.Tick);
                }
                if (CurrentAct != 4)
                {
                    UpdateGuiAndLog(Quests.BreakingSeal.Name, "Travel to Act 4");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.Highgate), null);
                }

                int state = GetState(Quests.BreakingSeal);
                if (state == 0)
                {
                    CompletedQuests.Instance.Add(Quests.BreakingSeal);
                    return QuestHandler.QuestAddedToCache;
                }
                if (state <= 2)
                {
                    UpdateGuiAndLog(Quests.BreakingSeal.Name, "Take reward");
                    return new QuestHandler(A4_Q1_BreakingSeal.TakeReward, null);
                }
                UpdateGuiAndLog(Quests.BreakingSeal.Name, "Kill Voll");
                return new QuestHandler(A4_Q1_BreakingSeal.KillVoll, A4_Q1_BreakingSeal.Tick);
            }

            //Indomitable Spirit
            if (Settings.Instance.IsQuestEnabled(Quests.IndomitableSpirit))
            {
                if (QuestIsNotCompleted(Quests.IndomitableSpirit))
                {
                    if (CurrentAct != 4)
                    {
                        UpdateGuiAndLog(Quests.IndomitableSpirit.Name, "Travel to Act 4");
                        return new QuestHandler(() => TravelToProperAct(AreaNames.Highgate), null);
                    }

                    int state = GetState(Quests.IndomitableSpirit);
                    if (state == 0)
                    {
                        CompletedQuests.Instance.Add(Quests.IndomitableSpirit);
                        return QuestHandler.QuestAddedToCache;
                    }
                    if (state >= 3)
                    {
                        UpdateGuiAndLog(Quests.IndomitableSpirit.Name, "Free Deshret Spirit");
                        return new QuestHandler(A4_Q2_IndomitableSpirit.FreeDeshret, A4_Q2_IndomitableSpirit.Tick);
                    }
                    UpdateGuiAndLog(Quests.IndomitableSpirit.Name, "Take reward");
                    return new QuestHandler(A4_Q2_IndomitableSpirit.TakeReward, null);
                }
            }

            //King of Fury
            if (QuestIsNotCompleted(Quests.KingOfFury))
            {
                if (Helpers.PlayerHasQuestItem(QuestItemMetadata.EyeOfFury))
                {
                    UpdateGuiAndLog(Quests.KingOfFury.Name, "Bring Eye of Fury to Dialla");
                    return new QuestHandler(A4_Q3_KingOfFury.TurnInQuest, A4_Q3_KingOfFury.Tick);
                }
                if (CurrentAct != 4)
                {
                    UpdateGuiAndLog(Quests.KingOfFury.Name, "Travel to Act 4");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.Highgate), null);
                }

                int state = GetState(Quests.KingOfFury);
                if (state == 0)
                {
                    CompletedQuests.Instance.Add(Quests.KingOfFury);
                    return QuestHandler.QuestAddedToCache;
                }
                if (state >= 5)
                {
                    UpdateGuiAndLog(Quests.KingOfFury.Name, "Kill Kaom");
                    return new QuestHandler(A4_Q3_KingOfFury.KillKaom, A4_Q3_KingOfFury.Tick);
                }
                if (state <= 2)
                {
                    UpdateGuiAndLog(Quests.EternalNightmare.Name, "Take reward");
                    return new QuestHandler(A4_Q4_KingOfDesire.TakeReward, null);
                }
            }

            //King of Desire
            if (QuestIsNotCompleted(Quests.KingOfDesire))
            {
                if (Helpers.PlayerHasQuestItem(QuestItemMetadata.EyeOfDesire))
                {
                    UpdateGuiAndLog(Quests.KingOfDesire.Name, "Bring Eye of Desire to Dialla");
                    return new QuestHandler(A4_Q4_KingOfDesire.TurnInQuest, A4_Q4_KingOfDesire.Tick);
                }
                if (CurrentAct != 4)
                {
                    UpdateGuiAndLog(Quests.KingOfDesire.Name, "Travel to Act 4");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.Highgate), null);
                }

                int state = GetState(Quests.KingOfDesire);
                if (state == 0)
                {
                    CompletedQuests.Instance.Add(Quests.KingOfDesire);
                    return QuestHandler.QuestAddedToCache;
                }
                if (state >= 5)
                {
                    UpdateGuiAndLog(Quests.KingOfDesire.Name, "Kill Daresso");
                    return new QuestHandler(A4_Q4_KingOfDesire.KillDaresso, A4_Q4_KingOfDesire.Tick);
                }
                if (state <= 2)
                {
                    UpdateGuiAndLog(Quests.EternalNightmare.Name, "Take reward");
                    return new QuestHandler(A4_Q4_KingOfDesire.TakeReward, null);
                }
            }

            //Eternal Nightmare
            if (QuestIsNotCompleted(Quests.EternalNightmare, 2))
            {
                if (CurrentAct != 4)
                {
                    UpdateGuiAndLog(Quests.EternalNightmare.Name, "Travel to Act 4");
                    return new QuestHandler(() => TravelToProperAct(AreaNames.Highgate), null);
                }
                int state = GetState(Quests.EternalNightmare);
                if (state <= 2)
                {
                    CompletedQuests.Instance.Add(Quests.EternalNightmare);
                    return QuestHandler.QuestAddedToCache;
                }
                UpdateGuiAndLog(Quests.EternalNightmare.Name, "Kill Malachai");
                return new QuestHandler(A4_Q5_EternalNightmare.KillMalachai, A4_Q5_EternalNightmare.Tick);
            }

            //Check if we have access to next difficulty
            var lastAct = World.LastOpenedAct;
            var lastDiff = lastAct.Difficulty;
            if (CurrentDiff < lastDiff)
            {
                var name = lastAct.Name;
                UpdateGuiAndLog("Travel", $"Go to last opened act: \"{name}\" ({lastDiff})");
                return new QuestHandler(() => GoToLastOpenedAct(name, lastDiff), null);
            }

            //Last thing we can do is try to enter Twilight Strand from Hightgate
            if (CurrentDiff != Difficulty.Merciless && World.HasWaypoint(AreaNames.Highgate))
            {
                UpdateGuiAndLog(Quests.EternalNightmare.Name, "Enter Twilight Strand");
                return new QuestHandler(A4_Q5_EternalNightmare.EnterStrand, null);
            }
            return QuestHandler.QuestsCompleted;
        }

        public static int GetState(DatQuestWrapper quest)
        {
            var state = LokiPoe.InGameState.GetCurrentQuestStateAccurate(quest.Id, CurrentDiff);
            if (state == null) return byte.MaxValue;
            return state.Id;
        }

        public static int GetStateInaccurate(DatQuestWrapper quest)
        {
            var state = LokiPoe.InGameState.GetCurrentQuestState(quest.Id, CurrentDiff);
            if (state == null) return byte.MaxValue;
            return state.Id;
        }

        public static bool QuestIsNotCompleted(DatQuestWrapper quest, int completedState = 0)
        {
            if (GetStateInaccurate(quest) <= completedState) return false;
            if (CompletedQuests.Instance.Contains(quest)) return false;
            return true;
        }

        public static QuestHandler AnalyzeBandits()
        {
            string chosenBandit = Settings.Instance.GetQuestReward(Quests.DealWithBandits.Id);

            if (chosenBandit != BanditHelper.AliraName && !Helpers.PlayerHasQuestItem(QuestItemMetadata.AmuletAlira))
            {
                UpdateGuiAndLog(Quests.DealWithBandits.Name, "Kill Alira");
                return new QuestHandler(A2_Q6_DealWithBandits.KillAlira, A2_Q6_DealWithBandits.Tick);
            }
            if (chosenBandit != BanditHelper.KraitynName && !Helpers.PlayerHasQuestItem(QuestItemMetadata.AmuletKraityn))
            {
                UpdateGuiAndLog(Quests.DealWithBandits.Name, "Kill Kraityn");
                return new QuestHandler(A2_Q6_DealWithBandits.KillKraityn, A2_Q6_DealWithBandits.Tick);
            }
            if (chosenBandit != BanditHelper.OakName && !Helpers.PlayerHasQuestItem(QuestItemMetadata.AmuletOak))
            {
                UpdateGuiAndLog(Quests.DealWithBandits.Name, "Kill Oak");
                return new QuestHandler(A2_Q6_DealWithBandits.KillOak, A2_Q6_DealWithBandits.Tick);
            }

            if (chosenBandit == BanditHelper.EramirName)
            {
                UpdateGuiAndLog(Quests.DealWithBandits.Name, "Help Eramir");
                return new QuestHandler(A2_Q6_DealWithBandits.HelpEramir, A2_Q6_DealWithBandits.Tick);
            }
            if (chosenBandit == BanditHelper.AliraName)
            {
                UpdateGuiAndLog(Quests.DealWithBandits.Name, "Help Alira");
                return new QuestHandler(A2_Q6_DealWithBandits.HelpAlira, A2_Q6_DealWithBandits.Tick);
            }
            if (chosenBandit == BanditHelper.KraitynName)
            {
                UpdateGuiAndLog(Quests.DealWithBandits.Name, "Help Kraityn");
                return new QuestHandler(A2_Q6_DealWithBandits.HelpKraityn, A2_Q6_DealWithBandits.Tick);
            }
            if (chosenBandit == BanditHelper.OakName)
            {
                UpdateGuiAndLog(Quests.DealWithBandits.Name, "Help Oak");
                return new QuestHandler(A2_Q6_DealWithBandits.HelpOak, A2_Q6_DealWithBandits.Tick);
            }
            return null;
        }

        public static QuestHandler AnalyzeBusts()
        {
            if (!Helpers.PlayerHasQuestItem(QuestItemMetadata.Bust + "1"))
            {
                UpdateGuiAndLog(Quests.VictarioSecrets.Name, "Grab Bust of Hector Titucius (1)");
                return new QuestHandler(A3_Q2_VictarioSecrets.GrabBust1, A3_Q2_VictarioSecrets.Tick);
            }
            if (!Helpers.PlayerHasQuestItem(QuestItemMetadata.Bust + "2"))
            {
                UpdateGuiAndLog(Quests.VictarioSecrets.Name, "Grab Bust of Gaius Sentari (2)");
                return new QuestHandler(A3_Q2_VictarioSecrets.GrabBust2, A3_Q2_VictarioSecrets.Tick);
            }
            if (!Helpers.PlayerHasQuestItem(QuestItemMetadata.Bust + "3"))
            {
                UpdateGuiAndLog(Quests.VictarioSecrets.Name, "Grab Bust of Marceus Lioneye (3)");
                return new QuestHandler(A3_Q2_VictarioSecrets.GrabBust3, A3_Q2_VictarioSecrets.Tick);
            }
            return null;
        }

        private static async Task<bool> TravelToProperAct(string areaName)
        {
            if (AreaNames.Current == areaName) return false;
            await Travel.To(areaName);
            return true;
        }

        private static async Task<bool> GoToLastOpenedAct(string act, Difficulty difficulty)
        {
            var current = LokiPoe.CurrentWorldArea;
            if (current.Name == act && current.Difficulty == difficulty)
                return false;

            if (!await PlayerAction.TakeWaypoint(act, difficulty))
                ErrorManager.ReportError();

            return true;
        }

        private static void UpdateGuiAndLog(string questName, string questState)
        {
            Settings.Instance.CurrentQuestName = questName;
            Settings.Instance.CurrentQuestState = questState;
            GlobalLog.Warn($"[QuestManager] {questName} - {questState}");
        }

        private static readonly Dictionary<int, int> StateToBustMap = new Dictionary<int, int>
        {
            [byte.MaxValue] = 1,
            [31] = 1,
            [30] = 1,
            [29] = 1,
            [27] = 1,
            [23] = 1,
            [22] = 1,
            [19] = 1,
            [18] = 1,
            [11] = 1,
            [28] = 2,
            [26] = 2,
            [21] = 2,
            [15] = 2,
            [14] = 2,
            [9] = 2,
            [25] = 3,
            [17] = 3,
            [13] = 3,
            [7] = 3
        };

        public class CompletedQuests
        {
            private static CompletedQuests _instance;
            public static CompletedQuests Instance => _instance ?? (_instance = new CompletedQuests());

            private readonly string _path;
            private Dictionary<string, Dictionary<Difficulty, HashSet<string>>> _cache;

            private CompletedQuests()
            {
                _path = Path.Combine(Configuration.Instance.Path, "CompletedQuests.json");

                if (File.Exists(_path))
                {
                    Load();
                }
                else
                {
                    _cache = new Dictionary<string, Dictionary<Difficulty, HashSet<string>>>();
                }
            }

            public void Add(DatQuestWrapper quest)
            {
                var charName = LokiPoe.Me.Name;
                var diff = CurrentDiff;

                Dictionary<Difficulty, HashSet<string>> difficulties;
                if (_cache.TryGetValue(charName, out difficulties))
                {
                    HashSet<string> questIds;
                    if (difficulties.TryGetValue(diff, out questIds))
                    {
                        questIds.Add(quest.Id);
                    }
                    else
                    {
                        difficulties.Add(diff, new HashSet<string> {quest.Id});
                    }
                }
                else
                {
                    difficulties = new Dictionary<Difficulty, HashSet<string>> {{diff, new HashSet<string> {quest.Id}}};
                    _cache.Add(charName, difficulties);
                }
                Save();
                GlobalLog.Debug($"[QuestManager] Quest \"{quest.Name}\"({quest.Id}) has been added to completed quests cache.");
            }

            public bool Contains(DatQuestWrapper quest)
            {
                var charName = LokiPoe.Me.Name;
                var diff = CurrentDiff;

                Dictionary<Difficulty, HashSet<string>> difficulties;
                if (_cache.TryGetValue(charName, out difficulties))
                {
                    HashSet<string> questIds;
                    if (difficulties.TryGetValue(diff, out questIds))
                    {
                        return questIds.Contains(quest.Id);
                    }
                }
                return false;
            }

            private void Save()
            {
                using (var writer = new StreamWriter(_path))
                {
                    var serializer = new JsonSerializer {Formatting = Formatting.Indented};
                    serializer.Serialize(writer, _cache);
                }
            }

            private void Load()
            {
                using (var reader = new StreamReader(_path))
                {
                    var serializer = new JsonSerializer();
                    _cache = (Dictionary<string, Dictionary<Difficulty, HashSet<string>>>)
                        serializer.Deserialize(reader, typeof(Dictionary<string, Dictionary<Difficulty, HashSet<string>>>));
                }
            }
        }
    }
}