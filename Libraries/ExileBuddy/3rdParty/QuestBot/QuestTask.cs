﻿using System.Threading.Tasks;
using EXtensions;
using Loki.Bot;
using Loki.Game;

namespace QuestBot
{
    public class QuestTask : ITask
    {
        private readonly Interval _scanInterval = new Interval(200);

        private QuestHandler _handler;

        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type != "task_execute" || LokiPoe.Me.IsDead) return false;

            if (_handler == null || !await _handler.Execute())
            {
                _handler = QuestManager.GetQuestHandler();
                if (_handler == null)
                {
                    GlobalLog.Error("[QuestTask] QuestManager returned null. Lets wait and see maybe something will change in game memory.");
                    ErrorManager.ReportError();
                    await Wait.SleepSafe(500);
                }
                else if (_handler == QuestHandler.QuestAddedToCache)
                {
                    GlobalLog.Debug("[QuestTask] Received QuestAddedToCache handler. Now getting handler again.");
                    _handler = null;
                }
                else if (_handler == QuestHandler.QuestsCompleted)
                {
                    GlobalLog.Warn("[QuestTask] It seems like all quests are complete.");
                    _handler = null;
                    BotManager.Stop();
                }
            }
            return true;
        }

        public void Tick()
        {
            if (!_scanInterval.Elapsed ||
                _handler?.Tick == null ||
                !LokiPoe.IsInGame ||
                LokiPoe.Me.IsDead) return;

            _handler.Tick();
        }

        #region Unused interface methods

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        public string Name => "QuestTask";
        public string Author => "ExVault";
        public string Description => "Task that executes quest logic.";
        public string Version => "1.0";

        #endregion
    }
}