﻿using System.Threading.Tasks;
using System.Windows.Controls;
using log4net;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;

//!CompilerOption|AddRef|HelixToolkit.dll
//!CompilerOption|AddRef|HelixToolkit.Wpf.dll

namespace nsAreaVisualizer
{
	internal class AreaVisualizer : IPlugin
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private Gui _instance;
		private AreaVisualizerData _data = new AreaVisualizerData();

		#region Implementation of IAuthored

		/// <summary> The name of the plugin. </summary>
		public string Name => "AreaVisualizer";

		/// <summary>The author of the plugin.</summary>
		public string Author => "Bossland GmbH";

		/// <summary> The description of the plugin. </summary>
		public string Description => "A plugin that provides basic area visualization.";

		/// <summary>The version of the plugin.</summary>
		public string Version => "0.0.1.1";

		#endregion

		#region Implementation of IBase

		/// <summary>Initializes this plugin.</summary>
		public void Initialize()
		{
			Log.DebugFormat("[AreaVisualizer] Initialize");
		}

		/// <summary>Deinitializes this object. This is called when the object is being unloaded from the bot.</summary>
		public void Deinitialize()
		{
			Log.DebugFormat("[AreaVisualizer] Deinitialize");
			_instance?.OnDeinitialize();
		}

		#endregion

		#region Implementation of IRunnable

		/// <summary> The plugin start callback. Do any initialization here. </summary>
		public void Start()
		{
			Log.DebugFormat("[AreaVisualizer] Start");
		}

		/// <summary> The plugin tick callback. Do any update logic here. </summary>
		public void Tick()
		{
			_data.Update();
		}

		/// <summary> The plugin stop callback. Do any pre-dispose cleanup here. </summary>
		public void Stop()
		{
			Log.DebugFormat("[AreaVisualizer] Stop");
		}

		#endregion

		#region Implementation of IConfigurable

		/// <summary>The settings object. This will be registered in the current configuration.</summary>
		public JsonSettings Settings => AreaVisualizerSettings.Instance;

		/// <summary> The plugin's settings control. This will be added to the Exilebuddy Settings tab.</summary>
		public UserControl Control => (_instance ?? (_instance = new Gui(() => PluginManager.IsEnabled(this), () => _data)));

		#endregion

		#region Implementation of ILogic

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		#endregion

		#region Implementation of IEnableable

		/// <summary> The plugin is being enabled.</summary>
		public void Enable()
		{
			Log.DebugFormat("[AreaVisualizer] Enable");
			LokiPoe.OnGuiTick += LokiPoeOnOnGuiTick;
		}

		/// <summary> The plugin is being disabled.</summary>
		public void Disable()
		{
			Log.DebugFormat("[AreaVisualizer] Disable");
			LokiPoe.OnGuiTick -= LokiPoeOnOnGuiTick;
		}

		#endregion

		#region Override of Object

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			return Name + ": " + Description;
		}

		#endregion

		private void LokiPoeOnOnGuiTick(object sender, GuiTickEventArgs guiTickEventArgs)
		{
			if (!BotManager.IsRunning)
			{
				using (LokiPoe.AcquireFrame())
				{
					_data.Update();
				}
			}
		}
	}
}