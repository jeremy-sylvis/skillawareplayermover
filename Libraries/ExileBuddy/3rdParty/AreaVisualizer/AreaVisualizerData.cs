﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loki.Common;
using Loki.Game;

namespace nsAreaVisualizer
{
	public class AreaVisualizerData
	{
		public bool IsValid { get; set; }

		public bool IsInGame { get; private set; }

		public uint Seed { get; private set; }

		public Vector2i MyPos { get; private set; }

		public Vector2 MyWorldPos { get; private set; }

		public CachedTerrainData CachedTerrainData { get; private set; }

		public void Update()
		{
			IsInGame = LokiPoe.IsInGame;

			if (IsInGame)
			{
				Seed = LokiPoe.LocalData.AreaHash;

				MyPos = LokiPoe.MyPosition;
				MyWorldPos = LokiPoe.MyWorldPosition;
				CachedTerrainData = LokiPoe.TerrainData.Cache;
			}
			else
			{
				Seed = 0;
			}

			IsValid = true;
		}
	}
}
