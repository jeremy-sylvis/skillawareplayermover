﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Loki;
using Loki.Common;
using Newtonsoft.Json;

namespace nsStuckDetection
{
    /// <summary>The method to use for chicken logout.</summary>
    public enum StuckDetectionMethods
    {
        /// <summary>Stops the bot.</summary>
        Stop,

        /// <summary>Logout to title screen.</summary>
        LogoutToTitleScreen,
    }

    /// <summary>Settings for the StuckDetection. </summary>
    public class StuckDetectionSettings : JsonSettings
    {
        private static StuckDetectionSettings _instance;

        /// <summary>The current instance for this class. </summary>
        public static StuckDetectionSettings Instance
        {
            get { return _instance ?? (_instance = new StuckDetectionSettings()); }
        }

        /// <summary>The default ctor. Will use the settings path "StuckDetection".</summary>
        public StuckDetectionSettings()
            : base(GetSettingsFilePath(Configuration.Instance.Name, string.Format("{0}.json", "StuckDetection")))
        {
            // Setup defaults here if needed for properties that don't support DefaultValue.

            if (MaxTimeInInstance == default(TimeSpan))
            {
                MaxTimeInInstance = TimeSpan.FromMinutes(30);
            }
        }

        private StuckDetectionMethods _stuckDetectionMethods;
        private bool _checkTimeInInstance;
        private TimeSpan _maxTimeInInstance;
        private bool _checkMovementBounds;
        private int _minBoundsX;
        private int _minBoundsY;
        private int _boundsSnapshots;
        private bool _checkPositionData;
        private int _positionThreshold;

        /// <summary>
        /// Should the position data of the bot be checked? This is for detecting the bot not moving at all.
        /// </summary>
        [DefaultValue(true)]
        public bool CheckPositionData
        {
            get { return _checkPositionData; }
            set
            {
                if (value.Equals(_checkPositionData))
                {
                    return;
                }
                _checkPositionData = value;
                NotifyPropertyChanged(() => CheckPositionData);
            }
        }

        /// <summary>
        /// How many times can the bot be detected at a position before it assumes it's stuck?
        /// </summary>
        [DefaultValue(25)]
        public int PositionThreshold
        {
            get { return _positionThreshold; }
            set
            {
                if (value.Equals(_positionThreshold))
                {
                    return;
                }
                _positionThreshold = value;
                NotifyPropertyChanged(() => PositionThreshold);
            }
        }

        /// <summary>
        /// Should the movement bounds of the bot be checked? This is to detect back and forth loops in smaller areas.
        /// </summary>
        [DefaultValue(true)]
        public bool CheckMovementBounds
        {
            get { return _checkMovementBounds; }
            set
            {
                if (value.Equals(_checkMovementBounds))
                {
                    return;
                }
                _checkMovementBounds = value;
                NotifyPropertyChanged(() => CheckMovementBounds);
            }
        }

        /// <summary>
        /// The number of movement bounds to process before checking to see if the bot is stuck.
        /// </summary>
        [DefaultValue(3)]
        public int BoundsSnapshots
        {
            get { return _boundsSnapshots; }
            set
            {
                if (value.Equals(_boundsSnapshots))
                {
                    return;
                }
                _boundsSnapshots = value;
                NotifyPropertyChanged(() => BoundsSnapshots);
            }
        }

        /// <summary>
        /// The minimal x bounds the bot must travel before triggering stuck detection.
        /// </summary>
        [DefaultValue(60)]
        public int MinBoundsX
        {
            get { return _minBoundsX; }
            set
            {
                if (value.Equals(_minBoundsX))
                {
                    return;
                }
                _minBoundsX = value;
                NotifyPropertyChanged(() => MinBoundsX);
            }
        }

        /// <summary>
        /// The minimal y bounds the bot must travel before triggering stuck detection.
        /// </summary>
        [DefaultValue(60)]
        public int MinBoundsY
        {
            get { return _minBoundsY; }
            set
            {
                if (value.Equals(_minBoundsY))
                {
                    return;
                }
                _minBoundsY = value;
                NotifyPropertyChanged(() => MinBoundsY);
            }
        }

        /// <summary>
        /// Should the time in instance be tracked? This is to catch stuck issues that cause the bot to stay in an instance for too long,
        /// but it can also interfere with normal botting if you are undergeared or exploring a really large area.
        /// </summary>
        [DefaultValue(true)]
        public bool CheckTimeInInstance
        {
            get { return _checkTimeInInstance; }
            set
            {
                if (value.Equals(_checkTimeInInstance))
                {
                    return;
                }
                _checkTimeInInstance = value;
                NotifyPropertyChanged(() => CheckTimeInInstance);
            }
        }

        /// <summary>
        /// The max time allowed in an instance.
        /// </summary>
        public TimeSpan MaxTimeInInstance
        {
            get { return _maxTimeInInstance; }
            set
            {
                if (value.Equals(_maxTimeInInstance))
                {
                    return;
                }
                _maxTimeInInstance = value;
                NotifyPropertyChanged(() => MaxTimeInInstance);
            }
        }

        /// <summary>
        /// What to do when the bot is detected as being stuck.
        /// </summary>
        [DefaultValue(StuckDetectionMethods.LogoutToTitleScreen)]
        public StuckDetectionMethods StuckDetectionMethod
        {
            get { return _stuckDetectionMethods; }
            set
            {
                if (value.Equals(_stuckDetectionMethods))
                {
                    return;
                }
                _stuckDetectionMethods = value;
                NotifyPropertyChanged(() => StuckDetectionMethod);
            }
        }

        [JsonIgnore]
        private List<StuckDetectionMethods> _allStuckDetectionMethods;

        /// <summary> </summary>
        [JsonIgnore]
        public List<StuckDetectionMethods> AllStuckDetectionMethods
        {
            get
            {
                return _allStuckDetectionMethods ?? (_allStuckDetectionMethods = new List<StuckDetectionMethods>
                {
                    StuckDetectionMethods.Stop,
                    StuckDetectionMethods.LogoutToTitleScreen
                });
            }
        }
    }
}
