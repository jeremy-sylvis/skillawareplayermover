﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;
using Exilebuddy;
using log4net;
using Loki.Bot;
using System;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;

namespace nsStuckDetection
{
    internal class StuckDetection : IPlugin
    {
        private static readonly ILog Log = Logger.GetLoggerInstanceForType();

        private readonly List<Vector2i> _bounds = new List<Vector2i>();
        private readonly List<Vector2i> _positions = new List<Vector2i>();
        private readonly Stopwatch _positionStopwatch = new Stopwatch();
        private readonly Stopwatch _tickStopwatch = new Stopwatch();

        private UserControl root;

        #region Implementation of IAuthored

        /// <summary> The name of the plugin. </summary>
        public string Name
        {
            get
            {
                return "StuckDetection";
            }
        }

        /// <summary> The description of the plugin. </summary>
        public string Description
        {
            get
            {
                return
                    "This the old plugin that adds some basic anti-stuck detection logic.";
            }
        }

        /// <summary>The author of the plugin.</summary>
        public string Author
        {
            get
            {
                return "Bossland GmbH";
            }
        }

        /// <summary>The version of the plugin.</summary>
        public string Version
        {
            get
            {
                return "0.0.1.1";
            }
        }

        #endregion

        #region Implementation of IBase

        /// <summary>Initializes this plugin.</summary>
        public void Initialize()
        {
            Log.DebugFormat("[StuckDetection] Initialize");
        }

        /// <summary>Deinitializes this object. This is called when the object is being unloaded from the bot.</summary>
        public void Deinitialize()
        {
            Log.DebugFormat("[StuckDetection] Deinitialize");
        }

        #endregion

        /// <summary> The plugin start callback. Do any initialization here. </summary>
        public void Start()
        {
            Log.DebugFormat("[StuckDetection] Start");

            _tickStopwatch.Restart();
        }

        /// <summary> The plugin tick callback. Do any update logic here. </summary>
        public void Tick()
        {
            if (!LokiPoe.IsInGame || LokiPoe.Me.IsInTown || LokiPoe.Me.IsInHideout || LokiPoe.Me.IsInMapRoom || LokiPoe.Me.IsDead)
            {
                _tickStopwatch.Restart();
                return;
            }

            if (!_positionStopwatch.IsRunning)
                _positionStopwatch.Start();

            if (_positionStopwatch.ElapsedMilliseconds > 1000)
            {
                _positions.Add(LokiPoe.MyPosition);
                _positionStopwatch.Restart();
            }

            if (_tickStopwatch.ElapsedMilliseconds < 30000)
                return;

            var isStuck = false;
            using (LokiPoe.Memory.ReleaseFrame(LokiPoe.UseHardlock))
            {
                var positionData = new Dictionary<Vector2i, int>();
                foreach (var position in _positions)
                {
                    if (!positionData.ContainsKey(position))
                        positionData.Add(position, 0);

                    positionData[position]++;
                }

                foreach (var kvp in positionData)
                {
                    if (kvp.Value > 1)
                    {
                        Log.DebugFormat("[StuckDetection] {0} => {1}", kvp.Key, kvp.Value);
                    }
                }

                // Should position data be checked?
                if (StuckDetectionSettings.Instance.CheckPositionData)
                {
                    foreach (var kvp in positionData)
                    {
                        if (kvp.Value > StuckDetectionSettings.Instance.PositionThreshold)
                        {
                            Log.InfoFormat(
                                "[StuckDetection] We have been at the position {0} {1} times. We are most likely stuck!",
                                kvp.Key, kvp.Value);

                            isStuck = true;
                        }
                    }
                }

                if (_positions.Count > 0)
                {
                    var minX = _positions.Min(p => p.X);
                    var minY = _positions.Min(p => p.Y);
                    var maxX = _positions.Max(p => p.X);
                    var maxY = _positions.Max(p => p.Y);

                    var bounds = new Vector2i(maxX - minX, maxY - minY);

                    if (StuckDetectionSettings.Instance.CheckMovementBounds)
                    {
                        Log.DebugFormat("[StuckDetection] bounds: {0}", bounds);
                        _bounds.Add(bounds);
                    }

                    // We are done with this snapshot of position data.
                    _positions.Clear();
                }

                // Should movement bounds be checked?
                if (StuckDetectionSettings.Instance.CheckMovementBounds)
                {
                    // Do we have enough snapshots?
                    if (_bounds.Count == StuckDetectionSettings.Instance.BoundsSnapshots)
                    {
                        // Check for a stuck.
                        var xBoundsThreshold = StuckDetectionSettings.Instance.MinBoundsX;
                        var yBoundsThreshold = StuckDetectionSettings.Instance.MinBoundsY;
                        if (_bounds[0].X < xBoundsThreshold && _bounds[1].X < xBoundsThreshold &&
                            _bounds[2].X < xBoundsThreshold && _bounds[0].Y < yBoundsThreshold &&
                            _bounds[1].Y < yBoundsThreshold && _bounds[2].Y < yBoundsThreshold)
                        {
                            Log.InfoFormat(
                                "[StuckDetection] The bounds threshold trigger has been actived. We are most likely stuck.");
                            isStuck = true;
                        }

                        // We are done with this snapshot of bounds data.
                        _bounds.Clear();
                    }
                }

                // Should we check time in instance?
                if (StuckDetectionSettings.Instance.CheckTimeInInstance)
                {
					var tii = (TimeSpan?)BotManager.CurrentBot.Execute("oldgrindbot_get_time_in_instance");
	                var tia = (TimeSpan?)BotManager.CurrentBot.Execute("oldgrindbot_get_time_in_area");
					if (tii != null && tia != null)
	                {
		                Log.DebugFormat("[StuckDetection] TimeInInstance: {0}", tii);
		                Log.DebugFormat("[StuckDetection] TimeInArea: {0}", tia);

		                // We shouldn't get stuck in town/hideouts, and since time can accumulate, don't trigger there.
		                if (!LokiPoe.Me.IsInTown && !LokiPoe.Me.IsInHideout &&
							tia > StuckDetectionSettings.Instance.MaxTimeInInstance)
		                {
			                Log.InfoFormat("[PoiManagerOnOnMetricsComplete] We have been in the current area for too long.");
			                isStuck = true;
		                }
	                }
                }
            }

            // Lastly, handle the stuck.
            if (isStuck)
            {
				BotManager.CurrentBot.Execute("oldgrindbot_set_new_instance_override");

				// Handle the stuck detection method.
				if (StuckDetectionSettings.Instance.StuckDetectionMethod == StuckDetectionMethods.Stop)
                {
                    BotManager.Stop();
                }
                else if (StuckDetectionSettings.Instance.StuckDetectionMethod ==
                         StuckDetectionMethods.LogoutToTitleScreen)
                {
                    var lttserr = LokiPoe.EscapeState.LogoutToTitleScreen();
                    if (lttserr != LokiPoe.EscapeState.LogoutError.None)
                    {
                        Log.ErrorFormat("[StuckDetection] LogoutToTitleScreen returned {0}.", lttserr);
                    }
                }
            }

            _tickStopwatch.Restart();
        }

        /// <summary> The plugin stop callback. Do any pre-dispose cleanup here. </summary>
        public void Stop()
        {
            Log.DebugFormat("[StuckDetection] Stop");
        }

        #region Implementation of IConfigurable

        /// <summary>The settings object. This will be registered in the current configuration.</summary>
        public JsonSettings Settings
        {
            get
            {
                return StuckDetectionSettings.Instance;
            }
        }

        /// <summary> The plugin's settings control. This will be added to the Exilebuddy Settings tab.</summary>
        public UserControl Control
        {
            get
            {
                if (root != null)
                    return root;

                using (var fs = new FileStream(Path.Combine(ThirdPartyLoader.GetInstance("StuckDetection").ContentPath, "SettingsGui.xaml"), FileMode.Open))
                {
                    root = (UserControl) XamlReader.Load(fs);

                    // Your settings binding here.
                    if (
                        !Wpf.SetupComboBoxItemsBinding(root, "StuckDetectionMethodComboBox", "AllStuckDetectionMethods",
                            BindingMode.OneWay, StuckDetectionSettings.Instance))
                    {
                        Log.DebugFormat(
                            "[SettingsControl] SetupComboBoxItemsBinding failed for 'StuckDetectionMethodComboBox'.");
                        throw new Exception("The SettingsControl could not be created.");
                    }

                    if (
                        !Wpf.SetupComboBoxSelectedItemBinding(root, "StuckDetectionMethodComboBox",
                            "StuckDetectionMethod", BindingMode.TwoWay, StuckDetectionSettings.Instance))
                    {
                        Log.DebugFormat(
                            "[SettingsControl] SetupComboBoxSelectedItemBinding failed for 'StuckDetectionMethodComboBox'.");
                        throw new Exception("The SettingsControl could not be created.");
                    }

                    if (!Wpf.SetupCheckBoxBinding(root, "CheckPositionDataCheckBox",
                        "CheckPositionData", BindingMode.TwoWay,
                        StuckDetectionSettings.Instance))
                    {
                        Log.DebugFormat(
                            "[SettingsControl] SetupTextBoxBinding failed for 'CheckPositionDataCheckBox'.");
                        throw new Exception("The SettingsControl could not be created.");
                    }

                    if (
                        !Wpf.SetupTextBoxBinding(root, "PositionThresholdTextBox",
                            "PositionThreshold", BindingMode.TwoWay,
                            StuckDetectionSettings.Instance))
                    {
                        Log.DebugFormat(
                            "[SettingsControl] SetupTextBoxBinding failed for 'PositionThresholdTextBox'.");
                        throw new Exception("The SettingsControl could not be created.");
                    }

                    if (!Wpf.SetupCheckBoxBinding(root, "CheckMovementBoundsCheckBox",
                        "CheckMovementBounds", BindingMode.TwoWay,
                        StuckDetectionSettings.Instance))
                    {
                        Log.DebugFormat(
                            "[SettingsControl] SetupTextBoxBinding failed for 'CheckMovementBoundsCheckBox'.");
                        throw new Exception("The SettingsControl could not be created.");
                    }

                    if (
                        !Wpf.SetupTextBoxBinding(root, "BoundsSnapshotsTextBox",
                            "BoundsSnapshots", BindingMode.TwoWay,
                            StuckDetectionSettings.Instance))
                    {
                        Log.DebugFormat(
                            "[SettingsControl] SetupTextBoxBinding failed for 'BoundsSnapshotsTextBox'.");
                        throw new Exception("The SettingsControl could not be created.");
                    }

                    if (
                        !Wpf.SetupTextBoxBinding(root, "MinBoundsXTextBox",
                            "MinBoundsX", BindingMode.TwoWay,
                            StuckDetectionSettings.Instance))
                    {
                        Log.DebugFormat(
                            "[SettingsControl] SetupTextBoxBinding failed for 'MinBoundsXTextBox'.");
                        throw new Exception("The SettingsControl could not be created.");
                    }

                    if (
                        !Wpf.SetupTextBoxBinding(root, "MinBoundsYTextBox",
                            "MinBoundsY", BindingMode.TwoWay,
                            StuckDetectionSettings.Instance))
                    {
                        Log.DebugFormat(
                            "[SettingsControl] SetupTextBoxBinding failed for 'MinBoundsYTextBox'.");
                        throw new Exception("The SettingsControl could not be created.");
                    }

                    if (!Wpf.SetupCheckBoxBinding(root, "CheckTimeInInstanceCheckBox",
                        "CheckTimeInInstance", BindingMode.TwoWay,
                        StuckDetectionSettings.Instance))
                    {
                        Log.DebugFormat(
                            "[SettingsControl] SetupTextBoxBinding failed for 'CheckTimeInInstanceCheckBox'.");
                        throw new Exception("The SettingsControl could not be created.");
                    }

                    if (
                        !TimePicker.SetupSelectedTimeBinding(root, "MaxTimeInInstanceTimePicker",
                            "MaxTimeInInstance", BindingMode.TwoWay, StuckDetectionSettings.Instance))
                    {
                        Log.DebugFormat(
                            "[SettingsControl] SetupCheckBoxBinding failed for 'MaxTimeInInstanceTimePicker'.");
                        throw new Exception("The SettingsControl could not be created.");
                    }

                    // Your settings event handlers here.

                    return root;
                }
            }
        }

        #endregion

        #region Implementation of IEnableable

        /// <summary> The plugin is being enabled.</summary>
        public void Enable()
        {
            Log.DebugFormat("[StuckDetection] Enable");
        }

        /// <summary> The plugin is being disabled.</summary>
        public void Disable()
        {
            Log.DebugFormat("[StuckDetection] Disable");
        }

        #endregion

        #region Implementation of ILogic

        /// <summary>
        /// Coroutine logic to execute.
        /// </summary>
        /// <param name="type">The requested type of logic to execute.</param>
        /// <param name="param">Data sent to the object from the caller.</param>
        /// <returns>true if logic was executed to handle this type and false otherwise.</returns>
        public async Task<bool> Logic(string type, params dynamic[] param)
        {
            if (type == "core_area_changed_event")
            {
                var oldSeed = (uint) param[0];
                var newSeed = (uint) param[1];
                var oldArea = (DatWorldAreaWrapper) param[2];
                var newArea = (DatWorldAreaWrapper) param[3];

                _bounds.Clear();
                _positions.Clear();
                _tickStopwatch.Restart();

                return true;
            }

            return false;
        }

        /// <summary>
        /// Non-coroutine logic to execute.
        /// </summary>
        /// <param name="name">The name of the logic to invoke.</param>
        /// <param name="param">The data passed to the logic.</param>
        /// <returns>Data from the executed logic.</returns>
        public object Execute(string name, params dynamic[] param)
        {
            return null;
        }

        #endregion

        #region Override of Object

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return Name + ": " + Description;
        }

        #endregion
    }
}