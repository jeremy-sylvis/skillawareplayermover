﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using Loki;
using Loki.Common;

namespace nsCommonEvents
{
	/// <summary>Settings for the Dev tab. </summary>
	public class CommonEventsSettings : JsonSettings
	{
		private static CommonEventsSettings _instance;

		/// <summary>The current instance for this class. </summary>
		public static CommonEventsSettings Instance
		{
			get
			{
				return _instance ?? (_instance = new CommonEventsSettings());
			}
		}

		/// <summary>The default ctor. Will use the settings path "CommonEvents".</summary>
		public CommonEventsSettings()
			: base(GetSettingsFilePath(Configuration.Instance.Name, string.Format("{0}.json", "CommonEvents")))
		{
		}
	}
}