﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Controls;
using log4net;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;

namespace nsCommonEvents
{
	internal class CommonEvents : IPlugin
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private Gui _instance;

		private static uint _seed;
		private static DatWorldAreaWrapper _areaEntry;

		private static bool _isDead;
		private static uint _isDeadSeed;
		private static readonly Dictionary<uint, int> DeathsPerInstance = new Dictionary<uint, int>();

		private static string _name;
		private static int _level;

		#region Implementation of IAuthored

		/// <summary> The name of the plugin. </summary>
		public string Name
		{
			get
			{
				return "CommonEvents";
			}
		}

		/// <summary>The author of the plugin.</summary>
		public string Author
		{
			get
			{
				return "Bossland GmbH";
			}
		}

		/// <summary> The description of the plugin. </summary>
		public string Description
		{
			get
			{
				return "A plugin that provides common events in a modular way.";
			}
		}

		/// <summary>The version of the plugin.</summary>
		public string Version
		{
			get
			{
				return "0.0.1.1";
			}
		}

		#endregion

		#region Implementation of IBase

		/// <summary>Initializes this plugin.</summary>
		public void Initialize()
		{
			Log.DebugFormat("[CommonEvents] Initialize");
		}

		/// <summary>Deinitializes this object. This is called when the object is being unloaded from the bot.</summary>
		public void Deinitialize()
		{
			Log.DebugFormat("[CommonEvents] Deinitialize");
		}

		#endregion

		#region Implementation of IRunnable

		/// <summary> The plugin start callback. Do any initialization here. </summary>
		public void Start()
		{
			Log.DebugFormat("[CommonEvents] Start");
		}

		/// <summary> The plugin tick callback. Do any update logic here. </summary>
		public void Tick()
		{
		}

		/// <summary> The plugin stop callback. Do any pre-dispose cleanup here. </summary>
		public void Stop()
		{
			Log.DebugFormat("[CommonEvents] Stop");
		}

		#endregion

		#region Implementation of IConfigurable

		/// <summary>The settings object. This will be registered in the current configuration.</summary>
		public JsonSettings Settings
		{
			get
			{
				return CommonEventsSettings.Instance;
			}
		}

		/// <summary> The plugin's settings control. This will be added to the Exilebuddy Settings tab.</summary>
		public UserControl Control
		{
			get
			{
				return (_instance ?? (_instance = new Gui()));
			}
		}

		#endregion

		#region Implementation of ILogic

		/// <summary>
		/// Coroutine logic to execute.
		/// </summary>
		/// <param name="type">The requested type of logic to execute.</param>
		/// <param name="param">Data sent to the object from the caller.</param>
		/// <returns>true if logic was executed to handle this type and false otherwise.</returns>
		public async Task<bool> Logic(string type, params dynamic[] param)
		{
			if (type == "plugin_coroutine_event")
			{
				//using (new PerformanceTimer("AreaChanged", 1))
				{
					var newSeed = LokiPoe.LocalData.AreaHash;
					if (newSeed != _seed)
					{
						var oldSeed = _seed;
						var oldEntry = _areaEntry;
						_seed = newSeed;
						_areaEntry = LokiPoe.CurrentWorldArea;

						Log.InfoFormat("[CommonEvents] core_area_changed_event");

						await Utility.BroadcastEventLogic("core_area_changed_event", oldSeed, _seed, oldEntry, _areaEntry);
					}
				}

				//using (new PerformanceTimer("PlayerDied", 1))
				{
					if (LokiPoe.Me.IsDead)
					{
						if (!_isDead)
						{
							var seed = LokiPoe.LocalData.AreaHash;
							if (_isDeadSeed != seed)
							{
								_isDeadSeed = seed;
								_isDead = true;
								if (!DeathsPerInstance.ContainsKey(seed))
								{
									DeathsPerInstance.Add(seed, 0);
								}
								DeathsPerInstance[seed]++;

								Log.InfoFormat("[CommonEvents] core_player_died_event({0})", DeathsPerInstance[seed]);

								await Utility.BroadcastEventLogic("core_player_died_event", DeathsPerInstance[seed]);
							}
						}
					}
					else
					{
						_isDead = false;
						_isDeadSeed = 0;
					}
				}

				//using (new PerformanceTimer("PlayerLeveled", 1))
				{
					var me = LokiPoe.Me;
					var name = me.Name;
					var level = me.Level;
					if (name != _name)
					{
						_name = name;
						_level = level;
					}
					else
					{
						if (level > _level)
						{
							_level = level;

							Log.InfoFormat("[CommonEvents] core_player_leveled_event({0})", level);

							await Utility.BroadcastEventLogic("core_player_leveled_event", level);
						}
					}
				}

				return true;
			}

			return false;
		}

		/// <summary>
		/// Non-coroutine logic to execute.
		/// </summary>
		/// <param name="name">The name of the logic to invoke.</param>
		/// <param name="param">The data passed to the logic.</param>
		/// <returns>Data from the executed logic.</returns>
		public object Execute(string name, params dynamic[] param)
		{
			return null;
		}

		#endregion

		#region Implementation of IEnableable

		/// <summary> The plugin is being enabled.</summary>
		public void Enable()
		{
			Log.DebugFormat("[CommonEvents] Enable");
		}

		/// <summary> The plugin is being disabled.</summary>
		public void Disable()
		{
			Log.DebugFormat("[CommonEvents] Disable");
		}

		#endregion

		#region Override of Object

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			return Name + ": " + Description;
		}

		#endregion
	}
}