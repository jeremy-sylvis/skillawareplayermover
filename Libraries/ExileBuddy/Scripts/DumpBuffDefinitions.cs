using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
    private static readonly ILog Log = Logger.GetLoggerInstanceForType();

    public void Execute()
    {
        var sb = new StringBuilder();
        sb.AppendFormat("[Index] [Id] [Name - Desc]");
        sb.AppendLine();
        using (LokiPoe.AcquireFrame())
        {
            foreach (var buff in Dat.BuffDefinitions)
            {
                sb.AppendFormat("[{0}] [{1}] [{2} - {3}]", buff.Index, buff.Id, buff.Name, buff.Desc);
                sb.AppendLine();
            }
        }
		Log.Debug(sb.ToString());
		Directory.CreateDirectory("dump");
        File.WriteAllText("dump\\BuffDefinitions-" + LokiPoe.SupportedClientVersion + ".txt", sb.ToString());
    }
}
