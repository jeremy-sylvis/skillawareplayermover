using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
    private static readonly ILog Log = Logger.GetLoggerInstanceForType();

    public void Execute()
    {
        var sb = new StringBuilder();
        sb.AppendFormat("Index,Id,Act,Name");
        sb.AppendLine();
        using (LokiPoe.AcquireFrame())
        {
            foreach (var entry in Dat.Quests)
            {
				sb.AppendFormat("{0},", entry.Index);
				sb.AppendFormat("{0},", entry.Id);
				sb.AppendFormat("{0},", entry.Act);
				sb.AppendFormat("{0}", entry.Name);
                sb.AppendLine();
            }
        }
		Log.Debug(sb.ToString());
		Directory.CreateDirectory("dump");
        File.WriteAllText("dump\\Quests-" + LokiPoe.SupportedClientVersion + ".csv", sb.ToString());
    }
}
