using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
	private static readonly ILog Log = Logger.GetLoggerInstanceForType();

	public void Execute()
	{
		var sb = new StringBuilder();
		sb.AppendFormat("Index,Key,Value");
		sb.AppendLine();
		using (LokiPoe.AcquireFrame())
		{
			foreach (var entry in Dat.ClientStrings)
			{
				sb.AppendFormat("{0},", entry.Index);
				sb.AppendFormat("\"{0}\",", entry.Key);
				sb.AppendFormat("\"{0}\"", entry.Value);
				sb.AppendLine();
			}
		}
		Log.Debug(sb.ToString());
		Directory.CreateDirectory("dump");
		File.WriteAllText("dump\\ClientStrings-" + LokiPoe.SupportedClientVersion + ".csv", sb.ToString());
	}
}
