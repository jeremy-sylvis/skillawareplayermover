using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
	private static readonly ILog Log = Logger.GetLoggerInstanceForType();

	public void Execute()
	{
		var sb = new StringBuilder();
		sb.AppendFormat("Index,NpcName,Text");
		sb.AppendLine();
		using (LokiPoe.AcquireFrame())
		{
			foreach (var entry in Dat.NpcTalk)
			{
				sb.AppendFormat("{0},", entry.Index);
				sb.AppendFormat("\"{0}\",", entry.NpcName);
				sb.AppendFormat("\"{0}\"", entry.Text);
				sb.AppendLine();
			}
		}
		Log.Debug(sb.ToString());
		Directory.CreateDirectory("dump");
		File.WriteAllText("dump\\NpcTalk-" + LokiPoe.SupportedClientVersion + ".csv", sb.ToString());
	}
}
