using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
    private static readonly ILog Log = Logger.GetLoggerInstanceForType();

    public void Execute()
    {
        var sb = new StringBuilder();
        sb.AppendFormat("Index,Id,Name,Class");
        sb.AppendLine();
        using (LokiPoe.AcquireFrame())
        {
            foreach (var entry in Dat.BaseItemTypes)
            {
				sb.AppendFormat("{0},", entry.Index);
				sb.AppendFormat("{0},", entry.Metadata);
				sb.AppendFormat("\"{0}\",", entry.Name);
				sb.AppendFormat("{0}", entry.ItemClass);
				sb.AppendLine();
            }
        }
		Log.Debug(sb.ToString());
		Directory.CreateDirectory("dump");
        File.WriteAllText("dump\\BaseItemTypes-" + LokiPoe.SupportedClientVersion + ".csv", sb.ToString());
    }
}
