using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
	private static readonly ILog Log = Logger.GetLoggerInstanceForType();

	public void Execute()
	{
		var sb = new StringBuilder();
		using (LokiPoe.AcquireFrame())
		{
			foreach (var entry in Dat.GrantedEffectsPerLevel)
			{
				sb.Append(entry.ToString());
				sb.AppendLine();
			}
		}
		Log.DebugFormat("GrantedEffectsPerLevel saved to file!");
		Directory.CreateDirectory("dump");
		File.WriteAllText("dump\\GrantedEffectsPerLevel-" + LokiPoe.SupportedClientVersion + ".txt", sb.ToString());
	}
}
