using System;
using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
    private static readonly ILog Log = Logger.GetLoggerInstanceForType();

    public void Execute()
    {
        var sb = new StringBuilder();
        sb.AppendFormat("namespace Loki.Game.GameData"); sb.AppendLine();
        sb.AppendFormat("{{"); sb.AppendLine();
        sb.AppendFormat("    // ReSharper disable UnusedMember.Global"); sb.AppendLine();
        sb.AppendFormat("    #pragma warning disable 1591"); sb.AppendLine();
        sb.AppendLine();
        if (LokiPoe.ClientVersion == LokiPoe.PoeVersion.Official || LokiPoe.ClientVersion == LokiPoe.PoeVersion.OfficialSteam)
            sb.AppendFormat("    public enum StatTypeGGG");
        else 
            throw new NotImplementedException();
        sb.AppendLine();
        sb.AppendFormat("    {{"); sb.AppendLine();
        using (LokiPoe.AcquireFrame())
        {
            foreach (var stat in Dat.Stats)
            {
                sb.AppendFormat("        {1} = {0},", stat.Index, stat.ApiId);
                sb.AppendLine();
            }
        }
        sb.AppendFormat("    }}"); sb.AppendLine();
        sb.AppendFormat("}}"); sb.AppendLine();
        Directory.CreateDirectory("dump");
        if (LokiPoe.ClientVersion == LokiPoe.PoeVersion.Official || LokiPoe.ClientVersion == LokiPoe.PoeVersion.OfficialSteam)
            File.WriteAllText("dump\\StatTypeGGG.cs", sb.ToString());
        else 
            throw new NotImplementedException();
    }
}
