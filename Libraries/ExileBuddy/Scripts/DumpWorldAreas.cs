using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
    private static readonly ILog Log = Logger.GetLoggerInstanceForType();

    public void Execute()
    {
        var sb = new StringBuilder();
        sb.AppendFormat("[Id | Index] [Name] [Connections] [CorruptedAreas]");
        sb.AppendLine();
        using (LokiPoe.AcquireFrame())
        {
            foreach (var worldArea in Dat.WorldAreas)
            {
                sb.AppendFormat("[{0} | {4}] [{1}] (Level {2}) | IsMap: {3}", worldArea.Id, worldArea.Name, worldArea.MonsterLevel, worldArea.IsMap, worldArea.Index);
                if (worldArea.Connections.Count > 0)
                {
                    sb.AppendFormat(" Connections: [");
                    foreach (var connection in worldArea.Connections)
                    {
                        sb.AppendFormat("[{0} - {1}]", connection.Id, connection.Name);
                    }
                    sb.AppendFormat("]");
                }
                if (worldArea.CorruptedAreas.Count > 0)
                {
                    sb.AppendFormat(" CorruptedAreas: [");
                    foreach (var area in worldArea.CorruptedAreas)
                    {
                        sb.AppendFormat("[{0} - {1}]", area.Id, area.Name);
                    }
                    sb.AppendFormat("]");
                }
                sb.AppendLine();
            }
        }
		Log.Debug(sb.ToString());
		Directory.CreateDirectory("dump");
        File.WriteAllText("dump\\WorldAreas-" + LokiPoe.SupportedClientVersion + ".txt", sb.ToString());
    }
}
