using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
    private static readonly ILog Log = Logger.GetLoggerInstanceForType();

    public void Execute()
    {
        var sb = new StringBuilder();
        sb.AppendFormat("[Index] [Id] [Text]");
        sb.AppendLine();
        using (LokiPoe.AcquireFrame())
        {
            foreach (var type in Dat.BackendErrors)
            {
                sb.AppendFormat("[{0}] [{1}] [{2}]", type.Index, type.Id, type.Text);
                sb.AppendLine();
            }
        }
		Log.Debug(sb.ToString());
		Directory.CreateDirectory("dump");
        File.WriteAllText("dump\\BackendErrors-" + LokiPoe.SupportedClientVersion + ".txt", sb.ToString());
    }
}
