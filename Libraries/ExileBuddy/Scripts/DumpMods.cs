using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
	private static readonly ILog Log = Logger.GetLoggerInstanceForType();

	public void Execute()
	{
		var sb = new StringBuilder();
		sb.AppendFormat("Index");
		sb.AppendLine();
		using (LokiPoe.AcquireFrame())
		{
			foreach (var entry in Dat.Mods)
			{
				sb.AppendFormat("[{0}] {1}", entry.Index, entry.ToString());
				sb.AppendLine();
			}
		}
		//Log.Debug(sb.ToString());
		Log.DebugFormat("Mods saved to file!");
		Directory.CreateDirectory("dump");
		File.WriteAllText("dump\\Mods-" + LokiPoe.SupportedClientVersion + ".txt", sb.ToString());
	}
}
