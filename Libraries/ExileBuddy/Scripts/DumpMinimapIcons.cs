using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
	private static readonly ILog Log = Logger.GetLoggerInstanceForType();

	public void Execute()
	{
		var sb = new StringBuilder();
		sb.AppendFormat("Index,Name,_04,_08,_0C,_0D,_0E");
		sb.AppendLine();
		using (LokiPoe.AcquireFrame())
		{
			foreach (var entry in Dat.MinimapIcons)
			{
				sb.AppendFormat("{0},", entry.Index);
				sb.AppendFormat("{0},", entry.Name);
				sb.AppendFormat("{0},", entry._04);
				sb.AppendFormat("{0},", entry._08);
				sb.AppendFormat("{0},", entry._0C);
				sb.AppendFormat("{0},", entry._0D);
				sb.AppendFormat("{0}", entry._0E);
				sb.AppendLine();
			}
		}
		Log.Debug(sb.ToString());
		Directory.CreateDirectory("dump");
		File.WriteAllText("dump\\MinimapIcons-" + LokiPoe.SupportedClientVersion + ".csv", sb.ToString());
	}
}
