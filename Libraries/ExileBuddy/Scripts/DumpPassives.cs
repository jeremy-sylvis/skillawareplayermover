using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
    private static readonly ILog Log = Logger.GetLoggerInstanceForType();

    public void Execute()
    {
        var sb = new StringBuilder();
        sb.AppendFormat("[Name] [Id] [stat1: value1 | stat2: value2 | stat3: value3 | stat4: value4]");
        sb.AppendLine();
        using (LokiPoe.AcquireFrame())
        {
            foreach (var passive in Dat.PassiveSkills)
            {
                sb.AppendFormat("{0} [{1}] [", passive.Name, passive.Id);
                foreach (var kvp in passive.Stats)
                {
                    sb.AppendFormat("{0}: {1} |", kvp.Key.Id, kvp.Value);
                }
                sb.AppendFormat("]");
                sb.AppendLine();
            }
        }
		Log.Debug(sb.ToString());
		Directory.CreateDirectory("dump");
        File.WriteAllText("dump\\PassiveSkills-" + LokiPoe.SupportedClientVersion + ".txt", sb.ToString());
    }
}
