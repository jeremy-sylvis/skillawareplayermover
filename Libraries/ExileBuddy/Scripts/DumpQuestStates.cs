using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
    private static readonly ILog Log = Logger.GetLoggerInstanceForType();

    public void Execute()
    {
        var sb = new StringBuilder();
        sb.AppendFormat("[Index] [StateId] [QuestProgressText] [QuestStateText]");
        sb.AppendLine();
        using (LokiPoe.AcquireFrame())
        {
            foreach (var state in Dat.QuestStates)
            {
                sb.AppendFormat("[{0}] [{1}] [{2}] [{3}]", state.Index, state.StateId, state.QuestProgressText, state.QuestStateText);
                sb.AppendLine();
            }
        }
		Log.Debug(sb.ToString());
		Directory.CreateDirectory("dump");
        File.WriteAllText("dump\\QuestStates-" + LokiPoe.SupportedClientVersion + ".txt", sb.ToString());
    }
}
