using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
    private static readonly ILog Log = Logger.GetLoggerInstanceForType();

    public void Execute()
    {
        var sb = new StringBuilder();
        sb.AppendFormat("[Index] [ApiId] [Id] [Name]");
        sb.AppendLine();
        using (LokiPoe.AcquireFrame())
        {
            foreach (var stat in Dat.Stats)
            {
                sb.AppendFormat("[{0}] [{1}] [{2}] [{3}]", stat.Index, stat.ApiId, stat.Id, stat.Name);
                sb.AppendLine();
            }
        }
		Log.Debug(sb.ToString());
		Directory.CreateDirectory("dump");
        File.WriteAllText("dump\\Stats-" + LokiPoe.SupportedClientVersion + ".txt", sb.ToString());
    }
}
