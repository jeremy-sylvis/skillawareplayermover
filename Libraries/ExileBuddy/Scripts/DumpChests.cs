using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
    private static readonly ILog Log = Logger.GetLoggerInstanceForType();

    public void Execute()
    {
        var sb = new StringBuilder();
        sb.AppendFormat("[Metadata] [Name]");
        sb.AppendLine();
        using (LokiPoe.AcquireFrame())
        {
            foreach (var chest in Dat.Chests)
            {
                sb.AppendFormat("[{0}] [{1}]", chest.Metadata, chest.Name);
                sb.AppendLine();
            }
        }
		Log.Debug(sb.ToString());
		Directory.CreateDirectory("dump");
        File.WriteAllText("dump\\Chests-" + LokiPoe.SupportedClientVersion + ".txt", sb.ToString());
    }
}
