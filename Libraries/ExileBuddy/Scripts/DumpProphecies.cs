using System.Text;
using System.IO;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Common;
using log4net;

public class RuntimeCode
{
	private static readonly ILog Log = Logger.GetLoggerInstanceForType();

	public void Execute()
	{
		var sb = new StringBuilder();
		sb.AppendFormat("[Index] [Id] [ProphecyId] [Name] [Description] [FlavourText]");
		sb.AppendLine();
		using (LokiPoe.AcquireFrame())
		{
			foreach (var prophecy in Dat.Prophecies)
			{
				sb.AppendFormat("[{0}] [{1}] [{2}|{3}|{4}|{5}] [{6}] [{7}] [{8}]", prophecy.Index, prophecy.Id,
					prophecy.ProphecyId, prophecy.SealCostNormal, prophecy.SealCostCruel, prophecy.SealCostMerciless,
					prophecy.Name, prophecy.Description, prophecy.FlavourText);
				sb.AppendLine();
				foreach (var datClientStringWrapper in prophecy.ClientStrings)
				{
					sb.AppendFormat("\t{0}: {1}", datClientStringWrapper.Key, datClientStringWrapper.Value);
					sb.AppendLine();
				}
				sb.AppendLine();
			}
		}
		Log.Debug(sb.ToString());
		Directory.CreateDirectory("dump");
		File.WriteAllText("dump\\Prophecies-" + LokiPoe.SupportedClientVersion + ".txt", sb.ToString());
	}
}